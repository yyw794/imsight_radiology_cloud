#!/bin/python
# coding=utf-8
from Label import Label
import copy


class Nodule_Label(Label):
    def __init__(self):
        Label.__init__(self)
        self.x = 0
        self.y = 0
        self.z = 0
        self.url = ""
        self.nodule_prob = 0.0
        self.nodule_type = {"Benign": 1.0, "Mainly_Benign": 0.0, "Mainly_Malignant": 0.0,
                            "Malignant": 0.0}
        # self.nodule_type_prob = 0.0
        self.nodule_bm = {"Solid": 1.0, "Calcification": 0.0, "Ground_Glass": 0.0,
                          "Part_Solid": 0.0}
        # self.nodule_bm_prob = 0.0
        self.nodule_volume = 0
        self.nodule_diameter = 0
        self.nodule_avgHU = 0
        self.nodule_location = 0
        self.nodule_slice = []
        self.addByUser = False
        self.nodule_cross = []

    def to_dict(self):
        # if self.nodule_type:
        #     type_probs = self.nodule_type.values()
        #     argmax = np.argmax(type_probs)
        #     nodule_type_prob = type_probs[argmax]
        #     nodule_type = self.nodule_type.keys()[argmax]
        # else:
        #     nodule_type_prob = 0.0
        #     nodule_type = "Error"
        #
        # if self.nodule_bm:
        #     bm_probs = self.nodule_bm.values()
        #     argmax = np.argmax(bm_probs)
        #     nodule_bm_prob = bm_probs[argmax]
        #     nodule_bm = self.nodule_bm.keys()[argmax]
        # else:
        #     nodule_bm_prob = 0.0
        #     nodule_bm = "Error"

        return {
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "coord": {"x": self.x, "y": self.y, "z": self.z},
            "width": 20,
            "height": 20,
            "nodule_prob": self.nodule_prob,
            "nodule_type": self.nodule_type,
            # "nodule_type_prob": nodule_type_prob,
            "nodule_bm": self.nodule_bm,
            # "nodule_bm_prob": nodule_bm_prob,
            "nodule_volume": self.nodule_volume,
            "nodule_diameter": self.nodule_diameter,
            "nodule_avgHU": self.nodule_avgHU,
            "nodule_location": self.nodule_location,
            "state": self.state,
            "url": self.url,
            "addByUser": self.addByUser,
            "nodule_cross": self.nodule_cross
        }

    def to_db(self):
        return {
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "state": self.state,
            "url": self.url,
            "x": self.x,
            "y": self.y,
            "z": self.z,
            "nodule_prob": self.nodule_prob,
            "nodule_type": self.nodule_type,
            # "nodule_type_prob": self.nodule_type_prob,
            "nodule_bm": self.nodule_bm,
            # "nodule_bm_prob": self.nodule_bm_prob,
            "nodule_volume": self.nodule_volume,
            "nodule_diameter": self.nodule_diameter,
            "nodule_avgHU": self.nodule_avgHU,
            "nodule_location": self.nodule_location,
            "nodule_slice": self.nodule_slice,
            "addByUser": self.addByUser,
            "nodule_cross": self.nodule_cross
        }


required_key = ["taskID", "labels", "studyUID", "labelType"]
required_key_label = ["labelID", "seriesID", "state", "x", "y", "z", \
                      "nodule_prob", "nodule_type", "nodule_bm", \
                      "nodule_volume", "nodule_diameter", "nodule_avgHU", "nodule_location",
                      "nodule_slice", "url",
                      "addByUser", "nodule_cross"]


def parseNoduleLabelist(labels):
    labelist = []
    for label in labels:
        if not all(key in label for key in required_key_label):
            raise ValueError(
                "Noule label must contain {}, while input contains {}".format(required_key_label,
                                                                              label.keys()))

        new_Nodulelabel = Nodule_Label()
        new_Nodulelabel.labelID = label['labelID']
        new_Nodulelabel.seriesID = label['seriesID']
        new_Nodulelabel.state = label['state']
        new_Nodulelabel.x = label['x']
        new_Nodulelabel.y = label['y']
        new_Nodulelabel.z = label['z']
        new_Nodulelabel.nodule_prob = label['nodule_prob']
        new_Nodulelabel.nodule_volume = label['nodule_volume']
        new_Nodulelabel.nodule_diameter = label['nodule_diameter']
        new_Nodulelabel.nodule_avgHU = label['nodule_avgHU']
        new_Nodulelabel.nodule_location = label['nodule_location']
        new_Nodulelabel.url = label['url']
        new_Nodulelabel.addByUser = label['addByUser']
        if label['nodule_type'] != None:
            new_Nodulelabel.nodule_type = label['nodule_type']
        if label['nodule_bm'] != None:
            new_Nodulelabel.nodule_bm = label['nodule_bm']
        if label['nodule_slice'] != None:
            new_Nodulelabel.nodule_slice = label['nodule_slice']
        new_Nodulelabel.nodule_cross = label["nodule_cross"]
        labelist.append(new_Nodulelabel)

    return labelist


def format_nodule_label_info(labels):
    """
    format nodule predicted result
    :return:
    """
    new_labels = copy.deepcopy(labels)
    for label in new_labels["labels"]:
        new_nodule_cross = []
        cross_list = label["nodule_cross"]
        for cross in cross_list:
            end_point = cross["diameter_endpoints"]
            new_nodule_cross.append({"z": cross["z"] + 1,
                                     "y": cross["y"] + 1,
                                     "x": cross["x"] + 1,
                                     "major_diam": [(end_point["major_diam"]["x_min"] + 1,
                                                     end_point["major_diam"]["y_min"] + 1),
                                                    (end_point["major_diam"]["x_max"] + 1,
                                                     end_point["major_diam"]["y_max"] + 1)],
                                     "minor_diam": [(end_point["minor_diam"]["x_min"] + 1,
                                                     end_point["minor_diam"]["y_min"] + 1),
                                                    (end_point["minor_diam"]["x_max"] + 1,
                                                     end_point["minor_diam"]["y_max"] + 1)]})
        label["nodule_cross"] = new_nodule_cross
    return new_labels
