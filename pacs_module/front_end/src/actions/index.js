const SET_LANGUAGE = 'setLanguage'
export const setLanguage = lang => ({type: SET_LANGUAGE, lang})

export default {
  SET_LANGUAGE,
}
