#!/bin/python
# coding=utf-8
import os
import sys

from flask import make_response, request, jsonify, abort
from flask_login import current_user

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_update_one, db_insert_one
from models.response.response import response
from models.template.Template import Template, parseTemplateInfo, saveTemplatelist

required_key_delete_put = ['templateID']
required_key_put = ['templateID', 'templateName', 'imagingFind', 'diagnosisAdvice']
required_key_post = ['templateName', 'bodyPartExamined', 'modality', 'imagingFind', 'diagnosisAdvice']


def on_template():
    """
    @api {get post put delete} /api/template  模板信息
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam (post参数) {String} bodyPartExamined 检查部位
    @apiParam (post参数) {String} createDate 创建时间
    @apiParam (post参数) {String} diagnosisAdvice 诊断建议
    @apiParam (post参数) {String} imagingFind  影像所见
    @apiParam (post参数) {String} modality 模式
    @apiParam (put参数) {String} templateID 模板ID
    @apiParam (put参数) {String} diagnosisAdvice 诊断建议
    @apiParam (put参数) {String} imagingFind  影像所见
    @apiParam (put参数) {String} modality 模式
    @apiParam (delete参数) {String} templateID 模板ID
    @apiParamExample {json} post Request-Example:
        {"bodyPartExamined":"CHEST","createDate":"20160518","diagnosisAdvice":"","imagingFind":"","modality":"DX",
        "templateName":"example","usergroup":"SYSTEM"}
    @apiParamExample {json} put Request-Example:
        {"diagnosisAdvice":"","imagingFind":"","modality":"DX","templateID":"example"}
    @apiParamExample {json} delete Request-Example:
        {"templateID":"example"}
    @apiSuccess {string} diagnosisAdvice 诊断建议
    @apiSuccess {string} imagingFind 影像所见
    @apiSuccess {string} templateName 模板名称
    @apiSuccess {string} templateID 模板ID
    @apiSuccessExample {json} get Success-Response:
    {
      "data": [
        {
          "bodyPartExamined": "CHEST",
          "createDate": "20160518",
          "diagnosisAdvice": "",
          "imagingFind": "",
          "modality": "DX",
          "templateID": "SYSDXCHEST001",
          "templateName": "template_exam1",
          "usergroup": "SYSTEM"
        }
      ]
    }
    @apiSuccessExample {json} post Success-Response:
    {
        "data": {
        "bodyPartExamined": "CHEST",
        "createDate": "20181204",
        "diagnosisAdvice": "",
        "imagingFind": "",
        "modality": "DX",
        "templateID": "USRCHESTDX001",
        "templateName": "example"
        },
        "message": "OK",
        "statusCode": 1000
    }
    @apiSuccessExample {json} put Success-Response:
    {
        "data": {
        "bodyPartExamined": "CHEST",
        "createDate": "20181204",
        "diagnosisAdvice": "",
        "imagingFind": "",
        "modality": "DX",
        "templateID": "USRCHESTDX001",
        "templateName": "example"
        },
        "message": "OK",
        "statusCode": 1000
    }
    @apiSuccessExample {json} delete Success-Response:
    {
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "statusCode": 1100
    }
    """
    res = response()
    userID = current_user.get_id()

    if request.method == "GET":
        ret = []
        system_template = db_find_one("template", {"group": "system"})
        user_template = db_find_one("template", {"userID": userID})

        if system_template is not None:
            for sys_temp in system_template['templates']:
                template = parseTemplateInfo(sys_temp)
                sys_dict = template.to_dict()
                sys_dict['usergroup'] = 'SYSTEM'
                ret.append(sys_dict)

        if user_template is not None:
            for user_temp in user_template['templates']:
                template = parseTemplateInfo(user_temp)
                user_dict = template.to_dict()
                user_dict['usergroup'] = 'USER'
                ret.append(user_dict)

        res.statusCode = 1000
        res.data = ret
        return make_response(jsonify(res.to_dict()), 200)

    params = request.json
    if params is None:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    if request.method == "POST":
        if not all(key in params for key in required_key_post):
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        template = Template()
        template.bodyPartExamined = params['bodyPartExamined']
        template.templateName = params['templateName']
        template.modality = params['modality']
        template.imagingFind = params['imagingFind']
        template.diagnosisAdvice = params['diagnosisAdvice']

        user_template = db_find_one("template", {"userID": userID})
        if user_template is None:
            template.set_id(1, params['bodyPartExamined'], params['modality'])
            db_insert_one('template', saveTemplatelist([template], "user", userID))
        else:
            index = len(user_template['templates']) + 1
            template.set_id(index, params['bodyPartExamined'], params['modality'])
            user_template['templates'].append(template.to_db())
            db_update_one('template', {"userID": userID}, user_template)

        res.statusCode = 1000
        res.data = template.to_dict()
        return make_response(jsonify(res.to_dict()), 200)

    if not all(key in params for key in required_key_delete_put):
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    templateID = params['templateID']
    user_template = db_find_one("template", {"userID": userID})
    if user_template is None:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    user_templatelist = []
    for template_info in user_template['templates']:
        new_template = parseTemplateInfo(template_info)
        user_templatelist.append(new_template)
    template = filter(lambda t: t.id == templateID, user_templatelist)
    if not template:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    if request.method == "DELETE":
        user_templatelist.remove(template[0])

        db_update_one('template', {"userID": userID}, saveTemplatelist(user_templatelist, "user", userID))

        res.statusCode = 1000
        res.data = template[0].to_dict()
        return make_response(jsonify(res.to_dict()), 200)

    if request.method == "PUT":
        if not all(key in params for key in required_key_put):
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        template[0].templateName = params['templateName']
        template[0].imagingFind = params['imagingFind']
        template[0].diagnosisAdvice = params['diagnosisAdvice']

        db_update_one('template', {"userID": userID}, saveTemplatelist(user_templatelist, "user", userID))

        res.statusCode = 1000
        res.data = template[0].to_dict()
        return make_response(jsonify(res.to_dict()), 200)
