# -*- coding: utf-8 -*-
import time, datetime, thread, os, json
from mongodb_interface import *
import initconfig as config
from http_client import dcmtk_find_patient_list, dcmtk_move_study_uid,dcmtk_get_study_uid
import logging,logger
from risconnect import *
from ConcurrentQueue import *
import threading
log = logging.getLogger("PACSModule")


# 队列的方式来并发处理数据,多生产者多消费者
_request_data_queue = ConcurrentQueue(3000)
_current_request_thread_count = 0
_max_request_thread_count = config.get_thread_number()
_thread_cond = threading.Condition()
_request_cond = threading.Condition()


# 增加列表数据到队列
def _add_data_request_queue(sorted_condition_list):
    if len(sorted_condition_list) == 0:
        return False

    start_time = datetime.datetime.now()
    log.info("^_^ _add_data_request_queue begin")
    for scl in sorted_condition_list:
        _request_data_queue.put(scl)

    # 阻塞等待被唤醒再退出
    if _request_cond.acquire():
        _request_cond.wait()

        log.info("_add_data_request_queue release success")
        _request_cond.notify()
        _request_cond.release()

    end_time = datetime.datetime.now()
    log.info("^_^ _add_data_request_queue {} end depends on {}s".format(len(sorted_condition_list), (end_time - start_time).seconds))

    return (end_time - start_time).seconds > 60


# 死循环线程处理队列
def _request_data_common_function():
    global _current_request_thread_count
    while True:
        try:
            condition = _request_data_queue.get()  # 等待需要处理的数据
            log.info("_request_data_common_function studyUID {} thread number {} remain {}".
                     format(condition['studyUID'], _current_request_thread_count, _request_data_queue.size()))
            if _thread_cond.acquire():
                while _current_request_thread_count >= _max_request_thread_count:  # 判断是否已经超过了最大的线程数量
                    _thread_cond.wait()
                _current_request_thread_count += 1
                thread.start_new_thread(_request_a_data, (condition,))
                _thread_cond.notify()
                _thread_cond.release()
        except Exception as e:
            log.error(e)


# 处理请求一个数据
def _request_a_data(condition):
    global _current_request_thread_count

    start_time = datetime.datetime.now()
    log.info('_request_a_data begin StudyUID {} '.format(condition['studyUID']))
    move_result = pull_data_from_pacs_server(condition['studyUID'], condition['studyDate'])
    end_time = datetime.datetime.now()
    log.info('_request_a_data end StudyUID {} depends on {}s {}'.
             format(condition['studyUID'], (end_time - start_time).seconds, move_result))

    # 释放一个线程占用
    if _thread_cond.acquire():
        _current_request_thread_count = _current_request_thread_count - 1
        _thread_cond.notify()
        _thread_cond.release()

    # 判断是否唤醒需要拉取数据,发现队列里面没有数据
    if _request_data_queue.size() == 0 and _request_cond.acquire():
        _request_cond.notify()
        _request_cond.release()


def special_hospital_check(condition):
    # 深圳市人民医院已登记的病例 每次返回的uid不同 因为还没有拍照
    if condition['studyTime'] != "":
        current_time = (datetime.datetime.now()-datetime.timedelta(minutes=4)).strftime('%H:%M:%S').decode('utf-8')
        if condition['studyTime'] >= current_time:
            return False

        db_instance_list = \
            db_find_specific("study_info_list", {'patientID': condition['patientID']}, {"studyTime": True, "_id": False})
        if db_instance_list is not None and {"studyTime": condition['studyTime']} in db_instance_list:
            return False

    return True


# 拉取图像数据
def pull_data_from_pacs_server(studyUID, studyDate):
    if config.get_pull_data_method() == 'get':
        return dcmtk_get_study_uid(studyUID, studyDate)
    else:
        return dcmtk_move_study_uid(studyUID, studyDate)


# 循环处理单个线程的数据队列
def thread_do_task(sorted_condition_list):

    for index, sorted_condition in enumerate(sorted_condition_list):
        log.info('{} thread_do_task index {} StudyUID {} remain number {} '.
                format(threading.current_thread().name, index, sorted_condition['studyUID'], (len(sorted_condition_list)-index)))

        move_result = pull_data_from_pacs_server(sorted_condition['studyUID'], sorted_condition['studyDate'])

        log.info('{} thread_do_task end StudyUID {} result {} '.
                format(threading.current_thread().name, sorted_condition['studyUID'], move_result))
    log.info("thread_do_task {} end".format(threading.current_thread().name))


# 开一个或多个线程处理列表
def multi_thread_do_list(sorted_condition_list, thread_number):
    threads_list = []
    threads_data_list = []

    if thread_number < 1 or len(sorted_condition_list) == 0:
        return False

    for index in range(thread_number):
        threads_data_list.append([])

    data_number_in_thread = int(len(sorted_condition_list)/thread_number)
    for index in range(thread_number):
        threads_data_list[index].extend(sorted_condition_list[index*data_number_in_thread:(index+1)*data_number_in_thread])

    thread_index = 0
    for index in range(data_number_in_thread*thread_number,len(sorted_condition_list)):
        threads_data_list[thread_index].append(sorted_condition_list[index])
        thread_index += 1

    for index in range(thread_number):
        thread_temp = threading.Thread(target=thread_do_task,args=(threads_data_list[index],),name="thread_{}".format(index))
        threads_list.append(thread_temp)

    start_time = datetime.datetime.now()
    for tl in threads_list:
        tl.start()

    log.info("^_^ multi_thread_do_list all thread start {}".format(thread_number))

    for tl in threads_list:
        tl.join()

    end_time = datetime.datetime.now()
    log.info("^_^ multi_thread_do_list data {} all thread end {} depends on {}s".
             format(len(sorted_condition_list), thread_number, (end_time - start_time).seconds))

    return (end_time - start_time).seconds > 60


# 添加数据到数据库
def add_condition_list_to_db(condition_list):
    if len(condition_list) == 0:
        return []
    new_condition_list = []

    ris_patient_list = []
    is_rsi_support = config.get_is_ris_support()
    if is_rsi_support is True:
        ris_patient_list = get_patient_list_from_ris(condition_list[0]['studyDate'])
        if ris_patient_list is None:
            return []

    for condition in condition_list:

        if condition['modality'] != "" and condition['modality'] not in ["CT", "DX", "CR", "DR"]:
            continue

        if db_find_one("study_info_list", {'studyUID': condition['studyUID']}) is not None:
            continue

        if special_hospital_check(condition) is False:
            continue

        condition['state'] = "meta_only"
        condition['bodyPartExamined'] = ""
        condition['doctorRequest'] = False
        condition['match_ris'] = False

        if is_rsi_support is True:
            result, ris_patient = condition_is_ris_support(condition, ris_patient_list)
            if result is False:  # 没有匹配到就不加到数据库中
                continue
            else:
                condition['match_ris'] = True
                condition['modality'] = ris_patient['Modality']
                condition['bodyPartExamined'] = ris_patient['StudyPart']
                log.info("condition {} {} {} {} insert to db".
                    format(condition['modality'],condition['patientID'],condition['studyTime'],condition['bodyPartExamined']))

        try:  # 增加异常判断，patientName存在乱码添加不进去
            db_insert_one('study_info_list', condition)
            new_condition_list.append(condition)
        except Exception as e:
            log.error(e)
            log.error("add_condition_list_to_db to db error {}".format(condition))
    return new_condition_list


# 处理当天数据
def deal_this_day_list(study_date):
    condition = {'studyDate': study_date}
    
    # step 1: find this day study_instance_uid list
    content_result = dcmtk_find_patient_list(condition)
    if content_result is None:
        log.info("find patient list of this day {} exception ".format(study_date))
        return False

    content_result = json.loads(content_result)
    if content_result['result'] != 'SUCCESS':
        log.error("find patient list of this day {} error, message {} ".format(study_date, content_result['message']))
        return False

    condition_list = content_result['data']
    log.info("find patient list of this day {} number is {}".format(study_date, len(condition_list)))

    # step 2: insert this day study_instance_uid into db
    new_condition_list = add_condition_list_to_db(condition_list)
    log.info("add new number this day {} is {}".format(study_date, len(new_condition_list)))

    # step 3: get un_request study_instance_uid list and sort
    un_request_list = db_find('study_info_list', {"studyDate": study_date, "state": 'meta_only',
                                                  'match_ris': config.get_is_ris_support()})
    if un_request_list is None:
        return False

    sorted_condition_list = sorted(un_request_list, key=lambda s: s['studyTime'], reverse=True)
    log.info('get sorted un request condition list number {}'.format(len(sorted_condition_list)))
    sorted_condition_list = sorted_condition_list if len(sorted_condition_list) < 50 else sorted_condition_list[0:50]

    # step 4: request images one by one
    # result_flag = multi_thread_do_list(sorted_condition_list, config.get_thread_number())
    result_flag = _add_data_request_queue(sorted_condition_list)

    return result_flag


# 当天循环处理
def today_request_function():
    thread.start_new_thread(_request_data_common_function, ())  # 开启队列处理线程
    while True:
        try:
            today = datetime.datetime.now().strftime('%Y-%m-%d')
            log.info('begin request current day {} patient list'.format(today))

            # 先暂定写成true,防止flask调用查找今天的pacs server
            db_find_update_otherwise_insert('study_info_list_state', {'studyDate': today},
                                            {'studyDate': today, "is_same": True})

            if False is deal_this_day_list(today):
                time.sleep(60 * 3)  # three minutes
        except Exception as e:
            log.error(e)
            time.sleep(60 * 3)  # three minutes


def get_yesterday():
    today = datetime.date.today()
    one_day = datetime.timedelta(days=1)
    yesterday = today-one_day
    return yesterday.strftime('%Y-%m-%d')


# 一小时定时器
def time_task_a_hour():
    while True:
        try:
            current_hour = int(datetime.datetime.now().strftime('%H'))
            if config.get_if_push_model() is False:
                if 0 == current_hour:
                    yesterday = get_yesterday()
                    log.info("time_task_a_hour last time to get yesterday list {}".format(yesterday))
                    # todo add to db
            
                if 3 == current_hour:
                    log.info("time_task_a_hour to request history data")
                    # thread.start_new_thread(request_history_list, (5,))

            if 5 == current_hour:
                log.info("time_task_a_hour to clear history data")
                thread.start_new_thread(clear_history_data, ())
        except Exception as e:
            log.error(e)
        time.sleep(60 * 60)


# 系统装机
def init_do_something():
    today = datetime.datetime.now().strftime('%Y-%m-%d')

    system_info = db_find_one("system_info", {})

    if system_info is None:
        db_insert_one("system_info", {'list_back_date': today,   # 拉取多久之前的列表
                                     'list_back_image_date': today,  # 拉取多久之前的数据
                                     'oldest_date':today,  # 保存多久之前的数据
                                     'is_push': config.get_if_push_model(),  # 是否是推的模式
                                     'pull_data_method': config.get_pull_data_method()})  # 获取图像的方式move/get

    elif system_info['is_push'] != config.get_if_push_model() \
            or system_info['pull_data_method'] != config.get_pull_data_method():
        system_info['is_push'] = config.get_if_push_model()
        system_info['pull_data_method'] = config.get_pull_data_method()
        db_find_update_otherwise_insert('system_info', {'list_back_date': system_info['list_back_date']}, system_info)

    if False is os.path.exists('logfile'):
        os.mkdir('logfile')


# 清除历史数据
def clear_history_data():
    if config.get_store_image_days() <= 0:
        return

    before_day = datetime.timedelta(days=config.get_store_image_days())
    current_day = datetime.datetime.now()
    delete_day = (current_day - before_day).strftime('%Y-%m-%d')

    log.info('begin clear history before day {} data at {}'.format(delete_day, datetime.datetime.now()))
    clear_data_before_study_date(config.get_server_account(), delete_day, False)

    system_info = db_find_one("system_info", {})
    if system_info is not None:
        system_info['oldest_date'] = delete_day
        db_find_update_otherwise_insert('system_info', {'list_back_date': system_info['list_back_date']}, system_info)

    log.info('end clear history data at {}'.format(datetime.datetime.now()))


def clear_data_from_study_uid(user_id, study_uid, is_delete_folder):
    # ownership
    ownership_data = db_find_one("ownership", {"userID": user_id})
    study_uid_new = study_uid.replace('.', '-')
    if study_uid_new in ownership_data['shared']:
        task_id = ownership_data['shared'].pop(study_uid_new)
        db_find_update_otherwise_insert("ownership", {"userID": user_id}, ownership_data)

    # ai_lable
    db_delete_one("ai_lable", {"studyUID": study_uid})

    # label
    db_delete_one("label", {"studyUID": study_uid})

    # radiomics
    db_delete_one("radiomics", {"studyUID": study_uid})

    # report
    db_delete_one("report", {"studyUID": study_uid})

    # resource
    #db_delete_many("resource", {"uid":})

    # study
    #if is_delete_folder:
        #study_info = db_find_one("study", {"studyUID": study_uid})
        #if study_info is not None:
            #series_list = study_info['series']
            #for series in series_list:
                #series_id = series['seriesID']
                #folder = os.path.join(DICOM_ROOT, series_id)
                #if os.path.exists(folder):
                    #shutil.rmtree(folder)

    db_delete_one("study", {"studyUID": study_uid})

    # studylist
    db_delete_one("studylist", {"studyUID": study_uid})

    # task
    db_delete_one("task", {"studyUID": study_uid})

    # tool
    db_delete_one("tool", {"studyUID": study_uid})


# before studyDate 2018-11-18
def clear_data_before_study_date(user_id, study_date, is_delete_folder):
    study_list = db_find("studylist", {"studyDate": {'$lte': study_date}})
    if study_list is not None:
        for study in study_list:
            clear_data_from_study_uid(user_id, study['studyUID'], is_delete_folder)

    # delete study_list_state
    db_delete_many('study_info_list_state', {"studyDate": {'$lte': study_date}})

    # delete study_list
    db_delete_many('study_info_list',  {"studyDate": {'$lte': study_date}})

