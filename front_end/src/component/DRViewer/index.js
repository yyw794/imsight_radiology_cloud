import React, { Component } from 'react'
import * as cornerstone from 'cornerstone-core'
import * as dicomParser from 'dicom-parser';
import * as cornerstoneWebImageLoader from "cornerstone-web-image-loader"
import * as cornerstoneWADOImageLoader from 'cornerstone-wado-image-loader'
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import Hammer from "hammerjs"
import {Icon, message, Progress} from 'antd'
import Immutable from 'immutable'

import LabelPopover from './LabelPopover'
import {deepCopy, dataURItoBlob} from '../../util'
import {getUser, getUserRole} from '../../auth'
import api from '../../api'
import axios from 'axios'

import LesionsMarker from '../tools/LesionsMarker'
import {angle} from '../tools/angleTool'
import {freehand} from '../tools/freehand'
import {length} from '../tools/length'
import {zoom} from '../tools/zoom'
import _ from 'underscore'
import {CHEST_DISEASES_EN} from '../../data'
import loadImage from './loadImage'

import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './index.less'

cornerstoneTools.external.cornerstone = cornerstone
cornerstoneTools.external.cornerstoneMath = cornerstoneMath
cornerstoneWebImageLoader.external.cornerstone = cornerstone
cornerstoneTools.external.Hammer = Hammer
cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
cornerstoneWADOImageLoader.external.dicomParser = dicomParser;

const config = {
  webWorkerPath : '/cornerstoneWADOImageLoaderWebWorker.js',
  taskConfiguration: {
        'decodeTask' : {
        codecsPath: '/cornerstoneWADOImageLoaderCodecs.js'
    }
  }
};
cornerstoneWADOImageLoader.webWorkerManager.initialize(config);
/**
 * DR工具name定义
 * @type {string[]}
 */
const TOOLS_NAME = ['length', 'angle', 'freehand']

/**
 * DR视图组件
 */
class Viewer extends Component {
    /**
     * 参照react生命周期
     * @param props
     */
  constructor(props) {
    super(props)
    // const labelsMap = {}
    // for (let label of props.study.labels) {
    //   if (!(label.instanceID in labelsMap)) {
    //     labelsMap[label.instanceID] = []
    //   }
    //   labelsMap[label.instanceID].push(label)
    // }
        /**
         *
         * @private {{
         * viewport: *,
         * displayInstance: *,
         * loading: boolean, 加载条
         * loadPercent: number, 加载百分比
         * labelPopover: {  标记属性
         * show: boolean,
         * position: {
         * x: number,
         * y: number
         * },
         * value: string,
         * id: null,
         * imageElm: null,
         * isAdding: boolean 是否添加
         * },
         * invert: boolean 反转
         * }}
         */
    this.state = {
      // currentInstanceIdx: 0,
      // stack: props.stack,
      viewport: cornerstone.getDefaultViewport(null, undefined),
      // activeTool: 'pan',
      diseaseText:"",
      displayInstance: this.props.study.instances[0],
      loading: true,
      loadPercent: 0,
      // labels: props.labels.filter(label => !label.globalTag),
      // labels: Immutable.fromJS(labelsMap),
      labelPopover: {
        show: false,
        position: {x: 0, y: 0},
        value: '',
        id: null,
        imageElm: null,
        isAdding: false
      },
      invert: false
    }

    // this.currentInstanceIdx = 0
    // this.toolsData = Immutable.fromJS(props.study.tools)

    this.onImageRendered = this.onImageRendered.bind(this)
    this.onNewImage = this.onNewImage.bind(this)
    this.onWindowResize = this.onWindowResize.bind(this)
        /**
         * 保存工具数据
         */
    this.saveToolsData_ = _.debounce(this.saveToolsData, 1000)
        /**
         * 保存标记数据
         */
    this.saveLabelsData_ = _.debounce(this.saveLabelsData, 1000)

  }

    /**
     * 参照生命周期
     * @param nextProps
     */
  componentWillReceiveProps(nextProps) {
    if (nextProps.study.studyUID !== this.props.study.studyUID) {
      this.reset(nextProps.study)
    }
    if (nextProps.instance.instanceID !== this.props.instance.instanceID) {
      this.onInstanceChange(nextProps.instance, nextProps.tools, nextProps.labels)
    }
  }

  // async reset(study) {
  //   let {instances, labels, tools} = study
  //   const instance = instances[0]
  //   // this.toolsData = Immutable.fromJS(tools)
  //   this.setState({labels: Immutable.fromJS(labels)}, () => {
  //     this.displayInstance(instance).then(() => {
  //       this.restoreToolState(instance.instanceID)
  //     })
  //   })
  // }

    /**
     * 初始化工具
     */
  initTools() {
    const element = this.element

    // Add the stack tool state to the enabled element
    // const stack = this.props.stack
    // cornerstoneTools.addStackStateManager(element, ["stack"])
    // cornerstoneTools.addToolState(element, "stack", stack)
    cornerstoneTools.mouseInput.enable(element)
    cornerstoneTools.mouseWheelInput.enable(element)
        /**
         *
         * @type {{wwwc,
         * pan: AudioParam | number,
         * zoom,
         * length: {enable, disable, activate, deactivate, getConfiguration, setConfiguration, mouseDownCallback, mouseMoveCallback, mouseDownActivateCallback, clearSelectedToolData},
         * angle: {enable, disable, activate, deactivate, getConfiguration, setConfiguration, mouseDownCallback, mouseMoveCallback, mouseDownActivateCallback, clearSelectedToolData},
         * freehand: {enable, disable, activate, deactivate, getConfiguration, setConfiguration}}}
         */
    this.tools = {
      // zoomWheel: cornerstoneTools.zoomWheel,
      wwwc: cornerstoneTools.wwwc,
      pan: cornerstoneTools.pan,
      zoom: zoom,
      length,
      angle,
      freehand,
    }
    // this.tools.zoomWheel.activate(this.element) // zoomWheel is activated by default
    this.tools.pan.activate(this.element, 1) // pan is the default tool for left mouse button
    this.tools.zoom.setConfiguration({minScale: 0.01, maxScale: 100})
    this.tools.zoom.activate(this.element, 4) // zoom is the default tool for right mouse button
    this.tools.lesionsMarker = new LesionsMarker(this.element, this)
    this.tools.lesionsMarker.on(LesionsMarker.EVENTS.DATA_ADDED, this.showLabelPopover.bind(this))
    this.tools.lesionsMarker.on(LesionsMarker.EVENTS.DATA_DELETED, this.closeLabelPopover)
    this.tools.lesionsMarker.setShouldStopAdding(() => this.state.labelPopover.show)
    this.tools.lesionsMarker.enable()
    this.activateTool("wwwc",1)
    this.activateTool("pan",2)
    // when pressed esc, exit selected tool
    document.addEventListener('keydown', this.onKeyDown, false)
  }

    /**
     * 重置视图
     * @returns {Object}
     */
  resetViewport() {
    return  cornerstone.reset(this.element)

     //this.restoreToolState(this.props.tools, this.props.labels)
  }

    /**
     * 调窗
     */
  onWindowResize() {
    cornerstone.resize(this.element)
  }

    /**
     * 图片渲染
     */
  onImageRendered() {
    const viewport = cornerstone.getViewport(this.element)
    this.setState({
      viewport
    })
  }

    /**
     * 渲染新的图片
     */
  onNewImage() {
    const enabledElement = cornerstone.getEnabledElement(this.element)
    this.setState({
      imageId: enabledElement.image.imageId
    })
  }

    /**
     * 显示标记选项
     * @param id
     * @param position
     * @param value
     */
  showLabelPopover(id, position, value) {
    this.setState({
      labelPopover: {show: true, position, value, id, isAdding: true}
    })
  }

    /**
     * 关闭标记选项
     */
  closeLabelPopover = () => {
      this.setState({
        labelPopover: Object.assign({}, this.state.labelPopover, {show: false})
      })
      this.tools.lesionsMarker.endResizing();
      this.saveLabelsData()
  }

    /**
     * 修改标记的名字
     * @param id
     * @param name
     */
  changeLabelName(id, name) {
    // change label of lesions
    this.tools.lesionsMarker.setName(id, name)
    this.setState({labelPopover: Object.assign({}, this.state.labelPopover, {value: name})})
    this.tools.lesionsMarker.endResizing();
    this.closeLabelPopover()
  }

    /**
     * 进度条加载完毕后显示视图
     * @param instance
     * @returns {Promise<void>}
     */
  async displayInstance(instance) {
    let element = this.element
    this.setState({
      loading: true,
      loadPercent: 0
    })
    cornerstone.events.removeEventListener("cornerstoneimageloadprogress", this.onImageLoadProgress)
    cornerstone.events.addEventListener("cornerstoneimageloadprogress", this.onImageLoadProgress)
    let image = await loadImage(instance.url)
    this.setState({
      loading: false,
      loadPercent: 100
    })
    // TODO: define a new image loader, these meta data is defined in https://github.com/cornerstonejs/cornerstoneWebImageLoader/blob/master/src/createImage.js
    image.color = false

    // Display the first image
    cornerstone.displayImage(element, image)
    cornerstone.reset(element)
        console.log(this.props.study)
      if(this.props.study.is_negative){
      this.toggleInvert()
     }
  }

    /**
     * 更新本地工具数据
     */
  updateLocalToolData = () => {
    let instanceID = this.props.instance.instanceID
    let toolData = {}
    for (let name of TOOLS_NAME) {
      // if (name === 'angle') continue
      let state = cornerstoneTools.getToolState(this.element, name)
      if (!state) continue
      // let data = Immutable.fromJS(state.data).toJS()
      let data = state.data
      data.forEach(d => {
        d.active = false
        d.selected = false
      })
      toolData[name] = data
    }
    // console.log('updateToolData', this.loading, instanceID, toolData);
    // this.toolsData = this.toolsData.set(instanceID, Immutable.fromJS(toolData))
    // this.props.onToolDataChange(toolData)
  }

  updateLocalLabelData = () => {
    let labels = this.tools.lesionsMarker.getState()
    let instanceID = this.props.instance.instanceID
    // this.setState({
    //   labels: this.state.labels.set(instanceID, Immutable.fromJS(labels))
    // })
  }

  /**
   * display another instance
   * @param  {json} instance [instance data]
   * @return {undefined}
   */
  onInstanceChange = (instance, tools, labels) => {
    // this.updateLocalToolData()
    this.displayInstance(instance).then(()=>{
    this.restoreToolState(tools, labels)})
  }
    /**
     * 加载进度条百分比变化
     * @param e
     */
  onImageLoadProgress = e => {
    this.setState({
      loadPercent: e.detail.percentComplete
    })
  }

    /**
     * 参照生命周期函数
     */
  componentDidMount() {
    const element = this.element

    // Enable the DOM Element for use with Cornerstone
    cornerstone.enable(element)

    const instance = this.props.study.instances[0]
    this.displayInstance(instance).then(() => {
      // init tools
      this.initTools()
      this.restoreToolState(this.props.tools, this.props.labels)

      // bind event listener
      element.addEventListener(
        "cornerstoneimagerendered",
        this.onImageRendered
      )
      element.addEventListener("cornerstonenewimage", this.onNewImage)
      window.addEventListener("resize", this.onWindowResize)
      // save automatically
      element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_ADDED, this.saveToolsData_)
      element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_MODIFIED, this.saveToolsData_)
      element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_REMOVED, this.saveToolsData_)
      this.tools.lesionsMarker.on(LesionsMarker.EVENTS.DATA_MODIFIED, this.saveLabelsData_)
      // update label data of each instance
      // this.tools.lesionsMarker.on(LesionsMarker.EVENTS.DATA_MODIFIED, this.updateLocalLabelData)
    })
  }

    /**
     * 参照生命周期函数
     */
  componentWillUnmount() {
    const element = this.element
    element.removeEventListener(
      "cornerstoneimagerendered",
      this.onImageRendered
    )

    element.removeEventListener("cornerstonenewimage", this.onNewImage)
    element.removeEventListener(cornerstoneTools.EVENTS.MEASUREMENT_ADDED, this.saveToolsData_)
    element.removeEventListener(cornerstoneTools.EVENTS.MEASUREMENT_MODIFIED, this.saveToolsData_)
    element.removeEventListener(cornerstoneTools.EVENTS.MEASUREMENT_REMOVED, this.saveToolsData_)
    window.removeEventListener("resize", this.onWindowResize)
    document.removeEventListener('keydown', this.onKeyDown, false)
    cornerstone.disable(element)
  }

    /**
     * 反转
     */
  toggleInvert() {
    let viewport = cornerstone.getViewport(this.element)
    viewport.invert = !this.state.invert
    cornerstone.setViewport(this.element, viewport)
    this.setState({
      invert: viewport.invert
    })
  }

    /**
     *
     * @returns {Promise<any>}
     */
  getScreenshot = () => {
    return new Promise((resolve, reject) => {
      const mimetype = 'image/png'
      const canvas = this.element.querySelector('canvas')
      const image = dataURItoBlob(canvas.toDataURL(mimetype, 1))
      resolve(image)
      /*canvas.toBlob(image => {
        resolve(image)
      }, mimetype, 1)*/
    })
  }

    /**
     * 清除工具
     * @returns {*}
     */
   clearTool(){
       const instance = this.props.instance;
       let obj={

       }
       if(this.props.tools["angle"]){
           const arr1=this.props.tools["angle"].map(function (item,index) {
              item.invalidated=false;
              return item;
           })
           obj['angle']=[];
       }
       if(this.props.tools["freehand"]){
           const arr2=this.props.tools["freehand"].map(function (item,index) {
               item.invalidated=false;
               return item;
           })
           obj['freehand']=[];
       }
       if(this.props.tools["length"]){
           const arr3=this.props.tools["length"].map(function (item,index) {
               item.invalidated=false;
               return item;
           })
           obj['length']=[];
       }

       return api.saveToolsData({
           studyUID: this.props.study.studyUID,
           seriesID: instance.seriesID,
           instanceID: instance.instanceID,
           data: obj
       })
       .then(resp=>{
          this.props.onToolsChange(resp.data)
           cornerstone.updateImage(this.element)
           this.restoreToolState(this.props.tools, this.props.labels)

       })



   }

    /**
     * 工具激活
     * @param name
     * @param mouseMask
     */
  activateTool(name, mouseMask=1) {
     this.tools[name].activate(this.element, mouseMask)
  }


  disableTool(name, mouseMask=1) {
      this.tools[name].disable()
  }

    /**
     * 工具显示或隐藏
     * @param name
     * @param mouseMask
     */
  enableTool(name, mouseMask=1) {
    if(name=="hideBoxes"){
        this.tools.lesionsMarker.enable()
        this.restoreToolState(this.props.tools, this.props.labels);
    }else{
        this.tools[name].enable(this.element, mouseMask)
    }
   }

    /**
     * 工具禁用
     * @param name
     * @param mouseMask
     */
  deactivateTool(name, mouseMask=1) {
    if (name === 'lesionsMarker') {
      this.setState({
        labelPopover: Object.assign({}, this.state.labelPopover, {show: false})
      })
    }
    this.tools[name].deactivate(this.element, mouseMask)

  }

    /**
     * 重置工具状态
     * @param tools
     * @param labels
     * @returns {Promise<void>}
     */
  restoreToolState = async (tools, labels) => {
    if (tools) {
      for (let name of TOOLS_NAME) {
        cornerstoneTools.clearToolState(this.element, name)
        if (!(name in tools)) continue
        for (let d of tools[name]) {
          cornerstoneTools.addToolState(this.element, name, d)
        }
        this.tools[name].enable(this.element)
      }
    }
    let lesionsData = labels.filter(label => !label.globalTag)
    this.tools.lesionsMarker.setState(lesionsData)
  }
    /**
     * 保存工具操作数据
     * @param e
     * @returns {*}
     */
  saveToolsData = (e) => {
    // FIXME: this function is called more frequently than expected
    let toolData = {}
    for (let name of TOOLS_NAME) {
      // if (name === 'angle') continue
      let state = cornerstoneTools.getToolState(this.element, name)
      if (!state) continue
      let data = Immutable.fromJS(state.data).toJS()
      data.forEach(d => {
        d.active = false
        d.selected = false
      })
      toolData[name] = data
    }
    const instance = this.props.instance
    return api.saveToolsData({
      studyUID: this.props.study.studyUID,
      seriesID: instance.seriesID,
      instanceID: instance.instanceID,
      data: toolData
    })
    .then(resp=>{
      this.props.onToolsChange(resp.data)
    })
  }

    /**
     * 清除lables
     */
    clearLabels(){
        this.tools.lesionsMarker.setState([])
        this.saveLabelsData()
    }
    /**
     * 保持label数据
     * @returns {*}
     */
  saveLabelsData = () => {
    const instance = this.props.instance
    let labels = this.tools.lesionsMarker.getState()
    labels = labels.map(label => Object.assign(label, {
      state: 'diagnosis',
      globalTag: false,
      instanceID: instance.instanceID,
      seriesID: instance.seriesID
    }))
    console.log('save label data', instance.instanceID, labels)
    // this.setState({
    //       labels:labels
    // })

    return api.saveLabels({
      // studyUID: this.props.study.studyUID,
      taskID: this.props.study.taskID,
      seriesID: instance.seriesID,
      instanceID: instance.instanceID,
      replace: true,
      labelType:"DX",
      labels
    })
    .then(resp => {
      this.props.onLabelsChange(resp)
      this.props.getDiseease();
      // this.tools.lesionsMarker.setState(resp.data)
    })
  }

    /**
     * DR视图组件
     * @returns {Component}
     */
  render() {
    let {age, gender, patientName, studyDate, studyTime,patientID} = this.props.study
        let that=this;
    return (
      <div id='viewer-container' style={this.props.style}>
        <div className='patient-info'>
          <p><FormattedMessage id='DRViewer.patient-info.Id'/>:{patientID}</p>
          <p><FormattedMessage id='DRViewer.patient-info.name'/>{patientName}</p>
          <p><FormattedMessage id='DRViewer.patient-info.gender'/>{gender}</p>
          <p><FormattedMessage id='DRViewer.patient-info.age'/>{age}</p>
          <p><FormattedMessage id='DRViewer.patient-info.checkDate'/>{studyDate}</p>
          <p><FormattedMessage id='DRViewer.patient-info.checkTime'/>{studyTime}</p>
        </div>
        <div ref={input => {this.element = input}} className='cornerstone-elm'>
          <canvas className="cornerstone-canvas"
            onContextMenu={e => e.preventDefault()}
            onClick={() => {
              if (this.state.labelPopover.isAdding) {
                return this.setState({
                  labelPopover: Object.assign({}, this.state.labelPopover, {isAdding: false})
                })
              }
              if (this.state.labelPopover.show) {
                this.setState({
                  labelPopover: Object.assign({}, this.state.labelPopover, {show: false})
                })
              }
            }}/>
        </div>
        <LabelPopover
          top={this.state.labelPopover.position.y}
          left={this.state.labelPopover.position.x}
          show={this.state.labelPopover.show}
          value={this.state.labelPopover.value}
          id={this.state.labelPopover.id}
          imageElm={this.state.labelPopover.imageElm}
          onChange={this.changeLabelName.bind(this)}
          onClose={this.closeLabelPopover}
          />
        {this.state.loading ? <Progress className='load-image-progress-bar' width={80} status='active' type="circle" percent={this.state.loadPercent} /> : ''}

        <div className='diagnosis'>
          {
            this.props.study.state==="completed" ?
               Number(this.props.disease_state)===1&&that.props.diseaseList.length!==0?
              <div className='abnormal'><FormattedMessage id='DRViewer.diagnosis.abnormal'/></div>
            :that.props.diseaseList.length===0?<div className='normal'><FormattedMessage id='DRViewer.diagnosis.normal'/></div>
                :
                Number(this.props.disease_state)===2&&that.props.diseaseList.length!==0?
              <div className='yellowWorn'><FormattedMessage id='DRViewer.diagnosis.abnormal'/></div>
              :
              that.props.diseaseList.length!==0&&Number(this.props.disease_state)===0?
                <div className='abnormal'><FormattedMessage id='DRViewer.diagnosis.abnormal'/></div>
              :""
            :""
          }
          <ul className="listDR">
            {
                this.props.diseaseList? that.props.diseaseList.map(function (item,index) {
                 return <li id={item} onMouseEnter={()=>{that.setState({ diseaseText:item}) }} onMouseLeave={()=>{that.setState({ diseaseText:""}) }}>
                      <FormattedMessage id={item}/>
                       {
                           that.state.diseaseText==item?<Icon type="close-circle" onClick={()=>{that.props.cancelDisease(item)}} />:""
                       }
                   </li>
              }):""
            }
          </ul>
        </div>

        <div className='bottom-right-text'>
          <p>Scale: {this.state.viewport.scale.toFixed(2)}</p>
          <p>WW: {Math.round(this.state.viewport.voi.windowWidth)}</p>
          <p>WL: {Math.round(this.state.viewport.voi.windowCenter)}</p>
        </div>

      </div>
    )
  }

}

export default injectIntl(Viewer,{withRef: true})
