#!/bin/python
# coding=utf-8

class Radiomics(object):
    def __init__(self):
        self.taskID = ""
        self.studyUID = ""
        self.seriesID = ""
        self.data_list = []

    def to_db(self):
        return {
            'taskID': self.taskID,
            'studyUID': self.studyUID,
            'seriesID': self.seriesID,
            'data_list': self.data_list
        }
