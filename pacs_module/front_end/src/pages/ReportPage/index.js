import React, { Component } from 'react'
import {Prompt} from 'react-router-dom'
import { Modal, Button, Row, Col, Input, Icon, TreeSelect, Layout, Menu, Alert, message } from 'antd'
import { List } from 'antd'
const { TextArea, Search } = Input
const { Header, Content, Footer } = Layout
const TreeNode = TreeSelect.TreeNode
import Template from './Template'
import LoadingWrapper from '../../component/LoadingWrapper'
import SignoutButton from '../../component/SignoutButton'
import Logo from '../../component/Logo'
import Report from '../../component/ReportLung'
import api from '../../api'
import _ from 'underscore'
import Immutable from 'immutable'
import {getUser} from '../../auth'
import moment from 'moment'
import io from 'socket.io-client'
import {CSVLink, CSVDownload} from 'react-csv'
import templateList from './data'

import LanguageDropdownList from '../../component/LanguageDropdownList'
import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './index.less'
import Return from "../../component/ReturnList";

class ReportPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
        visible:false,
      report: Immutable.fromJS(Object.assign({
        patientName: '',
        gender: '',
        age: '',
        patientID: '',
        modality: '',
        studyDate: '',
        imagingFind: '',
        diagnosisAdvice: '',
        updateDate: '',
        screenshotURLs: []
      }, this.props.data.report)), // immutable
      labels: this.props.data.labels,
      radiomics: this.props.data.radiomics,
      templateList: this.props.data.template,
      zReverse:this.props.data.study.reverse,
      maxZ:!!this.props.data.study.series[Object.keys(this.props.data.study.series)[0]].instances['z'] ?
            this.props.data.study.series[Object.keys(this.props.data.study.series)[0]].instances['z'].length
            :
            1,
      template: {},
      modalVisible: false,
      newTemplateName: '', // save new template as this name
      unsave: false      
    }
    this.allTemplate = this.props.data.template // store original template list
    this.currentUser = getUser()
    this.onClickModel=this.onClickModel.bind(this);
    this.handleOkModel=this.handleOkModel.bind(this);
    this.handleCancelModel=this.handleCancelModel.bind(this);
  }

  componentDidMount() {
    this.socket = io(window.location.origin, {transports: ['websocket']})
    this.socket.connect()
    this.socket.on('connect', () => {
      console.log('connected');
    })
    this.socket.on('reportUpdate', (payload) => {
      console.log('reportUpdated', payload);
      this.setState({
        report: this.state.report.merge(payload.data)
      })
    })
    this.socket.on('labelUpdate', (payload) => {
      console.log('labelUpdated', payload);
      this.setState({
        labels:payload.data
      })
    })
    this.socket.on('disconnect', () => {
      console.log('disconnect');
    });
    this.socket.emit('open', {
      userID: getUser().account,
      taskID: this.props.data.report.taskID
    })
  }

  componentWillUnmount() {
    this.socket.close()
  }

  removeImage = (index) => {
    if(!!this.state.report.toJS().labelist)
    {
      this.setState({
        report: this.state.report
        .update('labelist',labelist=>labelist.delete(index))
        .update('screenshotURLs', screenshotURLs => screenshotURLs.delete(index))
      })
    }else{
      this.setState({
        report: this.state.report
        .update('screenshotURLs', screenshotURLs => screenshotURLs.delete(index))
      })
    }
  }

  save() {
    // const data = Object.assign({operation: 'update'}, this.state.report)
    const data = this.state.report.toJSON()
    console.log('save report', data)
    return (
      api.updateReport(data)
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.saved"))
        this.setState({
          report: this.state.report.set('updateDate', moment().format('YYYYMMDD')),
          unsave: false
        })
      })
    )
  }

  radiomics(){
    console.log("downloaded radiomics")
    return true
  }

  submit = () => {
    const data = this.state.report.merge({action: 'submit'}).toJSON()
    return this.save()
      .then(() => api.updateReportStatus(data))
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.submitted"))
        this.setState({
          report: this.state.report.set('submitted', true)
        })
      })
  }

  unsubmit = () => {
    // this.save()
    const data = this.state.report.merge({action:'unsubmit'}).toJSON()
    return api.updateReportStatus(data)
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.unsubmitted"))
        this.setState({
          report: this.state.report.set('submitted', false)
        })
      })
  }

  examinate = () => {
    // this.save()
    const data = this.state.report.merge({action:'confirm'}).toJSON()
    return this.save()
      .then(() => api.updateReportStatus(data))
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.examinated"))
        this.setState({
          report: this.state.report.set('examined', true)
        })
      })
  }

  // template related
  handleTemplateChange = (key, value) => {
    let template = Object.assign({}, this.state.template, {[key]: value})
    this.setState({template})
  }

  saveTemplateChange = () => {
    api.updateTemplate(this.state.template)
    .then(() => message.success(GetLocaledText(this, "ReportPageLung.message.saved-successfully")))
  }

  applyTemplate = () => {
    if (this.isForbidEdit()) {
      return
    }
    // don't change report's modality
    let template = Object.assign({}, this.state.template)
    delete template.modality
    this.setState({report: this.state.report.merge(template)})
  }

  searchTemplate = (kw) => {
    this.setState({
      templateList: this.allTemplate.filter(t => t.templateName.includes(kw) || t.templateID.includes(kw))
    })
  }

  filterTemplate = (category) => {
    let cats = category.split('/')
    let templateList
    const isModalityMatched = (t) => {
      let matched = t.modality === cats[1]
      if (cats[1] === 'DR') {
        matched = matched || (t.modality === 'DX')
      }
      return matched
    }
    if (cats.length === 1) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0])
    } else if (cats.length === 2) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0] && isModalityMatched(t))
    } else if (cats.length === 3) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0] && isModalityMatched(t) && t.bodyPartExamined === cats[2])
    }
    this.setState({templateList})
  }

  openModal = () => {
    this.setState({
      modalVisible: true,
      newTemplateName: '',
    })
  }

  handleModalOk = () => {
    // validate input
    if (!this.state.newTemplateName) {
      // empty
      this.setState({modalErrorText: GetLocaledText(this, "ReportPageLung.mandatory-name")})
      return
    }
    let data = Object.assign({}, this.state.template, {
      "templateName": this.state.newTemplateName,
    })
    delete data.templateID
    api.addTemplate(data)
    .then(data => {
      data.usergroup = 'USER'
      this.setState({
        templateList: [data, ...this.state.templateList]
      })
      return api.getTemplate()
    })
    .then(data => {
      this.allTemplate = data
      this.setState({
        modalVisible: false
      })
    })
  }

  renderMenuItems() {
    let items = []
    const privilege = this.state.report.get('user_privilege').toJS()
    if (privilege.includes('reporter')) {
      if (!this.state.report.get('submitted')) {
        items.push(<Menu.Item key="save"><FormattedMessage id='ReportPageLung.save'/></Menu.Item>)
        items.push(<Menu.Item key="submit"><FormattedMessage id='ReportPageLung.submit'/></Menu.Item>)
        items.push(<Menu.Item key="print"><FormattedMessage id='ReportPageLung.print'/></Menu.Item>)
      } else if(!this.state.report.get('examined')) {
        items.push(<Menu.Item key="unsubmit"><FormattedMessage id='ReportPageLung.unsubmit'/></Menu.Item>)
      }
    } else if (privilege.includes('reviewer')
      && this.state.report.get('submitted')
      && !this.state.report.get('examined'))
    {
      items.push(<Menu.Item key="save"><FormattedMessage id='ReportPageLung.save'/></Menu.Item>)
      items.push(<Menu.Item key="examinate"><FormattedMessage id='ReportPageLung.examinate'/></Menu.Item>)
    }
    if(this.state.radiomics.length>0){
      items.push(<Menu.Item key="radiomics">
                   <CSVLink data={this.state.radiomics}
                            headers={[
                    GetLocaledText(this,"ReportPage.radiomics.nodule":"Nodule"),
                    "x_center",
                    "y_center",
                    "z_center",
                    "cm_Autocorrelat",
                    "cm_ClusterProminence",
                    "cm_ClusterShade",
                    "cm_ClusterTendency",
                    "cm_Contrast",
                    "cm_Correlat",
                    "cm_DifferenceAverage",
                    "cm_DifferenceEntropy",
                    "cm_DifferenceVariance",
                    "cm_Id",
                    "cm_Idm",
                    "cm_Imc1",
                    "cm_Imc2",
                    "cm_InverseVariance",
                    "cm_JointAverage",
                    "cm_JointEnergy",
                    "cm_JointEntropy",
                    "cm_MaximumProbability",
                    "cm_SumEntropy",
                    "cm_SumSquares",
                    "dm_DependenceEntropy",
                    "dm_DependenceNonUniformity",
                    "dm_DependenceNonUniformityNormalized",
                    "dm_DependenceVariance",
                    "dm_GrayLevelNonUniformity",
                    "dm_GrayLevelVariance",
                    "dm_HighGrayLevelEmphasis",
                    "dm_LargeDependenceEmphasis",
                    "dm_LargeDependenceHighGrayLevelEmphasis",
                    "dm_LargeDependenceLowGrayLevelEmphasis",
                    "dm_LowGrayLevelEmphasis",
                    "dm_SmallDependenceEmphasis",
                    "dm_SmallDependenceHighGrayLevelEmphasis",
                    "dm_SmallDependenceLowGrayLevelEmphasis",
                    "firstorder_10Percentile",
                    "firstorder_90Percentile",
                    "firstorder_Energy",
                    "firstorder_Entropy",
                    "firstorder_InterquartileRange",
                    "firstorder_Kurtosis",
                    "firstorder_Maximum",
                    "firstorder_Me",
                    "firstorder_MeanAbsoluteDeviat",
                    "firstorder_Med",
                    "firstorder_Minimum", 
                    "firstorder_Range",
                    "firstorder_RobustMeanAbsoluteDeviat",
                    "firstorder_RootMeanSquared",
                    "firstorder_Skewness",
                    "firstorder_TotalEnergy",
                    "firstorder_Uniformity",
                    "firstorder_Variance",
                    "m_GrayLevelNonUniformity",
                    "m_GrayLevelNonUniformityNormalized",
                    "m_GrayLevelVariance",
                    "m_HighGrayLevelRunEmphasis",
                    "m_LongRunEmphasis",
                    "m_LongRunHighGrayLevelEmphasis",
                    "m_LongRunLowGrayLevelEmphasis",
                    "m_LowGrayLevelRunEmphasis",
                    "m_RunEntropy",
                    "m_RunLengthNonUniformity",
                    "m_RunLengthNonUniformityNormalized",
                    "m_RunPercentage",
                    "m_RunVariance",
                    "m_ShortRunEmphasis",
                    "m_ShortRunHighGrayLevelEmphasis",
                    "m_ShortRunLowGrayLevelEmphasis",
                    "shape_Elongat",
                    "shape_Flatness",
                    "shape_LeastAxisLength",
                    "shape_MajorAxisLength",
                    "shape_Maximum2DDiameterColum",
                    "shape_Maximum2DDiameterRow",
                    "shape_Maximum2DDiameterSlice",
                    "shape_Maximum3DDiamete",
                    "shape_MeshVolume",
                    "shape_MinorAxisLength",
                    "shape_Sphericity",
                    "shape_SurfaceAre",
                    "shape_SurfaceVolumeRat",
                    "shape_VoxelVolume",
                    "szm_GrayLevelNonUniformity",
                    "szm_GrayLevelNonUniformityNormalized",
                    "szm_GrayLevelVariance",
                    "szm_HighGrayLevelZoneEmphasis",
                    "szm_LargeAreaEmphasis",
                    "szm_LargeAreaHighGrayLevelEmphasis",
                    "szm_LargeAreaLowGrayLevelEmphasis",
                    "szm_LowGrayLevelZoneEmphasis",
                    "szm_SizeZoneNonUniformity",
                    "szm_SizeZoneNonUniformityNormalized",
                    "szm_SmallAreaEmphasis",
                    "szm_SmallAreaHighGrayLevelEmphasis",
                    "szm_SmallAreaLowGrayLevelEmphasis",
                    "szm_ZoneEntropy",
                    "szm_ZonePercentage",
                    "szm_ZoneVariance"
			    ]
                   }
                            filename={"radiomics.csv"} 
                            className="download-radiomics-button"
                            >
                      <FormattedMessage id='ReportPageLung.radiomics'/>
                   </CSVLink>
                 </Menu.Item>)
    }
    return items
  }

  renderStamp() {
    if (this.state.report.get('submitted') && !this.state.report.get('examined') && getUser().usergroup === 'reporter') {
      return <img className='report-status-stamp' src={require('../../../static/images/submit-stamp.png')}/>
    } else if (this.state.report.get('examined')) {
      return <img className='report-status-stamp' src={require('../../../static/images/exam-stamp.png')}/>
    }
    return ''
  }

  isForbidEdit() {
    const hasExamined = this.state.report.get('examined')
    const hasSubmitted = this.state.report.get('submitted')
    const isReporter = this.props.data.report.user_privilege.includes('reporter')
    const isExmainer = this.props.data.report.user_privilege.includes('reviewer')
    return hasExamined || (isReporter && hasSubmitted) || (isExmainer && !hasSubmitted)
  }

  showRawImage(url) {

  }
  onPrint(){
    this.save();
    window.open("/report_print/"+this.getUrlParams())
  }
  getUrlParams(){
      let htmlHref = window.location.href;
      htmlHref = htmlHref.replace(/^http:\/\/[^/]+/, "");
      let addr = htmlHref.substr(htmlHref.lastIndexOf('/', htmlHref.lastIndexOf('/') - 1) + 1);
      let index = addr.lastIndexOf("\/");
      //js 获取字符串中最后一个斜杠后面的内容
      let str = decodeURI(addr.substring(index + 1, addr.length));
      // js 获取字符串中最后一个斜杠前面的内容
      // let str = decodeURI(addr.substring(0, index));
      return str;
  }
    //帮助文档
    onClickModel(){
        this.setState({
            visible:true,
        })
    }
    handleOkModel(){
        this.setState({
            visible:false,
        })
    }
    handleCancelModel(){
        this.setState({
            visible:false,
        })
    }
    render() {
    const isForbidEdit = this.isForbidEdit()
    return (
      <div className='page' id='report-page'>
        <Prompt
          when={this.state.unsave}
          message={location => GetLocaledText(this, "ReportPageLung.message.unsaved-before-leave")}
        />
        <Header>
          <Logo/>
          <Return  />
          <Menu
            theme="dark"
            mode="horizontal"
            style={{ lineHeight: '64px', borderBottom: 'none', display: 'inline-block' }}
            selectable={false}
            onClick={({item, key, keyPath}) => {
              if (key === 'submit') {this.submit()}
              else if (key === 'unsubmit') {this.unsubmit()}
              else if (key === 'save') {this.save()}
              else if (key === 'radiomics'){this.radiomics()}
              else if (key === 'examinate') {this.examinate()}
              else if (key === 'print') {this.onPrint()}
            }}
          >
            {this.renderMenuItems()}
          </Menu>
          <SignoutButton/>
            <span style={{color:"#fff",right:"180px",cursor:"pointer",position:"absolute",marginTop:"2px"}} onClick={this.onClickModel}><img src={require('../../../static/images/help_nor.png')}/></span>
          {/*<LanguageDropdownList setAppState={state => {this.props.setAppState(state)}}/>*/}
        </Header>
        <div className='content'>
          <div className='template-aside'>
            <Template
              list={this.state.templateList}
              template={this.state.template}
              onSearch={this.searchTemplate}
              onCategoryChange={this.filterTemplate}
              onSelected={template => this.setState({template})}
              onApply={this.applyTemplate}
              onTemplateChange={this.handleTemplateChange}
              onSave={this.saveTemplateChange}
              onSaveAs={this.openModal}
              showSaveBtn={this.state.template.usergroup === 'USER'}
              />
          </div>
          <div  style={{  flexGrow: 1,
              height: "100%",
              overflow:"auto"}}>
          <Report
            isForbidEdit={isForbidEdit}
            report={this.state.report.toJS()}
            labels={this.state.labels}
            removeImage={this.removeImage}
            zReverse={this.state.zReverse}
            maxZ = {this.state.maxZ}
            onChange={(key, value) => {
              this.setState({
                report: this.state.report.set(key, value)
              })
            }}
            />
          </div>
        </div>
        <Modal
          title={GetLocaledText(this, "ReportPageLung.title.save-template")}
          visible={this.state.modalVisible}
          onOk={this.handleModalOk}
          onCancel={() => this.setState({modalVisible: false, modalErrorText: ''})}>
          <Input onChange={(e) => this.setState({newTemplateName: e.target.value})}
            placeholder={GetLocaledText(this, "ReportPageLung.input.template.name")}
            value={this.state.newTemplateName}
            />
          {
            this.state.modalErrorText && !this.state.newTemplateName.length ?
            <Alert message={this.state.modalErrorText} type="error" style={{marginTop: 5}}/> : ''
          }
        </Modal>
          <Modal
              title={GetLocaledText(this, "HelpDocument")}
              visible={this.state.visible}
              onOk={this.handleOkModel}
              onCancel={this.handleCancelModel}
              width={840}
              bodyStyle={{background:"#000",color:"#fff"}}
              footer={null}
          >
              <img src={require('../../../static/images/helpDocument.png')} style={{width:"794px"}} />
          </Modal>
      </div>
    )
  }

}

async function getData(props) {
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let study= await api.getStudyDetail({studyUID})
  let report = await api.getReport({taskID:study.taskID})
  let labels = await api.getLabels({
    taskID:study.taskID,
    seriesID:Object.values(study.series)[0].seriesID
  });
  let radiomicsJS = await api.getRadiomics({
    taskID:study.taskID,
    seriesID:Object.values(study.series)[0].seriesID
  });
  
  let radiomics = []
  if(radiomicsJS.length!==0)
  {
    radiomicsJS[0].data_list.forEach(function(r,i){
      radiomics.push([("#"+(i+1)),
                    r.x_center ,
                    r.y_center ,
                    r.z_center ,
                    r.cm_Autocorrelat ,
                    r.cm_ClusterProminence ,
                    r.cm_ClusterShade ,
                    r.cm_ClusterTendency ,
                    r.cm_Contrast ,
                    r.cm_Correlat ,
                    r.cm_DifferenceAverage ,
                    r.cm_DifferenceEntropy ,
                    r.cm_DifferenceVariance ,
                    r.cm_Id ,
                    r.cm_Idm ,
                    r.cm_Imc1 ,
                    r.cm_Imc2 ,
                    r.cm_InverseVariance ,
                    r.cm_JointAverage ,
                    r.cm_JointEnergy ,
                    r.cm_JointEntropy ,
                    r.cm_MaximumProbability ,
                    r.cm_SumEntropy ,
                    r.cm_SumSquares ,
                    r.dm_DependenceEntropy ,
                    r.dm_DependenceNonUniformity ,
                    r.dm_DependenceNonUniformityNormalized ,
                    r.dm_DependenceVariance ,
                    r.dm_GrayLevelNonUniformity ,
                    r.dm_GrayLevelVariance ,
                    r.dm_HighGrayLevelEmphasis ,
                    r.dm_LargeDependenceEmphasis ,
                    r.dm_LargeDependenceHighGrayLevelEmphasis ,
                    r.dm_LargeDependenceLowGrayLevelEmphasis ,
                    r.dm_LowGrayLevelEmphasis ,
                    r.dm_SmallDependenceEmphasis ,
                    r.dm_SmallDependenceHighGrayLevelEmphasis ,
                    r.dm_SmallDependenceLowGrayLevelEmphasis ,
                    r.firstorder_10Percentile ,
                    r.firstorder_90Percentile , 
                    r.firstorder_Energy ,
                    r.firstorder_Entropy ,
                    r.firstorder_InterquartileRange ,
                    r.firstorder_Kurtosis ,
                    r.firstorder_Maximum , 
                    r.firstorder_Me ,
                    r.firstorder_MeanAbsoluteDeviat ,
                    r.firstorder_Med ,
                    r.firstorder_Minimum ,
                    r.firstorder_Range ,
                    r.firstorder_RobustMeanAbsoluteDeviat ,
                    r.firstorder_RootMeanSquared ,
                    r.firstorder_Skewness ,
                    r.firstorder_TotalEnergy ,
                    r.firstorder_Uniformity ,
                    r.firstorder_Variance ,
                    r.m_GrayLevelNonUniformity ,
                    r.m_GrayLevelNonUniformityNormalized ,
                    r.m_GrayLevelVariance ,
                    r.m_HighGrayLevelRunEmphasis ,
                    r.m_LongRunEmphasis ,
                    r.m_LongRunHighGrayLevelEmphasis ,
                    r.m_LongRunLowGrayLevelEmphasis ,
                    r.m_LowGrayLevelRunEmphasis ,
                    r.m_RunEntropy ,
                    r.m_RunLengthNonUniformity ,
                    r.m_RunLengthNonUniformityNormalized ,
                    r.m_RunPercentage ,
                    r.m_RunVariance ,
                    r.m_ShortRunEmphasis ,
                    r.m_ShortRunHighGrayLevelEmphasis ,
                    r.m_ShortRunLowGrayLevelEmphasis ,
                    r.shape_Elongat ,
                    r.shape_Flatness ,
                    r.shape_LeastAxisLength ,
                    r.shape_MajorAxisLength ,
                    r.shape_Maximum2DDiameterColum ,
                    r.shape_Maximum2DDiameterRow ,
                    r.shape_Maximum2DDiameterSlice ,
                    r.shape_Maximum3DDiamete ,
                    r.shape_MeshVolume ,
                    r.shape_MinorAxisLength ,
                    r.shape_Sphericity ,
                    r.shape_SurfaceAre ,
                    r.shape_SurfaceVolumeRat ,
                    r.shape_VoxelVolume ,
                    r.szm_GrayLevelNonUniformity ,
                    r.szm_GrayLevelNonUniformityNormalized ,
                    r.szm_GrayLevelVariance ,
                    r.szm_HighGrayLevelZoneEmphasis ,
                    r.szm_LargeAreaEmphasis ,
                    r.szm_LargeAreaHighGrayLevelEmphasis ,
                    r.szm_LargeAreaLowGrayLevelEmphasis ,
                    r.szm_LowGrayLevelZoneEmphasis ,
                    r.szm_SizeZoneNonUniformity ,
                    r.szm_SizeZoneNonUniformityNormalized ,
                    r.szm_SmallAreaEmphasis ,
                    r.szm_SmallAreaHighGrayLevelEmphasis ,
                    r.szm_SmallAreaLowGrayLevelEmphasis ,
                    r.szm_ZoneEntropy ,
                    r.szm_ZonePercentage ,
                    r.szm_ZoneVariance 
      ])
    })
  }
  
  let template = await api.getTemplate()
  let data = {report, template, labels,radiomics,study}
  return await data
}

ReportPage = LoadingWrapper(ReportPage, getData)

export default injectIntl(ReportPage)
