# ALGO_END
## Preparation
* Copy the checkpoint files for ai-detect here. Like a folder named **ckpt/** with two files named **ckpt/model.ckpt-100.meta** and **ckpt/model.ckpt-100**
* Modify **LIVER_AI_HOST** for ALGO_END in [config](../back_end/server/config.py)
* Modify **CUDA_VISIBLE_DEVICES** for GPU to use in [liver_ai_model](liver_ai_model.py)

## Run algo_end
```Python
python app_liver_ai.py
```