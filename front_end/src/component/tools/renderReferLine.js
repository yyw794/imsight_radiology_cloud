import * as cornerstone from 'cornerstone-core'
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import noduleBox from './noduleBox'

const {projectPatientPointToImagePlane, toolColors, toolStyle} = cornerstoneTools

function convertToVector3 (arrayOrVector3) {
  if (arrayOrVector3 instanceof cornerstoneMath.Vector3) {
    return arrayOrVector3;
  }

  return new cornerstoneMath.Vector3(arrayOrVector3[0], arrayOrVector3[1], arrayOrVector3[2]);
}

// Projects an image point to a patient point
function imagePointToPatientPoint (imagePoint, imagePlane) {
  const rowCosines = convertToVector3(imagePlane.rowCosines);
  const columnCosines = convertToVector3(imagePlane.columnCosines);
  const imagePositionPatient = convertToVector3(imagePlane.imagePositionPatient);

  const x = rowCosines.clone().multiplyScalar(imagePoint.x);

  x.multiplyScalar(imagePlane.columnPixelSpacing || 1);
  const y = columnCosines.clone().multiplyScalar(imagePoint.y);

  y.multiplyScalar(imagePlane.rowPixelSpacing || 1);
  const patientPoint = x.add(y);

  patientPoint.add(imagePositionPatient);

  return patientPoint;
}

function getRectangleFromImagePlane (imagePlane) {
  // Get the points
  const topLeft = imagePointToPatientPoint({
    x: 0,
    y: 0
  }, imagePlane);
  const topRight = imagePointToPatientPoint({
    x: imagePlane.columns,
    y: 0
  }, imagePlane);
  const bottomLeft = imagePointToPatientPoint({
    x: 0,
    y: imagePlane.rows
  }, imagePlane);
  const bottomRight = imagePointToPatientPoint({
    x: imagePlane.columns,
    y: imagePlane.rows
  }, imagePlane);

    // Get each side as a vector
  const rect = {
    top: new cornerstoneMath.Line3(topLeft, topRight),
    left: new cornerstoneMath.Line3(topLeft, bottomLeft),
    right: new cornerstoneMath.Line3(topRight, bottomRight),
    bottom: new cornerstoneMath.Line3(bottomLeft, bottomRight)
  };


  return rect;
}

function lineRectangleIntersection (line, rect) {
  const intersections = [];

  Object.keys(rect).forEach(function (side) {
    const segment = rect[side];
    const intersection = line.intersectLine(segment);

    if (intersection) {
      intersections.push(intersection);
    }
  });

  return intersections;
}


// Gets the line of intersection between two planes in patient space
function planePlaneIntersection (targetImagePlane, referenceImagePlane) {
  const targetRowCosines = convertToVector3(targetImagePlane.rowCosines);
  const targetColumnCosines = convertToVector3(targetImagePlane.columnCosines);
  const targetImagePositionPatient = convertToVector3(targetImagePlane.imagePositionPatient);
  const referenceRowCosines = convertToVector3(referenceImagePlane.rowCosines);
  const referenceColumnCosines = convertToVector3(referenceImagePlane.columnCosines);
  const referenceImagePositionPatient = convertToVector3(referenceImagePlane.imagePositionPatient);

  // First, get the normals of each image plane
  const targetNormal = targetRowCosines.clone().cross(targetColumnCosines);
  const targetPlane = new cornerstoneMath.Plane();

  targetPlane.setFromNormalAndCoplanarPoint(targetNormal, targetImagePositionPatient);

  const referenceNormal = referenceRowCosines.clone().cross(referenceColumnCosines);
  const referencePlane = new cornerstoneMath.Plane();

  referencePlane.setFromNormalAndCoplanarPoint(referenceNormal, referenceImagePositionPatient);

  const originDirection = referencePlane.clone().intersectPlane(targetPlane);
  const origin = originDirection.origin;
  const direction = originDirection.direction;

  // Calculate the longest possible length in the reference image plane (the length of the diagonal)
  const bottomRight = imagePointToPatientPoint({
    x: referenceImagePlane.columns,
    y: referenceImagePlane.rows
  }, referenceImagePlane);
  const distance = referenceImagePositionPatient.distanceTo(bottomRight);

  // Use this distance to bound the ray intersecting the two planes
  const line = new cornerstoneMath.Line3();

  line.start = origin;
  line.end = origin.clone().add(direction.multiplyScalar(distance*4));

  // Find the intersections between this line and the reference image plane's four sides
  const rect = getRectangleFromImagePlane(referenceImagePlane);
  const intersections = lineRectangleIntersection(line, rect);

  // Return the intersections between this line and the reference image plane's sides
  // In order to draw the reference line from the target image.
  if (intersections.length !== 2) {
    return;
  }

  return {
    start: intersections[0],
    end: intersections[1]
  };
}


function calculateReferenceLine(targetImagePlane, referenceImagePlane) {
  const points = planePlaneIntersection(targetImagePlane, referenceImagePlane);

  if (!points) {
    return;
  }

  return {
    start: projectPatientPointToImagePlane(points.start, targetImagePlane),
    end: projectPatientPointToImagePlane(points.end, targetImagePlane)
  };
}

// custom renference line renderer
const renderReferLine = (context, eventData, targetElement, referenceElement) => {
    const targetImage = cornerstone.getEnabledElement(targetElement).image;
    const referenceImage = cornerstone.getEnabledElement(referenceElement).image;

    // Make sure the images are actually loaded for the target and reference
    if (!targetImage || !referenceImage) {
      return;
    }
    const targetImagePlane = cornerstone.metaData.get('imagePlaneModule',targetImage.imageId);
    const referenceImagePlane = cornerstone.metaData.get('imagePlaneModule', referenceImage.imageId);
    // Make sure the target and reference actually have image plane metadata
    if (!targetImagePlane ||
          !referenceImagePlane ||
          !targetImagePlane.rowCosines ||
          !targetImagePlane.columnCosines ||
          !targetImagePlane.imagePositionPatient ||
          !referenceImagePlane.rowCosines ||
          !referenceImagePlane.columnCosines ||
          !referenceImagePlane.imagePositionPatient) {
      return;
    }

    // The image planes must be in the same frame of reference
//    if (targetImagePlane.frameOfReferenceUID !== referenceImagePlane.frameOfReferenceUID) {
//      return;
//    }

    targetImagePlane.rowCosines = convertToVector3(targetImagePlane.rowCosines);
    targetImagePlane.columnCosines = convertToVector3(targetImagePlane.columnCosines);
    targetImagePlane.imagePositionPatient = convertToVector3(targetImagePlane.imagePositionPatient);
    referenceImagePlane.rowCosines = convertToVector3(referenceImagePlane.rowCosines);
    referenceImagePlane.columnCosines = convertToVector3(referenceImagePlane.columnCosines);
    referenceImagePlane.imagePositionPatient = convertToVector3(referenceImagePlane.imagePositionPatient);

    // The image plane normals must be > 30 degrees apart
    const targetNormal = targetImagePlane.rowCosines.clone().cross(targetImagePlane.columnCosines);
    const referenceNormal = referenceImagePlane.rowCosines.clone().cross(referenceImagePlane.columnCosines);
    let angleInRadians = targetNormal.angleTo(referenceNormal);

    angleInRadians = Math.abs(angleInRadians);

    if (angleInRadians < 0.5) { // 0.5 radians = ~30 degrees
      return;
    }

    const referenceLine = calculateReferenceLine(targetImagePlane, referenceImagePlane);

    if (!referenceLine) {
      return;
    }

    // don't draw until get two cross referenceLines
    const toolData = cornerstoneTools.getToolState(targetElement, 'referenceLines');
    let intersection
    let line1
    let line2
    if (!toolData.anotherReferLine) {
      toolData.anotherReferLine = referenceLine
    } else {
      line1 = Object.assign({}, referenceLine)
      line2 = Object.assign({}, toolData.anotherReferLine)
      intersection = cornerstoneMath.lineSegment.intersectLine(line1, line2)
      toolData.anotherReferLine = null
    }

    // const color = toolColors.getActiveColor();
    // const lineWidth = toolStyle.getToolWidth();
    const color = 'yellow'
    const lineWidth = 1

    if (intersection && !isNaN(intersection.x) && !isNaN(intersection.y)) {
      // console.log(eventData.element, 'referline');
      // context.rect(intersection.x - 10, intersection.y - 10, 20, 20);
      const intersectionCanvas = cornerstone.pixelToCanvas(eventData.element, intersection);
      if (line1.start.y !== line1.end.y) {
        let tmp = line1
        line1 = line2
        line2 = tmp
      }

      const start1Canvas = cornerstone.pixelToCanvas(eventData.element, line1.start);
      const end1Canvas = cornerstone.pixelToCanvas(eventData.element, line1.end);
      const start2Canvas = cornerstone.pixelToCanvas(eventData.element, line2.start);
      const end2Canvas = cornerstone.pixelToCanvas(eventData.element, line2.end);
      const viewport = cornerstone.getViewport(eventData.element)
      const size = noduleBox.BOX_SIZE * viewport.scale
      // Draw the referenceLines
      context.setTransform(1, 0, 0, 1, 0, 0);

      context.save();
      context.beginPath();
      context.strokeStyle = color;
      context.lineWidth = lineWidth;
      context.setLineDash([5, 2])

      context.moveTo(start1Canvas.x, start1Canvas.y);
      context.lineTo(Math.max(intersectionCanvas.x - size / 2, start1Canvas.x), start1Canvas.y);
      context.moveTo(intersectionCanvas.x + size / 2, start1Canvas.y);
      context.lineTo(end1Canvas.x, end1Canvas.y);

      context.moveTo(start2Canvas.x, start2Canvas.y);
      context.lineTo(start2Canvas.x, Math.max(intersectionCanvas.y - size / 2, start2Canvas.y));
      context.moveTo(start2Canvas.x, intersectionCanvas.y + size / 2);
      context.lineTo(end2Canvas.x, end2Canvas.y);
      context.stroke();

      // context.rect(intersectionCanvas.x - size / 2, intersectionCanvas.y - size / 2, size, size)
      context.beginPath()
      context.fillStyle = color
      context.arc(intersectionCanvas.x, intersectionCanvas.y, 1, 0, 2 * Math.PI, true);
      context.fill();
      context.stroke();
      context.restore();
    }
}

export default renderReferLine
