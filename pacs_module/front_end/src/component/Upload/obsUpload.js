import browserMD5File from 'browser-md5-file'
import _ from 'underscore'
export const ACCESS_KEY_ID = 'GEAFUMMISIBRYLW0EUES'
export const SECRET_ACCESS_KEY = 'SH6ImPlf3Ib3TkMUPF3g0Y6Bq4jEt22xRBZAu3c7'

let obsClient = new ObsClient({
   access_key_id: ACCESS_KEY_ID,
   secret_access_key: SECRET_ACCESS_KEY,
   server : 'obs.cn-north-1.myhwclouds.com'
});

export function uploadFile(file) {
  return new Promise(function(resolve, reject) {
    browserMD5File(file, (err, md5) => {
      if (err) {
        console.log(err);
        reject(err)
        return
      }
      let params = {
         Bucket : 'imsight-cloud',
         Key : `${md5}.${_.last(file.name.split('.'))}`,
         SourceFile : file
      }
      obsClient.putObject(params, function (err, result) {
         if (err) {
           console.error('Error-->' + err);
           reject(params)
         } else {
           console.log('Status-->' + result.CommonMsg.Status);
           resolve(params)
         }
      });
    })
  });
}

export async function uploadFileList(fileList, onSuccess, onFail) {
  for (let file of fileList) {
    try {
      let res = await uploadFile(file)
      onSuccess(res)
    } catch (err) {
      onFail(err)
    }
  }
}
