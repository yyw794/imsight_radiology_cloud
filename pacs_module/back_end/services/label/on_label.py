#!/bin/python
#coding=utf-8
from flask import make_response, request, jsonify, abort
from flask_login import current_user
from socketIO_client import SocketIO, LoggingNamespace
import datetime, sys, os

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_update_one
from models.response.response import response
from models.report.Report import parseReportInfo
from models.label.Label_IO import updateLabel, parseLabelInfo, saveLabelist
from services.algo.nodule_service import run_lung_nodule_analyzer
import logging
logger = logging.getLogger("back_end.service.on_label")
from services.algo.on_radiomics import create_radiomics
from services.basic.basic_api import socket_emit

required_key_all = ["taskID"]
required_key_put = ['labels', 'seriesID']
optional_key_instance_label_type = ["seriesID", "instanceID"]
optional_key_series_label_type = ["seriesID"]
instance_label_type = ['General', 'DX']
series_label_type = ['Nodule', 'LiverTumor']

def on_label():
    if not current_user.is_authenticated:
        abort(401)

    res = response()
    userID = current_user.get_id()
    params = request.args.to_dict() if request.method == 'GET' else request.json
    if params is None:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    if not all(key in params for key in required_key_all):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
    taskID = params['taskID']

    task_info = db_find_one("task", {"taskID": taskID})
    if task_info is None:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)
    studyUID = task_info['studyUID']
    if userID != task_info['reporter'] and userID != task_info['reviewer']:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)
    isReporter = True if userID == task_info['reporter'] else False
    isReviewer = True if userID == task_info['reviewer'] else False
    if 'labelType' in params:
        label_info = db_find_one("label", {"taskID": taskID, 'labelType': params['labelType']})
    else:
        label_info = db_find_one("label", {"taskID": taskID})
    if label_info is None:
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)

    labelType = label_info['labelType']
    rest_labelist = []
    if labelType in instance_label_type:
        if all(key in params for key in optional_key_instance_label_type):
            seriesID = params['seriesID']
            instanceID = params['instanceID']
            all_labelist = parseLabelInfo(label_info)
            labelist = filter(lambda l: l.seriesID == seriesID and l.instanceID == instanceID, all_labelist)
            rest_labelist = [l for l in all_labelist if l not in labelist]
        else:
            labelist = parseLabelInfo(label_info)
    elif labelType in series_label_type:
        if all(key in params for key in optional_key_series_label_type):
            seriesID = params['seriesID']
            all_labelist = parseLabelInfo(label_info)
            labelist = filter(lambda l: l.seriesID == seriesID, all_labelist)
            rest_labelist = [l for l in all_labelist if l not in labelist]
        else:
            labelist = parseLabelInfo(label_info)

    else:
        labelist = []
        res.data = labelist
        res.statusCode = 1000
        return make_response(jsonify(res.to_dict()), 200)


    if request.method == 'GET':
        res.statusCode = 1000
        hellolist = [label.hello() for label in labelist]
        if True in hellolist:
            rest_labelist.extend(labelist)
            db_update_one("label", {"taskID": taskID}, saveLabelist(rest_labelist, studyUID, taskID, labelType))

        res.data = [label.to_dict() for label in labelist]
        return make_response(jsonify(res.to_dict()), 200)

    if not all(key in params for key in required_key_put):
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)


    if request.method == 'PUT':
        params['labelType'] = labelType
        new_labelist = []
        for label in params['labels']:
            label['labelType'] = labelType
            isCreate = False if 'labelID' in label else True
            new_labelist.append(updateLabel(label, 'diagnosis', isCreate= isCreate))

        if len(new_labelist) < len(labelist):
            pass
        elif len(new_labelist) == len(labelist):
            if labelType == 'Nodule' or labelType == 'LiverTumor':
                report_info = db_find_one("report",{"taskID": taskID})
                report = parseReportInfo(report_info)
                report.screenshotURLs = [label.url for label in new_labelist if label.url != '']
                db_update_one("report", {"taskID": taskID}, report.to_db())
                ret = report.to_dict()
                ret['labelist'] = [label.to_dict() for label in new_labelist if label.url != '']
                socket_emit('updateReport', {'data': ret, 'userID': userID, 'taskID': taskID})
        else:
            if labelType == 'Nodule' and task_info['type'] == "normal":
                 new_labels = [new_label for new_label in new_labelist if new_label.addByUser]
                 for new_label in new_labels:
                    nodule_info = new_label.to_dict()
                    nodule_info['taskID'] = taskID
                    analyzed_label = run_lung_nodule_analyzer(nodule_info)
                    new_labelist.remove(new_label)
                    analyzed_label.seriesID = params['seriesID']
                    analyzed_label.addByUser = True
                    new_labelist.append(analyzed_label)


        rest_labelist.extend(new_labelist)
        db_update_one("label", {"taskID": taskID}, saveLabelist(rest_labelist, studyUID, taskID, labelType))

        create_radiomics(saveLabelist(new_labelist, studyUID, taskID, labelType)['labels'],studyUID,labelType,taskID)
        res.data = [label.to_dict() for label in new_labelist]
        res.statusCode = 1000
        return make_response(jsonify(res.to_dict()), 200)


    # if request.method == 'POST':
    #     # TODO: if labelID exist, return immediately
    #     if labelType == 'Nodule' and task_info['type'] == "normal":
    #         label = run_lung_nodule_analyzer(params)
    #     else:
    #         state = "review" if isReviewer else "diagnosis"
    #         params['labelType'] = labelType
    #         label = updateLabel(params, state)
    #         if label is None:
    #             res.statusCode = 1101
    #             return make_response(jsonify(res.to_dict()), 200)

    #     labelist.append(label)
    #     db_update_one("label", {"taskID": taskID}, saveLabelist(labelist, studyUID, taskID, labelType))

    #     res.data = label.to_dict()
    #     res.statusCode = 1000
    #     return make_response(jsonify(res.to_dict()), 200)

    # # replace all labels
    # if request.method == 'PUT' and params.get('replace'):
    #     params['labelType'] = labelType
    #     labelist = []
    #     for label in params['labels']:
    #         label['labelType'] = labelType
    #         labelist.append(updateLabel(label, 'diagnosis'))
    #     db_update_one("label", {"taskID": taskID}, saveLabelist(other_labelist + labelist, studyUID, taskID, labelType))
    #     res.data = {}
    #     res.statusCode = 1000
    #     return make_response(jsonify(res.to_dict()), 200)

    # if not all(key in params for key in required_key_put_delete):
    #     res.statusCode = 1101
    #     return make_response(jsonify(res.to_dict()), 200)
    # labelID = params['labelID']
    # target_labelist = filter(lambda l: l.labelID == labelID, labelist)
    # if len(target_labelist) == 0:
    #     res.statusCode = 1200
    #     return make_response(jsonify(res.to_dict()), 200)
    # label = target_labelist[0]


    # if request.method == 'DELETE':
    #     if isReporter and isReviewer and label.state != 'review':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    #     elif isReviewer and not isReporter and label.state != 'review':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    #     elif isReporter and not isReviewer and label.state != 'diagnosis':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    #     labelist.remove(label)
    #     db_update_one("label", {"taskID": taskID}, saveLabelist(labelist, studyUID, taskID, labelType))

    #     res.data = label.to_dict()
    #     res.statusCode = 1000
    #     return make_response(jsonify(res.to_dict()), 200)


    # action = "" if not params.has_key('action') else params['action']
    # if action:
    #     if isReporter and action == 'submit' and label.state != 'diagnosis':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    #     elif isReporter and action == 'unsubmit' and label.state != 'review':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    #     elif isReviewer and action == 'confirm' and label.state != 'review':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    #     elif isReviewer and action == 'unconfirm' and label.state != 'pass':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    #     elif isReviewer and action == 'reject' and label.state != 'review':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    # else:
    #     if isReporter and label.state != 'diagnosis':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)
    #     elif isReviewer and label.state != 'review':
    #         res.statusCode = 1102
    #         return make_response(jsonify(res.to_dict()), 200)


    # if request.method == 'PUT':
    #     params['labelType'] = labelType
    #     labelist.remove(label)
    #     label = updateLabel(params, isCreate=False)
    #     labelist.append(label)

    #     if action == 'submit':
    #         label.submit()

    #     if action == 'unsubmit':
    #         label.unsubmit()

    #     if action == 'confirm':
    #         label.confirm()

    #     if action == 'unconfirm':
    #         label.unconfirm()

    #     if action == 'reject':
    #         label.reject()

    #     db_update_one("label", {"taskID": taskID}, saveLabelist(labelist, studyUID, taskID, labelType))

    #     res.data = label.to_dict()
    #     res.statusCode = 1000
    #     return make_response(jsonify(res.to_dict()), 200)
