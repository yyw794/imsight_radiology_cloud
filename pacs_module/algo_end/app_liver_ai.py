from flask import Flask, jsonify, Blueprint, session, abort, Response, url_for, make_response
from flask import request, redirect  # handle request
# from obs import downloadFile, uploadFile
import json

import os
import time
import numpy as np
import SimpleITK as sitk
import liver_ai_model
import shutil
import dataOpts
from fileGet import getfile, getFileStr, saveAsComprass, encodeb64

app = Flask(__name__)


RAWDATA_FOLDER = './data/raw/'
# HOST_LIVER_PATH = '../../algo_end/data/raw'

@app.route('/api/liver_ai_prediction', methods=['POST'])
def liver_prediction():
    if not request.json:
        return make_response(jsonify({"data": {}}), 200)

    data = request.json
    
    studyUID = str(data['studyUID'])
    userID = str(data['userID'])
    fileMethod = data['fileMethod']

    res_path = os.path.join(RAWDATA_FOLDER, userID, studyUID)

    # TODO:The algo_end hold no data.
    # if os.path.exists(res_path):
    #     shutil.rmtree(res_path)
    try:
        os.makedirs(res_path)
    except:
        print('makedirs error:', studyUID)

    getfile(data, res_path)
    folder = res_path.encode('utf-8')

    # folder = os.path.join(HOST_LIVER_PATH, userID, studyUID).encode('utf-8')
    print(folder)
    Reader = sitk.ImageSeriesReader()
    DicomNames = Reader.GetGDCMSeriesFileNames(folder)
    #DicomNames = tuple(map(lambda ins: os.path.join(RAWDATA_FOLDER, ins["obsKey"]), instances))
    # DicomNames = tuple(filepaths)
    print(DicomNames)
    Reader.SetFileNames(DicomNames)
    Image = Reader.Execute()

    minPixelValue = -1022.0
    maxPixelValue = 2817.0
    pixelRange = 3840.0
    
    img_arr  = sitk.GetArrayFromImage(Image)
    # img_arr[img_arr<minPixelValue] = minPixelValue
    # img_arr[img_arr>maxPixelValue] = maxPixelValue
    # img_arr = (img_arr.astype(np.float32) - minPixelValue) / pixelRange * 255.0
    
    spacing = list(Image.GetSpacing())
    
    # AI detection for liver
    # model_sess, model_PH_x, model_PH_y = liver_ai_model.initial_model()
    img_pred = liver_ai_model.predict(img_arr)
    #sess.close()
    
    # distinguish tumor
    # tumor_pred, tumor_num = liver_ai_model.count_tumor(img_pred)
    tumor_pred, tumor_num = dataOpts.instance_counter(img_pred)
    #print(list(set(tumor_pred.reshape([-1]).tolist())), tumor_num)
    tumor_dist_result = liver_ai_model.get_tumor_info(tumor_pred, tumor_num, spacing)
    #print(tumor_dist_result)
    pred_save_path = userID + studyUID + 'tumor_pred.npy'
    pred_png = np.zeros_like(tumor_pred)
    for idx in range(1, max(list(set(tumor_pred.reshape([-1]).tolist())))+1):
        pred_png[tumor_pred==idx] = 256 - idx

    pred_full_save_path = os.path.join(RAWDATA_FOLDER, pred_save_path)
    # send_full_save_path = os.path.join(HOST_LIVER_PATH, pred_save_path)
    np.save(pred_full_save_path, pred_png)

    #create the compress data.
    saveAsComprass(pred_full_save_path, pred_full_save_path + '.gz')
    send_info = encodeb64(getFileStr(pred_full_save_path + '.gz'))

    # create tumor label
    label = {}
    for key in tumor_dist_result.keys():
        tumor_result = tumor_dist_result[key]
        label[key] = {
            #"studyUID": studyUID,
            #"seriesNumber": seriesNumber,
            "modality": "CT",
            "copyFrom": "AI",
            "diseaseTag": "LiverTumor",
            "labelID": '{}-{}-{}'.format(tumor_result['pos'][0], tumor_result['pos'][1], tumor_result['instance']),#key,
            "createDate": time.strftime("%Y%m%d", time.localtime()),
            "vol": tumor_result['volume'],
            "maxDiamInstanceID": tumor_result['instance'],
            "coord": {"x":tumor_result['pos'][0], "y":tumor_result['pos'][1], "z":tumor_result['instance']},
            "height": tumor_result['height'],
            "width": tumor_result['width'],
            "diam": tumor_result['diameter'],
            "diam_p1": {'x':tumor_result['p1'][0], 'y':tumor_result['p1'][1]},
            "diam_p2": {'x':tumor_result['p2'][0], 'y':tumor_result['p2'][1]},
            ## something not sure
            "subt": np.random.random(),
            "globalTag": False,
            "avgHU": np.random.random_integers(0, 100),
            "loc": np.random.random_integers(0, 10),
            "effective": True,
            "instanceID": '{}'.format(tumor_result['instance']),
            "depth": 20,
            "malg": np.random.random(),
            "prob": np.random.random(),
        }
    print(json.dumps(label, sort_keys=True, indent=4))
    return make_response(jsonify({"predict": label, "path": send_info}), 200)


if __name__ == '__main__':
    liver_ai_model.init_globals()
    app.run(debug=False, threaded=True, host='0.0.0.0', port=5002)
