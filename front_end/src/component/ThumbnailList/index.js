import React, { Component } from 'react';
import _ from 'underscore'
import {Icon} from 'antd'
import './index.less'
import {VIEW_POSITION} from '../../data'
import { Scrollbars } from 'react-custom-scrollbars'
import Immutable from 'immutable'

/**
 *缩略图List组件
 */
class ThumbnailList extends Component {
    /**
     *
     * @type {{
     * collapsed: boolean 是否折叠
     * }}
     */
  state = {
    // currentIdx: this.props.defaultIdx,
    collapsed: false
  }

    /**
     * 控制是否折叠
     */
  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

    /**
     * 控制滚轮滚动
     * @param e
     */
  onMouseWheel = (e) => {
  	const currentScrollDelta = this.scrollbar.getScrollLeft();
  	this.scrollbar.scrollLeft(currentScrollDelta + e.deltaY);
  }

    /**
     * 视图缩略图组件
     * @return {Component}
     */
  render() {
    return (
      <div className={this.state.collapsed ? 'thumbnail-list-container collapsed' : 'thumbnail-list-container'}>
        <Scrollbars
          ref={input => this.scrollbar = input}
          style={{ height: 200 }}
          renderThumbHorizontal={props => <div {...props} className="thumb-horizontal"/>}
          renderTrackVertical={props => <div {...props} style={{display: 'none'}}/>}
          renderThumbVertical={props => <div {...props} style={{display: 'none'}}/>}
          onWheel={this.onMouseWheel}>
          <ul className='thumbnail-list'>
            {
              this.props.data.map((item, i) => (
                <li
                  key={item.id}
                  onClick={() => {_.isFunction(this.props.onItemClick) && this.props.onItemClick(item)}}
                  className={this.props.activeIds.includes(item.id) ? 'active' : ''}>
                  {this.props.renderItem(item, i)}
                </li>
              ))
            }
          </ul>
        </Scrollbars>
        <div className='toggle-collapsed vertical' onClick={this.toggleCollapsed}>
          {
            this.state.collapsed ?
            <Icon type="up" />
            :
            <Icon type="down" />
          }
        </div>
      </div>
    );
  }

}

export default ThumbnailList;
