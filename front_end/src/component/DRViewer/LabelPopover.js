import React, { Component } from 'react';
import { Card, Select, Icon } from 'antd';
import {CHEST_DISEASES_EN, CHEST_DISEASES_ZN} from '../../data'

/**
 * antd-design 下拉组件
 * @type {React.ClassicComponentClass<OptionProps>}
 */
const Option = Select.Option;
import {GetLocaledText} from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'

/**
 * 添加标记下拉选项组件
 */
class LabelPopover extends Component {
    /**
     * 参照生命周期函数
     * @param props
     */
  constructor(props) {
    super(props)
    // console.log(props.value);
    // this.state = {
    //   value: props.value
    // }
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log('componentWillReceiveProps');
  //   this.setState({
  //     value: nextProps.value
  //   })
  // }
    /**
     * 参照生命周期
     * @returns {Object}
     */
  render() {
    return (
      <Card
        style={{
          position: 'fixed', top: this.props.top, left: this.props.left,
          display: this.props.show ? 'block' : 'none'
        }}>
        <p><FormattedMessage id="DRViewer.dialog.sicknessPosition"/>
          <Icon type="close-circle-o"
            style={{float: 'right'}}
            onClick={this.props.onClose}/>
        </p>
        <Select
          value={this.props.value}
          style={{ width: 150 }}
          onChange={(value) => {
            this.props.onChange(this.props.id, value)
          }}
          >
        <Option value="">请选择病症</Option>
          {
            Object.keys(CHEST_DISEASES_EN).map((name, i) => <Option value={name} key={i}>{GetLocaledText(this,CHEST_DISEASES_EN[name])}</Option>)
          }
        </Select>
      </Card>
    );
  }

}

export default injectIntl(LabelPopover);
