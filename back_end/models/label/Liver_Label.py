#!/bin/python
# coding=utf-8
from Label import Label


class Liver_Label(Label):
    def __init__(self):
        Label.__init__(self)
        self.x = 0
        self.y = 0
        self.z = 0
        self.height = 0
        self.width = 0
        self.volume = 0
        self.diameter = 0
        self.p1_x = 0
        self.p1_y = 0
        self.p2_x = 0
        self.p2_y = 0
        self.avgHU = 0
        self.isAPE = False
        self.mfsEnhancing = False
        self.mfsThresholdGrowth = False
        self.mfsWashout = False
        self.LI_RADS = 'LR-3'
        self.url = ''
        self.malg = 0
        self.seriesNumber = 0

    def to_dict(self):
        return {
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "state": self.state,
            "coord": {"x": self.x, "y": self.y, "z": self.z},
            "width": self.width,
            "height": self.height,
            "volume": self.volume,
            "diameter": self.diameter,
            "p1": {"x": self.p1_x, "y": self.p1_y},
            "p2": {"x": self.p2_x, "y": self.p2_y},
            "avgHU": self.avgHU,
            "isAPE": self.isAPE,
            "mfsEnhancing": self.mfsEnhancing,
            "mfsThresholdGrowth": self.mfsThresholdGrowth,
            "mfsWashout": self.mfsWashout,
            "LI_RADS": self.LI_RADS,
            "url": self.url,
            "malg": self.malg,
            "seriesNumber": self.seriesNumber
        }

    def to_db(self):
        return {
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "state": self.state,
            "x": self.x,
            "y": self.y,
            "z": self.z,
            "width": self.width,
            "height": self.height,
            "volume": self.volume,
            "diameter": self.diameter,
            "p1_x": self.p1_x,
            "p1_y": self.p1_y,
            "p2_x": self.p2_x,
            "p2_y": self.p2_y,
            "avgHU": self.avgHU,
            "isAPE": self.isAPE,
            "mfsEnhancing": self.mfsEnhancing,
            "mfsThresholdGrowth": self.mfsThresholdGrowth,
            "mfsWashout": self.mfsWashout,
            "LI_RADS": self.LI_RADS,
            "url": self.url,
            "malg": self.malg,
            "seriesNumber": self.seriesNumber
        }


required_key_label = ["labelID", "seriesID", "state", "x", "y", "z", \
                      "width", "height", "volume", "diameter", "p1_x", "p1_y", "p2_x", "p2_y"]


def parseLiverLabelist(labels):
    labelist = []
    for label in labels:
        if not all(key in label for key in required_key_label):
            raise ValueError(
                "Liver label must contain {}, while input contains {}".format(required_key_label, label.keys()))

        new_Liverlabel = Liver_Label()
        new_Liverlabel.labelID = label['labelID']
        new_Liverlabel.seriesID = label['seriesID']
        new_Liverlabel.state = label['state']
        new_Liverlabel.x = label['x']
        new_Liverlabel.y = label['y']
        new_Liverlabel.z = label['z']
        new_Liverlabel.width = label['width']
        new_Liverlabel.height = label['height']
        new_Liverlabel.volume = label['volume']
        new_Liverlabel.diameter = label['diameter']
        new_Liverlabel.p1_x = label['p1_x']
        new_Liverlabel.p1_y = label['p1_y']
        new_Liverlabel.p2_x = label['p2_x']
        new_Liverlabel.p2_y = label['p2_y']
        new_Liverlabel.avgHU = label["avgHU"]
        new_Liverlabel.isAPE = label["isAPE"]
        new_Liverlabel.mfsEnhancing = label["mfsEnhancing"]
        new_Liverlabel.mfsThresholdGrowth = label["mfsThresholdGrowth"]
        new_Liverlabel.mfsWashout = label["mfsWashout"]
        new_Liverlabel.LI_RADS = label["LI_RADS"]
        new_Liverlabel.url = label["url"]
        new_Liverlabel.malg = label["malg"]
        new_Liverlabel.seriesNumber = label["seriesNumber"]

        labelist.append(new_Liverlabel)

    return labelist
