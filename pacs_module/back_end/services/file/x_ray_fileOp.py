#!/bin/python
#coding=utf-8

import SimpleITK as sitk
import numpy as np
import scipy.misc
from scipy.misc import imresize

import os, sys, shutil
import uuid

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_insert_one
from config import ADDRESS, PORT, IMAGE_ROOT, PROTOCOL

def cal_hist_dx(source, start_level=0, thresh=0.9995):
    source = source.ravel()
    total = len(source)

    s_values, bin_idx, s_counts = np.unique(source, return_inverse=True, return_counts=True)

    histogram_dict = {}
    for value, count in zip(s_values, s_counts):
        histogram_dict[value] = count

    current_count = 0
    forward = 0
    min_value = start_level
    max_value = 0
    while float(current_count) / float(total) < thresh:
        value = start_level + forward

        if value in histogram_dict:
            max_value = value
            current_count += histogram_dict[value]

        forward += 1

    return min_value, max_value

def insert_x_ray_instances(series):
    count = 0
    instances = series['instances']
    for ins in instances:
        uid = str(uuid.uuid1())
        image = sitk.ReadImage(str(ins['filepath']))
        spacing = list(image.GetSpacing())
        PNGfilename = '{:04}.png'.format(count)
        count += 1
        PNGfolder = os.path.join(IMAGE_ROOT, uid)
        PNGpath = os.path.join(PNGfolder, PNGfilename)
        if os.path.exists(PNGfolder):
            shutil.rmtree(PNGfolder)
        os.makedirs(PNGfolder)

        # convert to png
        img_arr = sitk.GetArrayFromImage(image)
        minPixelValue, maxPixelValue = cal_hist_dx(img_arr, np.min(img_arr), 0.999)
        series['minPixelValue'] = int(minPixelValue)
        series['maxPixelValue'] = int(maxPixelValue)
        pixelValueRange = maxPixelValue - minPixelValue
        img_arr[img_arr<minPixelValue] = minPixelValue
        img_arr[img_arr>maxPixelValue] = maxPixelValue
        img_arr = img_arr - minPixelValue
        img_arr = img_arr.astype(np.float32) / float(pixelValueRange)
        img_arr = img_arr * 255
        img_arr = img_arr.squeeze()
        img_arr = img_arr.astype(np.uint8)
        img_arr = img_arr.reshape(img_arr.shape[0], img_arr.shape[1])
        scipy.misc.imsave(PNGpath, img_arr)

        ins['url'] = "{}://{}:{}/api/study_image?uid={}".format(PROTOCOL, ADDRESS, PORT, uid)
        new_resource = {
            "uid": uid,
            "img_path": os.path.dirname(PNGpath),
            "filename": os.path.basename(PNGpath),
            "dcm_path": os.path.dirname(ins['filepath']),
            "dicom": os.path.basename(ins['filepath']),
        }
        db_insert_one("resource", new_resource)
        ins.pop('filename')
        ins.pop('filepath')

    return True
