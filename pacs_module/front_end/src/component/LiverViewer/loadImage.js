import * as cornerstone from 'cornerstone-core'

let cache = new Map()

export default function loadImage(imageId) {
  if (cache.has(imageId)) {
    return Promise.resolve(cache.get(imageId))
  }
  return cornerstone.loadImage(imageId).then(image => {
    cache.set(imageId, image)
    return image
  })
}
