import axios from 'axios';
import {CODE_MESSAGE} from './codeMessage'
import {message} from 'antd'

//const regionalthings = require('./regional_hostname.js');
//import get_regional_hostname from './regional_hostname.js'
/**
 *
 * @public {string} 接口服务器地址
 */
const path = '';
// const path = 'http://'+regionalthings.get_regional_hostname()+':5000';
/**
 * 后台状态码错误前端显示组件
 */
class StatusCodeError extends Error {
    /**
     * 组件构造函数根据react生命周期使用
     * @param payload
     */
  constructor(payload) {
    super(payload.message)
      /**
       * 将后端返回的状态错误信息赋给这个组件
       */
    this.payload = payload
      /**
       *
       * @private {string}
       */
    this.name = 'StatusCodeError'
  }
}

/**
 * 项目所有的接口列表
 * @public {{register: string[], checkAccount: string[], checkEmail: string[], login: string[], logout: string[], sendResetEmail: string[], resetPassword: string[], getStudylist: string[], getStudyDetail: string[], getStudySeries: string[], getLabels: string[], saveLabels: string[], getRadiomics: string[], getToolsData: string[], saveToolsData: string[], getToolsLiverData: string[], saveToolsLiverData: string[], getReport: string[], updateReport: string[], updateReportStatus: string[], getReportLiver: string[], updateReportLiver: string[], getTemplate: string[], updateTemplate: string[], addTemplate: string[], deleteTemplate: string[], getNoduleRegister: string[], upload: string[], getLabelTasks: string[], applyLabelTasks: string[], getLabelers: string[], reviewLabelTask: string[], submitLabelTask: string[], doctorRequest: string[], manualDetection: string[], beginXYConvert: string[]}}
 */
const config = {
  register: ['/auth/register', 'POST'],
  checkAccount: ['/auth/register/check_account', 'GET'],
  checkEmail: ['/auth/register/check_email', 'GET'],
  login: ['/auth/login', 'POST'],
  logout: ['/auth/logout', 'POST'],
  sendResetEmail: ['/auth/send_reset_email', 'POST'],
  resetPassword: ['/auth/reset_password', 'POST'],
  getStudylist: ['/api/studylist', 'GET'],
  getStudyDetail: ['/api/study', 'GET'],
  getStudySeries: ['/api/series/:(studyUID)', 'GET'],
  getLabels: ['/api/label', 'GET'],
  getDisease:['/api/disease','GET'],
  saveDisease:['/api/disease','PUT'],
  saveLabels: ['/api/label', 'PUT'],
  getRadiomics:['/api/radiomics','GET'], 
  getToolsData: ['/api/tool', 'GET'],
  saveToolsData: ['/api/tool', 'PUT'],
  getToolsLiverData: ['/api/tool-liver/:(studyUID)', 'GET'],
  saveToolsLiverData: ['/api/tool-liver/:(studyUID)', 'POST'],
  getReport: ['/api/report', 'GET'],
  updateReport: ['/api/report', 'PUT'],
  updateReportStatus:['/api/report','POST'],
  getReportLiver: ['/api/report-liver/:(studyUID)', 'GET'],
  updateReportLiver: ['/api/report-liver/:(studyUID)', 'POST'],
  getTemplate: ['/api/template', 'GET'],
  getVersion:['/api/version', 'GET'],
  updateTemplate: ['/api/template', 'PUT'],
  addTemplate:['/api/template','POST'],
  deleteTemplate:['/api/tempate','DELETE'],
  getNoduleRegister: ['/api/nodule_register_label', 'GET'],
  upload: ['/api/upload_image', 'POST'],
  getLabelTasks: ['/api/label_tasks', 'GET'],
  applyLabelTasks: ['/api/label_tasks', 'POST'],
  getLabelers: ['/api/labelers', 'GET'],
  reviewLabelTask: ['/api/review_label_task', 'PUT'],
  submitLabelTask: ['/api/submit_label_task', 'PUT'],
  doctorRequest:['/api/doctor_request','GET'],
  manualDetection:['/api/manual_detection','POST'],
  beginXYConvert:['/api/convert_ct_xy','POST'],
  dicomShot:['/api/dicom_shot','POST'],
  statisticsGet:['/api/statistics','GET']
};
/**
 * 即前端调用后台接口的函数集合对象
 * @type {{}}
 */
let api = {};

/**
 * 传参函数封装
 * @param url 后台接口地址
 * @param params 参数
 * @returns {*} 返回完整参数地址url
 */
function fillUrlParams(url, params) {
  for (let [key, value] of Object.entries(params)) {
    let replace = `:(${key})`
    url = url.replace(replace, value)
  }
  return url
}

for (let key of Object.keys(config)) {
  api[key] = (data={}, urlParams={}, conf={}) => {
    let url = path + config[key][0]
    url = fillUrlParams(url, urlParams)
    let axiosConf = {
      method: config[key][1],
      url,
      timeout: 20000,
      withCredentials: true
    };
    axiosConf = Object.assign(axiosConf, conf)
    if ((config[key][1] === 'POST' || config[key][1] === 'PUT') && data) {
      axiosConf.data = data;
    } else {
      if(url=="/auth/login"){
          axiosConf.params = data;
      }else{
          data["time"]=new Date().getTime();
          axiosConf.params = data;
      }

    }

    return axios(axiosConf).then(res => {
      if (res.status === 200) {
        // console.log(res.data);
        // return res.data
        let payload = res.data;
        if (payload.statusCode === 1000) {
          return payload.data;
        } else {
          if (CODE_MESSAGE.has(payload.statusCode)) {
            message.error(CODE_MESSAGE.get(payload.statusCode))
          }
          throw new StatusCodeError(payload);
        }
      }
    })
    .catch(err => {
      // will not handle error related to business logic
      if (err.name === 'StatusCodeError') {
        throw err
      }
      if (err.response.status === 401) {
        console.log('Redirect to login due to 401 ERROR response status.')
        // window.history.pushState(null, null, '/login')
        window.location.href = '/login'	//!*!*!*! Original Console Log
      } else {
        console.error('请求失败')
        throw err
      }
    })
  };
}

export {config}
export default api;
