import React, { Component } from 'react';
import _ from 'underscore'
import {Icon} from 'antd'
import classNames from 'classnames'
import { Select } from 'antd'
import './SeriesThumbnailItem.less'
import {VIEW_POSITION} from '../../../data'

import { injectIntl } from 'react-intl'
import { GetLocaledText, GetTestText } from '../../../localedFuncs'


const Option = Select.Option;

class SeriesThumbnailItem extends Component {
    constructor(props) {
        super(props)
        this.state = {}
        
    }
    onOptionChange() {
    }
    render() {
        let openedSeriesGcDomIDs = this.props.viewer.getOpenedSeriesGcDomIDs();
        //console.log("this.props.getOpenedSeries(): ");
        //console.log(openedSeriesGcDomIDs);
        let liClasses = classNames({
            'active': openedSeriesGcDomIDs.includes(this.props.seriesNumber) ? true : false,
            'seriesThumbnailItem': true
        });
        //console.log("This is rendered!! iter: " + this.props.iter);

        // if((this.props.iter+1) === (this.props.currentSeriesIdx+1)) {
        //     console.log("(*)(*)(*)(*)(*)(*)(*)(*)(*)(*)");
        //     console.log("this.props.viewer.state.currentSeriesNumber: " + this.props.viewer.state.currentSeriesNumber);
        //     console.log("this.props.series.seriesNumber: " + this.props.series.seriesNumber);
        //     console.log("this.props.currentSeriesIdx: " + this.props.currentSeriesIdx);
        //     console.log("this.props.iter: " + this.props.iter);
        //     this.props.setCurrentSeriesNumber(Number(this.props.series.seriesNumber));
        // }

        // if(this.props){
        //     let viewer = this.props.viewer;
        //     if(viewer){
        //         if(viewer.state){
        //             let currentSeriesNumber = viewer.state.currentSeriesNumber;
        //             if((currentSeriesNumber === undefined) || (currentSeriesNumber === null)){
        //                 console.log("||||||||||||||||||||||||||||||||||");
        //                 this.props.setCurrentSeriesNumber(Number(this.props.series.seriesNumber));
        //             }
        //             //else{console.log("LiverViewer::SeriesThumbnailItem: currentSeriesNumber NOT exist!");}
        //         }
        //         else{console.log("LiverViewer::SeriesThumbnailItem: viewer.state NOT exist!");}
        //     }
        //     else{console.log("LiverViewer::SeriesThumbnailItem: viewer NOT exist!");}
        // }
        // else{console.log("LiverViewer::SeriesThumbnailItem: this.props NOT exist!");}
        return (
            <li
                ref={inst => this.thisObjDom = inst}
                onClick={() => {
                    //console.log("SeriesThumbnailItem::onClick");	//!*!*!*! Jackey Console Log
                    this.props.onItemClick(this.props.iter, this.props.image.instanceID, this.props.series.seriesNumber)}
                }
                className={liClasses}
                onDragStart={(e) => {
                    //console.log("SeriesThumbnailItem::onDragStart");	//!*!*!*! Jackey Console Log
                    e.dataTransfer.setData('seriesNumber', JSON.stringify(this.props.series.seriesNumber));}
                }>
                <Select
                  className="series-name-select"
                  ref={inst => this.select = inst}
                  style={{color: 'white'}} 
                  dropdownStyle={{backgroundColor: 'black'}}
                  onChange={this.onOptionChange.bind(this)}
                  placeholder='Series Name...'>
                    <Option                   
                        className='series-name-dropdown-select-item'
                        value="arterial">
                        Arterial Phase
                    </Option>
                    <Option 
                        className='series-name-dropdown-select-item'
                        value="portal-venous">
                        Portal Venous
                    </Option>
                    <Option                   
                        className='series-name-dropdown-select-item'
                        value="delayed">
                        Delayed
                    </Option>
                    <Option                   
                        className='series-name-dropdown-select-item'
                        value="hbp">
                        HBP
                    </Option>
                </Select>
                <img src={this.props.image.url}/>
                <div 
                    className='info'
                    style={{
                        'display':'flex',
                        'flexDirection':'row',
                        'height': '20px',
                        'justifyContent':'space-between'
                }}>
                    <p >{GetLocaledText(this, "SeriesThumbnailItem.series-number")} {this.props.series.seriesNumber.slice(-2)}</p>
                    <div style={{
                        'display':'flex',
                        'flexDirection':'row',
                        'justifyContent':'space-between'
                    }}>
                        <img 
                            style={{
                                'width': '15px',
                                'height': '15px',
                                'marginRight': '5px'
                            }}
                            src={require("../../../../static/images/icon-layers.png")} />
                        <p>{this.props.totalSlices}</p>
                    </div>
                </div>
            </li>
        )
    }
}

export default injectIntl(SeriesThumbnailItem);
