#!/bin/python
# coding=utf-8
import logging
import traceback
from datetime import timedelta

from models.response.response import response

from flask import Flask, make_response, abort, request, jsonify
from flask_login import LoginManager, current_user, login_user
import flask_profiler
from urllib import quote_plus

from config import MAX_CONTENT_LENGTH, SECRET_KEY, PORT, NEED_AUTH, PRODUCT_VERSION
from services.algo.nodule_service import nodule_register_label
from services.algo.on_radiomics import fetch_radiomics
from services.auth.auth import login, load_user, logout
from services.auth.forget_passwd import reset_password_handler, reset_email_handler
from services.auth.signup import register_handler, confirm_email_handler, check_email, check_account
from services.file.ct_convert_xy import on_convert_ct_xy
from services.file.feed import feed_screenshot, feed_study_image, feed_pacs_study_image, fetch_manual_detection, \
    capture_screenshot
from services.file.fetch import fetch_screenshot, fetch_study_image, fetch_study_image_black, dicom_view
from services.label.on_label import on_label, on_disease
from services.pacs.pacs_api import fetch_doctor_request, update_study_state
from services.report.on_report import on_report
from services.study.get_studylist import get_studylist, get_studylist_count
from services.study.on_study import on_study
from services.template.on_template import on_template
from services.tool.on_tool import on_tool
from services.statistics.statistics import statis_handle
from config import MONGO_NAME, MONGO_USERNAME, MONGO_PASSWORD, MONGO_AUTHSOURCE, MONGO_DATABASE_NAME

logger = logging.getLogger("radiology")


def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = MAX_CONTENT_LENGTH
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(days=7)
app.secret_key = SECRET_KEY
app.after_request(after_request)

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.init_app(app)
login_manager.user_loader(load_user)


def get_version():
    res = response()
    res.data = {"version": PRODUCT_VERSION}
    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)


# user services
app.add_url_rule("/auth/login", "login", login, methods=['POST'])
app.add_url_rule("/auth/logout", "logout", logout, methods=['POST'])
app.add_url_rule("/auth/register", "register", register_handler, methods=['POST'])
app.add_url_rule("/auth/register/confirm", "confirm_email_handler", confirm_email_handler, methods=['GET'])
app.add_url_rule("/auth/register/check_account", "check_account", check_account, methods=['GET'])
app.add_url_rule("/auth/register/check_email", "check_email", check_email, methods=['GET'])
app.add_url_rule("/auth/send_reset_email", "reset_email_handler", reset_email_handler, methods=['POST'])
app.add_url_rule("/auth/reset_password", "reset_password_handler", reset_password_handler, methods=['POST'])

app.add_url_rule("/api/study", "on_study", on_study, methods=['GET', 'POST', 'PUT'])
app.add_url_rule("/api/studylist", "get_studylist", get_studylist, methods=['GET'])
app.add_url_rule("/api/studylist_count", "get_studylist_count", get_studylist_count, methods=['GET'])

app.add_url_rule("/api/report", "on_report", on_report, methods=['GET', 'POST', 'PUT'])
app.add_url_rule("/api/template", "on_template", on_template, methods=['GET', 'POST', 'PUT', 'DELETE'])
app.add_url_rule("/api/tool", "on_tool", on_tool, methods=['GET', 'POST', 'PUT', 'DELETE'])
app.add_url_rule("/api/label", "on_label", on_label, methods=['GET', 'POST', 'PUT', 'DELETE'])
app.add_url_rule("/api/disease", "on_disease", on_disease, methods=["GET", "PUT"])
app.add_url_rule('/api/nodule_register_label', 'nodule_register_label', nodule_register_label, methods=['GET'])

app.add_url_rule("/api/screenshot", "fetch_screenshot", fetch_screenshot, methods=['GET'])
app.add_url_rule("/api/study_image", "fetch_study_image", fetch_study_image, methods=['GET'])
app.add_url_rule("/api/study_image_black", "fetch_study_image_black", fetch_study_image_black, methods=['GET'])
app.add_url_rule("/api/upload_screenshot", "feed_screenshot", feed_screenshot, methods=['POST'])
app.add_url_rule("/api/dicom_shot", "capture_screenshot", capture_screenshot, methods=['POST'])
app.add_url_rule("/api/upload_image", "feed_study_image", feed_study_image, methods=['POST'])
app.add_url_rule("/api/upload_pacs_image", "feed_pacs_study_image", feed_pacs_study_image, methods=['POST'])
app.add_url_rule("/api/radiomics", "fetch_radiomics", fetch_radiomics, methods=['GET'])
app.add_url_rule("/api/doctor_request", "fetch_doctor_request", fetch_doctor_request, methods=['GET'])
app.add_url_rule("/api/update_study_state", "update_study_state", update_study_state, methods=['GET'])
app.add_url_rule("/api/manual_detection", "fetch_manual_detection", fetch_manual_detection, methods=['POST'])
app.add_url_rule("/api/convert_ct_xy", "on_convert_ct_xy", on_convert_ct_xy, methods=["POST"])
app.add_url_rule("/api/version", "get_version", get_version, methods=["GET"])
app.add_url_rule("/api/statistics", "get_statistics", statis_handle, methods=["GET"])

app.add_url_rule("/app/data/<folder>/<series>/<filename>", endpoint='/app/data', view_func=dicom_view)

app.config["flask_profiler"] = {
    "enabled": True,
    "storage": {
        "engine": "mongodb",
        "MONGO_URL": "mongodb://%s:%s@%s/default_db?authSource=%s" % (quote_plus(MONGO_USERNAME),
                                                                      quote_plus(MONGO_PASSWORD),
                                                                      MONGO_NAME,
                                                                      MONGO_AUTHSOURCE),
        "DATABASE": MONGO_DATABASE_NAME
    },
    "basicAuth": {
        "enabled": True,
        "username": "admin",
        "password": "imsightmed"
    },
    "ignore": [
        "^/static/.*",
        "^/app/data/,*"
    ]
}

flask_profiler.init_app(app)


@app.before_request
def pre_handler():
    if NEED_AUTH:
        if request.path in ["/auth/login", "/auth/register", "/auth/register/confirm", "/auth/register/check_account",
                            "/auth/register/check_email", "/auth/send_reset_email", "/auth/reset_password",
                            "/api/version"]:
            return
        else:
            if not current_user.is_authenticated:
                return abort(401)
    else:
        user = load_user("imsightmed")
        login_user(user)
        return


@app.errorhandler(Exception)
def internal_error(error):
    logger.error(error)
    logger.error(traceback.format_exc())
    return make_response("internal error", 500)


if __name__ == '__main__':
    app.run(debug=True, threaded=True, host='0.0.0.0', port=PORT)
