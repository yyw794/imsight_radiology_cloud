import tensorflow as tf
import os
import numpy as np
import sklearn.cluster as sc
from skimage import measure
import scipy
import base64

os.environ['CUDA_VISIBLE_DEVICES'] = '3'

global_vars = { "is_session_inited": False,
                "session1": None,
                "sess1_x": None,
                "sess1_y": None,
                "session2": None,
                "sess2_x": None,
                "sess2_y": None,
               }

def init_globals():
    if not global_vars["is_session_inited"]:
        print("init the session1")
        global_vars["session1"], global_vars["sess1_x"], global_vars["sess1_y"] = \
            initial_model(meta_path='ckpt/liver.ckpt-0.meta', cpkt_path='ckpt/liver.ckpt-0')
        print("finished the session1 init and init the session2")
        global_vars["session2"], global_vars["sess2_x"], global_vars["sess2_y"] = \
            initial_model(meta_path='ckpt/tumor.ckpt-0.meta', cpkt_path='ckpt/tumor.ckpt-0')
        print("finished the session2 init.")



def initial_model(
        # model initialize
        meta_path='ckpt/model.ckpt-100.meta',
        cpkt_path='ckpt/model.ckpt-100',
        input_name='Placeholder',
        net_name='pred_network',
):
    sess = tf.Session()
    # import graph and load weights
    with sess.as_default():
        model_saver = tf.train.import_meta_graph(meta_path)
        model_saver.restore(sess, cpkt_path)
        graph = tf.get_default_graph()
        # print(graph.get_all_collection_keys())
        # print(graph.get_operations()[0:2])
        x = graph.get_operation_by_name(input_name).outputs[0]
        y = tf.get_collection(net_name)[0]

    return sess, x, y


""" Old one-step model
def predict(sess, x, y, image, batch_size=10):
    # preprocess
    image_tmp = np.pad(image, ((1, 1), (0, 0), (0, 0)), 'edge')
    image_tmp = np.array([image_tmp[:-2,:,:], image_tmp[1:-1,:,:], image_tmp[2:,:,:]])
    image_input = np.transpose(image_tmp, axes=(1,2,3,0))
    image_pred = np.zeros_like(image)

    for i in range(0, image_input.shape[0], batch_size):
        image_feed = image_input[i:i+batch_size,:,:,:]
        pred = sess.run(y, feed_dict={x: image_feed})
        image_pred[i:i+batch_size,:,:] = pred
    return image_pred
"""


def predict(image, batch_size=10):
    # preprocess
    image_tmp = np.pad(image, ((1, 1), (0, 0), (0, 0)), 'edge')
    image_tmp = np.array([image_tmp[:-2, :, :], image_tmp[1:-1, :, :], image_tmp[2:, :, :]])
    image_input = np.transpose(image_tmp, axes=(1, 2, 3, 0))
    image_liver_pred = np.zeros_like(image)
    image_tumor_pred = np.zeros_like(image)

    # predict liver
    sess, x, y = global_vars["session1"], global_vars["sess1_x"], global_vars["sess1_y"]
    for i in range(0, image_input.shape[0], batch_size):
        image_feed = image_input[i:i + batch_size, :, :, :]
        pred = sess.run(y, feed_dict={x: image_feed})
        image_liver_pred[i:i + batch_size, :, :] = pred
    print(set(image_liver_pred.reshape([-1]).tolist()))
    image_liver_pred = (image_liver_pred > 0) * 1
    print(set(image_liver_pred.reshape([-1]).tolist()))
    # sess.close()

    # image[image < -200] = -200
    # image[image > 250] = 250
    # image = image - 48
    # image = (image.astype(np.float32) - minPixelValue) / pixelRange * 255.0
    # image_tmp = np.pad(image, ((1, 1), (0, 0), (0, 0)), 'edge')
    # image_tmp = np.array([image_tmp[:-2, :, :], image_tmp[1:-1, :, :], image_tmp[2:, :, :]])
    # image_tmp
    image_tmp[image_tmp < -200] = -200
    image_tmp[image_tmp > 250] = 250
    image_tmp = image_tmp - 48
    image_input = np.transpose(image_tmp, axes=(1, 2, 3, 0))

    # predict tumor
    sess, x, y = global_vars["session2"], global_vars["sess2_x"], global_vars["sess2_y"]
    for i in range(0, image_input.shape[0], batch_size):
        image_feed = image_input[i:i + batch_size, :, :, :]
        pred = sess.run(y, feed_dict={x: image_feed})
        image_tumor_pred[i:i + batch_size, :, :] = pred
    print(set(image_tumor_pred.reshape([-1]).tolist()))
    image_tumor_pred = (image_tumor_pred > 1) * 2
    print(set(image_tumor_pred.reshape([-1]).tolist()))
    # sess.close()

    mask_tumor = image_liver_pred * image_tumor_pred
    image_liver_pred[mask_tumor == 2] = 2
    return image_liver_pred


def count_tumor(image_pred):
    # [!] 'image_pred' would be changed.
    # image_pred (c,w,h) ~ [0,1,2]
    tumor_coords = np.argwhere(image_pred == 2)
    tumor_pos_pred = sc.MeanShift(bandwidth=10).fit_predict(tumor_coords)

    for i, coords in enumerate(tumor_coords):
        image_pred[coords[0], coords[1], coords[2]] = tumor_pos_pred[i] + 2

    return np.array(image_pred, dtype=np.int), max(tumor_pos_pred) + 1


def get_tumor_info(tumor_dist, tumor_num, spacing):
    tumor_info = {}
    for i in range(tumor_num):
        volume = len(tumor_dist[tumor_dist == (i + 2)])
        tumor_info[i + 2] = {'instance': 0, 'max_area': 0, 'diameter': 0, 'p1': [0, 0], 'p2': [0, 0],
                             'volume': volume * spacing[0] * spacing[1] * spacing[2]}

    for inst in range(tumor_dist.shape[0]):
        instance = tumor_dist[inst, :, :]
        if instance.max() < 2:
            continue
        for idx in range(max([2, instance[instance > 1].min()]), instance.max() + 1):
            tumor_i = np.zeros_like(instance)
            tumor_i[instance == idx] = 1

            area = tumor_i.sum()
            if area > tumor_info[idx]['max_area']:
                tumor_info[idx]['max_area'] = area
                tumor_info[idx]['instance'] = inst

                contour = measure.find_contours(tumor_i, 0)[0]
                xs = [p[0] for p in contour]
                ys = [p[1] for p in contour]
                xs_min = min(xs)
                xs_max = max(xs)
                ys_min = min(ys)
                ys_max = max(ys)
                D_arr = scipy.spatial.distance.cdist(contour, contour, 'euclidean')
                diameter = D_arr.max()
                pos_idx = np.unravel_index(np.argmax(D_arr, axis=None), D_arr.shape)
                tumor_info[idx]['diameter'] = diameter * spacing[0]
                tumor_info[idx]['p1'] = contour[pos_idx[0]]
                tumor_info[idx]['p2'] = contour[pos_idx[1]]
                tumor_info[idx]['pos'] = [int((ys_min + ys_max) / 2), int((xs_min + xs_max) / 2)]
                tumor_info[idx]['height'] = (xs_max - xs_min) * spacing[0]
                tumor_info[idx]['width'] = (ys_max - ys_min) * spacing[0]
    return tumor_info