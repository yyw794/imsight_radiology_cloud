# Imsight radiology cloud frontend

## First use
```npm install```

## Run development server on localhost:10000
Firstly, you should modify the proxy target in dev-server.js based on your backend server. Then run:
```npm start```
To avoid cross-origin-strategy, This dev-server will proxy request with '/api' or '/auth' prefix to backend server.

## Run production
```npm run build```
This command will produce static file in ./build. NOTICE: there is no proxy anymore.
To deploy in production, you need to do the following things:
+ Set up a static server with Reverse Proxy. You may want to use Nginx or nodejs.
+ Support html5 broswer hitstory. As this is a single-page-app, all routing is done by frontend. Accessing a url like '/login' may result in 404 because there is no such router in backend.

A example using nodejs is as followed:

```
const express = require('express');
const history = require('connect-history-api-fallback');
const proxy = require('http-proxy-middleware');
const path = require('path');
const port = process.env.PORT || 80;
const app = express();

app.use('/auth', proxy({target: 'http://192.168.1.118:5000', changeOrigin: true}));
app.use('/api', proxy({target: 'http://192.168.1.118:5000', changeOrigin: true}));

//加载指定目录静态资源
app.use(express.static(__dirname + '/build'))

//配置任何请求都转到index.html，而index.html会根据React-Router规则去匹配任何一个route
app.use(history({
  index: '/',
  verbose: true,
}));

app.get('/', function (request, response){
  response.sendFile(path.resolve(__dirname, 'build', 'index.html'))
})

app.listen(port, function () {
  console.log("server started on port " + port)
})
```
