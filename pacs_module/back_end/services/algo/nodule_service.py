#!/bin/python
#coding=utf-8
import sys
import os
from flask import Flask, make_response, jsonify, request, abort
from flask_login import current_user

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.label.Nodule_Label import Nodule_Label
from models.database.database_ops import db_find_one, db_update_one
from services.algo.algo_api import add_lung_nodule
from models.response.response import response
from config import DICOM_ROOT

required_key_register = ['taskID_fixed', 'taskID_moving']

def nodule_register_label():
    res = response()
    params = request.args.to_dict()
    if not all(key in params for key in required_key_register):
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    taskID_fixed = params['taskID_fixed']
    taskID_moving = params['taskID_moving']
    label_info = db_find_one('ai_label', {"taskID_fixed": taskID_fixed, "taskID_moving": taskID_moving})

    if label_info is None:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    register_nodules = label_info['registerNodules']
    for fixed, moving in zip(register_nodules['fixed'], register_nodules['moving']):
        fixed['coord'] = {'x': fixed['x'], 'y': fixed['y'], 'z': fixed['z']}
        moving['coord'] = {'x': moving['x'], 'y': moving['y'], 'z': moving['z']}
    res.data = register_nodules
    res.statusCode = 1000
    # return res.to_dict()
    return make_response(jsonify(res.to_dict()), 200)

def run_lung_nodule_analyzer(params):
    taskInfo = db_find_one('task', {'taskID': params['taskID']})
    studyUID = taskInfo['studyUID']

    lung_box = db_find_one('ai_label', {'studyUID': studyUID, 'seriesID': params['seriesID']})
    params['dcm_path'] = os.path.join(DICOM_ROOT, params['seriesID'])
    params['dicom'] = ""
    params['leftLungBBox'] = lung_box['leftLungBBox']
    params['rightLungBBox'] = lung_box['rightLungBBox']
    params['radius'] = 15

    resp = add_lung_nodule(params)
    resp = resp.json()['data']
    nodule = Nodule_Label()
    nodule.x = resp['x']
    nodule.y = resp['y']
    nodule.z = resp['z']
    nodule.nodule_prob = resp['nodule_prob']
    nodule.nodule_avgHU = resp['nodule_avgHU']
    nodule.nodule_location = resp['nodule_location']
    nodule.nodule_volume = resp['nodule_volume']
    nodule.nodule_diameter = resp['nodule_diameter']
    nodule.nodule_bm = resp['nodule_bm']
    nodule.nodule_bm_prob = resp['nodule_bm_prob']
    nodule.nodule_type = resp['nodule_type']
    nodule.nodule_type_prob = resp['nodule_type_prob']
    nodule.nodule_slice = resp['nodule_slice']
    #formated_result = formant_LUNG_predict(resp['data'], series['seriesID'])
    return nodule

if __name__ =='__main__':
    # ret = nodule_register_label('888', '74487b28-995d-11e8-8278-4ccc6af4b012')
    params = {"taskID": "74487b28-995d-11e8-8278-4ccc6af4b012", 'seriesID': '1.2.392.200036.9116.2.5.1.48.1221404901.1524567726.402005'}
    params['coord'] = {}
    params['coord']['x'] = 111
    params['coord']['y'] = 111
    params['coord']['z'] = 11

    ret = run_lung_nodule_analyzer(params)
    print ret.to_dict()
