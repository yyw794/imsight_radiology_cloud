#!/bin/python
from pymongo import MongoClient
import threading, os, sys

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from config import MONGO_NAME, MONGO_USERNAME, MONGO_PASSWORD, MONGO_AUTHSOURCE, MONGO_DATABASE_NAME

client = MongoClient(MONGO_NAME, username=MONGO_USERNAME, password=MONGO_PASSWORD, authSource=MONGO_AUTHSOURCE)
db = client[MONGO_DATABASE_NAME]
insert_mutex = threading.Lock()
update_mutex = threading.Lock()
delete_mutex = threading.Lock()

# insert a new document to target collection
def db_insert_one(collection_name, insert_value):
    if collection_name not in db.collection_names():
        return False

    collection = db[collection_name]

    insert_mutex.acquire()
    collection.insert_one(insert_value)
    insert_mutex.release()
    return True

# insert multiple documents to target collection
def db_insert_many(collection_name, insert_value):
    if collection_name not in db.collection_names():
        return False

    collection = db[collection_name]

    insert_mutex.acquire()
    collection.insert_many(insert_value)
    insert_mutex.release()
    return True

# find all documents fit criteria from target collection
def db_find(collection_name, query_value, projection=None):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    if projection is not None:
        cursors = collection.find(query_value, projection)
    else:
        cursors = collection.find(query_value)
    datalist = []
    if cursors is not None:
        for file in cursors:
            if "_id" in file:
                file.pop("_id")
            datalist.append(file)
    return datalist

# find one document in target collection
def db_find_one(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]

    data = collection.find_one(query_value)
    if data is not None:
        if "_id" in data:
            data.pop("_id")
    return data

# find all documents in target collection
def db_find_all(collection_name):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    data = collection.find({})
    if data is not None:
        for d in data:
            if "_id" in d:
                d.pop("_id")
    return data

def db_page_query(collection_name, query_value, page_size, page_no):
    skip = page_size * (page_no - 1)
    collection = db[collection_name]
    page_record = collection.find(query_value).limit(page_size).skip(skip)
    if page_record is not None:
        for p in page_record:
            if "_id" in p:
                p.pop("_id")
    return page_record

def db_update_one(collection_name, query_value, update_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    update_mutex.acquire()
    data = collection.update_one(query_value, {'$set': update_value})
    update_mutex.release()
    if data is not None:
        return True
    return False

def db_delete_one(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    delete_mutex.acquire()
    collection.delete_one(query_value)
    delete_mutex.release()
    return True

def db_find_update_otherwise_insert(collection_name, query_value, update_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    document = collection.find_one(query_value)
    if document:
        collection.update_one(query_value, {'$set': update_value})
    else:
        collection.insert_one(update_value)

if __name__ == '__main__':
    print db_find('user', {'account': 'user1'})
