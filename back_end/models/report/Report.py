#!/bin/python
# coding=utf-8
import datetime

statelist = ["assign", "diagnosis", "review", "pass"]


class Report(object):
    def __init__(self):
        self.createDate = datetime.datetime.today()
        self.updateDate = datetime.datetime.today()
        self.studyUID = ""
        self.taskID = ""
        self.patientName = ""
        self.patientID = ""
        self.gender = ""
        self.birthDate = None
        self.modality = ""
        self.studyDate = None
        self.screenshotURLs = []
        self.imagingFind = ""
        self.diagnosisAdvice = ""
        self._state = "assign"

    def hello(self):
        if self.state == statelist[0]:
            self.state = statelist[1]
            return True
        return False

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        if state not in statelist:
            raise ValueError("Report state must be one of {}, while input is {}".format(statelist, state))
        self._state = state

    def submit(self):
        self.state = statelist[2]

    def unsubmit(self):
        self.state = statelist[1]

    def confirm(self):
        self.state = statelist[3]

    def reject(self):
        self.state = statelist[1]

    def unconfirm(self):
        self.state = statelist[2]

    def reset(self):
        self.state = statelist[1]
        self.screenshotURLs = []
        self.imagingFind = ""
        self.diagnosisAdvice = ""

    def submitted(self):
        return self._state in statelist[2:]

    def editable(self, role):
        if role == "reporter" and self.state in statelist[:2]:
            return True
        if role == "reviewer" and self.state in statelist[2:]:
            return True
        else:
            return False

    def legalAction(self, role, action):
        if action == "reset":
            return True
        if role == "reporter" and action == "submit" and self.state == statelist[1]:
            return True
        if role == "reporter" and action == "unsubmit" and self.state == statelist[2]:
            return True
        if role == "reviewer" and action == "confirm" and self.state == statelist[2]:
            return True
        if role == "reviewer" and action == "reject" and self.state == statelist[2]:
            return True
        if role == "reviewer" and action == "unconfirm" and self.state == statelist[3]:
            return True
        else:
            return False

    def to_dict(self):
        age = 0
        birthDate = ""
        uploadTime = ""

        if self.birthDate is not None:
            birthDate = (self.birthDate).strftime('%Y-%m-%d')
            age = (datetime.datetime.today() - self.birthDate).days // 365

        updateDate = (self.updateDate).strftime('%Y-%m-%d %H:%M:%S')
        studyDate = (self.studyDate).strftime('%Y-%m-%d %H:%M:%S')

        return {
            "studyUID": self.studyUID,
            "taskID": self.taskID,
            "patientName": self.patientName,
            "patientID": self.patientID,
            "gender": self.gender,
            "age": age,
            "modality": self.modality,
            "studyDate": studyDate,
            "updateDate": updateDate,
            "screenshotURLs": self.screenshotURLs,
            "imagingFind": self.imagingFind,
            "diagnosisAdvice": self.diagnosisAdvice,
            "submitted": self.state in statelist[2:],
            "examined": self.state in statelist[3:]
        }

    def to_db(self):
        return {
            "studyUID": self.studyUID,
            "taskID": self.taskID,
            "birthDate": self.birthDate,
            "studyDate": self.studyDate,
            "updateDate": self.updateDate,
            "createDate": self.createDate,
            "patientName": self.patientName,
            "patientID": self.patientID,
            "gender": self.gender,
            "modality": self.modality,
            "screenshotURLs": self.screenshotURLs,
            "imagingFind": self.imagingFind,
            "diagnosisAdvice": self.diagnosisAdvice,
            "state": self.state
        }


required_key = ['studyUID', 'taskID', 'screenshotURLs', 'createDate', 'updateDate', 'studyDate', \
                'birthDate', 'patientName', 'patientID', 'gender', 'modality', 'imagingFind', \
                'diagnosisAdvice', 'state']


def parseReportInfo(info):
    if type(info) is not dict:
        raise ValueError("parseReportInfo input must be dict type, while type is {}".format(type(info)))

    if not all(key in info for key in required_key):
        raise ValueError("parseReportInfo key mismatch", info.keys(), required_key)

    new_report = Report()
    new_report.studyUID = info["studyUID"]
    new_report.taskID = info['taskID']
    new_report.createDate = info['createDate']
    new_report.updateDate = info['updateDate']
    new_report.studyDate = info['studyDate']
    new_report.birthDate = info['birthDate']
    new_report.patientName = info['patientName']
    new_report.patientID = info['patientID']
    new_report.gender = info['gender']
    new_report.modality = info['modality']
    new_report.screenshotURLs = info["screenshotURLs"]
    new_report.imagingFind = info["imagingFind"]
    new_report.diagnosisAdvice = info["diagnosisAdvice"]
    new_report.state = info['state']

    return new_report
