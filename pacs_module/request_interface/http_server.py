# -*- coding: UTF-8 -*-
from flask import Flask,make_response,request,jsonify
import initconfig as config
import json
import logging
import logger
from ConcurrentQueue import ConcurrentQueue
import httplib2
import json
import threading,thread,datetime,os
from mongodb_interface import *

log = logging.getLogger("PACSModule")

# flask server for dcmtk
app = Flask(__name__)


# 更新数据为不支持，并且如果是医生点击的照片，需要让医生通知socket
def update_db_send_flask(studyUID):
    studyInfo = db_find_one("study_info_list", {'studyUID': studyUID})
    if studyInfo is not None:
        studyInfo['state'] = "modality_unsupport"
        db_find_update_otherwise_insert("study_info_list", {'studyUID': studyUID}, studyInfo)
        log.warning("may be can not support modality {}".format(studyInfo))
        # 如果是医生主动请求的数据,则需要通知flask发socket给node
        if studyInfo['doctorRequest'] is True:
            update_study_state(studyUID)


@app.route('/api/after_receive', methods=['POST'])
def after_receive():
    """
    @api {POST} /api/after_receive
    @apiDescription   dcmtk接收完数据时发送的结果
    @apiVersion 1.0.0
    @apiParam {number=800,404,504,100} statecode receive state ,
                                  800:receive success, 404:not support, 504: receive abort, 100: other error
    @apiParam {string} message receive information
    @apiParam {string} studyUID study instance uid
    @apiParam {string} filepath dcm folder if receive success
    @apiParamExample {json} Request-Example:
                          {"statecode":800,
                          "message":"success receive dcm data 800 /store_image/20181201/1.22.12358795.5623",
                          "studyUID":"1.22.12358795.5623",
                          "filepath":"/store_image/20181201/1.22.12358795.5623"}
    @apiSuccessExample {json} Success-Response:
                          {"data"{},"message":"OK","statusCode":"1000"}
    """
    respose_json = {}
    params = request.json
    if params is not None:
        log.info("after receive notify {}".format(params))

        if params['statecode'] == 800 and os.path.exists(params['filepath']):
            thread_upload_image_path(params['filepath'])
            log.info("success after receive data...")

        elif params['statecode'] == 404:
            update_db_send_flask(params['studyUID'])

        elif params['statecode'] == 504:  # 传输过程中断，等待下次论寻重新拉取
            log.warning("other error accord...")
            if config.get_if_push_model() is True:
                thread_upload_image_path(params['filepath'])
        else:
            update_db_send_flask(params['studyUID'])


    respose_json["data"] = {}
    respose_json["message"] = 'OK'
    respose_json["statusCode"] = '1000'
    return json.dumps(respose_json, ensure_ascii=False)


@app.route('/')
def other():
    """
    @api {POST GET} /
    @apiDescription   other all request url
    @apiVersion 1.0.0
    @apiSuccessExample {json} Success-Response:
                          {"data"{},"message":"INVALID PARAMETERS","statusCode":"1001"}
    """
    respose_json = {}
    respose_json['data'] = {}
    respose_json["message"] = 'INVALID PARAMETERS'
    respose_json["statusCode"] = '1001'
    return json.dumps(respose_json, ensure_ascii=False)


def start_server():
    app.run(threaded=True, host=config.get_http_listen_ip(), port=config.get_http_listen_port())

# queue
study_inatance_uid_path = ConcurrentQueue(3000)
g_send_thread_count = 0
_G_MAX_THREAD_COUNT_ = 5
_cond = threading.Condition()


def thread_upload_image_path(study_uid_path):
    study_inatance_uid_path.put(study_uid_path)


# 死循环处理发送数据的队列
def do_upload_image_path():
    global g_send_thread_count
    while True:
        try:
            study_path = study_inatance_uid_path.get()  # 等待需要处理的数据
            logger.info("queue to flask get a data {}".format(study_path))

            if _cond.acquire():
                while g_send_thread_count >= _G_MAX_THREAD_COUNT_:  # 判断是否已经超过了最大的线程数量
                    _cond.wait()
                g_send_thread_count += 1
                thread.start_new_thread(thread_upload_image_path_temp, (study_path,))
                _cond.notify()
                _cond.release()
        except Exception as e:
            log.error(e)


# 最大几个线程给flask传输数据
def thread_upload_image_path_temp(study_path):
    global g_send_thread_count
    log.info("***************{} begin, remain {} , current thread {}"
             .format(study_path, study_inatance_uid_path.size(), g_send_thread_count))

    starttime = datetime.datetime.now()
    result = upload_image_path(study_path)
    endtime = datetime.datetime.now()

    log.info("*************** {} end depends on {}s result {} ".format(study_path, (endtime - starttime).seconds, result))

    if _cond.acquire():
        g_send_thread_count = g_send_thread_count - 1
        _cond.notify()
        _cond.release()


# client to flask
key_path = 'cert/server.key'
cert_path = 'cert/server.cert'
cookie = ''


def doresp(resp, content):
    if resp['status'] != '200':
        return {'status': resp['status']}
    else:
        return json.loads(content)


# 向flask发送登录
def login_request():
    try:
        http = httplib2.Http(disable_ssl_certificate_validation=True, timeout=10)
        user = {'account': config.get_server_account(), 'password': config.get_server_password()}
        headers = {'content-type': 'application/json'}
        http.add_certificate(key_path, cert_path, '')
        resp, content = http.request(config.get_server_url() + '/auth/login', 'POST', body=json.dumps(user),
                                     headers=headers)
        global cookie
        cookie = resp['set-cookie']
        content_json = json.loads(content)
        return content_json["message"] == "OK"
    except Exception as e:
        log.error(e)
        return False


# 向flask传输数据
def insert_a_image(filepaths):
    try:
        http = httplib2.Http(disable_ssl_certificate_validation=True, timeout=1200)
        headers = {'content-type': 'application/json', 'Cookie': cookie}
        filepathsJson = {'filepaths': filepaths}
        http.add_certificate(key_path, cert_path, '')
        resp, content = http.request(config.get_server_url()+'/api/upload_pacs_image', 'POST',
                                     body=json.dumps(filepathsJson), headers=headers)

        log.info("resp status is {}".format(resp['status']))
        if '401' == resp['status']:
            log.warning("insert_a_image resp 401. do login and insert again")
            login_request()
            insert_a_image(filepaths)
        return doresp(resp, content)
    except Exception as e:
        log.error(e)
        return None


def upload_image_path(file_path):
    file_path_list = []
    file_list = os.listdir(file_path)
    log.info('upload_image_path file_path {} has file number is {}'.format(file_path,len(file_list)))
    for file_temp in file_list:
        file_path_list.append(os.path.join(file_path, file_temp))
    return insert_a_image(file_path_list)


# 这里只是通知医生主动拉取数据时候不支持消息
def update_study_state(study_uid):
    try:
        http = httplib2.Http(disable_ssl_certificate_validation=True, timeout=20)
        headers = {'content-type': 'application/json', 'Cookie': cookie}
        body_json = {}
        http.add_certificate(key_path, cert_path, '')
        url = config.get_server_url()+'/api/update_study_state?studyUID='+study_uid
        resp, content = http.request(url, 'GET', body=json.dumps(body_json), headers=headers)

        log.info("resp status is {}".format(resp['status']))
        if '401' == resp['status']:
            log.error("update_study_state resp 401. do login and update again")
            login_request()
            update_study_state(study_uid)
        return doresp(resp, content)
    except Exception as e:
        log.error(e)
        return None
