# -*- coding: utf-8 -*-
import httplib2
import json
import sys
import initconfig as config
import logging
import logger
import os
log = logging.getLogger("PACSModule.http_client")
cookie = ''

key_path = 'cert/server.key'
cert_path = 'cert/server.cert'


def doresp(resp, content):
	if resp['status'] != '200':
		return {'status': resp['status']}
	else:
		return json.loads(content)


def login_request():
	try:
		http = httplib2.Http(disable_ssl_certificate_validation=True, timeout=10)
		user = {'account': config.get_server_account(), 'password': config.get_server_password()}
		headers = {'content-type': 'application/json'}
		http.add_certificate(key_path, cert_path, '')
		resp, content = http.request(config.get_server_url()+'/auth/login', 'POST', body=json.dumps(user), headers=headers)
		
		global cookie
		cookie = resp['set-cookie']
		content_json = json.loads(content)
		log.info('login_request message ' + content_json['message'] + " statusCode " + str(content_json['statusCode']))
		return content_json["message"] == "OK"
	except Exception as e:
		log.error(e)
		return False


# patient = {'studyUID':'123','studyDate':'20180603','birthDate':'20180302','hospital':'aaa','bodyPartExamined':'CHEST',
# 'gender':"MALE/FEMALE",'modality':'CT','patientID':'11','patientName':'22'}
def insert_a_patient_list(patient):
	http = httplib2.Http(disable_ssl_certificate_validation=True, timeout=10)
	headers = {'content-type': 'application/json', 'Cookie': cookie}
	body_json = json.dumps(patient)
	http.add_certificate(key_path, cert_path, '')
	resp, content = http.request(config.get_server_url()+'/api/study', 'POST', body=body_json, headers=headers)
	return doresp(resp, content)


def insert_patient_info_list(patient_info_list):
	http = httplib2.Http(disable_ssl_certificate_validation=True, timeout=10)
	headers = {'content-type': 'application/json', 'Cookie': cookie}
	body_json = json.dumps(patient_info_list)
	http.add_certificate(key_path, cert_path, '')
	resp, content = http.request(config.get_server_url()+'/api/study_info_list', 'POST', body=body_json, headers=headers)
	return doresp(resp, content)


def insert_study_state(study_date, study_uid, state):
	http = httplib2.Http(disable_ssl_certificate_validation=True, timeout=10)
	headers = {'content-type': 'application/json', 'Cookie': cookie}
	body_json = {}
	http.add_certificate(key_path, cert_path, '')
	url = config.get_server_url()+'/api/update_study_state?studyUID='+study_uid+'&studyDate='+study_date+'&study_state='+state
	resp, content = http.request(url, 'GET', body=json.dumps(body_json), headers=headers)
	return doresp(resp, content)    


# file_path = {'filepaths:[1.dcm,2.dcm...]'}
def insert_a_image(filepaths):
	http = httplib2.Http(disable_ssl_certificate_validation=True, timeout=500)
	headers = {'content-type': 'application/json', 'Cookie': cookie}
	filepathsJson = {'filepaths': filepaths}
	http.add_certificate(key_path, cert_path, '')
	resp, content = http.request(config.get_server_url()+'/api/upload_pacs_image', 'POST', body=json.dumps(filepathsJson), headers=headers)
	print "resp status is "+str(resp['status'])
	return doresp(resp, content)


def upload_image_path(file_path):
	file_path_list = []
	file_list = os.listdir(file_path)
	for file_temp in file_list:
		file_path_list.append(os.path.join(file_path, file_temp))
	return insert_a_image(file_path_list)
