import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'
import './index.less'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {VIEW_MODALITY_URL, openWindow, openWindowBlank} from "../StudyTable";
import Immutable from 'immutable'
const allSkip=new Immutable.Map();
/**
 *
 * @type {{CT: string, DR: string, DX: string, CR: string}}
 */
const typeClass = {
    CT: 'view_ct',
    MR: 'view_ct',
    DR: 'view_dr',
    DX: 'view_dr',
    CR: 'view_dr'
}
/**
 *
 * @type {{cursor: string, WebkitUserSelect: string, MozUserSelect: string, msUserSelect: string, userSelect: string}}
 */
const style = {
  'cursor': 'pointer',
  'WebkitUserSelect': 'none',
  'MozUserSelect': 'none',
  'msUserSelect': 'none',
  'userSelect': 'none',
}

/**
 * 随访与阅片之间切换button
 * @return {Component}
 */
const ReturnCT = withRouter(
  ({ history }) => (
      <span>
      <Link to="/">
      <a style={{float:"left",color:"#fff",height:"30px"}}>
        <img style={style} className='logo-image'style={{
            cursor:"pointer",
            userSelect:"none",
             width:"1rem",
            verticalAlign:"middle",
            marginRight:"10px",
            marginTop:"-0.1rem",
            marginLeft:"20px"}} src={require('../../../static/images/returnList.png')} /*onClick={() => history.push('/')}*//>
          <span style={{fontSize:"12px"}}><FormattedMessage id='ReturnList.button.name'/></span>
      </a>
      </Link>
       <a title="阅片" style={{float:"left",color:"#fff",height:"30px"}} onClick={e => {
           e.stopPropagation()
          window.location.href=`/${typeClass[JSON.parse(localStorage.getItem("fellowParams")).modality]}/${JSON.parse(localStorage.getItem("fellowParams")).studyUID.replace(/\./g, '-')}`}}>
                <img style={style} className='logo-image'style={{
                    cursor:"pointer",
                    userSelect:"none",
                    width:"46px",
                    verticalAlign:"middle",
                    marginRight:"10px",
                    marginLeft:"20px"}} src={require('../../../static/images/tool/followup.png')} /*onClick={() => history.push('/')}*//>
       </a>
      </span>

  )
)

export default ReturnCT
