const express = require('express')
const https = require('https')
const http = require('http')
const history = require('connect-history-api-fallback')
const proxy = require('http-proxy-middleware')
const path = require('path')
const fs = require('fs')
const ini = require('ini')
//const port = process.env.PORT || 80
//const port = process.env.PORT || 8000
let privateKey  = fs.readFileSync('/home/node/app/cert/server.key', 'utf8')
let certificate = fs.readFileSync('/home/node/app/cert/server.cert', 'utf8')
let credentials = {key: privateKey, cert: certificate}
// const port = 443
const useHttps = ini.parse(fs.readFileSync('/home/node/app/config.ini','utf8')).front_end.https || false
const app = express()

const HOST = 'flask'

//加载指定目录静态资源
const PATH = '/home/node/app/build'
app.use(express.static(PATH))

app.use(history({
  index: '/',
  verbose: true,
}))

app.get('/', function (request, response){
  response.sendFile(path.resolve(PATH, 'index.html'))
})

if (useHttps) {

  app.use(['/auth','/api'], proxy({
    ssl:credentials,
    target: `https://${HOST}:5000`,
    secure:false,
    changeOrigin: true
  }))

  let wsProxy = proxy({
    ssl:credentials,
    target: `wss://${HOST}:6000`,
    secure:false,
    changeOrigin: true,
    ws: true,
    logLevel: 'debug'})

  app.use('/socket.io', wsProxy)

  let server = https.createServer(credentials, app)

  server.listen(443, function() {
    console.log("server started on port " + 443)
  })

  // set up plain http server
  let httpServer = express()
  // set up a route to redirect http to https
  httpServer.get('*', function(req, res) {
    res.redirect('https://' + req.headers.host + req.url)
  })
  httpServer.listen(80)
  server.on('upgrade',wsProxy.upgrade)

} else {

  app.use(['/auth','/api'], proxy({
    target: `http://${HOST}:5000`,
    changeOrigin: true
  }))

  let wsProxy = proxy({
    target: `ws://${HOST}:6000`,
    changeOrigin: true,
    ws: true,
    logLevel: 'debug'})

  app.use('/socket.io', wsProxy)

  let server = app.listen(80, function () {
    console.log("server started on port " + 80)
  })
  server.on('upgrade', wsProxy.upgrade)
}
