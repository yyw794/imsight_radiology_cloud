#!/bin/python

from flask_login import UserMixin
import json
import hashlib


class User(UserMixin):
    def __init__(self, account, password):
        self.id = account
        self.account = account
        self.password = password
        self.hospital = ""
        self.contact = ""
        self.email = ""
        self.confirmEmail = False
        self.nickname = ""
        self.type = "normal"

    def to_dict(self):
        return {
            'account': self.account,
            'hospital': self.hospital,
            'contact': self.contact,
            'email': self.email,
            "nickname": self.nickname,
            'type': self.type
        }

    def to_db(self):
        return {
            "account": self.account,
            "password": self.password,
            "nickname": self.nickname,
            "hospital": self.hospital,
            "contact": self.contact,
            "email": self.email,
            "confirmEmail": self.confirmEmail,
            'type': self.type
        }

required_key = ['account', 'password']

def validateUserInfo(info):
    if type(info) is not dict:
        return None

    if not all(key in info for key in required_key):
        return None

    return info

def parseUserInfo(info):
    if type(info) is not dict:
        raise ValueError("parseUserInfo input must be dict type, while type is {}".format(type(info)))

    optional_key = ['hospital', 'contact', 'email', "nickname", "type"]
    new_user = User(info['account'], info['password'])

    for key in optional_key:
        if key in info:
            setattr(new_user, key, info[key])

    return new_user

if __name__ == '__main__':
    a = dict()
    a['account'] = 'test'
    a['password'] = 'aaa'
    a['hospital'] = 'imsight'
    a['contact'] = '123456'
    a['email'] = '123@sda.com'

    a = validateUserInfo(a)
    user = parseUserInfo(a)

    print json.dumps(user.to_dict())
