import React, { Component } from 'react';
import * as cornerstone from 'cornerstone-core'
import {EVENTS} from 'cornerstone-tools'
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';
import itemTypes from '../dragDropItemTypes'

import './index.less'

/**
 * Implements the drag source contract.
 */
const snapshotSource = {
  beginDrag(props) {
    return props.study
  }
};

/**
 * Specifies the props to inject into your component.
 */
function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

const propTypes = {
  studyUID: PropTypes.string.isRequired,

  // Injected by React DnD:
  isDragging: PropTypes.bool.isRequired,
  connectDragSource: PropTypes.func.isRequired
};

class Snapshot extends Component {

  displayImage(url, viewport) {
    cornerstone.loadImage(url)
      .then((image) => {
        cornerstone.displayImage(this.elem, image)
      })
  }

  onImageRendered = (e) => {
    if (!this.props.nodule) {
      return
    }
    const eventData = e.detail;
    const element = eventData.element
    const context = eventData.canvasContext.canvas.getContext('2d');
    context.strokeStyle = 'red';
    context.lineWidth = 1;
    context.setLineDash([5, 2])
    context.setTransform(1, 0, 0, 1, 0, 0);
    context.beginPath();
    let coord = {x: 50, y: 50}
    let size = 50
    context.rect(coord.x - size / 2, coord.y - size / 2, size, size);
    context.stroke();
  }

  componentDidMount() {
    const url = this.props.study.stacks.z.imageIds[0]
    const vp = {
      translation: {x: 0, y: 0},
      scale: 3
    }
    cornerstone.enable(this.elem)
    this.elem.addEventListener(EVENTS.IMAGE_RENDERED, this.onImageRendered)
    this.displayImage(url, vp)
  }

  render() {
    const { isDragging, connectDragSource, studyUID } = this.props;
    const study = this.props.study
    return connectDragSource(
      <div className='study-snapshot' style={{ opacity: isDragging ? 0.5 : 1 }}>
        <div ref={input => {this.elem = input}} className='cornerstone-elm'>
          <canvas className="cornerstone-canvas"
            onContextMenu={e => e.preventDefault()}/>
        </div>
        <div className='footer'>
          <p>{study.studyTime}</p>
        </div>
      </div>
    );
  }

}

export default DragSource(itemTypes.CT_STUDY, snapshotSource, collect)(Snapshot);
