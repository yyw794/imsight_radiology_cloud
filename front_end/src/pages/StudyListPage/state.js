import Immutable from 'immutable'
let state = Immutable.Map()

export function saveState(json) {
  // state = Immutable.fromJS(json)
  state = state.merge(json)
}

export function getState() {
  return state.toJS()
}

export function clearState() {
  state = state.clear()
}
