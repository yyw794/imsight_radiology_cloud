import React, {Component} from 'react';
import {withRouter} from 'react-router'
import './index.less'
import {Menu, Dropdown, Icon, Form, InputNumber, Modal, Button} from 'antd'
import {FormattedMessage, injectIntl} from 'react-intl'
import {GetLocaledText} from '../../localedFuncs'
/**
 * antd-design组件
 * @type {FormItem}
 */
const FormItem = Form.Item
/**
 * ct工具 button组件
 * @param props
 * @returns {boolean}
 * @constructor
 */
const ImageButton = (props) => {
    let style = {
        width: "3rem",
        height: "3rem",
        position: "relative",
        // backgroundImage: `url(${props.bgImg})`
    }
    if (props.active) style.backgroundColor = '#1890ff'
    return (
        <a href="javaScript:void(0)" title={props.word}>
            <div className='image-button'
                 style={style}
                 onClick={props.onClick}
                 onMouseEnter={props.onMouseEnter}
                 onMouseLeave={props.onMouseLeave}>
                {
                    typeof (props.bgImg) === 'string' ?
                        <img src={props.bgImg} />
                        :
                        ""
                }
                {
                    props.text.constructor.name === 'String' ?//should be 'string'?---------------------------------------------------------
                        <span>{props.text}</span>
                        :
                        props.text
                }
            </div>
        </a>
    )
}

/**
 * 校验工具
 * @param fieldsError
 * @returns {boolean}
 */
function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

/**
 *WWWC form表单组件
 * @param props
 * @returns {boolean}
 * @constructor
 */
let WWWCForm = (props) => {
    const {getFieldDecorator, getFieldsError, setFieldsValue} = props.form;
    return (
        <Form layout="inline" onSubmit={(e) => {
            e.preventDefault()
            props.form.validateFields((err, values) => {
                if (!err) {
                    console.log('Received values of form: ', values);
                    props.onSubmit(values)
                }
            });
        }}>
            <FormItem
                label={GetLocaledText({props}, "CTToolBar.window-width")}
            >
                {getFieldDecorator('ww', {
                    rules: [{required: true, message: GetLocaledText({props}, "CTToolBar.window-width-input")}],
                    initialValue: Math.round(1200)
                })(
                    <InputNumber placeholder={GetLocaledText({props}, "CTToolBar.window-width")}
                                 max={6000} min={-6000} step={100}/>
                )}
            </FormItem>
            <FormItem
                label={GetLocaledText({props}, "CTToolBar.window-center")}
            >
                {getFieldDecorator('wc', {
                    rules: [{required: true, message: GetLocaledText({props}, "CTToolBar.window-center-input")}],
                    initialValue: Math.round(-600)
                })(
                    <InputNumber placeholder={GetLocaledText({props}, "CTToolBar.window-center")}
                                 max={6000} min={-6000} step={100}/>
                )}
            </FormItem>
            <FormItem>
                <Button
                    type="primary"
                    style={{marginRight: 10}}
                    onClick={() => setFieldsValue({ww: Math.round(1200), wc: Math.round(-600)})}
                >
                    <FormattedMessage id='default'/>
                </Button>
                <Button
                    type="primary"
                    htmlType="submit"
                    disabled={hasErrors(getFieldsError())}
                >
                    <FormattedMessage id='confirm'/>
                </Button>
            </FormItem>
        </Form>
    )
}

WWWCForm = injectIntl(WWWCForm)

WWWCForm = Form.create()(WWWCForm);

/**
 * CT工具组件
 */
class ToolBar extends Component {
    /**
     * 参照生命周期函数
     * @param props
     */
    constructor(props) {
        super(props)
        /**
         *
         * @type {{
         * showModalWWWC: boolean,
         * WWWCMenuVisible: boolean,
         * gridMenuVisible: boolean,
         * detectionList: null}}
         */
        this.state = {
            showModalWWWC: false,
            WWWCMenuVisible: false,
            gridMenuVisible: false,
            detectionList: null
        }
        this.onKeyDown=this.onKeyDown.bind(this);
    }
    componentDidMount(){
        document.addEventListener('keydown', this.onKeyDown, false)
    }
    componentWillUnmount() {
        document.removeEventListener('keydown', this.onKeyDown)
    }

    onKeyDown(e){
      if(e.key=='6'){
          this.setState({showModalWWWC: true})
      }
    }
    /**
     * 布局切换
     * @returns {Object}
     */
    renderGridMenu() {
        let explorer = navigator.userAgent;
        var style = "";
        if (explorer.indexOf("Firefox") >= 0) {
            style = {
                top: "42px",
                width: "2.5rem",
                left: " -20px"
            }
        } else if (!!window.ActiveXObject || "ActiveXObject" in window) {
            style = {
                top: "30px",
                width: "2.5rem",
                left: " -20px"
            }
        } else {
            style = {
                width: "2.5rem",
                top: "29px",
            }
        }
        return (
            <Menu className="grid-menu" style={style}>
                <Menu.Item>
                    <a target="_blank"
                       rel="noopener noreferrer"
                       onClick={(e) => {
                           e.stopPropagation()
                           this.props.onToolClick('grid', 1)
                       }}>1</a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank"
                       rel="noopener noreferrer"
                       onClick={(e) => {
                           e.stopPropagation()
                           this.props.onToolClick('grid', 2)
                       }}>2</a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank"
                       rel="noopener noreferrer"
                       onClick={(e) => {
                           e.stopPropagation()
                           this.props.onToolClick('grid', 3)
                       }}>3</a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank"
                       rel="noopener noreferrer"
                       onClick={(e) => {
                           e.stopPropagation()
                           this.props.onToolClick('grid', 4)
                       }}>4</a>
                </Menu.Item>
            </Menu>
        )
    }

    /**
     * 调窗菜单
     * @returns {boolean}
     */
    renderWWWCMenu() {
        let explorer = navigator.userAgent;
        var style = "";
        if (explorer.indexOf("Firefox") >= 0) {
            style = {
                top: "42px",
                width: "9rem",
                left: " -25px"
            }
        } else if (!!window.ActiveXObject || "ActiveXObject" in window) {
            style = {
                width: "9rem",
                top: "29px",
                left: "-25px",
            }
        } else {
            style = {
                width: "9rem",
                top: "29px",

            }
        }
        return (
            <Menu className="wwwc-menu" style={style}>
                <Menu.Item>
                      <a target="_blank"
                       style={{overflow:"hidden"}}
                       rel="noopener noreferrer"
                       onClick={(e) => {
                           this.props.onToolClick('wwwc',{wc: -550, ww: 1500})
                       }}><FormattedMessage id='CTToolBar.lung-window'  style={{float:"left"}} /><span className="short-cut" style={{float:"right"}}>1</span></a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank"
                       style={{overflow:"hidden"}}
                       rel="noopener noreferrer"
                       onClick={(e) => {
                           this.props.onToolClick('wwwc',{wc: 60, ww: 400})
                       }}><FormattedMessage id='CTToolBar.abdomen-window' style={{float:"left"}} /><span className="short-cut" style={{float:"right"}}>2</span></a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank"
                       rel="noopener noreferrer"
                       style={{overflow:"hidden"}}
                       onClick={(e) => {
                           this.props.onToolClick('wwwc',{wc: 300, ww: 1500})
                       }}><FormattedMessage id='CTToolBar.bone-window' style={{float:"left"}}/><span className="short-cut" style={{float:"right"}}>3</span></a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank"
                       rel="noopener noreferrer"
                        style={{overflow:"hidden"}}
                       onClick={(e) => {
                           this.props.onToolClick('wwwc',{wc: 45, ww: 300})
                       }}><FormattedMessage id='CTToolBar.spine-window' style={{float:"left"}}/><span className="short-cut" style={{float:"right"}}>4</span></a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank"
                       rel="noopener noreferrer"
                       style={{overflow:"hidden"}}
                       onClick={(e) => {
                           this.props.onToolClick('wwwc',{wc: 40, ww: 80})
                       }}><FormattedMessage id='CTToolBar.brain-window' style={{float:"left"}}/><span className="short-cut" style={{float:"right"}}>5</span></a>
                </Menu.Item>
                <Menu.Item>
                    <a target="_blank"
                       rel="noopener noreferrer"
                       style={{overflow:"hidden"}}
                       onClick={(e) => {
                           this.setState({showModalWWWC: true})
                       }}><FormattedMessage id='CTToolBar.mannul-input' style={{float:"left"}}/><span className="short-cut" style={{float:"right"}}>6</span></a>
                </Menu.Item>
            </Menu>
        )
    }

    /**
     * tool Component
     * @param name
     * @returns {Object}
     */
    renderButton = (name) => {
        const menu = this.renderWWWCMenu()
        if (name === 'wwwc') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/tool/contrast.png')}
                    names={name}
                    active={this.props.activeTool.includes('wwwc')}
                    onMouseEnter={() => this.setState({WWWCMenuVisible: true})}
                    onMouseLeave={() => this.setState({WWWCMenuVisible: false})}
                    word={GetLocaledText(this, "CTToolBar.wwwc")}
                    text={
                        <Dropdown overlay={menu} trigger={[]} visible={this.state.WWWCMenuVisible}>
                            <span></span>
                        </Dropdown>
                    }/>
            )
        } else if (name === 'referenceLines') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/tool/coordinate.png')}
                    active={this.props.activeTool.includes('referenceLines')}
                     names={name}
                    word={GetLocaledText(this, "CTToolBar.referenceLines")}
                    text=""
                    onClick={() => this.props.onToolClick('referenceLines')}/>
            )
        } else if (name === 'viwerSync') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/tool/connect.png')}
                    names={name}
                    word={GetLocaledText(this, "CTToolBar.viwerSync")}
                    active={this.props.activeTool.includes('viwerSync')}
                    text=""
                    onClick={() => this.props.onToolClick('viwerSync')}/>
            )
        } else if (name === 'grid') {
            // return (
            //     <ImageButton
            //         bgImg={require('../../../static/images/gridNew.png')}
            //         active={this.props.activeTool.includes('grid')}
            //         onMouseEnter={() => this.setState({gridMenuVisible: true})}
            //         onMouseLeave={() => this.setState({gridMenuVisible: false})}
            //         word={GetLocaledText(this, "CTToolBar.layout")}
            //         text={
            //             <Dropdown overlay={this.renderGridMenu()} trigger={[]} visible={this.state.gridMenuVisible}>
            //                 <span></span>
            //             </Dropdown>
            //         }
            //         onClick={() => this.props.onToolClick('grid')}/>
            // )
        } else if (name === 'report') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/report_nor.png')}
                    names={name}
                    active={this.props.activeTool.includes('report')}
                    word={GetLocaledText(this, "CTToolBar.report")}
                    text=""
                    onClick={() => this.props.onToolClick('report')}/>
            )
        } else if (name === 'detect') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/detect.png')}
                    active={this.props.activeTool.includes('detect')}
                    names={name}
                    word={GetLocaledText(this, "CTToolBar.detect")}
                    text=""
                    onClick={() => this.props.onToolClick('detect')}
                />
            )
        } else if (name === 'hideBoxes') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/tool/hidden.png')}
                    active={this.props.activeTool.includes('hideBoxes')}
                    names={name}
                    word={GetLocaledText(this, "CTToolBar.lesion.hideBoxes")}
                    text=""
                    onClick={() => this.props.onToolClick('hideBoxes')}
                />
            )
        } else if (name === 'length') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/tool/ruler.png')}
                    word={GetLocaledText(this, "DRViewer.ToolBar.length")}
                    active={this.props.activeTool.includes('length')}
                    text=""
                    onClick={() => this.props.onToolClick('length')}/>
            )
        } else if (name === 'angle') {
            return (
                <ImageButton
                    names={name}
                    bgImg={require('../../../static/images/tool/angle.png')}
                    active={this.props.activeTool.includes('angle')}
                    word={GetLocaledText(this, "DRViewer.ToolBar.angle")}
                    text=""
                    onClick={() => this.props.onToolClick('angle')}/>
            )
        } else if (name === 'freehand') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/tool/polygon.png')}
                    active={this.props.activeTool.includes('freehand')}
                    word={GetLocaledText(this, "DRViewer.ToolBar.freehand")}
                    text=""
                    onClick={() => this.props.onToolClick('freehand')}/>
            )
        } else if (name === 'eraser') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/tool/delete.png')}
                    names={name}
                    active={this.props.activeTool.includes('eraser')}
                    word={GetLocaledText(this, "DRViewer.ToolBar.eraser")}
                    text=""
                    onClick={() => this.props.onToolClick('eraser')}/>
            )
        } else if (name === 'MPR') {
            return (
                <ImageButton
                    bgImg={require('../../../static/images/tool/layout.png')}
                    active={this.props.activeTool.includes('MPR')}
                    names={name}
                    word={GetLocaledText(this, "DRViewer.ToolBar.MPR")}
                    text=""
                    onClick={() => this.props.onToolClick('MPR')}/>
            )
        }
    }

    /**
     * 参照生命周期函数
     * @returns {Component}
     */
    render() {
        return (
            <div className='ct-viewer-toolbar' style={{padding: "0px", overflow: "hidden", height: "3rem"}}>
                <div className='tool-button-wrapper'
                     style={{background: "#001529", paddingLeft: "40px", height: "3rem"}}>
                    {this.props.tools.map(this.renderButton)}
                </div>
                <Modal
                    title={GetLocaledText(this, "CTToolBar.wwwc")}
                    visible={this.state.showModalWWWC}
                    onOk={this.handleOk}
                    onCancel={() => this.setState({showModalWWWC: false})}
                    footer={null}
                >
                    <WWWCForm onSubmit={values => {
                        values.ww = values.ww
                        values.wc = values.wc
                        this.props.onToolClick('wwwc', values)
                        this.setState({showModalWWWC: false})
                    }}/>
                </Modal>
            </div>
        );
    }

}

export default injectIntl(ToolBar);
