#!/usr/bin/python
# -*- coding:utf-8 -*-

'''
    OBS related api
'''

from com.obs.client.obs_client import ObsClient
import sys
AK = 'GEAFUMMISIBRYLW0EUES'
SK = 'SH6ImPlf3Ib3TkMUPF3g0Y6Bq4jEt22xRBZAu3c7'
server = 'obs.cn-north-1.myhwclouds.com'
bucketName = 'imsight-cloud'


# Constructs a obs client instance with your account for accessing OBS
obsClient = ObsClient(access_key_id=AK, secret_access_key=SK, server=server)


def downloadFile(objectKey, downloadPath):
    resp = obsClient.getObject(bucketName, objectKey, downloadPath=downloadPath)
    if resp.status < 300:
        print('requestId:', resp.requestId)
        print('url:', resp.body.url) 
    else:
        print(resp.errorCode)


def uploadFile(objectKey, uploadPath):
    resp = obsClient.putFile(bucketName, objectKey, file_path=uploadPath)
    if resp.status < 300:
        print('requestId:', resp.requestId)
        print('url:', resp.body.url) 
    else:
        print(resp.errorCode)        

        
if __name__ == '__main__':
    downloadFile(sys.argv[1], 'data/' + sys.argv[1])
