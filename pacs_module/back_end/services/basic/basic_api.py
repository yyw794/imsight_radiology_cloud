#!/bin/python
#coding=utf-8

from socketIO_client import SocketIO, LoggingNamespace
from config import SOCKET_PORT, SOCKET_PROTOCOL
import logging
logger = logging.getLogger()
socketIO_ = None


def socket_emit(key, value):
    global socketIO_

    if socketIO_ is None:
        if SOCKET_PROTOCOL == 'https':
            socketIO_ = SocketIO('https://localhost', 6000, LoggingNamespace, verify=False)
        else:
            socketIO_ = SocketIO('http://localhost', 6000, LoggingNamespace)
    logger.info('socket_emit:{} {}'.format(key,value))
    socketIO_.emit(key, value)

