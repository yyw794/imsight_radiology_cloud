import api from './api'
import Cookies from 'js-cookie'
import {isFunc} from './util'
import {clearState} from './pages/StudyListPage/state'

/**
 * 判断是否含有cookies
 * @returns {boolean}
 */
export function isAuthenticated() {
  return !!Cookies.get('currentUser')
}

/**
 * 登陆验证设置Cookies
 * @param user 登陆所需的user对象
 * @param onSuccess 成功回调
 * @param onFail 失败回调
 * @returns {*|Promise<T | never>|void}
 */
export function authenticate(user, onSuccess, onFail) {
 api.getVersion().then(function (response) {
    localStorage.setItem("version",response.version)
 },function (res) {
     console.log("getVersion",res)
 })
  return api.login(user)
  .then(data => {
    Cookies.set('currentUser', data, { expires: 7 })
    clearState()
    isFunc(onSuccess) && onSuccess()
  })
  .catch(err => {
    isFunc(onFail) && onFail(err)
  })
}

/**
 * 退出登录
 * @param cb
 */
export function signout(cb) {
  api.logout()
  .then(response => {
    Cookies.remove('currentUser')
    isFunc(cb) && cb()
  })
  .catch((err) => {
    console.log(err);
  })
}

/**
 * 获取Cookies中的当前登陆用户信息
 * @returns {any}
 */
export function getUser() {
  try {
    return JSON.parse(Cookies.get('currentUser'))
  } catch (e) {
    return  false;
  }
}

/**
 * 获取用户权限信息
 * @param privilege
 * @returns {string}
 */
export function getUserRole(privilege) {
  let user = getUser();
  if (user.type === 'label' && privilege.includes('reviewer')) {
    return 'reviewer'
  }
  if (user.type === 'label' && privilege.includes('label')) {
    return 'labeler'
  }
  return 'user'
}
