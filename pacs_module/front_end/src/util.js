export function isFunc(func) {
  return typeof func === 'function'
}

export function mapObject(obj, func) {
  let obj_new = {};
  for (let key in obj) {
    if (!obj.hasOwnProperty(key)) continue;
    obj_new[key] = func(obj[key]);
  }
  return obj_new;
}

export function deepCopy(jsonObj) {
  return JSON.parse(JSON.stringify(jsonObj))
}

export function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}

// scale wwwc value from 0~255 to -1022 ~ 2817
export function scaleWC(value) {
  let ratio = (value - 0) / 255
  return 3839 * ratio - 1022
}

export function scaleWCReverse(value) {
  let ratio = (value + 1022) / 3839
  return 255 * ratio
}

export function scaleWW(value) {
  let ratio = (value - 0) / 255
  return 3839 * ratio
}

export function scaleWWReverse(value) {
  let ratio = value / 3839
  return 255 * ratio
}

//scale ww wc value from 0~255 to min~max
export function scaleWCDynamic(value,min,max){
  let ratio = (value -0) /255
  return (max- min) * ratio + min
}

export function scaleWCReverseDynamic(value,min,max){
  let ratio = (value - min)/(max-min)
  return 255 * ratio
}

export function scaleWWDynamic(value,min,max){
  let ratio = (value - 0) / 255
  return (max - min)*ratio
}

export function scaleWWReverseDynamic(value,min,max){
  let ratio = value / (max -min)
  return 255 * ratio
}

export function afterFrames(n) {
  return new Promise(function(resolve, reject) {
    let next = () => {
      n--
      if (n <= 0) {
        resolve()
      } else {
        window.requestAnimationFrame(next)
      }
    }
    next()
  })
}

export function clip(value, min, max) {
  return Math.min(Math.max(value, min), max)
}


/**
 * Loops trough all promises and continues even if a promise was rejected
 *
 * @param {Array} promises an array of promises 
 * @returns {Object} with errors and results (example usage after function definition)
 */
export function executeAllPromises(promises) {
  // Wrap all Promises in a Promise that will always "resolve"
  var resolvingPromises = promises.map(function(promise) {
    return new Promise(function(resolve) {
      var payload = new Array(2);
      promise.then(function(result) {
          payload[0] = result;
        })
        .catch(function(error) {
          payload[1] = error;
        })
        .then(function() {
          /* 
           * The wrapped Promise returns an array:
           * The first position in the array holds the result (if any)
           * The second position in the array holds the error (if any)
           */
          resolve(payload);
        });
    });
  });

  var errors = [];
  var results = [];

  // Execute all wrapped Promises
  return Promise.all(resolvingPromises)
    .then(function(items) {
      items.forEach(function(payload) {
        if (payload[1]) {
          errors.push(payload[1]);
        } else {
          results.push(payload[0]);
        }
      });

      return {
        errors: errors,
        results: results
      };
    });
}
// example usage:
// executeAllPromises(promises).then(function(items) {
//     // Result
//     var errors = items.errors.map(function(error) {
//       return error.message
//     }).join(',');
//     var results = items.results.join(',');
    
//     console.log(`Executed all ${promises.length} Promises:`);
//     console.log(`— ${items.results.length} Promises were successful: ${results}`);
//     console.log(`— ${items.errors.length} Promises failed: ${errors}`);

//     console.log('image blobs', items.results)
//   });

// Draw arrow head
function drawHead (ctx, x0, y0, x1, y1, x2, y2, style, color, width) {
  if (typeof(x0) == 'string') {
    x0 = parseInt(x0);
  }
  if (typeof(y0) == 'string') {
    y0 = parseInt(y0);
  }
  if (typeof(x1) == 'string') {
    x1 = parseInt(x1);
  }
  if (typeof(y1) == 'string') {
    y1 = parseInt(y1);
  }
  if (typeof(x2) == 'string') {
    x2 = parseInt(x2);
  }
  if (typeof(y2) == 'string') {
    y2 = parseInt(y2);
  }
  
  var radius = 3,
      twoPI = 2 * Math.PI;
 
  ctx.save();
  ctx.beginPath();
  ctx.strokeStyle = color;
  ctx.fillStyle = color;
  ctx.lineWidth = width;
  ctx.moveTo(x0, y0);
  ctx.lineTo(x1, y1);
  ctx.lineTo(x2, y2);
  
  switch (style) {
    case 0:
      var backdist = Math.sqrt(((x2 - x0) * (x2 - x0)) + ((y2 - y0) * (y2 - y0)));
      ctx.arcTo(x1, y1, x0, y0, .55 * backdist);
      ctx.fill();
      break;
    case 1:
      ctx.beginPath();
      ctx.moveTo(x0, y0);
      ctx.lineTo(x1, y1);
      ctx.lineTo(x2, y2);
      ctx.lineTo(x0, y0);
      ctx.fill();
      break;
    case 2:
      ctx.stroke();
      break;
    case 3:
      var cpx = (x0 + x1 + x2) / 3;
      var cpy = (y0 + y1 + y2) / 3;
      ctx.quadraticCurveTo(cpx, cpy, x0, y0);
      ctx.fill();
      break;
    case 4:
      var cp1x, cp1y, cp2x, cp2y, backdist;
      var shiftamt = 5;
      if (x2 == x0) {
        backdist = y2 - y0;
        cp1x = (x1 + x0) / 2;
        cp2x = (x1 + x0) / 2;
        cp1y = y1 + backdist / shiftamt;
        cp2y = y1 - backdist / shiftamt;
      } else {
        backdist = Math.sqrt(((x2 - x0) * (x2 - x0)) + ((y2 - y0) * (y2 - y0)));
        var xback = (x0 + x2) / 2;
        var yback = (y0 + y2) / 2;
        var xmid = (xback + x1) / 2;
        var ymid = (yback + y1) / 2;
        var m = (y2 - y0) / (x2 - x0);
        var dx = (backdist / (2 * Math.sqrt(m * m + 1))) / shiftamt;
        var dy = m * dx;
        cp1x = xmid - dx;
        cp1y = ymid - dy;
        cp2x = xmid + dx;
        cp2y = ymid + dy;
      }
      ctx.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, x0, y0);
      ctx.fill();
      break;
  }
  ctx.restore();
}

// draw arrow
export function drawArrow(ctx, x1, y1, x2, y2, style, which, angle, d, color, width) {
  if (typeof(x1) == 'string') {
    x1 = parseInt(x1);
  }
  if (typeof(y1) == 'string') {
    y1 = parseInt(y1);
  }
  if (typeof(x2) == 'string') {
    x2 = parseInt(x2);
  }
  if (typeof(y2) == 'string') {
    y2 = parseInt(y2);
  }
  style = typeof(style) != 'undefined' ? style : 3;
  which = typeof(which) != 'undefined' ? which : 1;
  angle = typeof(angle) != 'undefined' ? angle : Math.PI / 9;
  d = typeof(d) != 'undefined' ? d : 10;
  color = typeof(color) != 'undefined' ? color : '#000';
  width = typeof(width) != 'undefined' ? width : 1;
  var toDrawHead = typeof(style) != 'function' ? drawHead : style;
  var dist = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
  var ratio = (dist - d / 3) / dist;
  var tox, toy, fromx, fromy;
  if (which & 1) {
    tox = Math.round(x1 + (x2 - x1) * ratio);
    toy = Math.round(y1 + (y2 - y1) * ratio);
  } else {
    tox = x2;
    toy = y2;
  }
  
  if (which & 2) {
    fromx = x1 + (x2 - x1) * (1 - ratio);
    fromy = y1 + (y2 - y1) * (1 - ratio);
  } else {
    fromx = x1;
    fromy = y1;
  }
  
  ctx.beginPath();
  ctx.strokeStyle = color;
  ctx.lineWidth = width;
  ctx.moveTo(fromx, fromy);
  ctx.lineTo(tox, toy);
  ctx.stroke();
  
  var lineangle = Math.atan2(y2 - y1, x2 - x1);
  var h = Math.abs(d / Math.cos(angle));
  if (which & 1) {
    var angle1 = lineangle + Math.PI + angle;
    var topx = x2 + Math.cos(angle1) * h;
    var topy = y2 + Math.sin(angle1) * h;
    var angle2 = lineangle + Math.PI - angle;
    var botx = x2 + Math.cos(angle2) * h;
    var boty = y2 + Math.sin(angle2) * h;
    toDrawHead(ctx, topx, topy, x2, y2, botx, boty, style, color, width);
    topx-50, topy-10, x2, y2, botx-50, boty+10
  }
  
  if (which & 2) {
    var angle1 = lineangle + angle;
    var topx = x1 + Math.cos(angle1) * h;
    var topy = y1 + Math.sin(angle1) * h;
    var angle2 = lineangle - angle;
    var botx = x1 + Math.cos(angle2) * h;
    var boty = y1 + Math.sin(angle2) * h;
    toDrawHead(ctx, topx, topy, x1, y1, botx, boty, style, color, width);
  }
}

export function calcLiradsScore(isAPE, majorFeatures, diam) {
  if (!isAPE) {
    if (majorFeatures == 0) {
      if (diam < 20) {
        return 'LR-3'
      } 
    }
  }  
  if (!isAPE) {
    if (majorFeatures == 0) {
      if (diam >= 20) {
        return 'LR-3'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures == 0) {
      if (diam < 10) {
        return 'LR-3'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures == 0) {
      if (diam >= 10 && diam <= 19) {
        return 'LR-3'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures == 0) {
      if (diam >= 20) {
        return 'LR-4'
      } 
    }
  }
  if (!isAPE) {
    if (majorFeatures == 1) {
      if (diam < 20) {
        return 'LR-3'
      } 
    }
  }
  if (!isAPE) {
    if (majorFeatures == 1) {
      if (diam >= 20) {
        return 'LR-4'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures == 1) {
      if (diam < 10) {
        return 'LR-4'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures == 1) {
      if (diam >= 10 && diam <= 19) {
        return 'LR-4'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures == 1) {
      if (diam >= 20) {
        return 'LR-5'
      } 
    }
  }
  if (!isAPE) {
    if (majorFeatures >= 2) {
      if (diam < 20) {
        return 'LR-4'
      } 
    }
  }
  if (!isAPE) {
    if (majorFeatures >= 2) {
      if (diam >= 20) {
        return 'LR-4'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures >= 2) {
      if (diam < 10) {
        return 'LR-4'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures >= 2) {
      if (diam >= 10 && diam <= 19) {
        return 'LR-5'
      } 
    }
  }
  if (isAPE) {
    if (majorFeatures >= 2) {
      if (diam >= 20) {
        return 'LR-5'
      } 
    }
  }
}