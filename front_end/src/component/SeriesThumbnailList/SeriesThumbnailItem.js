import React, { Component } from 'react';
import _ from 'underscore'
import {Icon} from 'antd'
import classNames from 'classnames'
import './SeriesThumbnailItem.less'
import {VIEW_POSITION} from '../../data'

/**
 * ct缩略图组件
 */
class SeriesThumbnailItem extends Component {
    /**
     * 单个缩略图渲染
     * @returns {Component}
     */
    render() {

        let liClasses = classNames({
            'active': this.props.iter === this.props.currentIdx ? true : false,
            'seriesThumbnailItem': true
        });

        return (
            <li
                onClick={() => {this.props.onItemClick(this.props.image.instanceID)}}
                className={liClasses}>
                <img src={this.props.image.url}/>
                <p className='info'>{this.props.image.viewPosition}</p>
            </li>
        )
    }
}

export default SeriesThumbnailItem;
