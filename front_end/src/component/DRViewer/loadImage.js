import * as cornerstone from 'cornerstone-core'

/**
 *
 * @private {Map<any, any>}
 */
let cache = new Map()
/**
 * DR 加载图片
 * @param imageId
 * @returns {*}
 */
export default function loadImage(imageId) {
  if (cache.has(imageId)) {
    return Promise.resolve(cache.get(imageId))
  }
  return cornerstone.loadImage(imageId).then(image => {
    cache.set(imageId, image)
    return image
  })
}
