#!/bin/python
# coding=utf-8

class Tool():
    def __init__(self, studyUID, seriesID, instanceID, data):
        self.studyUID = studyUID
        self.seriesID = seriesID
        self.instanceID = instanceID
        self.data = data

    def to_dict(self):
        return {
            "seriesID": self.seriesID,
            "instanceID": self.instanceID,
            "data": self.data
        }


required_key_db = ['userID', 'studyUID', 'tools']
required_key_tool = ['seriesID', 'instanceID', 'data']


def parseToolInfo(info):
    if type(info) is not dict:
        raise ValueError("parseToolInfo input must be dict type, while type is {}".format(type(info)))

    if all(key in info for key in required_key_db):
        studyUID = info['studyUID']
        tools = info['tools']
        toollist = []

        for tool in tools:
            if all(key in tool for key in required_key_tool):
                seriesID = tool['seriesID']
                instanceID = tool['instanceID']
                data = tool['data']
                new_tool = Tool(studyUID, seriesID, instanceID, data)
                toollist.append(new_tool)
            else:
                raise ValueError("parseToolInfo key mismatch", tool.keys(), required_key_tool)
        return toollist

    raise ValueError("parseToolInfo key mismatch", info.keys(), required_key_db)


def saveToollist(toolist, studyUID, userID):
    if type(toolist) is not list:
        raise ValueError("saveToollist input must be list type, while type is {}".format(type(toolist)))

    ret = {'studyUID': studyUID, 'userID': userID, 'tools': []}
    for tool in toolist:
        if isinstance(tool, Tool):
            ret['tools'].append(tool.to_dict())
        else:
            raise ValueError("tool is NOT Tool instance {}".format(type(tool)))
    return ret
