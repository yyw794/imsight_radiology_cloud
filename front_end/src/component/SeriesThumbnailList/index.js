import React, { Component } from 'react';
import _ from 'underscore'
import {Icon} from 'antd'
import './index.less'
import {VIEW_POSITION} from '../../data'
import SeriesThumbnailItem from './SeriesThumbnailItem'

/**
 * 缩略图List
 */
class SeriesThumbnailList extends Component {
    /**
     * 参照react生命周期函数
     */
  componentDidMount() {
    console.log('printing images', this.props.images);
  }

    /**
     * @type {{
     * currentIdx: *, 当前选中的缩略图id
     * collapsed: boolean 是否折叠
     * }}
     */
  state = {
    currentIdx: this.props.defaultIdx,
    collapsed: false
  }
    /**
     * 缩略图点击事件
     * @param id
     */
  onItemClick = (id) => {
    const currentIdx = _.findIndex(this.props.images, img => img.instanceID === id)
    if (currentIdx !== this.state.currentIdx) {
      _.isFunction(this.props.onChange) && this.props.onChange(this.props.images[currentIdx], currentIdx)
      this.setState({
        currentIdx
      })
    }
  }

    /**
     * 折叠切换事件
     */
  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

    /**
     * 组件渲染
     * @returns {Component}
     */
  render() {
    return (
      <div className={this.state.collapsed ? 'thumbnail-list-container collapsed' : 'thumbnail-list-container'}>
        <ul className='thumbnail-list'>
          {
            this.props.images.map((img, i) => (
              <SeriesThumbnailItem
              key={img.instanceID}
              iter={i}
              image={img} 
              currentIdx={this.state.currentIdx}
              onItemClick={this.onItemClick}
              />
            ))
          }
        </ul>
        <div className='toggle-collapsed horizontal' onClick={this.toggleCollapsed}>
          {
            this.state.collapsed ?
            <Icon type="left" />
            :
            <Icon type="right" />
          }
        </div>
        <div className='toggle-collapsed vertical' onClick={this.toggleCollapsed}>
          {
            this.state.collapsed ?
            <Icon type="up" />
            :
            <Icon type="down" />
          }
        </div>
      </div>
    );
  }

}

export default SeriesThumbnailList;
