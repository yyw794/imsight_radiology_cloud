#!/bin/python
#coding=utf-8
import SimpleITK as sitk
import numpy as np

import datetime, re, os, shutil, time, sys

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from services.file.x_ray_fileOp import insert_x_ray_instances
from services.file.ct_lung_fileOp import insert_CT_instances
from config import DICOM_ROOT, DR_FILTER, LUNG_FILTER
import logging
logger = logging.getLogger()
from services.file.filter import get_body_part_examined,get_DX_PA_ViewPosition
from services.pacs.pacs_api import get_patientInfo

def parse_meta(filepath):
    try:
        #logger.info('parse image:' + filepath)
        image = sitk.ReadImage(filepath)
        all_keys = image.GetMetaDataKeys()
    except:
        logger.error('get meta data fail')
        os.remove(filepath)
        return None

    direction  = list(image.GetDirection())
    origin     = list(image.GetOrigin())
    size       = list(image.GetSize())
    spacing    = list(image.GetSpacing())

    modality     = '' if '0008|0060' not in all_keys else image.GetMetaData('0008|0060').rstrip()
    studyUID     = '' if '0020|000d' not in all_keys else image.GetMetaData('0020|000d').rstrip()
    seriesNumber = '' if '0020|0011' not in all_keys else image.GetMetaData('0020|0011').rstrip()
    seriesID     = '' if '0020|000e' not in all_keys else image.GetMetaData('0020|000e').rstrip()
    instanceID   = '' if '0008|0018' not in all_keys else image.GetMetaData('0008|0018').rstrip()
    instanceNumber = '' if '0020|0013' not in all_keys else image.GetMetaData('0020|0013').rstrip()
    studyDate        = '' if '0008|0020' not in all_keys else image.GetMetaData('0008|0020').rstrip()
    patientID        = '' if '0010|0020' not in all_keys else image.GetMetaData('0010|0020').rstrip()
    patientName      = '' if '0010|0010' not in all_keys else image.GetMetaData('0010|0010').rstrip()
    bodyPartExamined = '' if '0018|0015' not in all_keys else image.GetMetaData('0018|0015').rstrip()
    studyDescription = '' if '0008|1030' not in all_keys else image.GetMetaData('0008|1030').rstrip()
    studyTime        = '' if '0008|0030' not in all_keys else image.GetMetaData('0008|0030').rsplit('.',1)[0]
    gender           = '' if '0010|0040' not in all_keys else image.GetMetaData('0010|0040').rstrip()
    seriesDescription = '' if '0008|103E' not in all_keys else image.GetMetaData('0008|103E').rstrip().upper()
    protocolName = '' if '0018|1030' not in all_keys else image.GetMetaData('0018|1030').rstrip().upper()
    imagecomments = '' if '0020|4000' not in all_keys else image.GetMetaData('0020|4000').rstrip().upper()
    
    chest_re = re.compile(LUNG_FILTER)
    #if 'CHEST' in bodyPartExamined or (bodyPartExamined == '' and (chest_re.search(protocolName) or chest_re.search(seriesDescription))):
        #bodyPartExamined = 'CHEST'
    #elif bodyPartExamined == '':
        #bodyPartExamined = protocolName if(protocolName != '') else seriesDescription

    bodyPartExamined = get_body_part_examined(bodyPartExamined,protocolName,seriesDescription,studyDescription,imagecomments)

    if studyUID == '' or instanceID == '' or modality == '' or seriesID == '':
        logger.error('key meta missing, pass file:{}'.format(filepath))
        os.remove(filepath)
        return None
    if gender != '':
        gender = 'MALE' if gender == 'M' else 'FEMALE'

    birthDate        = '' if '0010|0030' not in all_keys else image.GetMetaData('0010|0030').rstrip()
    hospital         = '' if '0008|0080' not in all_keys else image.GetMetaData('0008|0080').decode('GB18030').rstrip()
    WindowCenter     = '2048' if '0028|1050' not in all_keys else image.GetMetaData('0028|1050').rstrip()
    WindowWidth      = '4096' if '0028|1051' not in all_keys else image.GetMetaData('0028|1051').rstrip()
    ImagePositionPatient    = '' if '0020|0032' not in all_keys else image.GetMetaData('0020|0032').rstrip()
    ImageOrientationPatient = '' if '0020|0037' not in all_keys else image.GetMetaData('0020|0037').rstrip()
    PixelSpacing            = '1.0\\1.0' if '0028|0030' not in all_keys else image.GetMetaData('0028|0030').rstrip()
    SliceThickness          = '' if '0018|0050' not in all_keys else image.GetMetaData('0018|0050').rstrip()
    SpacingBetweenSlices    = '' if '0018|0088' not in all_keys else image.GetMetaData('0018|0088').rstrip()
    SmallestImagePixelValue = '' if '0028|0106' not in all_keys else image.GetMetaData('0028|0106').rstrip()
    LargestImagePixelValue  = '' if '0028|0107' not in all_keys else image.GetMetaData('0028|0107').rstrip()
    viewPosition            = '' if '0018|5101' not in all_keys else image.GetMetaData('0018|5101').rstrip()
    LUT = '' if '2050|0020' not in all_keys else image.GetMetaData('2050|0020').rstrip()
    patientOrientation = '' if '0020|0020' not in all_keys else image.GetMetaData('0020|0020').rstrip()
    #viewPosition_re = re.compile(DR_FILTER)
    #if 'PA' in viewPosition or viewPosition_re.search(viewPosition):
        #viewPosition = 'PA'
    
    viewPosition = get_DX_PA_ViewPosition(viewPosition,patientOrientation,protocolName,seriesDescription,studyDescription)
    try:
        studyDate = datetime.datetime.strptime(studyDate+studyTime.split('.')[0], "%Y%m%d%H%M%S")
    except:
        studyDate = datetime.datetime.today()

    try:
        birthDate = datetime.datetime.strptime(birthDate, "%Y%m%d")
    except:
        birthDate = datetime.datetime.today()

    try:
        patientName = patientName.decode('utf-8','ignore').encode("utf-8")
    except:
        patientName = ""

    try:
        bodyPartExamined = bodyPartExamined.decode('utf-8','ignore').encode("utf-8")
    except:
        bodyPartExamined = ""

    try:
        gender = gender.decode('utf-8','ignore').encode("utf-8")
    except:
        gender = "MALE"

    try:
        hospital = hospital.decode('utf-8','ignore').encode("utf-8")
    except:
        hospital = ""

    return {
        'studyUID': studyUID,
        'seriesID': seriesID,
        'instanceID': instanceID,
        'studyDate': studyDate,
        'birthDate': birthDate,
        'uploadDate': datetime.datetime.today(),
        'patientID': patientID,
        'patientName': patientName,
        'modality': modality,
        'bodyPartExamined': bodyPartExamined,
        'gender': gender,
        'hospital': hospital,
        'LUT': LUT,
        'viewPosition': viewPosition,
        'columnPixelSpacing': spacing[1],
        "rowPixelSpacing": spacing[0],
        "minPixelValue": 0,
        "maxPixelValue": 0,
        'seriesNumber':seriesNumber
    }

def group_by_series(images):
    group_data = {}
    for image in images:
        seriesID = image['seriesID']
        if seriesID in group_data:
            group_data[seriesID].append(image)
        else:
            group_data[seriesID] = [image]
    series = []
    minPixelValue = 99999
    maxPixelValue = -99999
    for group in group_data.values():
        instanceID_list = []
        new_series = {}
        new_series['seriesID'] = group[0]['seriesID']
        new_series['columnPixelSpacing'] = group[0]['columnPixelSpacing']
        new_series['rowPixelSpacing'] = group[0]['rowPixelSpacing']
        new_series['instances'] = []
        for image in group:
            if image['instanceID'] in instanceID_list:
                logger.info('repeat image {} in seriesID {}'.format(image['filepath'], new_series['seriesID']))
                if os.path.exists(image['filepath']):
                    os.remove(image['filepath'])
                else:
                    logger.info('file not exist {}'.format(image['filepath']))
            else:
                new_instance = {}
                new_instance['instanceID'] = image['instanceID']
                new_instance['url'] = ''
                # should pop finally
                new_instance['filepath'] = image['filepath']
                new_instance['filename'] = image['filename']
                if image['modality'] in ['DR', 'DX', "CR"]:
                    new_instance['viewPosition'] = image['viewPosition']

                new_series['instances'].append(new_instance)
                instanceID_list.append(new_instance['instanceID'])
                minPixelValue = image['minPixelValue'] if image['minPixelValue'] < minPixelValue else minPixelValue
                maxPixelValue = image['maxPixelValue'] if image['maxPixelValue'] > maxPixelValue else maxPixelValue
        new_series['maxPixelValue'] = int(maxPixelValue)
        new_series['minPixelValue'] = int(minPixelValue)
        new_series['seriesNumber'] = group[0]['seriesNumber']
        series.append(new_series)
    return series


def group_by_study(images):
    group_data = {}
    for image in images:
        studyUID = image['studyUID']
        if studyUID in group_data:
            group_data[studyUID].append(image)
        else:
            group_data[studyUID] = [image]
    studies = []
    for group in group_data.values():
        patient_name = group[0]['patientName']
        patient_info = get_patientInfo(group[0]['studyDate'].strftime('%Y%m%d'),group[0]['studyUID'])
        if patient_info != None: patient_name = patient_info['patientName']
        
        new_study = {}
        new_study['birthDate'] = group[0]['birthDate']
        new_study['studyDate'] = group[0]['studyDate']
        new_study['uploadDate'] = group[0]['uploadDate']
        new_study['bodyPartExamined'] = group[0]['bodyPartExamined']
        new_study['gender'] = group[0]['gender']
        new_study['hospital'] = group[0]['hospital']
        new_study['modality'] = group[0]['modality']
        new_study['patientID'] = group[0]['patientID']
        new_study['patientName'] = patient_name
        new_study['result'] = ""
        new_study['state'] = "meta_only"
        new_study['studyUID'] = group[0]['studyUID']
        new_study['series'] = group_by_series(group)

        if new_study['modality'] in ['DX', 'DR', 'CR']:
            new_study['labelType'] = 'DX'
        elif new_study['modality'] == 'CT' and new_study['bodyPartExamined'] == 'CHEST':
            new_study['labelType'] = 'Nodule'
        elif new_study['modality'] == 'CT' and (new_study['bodyPartExamined'] == 'LIVER' or new_study['bodyPartExamined'] == "ABDOMEN"):
            new_study['labelType'] = 'LiverTumor'
        else:
            new_study['labelType'] = 'Unknown'

        studies.append(new_study)

    return studies

def move_into_folders(studies):
    if not os.path.exists(DICOM_ROOT):
        os.makedirs(DICOM_ROOT)

    for study in studies:
        series = study['series']
        for s in series:
            seriesID = s['seriesID']
            folder = os.path.join(DICOM_ROOT, seriesID)
            if os.path.exists(folder):
                shutil.rmtree(folder)
            os.makedirs(folder)
            for ins in s['instances']:
                src = ins['filepath']
                dest = os.path.join(folder, ins['filename'])
                shutil.move(src, dest)
                ins['filepath'] = dest
                ins['filename'] = os.path.basename(dest)

    return studies

def process_study_image(study):
    modality = study['modality']
    generate_file = False

    if modality == 'DX' or modality == 'DR' or modality == 'CR':
        logger.info('convert DX series')
        for series in study['series']:
            flag = insert_x_ray_instances(series)
            if flag:
                generate_file = True
            else:
                study['series'].remove(series)

    elif modality == 'CT':
        max_instance_num = 0
        longest_series = None
        for s in study['series']:
            if len(s['instances']) > max_instance_num:
                max_instance_num = len(s['instances'])
                longest_series = s
        logger.info('convert longest CT series, len {}'.format(max_instance_num))
        study['series'] = [longest_series]
        reverse, flag = insert_CT_instances(longest_series)
        study['reverse'] = reverse
        generate_file = True if flag else False

    elif modality == 'MR':
        max_instance_num = 0
        longest_series = None
        for s in study['series']:
            if len(s['instances']) > max_instance_num:
                max_instance_num = len(s['instances'])
                longest_series = s
        logger.info('convert longest MR series, len {}'.format(max_instance_num))
        study['series'] = [longest_series]
        flag = insert_CT_instances(longest_series)
        generate_file = True if flag else False
    else:
        logger.info('modality not supported %s' % modality)
        for series in study['series']:
            instances = series['instances']
            for ins in instances:
                ins.pop('filename')
                ins.pop('filepath')
        generate_file = False

    return study, generate_file

def on_file(filepaths):
    parsed_dicom_files = []
    for filepath in filepaths:
        meta_info = parse_meta(str(filepath))
        if meta_info is None:
            continue
        filename = os.path.basename(filepath)
        meta_info['filepath'] = filepath
        meta_info['filename'] = filename
        parsed_dicom_files.append(meta_info)

    studyInfo_list = group_by_study(parsed_dicom_files)
    studyInfo_list = move_into_folders(studyInfo_list)
    for study in studyInfo_list:
        study['state']='meta_only'
    return studyInfo_list

if __name__ == "__main__":
    fileroot = '/home/imsight/data/storeimage/1.2.840.113704.1.111.5840.1533635850.1'
    filepaths = []
    for file in os.listdir(fileroot):
        if file.endswith(".dcm"):
            filepaths.append(os.path.join(fileroot, file))

    studyInfo_list = on_file(filepaths)
    print studyInfo_list
