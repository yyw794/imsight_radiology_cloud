import React, { Component } from 'react'
import {Prompt} from 'react-router-dom'
import { Modal, Button, Row, Col, Input, Icon, TreeSelect, Layout, Menu, Alert, message } from 'antd'
import { List } from 'antd'
const { TextArea, Search } = Input
const { Header, Content, Footer } = Layout
const TreeNode = TreeSelect.TreeNode
import Template from './Template'
import LoadingWrapper from '../../component/LoadingWrapper'
import SignoutButton from '../../component/SignoutButton'
import Logo from '../../component/Logo'
import Report from '../../component/ReportLung'
import api from '../../api'
import _ from 'underscore'
import Immutable from 'immutable'
import {getUser} from '../../auth'
import moment from 'moment'
import io from 'socket.io-client'
import {CSVLink, CSVDownload} from 'react-csv'

import templateList from './data'

import LanguageDropdownList from '../../component/LanguageDropdownList'
import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './index.less'

class ReportPrintPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      report: Immutable.fromJS(Object.assign({
        patientName: '',
        gender: '',
        age: '',
        patientID: '',
        modality: '',
        studyDate: '',
        imagingFind: '',
        diagnosisAdvice: '',
        updateDate: '',
        screenshotURLs: []
      }, this.props.data.report)), // immutable
      labels: this.props.data.labels,
      radiomics: this.props.data.radiomics,
      templateList: this.props.data.template,
      maxZ:!!this.props.data.study.series[Object.keys(this.props.data.study.series)[0]].instances['z'] ?
            this.props.data.study.series[Object.keys(this.props.data.study.series)[0]].instances['z'].length
            :
            1,
      template: {},
      modalVisible: false,
      newTemplateName: '', // save new template as this name
      unsave: false      
    }
    this.allTemplate = this.props.data.template // store original template list
    this.currentUser = getUser()

  }

  componentDidMount() {
    this.socket = io(window.location.origin, {transports: ['websocket']})
    this.socket.connect()
    this.socket.on('connect', () => {
      console.log('connected');
    })
    this.socket.on('reportUpdate', (payload) => {
      console.log('reportUpdated', payload);
      this.setState({
        report: this.state.report.merge(payload.data)
      })
    })
    this.socket.on('labelUpdate', (payload) => {
      console.log('labelUpdated', payload);
      this.setState({
        labels:payload.data
      })
    })
    this.socket.on('disconnect', () => {
      console.log('disconnect');
    });
    this.socket.emit('open', {
      userID: getUser().account,
      taskID: this.props.data.report.taskID
    })
      window.print();
      setTimeout(window.close, 0);
  }
  componentWillUnmount() {
    this.socket.close()
  }

  removeImage = (index) => {
    if(!!this.state.report.toJS().labelist)
    {
      this.setState({
        report: this.state.report
        .update('labelist',labelist=>labelist.delete(index))
        .update('screenshotURLs', screenshotURLs => screenshotURLs.delete(index))
      })
    }else{
      this.setState({
        report: this.state.report
        .update('screenshotURLs', screenshotURLs => screenshotURLs.delete(index))
      })
    }
  }

  save() {
    // const data = Object.assign({operation: 'update'}, this.state.report)
    const data = this.state.report.toJSON()
    console.log('save report', data)
    return (
      api.updateReport(data)
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.saved"))
        this.setState({
          report: this.state.report.set('updateDate', moment().format('YYYYMMDD')),
          unsave: false
        })
      })
    )
  }

  radiomics(){
    console.log("downloaded radiomics")
    return true
  }

  submit = () => {
    const data = this.state.report.merge({action: 'submit'}).toJSON()
    return this.save()
      .then(() => api.updateReportStatus(data))
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.submitted"))
        this.setState({
          report: this.state.report.set('submitted', true)
        })
      })
  }

  unsubmit = () => {
    // this.save()
    const data = this.state.report.merge({action:'unsubmit'}).toJSON()
    return api.updateReportStatus(data)
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.unsubmitted"))
        this.setState({
          report: this.state.report.set('submitted', false)
        })
      })
  }

  examinate = () => {
    // this.save()
    const data = this.state.report.merge({action:'confirm'}).toJSON()
    return this.save()
      .then(() => api.updateReportStatus(data))
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.examinated"))
        this.setState({
          report: this.state.report.set('examined', true)
        })
      })
  }

  // template related
  handleTemplateChange = (key, value) => {
    let template = Object.assign({}, this.state.template, {[key]: value})
    this.setState({template})
  }

  saveTemplateChange = () => {
    api.updateTemplate(this.state.template)
    .then(() => message.success(GetLocaledText(this, "ReportPageLung.message.saved-successfully")))
  }

  applyTemplate = () => {
    if (this.isForbidEdit()) {
      return
    }
    // don't change report's modality
    let template = Object.assign({}, this.state.template)
    delete template.modality
    this.setState({report: this.state.report.merge(template)})
  }

  searchTemplate = (kw) => {
    this.setState({
      templateList: this.allTemplate.filter(t => t.templateName.includes(kw) || t.templateID.includes(kw))
    })
  }

  filterTemplate = (category) => {
    let cats = category.split('/')
    let templateList
    const isModalityMatched = (t) => {
      let matched = t.modality === cats[1]
      if (cats[1] === 'DR') {
        matched = matched || (t.modality === 'DX')
      }
      return matched
    }
    if (cats.length === 1) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0])
    } else if (cats.length === 2) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0] && isModalityMatched(t))
    } else if (cats.length === 3) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0] && isModalityMatched(t) && t.bodyPartExamined === cats[2])
    }
    this.setState({templateList})
  }

  openModal = () => {
    this.setState({
      modalVisible: true,
      newTemplateName: '',
    })
  }

  handleModalOk = () => {
    // validate input
    if (!this.state.newTemplateName) {
      // empty
      this.setState({modalErrorText: GetLocaledText(this, "ReportPageLung.mandatory-name")})
      return
    }
    let data = Object.assign({}, this.state.template, {
      "templateName": this.state.newTemplateName,
    })
    delete data.templateID
    api.addTemplate(data)
    .then(data => {
      data.usergroup = 'USER'
      this.setState({
        templateList: [data, ...this.state.templateList]
      })
      return api.getTemplate()
    })
    .then(data => {
      this.allTemplate = data
      this.setState({
        modalVisible: false
      })
    })
  }

  renderMenuItems() {
    let items = []
    const privilege = this.state.report.get('user_privilege').toJS()
    if (privilege.includes('reporter')) {
      if (!this.state.report.get('submitted')) {
        items.push(<Menu.Item key="save"><FormattedMessage id='ReportPageLung.save'/></Menu.Item>)
        items.push(<Menu.Item key="submit"><FormattedMessage id='ReportPageLung.submit'/></Menu.Item>)
      } else if(!this.state.report.get('examined')) {
        items.push(<Menu.Item key="unsubmit"><FormattedMessage id='ReportPageLung.unsubmit'/></Menu.Item>)
      }
    } else if (privilege.includes('reviewer')
      && this.state.report.get('submitted')
      && !this.state.report.get('examined'))
    {
      items.push(<Menu.Item key="save"><FormattedMessage id='ReportPageLung.save'/></Menu.Item>)
      items.push(<Menu.Item key="examinate"><FormattedMessage id='ReportPageLung.examinate'/></Menu.Item>)
    }
    if(this.state.radiomics.length>0){
      items.push(<Menu.Item key="radiomics">
                   <CSVLink data={this.state.radiomics}
                            headers={[
                   GetLocaledText(this,"ReportPage.radiomics.nodule":"Nodule"),
                   GetLocaledText(this,"ReportPage.radiomics.volume":"Vol"),
                   GetLocaledText(this,"ReportPage.radiomics.avgCT":"Avg CT"),
                   GetLocaledText(this,"ReportPage.radiomics.maxCT":"Max CT"),
                   GetLocaledText(this,"ReportPage.radiomics.minCT":"Min CT"),
                   GetLocaledText(this,"ReportPage.radiomics.medCT":"Med CT"),
                   GetLocaledText(this,"ReportPage.radiomics.stdDev":"Std Dev"),
                   GetLocaledText(this,"ReportPage.radiomics.skewness":"Skewness"),
                   GetLocaledText(this,"ReportPage.radiomics.kurtosis":"Kurtosis"),
                   GetLocaledText(this,"ReportPage.radiomics.energy":"Energy"),
                   GetLocaledText(this,"ReportPage.radiomics.entropy":"Entropy"),
                   GetLocaledText(this,"ReportPage.radiomics.uniformity":"Uniformity"),
                   GetLocaledText(this,"ReportPage.radiomics.contrast":"Contrast"),
                   GetLocaledText(this,"ReportPage.radiomics.invDiffMoment":"Inv Diff Moment"),
                   GetLocaledText(this,"ReportPage.radiomics.correlation":"Correlation")]
                   }
                            filename={"radiomics.csv"} 
                            className="download-radiomics-button"
                            >
                      <FormattedMessage id='ReportPageLung.radiomics'/>
                   </CSVLink>
                 </Menu.Item>)
    }
    return items
  }

  renderStamp() {
    if (this.state.report.get('submitted') && !this.state.report.get('examined') && getUser().usergroup === 'reporter') {
      return <img className='report-status-stamp' src={require('../../../static/images/submit-stamp.png')}/>
    } else if (this.state.report.get('examined')) {
      return <img className='report-status-stamp' src={require('../../../static/images/exam-stamp.png')}/>
    }
    return ''
  }

  isForbidEdit() {
    const hasExamined = this.state.report.get('examined')
    const hasSubmitted = this.state.report.get('submitted')
    const isReporter = this.props.data.report.user_privilege.includes('reporter')
    const isExmainer = this.props.data.report.user_privilege.includes('reviewer')
    return hasExamined || (isReporter && hasSubmitted) || (isExmainer && !hasSubmitted)
  }

  showRawImage(url) {

  }

  render() {
    const isForbidEdit = this.isForbidEdit()
    return (
      <div className='page' id='report-page'>
        <Prompt
          when={this.state.unsave}
          message={location => GetLocaledText(this, "ReportPageLung.message.unsaved-before-leave")}
        />
        {/*<Header>*/}
          {/*<Logo/>*/}
          {/*<Menu*/}
            {/*theme="dark"*/}
            {/*mode="horizontal"*/}
            {/*style={{ lineHeight: '64px', borderBottom: 'none', display: 'inline-block' }}*/}
            {/*selectable={false}*/}
            {/*onClick={({item, key, keyPath}) => {*/}
              {/*if (key === 'submit') {this.submit()}*/}
              {/*else if (key === 'unsubmit') {this.unsubmit()}*/}
              {/*else if (key === 'save') {this.save()}*/}
              {/*else if (key === 'radiomics'){this.radiomics()}*/}
              {/*else if (key === 'examinate') {this.examinate()}*/}
              {/*else if (key === 'print') {message.info(GetLocaledText(this, "ReportPageLung.message-still-developing"))}*/}
            {/*}}*/}
          {/*>*/}
            {/*{this.renderMenuItems()}*/}
          {/*</Menu>*/}
          {/*<SignoutButton/>*/}
          {/*/!*<LanguageDropdownList setAppState={state => {this.props.setAppState(state)}}/>*!/*/}
        {/*</Header>*/}
        <div className='content'>
          {/*<div className='template-aside'>*/}
            {/*<Template*/}
              {/*list={this.state.templateList}*/}
              {/*template={this.state.template}*/}
              {/*onSearch={this.searchTemplate}*/}
              {/*onCategoryChange={this.filterTemplate}*/}
              {/*onSelected={template => this.setState({template})}*/}
              {/*onApply={this.applyTemplate}*/}
              {/*onTemplateChange={this.handleTemplateChange}*/}
              {/*onSave={this.saveTemplateChange}*/}
              {/*onSaveAs={this.openModal}*/}
              {/*showSaveBtn={this.state.template.usergroup === 'USER'}*/}
              {/*/>*/}
          {/*</div>*/}
          <Report
            isForbidEdit={isForbidEdit}
            report={this.state.report.toJS()}
            labels={this.state.labels}
            removeImage={this.removeImage}
            maxZ = {this.state.maxZ}
            onChange={(key, value) => {
              this.setState({
                report: this.state.report.set(key, value)
              })
            }}
            />
        </div>
        <Modal
          title={GetLocaledText(this, "ReportPageLung.title.save-template")}
          visible={this.state.modalVisible}
          onOk={this.handleModalOk}
          onCancel={() => this.setState({modalVisible: false, modalErrorText: ''})}>
          <Input onChange={(e) => this.setState({newTemplateName: e.target.value})}
            placeholder={GetLocaledText(this, "ReportPageLung.input.template.name")}
            value={this.state.newTemplateName}
            />
          {
            this.state.modalErrorText && !this.state.newTemplateName.length ?
            <Alert message={this.state.modalErrorText} type="error" style={{marginTop: 5}}/> : ''
          }
        </Modal>
      </div>
    )
  }

}

async function getData(props) {
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let study= await api.getStudyDetail({studyUID})
  let report = await api.getReport({taskID:study.taskID})
  let labels = await api.getLabels({
    taskID:study.taskID,
    seriesID:Object.values(study.series)[0].seriesID
  });
  let radiomicsJS = await api.getRadiomics({
    taskID:study.taskID,
    seriesID:Object.values(study.series)[0].seriesID
  });
  
  let radiomics = []
  if(radiomicsJS.length!==0)
  {
    radiomicsJS[0].data_list.forEach(function(r,i){
      radiomics.push([("#"+(i+1)),r.vol,r.avgct,r.maxct,r.minct,r.medct,r.stddev,r.skewness,r.kurtosis,r.energy,r.entropy,r.uniformity,r.contrast,r.invdiffmoment,r.correlation])
    })
  }
  
  let template = await api.getTemplate()
  let data = {report, template, labels,radiomics,study}
  return await data
}

ReportPrintPage = LoadingWrapper(ReportPrintPage, getData)

export default injectIntl(ReportPrintPage)
