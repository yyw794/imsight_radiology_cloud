export function GetLocaledText(obj, textID) {
  let intl = obj.props.intl
  let locale = intl.locale.toLowerCase();
  return intl.formatMessage({id: textID})
}

export function GetTestText(){
    return 'Yeah';
}
