import React, { Component } from 'react';
import { Modal, Button, Row, Col, Input, Icon, Table } from 'antd'

/**
 * antd-design 组件
 * @type {TextArea}
 */
const TextArea = Input.TextArea
import {getUser} from '../../auth'
import LungNoduleTable from './LungNoduleTable'

import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {getNoduleType} from '../../component/NodulePanel'

import './index.less'

/**
 * 肺结节报告
 */
class Report extends Component {
    /**
     * 参照生命周期函数
     * @param props
     */
  constructor(props) {
    super(props)
    this.state = {};

  }

    /**
     * 参照react生命周期函数
     */
  componentDidMount() {
    this.setState({});
  }

    /**
     * 参照react生命周期函数
     */
  componentWillUnmount() {
    this.setState({});
  }

    /**
     * 获取当前页报告数据
     * @returns {Array}
     */
  getPageData() {
    let pages = []
    let urls = this.props.report.screenshotURLs
    let imgNum = urls.length
    if (imgNum <= 3) {
      pages.push({
        pageIdx: 0,
        sections: [{name: 'Info'}, {name: 'Image', urls, offset: 0}, {name: 'LungNoduleTable'}, {name: 'ImageFind'}, {name: 'DiagnosisAdvice'}]
      })
    } else if (imgNum <= 6) {
      pages.push({
        pageIdx: 0,
        sections: [{name: 'Info'}, {name: 'Image', urls, offset: 0}, {name: 'LungNoduleTable'}]
      })
      pages.push({
        pageIdx: 0,
        sections: [{name: 'ImageFind'}, {name: 'DiagnosisAdvice'}]
      })
    } else if (imgNum <= 15) {
      pages.push({
        pageIdx: 0,
        sections: [{name: 'Info'}, {name: 'Image', urls: urls.slice(0, 9), offset: 0}]
      })
      pages.push({
        pageIdx: 1,
        sections: [{name: 'Image', urls: urls.slice(9), offset: 9}, {name: 'LungNoduleTable'}, {name: 'ImageFind'}, {name: 'DiagnosisAdvice'}]
      })
    } else {
      let pageIdx = 0
      pages.push({
        pageIdx,
        sections: [{name: 'Info'}, {name: 'Image', urls: urls.slice(0, 9), offset: 0}]
      })
      pageIdx++
      let i = 9
      for (; i < imgNum - 9; i += 12) {
        pages.push({
          pageIdx,
          sections: [{name: 'Image', urls: urls.slice(i, i + 12), offset: i}]
        })
        pageIdx++
      }
      if (imgNum - i < 6) {
        pages.push({
          pageIdx,
          sections: [{name: 'Image', urls: urls.slice(i), offset: i}, {name: 'LungNoduleTable'}, {name: 'ImageFind'}, {name: 'DiagnosisAdvice'}]
        })
      } else {
        pages.push({
          pageIdx,
          sections: [{name: 'Image', urls: urls.slice(i, imgNum), offset: i}]
        })
        pageIdx++
        pages.push({
          pageIdx: pageIdx,
          sections: [{name: 'Image', urls: [], offset: imgNum}, {name: 'LungNoduleTable'}, {name: 'ImageFind'}, {name: 'DiagnosisAdvice'}]
        })
      }

    }
    pages.forEach(page => {
      page.total = pages.length
    })
    return pages
  }

    /**
     * 获取结节位置
     * @param p0
     * @param p1
     * @param p2
     * @param p3
     * @param p4
     * @returns {string}
     */
  getNoduleLocs(p0,p1,p2,p3,p4)
  {
    let locs=""
    if(p0.length>0) locs+=GetLocaledText(this,"ReportLung.location.upperLeft")
    if(p1.length>0) locs+=(locs.length>0?GetLocaledText(this,"ReportLung.slight-pause"):"")+GetLocaledText(this,"ReportLung.location.lowerLeft")
    if(p2.length>0) locs+=(locs.length>0?GetLocaledText(this,"ReportLung.slight-pause"):"")+GetLocaledText(this,"ReportLung.location.upperRight")
    if(p3.length>0) locs+=(locs.length>0?GetLocaledText(this,"ReportLung.slight-pause"):"")+GetLocaledText(this,"ReportLung.location.lowerRight")
    if(p4.length>0) locs+=(locs.length>0?GetLocaledText(this,"ReportLung.slight-pause"):"")+GetLocaledText(this,"ReportLung.location.middleRight")
    return locs
  }

    /**
     * 获取结节亚类
     * @param type 亚类
     * @returns {*}
     */
  getTransNoduleType(type)
  {
      if (type === "GroundGlass"){
        return GetLocaledText(this,"NodulePanel.subclass.glass")
      }else if(type === "PartSolid"){
        return GetLocaledText(this,"NodulePanel.subclass.subsolid")
      }else if(type === "Solid"){
        return GetLocaledText(this,"NodulePanel.subclass.solid")
      }else if(type === "Calcification"){
        return GetLocaledText(this,"NodulePanel.subclass.calcified")
      }else
      {
        return " "
      }
  }


  getAdviceStr(nodules)
  {
    let high = nodules.filter(n=>n.nodule_bm==="Malignant")
    let low  = nodules.filter(n=>n.nodule_bm!=="Malignant")
    let high0=high.filter(n=>n.nodule_location===0)
    let high1=high.filter(n=>n.nodule_location===1)
    let high2=high.filter(n=>n.nodule_location===2)
    let high3=high.filter(n=>n.nodule_location===3)
    let high4=high.filter(n=>n.nodule_location===4)
    let low0=low.filter(n=>n.nodule_location===0)
    let low1=low.filter(n=>n.nodule_location===1)
    let low2=low.filter(n=>n.nodule_location===2)
    let low3=low.filter(n=>n.nodule_location===3)
    let low4=low.filter(n=>n.nodule_location===4)
    let highLocs =(high0.length>0?1:0) + (high1.length>0?1:0) + (high2.length>0?1:0) +(high3.length>0?1:0) +(high4.length>0?1:0)
    let lowLocs = (low0.length>0?1:0) + (low1.length>0?1:0) + (low2.length>0?1:0) +(low3.length>0?1:0) +(low4.length>0?1:0)
    
    let str1=""
    let str2=""
    if(nodules.length<=3)
    {
      if(highLocs ===1)
      {
        str1+=this.getNoduleLocs(high0,high1,high2,high3,high4)+GetLocaledText(this,"ReportLung.advice.countLess3.same.1")
            +high.length+GetLocaledText(this,"ReportLung.advice.countLess3.same.2")+GetLocaledText(this,"ReportLung.risk.high")
            +GetLocaledText(this,"ReportLung.advice.followup")
            +GetLocaledText(this,"ReportLung.fullstop")
      }
      else if(highLocs>1)
      {
        str1+=this.getNoduleLocs(high0,high1,high2,high3,high4)+GetLocaledText(this,"ReportLung.advice.countLess3.diff.1")
            +GetLocaledText(this,"ReportLung.risk.high")+GetLocaledText(this,"ReportLung.advice.followup")
            +GetLocaledText(this,"ReportLung.fullstop")
      }
      
      if(lowLocs ===1)
      {
        str2+=this.getNoduleLocs(low0,low1,low2,low3,low4)+GetLocaledText(this,"ReportLung.advice.countLess3.same.1")
            +low.length+GetLocaledText(this,"ReportLung.advice.countLess3.same.2")+GetLocaledText(this,"ReportLung.risk.low")
            +GetLocaledText(this,"ReportLung.advice.followup")
            +GetLocaledText(this,"ReportLung.fullstop")
      }else if(lowLocs>1)
      {
        str2+=this.getNoduleLocs(low0,low1,low2,low3,low4)+GetLocaledText(this,"ReportLung.advice.countLess3.diff.1")
            +GetLocaledText(this,"ReportLung.risk.low")+GetLocaledText(this,"ReportLung.advice.followup")
            +GetLocaledText(this,"ReportLung.fullstop")
      }
      
    }else
    {
      if(highLocs <= 3&& highLocs >0)
      {
        str1+=this.getNoduleLocs(high0,high1,high2,high3,high4)+GetLocaledText(this,"ReportLung.advice.countMore3.locLess3.1")
            +GetLocaledText(this,"ReportLung.risk.high")+GetLocaledText(this,"ReportLung.advice.followup")
            +GetLocaledText(this,"ReportLung.fullstop")
      }else if(highLocs >3)
      {
        str1+=GetLocaledText(this,"ReportLung.advice.countMore3.locMore3.1")
            +GetLocaledText(this,"ReportLung.risk.high")+GetLocaledText(this,"ReportLung.advice.followup")
            +GetLocaledText(this,"ReportLung.fullstop")
      }
      
      if(lowLocs <= 3 && lowLocs >0)
      {
        str2+=this.getNoduleLocs(low0,low1,low2,low3,low4)+GetLocaledText(this,"ReportLung.advice.countMore3.locLess3.1")
            +GetLocaledText(this,"ReportLung.risk.low")+GetLocaledText(this,"ReportLung.advice.followup")
            +GetLocaledText(this,"ReportLung.fullstop")
      }else if(lowLocs >3 )
      {
        str2+=GetLocaledText(this,"ReportLung.advice.countMore3.locMore3.1")
            +GetLocaledText(this,"ReportLung.risk.low")+GetLocaledText(this,"ReportLung.advice.followup")
            +GetLocaledText(this,"ReportLung.fullstop")
      }
    }    
    return str1+str2
  }
  
  getImageFindingStr(nodules)
  {
    let high = nodules.filter(n=>n.nodule_bm==="Malignant")
    let low  = nodules.filter(n=>n.nodule_bm!=="Malignant")
    let high0=high.filter(n=>n.nodule_location===0)
    let high1=high.filter(n=>n.nodule_location===1)
    let high2=high.filter(n=>n.nodule_location===2)
    let high3=high.filter(n=>n.nodule_location===3)
    let high4=high.filter(n=>n.nodule_location===4)
    let low0=low.filter(n=>n.nodule_location===0)
    let low1=low.filter(n=>n.nodule_location===1)
    let low2=low.filter(n=>n.nodule_location===2)
    let low3=low.filter(n=>n.nodule_location===3)
    let low4=low.filter(n=>n.nodule_location===4)
    let highLocs =(high0.length>0?1:0) + (high1.length>0?1:0) + (high2.length>0?1:0) +(high3.length>0?1:0) +(high4.length>0?1:0)
    let lowLocs = (low0.length>0?1:0) + (low1.length>0?1:0) + (low2.length>0?1:0) +(low3.length>0?1:0) +(low4.length>0?1:0)
    
    if(nodules.length<=3)
    {
      let str =""
      high.map(n=>{
        let h0 = high0.filter(hn=>hn.nodule_location===n.nodule_location)
        let h1 = high1.filter(hn=>hn.nodule_location===n.nodule_location)
        let h2 = high2.filter(hn=>hn.nodule_location===n.nodule_location)
        let h3 = high3.filter(hn=>hn.nodule_location===n.nodule_location)
        let h4 = high4.filter(hn=>hn.nodule_location===n.nodule_location)
        str+=this.getNoduleLocs(h0,h1,h2,h3,h4)+GetLocaledText(this,"ReportLung.imageFinding.single.1")
        +n.nodule_avgHU.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.single.2")
        +this.getTransNoduleType(getNoduleType(n))+GetLocaledText(this,"ReportLung.imageFinding.single.3")
        +n.nodule_diameter.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.single.4")
        +n.nodule_volume.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.single.5")
        +GetLocaledText(this,"ReportLung.risk.high")
        +GetLocaledText(this,"ReportLung.fullstop")
      })
      low.map(n=>{
        let l0 = low0.filter(ln=>ln.nodule_location===n.nodule_location)
        let l1 = low1.filter(ln=>ln.nodule_location===n.nodule_location)
        let l2 = low2.filter(ln=>ln.nodule_location===n.nodule_location)
        let l3 = low3.filter(ln=>ln.nodule_location===n.nodule_location)
        let l4 = low4.filter(ln=>ln.nodule_location===n.nodule_location)
        str+=this.getNoduleLocs(l0,l1,l2,l3,l4)+GetLocaledText(this,"ReportLung.imageFinding.single.1")
        +n.nodule_avgHU.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.single.2")
        +this.getTransNoduleType(getNoduleType(n))+GetLocaledText(this,"ReportLung.imageFinding.single.3")
        +n.nodule_diameter.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.single.4")
        +n.nodule_volume.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.single.5")
        +GetLocaledText(this,"ReportLung.risk.low")
        +GetLocaledText(this,"ReportLung.fullstop")
      })
      return str      
    }else
    {
      let str1=""
      let str2=""
      
      let maxDiameter=-1.0
      let maxVolume=-1.0
      high.map(n=>{if(n.nodule_diameter>maxDiameter){
                  maxDiameter = n.nodule_diameter
                  maxVolume = n.nodule_volume }})
      
      if(highLocs > 3)
      {        
        str1+=GetLocaledText(this,"ReportLung.imageFinding.multiple.more3.1")+maxDiameter.toFixed(1)
            +GetLocaledText(this,"ReportLung.imageFinding.multiple.more3.2")+maxVolume.toFixed(1)
            +GetLocaledText(this,"ReportLung.imageFinding.concolusion")+GetLocaledText(this,"ReportLung.risk.high")
            +GetLocaledText(this,"ReportLung.fullstop")
      }else if(highLocs<=3 && highLocs > 0)
      {
        str1+=this.getNoduleLocs(high0,high1,high2,high3,high4)+GetLocaledText(this,"ReportLung.imageFinding.multiple.less3.1")
            +maxDiameter.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.multiple.less3.2")
            +maxVolume.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.concolusion")
            +GetLocaledText(this,"ReportLung.risk.high")
            +GetLocaledText(this,"ReportLung.fullstop")
      }
      
      maxDiameter=-1.0
      maxVolume=-1.0      
      low.map(n=>{if(n.nodule_diameter>maxDiameter){
                  maxDiameter = n.nodule_diameter
                  maxVolume = n.nodule_volume }})
      
      if(lowLocs > 3)
      {
        str2+=GetLocaledText(this,"ReportLung.imageFinding.multiple.more3.1")+maxDiameter.toFixed(1)
            +GetLocaledText(this,"ReportLung.imageFinding.multiple.more3.2")+maxVolume.toFixed(1)
            +GetLocaledText(this,"ReportLung.imageFinding.concolusion")+GetLocaledText(this,"ReportLung.risk.low")
            +GetLocaledText(this,"ReportLung.fullstop")
      }else if(lowLocs <=3 && lowLocs >0 )
      {
        str2+=this.getNoduleLocs(low0,low1,low2,low3,low4)+GetLocaledText(this,"ReportLung.imageFinding.multiple.less3.1")
            +maxDiameter.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.multiple.less3.2")
            +maxVolume.toFixed(1)+GetLocaledText(this,"ReportLung.imageFinding.concolusion")
            +GetLocaledText(this,"ReportLung.risk.low")
            +GetLocaledText(this,"ReportLung.fullstop")
      }
      
      return str1+str2
    }
    
    return "Image finding"
  }

  renderStamp() {
    if (this.props.report.submitted && !this.props.report.examined && this.props.report.user_privilege.includes('reporter')) {
      return <img className='report-status-stamp' src={require('../../../static/images/submit-stamp.png')}/>
    } else if (this.props.report.examined) {
      return <img className='report-status-stamp' src={require('../../../static/images/exam-stamp.png')}/>
    }
    return ''
  }

  renderInfoSection() {
    return (
      <div className='report-info-section'>
        <Row style={{height:'25px'}}>
          <Col span={8}>{GetLocaledText(this, "ReportLung.patientName")}：{this.props.report.patientName}</Col>
          <Col span={8}>{GetLocaledText(this, "ReportLung.gender")}：{this.props.report.gender==="MALE"?GetLocaledText(this,"ReportLung.gender.male"):GetLocaledText(this,"ReportLung.gender.female")}</Col>
          <Col span={8}>{GetLocaledText(this, "ReportLung.age")}：{this.props.report.age}</Col>
        </Row>
        <Row style={{height:'25px'}}>
          <Col span={8}>{GetLocaledText(this, "ReportLung.patientID")}：{this.props.report.patientID}</Col>
          <Col span={8}>
          {GetLocaledText(this, "ReportLung.study-item")}：
      {/*<Input value={this.props.report.modality}*/}
      {/*style={{display: 'inline-block', width: 100}}
            {/*disabled={this.props.isForbidEdit}*/}
           {/*onChange={e => {this.props.onChange('modality', e.target.value)}}/>*/}
           ：{this.props.report.modality}
          </Col>
          <Col span={8}>{GetLocaledText(this, "ReportLung.study-time")}：{this.props.report.studyDate}</Col>
        </Row>
      </div>
    )
  }

  renderImageSection(section) {
    return (
      <div className='report-image-section'>
        {section.urls.map((url, i) => (
          <div className='report-image-wrapper' key={i}>
            {
              !this.props.isForbidEdit ? <Icon type="close" onClick={() => this.props.removeImage(section.offset + i)}/> : ''
            }
            <img src={url} onClick={() => this.showRawImage(url)}/>
          </div>
        ))}
      </div>
    )
  }

  renderLungNoduleTableSection() {
    //not render DR, liver table.
    if(this.props.report.modality !=='CT' || !this.props.report.labelist ||(this.props.report.labelist.length!==0 && !!this.props.report.labelist[0].LI_RADS))
    {
      return ''
    }
    
    if (!this.props.report.labelist.filter(l=>l.url!=="").length) {
      return ''
    }
    return (
      <div className='report-lung-nodule-section'>
        <LungNoduleTable data={this.props.labels.filter(l=>l.url!=="")} maxZ={this.props.maxZ} zReverse={this.props.zReverse}/>
      </div>
    )
  }

  renderImageFindSection() {
    return (
      <div className='report-diagnosis-section'>
        <p><FormattedMessage id='ReportLung.diagnosis.image-find'/></p>
        <TextArea rows={6}
          className="areaClassReport"
          value={(this.props.report.imagingFind==="" && this.props.report.modality==="CT")?
                  this.getImageFindingStr(this.props.labels)
                  :
                  this.props.report.imagingFind}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('imagingFind', e.target.value===""?" ":e.target.value)}}/>
      </div>
    )
  }

  renderDiagnosisAdviceSection() {
    return (
      <div className='report-diagnosis-section'>
        <p><FormattedMessage id='ReportLung.diagnosis.advice'/></p>
        <TextArea rows={2}
          className="areaClassReport"
          value={(this.props.report.diagnosisAdvice==="" && this.props.report.modality==="CT")?
                this.getAdviceStr(this.props.labels)
                :
                this.props.report.diagnosisAdvice}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('diagnosisAdvice', e.target.value===""?" ":e.target.value)}}/>
      </div>
    )
  }

  renderPage = (page) => {
    const {sections, total, pageIdx} = page
    let sectionsElems = []
    if (pageIdx === 0) {
      sectionsElems.push(
        <div className='report-header'>
          <h2>
            <img src={require('../../../static/images/logo.png')}/>
            {GetLocaledText(this, "ReportLung.ImsightMed.Title")}
          </h2>
          <p><FormattedMessage id='ReportLung.pulmonary-nodules-report'/></p>
        </div>
      )
    }
    sectionsElems = sectionsElems.concat(sections.map(sect => {
      return this[`render${sect.name}Section`](sect)
    }))
    sectionsElems.push(
      <div className='report-footer'>
        <Row style={{height:'20px'}}>
          <Col span={8}>{GetLocaledText(this, "ReportLung.submit-doctor")}: {this.props.report.submitted ? this.props.report.userID : ''}</Col>
          <Col span={8}>{GetLocaledText(this, "ReportLung.examine-doctor")}: {this.props.report.examined ? this.props.report.examiner : ''}</Col>
          <Col span={8}>{GetLocaledText(this, "ReportLung.report-date")}: {this.props.report.updateDate}</Col>
        </Row>
        <Row style={{height:'20px'}}>
          <Col span={8}><FormattedMessage id='ReportLung.just-for-clinicians-ref-only'/></Col>
          <Col span={8}><FormattedMessage id='ReportLung.ImsightMed.fullName-with-url'/></Col>
          <Col span={8}>{GetLocaledText(this, "ReportLung.page-no")}{pageIdx + 1}/{total}</Col>
        </Row>
      </div>
    )
    return (
      <div className='report' key={pageIdx}>
        {pageIdx === 0 ? this.renderStamp() : ''}
        {sectionsElems}
      </div>
    )
  }

  render() {
    return (
      <div className={`report-wrapper ${this.props.report.modality}`} >
        {this.getPageData().map(this.renderPage)}
      </div>
    );
  }

}

export default injectIntl(Report);
