import React, { Component } from 'react';
import { Table, Icon } from 'antd';
import {getUser} from '../../auth'
import {withRouter} from 'react-router'
import {GENDER_MAP, STATUS_MAP, VIEW_MODALITY_URL, openWindow,detectionResult} from '../StudyTable'
import _ from 'underscore'
import './index.less'

import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

/**
 *
 */
class StudyHistoryTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedRowKeys: []
    }

  }

  onSelectChange = (selectedRowKeys) => {
    // select at most 2
    if (this.state.selectedRowKeys.length === 2 && selectedRowKeys.length > this.state.selectedRowKeys.length) {
      return
    }
    this.setState({selectedRowKeys})
    if (_.isFunction(this.props.onSelectChange)) {
      this.props.onSelectChange(selectedRowKeys)
    }
  }

  getSelection() {
    return this.props.dataSource.filter((d, i) => this.state.selectedRowKeys.includes(i)).map(d => d.studyUID)
    // return [...this.state.selectedRowKeys]
  }

  renderRow(data, columns) {
    let cols = []
    cols = columns.map(col => {
      if (col.render) {
        return <td>{col.render(data[col.dataIndex], data)}</td>
      } else {
        return <td>{data[col.dataIndex]}</td>
      }
    })
    return (
      <tr>
        {cols}
      </tr>
    )
  }

  render() {
    const columns = [
        {
            title: GetLocaledText(this,"StudyHistoryTable.patientId"),
            dataIndex: 'patientID',
            key: 'patientID',
            render: text => <span></span>,
            width: 100,
        },
       {
        title: GetLocaledText(this,"StudyHistoryTable.patientId"),
        dataIndex: 'patientID',
        key: 'patientID',
        render: text => <span>{text}</span>,
        width: 90,
      },
      {
        title: GetLocaledText(this,"StudyHistoryTable.patientName"),
        dataIndex: 'patientName',
        key: 'patientName',
        width: 160,
        render: text => <span>{text}</span>,
      },
      {
        title: GetLocaledText(this,"StudyHistoryTable.gender"),
        dataIndex: 'gender',
        key: 'gender',
        width: 80,
        // render: text => <span style={{display: 'inline-block', width: 80}}>{GENDER_MAP[text]}</span>,
        render: text => {
          if(text == 'MALE'){return <span><FormattedMessage id='StudyPage.gender.male'/></span>}
          else if(text == 'FEMALE'){return <span><FormattedMessage id='StudyPage.gender.female'/></span>}
          else{return <span>ErrorText</span>}
          //<span>{GENDER_MAP[text]}</span>
        },
      },
      {
        title: GetLocaledText(this,"StudyHistoryTable.birthday"),
        dataIndex: 'birthDate',
        key: 'birthDate',
        width: 120,
        render: text => <span>{text}</span>,
      },
      {
        title: GetLocaledText(this,"StudyHistoryTable.modality"),
        dataIndex: 'modality',
        key: 'modality',
        width: 110,
        render: text => <span>{text}</span>,
      },
      {
        title: GetLocaledText(this,"StudyHistoryTable.bodyPart"),
        dataIndex: 'bodyPartExamined',
        key: 'bodyPartExamined',
        width: 110,
        render: text => <span>{text}</span>,
      },
      {
        title: GetLocaledText(this,"StudyHistoryTable.studyTime"),
        dataIndex: 'studyDate',
        key: 'studyDate',
        width: 240,
        render:(text,record) => <span>{record.studyDate+' '+record.studyTime}</span>,
      },
      {
        title: GetLocaledText(this,"StudyHistoryTable.result"),
        dataIndex: 'result',
        key: 'result',
        // render: text => <span style={{display: 'inline-block', width: 110}}>{STATUS_MAP[text]}</span>,
        render: (text, record) => {
	    return detectionResult(record)
        },
        width: 110,
      },
      {
        title: GetLocaledText(this,"StudyHistoryTable.advice"),
        dataIndex: 'diagnosisAdvice',
        key: 'diagnosisAdvice',
        render: text => <span>{text}</span>,
      },
    ]
    const rowSelection = {
      columnWidth:68, 
      selectedRowKeys: this.state.selectedRowKeys,
      onChange: this.onSelectChange,
      getCheckboxProps: record => {
        return {disabled: record.modality !== 'CT'}
      }
    }
    // when double click, click event will also be fired. so we have to wait to see
    // if it is a double click
    let isDoubleClick = false
    let onClickWaiting = false
    return (
      <div style={{width:'100%'}}>
        {
        !!this.props.dataSource ?
        <Table columns={columns}
          className='study-history-table'
          onRow={(record) => {
            return {
              onDoubleClick: () => {
                isDoubleClick = true
                if (record.user_privilege.includes('reviewer') && !record.submitted) {return}
                  localStorage.setItem("fellowParams",JSON.stringify(record))
                openWindow("report",record.studyUID, record.modality)
              },
              onClick: () => {
                if (onClickWaiting) {return}
                onClickWaiting = true
                setTimeout(() => {
                  if (isDoubleClick) {
                    isDoubleClick = false
                    onClickWaiting = false
                    return
                  }
                  localStorage.setItem("fellowParams",JSON.stringify(record))
                  onClickWaiting = false
                  openWindow("image",record.studyUID, record.modality)
                }, 500)
              }
            };
          }}
          showHeader={false}
          dataSource={this.props.dataSource}
          pagination={false}
         />
        :''
        }
      </div>
    )
  }

}

export default injectIntl(withRouter(StudyHistoryTable));
