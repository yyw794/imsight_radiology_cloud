#!/bin/python
#coding=utf-8
from flask import make_response, jsonify, request, abort
from werkzeug.utils import secure_filename
from flask_login import current_user
from socketIO_client import SocketIO, LoggingNamespace

import sys, os,threading,traceback
import uuid
import time

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import *
from models.response.response import response
from models.report.Report import Report, parseReportInfo
from models.tool.Tool import Tool, saveToollist
from models.study.Study import parseStudyInfo
from models.label.Label_IO import parseLabelInfo, saveLabelist
from models.task.Task import parseTaskInfo

from services.file.on_file import on_file,process_study_image
from services.algo.algo_service import predict, nodule_register
from services.algo.on_radiomics import create_radiomics
from config import PORT, ADDRESS, PROTOCOL, SCREENSHOT_ROOT, CACHE_ROOT
import logging
logger = logging.getLogger()
from services.pacs.pacs_api import update_study_instance_state,send_socketIO_to_node
from services.basic.basic_api import socket_emit

url = "{}://{}:{}".format(PROTOCOL,ADDRESS, PORT)

class ProcessThread(threading.Thread):
    def __init__(self, target, args=()):
        super(ProcessThread, self).__init__()
        self.target = target
        self.args = args
        self.exitcode = 0
        self.exception = None
        self.exc_traceback = ''

    def run(self):
        try:
            self.result = self.target(*self.args)
        except Exception as e:
            self.exitcode = 1
            self.exception = e
            self.exc_traceback = ''.join(traceback.format_exception(*sys.exc_info()))

    def get_result(self):
        try:
            return self.result
        except Exception:
            return None


required_key = ['taskID']
required_file = ['file']
required_key_nodule = ['labelID', 'seriesID']
mutex = threading.Lock()
def allowed_dcm_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in DCM_EXTENSIONS

def combined_URL(uid):
    return url + "/api/screenshot?uid=" + uid

def feed_screenshot():
    if not current_user.is_authenticated:
        abort(401)

    res = response()
    userID = current_user.get_id()
    taskID = request.form.get('taskID', '')
    if taskID == '':
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    report_info = db_find_one("report", {"taskID": taskID})
    if not report_info:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    report = parseReportInfo(report_info)
    studyUID = report.studyUID

    file_feild = request.files
    if not all(key in file_feild for key in required_file):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    if not os.path.exists(SCREENSHOT_ROOT):
        os.makedirs(SCREENSHOT_ROOT)

    file = request.files['file']
    filename = secure_filename(file.filename)
    uid = str(uuid.uuid1())
    folderpath = os.path.join(SCREENSHOT_ROOT, uid)
    os.makedirs(folderpath)
    filepath = os.path.join(folderpath, filename)
    file.save(filepath)

    if report.modality == 'CT':
        label_info = db_find_one("label", {"taskID": taskID})
        if label_info is None:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        labelID = request.form.get('labelID', '')
        seriesID = request.form.get('seriesID', '')
        if labelID == '' or seriesID == '':
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        labelist = parseLabelInfo(label_info)
        labelist = filter(lambda l: l.seriesID == seriesID, labelist)
        target_labelist = filter(lambda l: l.labelID == labelID, labelist)
        if len(target_labelist) == 0:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)
        label = target_labelist[0]
        labelist.remove(label)
        uid = label.labelID
        label.url = combined_URL(uid)
        labelist.append(label)
        db_update_one("label", {"taskID": taskID}, saveLabelist(labelist, studyUID, taskID, label_info["labelType"]))
        socket_emit('updateLabel', {'data': [label.to_dict() for label in labelist], 'userID': userID, 'taskID': taskID})

    new_resource = {
        "uid": uid,
        "img_path": folderpath,
        "filename": filename
    }
    db_insert_one("resource", new_resource)
    if report.modality == 'DX':
        report.screenshotURLs = [combined_URL(uid)]
    else:
        report.screenshotURLs.append(combined_URL(uid))

    db_update_one("report", {"taskID": taskID}, report.to_db())
    ret = report.to_dict()
    if report.modality == 'CT':
        label_info = db_find_one("label", {"taskID": taskID})
        labelist = parseLabelInfo(label_info)
        ret['labelist'] = [label.to_dict() for label in labelist if label.url != '']
    socket_emit('updateReport', {'data': ret, 'userID': userID, 'taskID': taskID})

    res.data = combined_URL(uid)
    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)


required_key_manual_detection = ['studyUID', 'labelType', 'taskID']


def fetch_manual_detection():
    if not current_user.is_authenticated:
        abort(401)
        
    res = response()
    userID = current_user.get_id()

    params = request.args.to_dict() if request.method == 'GET' else request.json

    logger.info(params)
    if not all(key in params for key in required_key_manual_detection):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    studyUID = params['studyUID']
    labelType = params['labelType']
    taskID = params['taskID']
    study_info = db_find_one("study", {"studyUID":studyUID})
    
    db_delete_one('ai_label', {'studyUID': studyUID})
    
    user_label_info = db_find_one("label", {"studyUID":studyUID})
    user_label_info['labels'] = []
    db_update_one('label', {"taskID": taskID}, user_label_info)
    
    study_info['state'] = 'file_only'
    db_find_update_otherwise_insert("study", {"studyUID": studyUID}, study_info)
    send_socketIO_to_node(studyUID, userID)
    study_info_temp = study_info.copy()
    if labelType == 'DX':
        study_info_temp['modality'] = 'DX'
        study_info_temp['bodyPartExamined'] = 'CHEST'
        for series_index in range(len(study_info_temp['series'])):
            instances = study_info_temp['series'][series_index]['instances']
            for instance_index in range(len(instances)):
                instances[instance_index]['viewPosition'] = 'PA'
    elif labelType == 'Nodule':
        study_info_temp['modality'] = 'CT'
        study_info_temp['bodyPartExamined'] = 'CHEST'
    elif labelType == 'LiverTumor':
        study_info_temp['modality'] = 'CT'
        study_info_temp['bodyPartExamined'] = 'LIVER'
    task_info = predict(study_info_temp, userID, taskID)

    if task_info:
        create_radiomics(task_info['labels'],task_info['studyUID'],task_info['labelType'],taskID)
        study_info['state'] = 'completed'
        
        user_label_info = db_find_one('label', {"taskID": taskID})
        user_label_info['labelType'] = labelType
        db_update_one('label', {"taskID": taskID}, user_label_info)
    else:
        study_info['state'] = 'algo_unsupport' 
    db_find_update_otherwise_insert("study", {"studyUID": studyUID}, study_info)
    
    new_study = parseStudyInfo(study_info)
    new_report = Report()
    new_report.taskID = taskID
    new_report.studyUID = new_study.studyUID
    new_report.studyDate = new_study.studyDate
    new_report.modality = new_study.modality
    new_report.birthDate = new_study.birthDate
    new_report.patientID = new_study.patientID
    new_report.patientName = new_study.patientName
    new_report.gender = new_study.gender
    db_find_update_otherwise_insert("report", {"taskID": taskID}, new_report.to_db())

    if new_study.modality in ['DX', 'DR', 'CR']:
        tool_list = []
        for series in study_info['series']:
            for instance in series['instances']:
                new_tool = Tool(new_study.studyUID, series['seriesID'], instance['instanceID'], None)
                tool_list.append(new_tool)
        logger.info("userID {} update tool {}".format(userID, taskID))
        db_find_update_otherwise_insert("tool", {"userID": userID, "studyUID": new_study.studyUID}, saveToollist(tool_list, new_study.studyUID, userID))

    # sui fang
    if study_info['state'] == 'completed' and labelType == 'Nodule':
        one_patient_studies = db_find('study', {'patientID': new_study.patientID})
        patient_other_studies = filter(lambda s: \
                                        s['studyUID'] != studyUID and \
                                        s['modality'] == 'CT' and \
                                        'CHEST' in s['bodyPartExamined'],\
                                       one_patient_studies)

        study_series_list = study_info['series']
        study_info['series'] = {}
        for series in study_series_list:
            study_info['series'][series['seriesID']] = series
        task_temp = db_find_one('task', {'studyUID': study_info['studyUID'], '$or': [{'reporter': userID}, {'reviewer': userID}]})
        study_info['taskID'] = task_temp['taskID']

        for other_study in patient_other_studies:
            other_series = other_study['series']
            other_study['series'] = {}
            for series in other_series:
                other_study['series'][series['seriesID']] = series
            task = db_find_one('task', {'studyUID': other_study['studyUID'], '$or': [{'reporter': userID}, {'reviewer': userID}]})
            if not task:
                continue
            other_study['taskID'] = task['taskID']
            if study_info['studyDate'].strftime('%Y-%m-%d %H:%M:%S') > other_study['studyDate'].strftime('%Y-%m-%d %H:%M:%S'):
                fixed_study = other_study
                moving_study = study_info
            else:
                fixed_study = study_info
                moving_study = other_study
            register_label_info = db_find_one('ai_label', {'fixed_studyUID': fixed_study['studyUID'], 'moving_studyUID': moving_study['studyUID']})
            if register_label_info:
                continue
            nodule_register(fixed_study, moving_study, userID)

    send_socketIO_to_node(studyUID, userID)  # update to front after nodule registration

    if request.method == "POST":
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)

def process_prediction(studyUID,study_info,userID,taskID,isNotify,doFollowUp):
    study_info,flag=process_study_image(study_info)
    study_info['state']='file_only' if flag else 'meta_only'
    db_find_update_otherwise_insert("study", {"studyUID": studyUID}, study_info)
    
    if(isNotify):
        send_socketIO_to_node(studyUID,userID)
    
    task_info = predict(study_info, userID, taskID)
    if task_info:
        create_radiomics(task_info['labels'],task_info['studyUID'],task_info['labelType'],taskID)
        logger.info("userID {} invoke algorithm success".format(userID))
        study_info['state'] = 'completed'
    else:
        study_info['state'] = 'algo_unsupport'

    db_find_update_otherwise_insert("study", {"studyUID": studyUID}, study_info)
    
    if(doFollowUp):
        #to_db to_dict to solve studyTime missing
        new_study = parseStudyInfo(study_info)
        new_dict = new_study.to_dict(withSeries=True)
        new_dict['taskID'] = taskID
        do_nodule_matching(new_dict,userID)

    if(isNotify):
        send_socketIO_to_node(studyUID,userID)

def do_nodule_matching(study,userID):
    logger.info("begin matching {}".format(study['studyUID']))
    study_info_current = db_find("study", {"studyUID": study['studyUID']})
    if len(study_info_current) != 1 or study_info_current[0]['state'] != 'completed' or \
            study_info_current[0]['labelType'] != 'Nodule':
        logger.info("end matching ,not completed state ,not Nodule labelType")
        return None

    one_patient_studies = db_find('study', {'patientID': study['patientID']})
    patient_other_studies = filter(lambda s: \
                                       s['studyUID'] != study['studyUID'] and \
                                       s['modality'] == 'CT' and \
                                       'CHEST' in s['bodyPartExamined'],\
                                   one_patient_studies)
    logger.info("matching 0.1")
    for other_study in patient_other_studies:
        logger.info("matching 0.2")
        other_series = other_study['series']
        logger.info("matching 0.3")
        other_study['series'] = {}
        logger.info("matching 0.4")
        for series in other_series:
            logger.info("matching 0.5")
            other_study['series'][series['seriesID']] = series
        logger.info("matching 0.6")
        task = db_find_one('task', {'studyUID': other_study['studyUID'], '$or': [{'reporter': userID}, {'reviewer': userID}]})
        logger.info("matching 0.7")
        if not task:
            logger.info("macthing 1 no task, continue")
            continue
        other_study['taskID'] = task['taskID']
        logger.info("matching 0.8")
        if '{0} {1}'.format(study['studyDate'], study['studyTime']) > other_study['studyDate'].strftime('%Y-%m-%d %H:%M:%S'):
            logger.info("matching 0.9")
            fixed_study = other_study
            moving_study = study
        else:
            logger.info("matching 0.99")
            fixed_study = study
            moving_study = other_study
        register_label_info = db_find_one('ai_label', {'fixed_studyUID': fixed_study['studyUID'], 'moving_studyUID': moving_study['studyUID']})
        if register_label_info:
            logger.info("matching 1 label registered continue")
            continue
        nodule_register(fixed_study, moving_study, userID)
        logger.info("end matching {}".format(other_study['studyUID']))
  

def process_user_filepaths(filepaths, userID):
    study_infolist = on_file(filepaths)
    shared = db_find_one("ownership", {"userID": userID})
    liability_info = db_find_one("liability", {"reporter": userID})
    
    studylist = []
    for study_info in study_infolist:
        labelType = study_info['labelType']
        new_study = parseStudyInfo(study_info)
        new_taskID = ""
        if (new_study.studyUID).replace(".", "-") not in shared['shared']:
            new_taskID = str(uuid.uuid1())
            logger.info("userID {} studyUID {} NOT in shared new taskID {}".format(userID, new_study.studyUID, new_taskID))
        else:
            new_taskID = shared['shared'][(new_study.studyUID).replace(".", "-")]
            logger.info("userID {} studyUID {} in shared taskID {}".format(userID, new_study.studyUID, new_taskID))
        new_report = Report()
        new_report.taskID = new_taskID
        new_report.studyUID = new_study.studyUID
        new_report.studyDate = new_study.studyDate
        new_report.modality = new_study.modality
        new_report.birthDate = new_study.birthDate
        new_report.patientID = new_study.patientID
        new_report.patientName = new_study.patientName
        new_report.gender = new_study.gender
        logger.info("userID {} update report {}".format(userID, new_taskID))
        db_find_update_otherwise_insert("report", {"taskID": new_taskID}, new_report.to_db())

        if new_study.modality in ['DX', 'DR', 'CR']:
            tool_list = []
            for series in study_info['series']:
                for instance in series['instances']:
                    new_tool = Tool(new_study.studyUID, series['seriesID'], instance['instanceID'], None)
                    tool_list.append(new_tool)
            logger.info("userID {} update tool {}".format(userID, new_taskID))
            db_find_update_otherwise_insert("tool", {"userID": userID, "studyUID": new_study.studyUID}, saveToollist(tool_list, new_study.studyUID, userID))

        new_task = {
            "taskID": new_taskID,
            "reporter": liability_info['reporter'],
            "reviewer": liability_info['reviewer'],
            "state": "assign",
            "type": current_user.type,
            "studyUID": new_study.studyUID
        }
        logger.info("userID {} update task {} ".format(userID, new_taskID))
        db_find_update_otherwise_insert("task", {"taskID": new_taskID}, new_task)
        task = parseTaskInfo(new_task)

        study_info['state']='pull_data'
        new_study.state='pull_data'
        logger.info("userID {} update studyUID {} taskID {} state {}".format(userID, new_study.studyUID, new_taskID,study_info['state']))
        db_find_update_otherwise_insert("study", {"studyUID": study_info['studyUID']}, study_info)
        
        mutex.acquire()
        new_shared = db_find_one("ownership", {"userID": userID})
        new_shared['shared'][(new_study.studyUID).replace(".", "-")] = new_taskID
        db_update_one("ownership", {"userID": userID}, new_shared)
        mutex.release()

        # update this label after get ai result
        db_find_update_otherwise_insert("label", {"taskID": new_taskID}, {"studyUID": new_study.studyUID, "taskID": new_taskID, "labelType": labelType, "labels": []})
        if current_user.type == 'normal':
            logger.info("userID {} invoke algorithm".format(userID))
            t=ProcessThread(target=process_prediction,args=(new_study.studyUID,study_info,userID,task.id,True,True))
            t.start()

        new_dict = new_study.to_dict(withSeries=True)
        new_dict["diagnosisAdvice"] = new_report.diagnosisAdvice
        new_dict['submitted'] = new_report.submitted()
        new_dict['labelNum']  = 0
        new_dict['taskID'] = new_taskID
        new_dict["result"] = "pending"
        new_dict['user_privilege'] = task.get_user_privilege(userID)
        studylist.append(new_dict)

    return studylist

def feed_study_image():
    if not current_user.is_authenticated:
        return abort(401)

    filepaths = []

    uploaded_files = request.files.getlist("file[]")
    if not uploaded_files:
        uploaded_files = request.files.values()

    if not os.path.exists(CACHE_ROOT):
        os.makedirs(CACHE_ROOT)

    logger.info(uploaded_files)
    for file in uploaded_files:
        filename = secure_filename(file.filename)
        filepath = os.path.join(CACHE_ROOT, filename)
        if filepath in filepaths:
            logger.info("already save file {}".format(filepath))
            continue
        file.save(filepath)
        filepaths.append(filepath)
        logger.info("userID {} upload file {}".format(current_user.get_id(), filepath))

    res = response()
    userID = current_user.get_id()

    logger.info("process files userID {}".format(userID))
    if len(filepaths)>0:
        studylist = process_user_filepaths(filepaths, userID)
    else:
        studylist = {}

    res.statusCode = 1000
    res.data = studylist
    return make_response(jsonify(res.to_dict()), 200)

def process_pacs_filepaths(filepaths,userID):
    study_infolist = on_file(filepaths)
    shared = db_find_one("ownership", {"userID": userID})
    liability_info = db_find_one("liability", {"reporter": userID})
    
    studylist = []
    for study_info in study_infolist:
        labelType = study_info['labelType']
        new_study = parseStudyInfo(study_info)
        new_taskID = ""
        if (new_study.studyUID).replace(".", "-") not in shared['shared']:
            new_taskID = str(uuid.uuid1())
            logger.info("userID {} studyUID {} NOT in shared new taskID {}".format(userID, new_study.studyUID, new_taskID))
        else:
            new_taskID = shared['shared'][(new_study.studyUID).replace(".", "-")]
            logger.info("userID {} studyUID {} in shared taskID {}".format(userID, new_study.studyUID, new_taskID))
        new_report = Report()
        new_report.taskID = new_taskID
        new_report.studyUID = new_study.studyUID
        new_report.studyDate = new_study.studyDate
        new_report.modality = new_study.modality
        new_report.birthDate = new_study.birthDate
        new_report.patientID = new_study.patientID
        new_report.patientName = new_study.patientName
        new_report.gender = new_study.gender
        logger.info("userID {} update report {}".format(userID, new_taskID))
        db_find_update_otherwise_insert("report", {"taskID": new_taskID}, new_report.to_db())

        if new_study.modality in ['DX', 'DR', 'CR']:
            tool_list = []
            for series in study_info['series']:
                for instance in series['instances']:
                    new_tool = Tool(new_study.studyUID, series['seriesID'], instance['instanceID'], None)
                    tool_list.append(new_tool)
            logger.info("userID {} update tool {}".format(userID, new_taskID))
            db_find_update_otherwise_insert("tool", {"taskID": new_taskID}, saveToollist(tool_list, new_study.studyUID, userID))

        new_task = {
            "taskID": new_taskID,
            "reporter": liability_info['reporter'],
            "reviewer": liability_info['reviewer'],
            "state": "assign",
            "type": current_user.type,
            "studyUID": new_study.studyUID
        }
        logger.info("userID {} update task {} ".format(userID, new_taskID))
        db_find_update_otherwise_insert("task", {"taskID": new_taskID}, new_task)
        task = parseTaskInfo(new_task)
        

        # update this label after get ai result
        db_find_update_otherwise_insert("label", {"taskID": new_taskID}, {"studyUID": new_study.studyUID, "taskID": new_taskID, "labelType": labelType, "labels": []})
        if current_user.type == 'normal' and new_study.state == 'meta_only':
            logger.info("userID {} invoke algorithm".format(userID))
            # not opening another thread,not sending socket message, not Follow up
            process_prediction(new_study.studyUID,study_info,userID,task.id,False,False)

        logger.info("userID {} update studyUID {} taskID {} state {}".format(userID, new_study.studyUID, new_taskID,study_info['state']))
        db_find_update_otherwise_insert("study", {"studyUID": study_info['studyUID']}, study_info)
        
        mutex.acquire()
        new_shared = db_find_one("ownership", {"userID": userID})
        new_shared['shared'][(new_study.studyUID).replace(".", "-")] = new_taskID
        db_update_one("ownership", {"userID": userID}, new_shared)
        mutex.release()

        new_dict = new_study.to_dict(withSeries=True)
        new_dict["diagnosisAdvice"] = new_report.diagnosisAdvice
        new_dict['submitted'] = new_report.submitted()
        new_dict['labelNum']  = 0
        new_dict['taskID'] = new_taskID
        new_dict["result"] = "pending"
        new_dict['user_privilege'] = task.get_user_privilege(userID)
        studylist.append(new_dict)

    #do follow up register here
    for study in studylist:
        do_nodule_matching(study,userID)

    #store study_infolist
    for study_info in study_infolist:
        update_study_instance_state(study_info['studyDate'].strftime('%Y%m%d'),study_info['studyUID'],study_info['state'])

    return studylist
    

def feed_pacs_study_image():
    if not current_user.is_authenticated:
        return abort(401)

    res = response()
    params = request.json
    if 'filepaths' not in params:
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)
    filepaths = params['filepaths']

    logger.info("PACS module upload file")
    res = response()
    userID = current_user.get_id()
    logger.info("process files userID {}(PACS)".format(userID))
    studylist = process_pacs_filepaths(filepaths, userID)

    res.statusCode = 1000
    res.data = studylist
    return make_response(jsonify(res.to_dict()), 200)
