import pointInFreehandROI from './pointInFreehandROI.js';

// const statisticsObj = {
//   count: 0,
//   mean: 0.0,
//   variance: 0.0,
//   stdDev: 0.0
// };


// Map a value from old range to new range
function mapRange(value, oldRange, newRange) {
  let [minOld, maxOld] = oldRange
  let [minNew, maxNew] = newRange
  return (value - minOld) / (maxOld - minOld) * (maxNew - minNew) + minNew
}

export default function (sp, boundingBox, dataHandles, actualRange=[0, 256]) {
  let sum = 0;
  let sumSquared = 0;
  let index = 0;
  let statisticsObj = {
    count: 0,
    mean: 0.0,
    variance: 0.0,
    stdDev: 0.0
  };

  for (let y = boundingBox.top; y < boundingBox.top + boundingBox.height; y++) {
    for (let x = boundingBox.left; x < boundingBox.left + boundingBox.width; x++) {
      if (pointInFreehandROI(dataHandles, {
        x,
        y
      })) {
        sum += mapRange(sp[index], [0, 256], actualRange);
        sumSquared += mapRange(sp[index], [0, 256], actualRange) * mapRange(sp[index], [0, 256], actualRange);
        statisticsObj.count++;
      }
      index++;
    }
  }

  if (statisticsObj.count === 0) {
    return statisticsObj;
  }

  statisticsObj.mean = sum / statisticsObj.count;
  statisticsObj.variance = sumSquared / statisticsObj.count - statisticsObj.mean * statisticsObj.mean;
  statisticsObj.stdDev = Math.sqrt(statisticsObj.variance);

  return statisticsObj;
}
