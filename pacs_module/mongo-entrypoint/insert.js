db = db.getSiblingDB('radiology')
db.createUser({user: 'work', pwd: 'work@imsightmed', roles: [{role: 'readWrite', db: 'radiology'},{role: 'readWrite', db: 'pacs_db'}]});
db.createCollection('study')
db.createCollection('label')
db.createCollection('ai_label')
db.createCollection('report')
db.createCollection('template')
db.createCollection('tool')
db.createCollection('user')
db.createCollection('task')
db.createCollection('ownership')
db.createCollection('liability')
db.createCollection('resource')
db.createCollection('radiomics')
db.createCollection('study_info_list')
db.user.insert({ "account" : "imsight", "hospital" : "imsight", "confirmEmail" : "true", "type" : "system", "password" : "dc919172793f290a7f612423b9764ab09b0eeccb49c9b62116bceeba82abc521", "nickname" : "imsight", "email" : "imsight@imsight.com" })
db.user.insert({ "account" : "imsightmed", "hospital" : "深圳市人民医院", "confirmEmail" : "false", "type" : "normal", "password" : "56edc6599abc939c75ebebb69bf7a433d4f0fc3c545f6583d942ed5d48a0053e", "nickname" : "imsightmed", "email" : "imsightmed@imsightmed.com" })
db.liability.insert({"reviewer" : "imsight", "reporter" : "imsightmed"})
db.ownership.insert({"shared" : {  }, "userID" : "imsightmed"})

db.template.insert({
        "group": "system",
        "templates": [
        {
            "templateID": "SYSDXCHEST001",
            "bodyPartExamined": "CHEST",
            "templateName": "正位片正常",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "胸廓骨骼及胸壁软组织未见异常。纵隔及气管居中未见移位。纵隔未见增宽。主动脉无异常，心脏形态大小未见异常。两膈光整，两肋膈角锐利。肺门形态大小位置未见异常。两肺纹理清楚，未见异常密度影。",
            "diagnosisAdvice": "胸部正位片未见异常。",
        },
        {
            "templateID": "SYSDXCHEST002",
            "bodyPartExamined": "CHEST",
            "templateName": "肺部炎症",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。两下肺纹理增多，变模糊，并可见斑点状斑片状模糊阴影，余肺野未见活动性病变，心膈未见异常。",
            "diagnosisAdvice": "两下肺感染，建议积极抗炎治疗后复查。",
        },
        {
            "templateID": "SYSDXCHEST003",
            "bodyPartExamined": "CHEST",
            "templateName": "支气管炎",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。两下肺纹理增多，增粗，紊乱，肺野未见活动性病变，心膈未见异常。",
            "diagnosisAdvice": "支气管炎。",
        },
        {
            "templateID": "SYSDXCHEST004",
            "bodyPartExamined": "CHEST",
            "templateName": "肺内感染性病变",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。右中下肺野中内带见片状模糊阴影，内密度不均匀，局部纹理增多、增粗，余肺野未见活动性病变，肺纹理走行自然，肺门不大，心脏呈斜位型，各房室未见明显增大，大血管未见明显异常改变。两膈面光滑，肋膈角锐利。",
            "diagnosisAdvice": "（右中下肺野）肺内感染性病变。建议CT进一步检查。",
        },
        {
            "templateID": "SYSDXCHEST005",
            "bodyPartExamined": "CHEST",
            "templateName": "肺结核",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "左肺中上野可见斑点状、斑片状模糊阴影，密度不均，并见有少许纤维索条状阴影，余肺野未见活动性病变，肺门不宽，心脏各房室未见明显增大，纵隔影不宽，两膈面光滑，肋膈角锐利。",
            "diagnosisAdvice": "左中上肺继发性肺结核（浸润型肺结核）。",
        },
        {
            "templateID": "SYSDXCHEST006",
            "bodyPartExamined": "CHEST",
            "templateName": "胸膜增厚",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。两下肺纹理增多，右肺野第1～2肋骨内缘见不均匀弧形较高密度影，境界尚清晰，水平叶间裂上移，余肺野未见活动性病变，心膈未见异常改变。",
            "diagnosisAdvice": "胸部后前位片示：右上胸膜增厚。",
        },
        {
            "templateID": "SYSDXCHEST007",
            "bodyPartExamined": "CHEST",
            "templateName": "浸润性肺结核",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "右肺中上野见斑点状及条索状不均匀较高密度影，境界尚清；左中肺野外带见有少许索条状密度增高影，外侧与侧胸壁相连；余肺野未见活动性病变，右肺门结构紊乱，心脏各房室未见明显增大，纵隔影不宽，两膈面光滑，肋膈角锐利。",
            "diagnosisAdvice": "胸部后前位片示：右上及左中肺浸润型肺结核，病变大致稳定，请与原片比较。",
        },
        {
            "templateID": "SYSDXCHEST008",
            "bodyPartExamined": "CHEST",
            "templateName": "支气管肺炎",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。右下肺纹理增多、增粗，并见斑点状模糊影，余肺野未见活动性病变，心膈未见异常。",
            "diagnosisAdvice": "支气管肺炎。",
        },
        {
            "templateID": "SYSDXCHEST009",
            "bodyPartExamined": "CHEST",
            "templateName": "肺气肿",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "modality": "DX",
            "imagingFind": "胸廓对称，气管居中；两肺纹理稍增强，肺野透光度增强；两肺未见活动性病变，肺门结构清晰；心脏外形、大小无明显改变；两膈面光滑，膈顶位于第11后肋以下水平；肋间隙稍增宽；肋膈角锐利。",
            "diagnosisAdvice": "胸部后前位片示：肺气肿（轻度）。",
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "两肺未见浸润性病变。",
            "imagingFind": "胸廓骨骼及胸壁软组织未见异常。  纵隔及气管居中未见移位，纵隔未见增宽。  主动脉无异常，心脏形态大小未见异常。   两膈光整，两肋膈角锐利。  肺门形态大小位置未见异常。  两肺纹理增多，结构清楚，未见浸润性病变。    ",
            "modality": "CT",
            "templateID": "SYSCTCHEST000",
            "templateName": "两肺未见浸润性病变"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "1）两下肺支气管肺炎。  2）主动脉迂曲，硬化征。",
            "imagingFind": "胸廓骨骼及胸壁软组织未见异常。  纵隔及气管居中未见移位。  纵隔未见增宽。  主动脉无异常，迂曲，主动脉结有钙化，心脏形态大小未见异常。   两膈光整，两肋膈角锐利。  肺门形态大小位置未见异常。  两下肺中内带纹理增多模糊，沿肺纹理可见小斑片状模糊渗出影。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST001",
            "templateName": "两下肺支气管肺炎"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "1）慢支肺气肿。  2）主动脉迂曲，主动脉硬化征。",
            "imagingFind": "胸廓呈桶状，骨骼及胸壁软组织未见异常。  纵隔及气管居中未见移位。  纵隔未见增宽。   主动脉无异常，主动脉迂，主动脉结内有钙化，心影瘦长。  两膈光整位置低平，两肋膈角锐利。  肺门形态大小位置未见异常。  两肺野透光度增强，两肺纹理增多紊乱。  肋间隙增宽。",
            "modality": "CT",
            "templateID": "SYSCTCHEST002",
            "templateName": "慢支肺气肿"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "两侧肺野多发球形病灶影，考虑转移性病变，请与前片比较。  ",
            "imagingFind": "所示胸廓骨骼及胸壁软组织未见异常。  纵隔及气管居中未见移位。  纵隔未见增宽。  心脏形态大小未见异常。  两膈光整，两肋膈角锐利。  肺门形态大小位置未见异常。  两侧肺野可见多发大小不一的棉花团状密度增高影。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST003",
            "templateName": "肺转移"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": " 肺下叶内段不张，建议增强检查。纵隔内多处钙化影。  ",
            "imagingFind": " 肺下叶内段膨胀不全，内可见支气管影，其余肺野未见异常密度影，两肺门区可见多处钙化影。纵隔内未见异常增大的淋巴结。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST004",
            "templateName": "肺下叶内段不张"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右，左肺Ca切除术后复查：  1、右，左肺叶切除后，右，左叶复张良好。  2、右，左胸套式胸膜增厚。  3、左，右胸无异常。",
            "imagingFind": "胸廓不对称，右，左胸狭小，右，左肺野透光度均匀性降低， 膈升高，轮廓变平直，外高内低，后缘略呈弧形，突向上方，下心缘被遮盖，气管、纵隔无明显移位。   胸无异常。",
            "modality": "CT",
            "templateID": "SYSCTCHEST005",
            "templateName": "肺切除术后改变"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "两肺所见符合细支气管肺泡炎。",
            "imagingFind": "胸廓对称，肋骨无畸形，胸壁软组织无异常。   两下肺呈弥漫小点状及细网状结构，肺野透光度下降，似磨砂玻璃状，肺纹理结构已无法辨别。  双侧肺门结构欠清晰，上肺静脉无增宽，位置、形态、大小、密度无异常。  气管、纵隔居中，纵隔无增宽。  双侧横膈光整，肋膈角锐利。        主动脉无异常，心影形态、大小无异常。    胸膜无异常。",
            "modality": "CT",
            "templateID": "SYSCTCHEST006",
            "templateName": "细支气管肺泡炎"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "1）两肺弥漫间质性改变（间质纤维化？）未见实质病灶，性质请结合临床。  2）无肺动脉高压征，心影无增大。",
            "imagingFind": "胸廓骨骼及胸壁软组织未见异常。  纵隔及气管居中未见移位。  纵隔未见增宽。  主动脉无异常，心影形态大小未见异常。   两膈光整，两肋膈角锐利。  肺门形态大小位置未见异常，右下肺动脉无增宽。  两肺纹理明显增多紊乱，呈枯枝状，且交织成网格，未见实质病灶。",
            "modality": "CT",
            "templateID": "SYSCTCHEST007",
            "templateName": "两肺间质纤维化"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右，左肺结节影，结合病史考虑肺转移可能性大。",
            "imagingFind": "胸廓对称，肋骨无畸形，胸壁软组织无异常。  右，左肺有 个圆形密影，密度均匀，边缘清楚，大如指甲，小如豆大，余肺野清晰。    双侧肺门位置、形态、大小、密度无异常。  气管、纵隔居中，纵隔无增宽。  双侧横膈光整、肋膈角锐利。        主动脉无异常，心影形态大小无异常。    胸膜无异常。",
            "modality": "CT",
            "templateID": "SYSCTCHEST008",
            "templateName": "肺转移瘤 "
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "左上肺癌，纵隔及左肺门淋巴结转移，并累及胸壁。",
            "imagingFind": "平扫图像显示：左侧胸廓塌陷，左肺上叶尖后段见一不规则软组织肿块，其密度为36HU，密度不均，与胸壁关系密切，邻近肋骨骨质破坏，并向外形成软组织块影；  增强图像显示：  肿块早期强化较显著，密度较高且不均匀，其密度为108HU。邻近胸膜软组织亦见强化。左侧肺门区及纵隔内可见淋巴结肿大。  左下肺动脉显示不清。",
            "modality": "CT",
            "templateID": "SYSCTCHEST009",
            "templateName": "左上肺癌累及胸壁"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "左下肺支气管扩张伴感染、肺气肿",
            "imagingFind": "肺窗：左肺下叶可见沿支气管走行多发囊柱状透亮影，大小不均，从中央到外周逐渐增多。左肺上叶舌段亦可见少量圆形囊状影。囊柱状透亮影旁可见血管影，呈“印戒”征，其内壁光滑，部分区域可见气液平面，周围可见小斑片模糊影，对应区域肺透亮度增高。其余肺组织透过度良好，血管及支气管未见异常。  纵隔窗：双侧肺门及纵隔未见增大淋巴结。血管结构清晰，心脏形态未见增大。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST010",
            "templateName": "支气管扩张症伴感染、肺气肿"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "慢性支气管炎、肺气肿",
            "imagingFind": "肺窗：双肺透过度弥漫性增高，支气管壁增厚，呈“轨道”征，管腔略扩张但通畅。血管走形正常。  纵隔窗：双侧肺门及纵隔未见增大淋巴结。血管结构清晰，心脏形态未见增大。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST011",
            "templateName": "慢性支气管炎、肺气肿"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右肺感染性病变。",
            "imagingFind": "肺窗：右肺上叶后段可见实变影，其前缘不规则，前段可见散在小片状渗出影，后缘以斜裂为边界与下叶形成清楚界线。双侧叶间裂基本对称，实变影内可见充气支气管影，充气管壁光整，管腔无明显扩张。纵隔窗：纵隔无明显移位，纵隔内未见确切增大淋巴结影，肺内病变范围小于肺窗，形态同肺窗一般，密度均匀一致。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST012",
            "templateName": "肺感染（大片）"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右肺感染性病变，肺脓肿形成。",
            "imagingFind": "肺窗：右肺上叶可见斑片状及大片状密度增高影，边界不清，其内可见空洞形成，空洞壁厚薄不均并见气-液面，其下缘以水平裂及斜裂为界，边缘较清晰，邻近胸膜增厚。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST013",
            "templateName": "肺脓肿"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "肺曲霉菌(曲菌球形成)",
            "imagingFind": "肺窗：右肺上叶见一薄壁空洞，内见球形病灶呈软组织密度，边缘光滑、锐利，大小：9mmX7mm，其内见空气伴月征。增强检查无强化。  纵隔窗：双侧肺门及纵隔未见增大淋巴结。血管结构清晰，心脏形态未见增大。",
            "modality": "CT",
            "templateID": "SYSCTCHEST014",
            "templateName": "肺曲霉菌球"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "变应性肺曲霉菌病。",
            "imagingFind": "肺窗：从肺门开始沿中心亚段支气管及段支气管分布的分支管状阴影，呈“指套状”表现。其内可见略高密度粘液充填。以上叶及位于近中心的肺门周围区为著。  纵隔窗：双侧肺门及纵隔未见增大淋巴结。血管结构清晰，心脏形态未见增大。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST015",
            "templateName": "变应性肺曲霉菌病"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "左下肺陈旧索条。",
            "imagingFind": "肺窗：左肺下叶见少许索条影，邻近胸膜略增厚。余肺野清晰，支气管通畅。  纵隔窗：双侧肺门及纵隔未见增大淋巴结。血管结构清晰，心脏形态未见增大。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST016",
            "templateName": "左下肺陈旧索条"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "双肺结核",
            "imagingFind": "肺窗：右肺下叶背段及后基底段可见多发结节及索条影，右下肺结节部分呈簇状分布。相应肺容积无明显缩小。其余肺野未见异常。  纵隔窗：右肺下叶实变内部密度较均匀。右肺门增大，在上腔静脉后可见增大并钙化的淋巴结。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST017",
            "templateName": "肺结核伴支气管播散"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "血行播散性肺结核",
            "imagingFind": "肺窗：双肺透过度下降。肺内可见弥漫性分布1mm左右小结节影，结节分布在肺内外带及上下肺均无差异。结节边界清楚。肺容积及血管形态无改变。未见小叶间隔增厚。  纵隔窗：纵隔形态及大小未见异常，纵隔内未见增大淋巴结。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST018",
            "templateName": "血行播散性肺结核"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "肺结核伴空洞形成",
            "imagingFind": "肺窗：双肺尖可见条索状影，左上叶可见4cm×5cm大小空洞，空洞壁厚薄不均，内外壁均不规则，内壁未见壁在结节。空洞周围肺结构牵拉变形，与外侧壁胸膜之间可见明显胸膜粘连。双下肺散在小叶中央性结节及树芽征，病变均匀弥漫性分布。  纵隔窗：纵隔内未见增大淋巴结影，左上肺空洞壁为均匀软组织密度，未见确切钙化囊块、空洞周围的胸膜粘连区内可见胸膜下脂肪沉着。右侧后胸壁胸膜肥厚。",
            "modality": "CT",
            "templateID": "SYSCTCHEST019",
            "templateName": "肺结核伴空洞形成"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "特发性肺间质纤维化",
            "imagingFind": "肺窗：双肺可见弥漫性小叶间隔增厚及小叶内间质增厚，病变主要分布于双肺下叶和部分右肺上叶，呈外周性分布，胸膜下区可见蜂窝肺样改变。血管支气管束未见明显增粗。双肺结构扭曲，下叶支气管轻度牵拉扩张。双肺未见实变及磨玻璃样改变。  纵隔窗：纵隔内未见增大淋巴结，主动脉及冠状动脉区可见多发钙化斑块。胸膜未见明显增厚。",
            "modality": "CT",
            "templateID": "SYSCTCHEST020",
            "templateName": "特发性肺间质纤维化"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "间质性肺炎",
            "imagingFind": "肺窗：双下肺及胸膜下区可见磨玻璃及网格影，部分区域可见蜂窝样改变。局部肺结构牵拉扭曲，并见牵拉性支气管、细支气管扩张。  纵隔窗：纵隔内未见增大淋巴结。胸膜未见明显增厚。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST021",
            "templateName": "间质性肺炎（网格）"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "间质性肺炎，隐原性机化性肺炎（COP）可能",
            "imagingFind": "肺窗：双肺可见多发沿支气管、血管周围分布的条片状密度增高影，内见支气管扩张影，部分可见中央密度较低，呈“反晕征”。  纵隔窗：纵隔内未见增大淋巴结。胸膜未见明显增厚。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST022",
            "templateName": "间质性肺炎（COP）"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "结合接触史，考虑尘肺",
            "imagingFind": "肺窗：双肺弥漫性对称分布小结节，大小不等，直径约：2-5mm，边缘清楚，密度较高，部分钙化，以中内带肺野分布为主，以上中肺野为著；双上肺结构略变形。  纵隔窗：双侧肺门见多发增大淋巴结，部分见蛋壳样钙化。纵隔内未见增大淋巴结。胸膜未见明显增厚。",
            "modality": "CT",
            "templateID": "SYSCTCHEST023",
            "templateName": "尘肺"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "结合病史，考虑肺出血",
            "imagingFind": "肺窗：右肺下叶见片状磨玻璃密度影，边界不清。气管、支气管通畅，未见狭窄。叶间裂无增厚及移位。  纵隔窗：胸廓对称，胸廓骨质结构及软组织未见异常；双肺门未见增大。纵隔内未见异常增大淋巴结影。血管结构清晰，心脏形态未见增大。",
            "modality": "CT",
            "templateID": "SYSCTCHEST024",
            "templateName": "肺出血"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "肺泡蛋白沉着症",
            "imagingFind": "肺窗： HRCT呈多发地图状分布磨玻璃密度影及光滑的小叶间隔增厚，即“碎石路”征。病变内肺结构无扭曲，未见牵拉性支气管扩张。病变外肺野透过度良好。  纵隔窗：胸廓对称，胸廓骨质结构及软组织未见异常；双肺门未见增大。纵隔内未见异常增大淋巴结影。血管结构清晰，心脏形态未见增大。",
            "modality": "CT",
            "templateID": "SYSCTCHEST025",
            "templateName": "肺泡蛋白沉着症"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "小叶中央肺气肿",
            "imagingFind": "肺窗：双肺可见弥漫性分布密度减低区，光滑无边缘，主要分布于双肺上叶，支气管通畅，管径正常。  纵隔窗：双肺门及纵隔未见增大淋巴结。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST026",
            "templateName": "双肺弥漫性小叶中央型肺气肿"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "肺水肿",
            "imagingFind": "肺窗：双肺中内带片状磨玻璃及实变影，呈蝶翼状分布，血管支气管束及小叶间隔增厚；支气管通畅。  纵隔窗：胸廓对称，胸廓骨质结构及软组织未见异常；双肺门影略增大。纵隔内未见异常增大淋巴结影。血管结构清晰，心脏增大，以左房、左室为主。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST027",
            "templateName": "肺水肿"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "肺内良性肿瘤，错构瘤可能。",
            "imagingFind": "肺窗：右肺中叶可见2cm×2.5cm大小类圆形软组织结节，边界整齐，未见脐凹及明显分叶，其内可见不规则低密度区，CT值在-40-0HU之间，部分边缘可见小钙化点，纵隔窗：纵隔形态正常，未见增大淋巴结。增强扫描：结节整体无明显强化，与平扫相比CT值无明显变化。其内未见增强血管影。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST028",
            "templateName": "肺错构瘤"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "左下肺中央型肺癌伴远端阻塞改变",
            "imagingFind": "肺窗：左下肺支气管闭塞，周围可见肿块影，自左上叶支气管开口部起支气管完全闭塞，气管壁增厚、远段肺组织内未见充气支气管征。远端不张的肺组织密度均匀。纵隔轻度左移。左后侧胸壁增厚。其余肺组织内未见异常。  纵隔窗：主肺动脉窗间隙内可见数个增大淋巴结影。左后侧壁胸膜肥厚。骨窗：各胸骨、肋骨和胸椎未见确切骨质异常。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST029",
            "templateName": "左下肺中央型肺癌"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "考虑左肺上叶中央型肺癌伴阻塞性肺炎，纵隔淋巴结转移。建议结合支气管镜检查。",
            "imagingFind": "\"肺窗:左肺上叶前段见不规则形软组织块影，大小约：29mmX25mm，密度不均，CT值：25-36HU,增强后呈边缘环形明显强化，中心大部分无强化，CT值：27-65HU。肿块与左肺动脉广基相连，境界不清。病灶远端见模糊斑片、结节影。左肺上叶前段支气管闭塞。  纵隔窗：纵隔内气管前腔静脉后、隆突下、主肺动脉窗内见多发短径约1.8cm以下异常增大的淋巴结，强化后呈环形强化。双肺门未见明确异常增大淋巴结。  \"",
            "modality": "CT",
            "templateID": "SYSCTCHEST030",
            "templateName": "左上肺中央型肺癌"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "结合病史，考虑肺转移可能  ",
            "imagingFind": "肺窗：双肺可见随机分布圆形小结节影，病灶多位于双肺外带，大小从5mm到1.5cm不等，病灶内部未见空洞及钙化影，边缘光滑。肺纹理形态未见异常。  纵隔窗：纵隔内未见增大淋巴结影。心脏及大血管影未见异常。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST031",
            "templateName": "肺转移瘤",
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右上肺发育异常",
            "imagingFind": "肺窗：右肺上叶容积减小，支气管扩张，血管变细，水平裂上移，可见纵隔及心脏右移。余肺野未见异常。  纵隔窗：胸廓对称，胸廓骨质结构及软组织未见异常；双肺门未见增大。纵隔内未见异常增大淋巴结影。血管结构清晰，心脏形态未见增大。",
            "modality": "CT",
            "templateID": "SYSCTCHEST032",
            "templateName": "右上肺发育异常"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右下肺先天性病变，肺隔离症。",
            "imagingFind": "肺窗：右下肺脊柱旁可见局限性高透光区，内部纹理明显少于周围肺组织，其内可见分枝状结构，增强后无强化。其余肺野透过度良好，肺纹理走形正常。  纵隔窗（增强）：纵隔内未见增大淋巴结影，心影正常，双肺门不大。增强后右下肺见一增粗血管影，自胸主动脉发出供应右下肺病变区。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST033",
            "templateName": "肺隔离症"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "肺栓塞",
            "imagingFind": "肺窗：双肺内未见异常密度影，肺透过度不均匀；支气管通畅。纵隔窗（增强）：纵隔内未见增大淋巴结影，左右心室及左右心房均见对比剂充盈。左右下肺动脉内可见充盈缺损，呈轨道征。肺动脉远端分支充盈良好。其余软组织未见异常。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST034",
            "templateName": "肺栓塞"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右下肺肺动脉栓塞并肺梗死",
            "imagingFind": "肺窗：右下肺胸膜下见楔形实变及磨玻璃密度影，边缘模糊，尖端指向肺门。纵隔窗（增强）：纵隔内未见增大淋巴结影，左右心室及左右心房均见对比剂充盈。右下肺动脉及分支内可见充盈缺损。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST035",
            "templateName": "肺梗死"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "左肺下叶动静脉瘘",
            "imagingFind": "肺窗：左肺下叶可见不规则结节影，界限清楚，并见与迂曲增粗的引流动静脉血管相连。其余肺野透过度良好，肺纹理未见异常。纵隔窗（增强）：纵隔内未见增大淋巴结影，左下肺病变呈均匀软组织密度影，增强后可见左下肺病变呈血管样强化，与增强的肺动脉及肺静脉相连。内部未见充盈缺损影。未见对比剂外溢。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST036",
            "templateName": "肺动静脉瘘"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右肺上叶切除术后改变",
            "imagingFind": "肺窗：右肺上叶支气管见夹闭，断端见高密度缝合线。残余肺组织代偿上移，右肺内见少许纤维索条影。  纵隔窗：纵隔内未见异常增大淋巴结。心脏大血管形态未见异常，局限胸膜增厚。",
            "modality": "CT",
            "templateID": "SYSCTCHEST037",
            "templateName": "右上肺术后复查"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "右肺癌化疗后：肺内病变及纵隔淋巴结转移较前缩小；双肺间质性改变。",
            "imagingFind": "肺窗：右肺上叶病变大小1cmx2cm，形态不规则，周围长索条影。双肺下叶胸膜下见带状磨玻璃密度影及细网状影、小条形影。  纵隔窗：纵隔内原增大淋巴结，现明显缩小。心脏大血管形态未见异常。",
            "modality": "CT",
            "templateID": "SYSCTCHEST038",
            "templateName": "右上肺癌治疗后复查"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "双肺感染性病变。",
            "imagingFind": "肺窗：双肺散在多发小片状实变阴影伴有磨玻璃样阴影，沿支气管分布，支气管通畅。肺内散在小叶状分布密度减低区。  ",
            "modality": "CT",
            "templateID": "SYSCTCHEST039",
            "templateName": "肺感染（小斑片）"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "胸部正位片未见异常。",
            "imagingFind": "胸廓骨骼及胸壁软组织未见异常。纵隔及气管居中未见移位。纵隔未见增宽。主动脉无异常，心脏形态大小未见异常。两膈光整，两肋膈角锐利。肺门形态大小位置未见异常。两肺纹理清楚，未见异常密度影。",
            "modality": "DX",
            "templateID": "SYSDXCHEST000",
            "templateName": "正位片正常"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "两下肺感染，建议积极抗炎治疗后复查。",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。两下肺纹理增多，变模糊，并可见斑点状斑片状模糊阴影，余肺野未见活动性病变，心膈未见异常。",
            "modality": "DX",
            "templateID": "SYSDXCHEST001",
            "templateName": "肺部炎症"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "支气管炎。",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。两下肺纹理增多，增粗，紊乱，肺野未见活动性病变，心膈未见异常。",
            "modality": "DX",
            "templateID": "SYSDXCHEST002",
            "templateName": "支气管炎"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "（右中下肺野）肺内感染性病变。建议CT进一步检查。",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。右中下肺野中内带见片状模糊阴影，内密度不均匀，局部纹理增多、增粗，余肺野未见活动性病变，肺纹理走行自然，肺门不大，心脏呈斜位型，各房室未见明显增大，大血管未见明显异常改变。两膈面光滑，肋膈角锐利。",
            "modality": "DX",
            "templateID": "SYSDXCHEST003",
            "templateName": "肺内感染性病变"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "左中上肺继发性肺结核（浸润型肺结核）。",
            "imagingFind": "左肺中上野可见斑点状、斑片状模糊阴影，密度不均，并见有少许纤维索条状阴影，余肺野未见活动性病变，肺门不宽，心脏各房室未见明显增大，纵隔影不宽，两膈面光滑，肋膈角锐利。",
            "modality": "DX",
            "templateID": "SYSDXCHEST004",
            "templateName": "肺结核"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "胸部后前位片示：右上胸膜增厚。",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。两下肺纹理增多，右肺野第1～2肋骨内缘见不均匀弧形较高密度影，境界尚清晰，水平叶间裂上移，余肺野未见活动性病变，心膈未见异常改变。",
            "modality": "DX",
            "templateID": "SYSDXCHEST005",
            "templateName": "胸膜增厚"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "\"胸部后前位片示：右上及左中肺浸润型肺结核，病变大致稳定，请与原片比较。\"",
            "imagingFind": "右肺中上野见斑点状及条索状不均匀较高密度影，境界尚清；左中肺野外带见有少许索条状密度增高影，外侧与侧胸壁相连；余肺野未见活动性病变，右肺门结构紊乱，心脏各房室未见明显增大，纵隔影不宽，两膈面光滑，肋膈角锐利。",
            "modality": "DX",
            "templateID": "SYSDXCHEST006",
            "templateName": "浸润性肺结核"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "支气管肺炎。",
            "imagingFind": "胸廓对称，所见骨质结构完整，未见异常改变。右下肺纹理增多、增粗，并见斑点状模糊影，余肺野未见活动性病变，心膈未见异常。",
            "modality": "DX",
            "templateID": "SYSDXCHEST007",
            "templateName": "支气管肺炎"
        },
        {
            "bodyPartExamined": "CHEST",
            "createDate": new Date("2016-05-18T16:00:00Z"),
            "diagnosisAdvice": "胸部后前位片示：肺气肿（轻度）。",
            "imagingFind": "胸廓对称，气管居中；两肺纹理稍增强，肺野透光度增强；两肺未见活动性病变，肺门结构清晰；心脏外形、大小无明显改变；两膈面光滑，膈顶位于第11后肋以下水平；肋间隙稍增宽；肋膈角锐利。",
            "modality": "DX",
            "templateID": "SYSDXCHEST008",
            "templateName": "肺气肿"
        }
    ]
})
db = db.getSiblingDB('pacs_db')
db.createCollection('system_label')
db.createCollection('study_instance_list')
