#!/bin/sh
echo "======== docker containers logs file ========"
logs=$(find /var/lib/docker/containers/ -name *-json.log)
if [ -d "docker_logs/" ];then
rm -rf docker_logs
fi
mkdir docker_logs
for log in $logs
do
ls -lh $log
cp $log docker_logs/
done

chmod 777 -R docker_logs
