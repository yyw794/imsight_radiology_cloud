import React, { Component } from 'react'
import { DatePicker } from 'antd';
import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'


import './index.less'

/**
 * 日期范围选择组件
 */
class DateRange extends React.Component {
  /**
   * 参照生命周期函数
   */
  constructor(props) {
    super(props)
      /**
       *
       * @type {{
       * startValue: null, 开始值
       * endValue: null, 结束值
       * endOpen: boolean 是否打开
       * }}
       */
    this.state = {
      startValue: props.value ? props.value[0] : null,
      endValue: props.value ? props.value[1] :null,
      endOpen: false,
    };

  }

    /**
     * 参照生命周期函数
     * @param nextProps
     */
  componentWillReceiveProps(nextProps){
    this.setState({
      startValue: nextProps.value ? nextProps.value[0] : null,
      endValue: nextProps.value ? nextProps.value[1] :null,
      endOpen: false
    })
  }

    /**
     * 禁用开始日期
     * @param startValue
     * @returns {boolean}
     */
  disabledStartDate = (startValue) => {
    const endValue = this.state.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.isAfter(endValue,"day");
  }
    /**
     * 禁用结束日期
     * @param endValue
     * @returns {boolean}
     */
  disabledEndDate = (endValue) => {
    const startValue = this.state.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.isBefore(startValue,"day");
  }
    /**
     * 时间更改
     * @param field
     * @param value
     */
  onChange = (field, value) => {
    this.setState({
      [field]: value,
    }, () => {
      this.props.onChange([this.state.startValue, this.state.endValue])
    });
  }

    /**
     * 开始时间值变化事件
     * @param value
     */
  onStartChange = (value) => {
    this.onChange('startValue', value);
  }
    /**
     * 结束时间值变化事件
     * @param value
     */
  onEndChange = (value) => {
    this.onChange('endValue', value);
  }
  /**
   * 开始时间打开时间选项框
   */
  handleStartOpenChange = (open) => {
    if (!open && !this.state.endValue) {
      this.setState({ endOpen: true });
    }
  }
    /**
     * 结束时间打开时间选项框
     * @param open
     */
  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  }

    /**
     * table查询条件日期时间组件
     * @returns {Component}
     */
  render() {
    const { startValue, endValue, endOpen } = this.state;
    return (
      <div className='date-range-picker'>
        <DatePicker
          disabledDate={this.disabledStartDate}
          showTime={false}
          format="YYYY-MM-DD"
          value={startValue}
          placeholder={GetLocaledText(this, "DateRange.start")}
          onChange={this.onStartChange}
          onOpenChange={this.handleStartOpenChange}
        />
        <DatePicker
          disabledDate={this.disabledEndDate}
          showTime={false}
          format="YYYY-MM-DD"
          value={endValue}
          placeholder={GetLocaledText(this, "DateRange.end")}
          onChange={this.onEndChange}
          open={endOpen}
          onOpenChange={this.handleEndOpenChange}
        />
      </div>
    );
  }
}

export default injectIntl(DateRange)
