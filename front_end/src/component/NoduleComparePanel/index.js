import React, { Component } from 'react';
import NoduleCompareTable from '../NoduleCompareTable'
import {getRiskText, getRiskColor, getNoduleType, POS_IMG, RISK_EN2CN, TYPE_EN2CN} from '../NodulePanel'
import {Table, Row, Col, Icon, Modal} from 'antd'
import _ from 'underscore'
import Immutable from 'immutable'

import '../NodulePanel/index.less'
import './index.less'
import {GetLocaledText} from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'
function isLeftNoduleRiskBigger(nLeft,nRight)
{
    let left = -1
    let right =-1
    if(getRiskText(nLeft) === "High") left = 3
    if(getRiskText(nLeft) === "Middle") left = 2
    if(getRiskText(nLeft) === "Low") left = 1
    if(getRiskText(nLeft) === "Benign") left = 0

    if(getRiskText(nRight) === "High") right = 3
    if(getRiskText(nRight) === "Middle") right = 2
    if(getRiskText(nRight) === "Low") right = 1
    if(getRiskText(nRight) === "Benign") right = 0
    return left > right
}
/**
 * 节点对比列表组件
 */
class NoduleComparePanel extends Component {
    /**
     * 参照生命周期函数
     * @param props
     */
  constructor(props) {
    super(props)
        /**
         * 传入组建数据初始化排序
         */
    this.originData = Immutable.fromJS(this.props.data).sort(this.getSorter('nodule_prob', true))
        /**
         *
         * @private {{
         * compareData: null,比较数据
         * data: *, 原数据
         * sortColumn: string 对比列名
         * }}
         */
    this.state = {
      compareData: null,
      data: this.originData,
      reverse:false,
      sortColumn: 'nodule_prob'
    }
    this.sortBy=this.sortBy.bind(this);
        /**
         * 对比组件分页pageSize
         * @private {number}
         */
    this.pageSize = window.screen.width < window.screen.height ? 13 : (window.screen.height-550)/40
  }

    /**
     * 根据设备屏幕的高度设置对比列表分页条数
     */
  onResize = () => {
    this.pageSize = window.screen.width < window.screen.height ? 13 : (window.screen.height-550)/40
  }

    /**
     * 参照react生命周期函数
     */
  componentDidMount() {
    window.addEventListener("resize", _.debounce(this.onResize, 100)) //Jackey: Why delay it by using _.debounce, but not simply passing this.onResize?
    //window.addEventListener("resize", this.onResize);
  }

    /**
     * 参照react生命周期函数
     * @param nextProps
     */
  componentWillReceiveProps(nextProps) {
    this.originData = Immutable.fromJS(nextProps.data).sort(this.getSorter('nodule_prob', true))
    this.sortBy(this.state.sortColumn)
  }
  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   return
  //   this.originData = Immutable.fromJS(this.props.data).sort(this.getSorter('nodule_bm_prob', true))
  //   this.sortBy(this.state.sortColumn)
  // }

    /**
     * 比较函数
     * @param col 列名
     * @param reverse 是否倒序
     * @returns {Function}
     */
  getSorter(col, reverse=false) {
    return (a, b) => {
      let v1 = a.get(0).get(col)
      let v2 = b.get(0).get(col)
      if (v1 < v2) {
        return reverse ? 1 : -1
      }
      if (v1 > v2) {
        return reverse ? -1 : 1
      }
      if (v1 === v2) {
        return 0
      }
    }
  }
renderMalgTrend(data) {
        if (data[0].nodule_bm.Malignant === data[1].nodule_bm.Malignant&&data[0].nodule_bm.Benign === data[1].nodule_bm.Benign&&data[0].nodule_bm.Mainly_Benign === data[1].nodule_bm.Mainly_Benign &&data[0].nodule_bm.Mainly_Malignant === data[1].nodule_bm.Mainly_Malignant) {
            return ''
        } else if ( isLeftNoduleRiskBigger(data[0].nodule_bm,data[1].nodule_bm)) {
            return <img className='malg-arrow' style={{width:"10px",marginLeft:"10px"}} src={require('../../../static/images/arrow_increase.png')}/>
        } else {
            return <img className='malg-arrow'  style={{width:"10px",marginLeft:"10px"}} src={require('../../../static/images/arrow_reduce.png')}/>
        }
    }

    /**
     * 根据什么列名排序
     * @param col 列名
     */
  sortBy(col) {
    let that=this;
    if (["key",'nodule_diameter', 'nodule_prob', 'nodule_avgHU'].includes(col)) {
        this.setState({
            reverse:!that.state.reverse
        })
    }
    let data = this.originData.sort(this.getSorter(col,that.state.reverse))
    this.setState({
      data,
      sortColumn: col
    })
  }

    /**
      * @param pair
     * @returns {number}
     */
  getPairPage(pair) {
    let page = 1
    let idx = this.state.data.findIndex(p => p.getIn([0, 'labelID']) === pair[0].labelID && p.getIn([1, 'labelID']) === pair[1].labelID)
    return Math.ceil((idx + 1) / this.pageSize)
  }

    /**
     *
     * @param compareData
     */
  selectPair(compareData) {
    let currentPage = this.getPairPage(compareData)
    this.setState({compareData, currentPage})
    _.isFunction(this.props.onSelectNodule) && this.props.onSelectNodule(compareData)
  }

    /**
     * 随访对比组件
     * @return {Component}
     */
  render() {
    const columns = [
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.index"),
        dataIndex: 'key',
        align:"center",
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              //this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              {/*<p>{`H${record[0].key}`}</p>*/}
              <p>{!record[1]['H']?`N${record[1].key}`:`H${record[1].key}`}
                 {
                    record[1]["+/-"]?record[1]["+/-"]=="+"?
                    <span style={{fontSize:"18px",fontWeight:"bold",marginLeft:"5px"}}>+</span>:
                    record[1]["+/-"]=="-"?
                    <span  style={{fontSize:"18px",fontWeight:"bold",marginLeft:"5px"}}>-</span>:"":""
                   }
                </p>
            </div>
          )
        }
      },
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.diameter"),
        dataIndex: 'nodule_diameter',
        align:"center",
        onHeaderCell: (column) => {
          return {
            onClick: () => {
             // this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              {/*<p>{record[0].nodule_diameter.toFixed(1)}</p>*/}
              <p style={{color:"#009ADF",fontWeight:"bold",fontSize:"16px"}}>{record[1].nodule_diameter.toFixed(1)}<span>{record[0].nodule_diameter.toFixed(1)!==record[1].nodule_diameter.toFixed(1)?record[1].nodule_diameter.toFixed(1)>record[0].nodule_diameter.toFixed(1)?<img className='malg-arrow' style={{width:"10px",marginRight:"-20px",marginLeft:"10px"}} src={require('../../../static/images/arrow_increase.png')}/>:<img className='malg-arrow'  style={{width:"10px",marginRight:"-20px",marginLeft:"10px"}} src={require('../../../static/images/arrow_reduce.png')}/>:""}</span></p>
            </div>
          )
        }
      },
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.subclass"),
        dataIndex: 'nodule_avgHU',
          align:"center",
        onHeaderCell: (column) => {
          return {
            onClick: () => {
             // this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              {/*<p>{TYPE_EN2CN[getNoduleType(record[0])]}</p>*/}
              <p style={{color:"#76D0E4",fontWeight:"bold",fontSize:"16px"}}>{TYPE_EN2CN[getNoduleType(record[1])]}</p>
            </div>
          )
        }
      },{
            title: GetLocaledText(this,"NoduleComparePanel.table-header.malignancy"),
            dataIndex: 'nodule_bm',
            align:"center",
            onHeaderCell: (column) => {
                return {
                    onClick: () => {
                        // this.sortBy(column.dataIndex)
                    }
                }
            },
            render: (value, record) => {
                return (
                    <div>
                    {/*<p style={{color: getRiskColor(record[0].nodule_bm)}}>{RISK_EN2CN[getRiskText(record[0].nodule_bm)]}</p>*/}
                    <p style={{color: getRiskColor(record[1].nodule_bm),fontWeight:"bold",fontSize:"16px"}}>{RISK_EN2CN[getRiskText(record[1].nodule_bm)]}{<span>{this.renderMalgTrend(record)}</span>}</p>
                </div>
                )
            }
        },
      // {
      //   title: GetLocaledText(this,"NoduleComparePanel.table-header.z"),
      //   dataIndex: 'coord',
      //   onHeaderCell: (column) => {
      //     return {
      //       onClick: () => {
      //         this.sortBy(column.dataIndex)
      //       }
      //     }
      //   },
      //   render: (value, record) => {
      //     return (
      //       <div>
      //         {/*<p>{this.props.zReverse1?(this.props.maxZ1-record[0].coord.z+1):record[0].coord.z}</p>*/}
      //         <p>{this.props.zReverse2?(this.props.maxZ2-record[1].coord.z+1):record[1].coord.z}</p>
      //       </div>
      //     )
      //   }
      // },
      //   {/*{*/}
      //   {/*title: GetLocaledText(this,"NoduleComparePanel.table-header.location"),*/}
      //   {/*dataIndex: 'nodule_location',*/}
      //   {/*onHeaderCell: (column) => {*/}
      //     {/*return {*/}
      //       {/*onClick: () => {*/}
      //         {/*this.sortBy(column.dataIndex)*/}
      //       {/*}*/}
      //     {/*}*/}
      //   {/*},*/}
      //  {/* render: (value, record) => {*/}
      //     {/*return (*/}
      //       {/*<div>*/}
      //        {/* <img src={POS_IMG[record[0].nodule_location]} className='location'/>*/}
      //      {/* </div>*/}
      //    {/* )*/}
      //   {/*}*/}
      // {/*},*/}
      //   {
      //       title: GetLocaledText(this,"NodulePanel.table-header.change"),
      //       dataIndex: 'nodule_location',
      //       onHeaderCell: (column) => {
      //           return {
      //               onClick: () => {
      //                   this.sortBy(column.dataIndex)
      //               }
      //           }
      //       },
      //       render: (value, record) => {
      //           return (
      //               <span style={{fontSize:'20px'}}>{this.renderMalgTrend(record)}</span>
      //           )
      //       }
      //   },
    ]
    return (
      <div className='nodule-panel nodule-compare-panel'>
        <div className='nodule-table'>
          <Table
            columns={columns}
            dataSource={this.state.data.toJS()}
            rowClassName={(record) => {
              const compareData = this.state.compareData
              return compareData && record[0].labelID === compareData[0].labelID ?
                'active' : ''
            }}
            size="small"
            pagination={{
              pageSize: this.pageSize,
              simple: true,
              showTotal: (total, range) => <span>共<span className='nodule-count'>{total}</span>个结节</span>,
              onChange: (currentPage) => this.setState({currentPage}),
              current: this.state.currentPage
            }}
            onRow={record => {
              return {
                onClick: () => {
                  this.setState({compareData: record})
                  _.isFunction(this.props.onSelectNodule) && this.props.onSelectNodule(record)
                }
              }
            }}/>
          {
            this.state.data.size > 0 ?
            <span className='ant-pagination-total-text'>共<span className='nodule-count'>{this.state.data.size}</span>条匹配</span>
            :
            ''
          }
        </div>
        <NoduleCompareTable data={this.state.compareData}/>
      </div>
    );
  }

}

export default injectIntl(NoduleComparePanel, {withRef: true});
