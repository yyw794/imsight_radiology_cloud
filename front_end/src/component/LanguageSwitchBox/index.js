import React from 'react'
import { injectIntl } from 'react-intl'
import { Button, Radio } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import './index.less'

import Cookies from 'js-cookie'
import { connect } from 'react-redux'
import {setLanguage} from '../../actions'

/**
 * 语言选项单选框
 */
class LanguageSwitchBox extends React.Component {
  /**
   * react生命周期函数
   */
  constructor(props) {
    super(props);
  }

    /**
     * 切换事件
     * @param e
     */
  handleChange = (e) => {
    //console.log(e);
    let langstr = e.target.value;  //e.target.value OR e
    // this.props.setAppState({ currentLocale: langstr});
    this.props.dispatch(setLanguage(langstr))
    Cookies.set('lang', langstr)
  }

    /**
     * 语言切换选项单选框组件
     * @returns {Component}
     */
  render() {
    return (
      <div className='language-switch-box'>
        <Radio.Group className='language-switch-box-group' defaultValue={this.props.intl.locale} onChange={this.handleChange}>
          <Radio.Button className='language-switch-box-button' value="zh-cn">中文</Radio.Button>
          <Radio.Button className='language-switch-box-button' value="en">English</Radio.Button>
        </Radio.Group>
      </div>
    );
  }
}

export default injectIntl(connect()(LanguageSwitchBox))
