import * as cornerstone from 'cornerstone-core'
import * as cornerstoneTools from "cornerstone-tools"

let baseScale = new Map()

export function setBaseScale(elem, scale) {
  baseScale.set(elem, scale)
}

export function getBaseScale(elem) {
  return baseScale.get(elem)
}

let debounceMap = new Map()

// This function synchronizes the target zoom and pan to match the source
export default function (synchronizer, sourceElement, targetElement) {
  // Ignore the case where the target is source very short time ago
  if (!debounceMap.has(targetElement)) {
    debounceMap.set(targetElement, 0);
  }
  let now = Date.now();
  debounceMap.set(sourceElement, now);
  let lastTimeAsSource = debounceMap.get(targetElement);
  if (now - lastTimeAsSource < 50) {
    return;
  }

  // Ignore the case where the source and target are the same enabled element
  if (targetElement === sourceElement) {
    return;
  }

  // Get the source and target viewports
  const sourceViewport = cornerstone.getViewport(sourceElement);
  const targetViewport = cornerstone.getViewport(targetElement);

  const sourceToolData = cornerstoneTools.getToolState(sourceElement, 'zoom')
  const targetToolData = cornerstoneTools.getToolState(targetElement, 'zoom')

  let sourceBaseScale = 1
  let targetBaseScale = 1
  if (sourceToolData && targetToolData && sourceToolData.data[0].baseScale && targetToolData.data[0].baseScale) {
    sourceBaseScale = sourceToolData.data[0].baseScale
    targetBaseScale = targetToolData.data[0].baseScale
  }

  // Do nothing if the scale compared to baseScale is the same
  if (Math.abs(targetViewport.scale / targetBaseScale - sourceViewport.scale / sourceBaseScale) < 0.01) {
  // if (targetViewport.scale === sourceViewport.scale) {
    return;
  }
  // let a = sourceViewport.scale / sourceBaseScale
  // let b = targetViewport.scale / targetBaseScale
  // console.log(a, b);
  // console.log(Math.abs(a - b) < 0.01);
  // console.log(sourceElement, targetElement);
  // console.log('--------------------------------');

  // Scale and/or translation are different, sync them
  targetViewport.scale = sourceViewport.scale * targetBaseScale / sourceBaseScale;
  // targetViewport.scale = sourceViewport.scale
  synchronizer.setViewport(targetElement, targetViewport);
  // synchronizer.setViewportDebounce(targetElement, targetViewport);
}
