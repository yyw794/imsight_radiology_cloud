# Imsight radiology platform
## api doc
http://192.168.1.157:8888/
# deploy with docker
## install docker
https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce-1
## install docker compose
```
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
## create docker image
WARNING!!MONGO_INITDB_ROOT_USERNAME and MONGO_INITDB_ROOT_PASSWORD are admin account and password, this is important, please use your own to replace the command below.
```
export MONGO_INITDB_ROOT_USERNAME=admin
export MONGO_INITDB_ROOT_PASSWORD=password
sudo docker-compose build
```
## start docker
```sudo docker-compose up```
## update code
* Change in backend code will trigger reloading
* Frontend will not recompiled automatically. After updating the code, stop docker-compose and run
```
docker-compose rm node
docker-compose build node
```
and then restart docker-compose.
## development
modify proxy in front_end/dev-server.js
run `npm start` to start dev-server that will automatically recompile front end code
## Project setup requirements
* Use python2 for back_end
* You will need to have flask==0.12.2
* when you run python2 app.py in backend server folder you can see some "No module found" errors
  in this case python dependencies can be installed with pip2 install dependency-name
* back_end folder should contain data folder with .png images
* you must have database called radiology in your local mongodb
* do `npm install` in front end folder for the first time after cloning this project
* If you are running `back_end` on remote machines,
  change 2 string of IPs in `regional_hostname.py` and `regional_hostname.js`,
  which contains either "0.0.0.0" or "192.168.1.118" or "radiology.imsightmed.com:5000" or something like that.
  This change is ONLY for your own PC.
  We can keep the default version in remote master branch and untrack them locally
  by using the following command once inside the directory imsight_radiology_cloud:  
  `git update-index --assume-unchanged back_end/server/regional_hostname.py`  
  `git update-index --assume-unchanged front_end/src/regional_hostname.js`  

* testing data will be stored in HK sftp server in `/files/PaulHK/radiology-debug-data`
  use mongoimport to import each json in `updated-liver-collections.tar.gz` (each file name == collection name in radiology db)

## run project

### back_end
cd back_end/server
python2 app.py
python2 app_socket.py

### front_end
(make sure you run `npm install` before following code)
cd front_end
npm start

### result
* front end server will be running on localhost:8000
* `app.py` will be on localhost:5000
* `app_socket.py` will be on localhost:6000
