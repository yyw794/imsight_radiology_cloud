#!/bin/python
# coding=utf-8

import argparse
import json
import os

import requests

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--folder_path", help="path to folder", type=str, required=True)
parser.add_argument("-x", "--extention_name", help="image file extention name", type=str, default='.dcm')
parser.add_argument("-s", "--server", help="server ip:port", type=str, default='localhost:5000')
parser.add_argument("-u", "--username", help="user account used for upload", type=str, required=True, default='test')
parser.add_argument("-pwd", "--password", help="user account password", type=str, required=True, default='12345678')
args = parser.parse_args()

login_url = 'http://{}/auth/login'.format(args.server)

if args.extention_name:
    print("image extention name {}".format(args.extention_name))

if args.server:
    print("upload to server {}".format(args.server))

if args.username and args.password:
    print("account:{} password:{}".format(args.username, args.password))


def get_files(folder_path):
    file_folders = []
    for root, dirs, files in os.walk(folder_path):
        documents = []
        for file in files:
            if file.endswith(args.extention_name):
                file_path = os.path.join(root, file)
                documents.append(file_path)
        if documents:
            file_folders.append(documents)
    return file_folders


if __name__ == '__main__':
    payload = {"account": args.username, "password": args.password}
    s = requests.session()
    r = s.post(login_url, json=payload)
    if not r:
        print(r)
        print("login failed")
        exit(0)
    elif r.json()['statusCode'] != 1000:
        print("login failed")
        print(r.json())
        exit(0)

    file_folders = get_files(args.folder_path)
    for file_folder in file_folders:
        # original
        # files = MultiDict({"file[]": [open(file_path, 'rb') for file_path in file_folder]})
        # change to
        data = {"upload": len(file_folder)}
        files = {}
        for file_path in file_folder:
            files[os.path.basename(file_path)] = open(file_path, 'rb')

        # files = FileMultiDict()
        # for file_path in file_folder:
        #     files.add_file('file[]', open(file_path, 'rb'), os.path.basename(file_path))
        print json.dumps(data)
        resp = s.post('http://{}/api/upload_image'.format(args.server), files=files, json=json.dumps(data))
        if resp:
            # print resp.headers
            print resp.text
        else:
            print("upload fail")
            # break
