import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import * as cornerstone from 'cornerstone-core'
import * as cornerstoneWebImageLoader from "cornerstone-web-image-loader"
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import Hammer from "hammerjs"
import {message} from 'antd'
import Immutable from 'immutable'

import ToolBar from './ToolBar'
import LabelPopover from './LabelPopover'
import { executeAllPromises, drawArrow } from '../../util'
import {getUser} from '../../auth'
import api from '../../api'
import axios from 'axios'

import {angle} from '../tools/angleTool'
import {freehand} from './tools/freehand'
import {length} from './tools/length'
import {zoom} from '../tools/zoom'
import _ from 'underscore'
import SeriesThumbnailList from './SeriesThumbnailList'
import loadImage from './loadImage'

import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './index.less'

import scrollToIndex from '../tools/scrollToIndex'
//import Synchronizer from '../tools/Synchronizer'
import { scaleWC } from '../../util'

import NodulePanelLiver from './NodulePanelLiver'
import noduleBox from './tools/noduleBox'
import referenceLine2D from './tools/referenceLine2D'

import '../../../node_modules/react-grid-layout/css/styles.css'
import '../../../node_modules/react-resizable/css/styles.css'

import GridLayout from 'react-grid-layout';
import Modal from 'react-modal'
import { ENGINE_METHOD_DIGESTS } from 'constants';

//====================================================================================================================================================================================
// Global Settings
//====================================================================================================================================================================================
cornerstoneTools.external.cornerstone = cornerstone
cornerstoneTools.external.cornerstoneMath = cornerstoneMath
cornerstoneWebImageLoader.external.cornerstone = cornerstone
cornerstoneTools.external.Hammer = Hammer

//====================================================================================================================================================================================
// Global Variables
//====================================================================================================================================================================================
const TOOLS_NAME = ['length', 'angle', 'freehand', 'arrowAnnotate']

//====================================================================================================================================================================================
// Global Functions
//====================================================================================================================================================================================
// A very useful static function to retrieve the React Component from an DOM element, please keep it. (By Jackey at 201807091025)
// (A dynamic version findCompByDOM inside the Viewer class is available, but sometimes static function is more useful for debugging.)
// function GetCompByDOM(node) {//Original name is findReactElement on StackOverflow
//   //console.log("==========================================================");
//   //console.log(node);
//   //console.log(node.gcindex);
//   //console.log(node.reactcompthis);
//   //console.log(node.props);
//   //console.log(node.getAttribute("gcindex"));
//   //console.log(node.getAttribute("reactcompthis"));
//   //Version A
//   for (var key in node) {
//     if (key.startsWith("__reactInternalInstance$")) {
//       //console.log("key: " + key);
//       //onsole.log(node[key]);
//       if(!(node[key])){
//         console.log("node[key] NOT exist");
//         return null;
//       }
//
//       //Version 1
//       if(!(node[key]._debugOwner)){
//         console.log("LiverViewer::GetCompByDOM: This function ONLY works in debugging mode.");
//         return null;
//       }
//       return node[key]._debugOwner.stateNode;
//   
//       //Version 2
//       // if((node[key]._debugOwner !== undefined) && (node[key]._debugOwner !== null)){
//       //   //Debug Mode
//       //   return node[key]._debugOwner.stateNode;
//       // }
//       // else{
//       //   //Release Mode
//       //   return node[key]._currentElement._owner._instance;
//       // }
//     }
//   }
//   return null;
//
//   //Version B
//   //return node.getAttribute("reactcompthis");
// }
function GetWheelDir(e) {
  // if(e){
  //   if((e.detail !== undefined) && (e.detail !== null) && (e.detail != 0)){//Firefox
  //     if(e.detail > 0){ return +1; }
  //     else{ return -1; }
  //   }
  //   else if((e.wheelDelta !== undefined) && (e.wheelDelta !== null) && (e.wheelDelta != 0)){//Google
  //     if(e.wheelDelta < 0){ return +1; }
  //     else{ return -1; }
  //   }
  // }
  // return 0;
  if(e){
    let delta = e.deltaY;
    if((delta !== undefined) && (delta !== null) && (delta != 0)){
      if(delta > 0){return +1;}
      else{return -1;}
    }
  }
  return 0;
}
function MinMax(v,a,b){return Math.max(Math.min(v,b),a);}
function ResizeArray(arr, size, val_or_func, bIsValue = true) {
  if((arr === undefined) || (arr === null) || (!(Array.isArray(arr)))){arr=[];}
  var delta = arr.length - size;
  if (delta > 0) { arr.length = size; }
  else if(delta < 0) {
    if(bIsValue === true){ while (delta++ < 0) { arr.push(val_or_func); } }
    else{                  while (delta++ < 0) { arr.push(val_or_func()); } }
  }
}

//====================================================================================================================================================================================
// Internal Classes
//====================================================================================================================================================================================
class SeriesCell extends Component {
  //In @props, prepare: viewer, gcDomID, gcIndex, seriesNumber, strConsoleMsg
  //Notes: @gcDomID is something like 'r0c0', @gcIndex is something like 0
  constructor(props){
    super(props);
    this.state = {
      element: null,
      gcDomID: this.props.gcDomID,
      gcIndex: this.props.gcIndex,
      seriesNumber: this.props.seriesNumber,
      sliceIndex: this.props.sliceIndex,
      strConsoleMsg: this.props.strConsoleMsg
    };
    if(this.props.viewer.reactcomps){this.props.viewer.reactcomps[this.props.gcIndex] = this;}
    else{
      console.log("LiverViewer::SeriesCell: viewer.reactcomps NOT exist! Why!?");
    }
    //console.log("SeriesConstructor::sliceIndex = " + this.state.sliceIndex);
  }
  componentDidMount() {
    if(this.state.element){
      cornerstone.enable(this.state.element); //By Jackey: From now on, every SeriesCell object is 'Cornerstone Enabled'
      //cornerstone.resize(this.state.element);
    }
    else{console.log("LiverViewer::componentDidMount: element is NOT exist!!!!! Something wrong here!!!");}
    // console.log(this.props.viewer.elementCovers);
    // console.log("SeriesCell: ");
    // console.log(this.state.element);
  }
  updateSeries(gcDomID, seriesNumber, bBeCurrentActive = false, strConsoleMsg = null) {
    this.setState({
      gcDomID: gcDomID,
      seriesNumber: seriesNumber,
      strConsoleMsg: strConsoleMsg
    });
    this.state.element.props.seriesNumber = seriesNumber;
    if((bBeCurrentActive === true) && (this.props.viewer)) { this.props.viewer.setState({currentActiveGridCell: gcDomID}); }
  }
  onClick_Handler(e) {
    e.preventDefault();
    if(this.props.strConsoleMsg!=null){ console.log("React Custom Class SeriesCell OnClick (InnerElement): " + this.props.strConsoleMsg); }
    if (this.props.viewer.state.labelPopover.isAdding) {
      return this.props.viewer.setState({
        labelPopover: Object.assign({}, this.props.viewer.state.labelPopover, {isAdding: false})
      });
    }
    if (this.props.viewer.state.labelPopover.show) {
      this.props.viewer.setState({
        labelPopover: Object.assign({}, this.props.viewer.state.labelPopover, {show: false})
      })
    }
    // This block of comment is very very useful to understand which one is element and which one is reactcomp. PLEASE keep this for future reference. (By Jackey)
    // console.log("Inside SeriesCell Class:");                                //!*!*!*! Jackey Console Log
    // console.log(this.state.element);                                        //Output A
    // console.log(this.state.current);                                        //undefined
    // console.log(this.props.viewer.elementCovers);                           //A normal array
    // console.log(this.props.viewer.elementCovers[0].current);                //Output B
    // console.log(this.props.viewer.elementCovers[0].current.state.element);  //Output A
    // console.log(this.props.viewer.findCompByDOM(this.state.element));       //Output B

    if(this.state){
      let seriesNumber = this.state.seriesNumber;
      if((seriesNumber !== undefined) && (seriesNumber !== null)){
        let viewer = this.props.viewer;
        if(viewer){
          let seriesIndex = viewer.getSeriesIndex_BySeriesNumber(seriesNumber);
          if((seriesIndex !== undefined) && (seriesIndex !== null)){
            let series = viewer.props.series[seriesIndex];
            if(series){
              console.log("+++=+++=+++=+++=+++=+++=+++=+++=+++=+++=+++=+++");
              // console.log(series);
              // console.log(typeof(viewer.state.currentSeriesNumber));
              // console.log(viewer.state.currentSeriesNumber);
              // console.log(viewer.nodulePanelLiverComponent);
              console.log(viewer.state.selectedNodules);
              console.log(viewer.state.selectedNodule); //Discovered by Jackey: @selectedNodule and @selectedNodules are confusing, there may be bug on these two names.
              // console.log(viewer.props.data);
              // console.log(viewer.props.tools);
              // console.log(this.state.element);
              // console.log(viewer.findCompByDOM(this.state.element));
              // console.log(this.state.sliceIndex);
              // console.log(this.state.seriesNumber);
              //this.setState({sliceIndex: 0},function(){console.log("HAHAHAHAHAHA");}.bind(this));
              //viewer.initNoduleBoxTool_SeriesCell(this.state.gcDomID);
            }
            else{console.log("LiverViewer::SeriesCell::onClick_Handler: seriesdoes NOT exist!!!!");}
          }
          else{console.log("LiverViewer::SeriesCell::onClick_Handler: seriesIndex does NOT exist!!!!");}
        }
        else{console.log("LiverViewer::SeriesCell::onClick_Handler: viewer does NOT exist!!!!");}
      }
      else{console.log("LiverViewer::SeriesCell::onClick_Handler: seriesNumber does NOT exist!!!!");}
    }
    else{console.log("LiverViewer::SeriesCell::onClick_Handler: state does NOT exist!!!!");}
    
  }
  onWheel_Handler(e) {
    e.preventDefault();
    // Firefox e.detail > 0 scroll back, < 0 scroll forward
    // chrome/safari e.wheelDelta < 0 scroll back, > 0 scroll forward
    // if (e.wheelDelta < 0 || e.detail > 0) {
    // } else {
    // }
    let viewer = this.props.viewer;
    if((this.state.seriesNumber !== undefined) && (this.state.seriesNumber !== null)){
      let element = this.state.element;
      if(element){
        let wheelDir = GetWheelDir(e);
        if(wheelDir != 0){
        //console.log("Wheel Info: " + wheelDir);
        let toolState_Stack = cornerstoneTools.getToolState(element, 'stack');
        if(toolState_Stack){
          let toolState_StackData = toolState_Stack.data;
          if(toolState_StackData){
            let stack = toolState_StackData[0];
            if(stack){
              //this.setState({sliceIndex: stack.currentImageIdIndex});
              let sliceIndex = stack.currentImageIdIndex;
              //console.log("LiverViewer::onWheel_Handler::sliceIndex: " + sliceIndex);	//!*!*!*! Jackey Console Log
              this.setState({sliceIndex: sliceIndex});
              //Object.assign(this.state, {currentInstanceIdx: reactcomp.state.sliceIndex});
              if(viewer.state.bIsViewerSyncing === true){
                let imgTtl_refr = viewer.getImagesCount_ByElement(element);
                  if(imgTtl_refr > 0){
                    let imgLastIndex_refr = imgTtl_refr - 1;
                    for (let r=0; r<viewer.gridLayoutRows; ++r) {
                      for (let c=0; c<viewer.gridLayoutCols; ++c) {
                        let gcDomID = 'r'+r+'c'+c;
                        //console.log("gcDomID: " + gcDomID);
                        if(gcDomID != this.props.gcDomID){
                          let ajacElement = document.getElementById(gcDomID);
                          if(ajacElement){
                            let ajacReactcomp = viewer.findCompByDOM(ajacElement);
                            if(ajacReactcomp){
                              if(ajacElement !== element){
                                //cornerstone.enable(ajacElement);
                                let enabledElement = cornerstone.getEnabledElement(ajacElement);
                                if(enabledElement && enabledElement.image && enabledElement.image.imageId){
                                  if (cornerstone.getViewport(element)) {
                                    let imgTtl_ajac = viewer.getImagesCount_ByElement(ajacElement);
                                    if(imgTtl_ajac > 0){
                                      let prom = null;
                                      if(imgTtl_refr == imgTtl_ajac) {
                                        prom = viewer.scrollTo_SeriesCell(ajacElement, sliceIndex, true);
                                      }
                                      else {
                                        prom = viewer.scrollTo_SeriesCell(ajacElement, sliceIndex / imgLastIndex_refr, false);
                                      }
                                      if(prom){
                                        prom
                                        .then(function(){
                                          //console.log("ScrollToIndex is successful!");
                                        })
                                        .catch(function(){
                                          //console.log("ScrollToIndex contains some delayed calls, it's fine and can be ignored.");
                                        });
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return false;// Prevent page from scrolling
  }
  render() {
    //Very useful comment (By Jackey)
    // if(this.props.gcIndex === 5){
    //   console.log("SeriesCell #" + this.props.gcIndex);
    //   if(this.state.element){
    //   }
    // }
    //console.log("SeriesRender::sliceIndex: " + this.state.sliceIndex);	//!*!*!*! Jackey Console Log
    //console.log("SeriesCell start to render now:");

    // if(this.state && ((this.state.sliceIndex === undefined) || (this.state.sliceIndex === null))){
    //   this.state.sliceIndex = this.props.viewer.getSuggestedInitialSeriesSliceIndex(this.state.sliceIndex);
    // }

    return ((this.state.element) && (this.state.sliceIndex !== undefined) && (this.state.sliceIndex !== null) && (this.state.seriesNumber !== undefined) && (this.state.seriesNumber !== null))?(
      <div
        id={this.props.gcDomID}
        gcindex={this.props.gcIndex}
        ref={input => {this.state.element = input}}
        className='cornerstone-elm'
        seriesnumber={this.state.seriesNumber}
        onDragStart={(e) => {}}
      >
        <canvas className="cornerstone-canvas"/>
        <div className='bottom-right-text'>
        <span>{
          (((this.state.sliceIndex !== undefined) && (this.state.sliceIndex !== null)) ? this.state.sliceIndex + 1 : 'Loading...') +
          ' / ' +
          (((this.state.seriesNumber !== undefined) && (this.state.seriesNumber !== null)) ? this.props.viewer.getImagesCount_BySeriesNumber(this.state.seriesNumber) : 'Calculating...')
          }</span>
        </div>
      </div>
    ):
    (
      <div
        id={this.props.gcDomID}
        gcindex={this.props.gcIndex}
        ref={input => {this.state.element = input}}
        className='cornerstone-elm'
        seriesnumber={this.props.seriesNumber}
      >
        <canvas className="cornerstone-canvas"/>
        <div className='bottom-right-text'>
          <span>
          {/*"NoImage"*/}
          {/*Math.floor(Math.random() * 10000)*/}
          </span>
        </div>
      </div>
    );
  }
}

//====================================================================================================================================================================================
// Main Class
//====================================================================================================================================================================================
class Viewer extends Component {
  constructor(props) {
    super(props)
    this.element = React.createRef()
    this.state = {
      viewport: cornerstone.getDefaultViewport(null, undefined),
      activeTool: [],
      imgPosZ: 1,
      imgPosX: 1,
      imgPosY: 1,
      labels: props.labels,
      labelPopover: {
        show: false,
        position: {x: 0, y: 0},
        value: '',
        id: null,
        imageElm: null,
        isAdding: false
      },
      viewportX: null,
      imageX: null,
      invert: false,
      HU: 0,
      fullScreen: null,
      gridLayoutWidth: 1200,
      currentActiveGridCell: 'r0c0',
      currentSeriesNumber: null,
      bIsViewerSyncing:false
    }
    this.gridLayoutCols = 0;  //By Jackey: This is set to 0 by default, then calling arrangeLayoutGridCells with non-zero dimension will update the GridCells.
    this.gridLayoutRows = 0;  //By Jackey: This is set to 0 by default, then calling arrangeLayoutGridCells with non-zero dimension will update the GridCells.
    this.reactcomps = [];
    this.elementWrappers = [];
    this.elementCovers = [];
    //this.enableCornerstoneGridCells();
    this.arrangeLayoutGridCells(1, 1);

    const layout = this.generateLayout();
    this.state = {...this.state, layout };
    this.nodulePanelCustomStyle = {
      position: "absolute",
      zIndex: "999",
      // display: "none"
    }
    this.currentInstanceIdx = 1
    //this.currentSeriesNumber = 3
    this.toolsData = Immutable.fromJS(props.tools)
    this.saveToolsData_SeriesCell = _.debounce(this.saveToolsData_SeriesCell, 2000)
    this.saveToolsData_SeriesCell = _.debounce(this.saveToolsData_SeriesCell, 1000)
    this.report = this.props.report
    this.config = {
      getTextCallback : this.getTextCallback,
      changeTextCallback : this.changeTextCallback,
      drawHandles : false,
      drawHandlesOnHover : true,
      arrowFirst : true
    }
    
  }
  findCompByDOM(element_or_gcIndex) {
    if((element_or_gcIndex !== undefined) && (element_or_gcIndex !== null)){
      let gcIndex = ((typeof(element_or_gcIndex) === 'number')?(gcIndex):(element_or_gcIndex.getAttribute("gcindex")));
      if(gcIndex){
        if(gcIndex < this.reactcomps.length){
          const reactcomp = this.reactcomps[gcIndex];
          if(reactcomp){
            return reactcomp;
          }
          else{console.log("LiverViewer::findCompByDOM: reactcomp NOT exist!");}
        }
        else{console.log("LiverViewer::findCompByDOM: gcIndex " + gcIndex + " exceeds " + this.reactcomps.length);}
      }
      else{console.log("LiverViewer::findCompByDOM: gcIndex NOT exist!");}
    }
    else{console.log("LiverViewer::findCompByDOM: element_or_gcIndex NOT exist!");}
    return null;
  }
  initTools_SeriesCell(element_or_gcDomID) {
    if(element_or_gcDomID){
      const element = ((typeof element_or_gcDomID == "string")?(document.getElementById(element_or_gcDomID)):(element_or_gcDomID));
      cornerstoneTools.mouseInput.enable(element);
      cornerstoneTools.mouseWheelInput.enable(element);
      this.tools = {
        wwwc: cornerstoneTools.wwwc,
        pan: cornerstoneTools.pan,
        zoom: zoom,
        length: length,
        angle: angle,
        freehand: freehand,
        referenceLines: referenceLine2D
      };
      this.tools.pan.activate(element, 1) // pan is the default tool for left mouse button
      this.tools.zoom.setConfiguration({minScale: 0.01, maxScale: 100})
      this.tools.zoom.activate(element, 4) // zoom is the default tool for right mouse button
      this.tools.length.deactivate(element, 1);
      this.tools.angle.deactivate(element, 1);
      this.tools.freehand.deactivate(element, 1);
      cornerstoneTools.stackScrollWheel.activate(element);
    }
  }
  initReferenceLines(element) {
    console.log('PAUL: HUH?')

    // setTimeout(this.tools['pan'].deactivate(element, 1), 200)
    // this.tools.pan.activate(element, 2)
    // this.tools.pan.deactivate(element, 1)
    // this.tools.pan.activate(element, 2)
    this.tools['referenceLines'].enable(element, {})
    this.tools['referenceLines'].activate(element, 1)
    // this.tools.pan.activate(element, 2)

    referenceLine2D.updateToolData(element,
      {
        x: this.state.selectedNodules.coord.x / 512,
        y: this.state.selectedNodules.coord.y / 512
      }
    )
    // setTimeout(() => {
      console.log('PAUL: tools pan deactivate')
      this.tools['pan'].deactivate(element, 1)
    // }, 200)
  }
  setWWWC(windowWidth, windowCenter) {
    // for (let axis of ['x', 'y', 'z']) {
      let elem = document.getElementById(this.state.currentActiveGridCell)
      let viewport = cornerstone.getViewport(elem)
      viewport.voi.windowWidth = windowWidth
      viewport.voi.windowCenter = windowCenter
      cornerstone.updateImage(elem)
    // }
  }
  initNoduleBoxTool_SeriesCell(element_or_gcDomID) {
    console.log("initNoduleBoxTool_SeriesCell is called.");
    if(element_or_gcDomID){
      //console.log("A");
      const element = ((typeof element_or_gcDomID == "string")?(document.getElementById(element_or_gcDomID)):(element_or_gcDomID));
      if(element){
        //console.log("B");
        const reactcomp = this.findCompByDOM(element);
        if(reactcomp){
          //console.log("C");
          if(reactcomp.state){
            //console.log("D");
            const seriesNumber = reactcomp.state.seriesNumber;
            if((seriesNumber !== undefined) && (seriesNumber !== null)){
              //console.log("E");
              const imgTtl = this.getImgTtl_BySeriesNumber(seriesNumber);
              if((imgTtl !== undefined) && (imgTtl !== null) && (imgTtl > 0)){
                //console.log("F");
                noduleBox.enable(element, {
                  axis: 'z',
                  depth: Math.round(noduleBox.BOX_SIZE / cornerstone.getImage(element).height * imgTtl), // in layers

                  // TODO: make horizontal and vertical layers same size as n of images in x y z arrays
                  horizontalLayers: 512,
                  verticalLayers: 512,
                  mainAxisLayers: imgTtl,
                  getData: () => this.state.labels,
                  getCurrentIdx: () => {return {x: this.state.imgPosX, y: this.state.imgPosY,z: this.state.imgPosZ}},
                  getSelectedNodules: () => {return this.state.selectedNodules},
                  setSelectedNodules: (selectedNodule) => {
                    this.setState({selectedNodules: selectedNodule})
                    this.nodulePanelLiverComponent.setState({selectedNodule: selectedNodule})
                  },
                  onClick: (xRatio, yRatio, clickedNodules, pixel) => {
                    if (clickedNodules.length > 0) {
                      if(_.isFunction(this.nodulePanel.selectNodule)){this.nodulePanel.selectNodule(clickedNodules[0])}
                    }
                    this.setState({HU: Math.round(scaleWC(pixel))})
                  },
                  getCurrentSeriesNumber: () => {return this.state.currentSeriesNumber},
                  getCurrentActiveGridCell: () => {return this.state.currentActiveGridCell},
                  getCurrentActiveTools: () => {return this.state.activeTool}
                })
                noduleBox.activate(element, 1);
                // console.log("noduleBox:::");
                // console.log(noduleBox);
                // console.log(element);
                //reactcomp.setState({});
              }
            }
          }
        }
      }
    }
  }
  initStack(element, stackData, direction) {
    //Jackey: BUG-0158
    //console.log("LiverViewer: initStack is called.");
    let viewerThis = this;
    if(element){
      let reactcomp = viewerThis.findCompByDOM(element);
      if(reactcomp) {
        cornerstone.enable(element)
        return cornerstone.loadImage(stackData.imageIds[0])
          .then((image) => {
            cornerstoneTools.addStackStateManager(element, ['stack', 'referenceLines']) //!@#@!@#@! Jackey Discovered Event Added~
            cornerstoneTools.addToolState(element, 'stack', stackData) //!@#@!@#@! Jackey Discovered Event Added~ //Uncomment this back to normal working version!!!
            //console.log(cornerstoneTools.getToolState(element, 'stack')); //Jackey: Good line of code for checking whether stack data is appended or set to ToolState.
          })
      }
    }
  }

  exitToolMode() {
    // console.log("exitToolMode");
    // const element = document.getElementById(this.state.currentActiveGridCell)
    // let activeTool = this.state.activeTool
    // if (activeTool === 'pan') {return}
    // this.setState({activeTool: ['pan']})
    // this.tools.pan.deactivate(element, 2)
    // this.tools.pan.activate(element, 1)
    let activeTool = this.state.activeTool;
    if(activeTool){
      //console.log(activeTool);
      if (activeTool != 'pan') {
        for (let r=0; r<this.gridLayoutRows; ++r) {
          for (let c=0; c<this.gridLayoutCols; ++c) {
            const element = document.getElementById('r'+r+'c'+c);
            if(element){
              this.setState({activeTool: ['pan']})
              this.tools.pan.deactivate(element, 2)
              this.tools.pan.activate(element, 1)
            }
            else{console.log("LiverViewer::exitToolMode: element does NOT exist.");}
          }
        }
      }
      //else{console.log("LiverViewer::exitToolMode: It is panning.");}
    }
    else{console.log("LiverViewer::exitToolMode: activeTool does NOT exist.");}
  }
  onKeyDown = (e) => {
    if (e.key === 'Escape') {
      this.exitToolMode()
    }
  }
  onWindowResize() {
    this.onLayoutSelected(this.gridLayoutRows, this.gridLayoutCols, true);
  };

  onNewImage = ((e) => {
    const element = e.target;
    if(element){
      let reactcomp = this.findCompByDOM(element);
      if(reactcomp){
        //console.log("on New Image @$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@$@");
        this.activateToolStates(element);
      }
      else{console.log("LiverViewer::onNewImage: reactcomp does NOT exist!!!");}
    }
    else{console.log("LiverViewer::onNewImage: element does NOT exist!!!");}
  }).bind(this);

  showLabelPopover(id, position, value) {
    this.setState({
      labelPopover: {show: true, position, value, id, isAdding: true}
    })
  }

  closeLabelPopover = () => {
    this.setState({
      labelPopover: Object.assign({}, this.state.labelPopover, {show: false})
    })
  }

  changeLabelName(id, name) {
    // change label of lesions
    this.tools.lesionsMarker.setName(id, name)
    this.setState({labelPopover: Object.assign({}, this.state.labelPopover, {value: name})})
  }

  async displayInstance(instance) {
    const element = document.getElementById(this.state.currentActiveGridCell);
    let image = await loadImage(instance.url);
    // TODO: define a new image loader, these meta data is defined in https://github.com/cornerstonejs/cornerstoneWebImageLoader/blob/master/src/createImage.js
    image.color = false;

    // Display the first image
    cornerstone.displayImage(element, image);
    cornerstone.reset(element);
  }

  async displayInstance_SeriesCell(element_or_gcDomID, instance) {
    if(element_or_gcDomID){
      const element = ((typeof element_or_gcDomID == "string")?(document.getElementById(element_or_gcDomID)):(element_or_gcDomID));
      let image = await loadImage(instance.url);
      // TODO: define a new image loader, these meta data is defined in https://github.com/cornerstonejs/cornerstoneWebImageLoader/blob/master/src/createImage.js
      image.color = false;

      // Display the first image
      cornerstone.displayImage(element, image);
      cornerstone.reset(element);
    }
  }

  updateLocalToolData = (element) => {
    // TODO again make flexible for not only axial view
    if (this.toolsData) {
      this.props.data.series.map(series => {

        let instanceID = series.instances.z[this.currentInstanceIdx]['instanceID']
        let toolData = {}
        for (let name of TOOLS_NAME) {
          // if (name === 'angle') continue
          let state = cornerstoneTools.getToolState(element, name)
          if (!state) continue
          // let data = Immutable.fromJS(state.data).toJS()
          let data = state.data
          data.forEach(d => {
            d.active = false
            d.selected = false
          })
          toolData[name] = data
        }
        this.toolsData = this.toolsData.set(instanceID, Immutable.fromJS(toolData))
      })
    }
  }

  updateLocalLabelData = () => {
    // TODO change as in updateLocalToolData map through series
    // let labels = this.tools.lesionsMarker.getState()
    // // let instanceID = this.props.instances[this.currentInstanceIdx].instanceID
    // let instanceID = this.props.instances[this.currentInstanceIdx].SOPinstanceUID
    // this.setState({
    //   labels: this.state.labels.set(instanceID, Immutable.fromJS(labels))
    // })
  }

  getOpenedSeriesGcDomIDs() { //Done! (By Jackey)
    let ans = [];
    for (let r=0; r<this.gridLayoutRows; ++r) {
      for (let c=0; c<this.gridLayoutCols; ++c) {
        const gcDomID = 'r'+r+'c'+c;
        const element = document.getElementById(gcDomID);
        if(element){
          const reactcomp = this.findCompByDOM(element);
          if(reactcomp){
            const seriesNumber = reactcomp.state.seriesNumber;
            //console.log("******>>: " + gcDomID + ": seriesNumber: " + seriesNumber);
            if((seriesNumber !== undefined)&&(seriesNumber !== null)){ans.push(seriesNumber);}
          }
        }
      }
    }
    return ans;
  }
  getOpenedSeriesGcDomID_BySeriesNumber(seriesNumber) { //Done! (By Jackey at 201807051423)
    if((seriesNumber !== undefined)&&(seriesNumber !== null)){
      for (let r=0; r<this.gridLayoutRows; ++r) {
        for (let c=0; c<this.gridLayoutCols; ++c) {
          const gcDomID = 'r'+r+'c'+c;
          const element = document.getElementById(gcDomID);
          if(element){
            const reactcomp = this.findCompByDOM(element);
            if(reactcomp){
              if(reactcomp.state.seriesNumber == seriesNumber){return gcDomID;}
            }
          }
        }
      }
    }
    return null;
  }
  isOpenedSeries_ByElement(element) { //Done! (By Jackey)
    if(element){
      const reactcomp = this.findCompByDOM(element);
      return this.isOpenedSeries_ByReactComp(reactcomp);
    }
    return false;
  }
  isOpenedSeries_BySeriesNumber(seriesNumber) { //Done! (By Jackey)
    if((seriesNumber !== undefined)&&(seriesNumber !== null)){
      for (let r=0; r<this.gridLayoutRows; ++r) {
        for (let c=0; c<this.gridLayoutCols; ++c) {
          const gcDomID = 'r'+r+'c'+c;
          const element = document.getElementById(gcDomID);
          if(element){
            const reactcomp = this.findCompByDOM(element);
            if(reactcomp){
              if(reactcomp.state.seriesNumber == seriesNumber){return true;}
            }
          }
        }
      }
    }
    return false;
  }
  isOpenedSeries_ByReactComp(reactcomp) { //Done! (By Jackey)
    if(reactcomp){
      const seriesNumber = reactcomp.state.seriesNumber;
      return ((seriesNumber !== undefined) && (seriesNumber !== null));
    }
    return false;
  }
  isOpenedSeries_ByGcIndex(gcIndex) { //Done! (By Jackey)
    const reactcompCovers = this.elementCovers;
    if(reactcompCovers){
      if(gcIndex < reactcompCovers.length){
        const reactcompCover = reactcompCovers[gcIndex];
        if(reactcompCover){
          const reactcomp = reactcompCover.current;
          return this.isOpenedSeries_ByReactComp(reactcomp);
        }
      }
    }
    return false;
  }
  isOpenedSeries_ByGcDomID(gcDomID) { //Done! (By Jackey at 201807051106)
    const element = document.getElementById(gcDomID);
    return this.isOpenedSeries_ByElement(element);
  }
  isOpenedSeries_BySeriesIndex = (seriesIndex) => { //Done! (By Jackey at 201807051148)
    if(this.props){
      const serieses = this.props.series;
      if(serieses){
        if(seriesIndex<serieses.length){
          const series = serieses[seriesIndex];
          if(series){
            const seriesNumber = Number(series.seriesNumber);
            return this.isOpenedSeries_BySeriesNumber(seriesNumber);
          }
        }
      }
    }
    return false;
  }
  getSeriesNumber_ByGcDomID(gcDomID) { //Done! (By Jackey at 201807051423)
    if((gcDomID !== undefined)&&(gcDomID !== null)){
      const element = document.getElementById(gcDomID);
      if(element){
        const reactcomp = this.findCompByDOM(element);
        if(reactcomp){
          return reactcomp.state.seriesNumber;
        }
      }
    }
    return null;
  }
  getSeriesIndex_BySeriesNumber(seriesNumber) {
    if((seriesNumber !== undefined)&&(seriesNumber !== null)){
      if(this.props){
        const serieses = this.props.series;
        if(serieses){
          for(let i = 0; i < serieses.length; ++i){
            const series = serieses[i];
            if(Number(series.seriesNumber) == seriesNumber) {return i;}
          }
        }
      }
    }
    return null;
  }
  getSeriesIndex_ByGcDomID(gcDomID) {
    const seriesNumber = this.getSeriesNumber_ByGcDomID(gcDomID);
    return this.getSeriesIndex_BySeriesNumber(seriesNumber);
  }
  getSeries_BySeriesIndex(seriesIndex) {
    if((seriesIndex !== undefined)&&(seriesIndex !== null)){
      if(this.props){
        const serieses = this.props.series;
        if(serieses){ return serieses[seriesIndex]; }
      }
    }
  }
  getSeries_BySeriesNumber(seriesNumber) {
    const seriesIndex = this.getSeriesIndex_BySeriesNumber(seriesNumber);
    return this.getSeries_BySeriesIndex(seriesIndex);
  }
  getSeries_ByGcDomID(gcDomID) {
    const seriesNumber = this.getSeriesNumber_ByGcDomID(gcDomID);
    return this.getSeries_BySeriesNumber(seriesNumber);
  }
  getStack_BySeriesNumber(seriesNumber) {
    if(this.props){
      const stacks = this.props.stacks;
      if(stacks){
        for(let i = 0; i < stacks.length; ++i){
          const stack = stacks[i];
          if(Number(stack.seriesNumber) == seriesNumber) {return stack;}
        }
      }
    }
  }
  getStack_BySeriesIndex(seriesIndex) {
    const seriesNumber = this.getSeriesNumber_BySeriesIndex(seriesIndex);
    return this.getStack_BySeriesNumber(seriesNumber);
  }
  getStack_ByGcDomID(gcDomID) {
    const seriesNumber = this.getSeriesNumber_ByGcDomID(gcDomID);
    return this.getStack_BySeriesNumber(seriesNumber);
  }
  getImgTtl_BySeriesNumber(seriesNumber) {
    const stack = this.getStack_BySeriesNumber(seriesNumber);
    if(stack){
      if(stack.imageIds){
        return stack.imageIds.length;
      }
    }
    return null;
  }
  getImgTtl_BySeriesIndex(seriesIndex) {
    const seriesNumber = this.getSeriesNumber_BySeriesIndex(seriesIndex);
    return this.getImgTtl_BySeriesNumber(seriesNumber);
  }
  getImgTtl_ByGcDomID(gcDomID) {
    const seriesNumber = this.getSeriesNumber_ByGcDomID(gcDomID);
    return this.getImgTtl_BySeriesNumber(seriesNumber);
  }
  getSeriesNumber_BySeriesIndex(seriesIndex){
    if((seriesIndex !== undefined)&&(seriesIndex !== null)){
      if(this.props){
        const serieses = this.props.series;
        if(serieses){
          const series = serieses[seriesIndex];
          return Number(series.seriesNumber);
        }
      }
    }
  }
  getSliceIndex_ByGcDomID(gcDomID){
    if(gcDomID){
      const element = document.getElementById(gcDomID);
      if(element){
        const reactcomp = this.findCompByDOM(element);
        if(reactcomp){
          if(reactcomp.state){
            const sliceIndex = reactcomp.state.sliceIndex;
            if((sliceIndex !== undefined) && (sliceIndex !== null)){
              return sliceIndex;
            }
          }
        }
      }
    }
    return null;
  }
  getCurrentOpenedActiveSeriesSliceIndex() {
    if(this.state){
      const gcDomID = this.state.currentActiveGridCell;
      return this.getSliceIndex_ByGcDomID(gcDomID);
    }
    return null;
  }
  getSuggestedInitialSeriesSliceIndex(initialSliceIndex = null){
    if(initialSliceIndex === null){initialSliceIndex = this.getCurrentOpenedActiveSeriesSliceIndex();}
    if(initialSliceIndex === null){initialSliceIndex = 0;}
    return initialSliceIndex;
  }
  getScrolledSliceIndex_ByRatio(ratio, lastIndex){
    return Math.round(lastIndex * ratio);
  }
  getScrolledSliceIndex_ByTotal(sliceIndex_old, imgTtl_old, imgTtl_new){
    const imgLastIndex_old = imgTtl_old - 1;
    const imgLastIndex_new = imgTtl_new - 1;
    return this.getScrolledSliceIndex_ByRatio(sliceIndex_old/imgLastIndex_old, imgLastIndex_new);
  }
  getProportionalSliceIndex(sliceIndex_old, seriesNumber_old, seriesNumber_new){
    if((sliceIndex_old!==undefined)&&(sliceIndex_old!==null)){
      if((seriesNumber_old!==undefined)&&(seriesNumber_old!==null)){
        if((seriesNumber_new!==undefined)&&(seriesNumber_new!==null)){
          const stack_old = this.getStack_BySeriesNumber(seriesNumber_old);
          if(stack_old && stack_old.imageIds){
            const stack_new = this.getStack_BySeriesNumber(seriesNumber_new);
            if(stack_new && stack_new.imageIds){
              const imgTtl_old = stack_old.imageIds.length;
              const imgTtl_new = stack_new.imageIds.length;
              // console.log("::::::::::::::::::::::::");
              // console.log(stack_old);
              // console.log(stack_new);
              // console.log(imgTtl_old);
              // console.log(imgTtl_new);
              if(imgTtl_old != imgTtl_new){
                return this.getScrolledSliceIndex_ByTotal(sliceIndex_old, imgTtl_old, imgTtl_new);
              }
            }
          }
        }
      }
    }
    return sliceIndex_old;
  }
  /**
   * display another instance
   * @param  {number} seriesNumber_new [The new seriesNumber that will be used]
   * @param  {json} instance_new [instance data]
   * @param  {number} seriesIndex_new [The new seriesIndex_new that will be used]
   * @return {undefined}
   */
  onInstanceChange = ((seriesNumber_new, initialSliceIndex = 0, callback = null, bResetNodule = true) => {
    //console.log("onInstanceChange:");
    //console.log("seriesNumber_new: " + seriesNumber_new);
    const gcDomID_new = this.getOpenedSeriesGcDomID_BySeriesNumber(seriesNumber_new);
    //console.log("gcDomID_new: " + gcDomID_new);
    //const viewerThis = this;
    if(gcDomID_new == null){
      //console.log("Just before weird line!!!!!!!!");
      const gcDomID_old = this.state.currentActiveGridCell;
      //console.log("gcDomID_old: " + gcDomID_old);
      const seriesNumber_old = this.getSeriesNumber_ByGcDomID(gcDomID_old);
      //console.log("seriesNumber> Old: " + seriesNumber_old + ", New: " + seriesNumber_new);
      // const seriesIndex_old = this.getSeriesIndex_ByGcDomID(gcDomID_old);
      // console.log("gcDomID_new: " + gcDomID_new);
      // console.log("gcDomID_old: " + gcDomID_old);
      // console.log("seriesNumber_new: " + seriesNumber_new);
      // console.log("seriesNumber_old: " + seriesNumber_old);nodule.coord.z
      // console.log("seriesIndex_new: " + seriesIndex_new);
      // console.log("seriesIndex_old: " + seriesIndex_old);
      //console.log("LiverViewer: currentSeriesNumber is checking");
      if(seriesNumber_old != seriesNumber_new){
        // this.resetSelectedNodule(function(){
        //   //console.log("LiverViewer: currentSeriesNumber is changing. seriesNumber_new = " + seriesNumber_new);
        //   this.setState({ currentSeriesNumber: seriesNumber_new }, function(){
        //     //console.log("LiverViewer (A): this.state.currentSeriesNumber = " + this.state.currentSeriesNumber);
        //     //console.log(this.state);
        //     //console.log(viewerThis.state);
        //     let element = document.getElementById(gcDomID_old);
        //     if(element) {
        //       let viewport = cornerstone.getViewport(element);
        //       if (viewport) {
        //         this.activateSeriesCell(element, seriesNumber_new, initialSliceIndex, callback);
        //       }
        //       else{console.log("LiverViewer::onInstanceChange: The cornerstone viewport of the element is problematic!");console.log(viewport);}
        //     }
        //     else{console.log("LiverViewer::onInstanceChange: The existing element is NOT exist!.");}
        //   }.bind(this));
        // }.bind(this), bResetNodule);
        this.setState({ currentSeriesNumber: seriesNumber_new }, function(){
          //console.log("LiverViewer (A): this.state.currentSeriesNumber = " + this.state.currentSeriesNumber);
          //console.log(this.state);
          //console.log(viewerThis.state);
          let element = document.getElementById(gcDomID_old);
          if(element) {
            let viewport = cornerstone.getViewport(element);
            if (viewport) {
              // console.log("Before Suggestion: initialSliceIndex = " + initialSliceIndex);
              initialSliceIndex = this.getSuggestedInitialSeriesSliceIndex(initialSliceIndex);
              // console.log("After  Suggestion: initialSliceIndex = " + initialSliceIndex);
              initialSliceIndex = this.getProportionalSliceIndex(initialSliceIndex, seriesNumber_old, seriesNumber_new);
              // console.log("After  Proportion: initialSliceIndex = " + initialSliceIndex);
              this.activateSeriesCell(element, seriesNumber_new, initialSliceIndex, function(){
                if(callback){callback();}
                //this.resetSelectedNodule();
              }.bind(this));
            }
            else{console.log("LiverViewer::onInstanceChange: The cornerstone viewport of the element is problematic!");console.log(viewport);}
          }
          else{console.log("LiverViewer::onInstanceChange: The existing element is NOT exist!.");}
        }.bind(this));
      }
      else{console.log("LiverViewer::onInstanceChange: Such series is already selected in the SeriesThumbnailList (By checking seriesNumber).");}
    }
    //else{if(callback){callback();}}
    //else{console.log("LiverViewer::onInstanceChange: Such series is already selected in the SeriesThumbnailList (By checking gcDomID).");}
  }).bind(this);
  onDropSeries = (element, seriesNumber, gcIndex, gcDomID, callback = null) => {
    //@gcIndex is an integer specifiying the index of gridcells
    //@gcDomID is a string, like "r0c0"
    //console.log("onDropSeries: gcIndex = " + gcIndex +", gcDomID = " + gcDomID + ", seriseNumber = " + seriesNumber);	//!*!*!*! Jackey Console Log
    //console.log(element);	//!*!*!*! Jackey Console Log
    //let reactcomp = this.findCompByDOM(element);
    if(gcIndex < this.reactcomps.length){
      let reactcomp = this.reactcomps[gcIndex];
      if(reactcomp){
        //console.log(reactcomp);	//!*!*!*! Jackey Console Log
        //console.log("onDropSeries: reactcomp.state.seriesNumber: " + reactcomp.state.seriesNumber);
        if(reactcomp.state.seriesNumber != seriesNumber){
          element.parentNode.setAttribute('seriesnumber', seriesNumber) //Added by Paul in during Merging by Jackey
          reactcomp.setState({seriesNumber: seriesNumber});
          if((reactcomp.state.sliceIndex === undefined) || (reactcomp.state.sliceIndex === null)){reactcomp.state.sliceIndex = 0;}
          Object.assign(this.state, {currentInstanceIdx: reactcomp.state.sliceIndex});
          this.resetSelectedNodule();
          //this.setState({currentSeriesNumber: seriesNumber});
          if(!(this.isOpenedSeries_BySeriesNumber(seriesNumber))){
            // if (cornerstone.getViewport(element)) {
            //   this.activateSeriesCell(element, seriesNumber, 0);
            // }
            //else{console.log("LiverViewer::onDropSeries: The cornerstone viewport of the element is problematic!");}
            element.setAttribute("gcindex", gcIndex);
            this.activateSeriesCell(element, seriesNumber, 0, callback);
          }
          else{console.log("LiverViewer::onDropSeries: Such series is already opended in the grid layout.");}
        }
        else{console.log("LiverViewer::onDropSeries: Same series, no action.");}
      }
      else{console.log("LiverViewer::onDropSeries: reactcomp NOT exist. (Currently, also means the gridcell is NOT empty.)");}
    }
    else{console.log("LiverViewer::onDropSeries: gcIndex " + gcIndex + " exceeds the size of reactcomps " + this.reactcomps.length);}
  }
  scrollTo_SeriesCell(element_or_gcDomID, pos, exact = null){
    if(element_or_gcDomID){
      if(exact == null) {exact = Number.isInteger(pos);}
      if(exact != null) {
        const element = ((typeof element_or_gcDomID == "string")?(document.getElementById(element_or_gcDomID)):(element_or_gcDomID));
        if(element){
          let reactcomp = this.findCompByDOM(element);
          if(reactcomp){
            if(reactcomp.state) {
              if(reactcomp.state.seriesNumber) {
                let stack = this.props.stacks.find(stack => stack.seriesNumber == reactcomp.state.seriesNumber);
                if(stack) {
                  if(stack.imageIds){
                    let imgTtl = stack.imageIds.length;
                    if(imgTtl > 0){
                      let imgLastIndex = imgTtl - 1;
                      let index = ( (exact === true) ? (pos) : (this.getScrolledSliceIndex_ByRatio(pos, imgLastIndex)) );
                      index = MinMax(index,0,imgLastIndex);
                      //console.log("LiverViewer::scrollTo_SeriesCell::index = " + index);	//!*!*!*! Jackey Console Log
                      reactcomp.setState({sliceIndex: index});
                      return scrollToIndex(element, index);
                    }
                    else{console.log("LiverViewer::scrollTo_SeriesCell: stack.imageIds contains nothing!");}
                  }
                  else{console.log("LiverViewer::scrollTo_SeriesCell: stack.imageIds NOT exist!");}
                }
                else{console.log("LiverViewer::scrollTo_SeriesCell: stack NOT exist!");}
              }
              else{console.log("LiverViewer::scrollTo_SeriesCell: reactcomp.state.seriesNumber NOT exist!");}
            }
            else{console.log("LiverViewer::scrollTo_SeriesCell: reactcomp.state NOT exist!");}
          }
          else{console.log("LiverViewer::scrollTo_SeriesCell: reactcomp NOT exist!");}
        }
        else{console.log("LiverViewer::scrollTo_SeriesCell: element NOT exist!");}
      }
      else{console.log("LiverViewer::scrollTo_SeriesCell: @exact is problematic!");}
    }
    else{console.log("LiverViewer::scrollTo_SeriesCell: element_or_gcDomID NOT exist.");}
  }
  getImagesCount_ByCurrentElement() {
    let stack = this.props.stacks.find(stack => stack.seriesNumber == this.state.currentSeriesNumber);
    if(stack){return this.props.stacks.find(stack => stack.seriesNumber == this.state.currentSeriesNumber).imageIds.length;}
    else{return 0;}    
  };
  getImagesCount_ByElement(element) {
    //return this.getImagesCount_BySeriesNumber(element.seriesNumber);
    let reactcomp = this.findCompByDOM(element);
    if(reactcomp){return this.getImagesCount_BySeriesNumber(reactcomp.state.seriesNumber);}
    else{return 0;}
  };
  getImagesCount_BySeriesNumber(seriesNumber) {
    let stack = this.props.stacks.find(stack => stack.seriesNumber == seriesNumber);
    if(stack){return this.props.stacks.find(stack => stack.seriesNumber == seriesNumber).imageIds.length;}
    else{return 0;}
  };
  activateToolStates(element_or_gcDomID) {
    let viewerThis = this;
    if(element_or_gcDomID){
      const element = ((typeof element_or_gcDomID == "string")?(document.getElementById(element_or_gcDomID)):(element_or_gcDomID));
      if(element){
        let reactcomp = viewerThis.findCompByDOM(element);
        if(reactcomp){
          if(reactcomp.state){
            if((reactcomp.state.seriesNumber !== undefined) && (reactcomp.state.seriesNumber !== null)){
              const seriesNumber = reactcomp.state.seriesNumber;
              const databaseSeriesToolsSet = viewerThis.props.tools;
              if(databaseSeriesToolsSet){
                let toolState_Stack = cornerstoneTools.getToolState(element, 'stack');
                if (toolState_Stack) {
                  const databaseSeriesTools = databaseSeriesToolsSet[String(seriesNumber)];
                  if (databaseSeriesTools) {
                    let toolState_StackData = toolState_Stack.data;
                    if(toolState_StackData){
                      if(toolState_StackData.length > 0){
                        const stackZ = toolState_StackData[0];
                        if(stackZ){
                          const currentImageIdIndex = stackZ.currentImageIdIndex;
                          if((currentImageIdIndex !== undefined) && (currentImageIdIndex !== null)){
                            const indexKey = String(currentImageIdIndex);
                            let theTool = databaseSeriesTools[indexKey];
                            if (theTool) {
                              Object.keys(theTool).map((key, idx) => {  //Jackey Notes: Object.keys(theTool) may return ["length"] while TOOLS_NAME is ["length", "angle", "freehand", "arrowAnnotate"] 
                                Object.keys(theTool[key]).map((keyd, idxd) => {
                                  let d = theTool[key][keyd];
                                  d.visible = true;
                                  if (!(d.isAdded)) { cornerstoneTools.addToolState(element, key, d); }
                                  d.isAdded = true;
                                })
                              })
                            }
                            //else{console.log("LiverViewer::activateToolStates: @theTool in @seriesToolsFromDb (@indexKey "+indexKey+") NOT exist.");}
                          }
                          else{console.log("LiverViewer::activateToolStates: @currentImageIdIndex NOT exist.");}
                        }
                        else{console.log("LiverViewer::activateToolStates: @stackZ NOT exist.");}
                      }
                      //else{console.log("LiverViewer::activateToolStates: toolState_StackData is empty.");}
                    }
                    else{console.log("LiverViewer::activateToolStates: toolState_StackData does NOT exist.");}
                  }
                  //else{console.log("LiverViewer::activateToolStates: @databaseSeriesTools NOT exist.");}
                }
                else{console.log("LiverViewer::activateToolStates: Stack of the element does NOT exist.");}
              }
              else{console.log("LiverViewer::activateToolStates: Tools of series in the Viewer NOT exist!");}
            }
            else{console.log("LiverViewer::activateToolStates: reactcomp.state.seriesNumber NOT exist!");}
          }
          else{console.log("LiverViewer::activateToolStates: reactcomp.state NOT exist!");}
        }
        else{console.log("LiverViewer::activateToolStates: reactcomp NOT exist!");}
      }
      else{console.log("LiverViewer::activateToolStates: element NOT exist!");}
    }
    else{console.log("LiverViewer::activateToolStates: element or gcDomID NOT exist.");}
  }
  activateSeriesCell(element_or_gcDomID, seriesNumber = null, initialSliceIndex = 0, callback = null) {
    //console.log("LiverViewer::activateSeriesCell is called");
    //console.log(element_or_gcDomID);
    //console.log(seriesNumber);
    //console.log(initialSliceIndex);
    //@gcDomID is something like 'r0r0' OR 'r0r1'
    let viewerThis = this;
    if(element_or_gcDomID){
      const element = ((typeof element_or_gcDomID == "string")?(document.getElementById(element_or_gcDomID)):(element_or_gcDomID));
      //console.log(element);
      if(element){
        //console.log("BBBBBBBBBBB");
        let reactcomp = viewerThis.findCompByDOM(element);
        //console.log(reactcomp);
        if(reactcomp){
          //console.log(this.state.currentSeriesNumber);
          //cornerstone.enable(element);
          //const seriesNumber = reactcomp.props.seriesNumber;
          //if(!seriesNumber){seriesNumber = reactcomp.props.seriesNumber;}

          //console.log(">>>> this.state.currentSeriesNumber: " + this.state.currentSeriesNumber + ", " + typeof(this.state.currentSeriesNumber));
          // console.log("reactcomp.state.seriesNumber: " + reactcomp.state.seriesNumber);
          if(seriesNumber === undefined || seriesNumber == null){
            //console.log("%$^$%: " + viewerThis.state.currentSeriesNumber);
            seriesNumber = viewerThis.state.currentSeriesNumber;
          }
          //console.log(seriesNumber);
          reactcomp.setState({seriesNumber: seriesNumber}, function(){
            //console.log(reactcomp.state.seriesNumber);
            //if(seriesNumber > 0){--seriesNumber;}
            // console.log("this.props.series:");
            // console.log(this.props.series);
            let seriesIndex = 0;
            for(; seriesIndex<viewerThis.props.series.length; ++seriesIndex){
              if(viewerThis.props.series[seriesIndex].seriesNumber == seriesNumber){break;}
            }
            if(seriesIndex<viewerThis.props.series.length){
              // console.log("-------------------------------------------------");
              // console.dir(initialSliceIndex);
              //console.log("seriesNumber: " + seriesNumber +", seriesIndex: " + seriesIndex);
              const instances = viewerThis.props.series[seriesIndex].instances;
              //if(!(instances.z[initialSliceIndex])){++initialSliceIndex;} //Jackey: In order to fix-zero-index bug
              let strInstanceID = (initialSliceIndex+1).toString();
              //if(instances.z[initialSliceIndex] === undefined || instances.z[initialSliceIndex] == null){++initialSliceIndex;} //Jackey: In order to fix-zero-index bug
              //console.dir(strInstanceID);
              const instance = instances.z[strInstanceID];
              // console.dir("The InstanceID: ");
              // console.dir(instances.z);
              if(instance){
                //console.log("initialSliceIndex A: " + initialSliceIndex);
                reactcomp.setState({sliceIndex: initialSliceIndex},function(){
                  //console.log("initialSliceIndex B: " + initialSliceIndex);
                  if(!(viewerThis.isOpenedSeries_BySeriesNumber(seriesNumber))){
                    //console.log(this.props.series[seriesIndex].instances);
                    //console.log("initialSliceIndex: " + initialSliceIndex);
                  }
                  //this.displayInstance(instance).then(() => {
                  viewerThis.displayInstance_SeriesCell(element, instance).then(() => {
                    //console.log("this.state.currentSeriesNumber: " + this.state.currentSeriesNumber);
                    viewerThis.initTools_SeriesCell(element);
                    element.removeEventListener("cornerstonenewimage", viewerThis.onNewImage);
                    element.addEventListener("cornerstonenewimage", viewerThis.onNewImage); //!@#@!@#@! Jackey Discovered Event Added~
                    element.currentSeriesNumber = viewerThis.state.currentSeriesNumber;
                    element.series = viewerThis.props.data.series;
                    //element.removeEventListener(cornerstoneTools.EVENTS.MEASUREMENT_ADDED, viewerThis.saveToolsData_SeriesCell);
                    // element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_MODIFIED, viewerThis.saveToolsData_SeriesCell); //!@#@!@#@! Jackey Discovered Event Added~
                    // element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_REMOVED, viewerThis.saveToolsData_SeriesCell); //!@#@!@#@! Jackey Discovered Event Added~
                  });
                  //console.log("viewerThis.props.series: ");	//!*!*!*! Jackey Console Log
                  //console.log(viewerThis.props.series);	    //!*!*!*! Jackey Console Log
                  //console.log("viewerThis.props.stacks: ");	//!*!*!*! Jackey Console Log
                  //console.log(viewerThis.props.stacks);	    //!*!*!*! Jackey Console Log
                  const zStack = viewerThis.props.stacks.find(stack => stack.seriesNumber == seriesNumber);
                  //console.log("LiverViewer::activateSeriesCell: Before calling initStack");	//!*!*!*! Jackey Console Log
                  let initZStack = viewerThis.initStack(element, zStack, 'Z');//Jackey: BUG-0158
                  Promise.all([initZStack])
                  .then(() => {
                    //console.log("LiverViewer::activateSeriesCell: After calling initStack");	//!*!*!*! Jackey Console Log
                    viewerThis.activateToolStates(element);
                    //element.addEventListener(cornerstoneTools.EVENTS.MEASUREMENT_ADDED, viewerThis.saveToolsData_SeriesCell); //!@#@!@#@! Jackey Discovered Event Added~
                    // this.enableReferLines()
                    // cornerstone.updateImage(element)
                    //
                    // let viewport = cornerstone.getViewport(element);
                    // viewport.pixelReplication = true; //!viewport.pixelReplication;
                    // cornerstone.setViewport(element, viewport);
                    //
                    // console.log('CLOSING MODAL')
                    // document.getElementsByClassName('annotationDialog')[0].close()
                    // document.getElementsByClassName('relabelDialog')[0].close()
                    //
                    // this.updateLocalToolData()
                    // this.tools.pan.deactivate(element, 1)
                    // element.setAttribute('selectedNodules', ['one', 'two']);
                    //
                    // Try commenting this out to see the default behaviour
                    // By default, the tool uses Javascript's Prompt function
                    // to ask the user for the annotation. This example uses a
                    // slightly nicer HTML5 dialog element.
                    //
                    // Enable all tools we want to use with this element
                    // cornerstoneTools.arrowAnnotate.activate(element, 1);
                    // cornerstoneTools.arrowAnnotateTouch.activate(element);
                    cornerstoneTools.arrowAnnotate.setConfiguration(viewerThis.config);
                    viewerThis.initNoduleBoxTool_SeriesCell(element);
                    reactcomp.setState({}, function(){
                      //console.log("reactcomp.state.sliceIndex: " + reactcomp.state.sliceIndex);
                      //viewerThis.initNoduleBoxTool_SeriesCell(element);
                      if(callback) { callback(); }
                      //viewerThis.initNoduleBoxTool_SeriesCell(element);
                    }.bind(this));
                    //if(callback) { callback(); }
                  });
                });
              }
              else{console.log("LiverViewer::activateSeriesCell: instance NOT exist!");}
            }
            else{console.log("LiverViewer::activateSeriesCell: seriesNumber NOT exist!");}
          });
        }
        else{console.log("LiverViewer::activateSeriesCell: reactcomp NOT exist!");}
      }
      else{console.log("LiverViewer::activateSeriesCell: element NOT exist!");}
    }
  }
  getFirstAvailableSeriesNumber() { 
    if(this.props){
      let serieses = this.props.series;
      if(serieses){
        if(serieses.length>0){
          let series = serieses[0];
          if(series){
            let seriesNumber = series.seriesNumber;
            if((seriesNumber !== undefined) && (seriesNumber !== null)){
              return seriesNumber;
            }
          }
        }  
      }
    }
    return null;
  }
  componentDidMount() {
    //console.log("LiverViewer: componentDidMount");
    this.setState({currentSeriesNumber: this.getFirstAvailableSeriesNumber()}, function(){
      //console.log("LiverViewer: componentDidMount: setState(ed): ");
      window.addEventListener("resize", this.onWindowResize);
      document.addEventListener('keydown', this.onKeyDown, false); // when pressed esc, exit selected tool
      this.activateSeriesCell(this.state.currentActiveGridCell, this.state.currentSeriesNumber, 0, function(){
        //let element = document.getElementById('r0c0');
        //let reactcomp = this.findCompByDOM(element);
        //console.log("DidMount: reactcomp.state.seriesNumber: " + reactcomp.state.seriesNumber);
        //this.nodulePanelLiverComponent.setState({},function(){console.log("________________-______________");}.bind(this));
        this.setState({});
      }.bind(this));
    }.bind(this));
  }
  componentWillMount() {
    if (this.props.tools != undefined) {
      Object.keys(this.props.tools).map(item => {
        Object.keys(this.props.tools[item]).map(inst => {
          if (this.props.tools[item][inst]['length']) {
            Object.keys(this.props.tools[item][inst]['length']).map(tool => {
              this.props.tools[item][inst]['length'][tool]['isAdded'] = false
            })
          }
          if (this.props.tools[item][inst]['angle']) {
            Object.keys(this.props.tools[item][inst]['angle']).map(tool => {
              this.props.tools[item][inst]['angle'][tool]['isAdded'] = false
            })
          }
          if (this.props.tools[item][inst]['freehand']) {
            Object.keys(this.props.tools[item][inst]['freehand']).map(tool => {
              this.props.tools[item][inst]['freehand'][tool]['isAdded'] = false
            })
          }
        })
      })
    }
    this.props.labels.map((n, idx) => {
      n['liradsScore'] = 'None'
      n['key'] = idx
    })
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.onWindowResize);
    document.removeEventListener('keydown', this.onKeyDown, false)
    this.disableCornerstoneGridCells();
  }

  toggleInvert() {
    for (let r=0; r<this.gridLayoutRows; ++r) {
      for (let c=0; c<this.gridLayoutCols; ++c) {
        try {
          const element = document.getElementById('r'+r+'c'+c)

          const viewport = cornerstone.getViewport(element)
          viewport.invert = !this.state.invert
          cornerstone.setViewport(element, viewport)
          this.setState({
            invert: viewport.invert
          })
        } catch (error) {
          continue
        }
      }
    }
  }
  saveObservationToReport = (selectedNodule) => {
    if (selectedNodule) {
      const percentX = selectedNodule.coord.x / 512
      const percentY = selectedNodule.coord.y / 512
      let imagesBlobs = {}
      let promises = []
      for (let r=0; r<this.gridLayoutRows; ++r) {
        for (let c=0; c<this.gridLayoutCols; ++c) {
          promises.push(new Promise((resolve, reject) => {
            try {
              const mimetype = 'image/png'
              const canvas = document.querySelector('#liver-viewer-container .crop-canvas')
              // const viewCanvas = document.querySelector('#r0c0 .cornerstone-canvas')
              const viewCanvasWrapper = document.querySelector('#r'+r+'c'+c)

              //console.log('current element!!!', viewCanvasWrapper)	//!*!*!*! Original Console Log
              // cornerstone.setToPixelCoordinateSystem(this.zElement, viewCanvas.getContext('2d'))
              const image = cornerstone.getImage(viewCanvasWrapper)

              let myimage = new Image();
              myimage.setAttribute('crossOrigin', 'anonymous');
              myimage.onload = () => {
                canvas.getContext('2d').drawImage(myimage, 0, 0);

                if (viewCanvasWrapper.getAttribute('seriesnumber') == selectedNodule.seriesNumber) {
                  const startX = canvas.width * percentX + 50
                  const startY = canvas.height * percentY + 50

                  //console.log('startX', startX)	//!*!*!*! Original Console Log
                  //console.log('startY', startY)	//!*!*!*! Original Console Log

                  drawArrow(canvas.getContext('2d'), startX, startY, startX-50, startY-50, 3, 1, 20, 10, '#f36', 4);
                }

                canvas.toBlob(image => {
                  imagesBlobs[viewCanvasWrapper.getAttribute('seriesnumber')] = image

                  resolve(image)

                }, mimetype, 1)
              }
              myimage.src = image.imageId;
            } catch(e) {
              console.warn('got undefined?', e)
              reject('element undefined!')
            }
          }))
        }
      }
      executeAllPromises(promises).then((items) => {
        // Result
        var errors = items.errors.map(function(error) {
          return error.message
        }).join(',');
        var results = items.results.join(',');

        //console.log(`Executed all ${promises.length} Promises:`);	//!*!*!*! Original Console Log
        //console.log(`— ${items.results.length} Promises were successful: ${results}`);	//!*!*!*! Original Console Log
        //console.log(`— ${items.errors.length} Promises failed: ${errors}`);	//!*!*!*! Original Console Log
        //console.log('image blobs', items.results)	//!*!*!*! Original Console Log

        var data = new FormData()
        let seriesNumbers = []
        Object.keys(imagesBlobs).map(function(key, index) {
          // imagesBlobs[key] *= 2;
          data.append('file-' + index, imagesBlobs[key], 'image' + index + '.png') //here key is series number
          seriesNumbers.push(key)
        });
        // for (let i=0; i<imagesBlobs.length; ++i) {
        //   data.append('file-' + i, imagesBlobs[i], 'image' + i + '.png')
        // }
        // data.append('files', items.results[0], 'image.png')

        data.append('labelID', selectedNodule.labelID)
        data.append('seriesNumbers', seriesNumbers)
        var config = {
          onUploadProgress: function(progressEvent) {
            var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total )
            //console.log(percentCompleted)	//!*!*!*! Original Console Log
          }
        }
        const studyUID = this.props.studyUID
        axios.post(`/api/send2report-liver/${studyUID}`, data, config)
          .then((res) => {
            this.report = Object.assign({operation: 'update'}, res.data.data)
            //console.log('response data!!!!', res.data.data)	//!*!*!*! Original Console Log
            message.success(GetLocaledText(this, "LiverViewer.ToolBar.screenshot.message.success"))
          })
          .catch(function (err) {
            message.error(GetLocaledText(this, "LiverViewer.ToolBar.screenshot.message.error"))
          })
      });

    } else {
      message.warn(GetLocaledText(this, "LiverViewer.ToolBar.screenshot.message.warn"))
    }
  }
  removeObservationInReport = (nodule) => {
    let newObservationsKeys = Object.keys(this.report.observations).filter((key, idx) => key !== nodule.labelID)
    let newObservations = {}
    newObservationsKeys.map(inst => {
      if (this.report.observations[inst]) {
        newObservations[inst] = this.report.observations[inst]
      }
    })


    this.report.observations = Object.assign(newObservations)

    console.log('this.props.report.observations', this.report.observations)

    let newReport = Object.assign(this.report)
    newReport['operation'] = 'update_observations'

    api.updateReportLiver(newReport, {studyUID: this.props.studyUID})
  }
  addNodule = () => {
    const element = document.getElementById(this.state.currentActiveGridCell)

    let coord = {
      // x: this.state.imgPosX + 1,
      // y: this.state.imgPosY + 1,
      z: this.state.imgPosZ + 1
    }
    api.saveLabelsLiver({
      operation: 'create',
      coord
    }, {studyUID: this.props.studyUID})
    .then(resp => {
      if (_.isEmpty(resp)) {return}
      if (this.state.labels.find(label => label.labelID === resp.labelID)) {return}
      resp.new = true
      this.setState({
        labels: [resp, ...this.state.labels]
      }, () => {
        // cornerstone.updateImage(this.xElement)
        // cornerstone.updateImage(this.yElement)
        cornerstone.updateImage(element)
      })
    })
  }
  removeNodule = (labelID) => {
    const element = document.getElementById(this.state.currentActiveGridCell)

    api.saveLabelsLiver({
      operation: 'remove',
      labelID
    }, {studyUID: this.props.studyUID})
    .then(resp => {
      this.setState({
        labels: [...this.state.labels].filter(label => label.labelID !== labelID)
      }, () => {
        // cornerstone.updateImage(this.xElement)
        // cornerstone.updateImage(this.yElement)
        cornerstone.updateImage(element)
        message.success(GetLocaledText(this, "NodulePanelLiver.message.deleted"))
      })
    })
  }
  onToolClick(name) {
    const elem = document.getElementById(this.state.currentActiveGridCell)
    const comp = this.findCompByDOM(elem);
    if (name === 'screenshot') {
      return;
    }
    // this.tools['freehand'].deactivate(element, 1)

    if (name === 'clear') {
      cornerstoneTools.clearToolState(elem, 'angle')
      cornerstoneTools.clearToolState(elem, 'freehand')
      cornerstoneTools.clearToolState(elem, 'length')

      try {
        let stateData = cornerstoneTools.getToolState(elem, 'angle').data
        stateData.forEach(d => d = {})
        cornerstoneTools.removeToolState(elem, 'angle')
      } catch (error) {
        console.log(error)
      }
      try {
        let stateData = cornerstoneTools.getToolState(elem, 'freehand').data
        stateData.forEach(d => d = {})
        cornerstoneTools.removeToolState(elem, 'freehand')
      } catch (error) {
        console.log(error)
      }
      // try {
      //   let stateData = cornerstoneTools.getToolState(elem, 'length').data = {}
      //   stateData.forEach(d => d = {})
      //   cornerstoneTools.removeToolState(elem, 'length')
      // } catch (error) {
      //   console.log(error)
      // }

      // cornerstoneTools.getToolState(elem, 'length').data

      const toolData = {}
      api.saveToolsLiverData({
        instanceID: comp.state.sliceIndex, data: toolData,
        seriesNumber: elem.currentSeriesNumber

      }, {studyUID: this.props.studyUID})

      cornerstone.updateImage(elem)
      return
    } else if(name === "viewerSync") {
      this.viewerSync();
    } else {
      for (let r=0; r<this.gridLayoutRows; ++r) {
        for (let c=0; c<this.gridLayoutCols; ++c) {
          try {
            const element = document.getElementById('r'+r+'c'+c)

            // this.tools['freehand'].deactivate(element, 1)

            if (name === 'invert') {
              return this.toggleInvert()
            }
            if (name === 'screenshot') {
              return this.saveScreenshotToReport()
            }
            if (name === 'recover') {
              return cornerstone.reset(element)
            }

            // if (name === 'referenceLines') {
            //   return this.state.activeTool.includes('referenceLines') ? this.disableReferLines() : this.enableReferLines()
            // }

            let activeTool = this.state.activeTool
            if (activeTool.includes(name)) {
              this.tools[name].deactivate(element, 1)
              if (name === 'referenceLines') {
                this.tools[name].disable(element)
              }
              if (name === 'freehand') {
                this.tools[name].deactivate(element)
              }
              this.setState({activeTool: 'pan'})
              this.tools.pan.deactivate(element, 2)
              this.tools.pan.activate(element, 1)
            } else {
              // disable other tool
              for (let aTool of activeTool) {
                if (aTool != name) {
                  if (aTool == 'pan') this.tools[aTool].deactivate(element, 1)
                  if (aTool == 'angle') this.tools[aTool].deactivate(element, 1)
                  if (aTool == 'length') this.tools[aTool].deactivate(element, 1)
                  if (aTool == 'freehand') this.tools[aTool].deactivate(element, 1)
                  if (aTool == 'wwwc') this.tools[aTool].deactivate(element, 1)
                  if (aTool == 'zoom') this.tools[aTool].deactivate(element, 1)
                  if (aTool == 'referenceLines') {
                    this.tools[aTool].disable(element)
                  }
                }
              }
              if (name === 'freehand') {
                noduleBox.deactivate(element, 1)

              }
              if (name === 'referenceLines') {
                this.tools[name].enable(element, {
                  updateCurrentPos: (crossX, crossY, crossZ) => {
                    this.setState({
                      imgPosX: crossX,
                      imgPosY: crossY,
                      imgPosZ: crossZ
                    })
                  }
                })
              }
              this.tools[name].activate(element, 1)
              this.tools.pan.activate(element, 2) // use scroll button on mouse to pan
              this.setState({activeTool: [name]})

            }

          } catch (error) {
            console.log(error);
            continue
          }
        }
      }
    }
  }
  saveToolsData_SeriesCell = (e) => { // FIXIT: this function is called more frequently than expected (Jackey: Seems fixed at 201807101747)
    //Jackey: BUG-0158
    console.log("LiverViewer::saveToolsData_SeriesCell is called");
    if(e){
      const element = e.target;
      if(element){
        const reactcomp = this.findCompByDOM(element);
        if(reactcomp){
          if(reactcomp.state){
            let seriesNumber = reactcomp.state.seriesNumber;
            if((seriesNumber !== undefined)&&(seriesNumber !== null)){
              //console.log("seriesNumber = " + seriesNumber);
              let toolData = this.retrieveElementToolData(element);
              if(!(_.isEmpty(toolData))){
                // this.updateLocalToolData(element)
                //console.log(element.series);
                const series = element.series.find(function(series) {return series.seriesNumber == String(seriesNumber);});
                if(series){
                  const instances = series.instances;
                  if(instances){
                    const instancesZ = instances.z; //This is Axial images
                    if(instancesZ){
                      const sliceIndex = reactcomp.state.sliceIndex;  //By Jackey: @sliceIndex is a number of a zero-based index
                      const sliceIDStr = String(sliceIndex + 1);      //By Jackey: @sliceIDStr is a string of a one-based index
                      //console.log("sliceIndex: " + sliceIndex);
                      const instance = series.instances.z[sliceIDStr];
                      //console.log("this.currentInstanceIdx: " + this.currentInstanceIdx + " ,(type: " + typeof(this.currentInstanceIdx) + ")");
                      if (instance) {
                        // this.updateLocalToolData(e.target)
                        api.saveToolsLiverData({  //This is the point having one more warnings every time user calls onInstanceChange (e.g. 19, 19+20, 19+20+21 warnings and so on...)
                          instanceID: sliceIndex,
                          seriesNumber: seriesNumber,
                          data: toolData
                        }, {studyUID: this.props.studyUID})
                      }
                      else{console.log("LiverViewer::saveToolsData_SeriesCell: The current image of the series does NOT exist!!!");}
                    }
                    else{console.log("LiverViewer::saveToolsData_SeriesCell: The Axial view of the images does NOT exist!!!");}
                  }
                  else{console.log("LiverViewer::saveToolsData_SeriesCell: The series contains no instances (no 2D images)");}
                }
                else{console.log("LiverViewer::saveToolsData_SeriesCell: No series with seriesNumber " + seriesNumber + ".");}
              }
              //else{console.log("LiverViewer::saveToolsData_SeriesCell: No Tool Data");} //This is normal because tool may not be added by user on images.
            }
            else{console.log("LiverViewer::saveToolsData_SeriesCell: seriesNumber of the React Component does NOT exist!!!");}
          }
          else{console.log("LiverViewer::saveToolsData_SeriesCell: The state of React Component does NOT exist");}
        }
        else{console.log("LiverViewer::saveToolsData_SeriesCell: The React Component does NOT exist!!!");}
      }
      else{console.log("LiverViewer::saveToolsData_SeriesCell: The element does NOT exist!!!");}
    }
    else{console.log("LiverViewer::saveToolsData_SeriesCell: The event object does NOT exist!!!");}
  }
  retrieveElementToolData = (element) => {
    let toolData = {}
    for (let name of TOOLS_NAME) {
      let state = cornerstoneTools.getToolState(element, name);
      if (state) {
        //console.log("name: " + name);
        //console.log(state.data);
        //let data = Immutable.fromJS(state.data).toJS()
        let data = state.data;
        //console.log(data);
        data.forEach(d => {
          d.active = false;
          d.selected = false;
          d.visible = true;
        })
        toolData[name] = data;
      }
    }
    return toolData;
  }

  saveLabelsData = () => {
    // TODO: make it work when new label added
    let labels = this.tools.lesionsMarker.getState()
    console.log('save label data', labels)
    // this.setState({
    //   labels
    // })
    const instance = this.props.instances.z[this.currentInstanceIdx]
    api.saveLabelsLiver({
      instanceID: instance.instanceID,
      labellist: labels
    }, {studyUID: this.props.studyUID})
    // .then(resp => {
    //   this.tools.lesionsMarker.setState(resp.data)
    // })
  }

  isForbidEdit() {
    const user = getUser()
    const report = this.props.report
    if (report.userID !== user.account && report.examiner !== user.account) {
      // not belong to current user
      return true
    }
    return (
      report.examined
      || (report.submitted && user.usergroup === 'reporter')
      || (!report.submitted && user.usergroup === 'examiner')
    )
  }
  generateLayout = () => {
    let layout = []
    for (let r=0; r<this.gridLayoutRows; ++r) {
      for (let c=0; c<this.gridLayoutCols; ++c) {
        layout.push({i: 'r'+r+'c'+c, x: c, y: r, w: 1, h: 1, static: true})
      }
    }
    return layout
  }
  onLayoutChange() {} //Jackey: Please keep this empty function, becuase someone calls it.

  handleClose() {
    this.setState({
      leftSideBarCollapsed: true
    })
  }

  handleOpen() {
    this.setState({
      leftSideBarCollapsed: false
    })
  }

  handleToggleLeftSideBar() {
    this.setState({
      leftSideBarCollapsed: this.state.leftSideBarCollapsed ? false : true
    })
  }

  arrangeLayoutGridCells(rows, cols, callback = null, bForce = false) {
    if((bForce === true) || (this.gridLayoutCols != cols) || (this.gridLayoutRows != rows)){
      this.gridLayoutRows = rows;
      this.gridLayoutCols = cols;
      let gcCount = this.getGridCellsCount();
      ResizeArray(this.reactcomps, gcCount, null);
      ResizeArray(this.elementCovers, gcCount, React.createRef, false);
      ResizeArray(this.elementWrappers, gcCount, null);
      if(callback != null) {this.setState({}, callback);}
    }
  }
  onLayoutSelected(rows, cols, bForce = false) {this.arrangeLayoutGridCells(rows, cols, this.resizeCornerstoneGridCells, bForce);}
  enableCornerstoneGridCells() {
    let gcCount = this.getGridCellsCount();
    for(let i =0; i<gcCount; ++i){
      let elementCover = this.elementCovers[i];
      if(typeof(elementCover) === 'object'){
        let reactcomp = elementCover.current;
        if((reactcomp !== undefined) && (reactcomp !== null)){
          if(reactcomp.state){
            let element = reactcomp.state.element;
            if((element !== undefined) && (element !== null)){
              cornerstone.enable(element);
            }
          }
        }
      }
    }
  }
  disableCornerstoneGridCells() {
    let gcCount = this.getGridCellsCount();
    for(let i =0; i<gcCount; ++i){
      let elementCover = this.elementCovers[i];
      if(typeof(elementCover) === 'object'){
        let reactcomp = elementCover.current;
        if((reactcomp !== undefined) && (reactcomp !== null)){
          if(reactcomp.state){
            let element = reactcomp.state.element;
            if((element !== undefined) && (element !== null)){
              cornerstone.disable(element);
            }
          }
        }
      }
    }
  }
  resizeCornerstoneGridCells() {
    let gcCount = this.getGridCellsCount();
    for(let i =0; i<gcCount; ++i){
      let elementCover = this.elementCovers[i];
      if(typeof(elementCover) === 'object'){
        let reactcomp = elementCover.current;
        if((reactcomp !== undefined) && (reactcomp !== null)){
          if(reactcomp.state){
            let element = reactcomp.state.element;
            if((element !== undefined) && (element !== null)){
              cornerstone.resize(element);
            }
          }
        }
      }
    }
  }
  getGridCellsCount() { return ((this.gridLayoutCols)*(this.gridLayoutRows)); }

  // generateElementWrappers(num) {
  //   this.elementWrappers = []
  //   for (let i=0; i<num; ++i) {
  //     this.elementWrappers.push(null)
  //   }
  // }
  //
  // generateCornerstoneElements(num) {
  //   this.elementCovers = []
  //   for (let i=0; i<num; ++i) {
  //     this.elementCovers.push(React.createRef());
  //   }
  // }
  //
  // generateCornerstoneReactComps(num) {
  //   this.reactcomps = [];
  //   for (let i=0; i<num; ++i) {
  //     this.reactcomps.push(null);
  //   }
  // }

  openAnnotationDialogModal = () => {
    this.setState({modalAnnotationDialogIsOpen: true});
  }

  afterOpenAnnotationDialogModal = () => {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }

  closeAnnotationDialogModal = () => {
    this.setState({modalAnnotationDialogIsOpen: false});
  }

  openRelabelDialogModal = () => {
    this.setState({modalRelabelDialogIsOpen: true});
  }

  afterOpenRelabelDialogModal = () => {
    // references are now sync'd and can be accessed.
    // this.subtitle.style.color = '#f00';
  }

  closeRelabelDialogModal = () => {
    this.setState({modalRelabelDialogIsOpen: false});
  }

  handleChangeAnnotationTextInput = (event) => {
    this.setState({AnnotationTextInputValue: event.target.value});
  }

  // Define a callback to get your text annotation
  // This could be used, e.g. to open a modal
  getTextCallback(doneChangingTextCallback) {
      var annotationDialog  = this.annotationDialog;
      var getTextInput = this.annotationTextInput;
      var confirm = this.annotationDialogConfirm
      // annotationDialog.showModal();
      this.openAnnotationDialogModal();
      this.annotationDialogConfirm.removeEventListener('click', closeHandler);
      // this.annotationDialogConfirm.addEventListener('click', closeHandler);
      this.annotationDialogConfirm.addEventListener('click', () => {
        // annotationDialog.close();
        this.closeAnnotationDialogModal();
        doneChangingTextCallback(this.state.AnnotationTextInputValue);
        // Reset the text value
        this.state.AnnotationTextInputValue = ""
        // this.annotationTextInput.value = "asdf";
        this.saveToolsData_SeriesCell();
      });
      // this.annotationDialog.removeEventListener("keydown", keyPressHandler);
      // this.annotationDialog.addEventListener('keydown', keyPressHandler);
      function keyPressHandler(e) {
          // If Enter is pressed, close the dialog
          if (e.which === 13) {
              closeHandler();
          }
      }
      function closeHandler(e, ref) {
          // annotationDialog.close();
          this.closeAnnotationDialogModal();
          doneChangingTextCallback(getTextInput.value);
          // Reset the text value
          getTextInput.value = "";
          cornerstoneTools.getToolState(
            document.getElementById(this.state.currentActiveGridCell,
              'arrowAnnotate')).data.push({})
          cornerstoneTools.getToolState(
            document.getElementById(this.state.currentActiveGridCell,
              'arrowAnnotate')).data.pop()
      }
  };

  // Define a callback to edit your text annotation
  // This could be used, e.g. to open a modal
  changeTextCallback(data, eventData, doneChangingTextCallback) {
      var relabelDialog = document.querySelector('.relabelDialog');
      var getTextInput = relabelDialog.querySelector('.annotationTextInput');
      var confirm = relabelDialog.querySelector('.relabelConfirm');
      var remove = relabelDialog.querySelector('.relabelRemove');

      getTextInput.value = data.text;
      relabelDialog.showModal();

      function confirmHandler() {
         relabelDialog.close();
         doneChangingTextCallback(data, getTextInput.value);
      }

      confirm.removeEventListener('click', confirmHandler);
      confirm.addEventListener('click', confirmHandler);

      // If the remove button is clicked, delete this marker
      function removeHandler() {
        relabelDialog.close();
        doneChangingTextCallback(data, undefined, true);
      }

      remove.removeEventListener('click', removeHandler);
      remove.addEventListener('click', removeHandler);

      function keyPressHandler(e) {
          // If Enter is pressed, close the dialog
          if (e.which === 13) {
              closeHandler();
          }
      }

      relabelDialog.removeEventListener("keydown", keyPressHandler);
      relabelDialog.addEventListener('keydown', keyPressHandler);

      function closeHandler() {
          relabelDialog.close();
          doneChangingTextCallback(data, getTextInput.value);
          // Reset the text value
          getTextInput.value = "";
      }
  };

  resetSelectedNodule = (callback = null, bResetNodule = true) => {
    if(bResetNodule === true){
      this.nodulePanelLiverComponent.setState({
        selectedNodule: null
      }, callback);
    }
  }

  onSelectNoduleHandle(nodule, callback = null) {
    console.log("onSelectNoduleHandle");
    // console.log(nodule);
    //noduleBox.resetLatestClickCoords()
    this.setState({selectedNodules: nodule}, function(){
      const element = document.getElementById(this.state.currentActiveGridCell);

      // if (this.state.activeTool.includes('referenceLines')) {
      //   // TODO: make for flexible width and height
      //   const horizontalLayers = 512
      //   const verticalLayers = 512
      //   const percentX = nodule.coord.x / horizontalLayers
      //   const percentY = nodule.coord.y / verticalLayers
  
      //   // console.log('POS: pixel to canvas', cornerstone.pixelToCanvas(element, {x: nodule.coord.x, y: nodule.coord.y}))
      //   referenceLine2D.updateToolData(element, {x: percentX, y: percentY})
      //   this.nodulePanelLiverComponent.setState({selectedNodules: nodule})
      // }
      // // referenceLine2D.disable(element)
      // // this.tools['referenceLines'].disable(element)
      // // this.tools['referenceLines'].deactivate(element)
  
      // //cornerstone.updateImage(element);
      //console.log("currentSeriesNumber: " + this.state.currentSeriesNumber + ", nodule.seriesNumber: " + nodule.seriesNumber);
      //this.initNoduleBoxTool_SeriesCell(element);
      if (this.state.currentSeriesNumber != nodule.seriesNumber) {
        // noduleBox.disable(element)
        // noduleBox.deactivate(element)
        this.onInstanceChange(nodule.seriesNumber, nodule.coord.z, function(){
          if(callback){
            callback();
            this.setState({},function(){
              this.scrollTo_SeriesCell(element, nodule.coord.z, true)
              .then(function(){
                //console.log("<><><><><><><><><><><><><><><><><>");
                //this.initNoduleBoxTool_SeriesCell(this.state.currentActiveGridCell);
                // const reactcomp = this.findCompByDOM(element);
                // if(reactcomp){
                //   reactcomp.setState({},function(){
                //     this.initNoduleBoxTool_SeriesCell(this.state.currentActiveGridCell);
                //     reactcomp.setState({});
                //   }.bind(this));
                // }
              }.bind(this));
            }.bind(this));
          }
        }.bind(this), false);
      }
      else {
        if(element){
          this.scrollTo_SeriesCell(element, nodule.coord.z, true)
          .then(function(){
            if(callback){callback();}
            //this.initNoduleBoxTool_SeriesCell(this.state.currentActiveGridCell);
          }.bind(this));
        }
      }
    }.bind(this));
  }

  viewerSync() {
    this.state.bIsViewerSyncing = !(this.state.bIsViewerSyncing);
    if(this.state.bIsViewerSyncing){ this.setState({activeTool: _.without(this.state.activeTool, 'viewerSync').concat(['viewerSync'])}); }
    else{                            this.setState({activeTool: _.without(this.state.activeTool, 'viewerSync')                      }); }
    //console.log("viewerSync: this.state.bIsViewerSyncing = " + this.state.bIsViewerSyncing);
    //console.log(this.state.activeTool);
  };

  render() {
    const generatedLayout = this.generateLayout()
    const element = document.getElementById(this.state.currentActiveGridCell)

    //console.log('this.getOpenedSeriesGcDomIDs()', this.getOpenedSeriesGcDomIDs())	//!*!*!*! Paul Console Log
    //console.log(this.props.report);	//!*!*!*! Paul Console Log
    return (
      <div id='liver-viewer-container'>
        <canvas className='crop-canvas' style={{position: 'absolute', zIndex: '-999'}} width='800' height='800'/>
        {/*<Modal
        ref={inp => this.annotationDialog = inp}
        isOpen={this.state.modalAnnotationDialogIsOpen}
        onAfterOpen={this.afterOpenAnnotationDialogModal}
        onRequestClose={this.closeAnnotationDialogModal}
        contentLabel="Example Modal"
        className="annotationDialog">
            <h5>Enter your annotation</h5>
            <div
            className="annotationTextInputOptions">
                <label for="annotationTextInput">New label</label>
                <input
                  ref={inp => this.annotationTextInput = inp}
                  name="annotationTextInput"
                  className="annotationTextInput"
                  type="text"
                  onChange={this.handleChangeAnnotationTextInput}
                  value={this.state.AnnotationTextInputValue} />
            </div>
            <a
            ref={inp => this.annotationDialogConfirm = inp}
            className="annotationDialogConfirm btn btn-sm btn-primary">OK</a>
        </Modal>*/}
        {/*<Modal
        ref={inp => this.relabelDialog = inp}
        isOpen={this.state.modalRelabelDialogIsOpen}
        onAfterOpen={this.afterOpenRelabelDialogModal}
        onRequestClose={this.closeRelabelDialogModal}
        contentLabel="Example Modal"
        className="relabelDialog" oncontextmenu="return false">
            <h5>Edit your annotation</h5>
            <div className="annotationTextInputOptions">
                <label for="annotationTextInput">New label</label>
                <input name="annotationTextInput" className="annotationTextInput" type="text"/>
            </div>
            <div>
                <a className="relabelRemove btn btn-sm btn-secondary">Remove marker</a>
                <a className="relabelConfirm btn btn-sm btn-primary">OK</a>
            </div>
        </Modal>*/}
        {/*<LanguageDropdownList setAppState={(state) => this.setState(state)}/>*/}
        {<ToolBar
          onToolClick={this.onToolClick.bind(this)}
          activeTool={this.state.activeTool}
          forbidEdit={this.isForbidEdit()}
          onLayoutSelected={(cols, rows) => this.onLayoutSelected(rows, cols)}
          // onScreenshotBtnClick={this.onScreenshotBtnClick.bind(this)}
        />}

        <div style={{
          display: 'flex',
          flexDirection: 'row',
          height: 'calc(100% - 64px)'
        }}>

          <div  className={this.state.leftSideBarCollapsed ? 'left-side-bar-collapsed' : 'left-side-bar'}>

            {<NodulePanelLiver
              series={this.props.series}
              data={this.state.labels}
              ref={input => {this.nodulePanel = input}}
              onSelectNodule={this.onSelectNoduleHandle.bind(this)}
              setThisComponentToParent={(component) => this.nodulePanelLiverComponent = component}
              viewer={this}
              cornerstoneElement={element}
              hideSelection={
                //!this.props.report
                false
              }
              report={this.props.report}
              onAddObservation={this.saveObservationToReport}
              onRemoveObservation={this.removeObservationInReport}
              onStartDetect={this.addNodule}
              onDelete={this.removeNodule}
              activeTool={this.state.activeTool}
              style={{alignSelf: 'flex-start' }}
              toggleLeftSideBar={this.handleToggleLeftSideBar.bind(this)}
              updateLiverViewerState={(record) => {
                this.setState({
                  selectedNodules: record,
                  imgPosZ: record.coord.z,
                  imgPosY: record.coord.y,
                  imgPosX: record.coord.x,
                })
                // this.setState({
                //   imgPosZ: record.coord.z,
                //   imgPosY: record.coord.y,
                //   imgPosX: record.coord.x,
                // })
                // try {
                // }
                // catch(error) {
                //   console.warn('got not defined record.coord...', error)
                // }
                //this.onWindowResize()
                // if (aTool == 'referenceLines') {
                  // this.tools['referenceLines'].deactivate(element)
                  // this.tools['referenceLines'].disable(element)
                // }
              }}
              studyUID={this.props.studyUID}
              />}

          </div>

          <div ref={inp => this.viewerContainer = inp} className='liver-viewer' style={this.props.style} style={{alignSelf: 'flex-start', height: '100%'}}>

            <div className='viewer-content' style={{
              // height: 'calc(100%-64px)',
              height: '100%'
            }}>
              {this.props.patientInfoBox}

              <div style={{
                width: '100%',
                position: 'relative',
                display: 'inline',
                height: '100%',
                    }}>
                {<GridLayout className="layout"
                  onLayoutChange={this.onLayoutChange.bind(this)}
                  margin={[2,2]}
                  layout={generatedLayout}
                  rowHeight={document.getElementsByClassName('viewer-content')[0] ? (document.getElementsByClassName('viewer-content')[0].offsetHeight ) / this.gridLayoutRows: 400}
                  cols={this.gridLayoutCols}
                  width={document.getElementsByClassName('viewer-content')[0] ? document.getElementsByClassName('viewer-content')[0].offsetWidth : this.state.gridLayoutWidth}
                  style={{
                    backgroundColor: '#181b1b',
                    position: 'relative',
                    display: 'inline-block',
                    width: '100%',
                    height: '100%',
                    zIndex: '0'}}
                  >
                  {
                    generatedLayout.map((item, gcIndex) => {
                      const gcDomID = item.i;
                      return (
                        <div
                          key={gcDomID}
                          // style={{backgroundColor: getRandomColor()}}
                          onDragOver={e => e.preventDefault()}
                          onDrop={e => {
                            e.preventDefault();
                            let canvasDOM = e.target;
                            let destElement = canvasDOM;
                            if(canvasDOM){if(canvasDOM.classList.contains("cornerstone-canvas")){destElement = destElement.parentNode;}}
                            if(destElement){this.onDropSeries(destElement, e.dataTransfer.getData('seriesNumber').replace(/['"]+/g, ''), gcIndex, gcDomID);}
                          }}
                        >
                          {
                            gcIndex < 0 ? <div></div> :
                            <div className='cornerstone-elm-wrapper'
                              onContextMenu={e => e.preventDefault()}
                              ref={input => {this.elementWrappers[gcIndex] = input}}
                              index={gcIndex}
                              onClick={(e) => {
                                e.preventDefault();
                                //console.log("React Custom Class SeriesCell OnClick (Wrapper): ");	//!*!*!*! Jackey Console Log
                                let theInnerElement = this.elementCovers[gcIndex];
                                if(theInnerElement){
                                  //let theInnerReactcomp = this.findCompByDOM(theInnerElement);        //By Jackey: gcIndex does not exist in @theInnerElement but this comment line is reserved for future reference
                                  //let theInnerReactcomp = this.elementWrappers[gcIndex].children[0];  //By Jackey: @theInnerReactcomp is useful for debugging and this comment line is reserved for future reference
                                  if (this.state.currentActiveGridCell != gcDomID) {
                                    this.resetSelectedNodule()
                                    this.setState({
                                      currentActiveGridCell: gcDomID,
                                      //currentSeriesNumber: theInnerReactcomp.state.seriesNumber
                                    });
                                  }
                                  theInnerElement.current.onClick_Handler(e);
                                }
                                else{console.log("React Wrapper theInnerElement NOT exist");}
                              }}
                              onWheel={(e) => {
                                e.preventDefault();
                                let theInnerElement = this.elementCovers[gcIndex];
                                if(theInnerElement){
                                  //let theInnerReactcomp = this.findCompByDOM(theInnerElement);
                                  let theInnerReactcomp = this.elementWrappers[gcIndex].children[0];
                                  if(theInnerReactcomp || true){  //Currently, theInnerReactcomp is null because it's empty before drop. Use "true" for temporary fix.
                                    theInnerElement.current.onWheel_Handler(e);
                                  }
                                  else{console.log("React Wrapper theInnerReactcomp NOT exist");}
                                }
                                else{console.log("React Wrapper theInnerElement NOT exist");}
                              }}
                              style={this.state.currentActiveGridCell == gcDomID ? {outline: '2px solid #3399ff'} : {outline: '1px solid rgba(153, 203, 255, 0.2)'}}
                              // style={{width: "200px", height: "200px"}}
                              // style={{position: 'absolute', zIndex: '-9999'}}
                              >
                              {/*<SeriesCell ref={this.elementCovers[gcIndex]} viewer={this} gcDomID={gcDomID} gcIndex={gcIndex} seriesNumber={null} sliceIndex={0} strConsoleMsg={"LiverViewer::SeriesCell["+gcDomID+"]"}></SeriesCell>*/}
                              {<SeriesCell ref={this.elementCovers[gcIndex]} viewer={this} gcDomID={gcDomID} gcIndex={gcIndex} seriesNumber={null} sliceIndex={0} strConsoleMsg={null}></SeriesCell>}
                            </div>
                          }
                        </div>
                      )
                    })
                  }
                </GridLayout>}
              </div>


              {<LabelPopover
                top={this.state.labelPopover.position.y}
                left={this.state.labelPopover.position.x}
                show={this.state.labelPopover.show}
                value={this.state.labelPopover.value}
                id={this.state.labelPopover.id}
                imageElm={this.state.labelPopover.imageElm}
                onChange={this.changeLabelName.bind(this)}
                onClose={this.closeLabelPopover}
              />}
              {
                this.props.instances.z.length > 1 ?
                <SeriesThumbnailList
                  viewer={this}
                  data={this.props.data}
                  defaultIdx={0}
                  series={this.props.series}
                  onChange={this.onInstanceChange}
                  setCurrentSeriesNumber={seriesNumber => this.state.currentSeriesNumber = seriesNumber}
                  setThisComponentToParent={(component) => this.seriesThumbnailList = component}/>
                :
                ''
              }
            </div>
            {/*<div className='bottom-left-text'>Scale: {this.state.viewport.scale.toFixed(2)}</div>*/}
            {/* <div className='bottom-right-text'>
              <p>WW: {Math.round(this.state.viewport.voi.windowWidth)}</p>
              <p>WC: {Math.round(this.state.viewport.voi.windowCenter)}</p>
            </div> */}
          </div>
        </div>
      </div>
    )
  }

}

export default injectIntl(Viewer)

//Variables name meaning:
//@gcIndex,     stands for "Grid Cell Index", is an integer specifiying the index of gridcells
//@gcDomID,     stands for "Grid Cell DOM ID", is a string, like "r0c0"
//@seriesIndex, stands for "Series's index", is the index on the "SeriesThumbnails" (Different to seriesNumber!)

//Notes:
//viewerThis.props.series contains instances (map), with one-based index.
//viewerThis.props.stacks contains currentImageIdIndex (int) and imageIds (array) with zero-based index.
//imageIds is a zero-based array storing the urls of the PNG of each image
//currentImageIdIndex is a zero-based number to access the imageIds;
//"elementWrapper" has an "elementCover" has an "element", each "element" is linked to a "ReactComp" SeriesCell instance.
//An "element" is a div, it contains a child who is a canvas.
//About passing arguments on "onLayoutSelected", in Toolbar, they are "cols, rows", but in this LiverViewer, they are "rows, cols"
//databaseSeriesToolsSet   is a map where the keys are the seriesNumber (This databaseSeriesToolsSet is a reference to viewerThis.props.tools)
//databaseSeriesTools      is a map with zero-based
//databaseSeriesTools[key] is a map with keys equal to TOOLS_NAME (i.e. 'length', 'angle', 'freehand', 'arrowAnnotate')

//Default values:
//Lung: this.setWWWC(80, 28);
//Liver: ?

//Potential issues:
//Discovered by Jackey at 201807171216: In the viewer class's state, @selectedNodule and @selectedNodules are confusing, there may be bugs because of these two names.

//Possibly but not sure issues:
//Discovered by Jackey at 201807171216: Tools displaying seems 1 index less than correct. (Please check whether it is really the case. I'm not sure yet.)

//Known issues:
//Discovered by Jackey at 201807171216: Cannot save tools on images
//Discovered by Jackey at 201807171216: Selecting a nodule on left panel (not ticking it yet) display wrong index within 0.1s (Seems acceptable but it's better to fix it in the future)
//Discovered by Jackey at 201807171358: When selecting the nodule, if the series is changed twice, the nodule box will not be shown for that series.
//Discovered by Jackey at 201807171542: When clicking many nodules (ticking them) many times randomly, the sequence of the nodule will be changed permanantly after refreshing the page.
