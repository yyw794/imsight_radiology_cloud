#!/bin/python
# coding=utf-8
import ConfigParser
import logging
import os
from logging.handlers import RotatingFileHandler
from logging import StreamHandler
import requests


config = ConfigParser.ConfigParser()
config.read("/app/config.ini")

# server configure
ADDRESS = config.get("SERVER", "ADDRESS")
PORT = config.getint("SERVER", "PORT")
PROTOCOL = config.get("SERVER", "PROTOCOL")
NEED_AUTH = config.getboolean("SERVER", "NEED_AUTH")
SECRET_KEY = config.get("SERVER", "SECRET_KEY")
MAX_CONTENT_LENGTH = config.getint("SERVER", "MAX_CONTENT_LENGTH")
PACS_PLUGIN_SUPPORT = config.getboolean("SERVER", "PACS_PLUGIN_SUPPORT")
PACS_PLUGIN_TOEKN = config.get("SERVER", "PACS_PLUGIN_TOEKN")
LABEL_PLATFORM = config.getboolean("SERVER", "LABEL_PLATFORM")
SCREENSHOT_ROOT = config.get("SERVER", "SCREENSHOT_ROOT")
IMAGE_ROOT = config.get("SERVER", "IMAGE_ROOT")
DICOM_ROOT = config.get("SERVER", "DICOM_ROOT")
CACHE_ROOT = config.get("SERVER", "CACHE_ROOT")
ERROR_ROOT = config.get("SERVER", "ERROR_ROOT")
CFDA_ROOT = config.get("SERVER", "CFDA_ROOT")
HOST_ROOT = config.get("SERVER", "HOST_ROOT")
LOG_FOLDER = config.get("SERVER", "LOG_FOLDER")
USE_INSTANCE_NUMBER = config.getboolean("SERVER", "USE_INSTANCE_NUMBER")
RADIOMICS_PARAMS = config.get("SERVER", "RADIOMICS_PARAMS")
RADIOMICS_HEADER = config.get("SERVER", "RADIOMICS_HEADER")

SOCKET_PORT = config.get("SOCKET_SERVER", "SOCKET_PORT")
SOCKET_PROTOCOL = config.get("SOCKET_SERVER", "SOCKET_PROTOCOL")

# algorithm server configure
ALGO_ADDRESS = config.get("ALGO_SERVER", "ADDRESS")
ALGO_PORT = config.getint("ALGO_SERVER", "PORT")
FILEMETHOD = config.get("ALGO_SERVER", "FILEMETHOD")

# cloud configure
HOSTNAME = config.get("REGIONAL", "HOSTNAME")
LOG_PATH = config.get("REGIONAL", "LOG_PATH")

# mongoDB
MONGO_NAME = config.get("MONGO", "MONGO_NAME")
MONGO_USERNAME = config.get("MONGO", "MONGO_USERNAME")
MONGO_PASSWORD = config.get("MONGO", "MONGO_PASSWORD")
MONGO_AUTHSOURCE = config.get("MONGO", "MONGO_AUTHSOURCE")
MONGO_DATABASE_NAME = config.get("MONGO", "MONGO_DATABASE_NAME")

# email
MAIL_VERIFY_REQUIRE = config.getboolean("EMAIL", "MAIL_VERIFY_REQUIRE")
MAIL_SERVER = config.get("EMAIL", "MAIL_SERVER")
MAIL_PORT = config.getint("EMAIL", "MAIL_PORT")
MAIL_USE_SSL = config.getboolean("EMAIL", "MAIL_USE_SSL")
MAIL_USERNAME = config.get("EMAIL", "MAIL_USERNAME")
MAIL_PASSWORD = config.get("EMAIL", "MAIL_PASSWORD")
MAIL_DEFAULT_SENDER = config.get("EMAIL", "MAIL_DEFAULT_SENDER")

# filter
DR_FILTER = config.get('FILTER', 'DR_FILTER')
LUNG_FILTER = config.get('FILTER', 'LUNG_FILTER')
IS_FILTER = config.getboolean("FILTER", "IS_FILTER")

C_BODY_PART_EXAMINED = config.get('CT_DX_CHEST', 'BODY_PART_EXAMINED')
C_PROTOCOL_NAME = config.get('CT_DX_CHEST', 'PROTOCOL_NAME')
C_STUDY_DESCRIPTION = config.get('CT_DX_CHEST', 'STUDY_DESCRIPTION')
C_SERIES_DESCRIPTION = config.get('CT_DX_CHEST', 'SERIES_DESCRIPTION')
C_IMAGE_COMMENTS = config.get('CT_DX_CHEST', 'IMAGE_COMMENTS')

L_BODY_PART_EXAMINED = config.get('CT_LIVER', 'BODY_PART_EXAMINED')
L_PROTOCOL_NAME = config.get('CT_LIVER', 'PROTOCOL_NAME')
L_STUDY_DESCRIPTION = config.get('CT_LIVER', 'STUDY_DESCRIPTION')
L_SERIES_DESCRIPTION = config.get('CT_LIVER', 'SERIES_DESCRIPTION')
L_IMAGE_COMMENTS = config.get('CT_LIVER', 'IMAGE_COMMENTS')

V_VIEW_POSITION = config.get('DX_PA_VIEWPOSITION', 'VIEW_POSITION')
V_PATIENT_ORIENTATION = config.get('DX_PA_VIEWPOSITION', 'PATIENT_ORIENTATION')
V_PROTOCOL_NAME = config.get('DX_PA_VIEWPOSITION', 'PROTOCOL_NAME')
V_STUDY_DESCRIPTION = config.get('DX_PA_VIEWPOSITION', 'STUDY_DESCRIPTION')
V_SERIES_DESCRIPTION = config.get('DX_PA_VIEWPOSITION', 'SERIES_DESCRIPTION')

version_config = ConfigParser.ConfigParser()
version_config.read("/app/version.ini")
PRODUCT_VERSION = version_config.get("VERSION", "VERSION")

if not os.path.exists(LOG_FOLDER):
    os.makedirs(LOG_FOLDER)
log_fmt = '%(asctime)s - %(levelname)s - %(filename)s - %(lineno)s - %(message)s'
formatter = logging.Formatter(log_fmt)
log_file_handler = RotatingFileHandler(filename=LOG_FOLDER + '/server.log', maxBytes=1024 * 1024 * 1024,
                                       backupCount=5)
stream_handler = StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)
log_file_handler.suffix = '%Y%m%d.log'
log_file_handler.setFormatter(formatter)
log_file_handler.setLevel(logging.DEBUG)
log = logging.getLogger("radiology")
log.setLevel(logging.DEBUG)
log.addHandler(log_file_handler)
log.addHandler(stream_handler)

logging.getLogger('socketio').setLevel(logging.ERROR)
logging.getLogger('engineio').setLevel(logging.ERROR)

CT_LUNG_CFG = None
DX_CFG = None
RIBS_CFG = None

try:
    algo_config = requests.get("http://{}:{}/api/config".format(ALGO_ADDRESS, ALGO_PORT))
except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as err:
    logging.error("get algo config request error. err=%s", err)

if algo_config.ok:
    algo_cfg = algo_config.json()
    CT_LUNG_CFG = algo_cfg["config"]["CT_LUNG"]
    DX_CFG = algo_cfg["config"]["DX"]
    RIBS_CFG = algo_cfg["config"]["RIBS"]
else:
    logging.error("get algo config response error. err=%s", err)

