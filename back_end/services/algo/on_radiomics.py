#!/bin/python
# coding=utf-8
import os
import six
import thread

import SimpleITK as sitk
import numpy as np
import csv
from flask import make_response, request, jsonify
from radiomics import featureextractor
from scipy.ndimage.morphology import binary_fill_holes

from models.database.database_ops import db_find_one, db_find_update_otherwise_insert
from models.response.response import response
from config import DICOM_ROOT, RADIOMICS_PARAMS, RADIOMICS_HEADER
from models.radiomics.Radiomics import Radiomics
import logging
from config import CT_LUNG_CFG, CFDA_ROOT

logger = logging.getLogger("radiology")

if CT_LUNG_CFG:
    CFDA_TAG = CT_LUNG_CFG["cfda"]
else:
    CFDA_TAG = False


def calRadiomics(image, label_list):
    image_array = sitk.GetArrayFromImage(image)
    image_array = image_array.transpose(2, 1, 0)

    result_list = []
    for label in label_list:
        mask_contour = np.zeros_like(image_array)
        fill_mask = np.zeros_like(image_array)

        for slice in label['nodule_slice']:
            z_pixel = slice['z']
            for point in slice['contour']:
                x_edge = np.int(point[0])
                y_edge = np.int(point[1])
                mask_contour[x_edge, y_edge, z_pixel] = 1
            fill_mask[:, :, z_pixel] = binary_fill_holes(mask_contour[:, :, z_pixel])

        x_center = label['x']
        y_center = label['y']
        z_center = label['z']

        fill_mask = np.asarray(fill_mask, dtype=np.int16)
        mask = sitk.GetImageFromArray(fill_mask)

        extractor = featureextractor.RadiomicsFeaturesExtractor(RADIOMICS_PARAMS)
        result = extractor.execute(sitk.GetImageFromArray(image_array), mask)

        feature_names = ['x_center', 'y_center', 'z_center']
        features = [x_center, y_center, z_center]
        for k, v in six.iteritems(result):
            if k.startswith('original_'):
                feature_names.append(k.strip('original_'))
                features.append(v)

        result_list.append(dict(zip(feature_names, features)))
    return result_list


def thread_create_radiomics(label_list, studyUID, labelType, taskID):
    seriesID = label_list[0]['seriesID']
    dcm_folder_path = os.path.join(DICOM_ROOT, seriesID)

    # find existed radiomices info
    radiomics_info = db_find_one("radiomics", {"taskID": taskID, 'seriesID': seriesID})
    old_data_list = []
    if radiomics_info is not None:
        old_data_list = radiomics_info['data_list']
        old_label_list = [(d['x_center'], d['y_center'], d['z_center']) for d in old_data_list]
        new_label_list = [(l['x'], l['y'], l['z']) for l in label_list]
        # filter new add label when it not in old data list
        label_list = filter(lambda l: (l['x'], l['y'], l['z']) not in old_label_list, label_list)
        # filter old data list when label in new label list
        old_data_list = filter(
            lambda l: (l['x_center'], l['y_center'], l['z_center']) in new_label_list,
            old_data_list)

    radiomics = Radiomics()
    radiomics.taskID = taskID
    radiomics.studyUID = studyUID
    radiomics.seriesID = seriesID
    radiomics.data_list.extend(old_data_list)
    result_state = ""
    try:
        series_names = sitk.ImageSeriesReader.GetGDCMSeriesFileNames(str(dcm_folder_path))
        series_reader = sitk.ImageSeriesReader()
        series_reader.SetFileNames(series_names)
        image = series_reader.Execute()
        if len(label_list) > 0:
            result_list = calRadiomics(image, label_list)
            radiomics.data_list.extend(result_list)
        result_state = "success"
    except Exception as e:
        logger.error('create radiomics error, err=%s', e)
        with open(RADIOMICS_HEADER, 'rb') as reader:
            lines = reader.readlines()
            for label in label_list:
                radiomics_head = [l.strip() for l in lines]
                data_temp = dict(zip(radiomics_head, [0] * len(radiomics_head)))
                data_temp['x_center'] = label['x']
                data_temp['y_center'] = label['y']
                data_temp['z_center'] = label['z']
                result_state = "fail"
                radiomics.data_list.append(data_temp)
    db_find_update_otherwise_insert("radiomics",
                                    {"taskID": radiomics.taskID, 'seriesID': radiomics.seriesID},
                                    radiomics.to_db())
    logger.info("radiomics update " + result_state)


def create_radiomics(label_list, studyUID, labelType, taskID):
    if labelType != 'Nodule': return
    if label_list == None: return
    if len(label_list) == 0: return
    thread.start_new_thread(thread_create_radiomics, (label_list, studyUID, labelType, taskID))


required_key_all = ['taskID', 'seriesID']


def fetch_radiomics():
    """
    @api {get} /auth/radiomics  影像组学
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} taskID 任务ID
    @apiParam {String} seriesID 序列ID
    @apiParamExample {string} Request-Example:
        /api/radiomics?taskID=e3020b5e-f706-11e8-aafd-0242c0a8e004&seriesID=1.3.12.2.1107.5.1.4.66045.30000017120400080334400036582
    @apiSuccessExample {json} Success-Response:
    {
      "data": [
        {
          "data_list": [
            {
              "cm_Autocorrelat": 0,
              "cm_ClusterProminence": 0,
              "cm_ClusterShade": 0,
              "cm_ClusterTendency": 0,
              "cm_Contrast": 0,
              "cm_Correlat": 0,
              "cm_DifferenceAverage": 0,
              "cm_DifferenceEntropy": 0,
              "cm_DifferenceVariance": 0,
              "cm_Id": 0,
              "cm_Idm": 0,
              "cm_Imc1": 0,
              "cm_Imc2": 0,
              "cm_InverseVariance": 0,
              "cm_JointAverage": 0,
              "cm_JointEnergy": 0,
              "cm_JointEntropy": 0,
              "cm_MaximumProbability": 0,
              "cm_SumEntropy": 0,
              "cm_SumSquares": 0,
              "dm_DependenceEntropy": 0,
              "dm_DependenceNonUniformity": 0,
              "dm_DependenceNonUniformityNormalized": 0,
              "dm_DependenceVariance": 0,
              "dm_GrayLevelNonUniformity": 0,
              "dm_GrayLevelVariance": 0,
              "dm_HighGrayLevelEmphasis": 0,
              "dm_LargeDependenceEmphasis": 0,
              "dm_LargeDependenceHighGrayLevelEmphasis": 0,
              "dm_LargeDependenceLowGrayLevelEmphasis": 0,
              "dm_LowGrayLevelEmphasis": 0,
              "dm_SmallDependenceEmphasis": 0,
              "dm_SmallDependenceHighGrayLevelEmphasis": 0,
              "dm_SmallDependenceLowGrayLevelEmphasis": 0,
              "firstorder_10Percentile": 0,
              "firstorder_90Percentile": 0,
              "firstorder_Energy": 0,
              "firstorder_Entropy": 0,
              "firstorder_InterquartileRange": 0,
              "firstorder_Kurtosis": 0,
              "firstorder_Maximum": 0,
              "firstorder_Me": 0,
              "firstorder_MeanAbsoluteDeviat": 0,
              "firstorder_Med": 0,
              "firstorder_Minimum": 0,
              "firstorder_Range": 0,
              "firstorder_RobustMeanAbsoluteDeviat": 0,
              "firstorder_RootMeanSquared": 0,
              "firstorder_Skewness": 0,
              "firstorder_TotalEnergy": 0,
              "firstorder_Uniformity": 0,
              "firstorder_Variance": 0,
              "m_GrayLevelNonUniformity": 0,
              "m_GrayLevelNonUniformityNormalized": 0,
              "m_GrayLevelVariance": 0,
              "m_HighGrayLevelRunEmphasis": 0,
              "m_LongRunEmphasis": 0,
              "m_LongRunHighGrayLevelEmphasis": 0,
              "m_LongRunLowGrayLevelEmphasis": 0,
              "m_LowGrayLevelRunEmphasis": 0,
              "m_RunEntropy": 0,
              "m_RunLengthNonUniformity": 0,
              "m_RunLengthNonUniformityNormalized": 0,
              "m_RunPercentage": 0,
              "m_RunVariance": 0,
              "m_ShortRunEmphasis": 0,
              "m_ShortRunHighGrayLevelEmphasis": 0,
              "m_ShortRunLowGrayLevelEmphasis": 0,
              "shape_Elongat": 0,
              "shape_Flatness": 0,
              "shape_LeastAxisLength": 0,
              "shape_MajorAxisLength": 0,
              "shape_Maximum2DDiameterColum": 0,
              "shape_Maximum2DDiameterRow": 0,
              "shape_Maximum2DDiameterSlice": 0,
              "shape_Maximum3DDiamete": 0,
              "shape_MeshVolume": 0,
              "shape_MinorAxisLength": 0,
              "shape_Sphericity": 0,
              "shape_SurfaceAre": 0,
              "shape_SurfaceVolumeRat": 0,
              "shape_VoxelVolume": 0,
              "szm_GrayLevelNonUniformity": 0,
              "szm_GrayLevelNonUniformityNormalized": 0,
              "szm_GrayLevelVariance": 0,
              "szm_HighGrayLevelZoneEmphasis": 0,
              "szm_LargeAreaEmphasis": 0,
              "szm_LargeAreaHighGrayLevelEmphasis": 0,
              "szm_LargeAreaLowGrayLevelEmphasis": 0,
              "szm_LowGrayLevelZoneEmphasis": 0,
              "szm_SizeZoneNonUniformity": 0,
              "szm_SizeZoneNonUniformityNormalized": 0,
              "szm_SmallAreaEmphasis": 0,
              "szm_SmallAreaHighGrayLevelEmphasis": 0,
              "szm_SmallAreaLowGrayLevelEmphasis": 0,
              "szm_ZoneEntropy": 0,
              "szm_ZonePercentage": 0,
              "szm_ZoneVariance": 0,
              "x_center": 371,
              "y_center": 220,
              "z_center": 27
            },
          ],
          "seriesID": "1.3.12.2.1107.5.1.4.66045.30000017120400080334400036582",
          "studyUID": "1.2.840.78.75.7.5.3905862.1512362722",
          "taskID": "e3020b5e-f706-11e8-aafd-0242c0a8e004"
        }
      ],
      "message": "OK",
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "data": {},
      "message": "INVALID PASSWORD",
      "statusCode": 1103
    }
    """
    res = response()

    params = request.args.to_dict() if request.method == 'GET' else request.json

    if not all(key in params for key in required_key_all):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    taskID = params['taskID']
    seriesID = params['seriesID']
    if CFDA_TAG:
        rad_csv = os.path.join(CFDA_ROOT, "{}.csv".format(seriesID))
        if not os.path.exists(rad_csv):
            radiomics_list = [{"data_list": [], "headers": []}]
        else:
            with open(rad_csv, "r") as reader:
                v_reader = csv.reader(reader, delimiter=",")
                row_data = list(v_reader)

            headers = row_data[0]
            headers[0] = "id"
            bodys = row_data[1:]
            new_bodys = []
            for body in bodys:
                new_body = {}
                for index, value in enumerate(body):
                    new_body[headers[index]] = value
                new_bodys.append(new_body)
            radiomics_list = [{"data_list": new_bodys, "headers": headers}]
    else:
        headers = [
            "nodule", "x_center", "y_center", "z_center", "cm_Autocorrelat", "cm_ClusterProminence",
            "cm_ClusterShade", "cm_ClusterTendency", "cm_Contrast", "cm_Correlat",
            "cm_DifferenceAverage", "cm_DifferenceEntropy", "cm_DifferenceVariance", "cm_Id",
            "cm_Idm", "cm_Imc1", "cm_Imc2", "cm_InverseVariance", "cm_JointAverage",
            "cm_JointEnergy",
            "cm_JointEntropy", "cm_MaximumProbability", "cm_SumEntropy", "cm_SumSquares",
            "dm_DependenceEntropy", "dm_DependenceNonUniformity",
            "dm_DependenceNonUniformityNormalized", "dm_DependenceVariance",
            "dm_GrayLevelNonUniformity", "dm_GrayLevelVariance", "dm_HighGrayLevelEmphasis",
            "dm_LargeDependenceEmphasis", "dm_LargeDependenceHighGrayLevelEmphasis",
            "dm_LargeDependenceLowGrayLevelEmphasis", "dm_LowGrayLevelEmphasis",
            "dm_SmallDependenceEmphasis", "dm_SmallDependenceHighGrayLevelEmphasis",
            "dm_SmallDependenceLowGrayLevelEmphasis", "firstorder_10Percentile",
            "firstorder_90Percentile", "firstorder_Energy", "firstorder_Entropy",
            "firstorder_InterquartileRange", "firstorder_Kurtosis",
            "firstorder_Maximum", "firstorder_Me",
            "firstorder_MeanAbsoluteDeviat", "firstorder_Med",
            "firstorder_Minimum", "firstorder_Range",
            "firstorder_RobustMeanAbsoluteDeviat", "firstorder_RootMeanSquared",
            "firstorder_Skewness", "firstorder_TotalEnergy", "firstorder_Uniformity",
            "firstorder_Variance", "m_GrayLevelNonUniformity", "m_GrayLevelNonUniformityNormalized",
            "m_GrayLevelVariance", "m_HighGrayLevelRunEmphasis", "m_LongRunEmphasis",
            "m_LongRunHighGrayLevelEmphasis"]
        radiomics_list = db_find_one("radiomics", {"taskID": taskID, 'seriesID': seriesID})
        if radiomics_list is None:
            radiomics_list = [{"data_list": [], "headers": headers}]
        else:
            radiomics_list["headers"] = headers
            for index, radiomics in enumerate(radiomics_list["data_list"]):
                radiomics["nodule"] = index + 1
            radiomics_list = [radiomics_list]

    res.statusCode = 1000
    res.data = radiomics_list
    return make_response(jsonify(res.to_dict()), 200)


if __name__ == "__main__":
    image_path = ""
    label_list = []
    calRadiomics(image_path, label_list)
