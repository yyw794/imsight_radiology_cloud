import React, { Component } from 'react';
import './ToolBar.less'
import {Divider, Icon} from 'antd'
import {withRouter} from 'react-router'
import LayoutChooser from './LayoutChooser';
import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'


const Logo = withRouter(
  ({ history }) => (
    <div className="logo">
      <img className='logo-image' src={require('../../../static/images/logo_corner.png')} onClick={() => history.push('/')}/>
    </div>
  )
)

const ImageButton = (props) => {
  let style = { width: props.width, height: props.height }
  if (props.active) style.backgroundColor = '#1890ff'
  return (
    <div className='image-button'
      style={style}
      onClick={props.onClick}>
      {         typeof(props.bgImg) === 'string' ? <img style={{position: 'absolute', zIndex: "999999"}} src={props.bgImg}/> : props.bgImg }
      { props.text.constructor.name === 'String' ? <span>{props.text}</span>                                                 : props.text  }
    </div>
  )
}

const LayoutButton = (props) => {
  let styleBtn = {
    width: props.width,
    height: props.height,
    backgroundImage: `url(${props.bgImg})`,
  }
  let imgHolderStyle = {
    width: props.width,
    height: props.height,
  }
  let styleChooser = {
    position: 'absolute'
  }
  return (
      <div
      style={styleBtn}
      className="btn-group image-button">
          <button id="layout" type="button"
                  className="btn btn-sm btn-default dropdown-toggle"
                  data-container="body" data-toggle="dropdown"
                  aria-expanded="false" data-placement="right" title="Layout" rel="tooltip"
                  onClick={props.clickHelper}
                  style={{backgroundColor: 'rgba(52, 52, 52, 0.0)',
                  border: '0px solid black'
                }}>
              <span style={imgHolderStyle}></span>
              <span className="fa fa-th-large" style={{color: "#ababab"}}>{props.text}</span>
          </button>
          <LayoutChooser
            style={styleChooser}
            isDisplayNone={props.isActive}
            onLayoutSelected={props.onLayoutSelected}
          />
      </div>
  )
}

class ToolBar extends Component {
  constructor(props) {
    super(props)
    
  }
  state = {
    layoutButtonActive: false
  }
  onLayoutClickHandler = () => {
    const isLayoutButtonActive = this.state.layoutButtonActive;
    this.setState({layoutButtonActive: isLayoutButtonActive ? false : true});
  }
  render() {
    return (
      <div className='viewer-toolbar'>
        <Logo/>
        <div className='tool-button-wrapper'>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/contrast.png')}
            active={this.props.activeTool.includes('wwwc')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.wwwc")}
            onClick={() => this.props.onToolClick('wwwc')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/inverse.png')}
            active={this.props.activeTool.includes('invert')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.invert")}
            onClick={() => this.props.onToolClick('invert')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/refresh.png')}
            active={this.props.activeTool.includes('recover')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.recover")}
            onClick={() => this.props.onToolClick('recover')}/>
          <Divider type="vertical" />
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/ruler.png')}
            active={this.props.activeTool.includes('length')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.length")}
            onClick={() => this.props.onToolClick('length')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/protractor.png')}
            active={this.props.activeTool.includes('angle')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.angle")}
            onClick={() => this.props.onToolClick('angle')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/polygon.png')}
            active={this.props.activeTool.includes('freehand')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.freehand")}
            onClick={() => this.props.onToolClick('freehand')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/reference_lines.png')}
            active={this.props.activeTool.includes('referenceLines')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.referenceLines")}
            onClick={() => this.props.onToolClick('referenceLines')}/>
          {
            this.props.forbidEdit ? '' :
            <Divider type="vertical" />
          }
          {
            this.props.forbidEdit ? '' :
            <LayoutButton
            width={64} height={64}
            bgImg={require('../../../static/images/layout.png')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.layout")}
            clickHelper={this.onLayoutClickHandler}
            isActive={this.state.layoutButtonActive}
            onLayoutSelected={(cols, rows) => {
              this.props.onLayoutSelected(cols,rows)
              this.setState({layoutButtonActive: false})
            }}/>
          }
          <ImageButton
            width={64} height={64}
            bgImg={<Icon type="delete" />}
            text={GetLocaledText(this, "LiverViewer.ToolBar.clear")}
            onClick={() => this.props.onToolClick('clear')}/>
          <ImageButton
            width={64} height={64}
            bgImg={<Icon type="link" />}
            active={this.props.activeTool.includes('viewerSync')}
            text={GetLocaledText(this, "LiverViewer.ToolBar.chain")}
            onClick={() => this.props.onToolClick('viewerSync')}/>
        </div>
      </div>
    );
  }

}

export default injectIntl(ToolBar);
