#!/bin/python
import sys
import os
import hashlib
from flask import make_response, jsonify, request, abort,session
from flask_login import login_user, logout_user, current_user

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one
from models.response.response import response
from models.user.User import User, parseUserInfo, validateUserInfo

def load_user(user_id):
    user_db = db_find_one("user", {"account": user_id})
    user_info = validateUserInfo(user_db)
    user = None if user_info is None else parseUserInfo(user_info)
    return user

def login():
    res = response()

    if not request.json:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    user_info = validateUserInfo(request.json)
    if user_info is None:
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    user = load_user(user_info['account'])
    if user is None:
        res.statusCode = 1104
        return make_response(jsonify(res.to_dict()), 200)

    hashed_pwd = hashlib.sha256(user_info['password']).hexdigest()
    if user.password != hashed_pwd:
        res.statusCode = 1103
        return make_response(jsonify(res.to_dict()), 200)
    
    session.permanent=True
    login_user(user)
    res.data = user.to_dict()
    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)

def logout():
    if not current_user.is_authenticated:
        abort(401)

    res = response()
    user_id = current_user.get_id()

    logout_user()
    res.data = {}
    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)
