#!/bin/python
# coding=utf-8
from Label import Label


class DX_Label(Label):
    def __init__(self):
        Label.__init__(self)
        self.x = 0
        self.y = 0
        self.w = 0
        self.h = 0
        self.prob = 0

    def to_dict(self):
        ret = {
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "instanceID": self.instanceID,
            "globalTag": self.globalTag,
            "diseaseTag": self.diseaseTag,
            "state": self.state,
            "prob": self.prob
        }
        if self.globalTag:
            return ret
        else:
            ret['coord'] = {"x": self.x, "y": self.y}
            ret['width'] = self.w
            ret['height'] = self.h
            return ret

    def to_db(self):
        ret = {
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "instanceID": self.instanceID,
            "globalTag": self.globalTag,
            "diseaseTag": self.diseaseTag,
            "state": self.state,
            "prob": self.prob
        }
        if self.globalTag:
            return ret
        else:
            ret['x'] = self.x
            ret['y'] = self.y
            ret['w'] = self.w
            ret['h'] = self.h
            return ret


required_key_label = ["labelID", "seriesID", "instanceID", "globalTag", "diseaseTag", "state", 'prob']
optional_key = ["x", "y", "w", "h"]


def parseDXLabelist(labels):
    labelist = []
    for label in labels:
        if not all(key in label for key in required_key_label):
            raise ValueError(
                "DX label must contain {}, while input contains {}".format(required_key_label, label.keys()))

        new_DXlabel = DX_Label()
        new_DXlabel.labelID = label['labelID']
        new_DXlabel.seriesID = label['seriesID']
        new_DXlabel.instanceID = label['instanceID']
        new_DXlabel.globalTag = label['globalTag']
        new_DXlabel.diseaseTag = label['diseaseTag']
        new_DXlabel.state = label['state']
        new_DXlabel.prob = label['prob']
        if not label['globalTag']:
            if not all(key in label for key in optional_key):
                raise ValueError(
                    "non global DX label must contain {}, while input contatins {}".format(optional_key, label.keys()))
            else:
                new_DXlabel.x = label['x']
                new_DXlabel.y = label['y']
                new_DXlabel.w = label['w']
                new_DXlabel.h = label['h']

        labelist.append(new_DXlabel)

    return labelist
