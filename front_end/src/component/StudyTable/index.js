import React, {Component} from 'react'
import {Table, Divider, Popover} from 'antd';
import {withRouter} from 'react-router'

import {injectIntl, FormattedMessage} from 'react-intl'
import {GetLocaledText} from '../../localedFuncs'

import './index.less'

/**
 * 性别对象集合
 * @private {Object}
 */
const GENDER_MAP = {
    MALE: <FormattedMessage id="StudyPage.gender.male"/>,
    FEMALE: <FormattedMessage id="StudyPage.gender.female"/>
}

/**
 * 检测状态对象集合
 * @private {Object}
 */
const STATUS_MAP = {
    ABNORMAL: <FormattedMessage id="StudyPage.diagnosis-result.abnormal"/>,
    NORMAL: <FormattedMessage id="StudyPage.diagnosis-result.normal"/>
}

/**
 * 阅片视图类别集合
 * @private {{CT: string, DR: string, DX: string, CR: string}}
 */
const VIEW_MODALITY_URL = {
    CT: 'view_ct',
    MR: 'view_ct',
    DR: 'view_dr',
    DX: 'view_dr',
    CR: 'view_dr'
}

/**
 * 打开新窗口的list
 * @type {Map<any, any>}
 */
let openedWindows = new Map()

/**
 * 页面跳转 不打开新页面
 * @param type 页面类型
 * @param studyUID
 * @param modality
 * @param patientID
 * @param studyUIDs
 * @returns {*}
 */
function openWindow(type, studyUID, modality = "CT", patientID = "", studyUIDs = []) {
    let top = window.screen.availTop
    let left = window.screen.availLeft
    let height = window.screen.availHeight
    let width = window.screen.availWidth
    //delete null windows
    let nullWin = []
    openedWindows.forEach(function (aWin, key) {
        if (!aWin) {
            nullWin.push(key)
        }
    })
    nullWin.map(w => openedWindows.delete(w))

    //close window of other type(if any) if studyUID is not the same as last opened
    let otherType = type === "image" ? "report" : "image"
    if (openedWindows.has(otherType)) {
        let otherWin = openedWindows.get(otherType)
        if (!otherWin.closed) {
            if (otherWin.location.href.replace(/-/g, '.').indexOf(studyUID) < 0 && otherWin.location.href !== "about:blank") {
                otherWin.close()
                openedWindows.delete(otherType)
            }
        }
    }

    if (openedWindows.has(type)) {

        let win = openedWindows.get(type)
        if (win.closed) {
            openedWindows.delete(type)
        } else {
            if (win.location.href.replace(/-/g, '.').indexOf(studyUID) < 0 ||
                (studyUIDs.length > 1 && win.location.href.replace(/-/g, '.').indexOf(studyUIDs[0]) < 0) ||
                (studyUIDs.length > 1 && win.location.href.replace(/-/g, '.').indexOf(studyUIDs[1]) < 0)) {
                top = win.screenTop
                left = win.screenLeft
                height = win.outerHeight
                width = win.outerWidth
                win.close()
                openedWindows.delete(type)
            } else {
                win.focus()
                return true
            }
        }
    }
    if (type === "image") {
        if (studyUIDs.length > 1) {
            let ids = studyUIDs.reverse().join(',').replace(/\./g, '-')
            openedWindows.set(type, window.location.href = `/view_ct_follow_up/${patientID}/${ids}`
                // `'image',${ids}`,
                // `location=yes,top=${top},left=${left},height=${height},width=${width},`
            )
            // openedWindows.set(type, window.open(
            //   `/view_ct_follow_up/${patientID}/${ids}`,
            //   `'image',${ids}`,
            //   `location=yes,top=${top},left=${left},height=${height},width=${width},`
            // ))
            studyUIDs.reverse()
        } else {
            openedWindows.set(type, window.location.href = `/${VIEW_MODALITY_URL[modality]}/${studyUID.replace(/\./g, '-')}`)
            // openedWindows.set(type, window.open(
            //   `/${VIEW_MODALITY_URL[modality]}/${studyUID.replace(/\./g, '-')}`,
            //   `'image',${studyUID}`,
            //   `location=yes,top=${top},left=${left},height=${height},width=${width},`
            // ))
        }
    } else if (type === "report") {
        openedWindows.set(type, window.location.href =
                `/report/${studyUID.replace(/\./g, '-')}`,
            `'report',${studyUID}`,
            `location=yes,top=${top},left=${left},height=${height},width=${width},`
        )

    }
    return true
}

/**
 * 打开新窗口
 * @param type 页面类型
 * @param studyUID
 * @param modality
 * @param patientID
 * @param studyUIDs
 * @returns {*}
 */
function openWindowBlank(type, studyUID, modality = "CT", patientID = "", studyUIDs = []) {
    let top = window.screen.availTop
    let left = window.screen.availLeft
    let height = window.screen.availHeight
    let width = window.screen.availWidth
    //delete null windows
    let nullWin = []
    openedWindows.forEach(function (aWin, key) {
        if (!aWin) {
            nullWin.push(key)
        }
    })
    nullWin.map(w => openedWindows.delete(w))

    //close window of other type(if any) if studyUID is not the same as last opened
    let otherType = type === "image" ? "report" : "image"
    if (openedWindows.has(otherType)) {
        let otherWin = openedWindows.get(otherType)
        if (!otherWin.closed) {
            if (otherWin.location.href.replace(/-/g, '.').indexOf(studyUID) < 0 && otherWin.location.href !== "about:blank") {
                otherWin.close()
                openedWindows.delete(otherType)
            }
        }
    }

    if (openedWindows.has(type)) {

        let win = openedWindows.get(type)
        if (win.closed) {
            openedWindows.delete(type)
        } else {
            if (win.location.href.replace(/-/g, '.').indexOf(studyUID) < 0 ||
                (studyUIDs.length > 1 && win.location.href.replace(/-/g, '.').indexOf(studyUIDs[0]) < 0) ||
                (studyUIDs.length > 1 && win.location.href.replace(/-/g, '.').indexOf(studyUIDs[1]) < 0)) {
                top = win.screenTop
                left = win.screenLeft
                height = win.outerHeight
                width = win.outerWidth
                win.close()
                openedWindows.delete(type)
            } else {
                win.focus()
                return true
            }
        }
    }
    if (type === "image") {
        if (studyUIDs.length > 1) {
            let ids = studyUIDs.reverse().join(',').replace(/\./g, '-')
            // openedWindows.set(type, window.location.href=`/view_ct_follow_up/${patientID}/${ids}`
            //     // `'image',${ids}`,
            //     // `location=yes,top=${top},left=${left},height=${height},width=${width},`
            // )
            openedWindows.set(type, window.open(
                `/view_ct_follow_up/${patientID}/${ids}`,
                `'image',${ids}`,
                `location=yes,top=${top},left=${left},height=${height},width=${width},`
            ))
            studyUIDs.reverse()
        } else {
            // openedWindows.set(type, window.location.href= `/${VIEW_MODALITY_URL[modality]}/${studyUID.replace(/\./g, '-')}`)
            openedWindows.set(type, window.open(
                `/${VIEW_MODALITY_URL[modality]}/${studyUID.replace(/\./g, '-')}`,
                `'image',${studyUID}`,
                `location=yes,top=${top},left=${left},height=${height},width=${width},`
            ))
        }
    } else if (type === "report") {
        openedWindows.set(type, window.open(
            `/report/${studyUID.replace(/\./g, '-')}`,
            `'report',${studyUID}`,
            `location=yes,top=${top},left=${left},height=${height},width=${width},`
        ))

    }
    return true
}

/**
 * 打开新的tab页面
 * @param type  页面类型
 * @param studyUID
 * @param modality
 * @param patientID
 * @param studyUIDs
 * @returns {boolean}
 */
function openWindowBlankTab(type, studyUID, modality = "CT", patientID = "", studyUIDs = []) {
    let top = window.screen.availTop
    let left = window.screen.availLeft
    let height = window.screen.availHeight
    let width = window.screen.availWidth
    //delete null windows
    let nullWin = []
    openedWindows.forEach(function (aWin, key) {
        if (!aWin) {
            nullWin.push(key)
        }
    })
    nullWin.map(w => openedWindows.delete(w))

    //close window of other type(if any) if studyUID is not the same as last opened
    let otherType = type === "image" ? "report" : "image"
    if (openedWindows.has(otherType)) {
        let otherWin = openedWindows.get(otherType)
        if (!otherWin.closed) {
            if (otherWin.location.href.replace(/-/g, '.').indexOf(studyUID) < 0 && otherWin.location.href !== "about:blank") {
                otherWin.close()
                openedWindows.delete(otherType)
            }
        }
    }

    if (openedWindows.has(type)) {

        let win = openedWindows.get(type)
        if (win.closed) {
            openedWindows.delete(type)
        } else {
            if (win.location.href.replace(/-/g, '.').indexOf(studyUID) < 0 ||
                (studyUIDs.length > 1 && win.location.href.replace(/-/g, '.').indexOf(studyUIDs[0]) < 0) ||
                (studyUIDs.length > 1 && win.location.href.replace(/-/g, '.').indexOf(studyUIDs[1]) < 0)) {
                top = win.screenTop
                left = win.screenLeft
                height = win.outerHeight
                width = win.outerWidth
                win.close()
                openedWindows.delete(type)
            } else {
                win.focus()
                return true
            }
        }
    }
    if (type === "image") {
        if (studyUIDs.length > 1) {
            let ids = studyUIDs.reverse().join(',').replace(/\./g, '-')
            // openedWindows.set(type, window.location.href=`/view_ct_follow_up/${patientID}/${ids}`
            //     // `'image',${ids}`,
            //     // `location=yes,top=${top},left=${left},height=${height},width=${width},`
            // )
            openedWindows.set(type, window.open(
                `/view_ct_follow_up/${patientID}/${ids}`,
                `'image',${ids}`,
                `location=yes,top=${top},left=${left},height=${height},width=${width},`
            ))
            studyUIDs.reverse()
        } else {
            // openedWindows.set(type, window.location.href= `/${VIEW_MODALITY_URL[modality]}/${studyUID.replace(/\./g, '-')}`)
            openedWindows.set(type, window.open(
                `/${VIEW_MODALITY_URL[modality]}/${studyUID.replace(/\./g, '-')}`,
                `'image',${studyUID}`,
                `location=yes,top=${top},left=${left},height=${height},width=${width},`
            ))
        }
    } else if (type === "report") {
        openedWindows.set(type, window.open(
            `/report/${studyUID.replace(/\./g, '-')}`
        ))

    }
    return true
}

/**
 * 用来判断报告按钮是否禁用
 * @param study
 * @returns {boolean}
 */
function canOpenReport(study) {
    if (study.state !== 'completed' || study.state === "png_unsupport") {
        return false
    }
    if (study.user_privilege.includes('reviewer') && !study.submitted) {
        return false
    }
    return true
}

/**
 * 阅片按钮是否禁用
 * @param study
 * @returns {boolean}
 */
function canOpenViewer(study) {
    if (study.state === 'meta_only' || study.state === "pull_data" || study.state === "modality_unsupport" || study.state === "png_unsupport") {
        return false
    }
    return true
}

/**
 *检测按钮是否禁用
 * @param study
 * @returns {boolean}
 */
function canRunDetection(study) {
    if (study.state === "meta_only" || study.state === "completed" || study.state === "algo_unsupport") {
        return true
    }
    return false
}

/**
 * 检测结果
 * @param record rows 数据
 * @returns {string}
 */
function detectionResult(record) {
    if (record.state === 'meta_only') {
        return <FormattedMessage id='StudyPage.state.meta_only'/>
    }
    if (record.state === 'pull_data') {
        return <FormattedMessage id='StudyPage.state.pull_data'/>
    }
    if (record.state === 'file_only') {
        return <FormattedMessage id='StudyPage.state.file_only'/>
    }
    if (record.state === 'algo_unsupport') {
        return <FormattedMessage id='StudyPage.state.algo_unsupport'/>
    }
    if (record.state === 'png_unsupport') {
        return <FormattedMessage id='StudyPage.state.png_unsupport'/>
    }
    if (record.state === 'modality_unsupport') {
        return <FormattedMessage id='StudyPage.state.modality_unsupport'/>
    }
    if (record.state === 'completed') {

        if (!!record.labelType) {
            if (record.labelType === "LiverTumor" && record.labelNum > 0) {
                  switch (record.labelNum) {
                     case 1:
                         return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.tumor"/></span>
                         break;
                     default:
                         return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.tumor"/></span>
                   }
            } else if (record.labelType === "Nodule" && record.labelNum > 0) {
                switch (record.labelNum) {
                    case 1:
                        return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.nodule"/></span>
                        break;
                   default:
                         return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.nodules"/></span>
                }
            }
        }
        // if (record.modality === 'CT' && record.bodyPartExamined === 'ABDOMEN' && record.labelNum > 0) {
         if (record.modality !== 'CT'&&record.result == 'ABNORMAL') {
            return <span><FormattedMessage id='StudyPage.diagnosis-result.abnormal'/></span>
         } else if (record.result == 'NORMAL') {
            // if (record.modality === 'CT' && record.bodyPartExamined === 'CHEST') {
            if (record.modality === 'CT') {
                return <span><FormattedMessage id='StudyPage.diagnosis-result.normal.chestNodule'/></span>
            }
             return <span><FormattedMessage id='StudyPage.diagnosis-result.normal'/></span>
            } else {
             if (record.modality === 'CT'&&record.result == 'ABNORMAL') {
                 return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.nodule"/></span>
             }
        }
    }
}

/**
 * 列表组件
 */
class StudyTable extends Component {
    /**
     * 生命周期构造函数
     * @param props
     */
    constructor(props) {
        super(props)
        /**
         *
         * @type {{
         * visible: boolean,
         * recordDetail: string,列表单条数据
         * patientID: string
         * }}
         */
        this.state = {
            visible: false,
            recordDetail: "",
            patientID: "",
        };
        /**
         * 控制是否可以双击
         * @type {boolean}
         */
        this.isDoubleClick = false
        /**
         * 控制是否可以点击
         * @type {boolean}
         */
        this.onClickWaiting = false
        this.hideTooltip = this.hideTooltip.bind(this);

    }

    /**
     * 参照react生命周期函数
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        this.setState({})
    }

    /**
     * 隐藏右键打开新页面
     * @param e
     */
    hideTooltip(e) {
        this.setState({
            patientID: "",
        })
    }

    /**
     * 控制阅片右键显示table list 单条打开新页面按钮
     * @param e
     */
    handleVisibleChange(e) {
        console.log(e)
        this.setState({
            openWindowType: e.target.title,
            patientID: e.target.id,
            recordDetail: JSON.parse(e.target.name),
        })
        e.preventDefault();
    }

    /**
     *
     * 控制报告右键显示打开新页面按钮
     * @param e
     */
    handleVisibleChange1(e) {
        this.setState({
            openWindowType: e.target.title,
            indexId: e.target.id,
            recordDetail: JSON.parse(e.target.name),
        })
        e.preventDefault();
    }

    /**
     * 返回table list 组件
     * @return { Component}
     */
    render() {
        const content = (
            <div>
                <a style={{textDecoration: "none"}} href="javaScript:void(0)" onClick={(e) => {
                    e.stopPropagation()
                    this.hideTooltip()
                    localStorage.setItem("fellowParams", JSON.stringify(this.state.recordDetail))
                    openWindowBlank(this.state.openWindowType, this.state.recordDetail.studyUID, this.state.recordDetail.modality)
                }}><FormattedMessage id="StudyPage.OpenPage"/></a>
            </div>
        );
        const columns = [
            {
                title: " ",
                // render: text => <a href="javascript:;">{text}</a>,
                render: (text, record) => {
                    return record.history ? record.history.length >= 1 ? <span style={{
                        border: "1px solid #2fa8e6",
                        color: " #2fa8e6",
                        padding: "2px 5px"
                    }}>{record.history.length}</span> : "" : ""
                },
                width: 20,
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.patientID"),
                dataIndex: 'patientID',
                key: 'patientID',
                // render: text => <a href="javascript:;">{text}</a>,
                width: 90,
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.patient-name"),
                dataIndex: 'patientName',
                key: 'patientName',
                width: 160,
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.gender"),
                dataIndex: 'gender',
                key: 'gender',
                width: 80,
                render: text => {
                    if (text == 'MALE') {
                        return <span><FormattedMessage id='StudyPage.gender.male'/></span>
                    } else if (text == 'FEMALE') {
                        return <span><FormattedMessage id='StudyPage.gender.female'/></span>
                    } else {
                        return <span>ErrorText</span>
                    }
                    //<span>{GENDER_MAP[text]}</span>
                },
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.birthday"),
                dataIndex: 'birthDate',
                key: 'birthDate',
                width: 120,
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.modality"),
                dataIndex: 'modality',
                key: 'modality',
                width: 110,
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.body-part"),
                dataIndex: 'bodyPartExamined',
                key: 'bodyPartExamined',
                width: 110,
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.study-time"),
                dataIndex: 'studyDate',
                key: 'studyDate',
                sorter: (a, b) => {
                    if (a.studyDate + a.studyTime < b.studyDate + b.studyTime) {
                        return -1;
                    }
                    if (a.studyDate + a.studyTime > b.studyDate + b.studyTime) {
                        return 1;
                    }
                    if (a.studyDate + a.studyTime === b.studyDate + b.studyTime) {
                        return 0;
                    }
                },
                render: (text, record) => {
                    return record.studyDate + ' ' + record.studyTime
                },
                width: 240,
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.diagnosis-result"),
                dataIndex: 'result',
                key: 'result',
                width: 110,
                render: (text, record) => {
                    return detectionResult(record)
                },
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.diagnosis-advice"),
                dataIndex: 'diagnosisAdvice',
                key: 'diagnosisAdvice',
            },
            {
                title: GetLocaledText(this, "DRViewer.StudyTable.action"),
                key: 'action',
                width: 250,
                render: (text, record, index) => (
                    <span>
              <Popover content={content} placement="bottom" trigger="click" title=""
                       visible={(record.studyUID == this.state.patientID) && canOpenViewer(record)}>
                <a
                    disabled={!canOpenViewer(record)}
                    id={record.studyUID}
                    title={"image"}
                    name={JSON.stringify(record)}
                    onContextMenu={this.handleVisibleChange.bind(this)}
                    onClick={e => {
                        e.stopPropagation()
                        localStorage.setItem("fellowParams", JSON.stringify(record))
                        canOpenViewer(record) && openWindow("image", record.studyUID, record.modality)
                    }}>
                  {GetLocaledText(this, "DRViewer.StudyTable.view-image")}
                </a>
              </Popover>
            <Divider type="vertical"/>
            <Popover content={content} placement="bottom" trigger="click" title=""
                     visible={this.state.patientID ? (record.patientID == this.state.patientID) && canOpenReport(record) : false}>
                  <a
                      id={record.patientID}
                      title={"report"}
                      name={JSON.stringify(record)}
                      onContextMenu={this.handleVisibleChange.bind(this)}
                      onClick={(e) => {
                          e.stopPropagation()
                          canOpenReport(record) && openWindowBlank("report", record.studyUID, record.modality)
                          canOpenViewer(record) && openWindow("image", record.studyUID, record.modality)
                      }}
                      disabled={!canOpenReport(record)}>
                    {GetLocaledText(this, "DRViewer.StudyTable.report")}
                  </a>
            </Popover>
                        {/*{*/}
                        {/*this.props.showDetectionOption ?*/}
                        {/*<Divider type="vertical"/>:''*/}
                        {/* }*/}
                        {/*{*/}
                        {/*this.props.showDetectionOption ?*/}
                        {/*<a*/}
                        {/*onClick={(e)=>{*/}
                        {/*e.stopPropagation()*/}
                        {/*this.props.runDetection(record)*/}
                        {/* }}*/}
                        {/* disabled={!canRunDetection(record)}>*/}
                        {/*{GetLocaledText(this,"StudyTable.detection")}*/}
                        {/*</a>:''*/}
                        {/*}*/}
                        {/*<Divider type="vertical"/>*/}
                        {/*<a*/}
                        {/*onClick={(e)=>{*/}
                        {/*e.stopPropagation()*/}
                        {/*this.props.deleteStudyListpage(record)*/}
                        {/*}}*/}
                        {/*>*/}
                        {/*{GetLocaledText(this,"StudyTable.delete")}*/}
                        {/*</a>*/}
          </span>
                ),
            }
        ]
        return (
            <div onClick={this.hideTooltip}>
                <Table
                    columns={columns}
                    className='study-table'
                    onRow={(record) => {
                        return {
                            onDoubleClick: () => {
                                this.isDoubleClick = true
                                canOpenReport(record) && openWindow("report", record.studyUID, record.modality)
                                canOpenViewer(record) && openWindow("image", record.studyUID, record.modality)
                            },
                            onClick: () => {
                                if (this.onClickWaiting) {
                                    return
                                }
                                this.onClickWaiting = true
                                setTimeout(() => {
                                    if (this.isDoubleClick) {
                                        this.isDoubleClick = false
                                        this.onClickWaiting = false
                                        return
                                    }
                                    localStorage.setItem("fellowParams", JSON.stringify(record))
                                    this.onClickWaiting = false
                                    canOpenViewer(record) && openWindow("image", record.studyUID, record.modality)
                                }, 500)
                            }
                        };
                    }}
                    {...this.props}/>
            </div>
        )
    }
}

export {
    GENDER_MAP,
    STATUS_MAP,
    VIEW_MODALITY_URL,
    openWindow,
    canOpenReport,
    detectionResult,
    openWindowBlank,
    openWindowBlankTab
}
export default injectIntl(withRouter(StudyTable))
