import {
  external,
  EVENTS,
  // mouseButtonTool,
  touchTool,
  drawTextBox,
  toolStyle,
  textStyle,
  toolColors,
  drawHandles,
  getToolState,
  getToolOptions,
  setToolOptions,
  isMouseButtonEnabled
} from 'cornerstone-tools';
import * as cornerstone from 'cornerstone-core'
import mouseButtonTool from './mouseButtonTool'
import {BOX_SIZE} from './noduleBox'
import _ from 'underscore'

const toolType = 'referenceLine2D';

let configuration = {}
let elemSet = new Set()

function mouseClickCallback(e) {
  const eventData = e.detail;
  const element = eventData.element;
  const {width, height} = eventData.image
  const options = getToolOptions(toolType, element);
  if (isMouseButtonEnabled(eventData.which, options.mouseButtonMask)) {
    const coord = eventData.currentPoints.image
    configuration.intersection = {
      x: coord.x / width,
      y: coord.y / height
    }
    elemSet.forEach(elem => {
      cornerstone.updateImage(elem)
    })
    // _.isFunction(options.onClick) && options.onClick()
  }
}

function onImageRendered(e) {
  const eventData = e.detail;
  const element = eventData.element
  const context = eventData.canvasContext.canvas.getContext('2d')
  const {width, height} = eventData.image
  // const toolData = getToolState(element, toolType);
  // if (!toolData) return
  // let intersection = toolData.data[0].intersection
  if (!configuration.intersection) return
  const intersection = {x: configuration.intersection.x * width, y: configuration.intersection.y * height}
  let line1 = {start: {x: 0, y: intersection.y}, end: {x: width, y: intersection.y}}
  let line2 = {start: {x: intersection.x, y: 0}, end: {x: intersection.x, y: height}}

  // const color = toolColors.getActiveColor();
  // const lineWidth = toolStyle.getToolWidth();
  const color = 'yellow'
  const lineWidth = 1

  if (intersection && !isNaN(intersection.x) && !isNaN(intersection.y)) {
    // context.rect(intersection.x - 10, intersection.y - 10, 20, 20);
    const intersectionCanvas = cornerstone.pixelToCanvas(element, intersection);
    if (line1.start.y !== line1.end.y) {
      let tmp = line1
      line1 = line2
      line2 = tmp
    }
    const start1Canvas = cornerstone.pixelToCanvas(element, line1.start);
    const end1Canvas = cornerstone.pixelToCanvas(element, line1.end);
    const start2Canvas = cornerstone.pixelToCanvas(element, line2.start);
    const end2Canvas = cornerstone.pixelToCanvas(element, line2.end);
    const viewport = cornerstone.getViewport(element)
    const size = BOX_SIZE * viewport.scale
    // Draw the referenceLines
    context.setTransform(1, 0, 0, 1, 0, 0);

    context.save();
    context.beginPath();
    context.strokeStyle = color;
    context.lineWidth = lineWidth;
    context.setLineDash([5, 2])

    context.moveTo(start1Canvas.x, start1Canvas.y);
    // context.lineTo(Math.max(intersectionCanvas.x - size / 2, start1Canvas.x), start1Canvas.y);
    // context.moveTo(intersectionCanvas.x + size / 2, start1Canvas.y);
    context.lineTo(end1Canvas.x, end1Canvas.y);

    context.moveTo(start2Canvas.x, start2Canvas.y);
    // context.lineTo(start2Canvas.x, Math.max(intersectionCanvas.y - size / 2, start2Canvas.y));
    // context.moveTo(start2Canvas.x, intersectionCanvas.y + size / 2);
    context.lineTo(end2Canvas.x, end2Canvas.y);
    context.stroke();

    // context.rect(intersectionCanvas.x - size / 2, intersectionCanvas.y - size / 2, size, size)
    // context.beginPath()
    // context.fillStyle = color
    // context.arc(intersectionCanvas.x, intersectionCanvas.y, 1, 0, 2 * Math.PI, true);
    // context.fill();
    // context.stroke();
    context.restore();
  }
}

export default {
  activate (element, mouseButtonMask) {
    let options = getToolOptions(toolType, element)
    options = Object.assign({}, options, {mouseButtonMask})
    // options.mouseButtonMask = mouseButtonMask;
    setToolOptions(toolType, element, options);

    element.removeEventListener(EVENTS.IMAGE_RENDERED, onImageRendered)
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
    element.addEventListener(EVENTS.IMAGE_RENDERED, onImageRendered)
    element.addEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
    cornerstone.updateImage(element)
  },
  disable (element) {
    element.removeEventListener(EVENTS.IMAGE_RENDERED, onImageRendered)
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
    cornerstone.updateImage(element)
    elemSet.delete(element)
  },
  enable (element, options={}) {
    setToolOptions(toolType, element, options);
    element.addEventListener(EVENTS.IMAGE_RENDERED, onImageRendered)
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
    cornerstone.updateImage(element)
    elemSet.add(element)
  },
  deactivate (element) {
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  getConfiguration () {
    return configuration;
  },
  setConfiguration (config) {
    configuration = config;
  },
}
