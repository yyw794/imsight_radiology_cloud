import React, { Component } from 'react';
import { Table } from 'antd'
import {getRiskText, getRiskColor, getNoduleType, POS_IMG, RISK_EN2CN, TYPE_EN2CN} from '../NodulePanel'
import {injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

/**
 * 肺结节table List
 * @param props
 * @returns {Component}
 * @constructor
 */
const LungNoduleTable = (props) => {
  const columns = [
    {
      title: GetLocaledText({props},"NodulePanel.table-header.number"),/*translate to english GetLocaledText{,""}*/
      dataIndex: 'key',
      width: 100,
      render: (value, record, i) => {
        let text = `${props.keyPrefix ? props.keyPrefix : 'N'}${i+1}`
        if (record.addByUser) {
          text += '*'
        }
        return text
      }
    },
    {
      title: GetLocaledText({props},"NodulePanel.table-header.diam"),/*translate to english GetLocaledText{,""}*/
      dataIndex: 'nodule_diameter',
      width: 100,
      render: (value, record) => {
        if (value === null) {return '-'}
        return value.toFixed(1)
      }
    },
    {
      title: GetLocaledText({props},"NodulePanel.table-header.malignancy"),/*translate to english GetLocaledText{,""}*/
      dataIndex: 'nodule_bm',
      width: 100,
      render: (value, record) => {
        if (value === null) {return ''}
        return <span style={{color: getRiskColor(value)}}>{RISK_EN2CN[getRiskText(value)]}</span>
      }
    },
    {
      title: GetLocaledText({props},"NodulePanel.table-header.subclass"),/*translate to english GetLocaledText{,""}*/
      dataIndex: 'nodule_type',
      width: 100,
      render: (value, record) => {
        if (record.subtTrue === null && record.subt === null) {return ''}
        return TYPE_EN2CN[getNoduleType(record)]
      }
    },
    {
      title: GetLocaledText({props},"NodulePanel.table-header.z"),/*translate to english GetLocaledText{,""}*/
      dataIndex: 'coord.z',
      width: 100,
      render:(value,record)=>{
        return props.zReverse?(props.maxZ - record.coord.z +1 ):record.coord.z
      }
    },
    {
      title: GetLocaledText({props},"NodulePanel.nodule-detail.average-density"),/*translate to english GetLocaledText{,""}*/
      dataIndex: 'nodule_avgHU',
      width: 100,
      render: (value) => value.toFixed(1)
    },
    {
      title: GetLocaledText({props},"NodulePanel.nodule-detail.volume"),/*translate to english GetLocaledText{,""}*/
      dataIndex: 'nodule_volume',
      width: 100,
      render: (value) => value.toFixed(1)
    },
    {
      title: GetLocaledText({props},"NodulePanel.table-header.location"),/*translate to english GetLocaledText{,""}*/
      dataIndex: 'nodule_location',
      render: (value) => {
        if (value === null) {return '-'}
        return <img src={POS_IMG[value]} className='location'/>
      }
    },
  ]
  return (
    <Table className='lung-nodule-table' columns={columns} dataSource={props.data} size="small" scroll={{ y: 150 }} pagination={false}/>
  )
}

export default injectIntl(LungNoduleTable)
