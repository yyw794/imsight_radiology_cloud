import React, { Component } from 'react'
import * as cornerstone from 'cornerstone-core'
import * as cornerstoneWebImageLoader from "cornerstone-web-image-loader"
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import Hammer from "hammerjs"
import {Icon, message, Button, Tabs,Layout,Modal} from 'antd'
const TabPane = Tabs.TabPane
import Immutable from 'immutable'

import ToolBar from '../CTToolBar'
import NodulePanel from '../NodulePanel'
import noduleBox from '../tools/noduleBox'
import NoduleComparePanel from '../NoduleComparePanel'
import referenceLine2D from '../tools/referenceLine2D'
import {zoom} from '../tools/zoom'
// import zoomSynchronizer from '../tools/zoomSynchronizer'
import Synchronizer from '../tools/Synchronizer'
import scrollToIndex from '../tools/scrollToIndex'
import scaleTool from '../tools/scaleTool'
import {deepCopy, dataURItoBlob} from '../../util'
import {getUser} from '../../auth'
import api from '../../api'
import axios from 'axios'
import Indicator from '../Indicator'
import Snapshot from '../Snapshot'
import ThumbnailList from '../ThumbnailList'
import {scaleWW, scaleWC, scaleWWReverse, scaleWCReverse} from '../../util'
import io from 'socket.io-client'
import Viewer from './Viewer'
import { GetLocaledText } from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'

import _ from 'underscore'
// import loadImage from './loadImage'

import './index.less'
import Logo from "../Logo";
import SignoutButton from "../SignoutButton";
import ReturnCT from "../RetunCT";
const { Header, Content, Footer } = Layout;
cornerstoneTools.external.cornerstone = cornerstone
cornerstoneTools.cornerstoneMath = cornerstoneMath
cornerstoneWebImageLoader.external.cornerstone = cornerstone
cornerstoneTools.external.Hammer = Hammer

class FollowUp extends Component {

  constructor(props) {
    super(props)
    let matchKeys1 = this.props.compareNodules.map(pair => pair[0].key)
    let matchKeys2 = this.props.compareNodules.map(pair => pair[1].key)
    this.state = {
        visible:false,
      position1: {x: 0, y: 0, z: 0},
      position2: {x: 0, y: 0, z: 0},
      study1: props.study1,
      study2: props.study2,
      compareNodules: props.compareNodules,
      nodules1: props.labels1.filter(label => !matchKeys1.includes(label.key)),  // only thoese not in compareNodules
      nodules2: props.labels2.filter(label => !matchKeys1.includes(label.key)),
      labels1: props.labels1, // all labels
      labels2: props.labels2,
      activeTool: [],
      viewport1: null,
      viewport2: null,
      HU1: 0,
      HU2: 0,
      fullScreen: null,
      compareData: null,
      activeTabKey: '0',
    }
    this.socket = null
    this.initReferencePos = null
    this.synchronizer = new Synchronizer("cornerstonenewimage", cornerstoneTools.stackImageIndexSynchronizer)
    this.onClickModel=this.onClickModel.bind(this);
    this.handleOkModel=this.handleOkModel.bind(this);
    this.handleCancelModel=this.handleCancelModel.bind(this);
  }

  replaceStudy = async (idx, study) => {
    let study1
    let study2
    if (idx === 1) {
      study1 = study
      study2 = this.state.study2
    } else {
      study1 = this.state.study1
      study2 = study
    }
    // get register nodule list
    let noduleRegister = await api.getNoduleRegister({fixed: study1.studyUID, moving: study2.studyUID})
    let compareNodules = _.zip(noduleRegister.fixed, noduleRegister.moving)
    compareNodules.forEach(pair => {
      pair[0].key = study1.labels.find(label => label.labelID === pair[0].labelID).key
      pair[1].key = study2.labels.find(label => label.labelID === pair[1].labelID).key
    })
    // reset state
    let matchKeys1 = compareNodules.map(pair => pair[0].key)
    let matchKeys2 = compareNodules.map(pair => pair[1].key)
    this.setState({
      [`position${idx}`]: {x: 0, y: 0, z: 0},
      compareNodules,
      nodules1: study1.labels.filter(label => !matchKeys1.includes(label.key)),
      nodules2: study2.labels.filter(label => !matchKeys2.includes(label.key)),
      labels1: study1.labels,
      labels2: study2.labels,
      [`study${idx}`]: study,
      [`viewport${idx}`]: null,
      [`HU${idx}`]: 0,
      compareData: null,
      activeTabKey: '0',
    })
  }

  getNodulePair(nodule) {
    for (let pair of this.state.compareNodules) {
      if (pair[0].labelID === nodule.labelID || pair[1].labelID === nodule.labelID) {
        return pair
      }
    }
    return false
  }

  componentDidMount() {
    // FIXME: parent mount before child in this component
    setTimeout(this.enableReferLines, 500)
    document.addEventListener('keydown', this.onKeyDown, false)
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeyDown)
  }

  onKeyDown = (e) => {
    const KEYS_WWWC = {
      'F1': {wc: -600, ww: 1200},
      'F2': {wc: 40, ww: 400},
      'F3': {wc: 40, ww: 300},
      'F4': {wc: 800, ww: 2600},
      'F5': {wc: 45, ww: 300},
      'F6': {wc: 35, ww: 80}
    }
    if (e.key in KEYS_WWWC) {
      this.setWWWC(KEYS_WWWC[e.key].ww, KEYS_WWWC[e.key].wc)
    }
  }

  onResize = () => {
    if (!this.state.fullScreen) {
      for (let i of ['1', '2']) {
        let element = this[`element${i}`]
        cornerstone.resize(element, true)
      }
    } else {
      let i = this.state.fullScreen
      let element = this[`element${i}`]
      cornerstone.resize(element, true)
    }
  }

  setWWWC(windowWidth, windowCenter) {
    this.viewer1.getDecoratedComponentInstance().setWWWC(windowWidth, windowCenter)
    this.viewer2.getDecoratedComponentInstance().setWWWC(windowWidth, windowCenter)
  }

  enableReferLines = () => {
    let elem1 = this.viewer1.getDecoratedComponentInstance().getElement()
    let elem2 = this.viewer2.getDecoratedComponentInstance().getElement()
    let config = referenceLine2D.getConfiguration()
    if (!elem1 || !elem2) {
      return false
    }
    if (_.isEmpty(config)) {
      const intersection = {x: 0.5, y: 0.5}
      referenceLine2D.setConfiguration({intersection})
    }
    referenceLine2D.enable(elem1)
    referenceLine2D.enable(elem2)
    referenceLine2D.activate(elem1, 1)
    referenceLine2D.activate(elem2, 1)
    this.setState({
      activeTool: _.without(this.state.activeTool, 'referenceLines').concat(['referenceLines'])
    })
  }

  disableReferLines() {
    let elem1 = this.viewer1.getDecoratedComponentInstance().getElement()
    let elem2 = this.viewer2.getDecoratedComponentInstance().getElement()
    referenceLine2D.disable(elem1)
    referenceLine2D.disable(elem2)
    this.setState({
      activeTool: _.without(this.state.activeTool, 'referenceLines')
    })
  }

  toggleViewerSync() {
    let elem1 = this.viewer1.getDecoratedComponentInstance().getElement()
    let elem2 = this.viewer2.getDecoratedComponentInstance().getElement()
    if (this.state.activeTool.includes('viwerSync')) {
      this.synchronizer.remove(elem1)
      this.synchronizer.remove(elem2)
      this.setState({
        activeTool: _.without(this.state.activeTool, 'viwerSync')
      })
    } else {
      this.synchronizer.add(elem1)
      this.synchronizer.add(elem2)
      this.setState({
        activeTool: _.without(this.state.activeTool, 'viwerSync').concat(['viwerSync'])
      })
    }
  }

  onToolClick = (name, extra) => {
    switch (name) {
      case 'wwwc':
        if (extra) {
          this.setWWWC(extra.ww, extra.wc)
        } else if (!this.state.activeTool.includes('wwwc')) {
          let elem1 = this.viewer1.getDecoratedComponentInstance().getElement()
          let elem2 = this.viewer2.getDecoratedComponentInstance().getElement()
          cornerstoneTools.wwwc.activate(elem2, 1)
          cornerstoneTools.wwwc.activate(elem1, 1)
          cornerstoneTools.pan.deactivate(elem2, 1)
          cornerstoneTools.pan.deactivate(elem1, 1)
          this.setState({
            activeTool: ['wwwc', ...this.state.activeTool]
          })
        } else {
          let elem1 = this.viewer1.getDecoratedComponentInstance().getElement()
          let elem2 = this.viewer2.getDecoratedComponentInstance().getElement()
          cornerstoneTools.wwwc.deactivate(elem2, 1)
          cornerstoneTools.wwwc.deactivate(elem1, 1)
          cornerstoneTools.pan.activate(elem2, 1)
          cornerstoneTools.pan.activate(elem1, 1)
          this.setState({
            activeTool: _.without([...this.state.activeTool], 'wwwc')
          })
        }
        break;
      case 'referenceLines':
        this.state.activeTool.includes('referenceLines') ? this.disableReferLines() : this.enableReferLines()
        break;
      case 'viwerSync':
        this.toggleViewerSync()
        break;
      default:
        break
    }
  }

  toggleFullScreen = (i) => {
    return new Promise((resolve, reject) => {
      if (this.state.fullScreen === i) {
        this.setState({
          fullScreen: null
        }, resolve)
      } else {
        this.setState({
          fullScreen: i
        }, resolve)
      }
    });
  }

  updateCompareData(nodule1, nodule2) {
    const compareData = {
      nodule_diameter: [nodule1.nodule_diameter, nodule2.nodule_diameter],
      nodule_bm_prob: [nodule1.nodule_bm_prob, nodule2.nodule_bm_prob],
      nodule_avgHU: [nodule1.nodule_avgHU, nodule2.nodule_avgHU],
      coord: [nodule1.coord, nodule2.coord],
      nodule_volume: [nodule1.nodule_volume, nodule2.nodule_volume]
    }
    this.setState({
      compareData
    })
  }
//帮助文档
    onClickModel(){
        this.setState({
            visible:true,
        })
    }
    handleOkModel(){
        this.setState({
            visible:false,
        })
    }
    handleCancelModel(){
        this.setState({
            visible:false,
        })
    }
  render() {
    const {study1, study2} = this.state

    return (
      <div id='ct-follow-up-container' style={this.props.style} className='ct-follow-up'>
          <div style={{ position:"absolute",
              width:"100%",
              borderBottom:"1px solid #fff",
              zIndex:"111111"}}>
              <Header>
                  <Logo/>
                  <ReturnCT  />
                  <div style={{float:"left"}}>
                  <ToolBar tools={['wwwc', 'referenceLines', 'viwerSync']} activeTool={this.state.activeTool} forbidEdit={false} onToolClick={this.onToolClick}/></div>
                  <SignoutButton/>
                  <span style={{color:"#fff",float:"right",marginRight:"10px",cursor:"pointer",marginTop:"1px"}} onClick={this.onClickModel}><img src={require('../../../static/images/help_nor.png')}/></span>
              </Header>
          </div>
        <div className='viewer-content'>
       <div style={{width:"325px",overflowX:"hidden"}}>
          <div className='aside'  style={{paddingTop:"67px",overflowY:"scroll"}}>
            <Tabs defaultActiveKey="0"
              type="card"
              activeKey={this.state.activeTabKey}
              onChange={(activeTabKey) => this.setState({activeTabKey})}>
              <TabPane tab={GetLocaledText(this,"CTFollowUp.matched")} key="0">
                <NoduleComparePanel data={this.state.compareNodules}
                  ref={input => {this.noduleComparePanel = input}}
                  maxZ1={this.state.study1.stacks.z.imageIds.length}
                  maxZ2={this.state.study2.stacks.z.imageIds.length}
		  zReverse1={this.state.study1.reverse}
		  zReverse2={this.state.study2.reverse}
                  onSelectNodule={(pair) => {
                    let pos1 = {x: pair[0].x, y: pair[0].y, z: pair[0].z}
                    let pos2 = {x: pair[1].x, y: pair[1].y, z: pair[1].z}
                    // FIXME: avoid using ref. Viewer component is wrapped by ReactDnD's DropTarget
                    // https://github.com/react-dnd/react-dnd/issues/378
                    this.synchronizer.disable()
                    Promise.all([
                      this.viewer1.getDecoratedComponentInstance().setPosition({x: pos1.x - 1, y: pos1.y - 1, z: pos1.z - 1}),
                      this.viewer2.getDecoratedComponentInstance().setPosition({x: pos2.x - 1, y: pos2.y - 1, z: pos2.z - 1})
                    ])
                    .then(() => this.synchronizer.enable())
                  }}/>
              </TabPane>
              <TabPane tab={GetLocaledText(this,"CTFollowUp.followUp1")} key="1">
                <NodulePanel data={this.state.nodules1}
                  ref={input => {this.nodulePanel1 = input}}
                  fixedKey={true}
                  keyPrefix='H'
                  hideSelection={true}
                  hideEditNoduleBtn={true}
                  maxZ={this.state.study1.stacks.z.imageIds.length}
		  zReverse={this.state.study1.reverse}
                  onSelectNodule={(nodule) => {
                    const coord = nodule.coord
                    this.viewer1.getDecoratedComponentInstance().setPosition({x: coord.x - 1, y: coord.y - 1, z: coord.z - 1})
                  }}/>
              </TabPane>
              <TabPane tab={GetLocaledText(this,"CTFollowUp.followUp2")} key="2">
                <NodulePanel data={this.state.nodules2}
                  ref={input => {this.nodulePanel2 = input}}
                  fixedKey={true}
                  keyPrefix='N'
                  hideSelection={true}
                  hideEditNoduleBtn={true}
                  maxZ={this.state.study2.stacks.z.imageIds.length}
		  zReverse={this.state.study2.reverse}
                  onSelectNodule={(nodule) => {
                    const coord = nodule.coord
                    this.viewer2.getDecoratedComponentInstance().setPosition({x: coord.x - 1, y: coord.y - 1, z: coord.z - 1})
                  }}/>
              </TabPane>
            </Tabs>
          </div>
       </div>
          <div style={{height:'100%',paddingTop:"67px"}} className='views'>
            <Viewer
              ref={(input) => this.viewer1 = input}
              study={study1}
              labels={this.state.labels1}
              position={this.state.position1}
              className='view-1'
              title={GetLocaledText(this,"CTFollowUp.followUp1H")}
              active={this.state.activeTool.includes('viwerSync')}
              fullScreen={this.state.fullScreen === '1'}
              hide={this.state.fullScreen === '2'}
              toggleFullScreen={() => this.toggleFullScreen('1')}
              onNoduleClick={(nodule) => {
                let pair = this.getNodulePair(nodule)
                if (pair) {
                  this.setState({activeTabKey: '0'})
                  this.noduleComparePanel.getWrappedInstance().selectPair(pair)
                } else {
                  this.setState({activeTabKey: '1'})
                  this.nodulePanel1.getWrappedInstance().selectNodule(nodule)
                }
              }}
              onChangeStudy={study => this.replaceStudy(1, study)}/>
            <Viewer
              ref={(input) => this.viewer2 = input}
              study={study2}
              labels={this.state.labels2}
              position={this.state.position2}
              className='view-2'
              title={GetLocaledText(this,"CTFollowUp.followUp2H")}
              active={this.state.activeTool.includes('viwerSync')}
              fullScreen={this.state.fullScreen === '2'}
              hide={this.state.fullScreen === '1'}
              toggleFullScreen={() => this.toggleFullScreen('2')}
              onNoduleClick={(nodule) => {
                let pair = this.getNodulePair(nodule)
                if (pair) {
                  this.setState({activeTabKey: '0'})
                  this.noduleComparePanel.getWrappedInstance().selectPair(pair)
                } else {
                  this.setState({activeTabKey: '2'})
                  this.nodulePanel2.getWrappedInstance().selectNodule(nodule)
                }
              }}
              onChangeStudy={study => this.replaceStudy(2, study)}/>
            {
              this.props.studylist.length > 2 ?
              <ThumbnailList
                activeIds={[study1.studyUID, study2.studyUID]}
                data={this.props.studylist.map(study => {
                  return Object.assign({id: study.studyUID}, study)
                })}
                renderItem={study => (
                  <Snapshot study={study}/>
                )}/>
              :
              ''
            }
          </div>
        </div>
          <Modal
              title={GetLocaledText(this, "HelpDocument")}
              visible={this.state.visible}
              onOk={this.handleOkModel}
              onCancel={this.handleCancelModel}
              width={840}
              bodyStyle={{background:"#000",color:"#fff"}}
              footer={null}
          >
              <img src={require('../../../static/images/helpDocument.png')} style={{width:"794px"}} />
          </Modal>
      </div>
    )
  }

}

export default injectIntl(FollowUp)
