#!/bin/python
# coding=utf-8
import os
import sys

from flask import send_from_directory, request

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one

required_key = ['uid']


def fetch_screenshot():
    """
    @api {get} /api/screenshot  获取截图地址
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} uid 截图uid
    @apiParamExample {string} Request-Example:
        /api/screenshot?uid=bb3f2034-f706-11e8-9ea4-0242c0a8e004
    """
    params = request.args.to_dict()
    if not all(key in params for key in required_key):
        return send_from_directory("./data/image/notFound", "notFound.png")

    uid = params['uid']
    resource_info = db_find_one("resource", {"uid": uid})
    if resource_info != None:
        path = resource_info['img_path']
        filename = resource_info['filename']
        return send_from_directory(path, filename)
    else:
        return send_from_directory("./data/image/notFound", "notFound.png")


def fetch_study_image():
    """
    @api {get} /api/study_image  获取PNG图片地址
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} uid PNG图片uid
    @apiParamExample {string} Request-Example:
        /api/study_image?uid=/app/data/image/1.3.12.2.1107.5.1.4.73370.30000018022202360490800001360/z_000.png
    """
    params = request.args.to_dict()

    if not all(key in params for key in required_key):
        return send_from_directory("./data/image/notFound", "notFound.png")

    uid = params['uid']
    if os.path.exists(uid):
        path = os.path.dirname(uid)
        filename = os.path.basename(uid)
        return send_from_directory(path, filename)

    resource_info = db_find_one("resource", {"uid": uid})
    if resource_info != None:
        path = resource_info['img_path']
        filename = resource_info['filename']
        return send_from_directory(path, filename)
    else:
        return send_from_directory("./data/image/notFound", "notFound.png")


def dicom_view(folder, series, filename):
    """
    @api {get} /app/data/dicom/  获取DCM图片地址
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParamExample {string} Request-Example:
        /app/data/dicom/1.3.12.2.1107.5.1.4.85766.30000018051706591820400002338/ec08ff650f7490fc49d7b00761d3e589.dcm
    """
    basedir = "/app/data/%s/%s" % (folder, series)

    return send_from_directory(basedir, filename)


def fetch_study_image_black():
    """
    @api {get} /api/study_image_black  获取缺省图片地址
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} uid PNG图片uid
    @apiParamExample {string} Request-Example:
        /api/study_image?uid=/app/data/image/1.3.12.2.1107.5.1.4.73370.30000018022202360490800001360/z_000.png
    """
    params = request.args.to_dict()
    if "path" in params:
        path = params["path"]
        dirname = os.path.dirname(path)
        basename = os.path.basename(path)
        return send_from_directory(dirname, basename)
    else:
        path = "./data/image/black"
        return send_from_directory(path, "black.png")


def fetch_study_dicom():
    params = request.args.to_dict()

    if not all(key in params for key in required_key):
        return send_from_directory("./data/image/notFound", "notFound.png")

    uid = params['uid']
    resource_info = db_find_one("resource", {"uid": uid})

    path = resource_info['dcm_path']
    filename = resource_info['dicom']
    return send_from_directory(path, filename)
