#!/bin/python
# coding=utf-8
from flask import Flask
from flask_login import LoginManager

from services.auth.auth import login, load_user, logout
from services.auth.signup import register_handler, confirm_email_handler, check_email, check_account
from services.auth.forget_passwd import reset_password_handler, reset_email_handler

from services.study.on_study import on_study
from services.study.get_studylist import get_studylist

from services.report.on_report import on_report
from services.template.on_template import on_template
from services.tool.on_tool import on_tool
from services.label.on_label import on_label
from services.algo.on_radiomics import fetch_radiomics

from services.file.fetch import fetch_screenshot, fetch_study_image
from services.file.feed import feed_screenshot, feed_study_image, feed_pacs_study_image,fetch_manual_detection
from services.algo.nodule_service import nodule_register_label
from services.pacs.pacs_api import fetch_study_info_list,fetch_doctor_request,update_study_state

from config import MAX_CONTENT_LENGTH, SECRET_KEY, PORT
from datetime import timedelta

def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = MAX_CONTENT_LENGTH
app.config['PERMANENT_SESSION_LIFETIME']=timedelta(days=7)
app.secret_key = SECRET_KEY
app.after_request(after_request)

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.init_app(app)
login_manager.user_loader(load_user)

# user services
app.add_url_rule("/auth/login", "login", login, methods=['POST'])
app.add_url_rule("/auth/logout", "logout", logout, methods=['POST'])
app.add_url_rule("/auth/register", "register", register_handler, methods=['POST'])
app.add_url_rule("/auth/register/confirm", "confirm_email_handler", confirm_email_handler, methods=['GET'])
app.add_url_rule("/auth/register/check_account", "check_account", check_account, methods=['GET'])
app.add_url_rule("/auth/register/check_email", "check_email", check_email, methods=['GET'])
app.add_url_rule("/auth/send_reset_email", "reset_email_handler", reset_email_handler, methods=['POST'])
app.add_url_rule("/auth/reset_password", "reset_password_handler", reset_password_handler, methods=['POST'])

app.add_url_rule("/api/study", "on_study", on_study, methods=['GET', 'POST', 'PUT'])
app.add_url_rule("/api/studylist", "get_studylist", get_studylist, methods=['GET'])

app.add_url_rule("/api/report", "on_report", on_report, methods=['GET', 'POST', 'PUT'])
app.add_url_rule("/api/template", "on_template", on_template, methods=['GET', 'POST', 'PUT', 'DELETE'])
app.add_url_rule("/api/tool", "on_tool", on_tool, methods=['GET', 'POST', 'PUT', 'DELETE'])
app.add_url_rule("/api/label", "on_label", on_label, methods=['GET', 'POST', 'PUT', 'DELETE'])
app.add_url_rule('/api/nodule_register_label', 'nodule_register_label', nodule_register_label, methods=['GET'])

app.add_url_rule("/api/screenshot", "fetch_screenshot", fetch_screenshot, methods=['GET'])
app.add_url_rule("/api/study_image", "fetch_study_image", fetch_study_image, methods=['GET'])
app.add_url_rule("/api/upload_screenshot", "feed_screenshot", feed_screenshot, methods=['POST'])
app.add_url_rule("/api/upload_image", "feed_study_image", feed_study_image, methods=['POST'])
app.add_url_rule("/api/upload_pacs_image", "feed_pacs_study_image", feed_pacs_study_image, methods=['POST'])
app.add_url_rule("/api/radiomics", "fetch_radiomics", fetch_radiomics, methods=['GET'])
app.add_url_rule("/api/study_info_list", "fetch_study_info_list", fetch_study_info_list, methods=['POST'])
app.add_url_rule("/api/doctor_request", "fetch_doctor_request", fetch_doctor_request, methods=['GET'])
app.add_url_rule("/api/update_study_state", "update_study_state", update_study_state, methods=['GET'])
app.add_url_rule("/api/manual_detection", "fetch_manual_detection", fetch_manual_detection, methods=['POST'])

if __name__ == '__main__':
    app.run(debug=True, threaded=True, host='0.0.0.0',port=PORT)
