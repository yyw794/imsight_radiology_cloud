import React, { Component } from 'react'
import * as cornerstone from 'cornerstone-core'
import * as cornerstoneWebImageLoader from "cornerstone-web-image-loader"
import * as dicomParser from 'dicom-parser';
import * as cornerstoneWADOImageLoader from 'cornerstone-wado-image-loader'
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import Hammer from "hammerjs"
import {Icon, message, Button, Modal, Spin, Menu, Dropdown, Row, Col, Select,Slider} from 'antd'
import Immutable from 'immutable'

// import ToolBar from '../CTToolBar'
// import NodulePanel from '../NodulePanel'
// import {getRiskText, getNoduleType, RISK_EN2CN, TYPE_EN2CN} from '../NodulePanel'
import noduleBox from '../tools/noduleBox'
import renderReferLine from '../tools/renderReferLine'
import zoomSynchronizer from '../tools/zoomSynchronizer'
import Synchronizer from '../tools/Synchronizer'
import scrollToIndex from '../tools/scrollToIndex'
import scaleTool from '../tools/scaleTool'
import stackPreloader from '../tools/stackPreloader'
import {deepCopy, dataURItoBlob, clip} from '../../util'
import {getUser} from '../../auth'
import axios from 'axios'
import Indicator from '../Indicator'
import {scaleWWDynamic, scaleWCDynamic, scaleWWReverseDynamic, scaleWCReverseDynamic} from '../../util'
import {FormattedMessage, injectIntl} from 'react-intl'
import {angle} from '../tools/angleTool'
import {freehand} from '../tools/freehand'
import {length} from '../tools/length'
import {zoom} from '../tools/zoom'

/**
 * 下拉组件选项
 */
const {Option} = Select

import _ from 'underscore'
// import loadImage from './loadImage'

import './index.less'

cornerstoneTools.external.cornerstone = cornerstone
cornerstoneTools.cornerstoneMath = cornerstoneMath
cornerstoneWebImageLoader.external.cornerstone = cornerstone
cornerstoneTools.external.Hammer = Hammer
cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
cornerstoneWADOImageLoader.external.dicomParser = dicomParser;
//no need to initialized webLoaderManeger here, since DRViewer already initialized it
/**
 * 所用的工具名字声明
 * @type {string[]}
 */
const TOOLS_NAME = ['length', 'angle', 'freehand']

/**
 * CT三视图组件声明
 */
class Viewer extends Component {
    /**
     * 参照react周期函数
     * @param props
     */
  constructor(props) {
    super(props)
      /**
       *
       * @private {{
       * imgPosX: number,  X图片位置
       * imgPosY: number,  Y图片位置
       * imgPosZ: number,  Z图片位置
       * imageX: null,   X图片赋值
       * imageY: null,   Y图片赋值
       * imageZ: null,   Z图片赋值声名
       * viewportX: null, 视图X
       * viewportY: null,  视图Y
       * viewportZ: null, 视图Z
       * HU: number, 层数
       * fullScreen: *,  全屏控制
       * loadingXY: boolean  判断主视图是否加载 props.series.stacks.x.imageIds[0].indexOf("study_image_black")>=0
       * }}
       */
    this.state = {
      dbClickState:true,
      imgPosX: 0,
      imgPosY: 0,
      imgPosZ: 0,
      toolStates:true,
      // labels: props.labels,
      // activeTool: [],
      imageX: null,
      imageY: null,
      imageZ: null,
      viewportX: null,
      viewportY: null,
      viewportZ: null,
      HU: 0,
      obj:{ },
      fullScreen: props.onlyZ ? 'z' : null,
      loadingXY:props.series.stacks.x.imageIds[0].indexOf("study_image_black")>=0,
    }
    this.commonCTFunction=this.commonCTFunction.bind(this);
    this.lablesMark=this.lablesMark.bind(this);
    this.wwxyWcxy=this.wwxyWcxy.bind(this);
    this.scroolUp=this.scroolUp.bind(this);
    this.scroolDown=this.scroolDown.bind(this);
  }

    /**
     * 组件属性onlyZ值是否有变化
     * @param nextProps
     */
  componentWillReceiveProps(nextProps) {
    if (nextProps.onlyZ) {
      this.zoomSynchronizer.disable()
      // this.synchronizer.disable()
    } else {
      this.zoomSynchronizer.enable()
      // this.synchronizer.enable()
    }
    
    let imageIdUpdated =nextProps.series.stacks.x.imageIds[0]!==this.props.series.stacks.x.imageIds[0]

    if(imageIdUpdated){this.setState({loadingXY:false})}

    if (nextProps.study.studyUID !== this.props.study.studyUID || nextProps.series.seriesID !== this.props.series.seriesID || imageIdUpdated ) {
      // study have changed
      this.reset(nextProps.study, nextProps.series)
    }
    if (nextProps.onlyZ != this.props.onlyZ) {
      this.setState({
        fullScreen: nextProps.onlyZ ? 'z' : null
      }, this.onResize)
    }
    if (!Immutable.is(nextProps.labels, this.props.labels)) {
      // labels have changed
      this.forceUpdateImage()
    }
  }
    /**
     * 获取图片的位置
     */
  getImgPos() {
    return {
      x: this.state.imgPosX,
      y: this.state.imgPosY,
      z: this.state.imgPosZ
    }
  }
    /**
     * 强制更新cornerstone页面元素渲染
     */
  forceUpdateImage() {
    cornerstone.updateImage(this.xElement)
    cornerstone.updateImage(this.yElement)
    cornerstone.updateImage(this.zElement)
  }


    /**
     * 获取当前图片对象
     * @param axis
     * @returns {Object}
     */
  getCurrentImageObj(axis) {
    return cornerstone.getImage(this[`${axis}Element`])
  }

    /**
     * 该方法已经失效
     * @param coordImage
     * @param size
     * @returns {Promise<any>}
     */
  getScreenshot = (coordImage, size) => {
    return new Promise((resolve, reject) => {
      const mimetype = 'image/png'
      // const canvas = document.querySelector('#ct-3-viewer-container .crop-canvas')
      // const viewCanvas = document.querySelector('#ct-3-viewer-container .z-view .cornerstone-canvas')
      this.viewCanvas.getContext('2d').setTransform(1, 0, 0, 1, 0, 0);
      let coord = cornerstone.pixelToCanvas(this.zElement, coordImage)
      let imageData = this.viewCanvas.getContext('2d').getImageData(coord.x - size / 2, coord.y - size / 2, size, size)
      this.cropCanvas.getContext('2d').putImageData(imageData, 0, 0)
      const image = dataURItoBlob(this.cropCanvas.toDataURL(mimetype, 1))
      resolve(image)
      /*this.cropCanvas.toBlob(img => {
        resolve(img)
      }, mimetype, 1)*/
    });
  }
    /**
     *同步初始化视图
     */
  initZoomSynchronizer = () => {
    //this.zoomSynchronizer = new cornerstoneTools.Synchronizer("cornerstoneimagerendered", zoomSynchronizer);
      /**
       * 图片渲染 cornerstoneTools里面的接口函数
       * @public {Synchronizer}
       */
    this.zoomSynchronizer = new Synchronizer("cornerstoneimagerendered", zoomSynchronizer)
    this.zoomSynchronizer.add(this.xElement)
    this.zoomSynchronizer.add(this.yElement)
    this.zoomSynchronizer.add(this.zElement)
    this.props.onlyZ && this.zoomSynchronizer.disable()

  }

    /**
     *
     * @returns {*}
     */
  getPixelInCurrentPosition() {
    let {height, width, columns, getPixelData} = cornerstone.getImage(this.zElement)
    const imgNumX = this.props.series.stacks.x.imageIds.length
    const imgNumY = this.props.series.stacks.y.imageIds.length
    const imgNumZ = this.props.series.stacks.z.imageIds.length
    let x = this.state.imgPosX / imgNumX * width
    let y = this.state.imgPosY / imgNumY * height
    let pixelData = getPixelData()
    let idx = (Math.round(x) + Math.round(y) * columns) 
    return pixelData[idx]
  }
  /**
   * 初始化工具
   */
  initLabelTool() {
    const imgNumX = this.props.series.stacks.x.imageIds.length
    const imgNumY = this.props.series.stacks.y.imageIds.length
    const imgNumZ = this.props.series.stacks.z.imageIds.length
    this.activateTool("wwwc")
    noduleBox.enable(this.zElement, {
      axis: 'z',
      depth:Math.round(noduleBox.BOX_SIZE / cornerstone.getImage(this.xElement).height * imgNumZ), // in layers
      horizontalLayers: imgNumX,
      verticalLayers: imgNumY,
      type:this.props.activeTabKey,
      mainAxisLayers: imgNumZ,
      getData: () => this.props.labels.toJS(),
      getCurrentIdx: () => {
        return {x: this.state.imgPosX, y: this.state.imgPosY, z: this.state.imgPosZ}
      },
      hideBoxes:false
    })
    noduleBox.activate(this.zElement, 1)
    noduleBox.activate(this.zElement, 3)

    noduleBox.enable(this.xElement, {
      axis: 'x',
      depth:Math.round(noduleBox.BOX_SIZE / cornerstone.getImage(this.yElement).width * imgNumX), // in layers
      horizontalLayers: imgNumY,
      verticalLayers: imgNumZ,
      mainAxisLayers: imgNumX,
      type:this.props.activeTabKey,
      getData: () => this.props.labels.toJS(),
      getCurrentIdx: () => {
        return {x: this.state.imgPosX, y: this.state.imgPosY, z: this.state.imgPosZ}
      },
      hideBoxes:false
    })
    noduleBox.activate(this.xElement, 1)
    noduleBox.activate(this.xElement, 3)

    noduleBox.enable(this.yElement, {
      axis: 'y',
      depth:Math.round(noduleBox.BOX_SIZE / cornerstone.getImage(this.xElement).width * imgNumY), // in layers
      horizontalLayers: imgNumX,
      type:this.props.activeTabKey,
      verticalLayers: imgNumZ,
      mainAxisLayers: imgNumY,
      getData: () => this.props.labels.toJS(),
      getCurrentIdx: () => {
        return {x: this.state.imgPosX, y: this.state.imgPosY, z: this.state.imgPosZ}
      },
      hideBoxes:false
    })
    noduleBox.activate(this.yElement, 1)
    noduleBox.activate(this.yElement, 3)
  }

  /**
   * 重置节点
   */
  async reset(study, series) {
    const {x: xStack, y: yStack, z: zStack} = series.stacks
    for (let axis of ['x', 'y', 'z']) {
      let element = this[axis + 'Element']
      let image = await cornerstone.loadImage(series.stacks[axis].imageIds[0])
      cornerstoneTools.clearToolState(element, 'stack')
      cornerstoneTools.addToolState(element, 'stack', series.stacks[axis])
      cornerstone.displayImage(element, image)
      cornerstoneTools.wwwc.activate(element, 1)
      cornerstoneTools.pan.activate(element, 2)
      this.disableReferLines()
      // baseScale
      let baseScale = cornerstone.getViewport(element).scale
      cornerstoneTools.clearToolState(element, 'zoom')
      cornerstoneTools.addToolState(element, 'zoom', {baseScale})
        /**
         * 当前视图缩放大小
         */
      this['baseScale' + axis.toUpperCase()] = baseScale
      this.scrollTo(axis, 0.5)
    }
    this.initZoomSynchronizer()
      if(this.props.activeTabKey==="ribs"){
          this.setWWWC(1500, 300)
      }else{
          this.setWWWC(1500, -550)
      }
    //this.enableReferLines()
    this.setState({
      HU: this.getPixelInCurrentPosition(),
      fullScreen: null 
    },this.onResize)
  }

    /**
     * 初始话事件监听
     */
  initEventListener() {

    this.zElement.addEventListener(cornerstoneTools.EVENTS.MOUSE_CLICK, (e) => {
      const eventData = e.detail
      const coord = eventData.currentPoints.image
      const {columns, rows} = eventData.image
      // rgba
      let idx = (Math.round(coord.x) + Math.round(coord.y) * eventData.image.columns)
      let pixelData = eventData.image.getPixelData()
      // // WARNING: since it's a gray scale image stored in rgba, rgb value will be equal to gray scale
      let pixel = pixelData[idx]
      let HU = pixel
      this.setState({HU})
      Promise.all([
        this.scrollTo('x', coord.x / columns),
        this.scrollTo('y', coord.y / rows)
      ]).then(() => {
        this.props.onClick(e)
      })
      this.initZoomSynchronizer()
    })
    this.xElement.addEventListener(cornerstoneTools.EVENTS.MOUSE_CLICK, (e) => {
      const eventData = e.detail
      const coord = eventData.currentPoints.image
      const {columns, rows} = eventData.image
      Promise.all([
        this.scrollTo('y', coord.x / columns),
        this.scrollTo('z', 1 - coord.y / rows)
      ]).then(()=>{
        this.props.onClick(e)
      })
     this.initZoomSynchronizer()
    })
    this.yElement.addEventListener(cornerstoneTools.EVENTS.MOUSE_CLICK, (e) => {
      const eventData = e.detail
      const coord = eventData.currentPoints.image
      const {columns, rows} = eventData.image
      Promise.all([
        this.scrollTo('x', coord.x / columns),
        this.scrollTo('z', 1 - coord.y / rows)
      ]).then(()=>{
        this.props.onClick(e)
      })
      this.initZoomSynchronizer()
    })
    document.addEventListener('keydown', this.onKeyDown)
    window.addEventListener("resize", _.debounce(this.onResize, 500))
  }

    /**
     *在第一次渲染后调用，只在客户端。之后组件已经生成了对应的DOM结构，可以通过this.getDOMNode()来进行访问。 如果你想和其他JavaScript框架一起使用，可以在这个方法中调用setTimeout, setInterval或者发送AJAX请求等操作(防止异步操作阻塞UI)。
     */
  componentDidMount() {
    const {x: xStack, y: yStack, z: zStack} = this.props.series.stacks
        const that=this;
      /**
       * cornerstOneJs工具同步更新视图
       * @public {Synchronizer}
       */
    this.synchronizer = new Synchronizer("cornerstonenewimage", cornerstoneTools.updateImageSynchronizer)
    this.lablesMark();
    let initXStack = this.initStack(this.xElement, xStack, this.synchronizer, 'X')
    let initYStack = this.initStack(this.yElement, yStack, this.synchronizer, 'Y')
    let initZStack = this.initStack(this.zElement, zStack, this.synchronizer, 'Z')
    Promise.all([initZStack, initXStack, initYStack])
    .then(() => {
      return Promise.all([
        this.scrollTo('x', 0.5),
        this.scrollTo('y', 0.5),
        this.scrollTo('z', 0.5)
      ])
    })
    .then(() => {
      // preload images
      // stackPreloader.enable(this.xElement)
      // stackPreloader.enable(this.yElement)
      // stackPreloader.enable(this.zElement)

      scaleTool.enable(this.zElement)
      if(Object.keys(this.state.obj).length>0){
            that.scrollTo('z',null,Number(Object.keys(that.state.obj)[0]))
            that.setState({
                imgPosZ:Number(Object.keys(that.state.obj)[0])
            })
        }
      // default lung
      this.setWWWC(1500, -550)
      this.initZoomSynchronizer()
      this.initLabelTool()
      //this.enableReferLines()
      this.setState({HU: this.getPixelInCurrentPosition()})
      // document.addEventListener('keydown', this.onKeyDown)
      // window.addEventListener("resize", _.debounce(this.onResize, 500))
      this.initEventListener()
      this.deactivateTool("referenceLines")

      // this.initSocket()
    })
  }

  lablesMark(data){
      const that=this;
      if(this.props.labels){
          var obj={}
          this.props.labels.toJS().map(function(item,index) {
              var key=Math.ceil(item.coord.z);
              if(obj[key]){
                  obj[key]++;
              }else{
                  obj[key] = 1;
              }
          })
          this.setState({
              obj:obj
          },function () {
              if(data){
                  that.scrollTo('z',null,Number(Object.keys(that.state.obj)[0]))
              }
          })
      }
  }
    /**
     * 在组件从 DOM 中移除之前立刻被调用。
     */
  componentWillUnmount() {
    window.removeEventListener("resize", this.onResize)
    document.removeEventListener('keydown', this.onKeyDown)
    // stackPreloader.disable(this.xElement)
    // stackPreloader.disable(this.yElement)
    // stackPreloader.disable(this.zElement)
  }


    /**
     * 排序
     */
   compare(x, y) {
        if (x < y) {
            return -1;
        } else if (x > y) {
            return 1;
        } else {
            return 0;
        }
    }
    /**
     * 调窗button快捷键
     * @param e
     */
  onKeyDown = (e) => {
    const KEYS_WWWC = {
      '1': {wc: -550, ww: 1500},
      '2': {wc: 60, ww: 400},
      '3': {wc: 300, ww: 1500},
      '4': {wc: 45, ww: 300},
      '5': {wc: 40, ww: 80}
    }
    if (e.key in KEYS_WWWC) {
      this.setWWWC(KEYS_WWWC[e.key].ww,KEYS_WWWC[e.key].wc)
    }
    var objArr=[];
    if(Object.keys(this.state.obj).length>0){
      const arr=Array.from(Object.keys(this.state.obj));
        const arrB=Array.from(new Set(arr.concat([""+this.state.imgPosZ])));
        const arrC=arrB.map(function (item,index) {
            return Number(item)
        })
        objArr=arrC.sort(this.compare)
    }
      switch (e.key) {
          case 'ArrowRight':
              this.scrollTo('x', null, this.state.imgPosX + 1)
                  .then(() => this.setState({HU: this.getPixelInCurrentPosition()}))
              break;
          case 'ArrowLeft':
              this.scrollTo('x', null, this.state.imgPosX - 1)
                  .then(() => this.setState({HU: this.getPixelInCurrentPosition()}))
              break;
          case 'ArrowUp':
             this.scroolUp(objArr)
              break;
          case 'ArrowDown':
            this.scroolDown(objArr)
              break;
          case 'PageUp':
              this.scrollTo('z', null, this.props.study.reverse?(this.state.imgPosZ-1):(this.state.imgPosZ+1))
                  .then(() => this.setState({HU: this.getPixelInCurrentPosition()}))
              break;
          case 'PageDown':
              this.scrollTo('z', null, this.props.study.reverse?(this.state.imgPosZ+1):(this.state.imgPosZ-1))
                  .then(() => this.setState({HU: this.getPixelInCurrentPosition()}))
              break;
          case 'Escape':
              this.commonCTFunction()
              break;
          default:
      }
      if (!!window.ActiveXObject || "ActiveXObject" in window) {
          switch (e.key) {
              case 'Up':
                  this.scroolUp(objArr)
                  break;
              case 'Down':
                  this.scroolDown(objArr)
                  break;
              case 'Esc':
                this.commonCTFunction()
                  break;
          }
      }
  }
  scroolUp(objArr){
      if (objArr.indexOf(this.state.imgPosZ) + 1 < objArr.length) {
          this.scrollTo('z', null, Number(objArr[objArr.indexOf(this.state.imgPosZ)+1]))
              .then(() => this.setState({
                  HU: this.getPixelInCurrentPosition()
              }))
      }
  }
  scroolDown(objArr){
      if (objArr.indexOf(this.state.imgPosZ)-1>=0) {
          this.scrollTo('z', null,Number(objArr[objArr.indexOf(this.state.imgPosZ)-1]))
              .then(() => this.setState({
                   HU: this.getPixelInCurrentPosition()
              }))
      }
    }
  commonCTFunction(){
      this.zoomSynchronizer.disable()
      if(this.zElement.children[0].width!==0){
          let viewport = cornerstone.getViewport(this.zElement)
          viewport.scale = this.baseScaleZ
          viewport.translation = {x: 0, y: 0}
         if(this.props.activeTabKey=="ribs"){
             viewport.voi.windowWidth = 1500
             viewport.voi.windowCenter =300
         }else{
             viewport.voi.windowWidth = 1500
             viewport.voi.windowCenter = -550
         }
          cornerstone.setViewport(this.zElement, viewport)
      }

      if(this.xElement.children[0].width!==0){
          let viewportX = cornerstone.getViewport(this.xElement)
          viewportX.scale = this.baseScaleX
          viewportX.translation = {x: 0, y: 0}
          const obj=this.wwxyWcxy();
          viewportX.voi.windowWidth =obj.wwxy;
          viewportX.voi.windowCenter =obj.wcxy;
          cornerstone.setViewport(this.xElement, viewportX)
      }
      if(this.yElement.children[0].width!==0){
          let viewportY = cornerstone.getViewport(this.yElement)
          viewportY.scale = this.baseScaleY
          viewportY.translation = {x: 0, y: 0}
          const obj=this.wwxyWcxy();
          viewportY.voi.windowWidth = obj.wwxy;
          viewportY.voi.windowCenter = obj.wcxy;
          cornerstone.setViewport(this.yElement, viewportY)

      }
      this.zoomSynchronizer.enable()
  }

  wwxyWcxy(){
      let wwxy,wcxy;
      if(this.props.activeTabKey=="ribs"){
          wwxy = scaleWWReverseDynamic(1500, this.props.series.minPixelValue, this.props.series.maxPixelValue)
          wcxy = scaleWCReverseDynamic(300, this.props.series.minPixelValue, this.props.series.maxPixelValue)
      }else {
          wwxy = scaleWWReverseDynamic(1500, this.props.series.minPixelValue, this.props.series.maxPixelValue)
          wcxy = scaleWCReverseDynamic(-550, this.props.series.minPixelValue, this.props.series.maxPixelValue)
      }
      return {wwxy,wcxy}
  }

  closeKeyDown(){
    document.removeEventListener("keydown",this.onKeyDown)
  }

  addKeyDown(){
    document.addEventListener("keydown",this.onKeyDown)
  }
    /**
     * 调整窗口大小
     */
  onResize = () => {
    this.synchronizer.disable()
    this.zoomSynchronizer.disable()
    if (!this.state.fullScreen) {
      for (let axis of ['x', 'y', 'z']) {
        let element = this[axis + 'Element']
        cornerstone.resize(element, true)
        let baseScale = cornerstone.getViewport(element).scale
        cornerstoneTools.clearToolState(element, 'zoom')
        cornerstoneTools.addToolState(element, 'zoom', {baseScale})
        this['baseScale' + axis.toUpperCase()] = baseScale
      }
    } else {
      let axis = this.state.fullScreen
      let element = this[axis + 'Element']
      cornerstone.resize(element, true)
      let baseScale = cornerstone.getViewport(element).scale
      cornerstoneTools.clearToolState(element, 'zoom')
      cornerstoneTools.addToolState(element, 'zoom', {baseScale})
      this['baseScale' + axis.toUpperCase()] = baseScale
    }
    this.synchronizer.enable()
    this.zoomSynchronizer.enable()
    // /this.initZoomSynchronizer()
  }

  /**
   * [scrollTo description]
   * @param  {[type]}  axis          [x, y, z]
   * @param  {[type]}  ratio         [ratio to calculate index]
   * @param  {Boolean} [exact=false] [exact index]
   * @return {[Promise]} resolve when image is loaded
   */
  scrollTo(axis, ratio, exact=false) {
    let idx
    let imgNum = this.props.series.stacks[axis].imageIds.length
    if (exact !== false) {
      idx = clip(exact, 0, imgNum - 1)
    } else {
      idx = clip(Math.ceil(imgNum * ratio) - 1, 0, imgNum - 1)
    }
    return scrollToIndex(this[axis + 'Element'], Math.ceil(idx))
  }

    /**
     *初始话视图加载医学影像图
     * @param element
     * @param stackData
     * @param synchronizer
     * @param direction
     * @returns {Promise}
     */
  initStack(element, stackData, synchronizer, direction) {
    cornerstone.enable(element)
    // Enable mouse inputs
    cornerstoneTools.mouseInput.enable(element)
    cornerstoneTools.mouseWheelInput.enable(element)

    return cornerstone.loadImage(stackData.imageIds[0])
      .then((image) => {
        cornerstone.displayImage(element, image)
        // set the stack as tool state
        cornerstoneTools.addStackStateManager(element, ['stack'])
        cornerstoneTools.addToolState(element, 'stack', stackData)
        // Enable all tools we want to use with this element
        cornerstoneTools.wwwc.activate(element, 1)
        cornerstoneTools.pan.activate(element, 2)
        zoom.activate(element, 4)
        cornerstoneTools.stackScrollWheel.activate(element)

        element.addEventListener("cornerstonenewimage", (e) => {
          const stackData = cornerstoneTools.getToolState(element, 'stack')
          this.setState({
            ['imgPos' + direction.toUpperCase()]: stackData.data[0].currentImageIdIndex,
            ['image' + direction.toUpperCase()]: e.detail.image
          })
          cornerstoneTools.clearToolState(element,'zoom')
          cornerstoneTools.addToolState(element, 'zoom', {baseScale: this['baseScale' + direction.toUpperCase()]})
        })

        this.setState({['image' + direction.toUpperCase()]: image})

        // baseScale
        let baseScale = cornerstone.getViewport(element).scale
       // cornerstoneTools.clearToolState(element,'zoom')
        cornerstoneTools.addToolState(element, 'zoom', {baseScale})
          /**
           * 视图缩放大小
           */
        this['baseScale' + direction.toUpperCase()] = baseScale
        element.addEventListener('cornerstoneimagerendered', () => {
          this.setState({
            ['viewport' + direction.toUpperCase()]: cornerstone.getViewport(element)
          })
        })

        synchronizer.add(element)
        // this.props.onlyZ && synchronizer.disable()
      })
  }

    /**
     * 工具激活
     * @param name 工具名
     */
  activateTool(name) {
    switch (name) {
      case 'wwwc':
        cornerstoneTools.wwwc.activate(this.xElement, 1)
        cornerstoneTools.wwwc.activate(this.yElement, 1)
        cornerstoneTools.wwwc.activate(this.zElement, 1)
        cornerstoneTools.pan.activate(this.xElement, 2)
        cornerstoneTools.pan.activate(this.yElement, 2)
        cornerstoneTools.pan.activate(this.zElement, 2)
        this.disableLine()
        this.disableAngle()
        this.disableFreehand()
        break;
        case 'pan':
            cornerstoneTools.pan.activate(this.xElement, 2)
            cornerstoneTools.pan.activate(this.yElement, 2)
            cornerstoneTools.pan.activate(this.zElement, 2)
          break;
      case 'referenceLines':
          this.setState({
              dbClickState:true,
          })
          this.disableLine()
          this.disableAngle()
          this.activateTool("pan")
          this.disableFreehand()
          this.enableReferLines()

        break;
      case 'hideBoxes':
          this.setState({
              dbClickState:true,
          })
          this.disableLine()
          this.disableAngle()
          this.activateTool("pan")
          this.disableFreehand()
        this.hideBoxes()
        break;
      case 'length':
        this.setState({
            dbClickState:false,
        })
          this.deactivateTool("wwwc")
          this.disableAngle()
          this.activateTool("pan")
          this.disableReferLines()
          this.disableFreehand()
          this.enableLine()
        break;
      case 'angle':
          this.setState({
              dbClickState:false,
          })
          this.deactivateTool("wwwc")
          this.disableLine()
          this.activateTool("pan")
          this.disableReferLines()
          this.disableFreehand()
        this.enableAngle()
        break;
      case 'freehand':
          this.setState({
              dbClickState:false,
          })
        this.deactivateTool("wwwc")
         this.disableLine()
          this.disableReferLines()
          this.activateTool("pan")
          this.disableAngle()
        this.enableFreehand()
        break;
      default:
        break
    }
   this.initZoomSynchronizer()
  }

    /**
     * 清除绘制
     */
  clearAllTool(){
    this.clearLine()
    this.clearAngle()
    this.clearFreehand()
    this.activateTool("wwwc")
    this.disableReferLines()
    this.setState({
       dbClickState:true,
    })
  }

    /**
     * MPR
     */
    onMPR(){
        this.toggleFullScreen('z')
        this.state.fullScreen==='z' && this.props.beginXYConvert()
        this.initZoomSynchronizer()
        if(this.state.toolStates){
          this.setState({
              toolStates:false
          })
        }else{
            this.setState({
                toolStates:true
            })
        }
    }


    /**
     * 工具停用
     * @param name
     */
  deactivateTool(name) {
    switch (name) {
      case 'wwwc':
        cornerstoneTools.wwwc.deactivate(this.xElement, 1)
        cornerstoneTools.wwwc.deactivate(this.yElement, 1)
        cornerstoneTools.wwwc.deactivate(this.zElement, 1)
        cornerstoneTools.pan.activate(this.xElement, 1)
        cornerstoneTools.pan.activate(this.yElement, 1)
        cornerstoneTools.pan.activate(this.zElement, 1)
        break;
      case 'referenceLines':
        this.activateTool("wwwc")
        this.disableReferLines()
        break;
      case 'hideBoxes':
        this.activateTool("wwwc")
        this.showBoxes()
        break;
      case 'length':
         this.activateTool("wwwc")
          this.setState({
              dbClickState:true,
          })
         this.disableLine();
         break;
       case 'angle':
         this.activateTool("wwwc")
           this.setState({
               dbClickState:true,
           })
         this.disableAngle()
          break;
      case 'freehand':
          this.activateTool("wwwc")
          this.setState({
              dbClickState:true,
          })
          this.disableFreehand()
         break;
      default:
        break
    }

  }

    /**
     *调窗
     * @param windowWidth 窗宽
     * @param windowCenter 窗位
     */
  setWWWC(windowWidth, windowCenter) {
    let wwxy = scaleWWReverseDynamic(windowWidth,this.props.series.minPixelValue,this.props.series.maxPixelValue)
    let wcxy = scaleWCReverseDynamic(windowCenter,this.props.series.minPixelValue,this.props.series.maxPixelValue)
    let wwz = windowWidth
    let wcz = windowCenter
    for (let axis of ['x', 'y']) {
      let elem = this[axis + 'Element']
      let viewport = cornerstone.getViewport(elem)
      viewport.voi.windowWidth = wwxy
      viewport.voi.windowCenter = wcxy
      cornerstone.updateImage(elem)
    }
    let elem = this['z'+'Element']
    let viewport = cornerstone.getViewport(elem)
    viewport.voi.windowWidth = wwz
    viewport.voi.windowCenter = wcz
    cornerstone.updateImage(elem)
  }

  getWW(){return this.state.viewportZ.voi.windowWidth}
  getWC(){return this.state.viewportZ.voi.windowCenter}

    /**
     * 显示病灶
     */
  showBoxes(){
    noduleBox.updateOptions(this.zElement,{hideBoxes:false})
    noduleBox.updateOptions(this.xElement,{hideBoxes:false})
    noduleBox.updateOptions(this.yElement,{hideBoxes:false})
    this.forceUpdateImage()
  }
    /**
     * 隐藏病灶
     */
  hideBoxes(){
    noduleBox.updateOptions(this.zElement,{hideBoxes:true})
    noduleBox.updateOptions(this.xElement,{hideBoxes:true})
    noduleBox.updateOptions(this.yElement,{hideBoxes:true})
    this.forceUpdateImage()
  }

    /**
     * 停止参考线
     */
  enableReferLines() {
    cornerstoneTools.referenceLines.tool.enable(this.xElement, this.synchronizer, renderReferLine)
    cornerstoneTools.referenceLines.tool.enable(this.yElement, this.synchronizer, renderReferLine)
    cornerstoneTools.referenceLines.tool.enable(this.zElement, this.synchronizer, renderReferLine)
    // this.setState({
    //   // activeTool: [...this.state.activeTool, 'referenceLines'],
    //   activeTool: _.without([...this.state.activeTool], 'referenceLines').concat(['referenceLines'])
    // })
  }

    /**
     * 画线开启
     */
  enableLine(){
      length.activate(this.xElement, 1)
      length.activate(this.yElement, 1)
      length.activate(this.zElement, 1)
  }

    /**
     * 清除画线
     */
  clearLine(){
      cornerstoneTools.clearToolState(this.xElement,  "length")
      cornerstoneTools.clearToolState(this.yElement,  "length")
      cornerstoneTools.clearToolState(this.zElement,  "length")
     length.enable(this.xElement)
     length.enable(this.yElement)
     length.enable(this.zElement)
  }

    /**
     * 开启画角工具
     */
  enableAngle(){
      angle.activate(this.xElement, 1)
      angle.activate(this.yElement, 1)
      angle.activate(this.zElement, 1)

  }

    /**
     * 清除角度
     */
  clearAngle(){
      cornerstoneTools.clearToolState(this.xElement,  "angle")
      cornerstoneTools.clearToolState(this.yElement,  "angle")
      cornerstoneTools.clearToolState(this.zElement,  "angle")
      angle.enable(this.xElement,1)
      angle.enable(this.yElement,1)
      angle.enable(this.zElement,1)
  }

    /**
     * 开启画面积工具
     */
  enableFreehand(){
   freehand.activate(this.xElement, 1)
   freehand.activate(this.yElement, 1)
   freehand.activate(this.zElement, 1)
    }

    /**
     * 清除面积label
     */
  clearFreehand(){
      cornerstoneTools.clearToolState(this.xElement,  "freehand")
      cornerstoneTools.clearToolState(this.yElement,  "freehand")
      cornerstoneTools.clearToolState(this.zElement,  "freehand")
        freehand.enable(this.xElement, 1)
        freehand.enable(this.yElement, 1)
        freehand.enable(this.zElement, 1)
    }

    /**
     * 停止面积工具
     */
  disableFreehand(){
   freehand.deactivate(this.xElement, 1)
   freehand.deactivate(this.yElement, 1)
   freehand.deactivate(this.zElement, 1)
  }
    /**
     * 停止角度工具
     */
  disableAngle(){
  angle.deactivate(this.xElement, 1)
  angle.deactivate(this.yElement, 1)
  angle.deactivate(this.zElement, 1)
  }
    /**
     * 停止线段工具
     */
  disableLine(){
    length.deactivate(this.xElement, 1)
    length.deactivate(this.yElement, 1)
    length.deactivate(this.zElement, 1)
  }
    /**
     * 停止参考线
     */
  disableReferLines() {
    cornerstoneTools.referenceLines.tool.disable(this.xElement, this.synchronizer, renderReferLine)
    cornerstoneTools.referenceLines.tool.disable(this.yElement, this.synchronizer, renderReferLine)
    cornerstoneTools.referenceLines.tool.disable(this.zElement, this.synchronizer, renderReferLine)
    // this.setState({
    //   activeTool: _.without([...this.state.activeTool], 'referenceLines')
    // })
  }

    /**
     * 全屏切换
     * @param axis
     */
  toggleFullScreen(axis) {
    if (this.state.fullScreen === axis) {
      this.setState({
        fullScreen: null
      }, () => window.requestAnimationFrame(this.onResize))
    } else {
      this.setState({
        fullScreen: axis
      }, () => window.requestAnimationFrame(this.onResize))
    }
  }

    /**
     *CT阅片组件
     * @returns {Component}
     */
  render() {
    const nodulePopoverBtn = this.state.nodulePopoverBtn;
    let arr=[];

    const showImgPosZ = this.props.study.reverse?(this.props.series.stacks.z.imageIds.length - this.state.imgPosZ):(this.state.imgPosZ)
    return (
      <div
         style={this.props.style}
         className='ct-viewer-container'

       >
        <canvas  className='crop-canvas' width='300' height='300' ref={input => this.cropCanvas = input}/>
        <div className={this.state.fullScreen ? `views full-screen-${this.state.fullScreen}` : 'views'}>
          <div className='view z-view'>
            <div ref={input => {this.zElement = input}} className='cornerstone-elm'
        onDoubleClick ={(e)=>{
              if(this.state.dbClickState&&Number(e.button)===0){
                     this.toggleFullScreen('z')
                     this.state.fullScreen==='z' && this.props.beginXYConvert()
                     this.initZoomSynchronizer()
                     this.props.changeToolState(this.state.toolStates)
                     this.setState({
                         toolStates:!this.state.toolStates
                     })
                 }
              }}>
              <canvas className="cornerstone-canvas"
                ref={input => this.viewCanvas = input}
                onContextMenu={e=>e.preventDefault()}/>
              <div className='info-box'>
                <p>HU: {Math.round(this.state.HU)}</p>
                <p>WW: {
                    this.state.viewportZ ?
                    Math.round(this.state.viewportZ.voi.windowWidth) : 0}</p>
                <p>WL: {this.state.viewportZ ?
                    Math.round(this.state.viewportZ.voi.windowCenter) : 0}</p>
                  <div>
                       <span className='img-pos'>{showImgPosZ} / {this.props.series.stacks.z.imageIds.length}</span>
                  </div>
              </div>
            </div>
            <Slider defaultValue={0}
              included={false}
              vertical={true}
              min = {1}
              marks={this.state.obj}
              max = {this.props.series.stacks.z.imageIds.length}
              value={showImgPosZ}
              onChange={(value)=>{
                  this.scrollTo('z',null,value)
                      .then(()=>{
                           this.setState({imgPosZ:this.props.study.reverse?(this.props.series.stacks.z.imageIds.length-value):(value),
					       HU: this.getPixelInCurrentPosition()
					   })}
                      )
                      .catch((err)=>console.log("scroll to error, to be fixed",err))
              }}/>
            <Indicator
              image={this.state.imageZ}
              viewport={this.state.viewportZ}
              onMouseDrag={translation => {
                let viewport = Object.assign({}, this.state.viewportZ)
                viewport.translation = translation
                cornerstone.setViewport(this.zElement, viewport)
              }}
              baseScale={this.baseScaleZ}/>

            <Button
              ghost
              hidden={this.props.enableToggler}
              style={{display:"none"}}
              icon={this.state.fullScreen === 'z' ? 'shrink' : 'arrows-alt'}
              className='toggle-full-screen'
              onClick={(e) => {
                  if(Number(e.button)===0){
                      this.toggleFullScreen('z')
                      this.state.fullScreen==='z' && this.props.beginXYConvert()
                      this.initZoomSynchronizer()
                  }
              }}>
            </Button>
              <div style={{position:"absolute",right:"4px",bottom:"4px"}}>
              {/*<Button ghost onClick={() => {*/}
                    {/*this.zoomSynchronizer.disable()*/}
                    {/*let viewport = cornerstone.getViewport(this.zElement)*/}
                    {/*viewport.scale = this.baseScaleZ*/}
                    {/*viewport.translation = {x: 0, y: 0}*/}
                    {/*cornerstone.setViewport(this.zElement, viewport)*/}
                    {/*this.zoomSynchronizer.enable()*/}
                {/*}}><FormattedMessage id="CTViewer.autoFit"/></Button>*/}
              </div>
          </div>

              <div className='view x-view'>
                <div ref={input => {this.xElement = input}} className='cornerstone-elm'
                  onDoubleClick ={(e)=>{
                    if(Number(e.button)===0) {
                        this.toggleFullScreen('x')
                        this.props.changeToolState(this.state.toolStates)
                        this.setState({
                            toolStates: !this.state.toolStates
                        })
                    }
              }}>
              <canvas className="cornerstone-canvas"
                ref={input => this.viewCanvas = input}
                onContextMenu={e=>e.preventDefault()}
                />
            </div>
            {
              this.state.loadingXY ?
              <Spin size='large'/>:''
            }
            <Indicator
              image={this.state.imageX}
              viewport={this.state.viewportX}
              onMouseDrag={translation => {
                let viewport = Object.assign({}, this.state.viewportX)
                viewport.translation = translation
                cornerstone.setViewport(this.xElement, viewport)
              }}
              baseScale={this.baseScaleX}/>
            <Button
              ghost
              hidden={this.props.enableToggler}
              icon={this.state.fullScreen === 'x' ? 'shrink' : 'arrows-alt'}
              className='toggle-full-screen'
              style={{display:"none"}}
              onClick={() => this.toggleFullScreen('x')}>
            </Button>
            <div className='view-footer'>
            {/*<Button ghost onClick={() => {*/}
                {/*this.zoomSynchronizer.disable()*/}
                {/*let viewportX = cornerstone.getViewport(this.xElement)*/}
                {/*viewportX.scale = this.baseScaleX*/}
                {/*viewportX.translation = {x: 0, y: 0}*/}
                {/*cornerstone.setViewport(this.xElement, viewportX)*/}
                {/*this.zoomSynchronizer.enable()*/}
            {/*}}><FormattedMessage id="CTViewer.autoFit"/></Button>*/}
              <span className='img-pos'>{this.state.imgPosX + 1} / {this.props.series.stacks.x.imageIds.length}</span>
            </div>
          </div>

          <div className='view y-view'>
            <div ref={input => {this.yElement = input}} className='cornerstone-elm' >
              <canvas className="cornerstone-canvas"
                ref={input => this.viewCanvas = input}
                onDoubleClick ={(e)=>{
                    if(Number(e.button)===0) {
                        this.toggleFullScreen('y')
                        this.initZoomSynchronizer()
                        this.props.changeToolState(this.state.toolStates)
                        this.setState({
                            toolStates: !this.state.toolStates
                        })
                    }
                }}
                onContextMenu={e=> e.preventDefault()}/>
            </div>
            {
              this.state.loadingXY ?
              <Spin size='large'/>:''
            }
            <Indicator
              image={this.state.imageY}
              viewport={this.state.viewportY}
              onMouseDrag={translation => {
                let viewport = Object.assign({}, this.state.viewportY)
                viewport.translation = translation
                cornerstone.setViewport(this.yElement, viewport)
              }}
              baseScale={this.baseScaleY}/>
            <Button
              ghost
              hidden={this.props.enableToggler}
              icon={this.state.fullScreen === 'y' ? 'shrink' : 'arrows-alt'}
              className='toggle-full-screen'
              style={{display:"none"}}
              onClick={() => this.toggleFullScreen('y')}>
            </Button>
            <div className='view-footer'>
            {/*<Button ghost onClick={() => {*/}
            {/*this.zoomSynchronizer.disable()*/}
            {/*let viewportY = cornerstone.getViewport(this.yElement)*/}
            {/* viewportY.scale = this.baseScaleY*/}
            {/*viewportY.translation = {x: 0, y: 0}*/}
            {/*cornerstone.setViewport(this.yElement, viewportY)*/}
            {/*this.zoomSynchronizer.enable()*/}
            {/* }}><FormattedMessage id="CTViewer.autoFit"/></Button>*/}
              <span className='img-pos'>{this.state.imgPosY + 1} / {this.props.series.stacks.y.imageIds.length}</span>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

export default Viewer
