import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'
import './index.less'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
/**
 * 样式
 * @type {{cursor: string, WebkitUserSelect: string, MozUserSelect: string, msUserSelect: string, userSelect: string}}
 */
const style = {
  'cursor': 'pointer',
  'WebkitUserSelect': 'none',
  'MozUserSelect': 'none',
  'msUserSelect': 'none',
  'userSelect': 'none',
}
/**
 * 返回上一步
 */
const goBack=()=>{
    window.history.back()
}
/**
 * 返回上一步组件
 */
const UpStep = withRouter(
  ({ history }) => (
      <a style={{float:"left",color:"#fff",height:"1rem"}} onClick={goBack}>
        <img style={style} className='logo-image'style={{
            cursor:"pointer",
            userSelect:"none",
            width:"1rem",
            verticalAlign:"middle",
            marginRight:"10px",
            marginTop:"-0.1rem",
            marginLeft:"20px"}} src={require('../../../static/images/up.png')} /*onClick={() => history.push('/')}*//>
          <span style={{fontSize:"12px"}}><FormattedMessage id='UpStep.button.name'/></span>
      </a>
   )
)

export default UpStep
