import React, { Component } from 'react';
import _ from 'underscore'
import {Icon} from 'antd'
import './index.less'
import {VIEW_POSITION} from '../../data'
import { Scrollbars } from 'react-custom-scrollbars'
import Immutable from 'immutable'

class ThumbnailList extends Component {
  state = {
    // currentIdx: this.props.defaultIdx,
    collapsed: false
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  onMouseWheel = (e) => {
  	const currentScrollDelta = this.scrollbar.getScrollLeft();
  	this.scrollbar.scrollLeft(currentScrollDelta + e.deltaY);
  }

  render() {
    return (
      <div className={this.state.collapsed ? 'thumbnail-list-container collapsed' : 'thumbnail-list-container'}>
        <Scrollbars
          ref={input => this.scrollbar = input}
          style={{ height: 200 }}
          renderThumbHorizontal={props => <div {...props} className="thumb-horizontal"/>}
          renderTrackVertical={props => <div {...props} style={{display: 'none'}}/>}
          renderThumbVertical={props => <div {...props} style={{display: 'none'}}/>}
          onWheel={this.onMouseWheel}>
          <ul className='thumbnail-list'>
            {
              this.props.data.map((item, i) => (
                <li
                  key={item.id}
                  onClick={() => {_.isFunction(this.props.onItemClick) && this.props.onItemClick(item)}}
                  className={this.props.activeIds.includes(item.id) ? 'active' : ''}>
                  {this.props.renderItem(item, i)}
                </li>
              ))
            }
          </ul>
        </Scrollbars>
        <div className='toggle-collapsed vertical' onClick={this.toggleCollapsed}>
          {
            this.state.collapsed ?
            <Icon type="up" />
            :
            <Icon type="down" />
          }
        </div>
      </div>
    );
  }

}

export default ThumbnailList;
