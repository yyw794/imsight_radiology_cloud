#!/bin/python
#coding=utf-8
from flask import make_response, request, jsonify, abort
from flask_login import current_user
import datetime, sys, os

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_find, db_update_one, db_insert_one
from models.response.response import response
from models.template.Template import Template, parseTemplateInfo, saveTemplatelist

required_key_delete_put = ['templateID']
required_key_put = ['templateID', 'templateName', 'imagingFind', 'diagnosisAdvice']
required_key_post = ['templateName', 'bodyPartExamined', 'modality', 'imagingFind', 'diagnosisAdvice']

def on_template():
    if not current_user.is_authenticated:
        abort(401)
        
    res = response()
    userID = current_user.get_id()

    if request.method == "GET":
        ret = []
        system_template = db_find_one("template", {"group": "system"})
        user_template = db_find_one("template", {"userID": userID})
        
        if system_template is not None:
            for sys_temp in system_template['templates']:
                template = parseTemplateInfo(sys_temp)
                sys_dict = template.to_dict()
                sys_dict['usergroup'] = 'SYSTEM'
                ret.append(sys_dict)

        if user_template is not None:
            for user_temp in user_template['templates']:
                template = parseTemplateInfo(user_temp)
                user_dict = template.to_dict()
                user_dict['usergroup'] = 'USER'
                ret.append(user_dict)

        res.statusCode = 1000
        res.data = ret
        return make_response(jsonify(res.to_dict()), 200)


    params = request.json
    if params is None:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)


    if request.method == "POST":
        if not all(key in params for key in required_key_post):
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        template = Template()
        template.bodyPartExamined = params['bodyPartExamined']
        template.templateName = params['templateName']
        template.modality = params['modality']
        template.imagingFind = params['imagingFind']
        template.diagnosisAdvice = params['diagnosisAdvice']

        user_template = db_find_one("template", {"userID": userID})
        if user_template is None:
            template.set_id(1, params['bodyPartExamined'], params['modality'])
            db_insert_one('template', saveTemplatelist([template], "user", userID))
        else:
            index = len(user_template['templates']) + 1
            template.set_id(index, params['bodyPartExamined'], params['modality'])
            user_template['templates'].append(template.to_db())
            db_update_one('template', {"userID": userID}, user_template)

        res.statusCode = 1000
        res.data = template.to_dict()
        return make_response(jsonify(res.to_dict()), 200)


    if not all(key in params for key in required_key_delete_put):
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    templateID = params['templateID']
    user_template = db_find_one("template", {"userID": userID})
    if user_template is None:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    user_templatelist = []
    for template_info in user_template['templates']:
        new_template = parseTemplateInfo(template_info)
        user_templatelist.append(new_template)
    template = filter(lambda t:t.id == templateID, user_templatelist)
    if not template:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)


    if request.method == "DELETE":
        user_templatelist.remove(template[0])

        db_update_one('template', {"userID": userID}, saveTemplatelist(user_templatelist, "user", userID))

        res.statusCode = 1000
        res.data = template[0].to_dict()
        return make_response(jsonify(res.to_dict()), 200)


    if request.method == "PUT":
        if not all(key in params for key in required_key_put):
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        template[0].templateName = params['templateName']
        template[0].imagingFind = params['imagingFind']
        template[0].diagnosisAdvice = params['diagnosisAdvice']

        db_update_one('template', {"userID": userID}, saveTemplatelist(user_templatelist, "user", userID))

        res.statusCode = 1000
        res.data = template[0].to_dict()
        return make_response(jsonify(res.to_dict()), 200)
