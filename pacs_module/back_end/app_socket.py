from flask import Flask
from flask_socketio import SocketIO, join_room, leave_room, emit
import os
from config import SOCKET_PORT, SOCKET_PROTOCOL

app = Flask(__name__)
app.secret_key = os.urandom(24)
socketio = SocketIO(app)
total_number = 0

@socketio.on('connect')
def client_connect():
    global total_number
    total_number += 1


@socketio.on('disconnect')
def client_disconnect():
    global total_number
    total_number -= 1

@socketio.on('studyListOpen')
def list_open(data):
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    join_room(room)
    ret_data = {"statusCode": 101, "message": "listOpen"}
    emit("ret_data", ret_data,room=room)

@socketio.on('studyListClose')
def on_leave(data):
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    leave_room(room)
    ret_data = {"statusCode": 102, "message": "close"}
    emit("ret_data", ret_data,room=room)

@socketio.on('UpdatestudyList')
def refresh_report(data):
    studyList = data['data']
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    ret_data = {"data": studyList, "statusCode": 100, "message": "OK"}
    emit('studyListUpdate', ret_data,room=room)


@socketio.on('open')
def on_join(data):
    print data
    taskID = data['taskID']
    userID = data['userID']
    room = userID + taskID
    join_room(room)
    print userID, taskID
    ret_data = {"statusCode": 101, "message": "open"}
    emit("ret_data", room=room)

@socketio.on('updateReport')
def refresh_report(data):
    report = data['data']
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    ret_data = {"data": report, "statusCode": 100, "message": "OK"}
    emit('reportUpdate', ret_data, room=room)

@socketio.on('updateLabel')
def refresh_report(data):
    labellist = data['data']
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    ret_data = {"data": labellist, "statusCode": 100, "message": "OK"}
    emit('labelUpdate', ret_data, room=room)

@socketio.on('close')
def on_leave(data):
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    leave_room(room)
    print userID, taskID
    ret_data = {"statusCode": 102, "message": "close"}
    emit("ret_data", room=room)

if __name__ == '__main__':
    if SOCKET_PROTOCOL == 'https':
        socketio.run(app, debug=True, host='0.0.0.0', port=SOCKET_PORT, certfile='/app/server.cert', keyfile='/app/server.key')
    elif SOCKET_PROTOCOL == 'http':
        socketio.run(app, debug=True, host='0.0.0.0', port=SOCKET_PORT)