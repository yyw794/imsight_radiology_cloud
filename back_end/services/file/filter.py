#!/bin/python
# coding=utf-8
import os
import sys

import SimpleITK as sitk

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from config import *


def get_body_part_examined(bodyPartExamined, protocolName, seriesDescription, studyDescription, imagecomments):
    if is_CT_DX_Chest(bodyPartExamined.upper(), protocolName.upper(), seriesDescription.upper(),
                      studyDescription.upper(), imagecomments.upper()):
        return 'CHEST'
    if is_CT_Liver(bodyPartExamined.upper(), protocolName.upper(), seriesDescription.upper(),
                   studyDescription.upper(), imagecomments.upper()):
        return 'LIVER'

    if '' == bodyPartExamined or 'UNKNOWN' == bodyPartExamined:
        if '' != protocolName:
            return protocolName
        elif '' != studyDescription:
            return studyDescription
        elif '' != seriesDescription:
            return seriesDescription
        elif '' != imagecomments:
            return imagecomments
        else:
            return ''
    else:
        return bodyPartExamined


def get_DX_PA_ViewPosition(viewposition, patientorientation, protocolName, seriesDescription, studyDescription):
    if is_DX_PA_ViewPosition(viewposition.upper(), patientorientation.upper(), protocolName.upper(),
                             seriesDescription.upper(), studyDescription.upper()):
        return 'PA'
    if viewposition == '':
        if patientorientation != '':
            return patientorientation
        elif protocolName != '':
            return protocolName
        elif studyDescription != '':
            return studyDescription
        elif seriesDescription != '':
            return seriesDescription
        else:
            return 'PA'
    else:
        return viewposition


def filter_list_in(filter_str, value):
    if '' == value:
        return False

    filter_list = filter_str.split('|')
    for filter_temp in filter_list:
        if filter_temp.upper() in value:
            return True
    return False


def is_CT_DX_Chest(bodyPartExamined, protocolName, seriesDescription, studyDescription, imagecomments):
    if filter_list_in(C_BODY_PART_EXAMINED, bodyPartExamined): return True
    if '' == bodyPartExamined or 'UNKNOWN' == bodyPartExamined:
        if filter_list_in(C_PROTOCOL_NAME, protocolName): return True
        if filter_list_in(C_STUDY_DESCRIPTION, studyDescription): return True
        if filter_list_in(C_SERIES_DESCRIPTION, seriesDescription): return True
        if filter_list_in(C_IMAGE_COMMENTS, imagecomments): return True
    return False


def is_CT_Liver(bodyPartExamined, protocolName, seriesDescription, studyDescription, imagecomments):
    if filter_list_in(L_BODY_PART_EXAMINED, bodyPartExamined):
        return True

    if '' == bodyPartExamined or 'UNKNOWN' == bodyPartExamined:
        if filter_list_in(L_PROTOCOL_NAME, protocolName):
            return True
        if filter_list_in(L_STUDY_DESCRIPTION, studyDescription):
            return True
        if filter_list_in(L_SERIES_DESCRIPTION, seriesDescription):
            return True
        if filter_list_in(L_IMAGE_COMMENTS, imagecomments):
            return True

    return False


def is_DX_PA_ViewPosition(viewposition, patientorientation, protocolName, seriesDescription, studyDescription):
    if filter_list_in(V_VIEW_POSITION, viewposition):
        return True
    if '' == viewposition:
        if filter_list_in(V_PATIENT_ORIENTATION, patientorientation):
            return True
        if filter_list_in(V_PROTOCOL_NAME, protocolName):
            return True
        if filter_list_in(V_STUDY_DESCRIPTION, studyDescription):
            return True
        if filter_list_in(V_SERIES_DESCRIPTION, seriesDescription):
            return True

    return False


def deal_a_dcm_file(file_name):
    image = sitk.ReadImage(file_name)
    all_keys = image.GetMetaDataKeys()
    bodyPartExamined = '' if '0018|0015' not in all_keys else image.GetMetaData('0018|0015').rstrip()
    studyDescription = '' if '0008|1030' not in all_keys else image.GetMetaData('0008|1030').rstrip()
    seriesDescription = '' if '0008|103E' not in all_keys else image.GetMetaData('0008|103E').rstrip().upper()
    protocolName = '' if '0018|1030' not in all_keys else image.GetMetaData('0018|1030').rstrip().upper()
    imagecomments = '' if '0020|4000' not in all_keys else image.GetMetaData('0020|4000').rstrip().upper()

    viewPosition = '' if '0018|5101' not in all_keys else image.GetMetaData('0018|5101').rstrip()
    patientOrientation = '' if '0020|0020' not in all_keys else image.GetMetaData('0020|0020').rstrip()
    ImageOrientationPatient = '' if '0020|0037' not in all_keys else image.GetMetaData('0020|0037').rstrip()

    print('bodyPartExamined:' + bodyPartExamined)
    print('studyDescription:' + studyDescription)
    print('seriesDescription:' + seriesDescription)
    print('protocolName:' + protocolName)
    print('imagecomments:' + imagecomments)
    print('viewPosition:' + viewPosition)
    print('patientOrientation:' + patientOrientation)
    print('ImageOrientationPatient:' + ImageOrientationPatient.decode("GBK"))
    print('')
    print('is_CT_DX_Chest:' + str(
        is_CT_DX_Chest(bodyPartExamined, protocolName, seriesDescription, studyDescription, imagecomments)))
    print('is_CT_Liver:' + str(
        is_CT_Liver(bodyPartExamined, protocolName, seriesDescription, studyDescription, imagecomments)))
    print('is_DX_PA_ViewPosition:' + str(
        is_DX_PA_ViewPosition(viewPosition, patientOrientation, protocolName, seriesDescription, studyDescription)))
    print(
    'bodyPartExamined:' + get_body_part_examined(bodyPartExamined, protocolName, seriesDescription, studyDescription,
                                                 imagecomments))
    print('viewPosition:' + get_DX_PA_ViewPosition(viewPosition, patientOrientation, protocolName, seriesDescription,
                                                   studyDescription))


if __name__ == "__main__":

    folder_path = '/home/imsight/下载/ai DICOM/all'

    child_list = os.listdir(folder_path)
    for child in child_list:
        child_path = os.path.join(folder_path, child)
        print("***************" + child_path)
        deal_a_dcm_file(child_path)
