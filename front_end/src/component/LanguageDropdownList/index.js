import React from 'react'
//import { instanceOf } from 'prop-types';
import { injectIntl } from 'react-intl'
import { Select } from 'antd';
//import { Cookies } from 'react-cookie';
//import ReactDOM from 'react-dom';
import enUS from 'antd/lib/locale-provider/en_US';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import './index.less'

//import { Cookies, withCookies } from 'react-cookie';
import { withCookies } from 'react-cookie';

/**
 * antd-design 组件
 * @type {React.ClassicComponentClass<OptionProps>}
 */
const Option = Select.Option;

/**
 * 下拉选择语言组件
 */
class LanguageDropdownList extends React.Component {
    /**
     * 参照生命周期构造函数
     * @param props
     */
  constructor(props) {
    super(props);
    // this.cookies = new Cookies()
  }

    /**
     * 参照生命周期构造函数
     */
  componentWillMount() {
  }

    /**
     * 切换选择语言组件
     * @param e
     */
  handleChange = (e) => {
    let langstr = e;  //e.target.value
    if(langstr == 'zh-cn') { 
      this.props.setAppState({ currentLocale: zhCN});
      console.log('setting locale: ', zhCN)
      this.props.intl.locale = 'zh-cn';
      this.props.cookies.set('lang', 'zh-cn') 
    }  //new Cookies().set('lang', 'zh_hans');
    else { 
      this.props.setAppState({ currentLocale: enUS});
      console.log('setting locale: ', enUS)
      this.props.intl.locale = 'en';
      this.props.cookies.set('lang', 'en') 
    }  //new Cookies().set('lang', 'en');
    
  }

    /**
     * 语言下拉选择组件
     * @returns {Component}
     */
  render() {
    return (
      <div className='language-dropdown-list'>
        <Select onChange={this.handleChange} style={{ width: '150px' }} value={this.props.intl.locale}>
          <Option value="en">English</Option>
          <Option value="zh-cn">中文（简体）</Option>
        </Select>
      </div>
    );
  }
}

export default injectIntl(withCookies(LanguageDropdownList))
