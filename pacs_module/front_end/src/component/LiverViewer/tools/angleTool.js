import {
  external,
  toolCoordinates,
  handleActivator,
  touchTool,
  drawTextBox,
  roundToDecimal,
  toolStyle,
  textStyle,
  toolColors,
  drawHandles,
  getToolState,
  addToolState,
} from 'cornerstone-tools';
import mouseButtonTool from '../../tools/mouseButtonTool'
import _ from 'underscore'

const toolType = 'angle';

let metaData = {
  currentHandle: null,
  currentTool: -1
}

// /////// BEGIN ACTIVE TOOL ///////
function addNewMeasurement(mouseEventData) {
  // Create the measurement data for this tool with the end handle activated
  const angleData = {
    visible: true,
    active: true,
    handles: {
      end1: {
        x: mouseEventData.currentPoints.image.x,
        y: mouseEventData.currentPoints.image.y,
        highlight: false,
        active: false
      },
      start: {
        x: mouseEventData.currentPoints.image.x,
        y: mouseEventData.currentPoints.image.y,
        highlight: true,
        active: true
      },
    },
  };

  addToolState(mouseEventData.element, toolType, angleData);
  metaData.currentTool = getToolState(mouseEventData.element, toolType).data.length - 1
  metaData.currentHandle = 'start'

  console.log('ADDING NEW MEASUREMENT!')
  return angleData;
}

function mouseMoveCallback(e) {
  const eventData = e.detail
  const toolData = getToolState(eventData.element, toolType)
  if (!toolData) {
    return
  }

  // mouse hover
  if (metaData.currentTool === -1) {
    toolCoordinates.setCoords(eventData);

    // We have tool data, search through all data
    // And see if we can activate a handle
    let imageNeedsUpdate = false;

    for (let i = 0; i < toolData.data.length; i++) {
      // Get the cursor position in canvas coordinates
      const coords = eventData.currentPoints.canvas;

      const data = toolData.data[i];

      if (handleActivator(eventData.element, data.handles, coords) === true) {
        imageNeedsUpdate = true;
      }

      if (!data.selected) {
        // if is selected, just keep it's active state
        if ((pointNearTool(eventData.element, data, coords) && !data.active) ||
            (!pointNearTool(eventData.element, data, coords) && data.active)) {
          data.active = !data.active;
          imageNeedsUpdate = true;
        }
      }
    }

    // Handle activation status changed, redraw the image
    if (imageNeedsUpdate === true) {
      external.cornerstone.updateImage(eventData.element);
    }
  } else {
    const data = toolData.data[metaData.currentTool]
    let activeHandle = data.handles[metaData.currentHandle]
    activeHandle.x = eventData.currentPoints.image.x
    activeHandle.y = eventData.currentPoints.image.y
    external.cornerstone.updateImage(eventData.element)
  }
}

function mouseClickCallback(e) {
  const eventData = e.detail
  const toolData = getToolState(eventData.element, toolType)
  const coords = eventData.startPoints.canvas

  if (metaData.currentTool === -1) {
    // if click on exist measurement
    // let mouseClickCallback in mouseButtonTool handle it
    if (toolData) {
      for (let i = 0; i < toolData.data.length; i++) {
        const data = toolData.data[i];
        if (pointNearTool(eventData.element, data, coords)) {
          return;
        }
      }
    }
    // add new measurement
    addNewMeasurement(eventData)
    return
  }
  // during adding
  const data = toolData.data[metaData.currentTool]
  console.log('click', data);
  if (metaData.currentHandle === 'start') {
    data.handles.start.highlight = false
    data.handles.start.active = false
    data.handles.end2 = {
      x: eventData.currentPoints.image.x,
      y: eventData.currentPoints.image.y,
      highlight: true,
      active: true
    }
    metaData.currentHandle = 'end2'
  } else if (metaData.currentHandle === 'end2') {
    data.handles.end2.highlight = false
    data.handles.end2.active = false
    metaData.currentTool = -1
    metaData.currentHandle = null
  }
  external.cornerstone.updateImage(eventData.element)
}
// /////// END ACTIVE TOOL ///////

function pointNearTool (element, data, coords) {
  const cornerstone = external.cornerstone;

  const lineSegment = {
    start: cornerstone.pixelToCanvas(element, data.handles.start),
    end: cornerstone.pixelToCanvas(element, data.handles.end1)
  };

  let distanceToPoint = external.cornerstoneMath.lineSegment.distanceToPoint(lineSegment, coords);

  if (distanceToPoint < 5) {
    return true;
  }

  if (!_.has(data.handles, 'end2')) {return false}

  lineSegment.start = cornerstone.pixelToCanvas(element, data.handles.start);
  lineSegment.end = cornerstone.pixelToCanvas(element, data.handles.end2);

  distanceToPoint = external.cornerstoneMath.lineSegment.distanceToPoint(lineSegment, coords);

  return (distanceToPoint < 5);
}

// /////// BEGIN IMAGE RENDERING ///////
function onImageRendered (e) {
  const eventData = e.detail;

  // If we have no toolData for this element, return immediately as there is nothing to do
  const toolData = getToolState(e.currentTarget, toolType);

  if (toolData === undefined) {
    return;
  }

  // We have tool data for this element - iterate over each one and draw it
  const context = eventData.canvasContext.canvas.getContext('2d');

  context.setTransform(1, 0, 0, 1, 0, 0);

  // Activation color
  let color;
  // const lineWidth = toolStyle.getToolWidth();
  const font = textStyle.getFont();
  const config = angle.getConfiguration();
  const cornerstone = external.cornerstone;

  for (let i = 0; i < toolData.data.length; i++) {
    context.save();

    // Configurable shadow
    if (config && config.shadow) {
      context.shadowColor = config.shadowColor || '#000000';
      context.shadowOffsetX = config.shadowOffsetX || 1;
      context.shadowOffsetY = config.shadowOffsetY || 1;
    }

    const data = toolData.data[i];

    // Differentiate the color of activation tool
    if (data.active) {
      color = toolColors.getActiveColor();
    } else {
      color = toolColors.getToolColor();
    }
    const lineWidth = data.selected ? 2 : 1

    // Draw the line
    context.beginPath();
    context.strokeStyle = color;
    context.lineWidth = lineWidth;

    let handleStartCanvas = cornerstone.pixelToCanvas(eventData.element, data.handles.end1);
    let handleEndCanvas = cornerstone.pixelToCanvas(eventData.element, data.handles.start);

    context.moveTo(handleStartCanvas.x, handleStartCanvas.y);
    context.lineTo(handleEndCanvas.x, handleEndCanvas.y);

    if (_.has(data.handles, 'end2')) {
      handleStartCanvas = cornerstone.pixelToCanvas(eventData.element, data.handles.start);
      handleEndCanvas = cornerstone.pixelToCanvas(eventData.element, data.handles.end2);

      context.moveTo(handleStartCanvas.x, handleStartCanvas.y);
      context.lineTo(handleEndCanvas.x, handleEndCanvas.y);
    }

    context.stroke();

    const handleOptions = {
      fill: data.selected ? color : false
    };
    // Draw the handles
    drawHandles(context, eventData, data.handles, color, handleOptions);

    if (_.has(data.handles, 'end2')) {
      // Draw the text
      context.fillStyle = color;

      // Need to work on correct angle to measure.  This is a cobb angle and we need to determine
      // Where lines cross to measure angle. For now it will show smallest angle.
      let columnPixelSpacing = eventData.image.columnPixelSpacing || 1
      let rowPixelSpacing = eventData.image.rowPixelSpacing || 1
      const dx1 = (Math.ceil(data.handles.end1.x) - Math.ceil(data.handles.start.x)) * columnPixelSpacing;
      const dy1 = (Math.ceil(data.handles.end1.y) - Math.ceil(data.handles.start.y)) * rowPixelSpacing;
      const dx2 = (Math.ceil(data.handles.end2.x) - Math.ceil(data.handles.start.x)) * columnPixelSpacing;
      const dy2 = (Math.ceil(data.handles.end2.y) - Math.ceil(data.handles.start.y)) * rowPixelSpacing;

      let angle = Math.acos(((dx1 * dx2) + (dy1 * dy2)) / (Math.sqrt((dx1 * dx1) + (dy1 * dy1)) * Math.sqrt((dx2 * dx2) + (dy2 * dy2))));

      angle *= (180 / Math.PI);

      // when end2 and start coincide, angle will be NaN due to zero division
      if (!isNaN(angle)) {
        const rAngle = roundToDecimal(angle, 2);
        const str = '00B0'; // Degrees symbol
        const text = rAngle.toString() + String.fromCharCode(parseInt(str, 16));
        // const text = '80' + String.fromCharCode(parseInt(str, 16))

        const textX = (handleStartCanvas.x + handleEndCanvas.x) / 2;
        const textY = (handleStartCanvas.y + handleEndCanvas.y) / 2;

        context.font = font;
        drawTextBox(context, text, textX, textY, color);
      }
    }

    context.restore();
  }
}
// /////// END IMAGE RENDERING ///////

// Module exports
const angle = mouseButtonTool({
  addNewMeasurement: () => {},
  onImageRendered,
  mouseClickCallback,
  mouseMoveCallback,
  pointNearTool,
  toolType
});

export {
  angle
};
