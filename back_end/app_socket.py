import os
import logging
import traceback

from logging.handlers import TimedRotatingFileHandler
from flask import Flask, request
from flask_socketio import SocketIO, join_room, leave_room, emit

from config import SOCKET_PORT, SOCKET_PROTOCOL, LOG_FOLDER

app = Flask(__name__)
app.secret_key = os.urandom(24)
socketio = SocketIO(app, async_mode="eventlet")
total_number = 0

if not os.path.exists(LOG_FOLDER):
    os.makedirs(LOG_FOLDER)
log_fmt = '%(asctime)s - %(levelname)s - %(filename)s - %(lineno)s - %(message)s'
formatter = logging.Formatter(log_fmt)
log_file_handler = TimedRotatingFileHandler(filename=LOG_FOLDER + '/socket-server.log', when='MIDNIGHT', interval=1,
                                            backupCount=15)
log_file_handler.setLevel(logging.DEBUG)
logger = logging.getLogger("wssocket")
logger.addHandler(log_file_handler)
logger.setLevel(logging.DEBUG)



@socketio.on('connect')
def client_connect():
    global total_number
    env = socketio.server.environ
    logger.debug("websocket client connect, remote_ip=%s", env[request.sid]["REMOTE_ADDR"])
    total_number += 1


@socketio.on('disconnect')
def client_disconnect():
    global total_number
    env = socketio.server.environ
    logger.debug("websocket client disconnect, remote_ip=%s", env[request.sid]["REMOTE_ADDR"])
    total_number -= 1


@socketio.on('studyListOpen')
def list_open(data):
    if "uid" in data:
        logger.debug("recv event uid=%s", data["uid"])
    logger.info("recv event studyListOpen")
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    join_room(room)
    logger.debug("join room .room=%s", room)
    ret_data = {"statusCode": 101, "message": "listOpen"}
    emit("ret_data", ret_data, room=room)


@socketio.on('studyListClose')
def on_leave(data):
    if "uid" in data:
        logger.debug("recv event uid=%s", data["uid"])
    logger.info("recv event studyListClose")
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    leave_room(room)
    ret_data = {"statusCode": 102, "message": "close"}
    logger.debug("leave room. room=%s", room)
    emit("ret_data", ret_data, room=room)


@socketio.on('UpdatestudyList')
def refresh_report(data):
    if "uid" in data:
        logger.debug("recv event uid=%s", data["uid"])
    logger.info("recv event UpdatestudyList")
    studyList = data['data']
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    ret_data = {"data": studyList, "statusCode": 100, "message": "OK"}
    emit('studyListUpdate', ret_data, room=room)


@socketio.on('open')
def on_join(data):
    if "uid" in data:
        logger.debug("recv event uid=%s", data["uid"])
    logger.info("recv event open")
    taskID = data['taskID']
    userID = data['userID']
    room = userID + taskID
    logger.debug("join room .room=%s", room)
    join_room(room)
    ret_data = {"statusCode": 101, "message": "open"}
    emit("ret_data", room=room)


@socketio.on('updateReport')
def refresh_report(data):
    if "uid" in data:
        logger.debug("recv event uid=%s", data["uid"])
    logger.info("recv event updateReport")
    report = data['data']
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    ret_data = {"data": report, "statusCode": 100, "message": "OK"}
    emit('reportUpdate', ret_data, room=room)


@socketio.on('updateLabel')
def refresh_report(data):
    if "uid" in data:
        logger.debug("recv event uid=%s", data["uid"])
    logger.info("recv event updateLabel")
    labellist = data['data']
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    ret_data = {"data": labellist, "statusCode": 100, "message": "OK"}
    emit('labelUpdate', ret_data, room=room)


@socketio.on('convertXY')
def conver_xy(data):
    if "uid" in data:
        logger.debug("recv event uid=%s", data["uid"])
    logger.info("recv event convertXY")
    study_info = data['study_info']
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    ret_data = {"data": study_info, "statusCode": 100, "message": "OK"}
    emit('xyConvert', ret_data, room=room)


@socketio.on('close')
def on_leave(data):
    if "uid" in data:
        logger.debug("recv event uid=%s", data["uid"])
    logger.info("recv event close")
    userID = data['userID']
    taskID = data['taskID']
    room = userID + taskID
    leave_room(room)
    logger.debug("leave room. room=%s", room)
    ret_data = {"statusCode": 102, "message": "close"}
    emit("ret_data", room=room)


@socketio.on_error_default
def default_error_handler(exc):
    logger.error("websocket handle error: %s", str(exc))
    logger.error(traceback.format_exc())


if __name__ == '__main__':
    if SOCKET_PROTOCOL == 'https':
        socketio.run(app, debug=True, host='0.0.0.0', port=int(SOCKET_PORT), certfile='/app/server.cert',
                     keyfile='/app/server.key')
    elif SOCKET_PROTOCOL == 'http':
        socketio.run(app, debug=True, host='0.0.0.0', port=int(SOCKET_PORT))
