import React, { Component } from 'react'
import {Modal, Button, Spin} from 'antd'
import { GetLocaledText } from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'

const AddNoduleModal = props => {
  return (
    <Modal
      title= {GetLocaledText({props},"AddNoduleModal.title")}
      visible={props.visible}
      onOk={props.handleOk}
      onCancel={props.handleCancel}
      style={{textAlign: 'center'}}
      footer={[
        <Button key="cancel" disabled={props.status === 'adding'} onClick={props.handleCancel}>
          <FormattedMessage id="AddNoduleModal.button.cancel"/>
        </Button>,
        <Button key="ok" type="primary" disabled={props.status === 'adding'} onClick={props.handleOk}>
          <FormattedMessage id="AddNoduleModal.button.confirm"/>
        </Button>,
      ]}
      >
        {
          props.status === 'confirm' ?
          <p style={{fontSize: '15px'}}>
            <FormattedMessage id="AddNoduleModal.content.confirmDetection"/>
          </p>
          :
          ''
        }
        {
          props.status === 'adding' ?
          <div>
            <Spin size="large" />
            <p><FormattedMessage id="AddNoduleModal.content.detecting"/></p>
          </div>
          :
          ''
        }
        {
          props.status === 'finished' ?
          <p style={{fontSize: '15px'}}>
            <FormattedMessage id="AddNoduleModal.content.finished"/>
          </p>
          :
          ''
        }
      </Modal>
  )
}

export default injectIntl(AddNoduleModal)
