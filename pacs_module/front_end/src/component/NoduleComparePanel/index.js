import React, { Component } from 'react';
import NoduleCompareTable from '../NoduleCompareTable'
import {getRiskText, getRiskColor, getNoduleType, POS_IMG, RISK_EN2CN, TYPE_EN2CN} from '../NodulePanel'
import {Table, Row, Col, Icon, Modal} from 'antd'
import _ from 'underscore'
import Immutable from 'immutable'

import '../NodulePanel/index.less'
import './index.less'
import {GetLocaledText} from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'

class NoduleComparePanel extends Component {
  constructor(props) {
    super(props)
    this.originData = Immutable.fromJS(this.props.data).sort(this.getSorter('nodule_prob', true))
    this.state = {
      compareData: null,
      data: this.originData,
      sortColumn: 'nodule_prob'
    }
    this.pageSize = window.screen.width < window.screen.height ? 7 : (window.screen.height-550)/68
  }
  onResize = () => {
    this.pageSize = window.screen.width < window.screen.height ? 7 : (window.screen.height-550)/68
  }
  componentDidMount() {
    window.addEventListener("resize", _.debounce(this.onResize, 100)) //Jackey: Why delay it by using _.debounce, but not simply passing this.onResize?
    //window.addEventListener("resize", this.onResize);
  }
  componentWillReceiveProps(nextProps) {
    this.originData = Immutable.fromJS(nextProps.data).sort(this.getSorter('nodule_prob', true))
    this.sortBy(this.state.sortColumn)
  }
  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   return
  //   this.originData = Immutable.fromJS(this.props.data).sort(this.getSorter('nodule_bm_prob', true))
  //   this.sortBy(this.state.sortColumn)
  // }

  getSorter(col, reverse=false) {
    return (a, b) => {
      let v1 = a.get(0).get(col)
      let v2 = b.get(0).get(col)
      if (v1 < v2) {
        return reverse ? 1 : -1
      }
      if (v1 > v2) {
        return reverse ? -1 : 1
      }
      if (v1 === v2) {
        return 0
      }
    }
  }

  sortBy(col) {
    let reverse = false
    if (['nodule_diameter', 'nodule_prob', 'nodule_avgHU'].includes(col)) {
      reverse = true
    }
    let data = this.originData.sort(this.getSorter(col, reverse))
    this.setState({
      data,
      sortColumn: col
    })
  }

  getPairPage(pair) {
    let page = 1
    let idx = this.state.data.findIndex(p => p.getIn([0, 'labelID']) === pair[0].labelID && p.getIn([1, 'labelID']) === pair[1].labelID)
    return Math.ceil((idx + 1) / this.pageSize)
  }

  selectPair(compareData) {
    let currentPage = this.getPairPage(compareData)
    this.setState({compareData, currentPage})
    _.isFunction(this.props.onSelectNodule) && this.props.onSelectNodule(compareData)
  }

  render() {
    const columns = [
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.index"),
        dataIndex: 'key',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              <p>{`H${record[0].key}`}</p>
              <p>{`N${record[1].key}`}</p>
            </div>
          )
        }
      },
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.diameter"),
        dataIndex: 'nodule_diameter',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              <p>{record[0].nodule_diameter.toFixed(1)}</p>
              <p>{record[1].nodule_diameter.toFixed(1)}</p>
            </div>
          )
        }
      },
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.malignancy"),
        dataIndex: 'nodule_bm',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              <p style={{color: getRiskColor(record[0].nodule_bm)}}>{RISK_EN2CN[getRiskText(record[0].nodule_bm)]}</p>
              <p style={{color: getRiskColor(record[1].nodule_bm)}}>{RISK_EN2CN[getRiskText(record[1].nodule_bm)]}</p>
            </div>
          )
        }
      },
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.subclass"),
        dataIndex: 'nodule_avgHU',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              <p>{TYPE_EN2CN[getNoduleType(record[0])]}</p>
              <p>{TYPE_EN2CN[getNoduleType(record[1])]}</p>
            </div>
          )
        }
      },
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.z"),
        dataIndex: 'instanceID',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              <p>{this.props.zReverse1?(this.props.maxZ1-record[0].coord.z+1):record[0].instanceID}</p>
              <p>{this.props.zReverse2?(this.props.maxZ2-record[1].coord.z+1):record[0].instanceID}</p>
            </div>
          )
        }
      },
      {
        title: GetLocaledText(this,"NoduleComparePanel.table-header.location"),
        dataIndex: 'nodule_location',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <div>
              <img src={POS_IMG[record[0].nodule_location]} className='location'/>
            </div>
          )
        }
      },
    ]

    return (
      <div className='nodule-panel nodule-compare-panel'>
        <div className='nodule-table'>
          <Table
            columns={columns}
            dataSource={this.state.data.toJS()}
            rowClassName={(record) => {
              const compareData = this.state.compareData
              return compareData && record[0].labelID === compareData[0].labelID ?
                'active' : ''
            }}
            size="small"
            pagination={{
              pageSize: this.pageSize,
              simple: true,
              showTotal: (total, range) => <span>共<span className='nodule-count'>{total}</span>个结节</span>,
              onChange: (currentPage) => this.setState({currentPage}),
              current: this.state.currentPage
            }}
            onRow={record => {
              return {
                onClick: () => {
                  this.setState({compareData: record})
                  _.isFunction(this.props.onSelectNodule) && this.props.onSelectNodule(record)
                }
              }
            }}/>
          {
            this.state.data.size > 0 ?
            <span className='ant-pagination-total-text'>共<span className='nodule-count'>{this.state.data.size}</span>条匹配</span>
            :
            ''
          }
        </div>
        <NoduleCompareTable data={this.state.compareData}/>
      </div>
    );
  }

}

export default injectIntl(NoduleComparePanel, {withRef: true});
