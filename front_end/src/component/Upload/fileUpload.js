import browserMD5File from 'browser-md5-file'
import _ from 'underscore'
import axios from 'axios'
import {config} from '../../api'

/**
 * 获取文件的md5加密后的文件
 * @param file
 * @return {Promise<any>}
 */
function getFileMD5(file) {
  return new Promise(function(resolve, reject) {
    browserMD5File(file, (err, md5) => {
      if (err) {
        reject(err)
      } else {
        resolve(md5)
      }
    })
  })
}

/**
 * 多文件上传函数
 * @param fileList 文件列表
 * @param onProgress 进度
 * @param onSuccess 上传成功
 * @param onFail  上传失败
 * @return {Promise<void>}
 */
export async function uploadFileList(fileList, onProgress, onSuccess, onFail) {
  let formData = new FormData()
  for (let file of fileList) {
    let md5 = await getFileMD5(file)
    formData.append('file[]', file, `${md5}.${_.last(file.name.split('.'))}`)
  }
  axios.post(config.upload[0], formData,
    {
      headers: {'Content-Type': 'multipart/form-data'},
      onUploadProgress: progressEvent => {
        let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
        //console.log(percentCompleted);	//!*!*!*! Original Console Log
        onProgress(percentCompleted)
      }
    }
  )
  .then(function (response) {
    if (response.data.statusCode !== 1000) {
      let err = new Error()
      err.response = response.data
      throw err
    }
    //handle success
    console.log(response.data);
    onSuccess(response.data.data)
  })
  .catch(function (err) {
    //handle error
    onFail(err)
  });
}
