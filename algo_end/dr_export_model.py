import numpy as np
import os.path
import tensorflow as tf
import densenet169_v0330

from keras import backend as K
from keras import Model
from keras.models import load_model

from tensorflow.python.ops import (
        control_flow_ops,
        variables,
        lookup_ops
)

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

tf.app.flags.DEFINE_string('output_dir', '/home/imsight/serving_models/dr_model',
                           """Directory where to export inference model.""")
tf.app.flags.DEFINE_integer('model_version', 2,
                            """Version number of the model.""")
tf.app.flags.DEFINE_string('keras_model', '/home/imsight/Projects/DR/weights/14+1labels_0330.h5',
                            """Path to DR keras model.""")
FLAGS = tf.app.flags.FLAGS

with tf.device('/gpu:0'):
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8, allow_growth=False)
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, allow_soft_placement=True))
    # Missing this was the source of one of the most challenging an insidious bugs that I've ever encountered.  
    # Without explicitly linking the session the weights for the dense layer added below don't get loaded
    # and so the model returns random results which vary with each model you upload because of random seeds.
    # K.set_session(sess)

    # Use this only for export of the model.  
    # This must come before the instantiation of ResNet50
    # K._LEARNING_PHASE = tf.constant(0)
    K.set_learning_phase(0)

    K.set_session(sess)
    # sess = K.get_session()

    # Transfer learning from ResNet50
    new_model = densenet169_v0330.DenseNet(classes=14)

    # The creation of a new model might be optional depending on the goal
    config = new_model.get_config()
    model = Model.from_config(config)
    model.load_weights(FLAGS.keras_model)
    print "load keras model done"

# Import the libraries needed for saving models
# Note that in some other tutorials these are framed as coming from tensorflow_serving_api which is no longer correct
from tensorflow.python.saved_model import builder as saved_model_builder
from tensorflow.python.saved_model import tag_constants, signature_constants, signature_def_utils_impl

# I want the full prediction tensor out, not classification. This format: {"image": Resnet50model.input} took me a while to track down

prediction_signature = tf.saved_model.signature_def_utils.predict_signature_def({"image": model.input}, {"disease_prob":model.output[0], "abnormal_prob":model.output[1]})

# export_path is a directory in which the model will be created

output_path = os.path.join(tf.compat.as_bytes(FLAGS.output_dir), tf.compat.as_bytes(str(FLAGS.model_version)))
builder = saved_model_builder.SavedModelBuilder(output_path)


main_op_new = control_flow_ops.group(
    lookup_ops.tables_initializer(),
    variables.local_variables_initializer(),
    #variables.global_variables_initializer()
)


# Initialize global variables and the model
# init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
# sess.run(init_op)

# Add the meta_graph and the variables to the builder
builder.add_meta_graph_and_variables(
      sess = sess, 
      tags = [tag_constants.SERVING],
      signature_def_map={
          "pred_dr" : prediction_signature
      },
      main_op=main_op_new
  )

# save the graph      
builder.save()
print "DR serving model save done\nmodel path: %s" % (output_path)