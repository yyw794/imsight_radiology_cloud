import {
  EVENTS,
  toolColors,
  addToolState,
  getToolState,
  clearToolState,
  setToolOptions,
  getToolOptions,
  isMouseButtonEnabled,
} from 'cornerstone-tools';
import * as cornerstone from 'cornerstone-core'
import {point} from 'cornerstone-math'
import _ from 'underscore'

const toolType = 'CT-noduleBox'
const BOX_SIZE = 40
let configuration = {}
let latestClickCoords = {x: 0, y: 0}

function mouseClickCallback(e) {
  const eventData = e.detail;
  const element = eventData.element;
  const options = getToolOptions(toolType, element);

  if (isMouseButtonEnabled(eventData.which, options.mouseButtonMask)) {
    if (_.isFunction(options.onClick)) {
      const coord = eventData.currentPoints.image
      const {columns, rows} = eventData.image
      const coordCanvas = eventData.currentPoints.canvas

      latestClickCoords = {x: Math.ceil((coord.x / eventData.image.rows) * 512), y: Math.ceil((coord.y / eventData.image.rows) * 512)}

      // rgba
      let idx = (Math.round(coord.x) + Math.round(coord.y) * eventData.image.columns) * 4
      let pixelData = eventData.image.getPixelData()

      // // WARNING: since it's a gray scale image stored in rgba, rgb value will be equal to gray scale
      let grayScale = pixelData[idx]

      let horizontalRatio = Math.min(Math.max(coord.x / columns, 0), 1)
      let verticalRatio = Math.min(Math.max(coord.y / rows, 0), 1)
      let clickedNodules = getClickedNodule(element, horizontalRatio, verticalRatio)
      options.onClick(horizontalRatio, verticalRatio, clickedNodules, grayScale, coord)
    }
  }

  cornerstone.updateImage(element)
}

function onImageRendered(e) {
  const eventData = e.detail;
  const element = eventData.element

  // console.log('print element', element )
  // console.log('printing selected nodules', element.getAttribute("selectedNodules"));

  const stack = getToolState(element, 'stack')
  if (stack === undefined) {
    return
  }

  const context = eventData.canvasContext.canvas.getContext('2d');
  let color = 'yellow'
  const options = getToolOptions(toolType, element)
  const nodules = options.getData()

  const axis = options.axis
  const depth = options.depth
  const {horizontalLayers, verticalLayers} = options

  context.save()
  context.setTransform(1, 0, 0, 1, 0, 0);
  const viewport = cornerstone.getViewport(element)

  const currentIdx = stack.data[0].currentImageIdIndex

  const currentSeriesNumber = options.getCurrentSeriesNumber()
  const currentActiveGridCell = options.getCurrentActiveGridCell()
  const currentActiveTools = options.getCurrentActiveTools()


  let insideNodule = false
  for (let i = 0; i < nodules.length; i++) {
    const nod = nodules[i]

    if (nod.seriesNumber == element.getAttribute('seriesnumber')) {
      if (currentIdx + 1 < nod.coord[axis] - depth / 2 || currentIdx + 1 > nod.coord[axis] + depth / 2) {
        continue
      }
      let coord = {x: 0, y: 0}
      // HACK: dont't know why the -1 here, but the box's position will not be accurate without it
      if (axis === 'z') {
        coord.x = (nod.coord.x - 1) / options.horizontalLayers
        coord.y = (nod.coord.y - 1) / options.verticalLayers
      }


      // color = 'yellow'

      // let insideNodules = getInsideNodules(element, options.getCurrentIdx())
      if (currentActiveTools.includes('referenceLines') && latestClickCoords) {
        let insideNodules = getInsideNodules(element, {x: latestClickCoords.x -1, y: latestClickCoords.y-1, z: options.getCurrentIdx().z-1})
        if (insideNodules.length && insideNodules[0].labelID === nod.labelID) {
          color = 'red'
          options.setSelectedNodules(insideNodules[0])
        } else {
          color = 'yellow'
        }
      } else {
        if (options.getSelectedNodules()) {
          let insideNodules = getInsideNodules(element, options.getSelectedNodules().coord)
  
          if (insideNodules.length && insideNodules[0].labelID === nod.labelID) {
            color = 'red'
          } else {
            color = 'yellow'
          }
        }
      }
      
      coord.x *= eventData.image.width
      coord.y *= eventData.image.height

      let line1 = {start: {x: 0, y: coord.y}, end: {x: eventData.image.width, y: coord.y}}
      let line2 = {start: {x: coord.x, y: 0}, end: {x: coord.x, y: eventData.image.height}}

      // const color = toolColors.getActiveColor();
      // const lineWidth = toolStyle.getToolWidth();
      // const color = 'yellow'
      const lineWidth = 1

      // if (currentActiveTools.includes('referenceLines')) {
      //   const intersectionCanvas = cornerstone.pixelToCanvas(eventData.element, coord);
      //   if (line1.start.y !== line1.end.y) {
      //     let tmp = line1
      //     line1 = line2
      //     line2 = tmp
      //   }
      //   const start1Canvas = cornerstone.pixelToCanvas(eventData.element, line1.start);
      //   const end1Canvas = cornerstone.pixelToCanvas(eventData.element, line1.end);
      //   const start2Canvas = cornerstone.pixelToCanvas(eventData.element, line2.start);
      //   const end2Canvas = cornerstone.pixelToCanvas(eventData.element, line2.end);
      //   const viewport = cornerstone.getViewport(eventData.element)
      //   const size = BOX_SIZE * viewport.scale
      //   // Draw the referenceLines
      //   context.setTransform(1, 0, 0, 1, 0, 0);

      //   context.save();
      //   context.beginPath();
      //   context.strokeStyle = color;
      //   context.lineWidth = lineWidth;
      //   context.setLineDash([5, 2])

      //   context.moveTo(start1Canvas.x, start1Canvas.y);
      //   context.lineTo(Math.max(intersectionCanvas.x - size / 2, start1Canvas.x), start1Canvas.y);
      //   context.moveTo(intersectionCanvas.x + size / 2, start1Canvas.y);
      //   context.lineTo(end1Canvas.x, end1Canvas.y);

      //   context.moveTo(start2Canvas.x, start2Canvas.y);
      //   context.lineTo(start2Canvas.x, Math.max(intersectionCanvas.y - size / 2, start2Canvas.y));
      //   context.moveTo(start2Canvas.x, intersectionCanvas.y + size / 2);
      //   context.lineTo(end2Canvas.x, end2Canvas.y);
      //   context.stroke();
        
      //   context.beginPath()
      //   context.fillStyle = color
      //   context.arc(intersectionCanvas.x, intersectionCanvas.y, 1, 0, 2 * Math.PI, true);
      //   context.fill();
      //   context.stroke();
      //   // context.rect(intersectionCanvas.x /* - size / 2 */, intersectionCanvas.y /* - size / 2 */, size, size)
      //   // context.stroke();

      //   context.beginPath();
      //   // context.lineWidth="6";
      //   // context.strokeStyle="red";
      //   context.rect(intersectionCanvas.x - size / 2, intersectionCanvas.y - size / 2,size,size);
    
      //   context.stroke();

      //   context.restore();
      // } else {
        context.beginPath();
        context.strokeStyle = color;
        context.setLineDash([5, 2])
        context.lineWidth = 1;
        // context.rect(x - BOX_SIZE, y - BOX_SIZE, 40, 40);
        coord = cornerstone.pixelToCanvas(eventData.element, coord)
        const size = BOX_SIZE * viewport.scale
        context.rect(coord.x - size / 2, coord.y - size / 2, size, size);
        context.stroke();
      // }
    }
  }

  context.restore()
}

function getClickedNodule(element, horizontalRatio, verticalRatio) {
  const options = getToolOptions(toolType, element)

  const {
    horizontalLayers, verticalLayers, mainAxisLayers,
    axis, getData, getCurrentIdx
  } = options
  const nodules = getData()
  const currentIdx = getCurrentIdx()

  const imgWidth = cornerstone.getImage(element).width
  const imgHeight = cornerstone.getImage(element).height

  let currentX
  let currentY
  let currentZ

  if (axis === 'x') {
    currentX = currentIdx.x
    currentY = Math.floor(horizontalRatio * horizontalLayers)
    currentZ = Math.floor((1 - verticalRatio) * verticalLayers)
  } else if (axis === 'y') {
    currentX = Math.floor(horizontalRatio * horizontalLayers)
    currentY = currentIdx.y
    currentZ = Math.floor((1 - verticalRatio) * verticalLayers)
  } else if (axis === 'z') {
    currentX = Math.floor(horizontalRatio * horizontalLayers)
    currentY = Math.floor(verticalRatio * verticalLayers)
    currentZ = currentIdx.z
  }

  return getInsideNodules(element, {x: currentX, y: currentY, z: currentZ})
}

function getInsideNodules(element, currentIdx) {
  const options = getToolOptions(toolType, element)

  const {
    horizontalLayers, verticalLayers, mainAxisLayers,
    axis, getData, getCurrentIdx
  } = options
  const nodules = getData()
  // const currentIdx = getCurrentIdx()
  const imgWidth = cornerstone.getImage(element).width
  const imgHeight = cornerstone.getImage(element).height


  let xDepth
  let yDepth
  let zDepth
  if (axis === 'x') {
    xDepth = Math.round(BOX_SIZE / imgWidth * mainAxisLayers)
    yDepth = Math.round(BOX_SIZE / imgWidth * horizontalLayers)
    zDepth = Math.round(BOX_SIZE / imgHeight * verticalLayers)
  } else if (axis === 'y') {
    xDepth = Math.round(BOX_SIZE / imgWidth * horizontalLayers)
    yDepth = Math.round(BOX_SIZE / imgWidth * mainAxisLayers)
    zDepth = Math.round(BOX_SIZE / imgHeight * verticalLayers)
  } else if (axis === 'z') {
    xDepth = Math.round(BOX_SIZE / imgWidth * horizontalLayers)
    yDepth = Math.round(BOX_SIZE / imgWidth * verticalLayers)
    zDepth = Math.round(BOX_SIZE / imgHeight * mainAxisLayers)
  }
  let candidates = []
  nodules.forEach(nodule => {
    let {z,y,x} = nodule.coord
    // let {z} = nodule.coord

    if (currentIdx.x + 1 >= x - xDepth / 2 && currentIdx.x + 1 <= x + xDepth / 2
      && currentIdx.y + 1 >= y - yDepth / 2 && currentIdx.y + 1 <= y + yDepth / 2
      && currentIdx.z + 1 >= z - zDepth / 2 && currentIdx.z + 1 <= z + zDepth / 2) {
      candidates.push(nodule)
    }
  })
  // if the number of candidates is more than one, pick the nearest one
  candidates.sort(nodule => {
    return Math.pow(nodule.x - currentIdx.x - 1, 2) + Math.pow(nodule.y - currentIdx.y - 1, 2) + Math.pow(nodule.z - currentIdx.z - 1, 2)
  })
  return candidates
}

function onNewImage(e) {
  // const eventData = e.detail;
  // const element = eventData.element
  // const options = getToolOptions(toolType, element)
  // clearToolState(element, toolType)
  // for (let data of options.nodules) {
  //   addToolState(element, toolType, Object.assign({}, data))
  // }
}

export default {
  activate (element, mouseButtonMask) {
    let options = getToolOptions(toolType, element)
    options = Object.assign({}, options, {mouseButtonMask})
    // options.mouseButtonMask = mouseButtonMask;
    setToolOptions(toolType, element, options);

    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
    element.addEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  disable (element) {
    element.removeEventListener(EVENTS.IMAGE_RENDERED, onImageRendered)
    element.removeEventListener('cornerstonenewimage', onNewImage)
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  enable (element, options) {
    setToolOptions(toolType, element, options);
    element.addEventListener(EVENTS.IMAGE_RENDERED, onImageRendered)
    element.addEventListener('cornerstonenewimage', onNewImage)
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  deactivate (element) {
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  getConfiguration () {
    return configuration;
  },
  setConfiguration (config) {
    configuration = config;
  },
  resetLatestClickCoords () {
    latestClickCoords = null
  },
  BOX_SIZE
}
