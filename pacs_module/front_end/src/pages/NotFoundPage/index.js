import React, { Component } from 'react'
import {Layout} from 'antd'
import {Link} from 'react-router-dom'
const {Content} = Layout
import {FormattedMessage, injectIntl} from 'react-intl'

import './index.less'

const NotFound = () => (
  <div className='page' id='not-found-page'>
    <Content className='content'>
      <img src={require('../../../static/images/404.png')}/>
      <h2><FormattedMessage id='NotFoundPage.content.notfound'/></h2>
      <Link to='/'><FormattedMessage id='NotFoundPage.content.backToIndex'/></Link>
    </Content>
  </div>
)

export default NotFound
