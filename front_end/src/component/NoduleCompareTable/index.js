import React, { Component } from 'react';
import {getRiskText, getRiskColor, getNoduleType, RISK_EN2CN, TYPE_EN2CN} from '../NodulePanel'
import './index.less'
import {FormattedMessage, injectIntl} from 'react-intl'

/**
 *节点风险比较函数
 * @param nLeft 左节恶性结风险
 * @param nRight 左节结恶性风险
 * @returns {boolean}
 */
function isLeftNoduleRiskBigger(nLeft,nRight)
{
  let left = -1
  let right =-1
  if(getRiskText(nLeft) === "High") left = 3
  if(getRiskText(nLeft) === "Middle") left = 2
  if(getRiskText(nLeft) === "Low") left = 1
  if(getRiskText(nLeft) === "Benign") left = 0
  
  if(getRiskText(nRight) === "High") right = 3
  if(getRiskText(nRight) === "Middle") right = 2
  if(getRiskText(nRight) === "Low") right = 1
  if(getRiskText(nRight) === "Benign") right = 0
  return left > right
}

/**
 * 节点对比详情表格组件
 */
class NoduleCompareTable extends Component {
    /**
     * 风险对比图标显示
     * @param data
     * @returns {string}
     */
  renderMalgTrend(data) {
    if (data[0].nodule_bm.Malignant === data[1].nodule_bm.Malignant&&data[0].nodule_bm.Benign === data[1].nodule_bm.Benign&&data[0].nodule_bm.Mainly_Benign === data[1].nodule_bm.Mainly_Benign &&data[0].nodule_bm.Mainly_Malignant === data[1].nodule_bm.Mainly_Malignant) {
      return "-"
    } else if ( isLeftNoduleRiskBigger(data[0].nodule_bm,data[1].nodule_bm)) {
      return <img className='malg-arrow' src={require('../../../static/images/arrow_increase.png')}/>
    } else {
      return <img className='malg-arrow' src={require('../../../static/images/arrow_reduce.png')}/>
    }
  }

    /**
     * 随访对比展示table组件
     * @returns {Component}
     */
  render() {
    const data = this.props.data
    if (!data) {
      return ''
    }

    // const {nodule_diameter, nodule_malg, avgHU, coord, vol} = this.props.data
    return (
        data[1]["+/-"]=="+"||data[1]["+/-"]=="-"?"":
      <table className='nodule-compare-table' >
        <thead>
          <tr>
            <th></th>
            <th><FormattedMessage id="NoduleComparePanel.nodule-compare-table.followUp1"/></th>
            <th><FormattedMessage id="NoduleComparePanel.nodule-compare-table.followUp2"/></th>
            <th><FormattedMessage id="NoduleComparePanel.nodule-compare-table.variant"/></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.diameter"/></td>
            <td>{data[0].nodule_diameter.toFixed(1)}</td>
            <td>{data[1].nodule_diameter.toFixed(1)}</td>
            <td>{Math.round((data[1].nodule_diameter/data[0].nodule_diameter - 1) * 100)}%</td>
          </tr>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.malignancy"/></td>
            <td style={{color: getRiskColor(data[0].nodule_bm)}}>{RISK_EN2CN[getRiskText(data[0].nodule_bm)]}</td>
            <td style={{color: getRiskColor(data[1].nodule_bm)}}>{RISK_EN2CN[getRiskText(data[1].nodule_bm)]}</td>
            <td className='malg'>
              {this.renderMalgTrend(data)}
            </td>
          </tr>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.subclass"/></td>
            <td>{TYPE_EN2CN[getNoduleType(data[0])]}</td>
            <td>{TYPE_EN2CN[getNoduleType(data[1])]}</td>
            <td>-</td>
          </tr>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.location"/></td>
            <td>{`[${data[0].coord.x}, ${data[0].coord.y}, ${data[0].coord.z}]`}</td>
            <td>{`[${data[1].coord.x}, ${data[1].coord.y}, ${data[1].coord.z}]`}</td>
            <td>-</td>
          </tr>
          <tr>
            <td style={{width:"110px"}}><FormattedMessage id="NoduleComparePanel.nodule-compare-table.volume"/></td>
            <td>{data[0].nodule_volume.toFixed(1)}</td>
            <td>{data[1].nodule_volume.toFixed(1)}</td>
            <td>{Math.round((data[1].nodule_volume/data[0].nodule_volume - 1) * 100)}%</td>
          </tr>
        </tbody>
      </table>
    );
  }

}

export default NoduleCompareTable;
