import * as cornerstone from 'cornerstone-core'
import * as cornerstoneTools from "cornerstone-tools"
import _ from 'underscore'
import {clip} from '../../util'

function preloadStack(stackData, start, end) {
  for (let id of stackData.imageIds.slice(start, end)) {
    cornerstone.loadAndCacheImage(id)
  }
}

function onNewImage(e) {
  const eventData = e.detail;
  const element = eventData.element
  const stackData = cornerstoneTools.getToolState(element, 'stack')
  if (!stackData) return
  let currentIdx = stackData.data[0].currentImageIdIndex
  let length = stackData.data[0].imageIds.length
  console.log('preload stack data')
  preloadStack(stackData.data[0], clip(currentIdx - 10, 0, length - 1), clip(currentIdx + 10, 0, length - 1))
}

onNewImage = _.debounce(onNewImage, 500)

export default {
  enable(elem) {
    elem.addEventListener('cornerstonenewimage', onNewImage)
  },

  disable(elem) {
    elem.removeEventListener('cornerstonenewimage', onNewImage)
  },
}
