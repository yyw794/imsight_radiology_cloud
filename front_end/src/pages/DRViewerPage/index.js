import React, { Component } from 'react'
import DRViewer from '../../component/DRViewer'
import api from '../../api'
import LoadingWrapper from '../../component/LoadingWrapper'
import ToolBar from '../../component/DRToolBar'
import ThumbnailList from '../../component/ThumbnailList'
import axios from 'axios'
import { Card, Layout, message } from 'antd'
import {addDRMetaData} from '../../metaData.js'
import {getUser, getUserRole} from '../../auth'
import _ from 'underscore'

import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {EMPTYSTUDYDR,EMPTYTOOLS,EMPTYDRLABELS} from '../../component/DefaultData'
import './index.less'
import {URL_PREFIX,DICOM_URL_PREFIX} from "../../component/tools/config";

const { Header, Content, Footer } = Layout;

class DRViewerPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTool: ['wwwc',"pan"],
      study: props.data,
      labels: props.data.labels,
      seriesID: props.data.seriesID,
      instanceID: props.data.instanceID,
      diseaseList:props.data.diseaseList,
      disease_state:props.data.disease_state,
      tools: props.data.tools.data,
      displayInstance: this.props.data.instances[0],
    }
   this.cancelDisease=this.cancelDisease.bind(this);
    this.getDiseease=this.getDiseease.bind(this);
  }

  componentDidMount() {
    // this.setState({});
      document.addEventListener("keydown",this.onKeyDown)
     // this.viewer.getWrappedInstance().enableTool("wwwc")
  }

  componentWillUnmount() {
    // this.setState({});
      document.removeEventListener("keydown",this.onKeyDown)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.id.replace(/-/g, '.') !== this.state.study.studyUID) {
      console.log('DRViewerPage reload data')
      getData(nextProps).then(data => {
        this.setState({
          study: data,
          labels: data.labels,
          tools: data.tools
        })
        this.exitToolMode()
      })
    }
  }

  async saveScreenshotToReport() {
    let image = await this.viewer.getWrappedInstance().getScreenshot()
    var data = new FormData()
    data.append('file', image, 'image.png')
    data.append('taskID',this.state.study.taskID)
    var config = {
      onUploadProgress: function(progressEvent) {
        var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total )
      }
    }
    return axios.post(`/api/upload_screenshot`, data, config)
      .then((res) => {
        message.success(GetLocaledText(this,"DRViewerPage.dialog.insertSuccess"))
        // message.success(GetLocaledText(this, "DRViewer.screenshot.success"))
      })
      .catch((err) => {
        message.success(GetLocaledText(this,"DRViewerPage.dialog.insertFail"))
        // message.error(GetLocaledText(this, "DRViewer.screenshot.failure"))
      })
  }

  exitToolMode() {
      let activeTool = this.state.activeTool
      if(activeTool.indexOf("pan")!==-1){return}
      for(let i=0;i<activeTool.length;i++){
          if(activeTool[i]!=="hideBoxes") {
              this.viewer.getWrappedInstance().deactivateTool(activeTool[i], 1)
          }
      }

      this.viewer.getWrappedInstance().deactivateTool('pan', 2)
      this.viewer.getWrappedInstance().activateTool('wwwc', 1)
      this.viewer.getWrappedInstance().activateTool('pan', 2)
      if(this.state.activeTool.indexOf("hideBoxes")!==-1){
          this.setState({activeTool:["wwwc","pan","hideBoxes"]})
      }else{
          this.setState({activeTool:["wwwc","pan"]})
      }
  }

  onKeyDown = (e) => {

    if (e.key === 'Escape') {
      this.viewer.getWrappedInstance().resetViewport()
      return this.exitToolMode()
    }
    if (!!window.ActiveXObject || "ActiveXObject" in window) {
        if (e.key === 'Esc') {
            this.viewer.getWrappedInstance().resetViewport()
            return this.exitToolMode()
        }
    }
  }

  onToolClick(name) {
    if (name === 'invert') {
      // this.setState({activeTool: null})
      return this.viewer.getWrappedInstance().toggleInvert()
    }
    if(name==='eraser'){
        this.exitToolMode();
        return this.viewer.getWrappedInstance().clearTool()
    }
    if (name === 'screenshot') {
      // this.setState({activeTool: null})
      return this.saveScreenshotToReport()
    }
    if (name === 'recover') {
        //this.viewer.getWrappedInstance().toggleInvert()
      return this.viewer.getWrappedInstance().resetViewport()
    }
    if(name === 'report'){
      return  window.open("/report/"+JSON.parse(localStorage.getItem("fellowParams")).studyUID.split(".").join("-"));
    }
           if (this.state.activeTool.indexOf(name)!==-1) {
               this.exitToolMode()
           } else {
               for(let i=0;i<this.state.activeTool.length;i++){
                  if(this.state.activeTool[i]!=="hideBoxes"){
                     this.viewer.getWrappedInstance().deactivateTool(this.state.activeTool[i], 1)
                  }
               }
               this.viewer.getWrappedInstance().activateTool(name, 1)
               this.viewer.getWrappedInstance().activateTool('pan', 2) // use scroll button on mouse to pan
               if(this.state.activeTool.indexOf("hideBoxes")!==-1){
                   this.setState({activeTool: [name,'wwwc',"hideBoxes"]})
               }else{
                   this.setState({activeTool: [name,'wwwc']})
               }
           }
  }
    onToolClickShow(name){
        if(this.state.activeTool.indexOf("pan")!==-1){
            this.viewer.getWrappedInstance().disableTool("lesionsMarker",1)
            this.setState({activeTool: ["hideBoxes",'wwwc']})
        }else{
            this.viewer.getWrappedInstance().enableTool(name)
            this.setState({activeTool: ['wwwc','pan']})
        }
        // let activeTool = this.state.activeTool;
        // if(activeTool!=="hideBoxes"){
        //     this.setState({activeTool:name})
        //     return this.viewer.getWrappedInstance().deactivateTool(activeTool, 1)
        // }
        // if(activeTool=="hideBoxes"){
        //     this.viewer.getWrappedInstance().disableTool("lesionsMarker")
        //     this.setState({activeTool: 'pan'})
        // }else{
        //     this.viewer.getWrappedInstance().enableTool("lesionsMarker")
        //     this.setState({activeTool: name})
        // }
        // if(activeTool===name){
        //     this.viewer.getWrappedInstance().activateTool('pan', 2)
        //     this.viewer.getWrappedInstance().enableTool("lesionsMarker")
        //     this.setState({activeTool: 'pan'})
        // }else{
        //     this.viewer.getWrappedInstance().disableTool("lesionsMarker")
        //     this.setState({activeTool: name})
        // }


   }

    /**
     * 获取
     */
    async getDiseease(){
       let obj=await api.getDisease({
            taskID: this.state.study.taskID,
            seriesID: this.state.seriesID,
            instanceID: this.state.instanceID
        });
       let diseaseList=obj.disease;
        this.setState({
            diseaseList:Array.from(new Set(diseaseList)).filter(function (item) {
                if(item){
                    return item;
                }
            }),
            disease_state:obj.disease_state
        })
    }
    /**
     * 删除Disease
     * @return {boolean}
     */
    cancelDisease(data){
      let that=this;
      this.state.diseaseList.map(function (item,index) {
          if(item==data){
              that.state.diseaseList.splice(index,1)
          }
      })
      that.setState({
           diseaseList: that.state.diseaseList
       },function () {
          api.saveDisease({
              taskID:this.state.study.taskID,
              disease:that.state.diseaseList,
          }).then(function () {
              message.success("删除成功");
              if(that.state.diseaseList.length==0){
                  that.viewer.getWrappedInstance().clearLabels()
              }
          }).catch((err)=>{
              message.error("API error, getToolsData or getLabels",600)
              throw err
          })
      })
    }
  isForbidEdit() {
    const study = this.state.study
    const report = this.state.study.report
    if (!report) {return true}
    return (
      report.examined
      || (report.submitted && study.user_privilege.includes('reporter'))
      || (!report.submitted && study.user_privilege.includes('reviewer'))
    )
  }

  /**
   * display another instance
   * @param  {json} instance [instance data]
   * @return {undefined}
   */
  onInstanceClick = (instance) => {
    if (instance.instanceID === this.state.displayInstance.instanceID) {
      return
    }
    let {seriesID, instanceID} = instance
    let {taskID, studyUID} = this.state.study
    this.setState({
          seriesID:seriesID,
          instanceID:instanceID
    })
    Promise.all([
      this.viewer.getWrappedInstance().saveToolsData(),
      this.viewer.getWrappedInstance().saveLabelsData()
    ])
    .then(()=>{
      return Promise.all([
        api.getLabels({
          taskID,
          labelType: this.state.study.modality=="CT"?"Nodule":"DX",
          seriesID,
          instanceID
        }),
        api.getToolsData({
          studyUID,
          seriesID,
          instanceID
        })
      ])
    })
    .then(resp => {
      let [labels, tools] = resp
      tools = tools.data
      this.setState({
        displayInstance: instance,
        tools,
        labels
      })
      this.exitToolMode()
    }).catch((err)=>{
      message.error("API error, getToolsData or getLabels",600)
      throw err
    })
  }

  render() {
    let {age, gender, patientName, studyDate, studyTime} = this.state.study
    let user = getUser()
    let userID = ''
    if (user) {
      userID = user.account
    }
    const studyUID = this.props.match.params.id.replace(/-/g, '.');
    return (
      <div style={{height: '100%', backgroundColor: '#000'}} id='dr-viewer-page'>
        <ToolBar
          onToolClick={this.onToolClick.bind(this)}
          onToolClickShow={this.onToolClickShow.bind(this)}
          activeTool={this.state.activeTool}
          forbidEdit={this.isForbidEdit()}
          forbidScreenshot={this.state.mode === 'label'}/>
        <DRViewer
          ref={input => {this.viewer = input}}
          study={this.state.study}
          diseaseList={this.state.diseaseList}
          disease_state={this.state.disease_state}
          instance={this.state.displayInstance}
          tools={this.state.tools}
          getDiseease={this.getDiseease}
          cancelDisease={this.cancelDisease}
          labels={this.state.labels}
          onLabelsChange={labels => this.setState({labels})}
          onToolsChange={tools=>this.setState({tools})}
          style={{height: '100%', position: 'relative'}}/>
        {
          this.props.data.instances.length > 1 ?
          <ThumbnailList
            data={this.props.data.instances.map(ins => {
              return Object.assign({id: ins.instanceID}, ins)
            })}
            activeIds={[this.state.displayInstance.instanceID]}
            renderItem={(ins, i) => (
              <div>
                <img src={ins.pngurl}/>
                <p className='info'>{ins.viewPosition}</p>
              </div>
            )}
            onItemClick={this.onInstanceClick}/>
          :
          ''
        }
      </div>
    )
  }

}

async function getData(props) {
  let studyErr = false
  let labelsErr = false
  let toolsErr = false
  const user = getUser()
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let study = await api.getStudyDetail({studyUID})
  .catch((err)=>{
    message.error("API error, getStudyDetail",60)
    studyErr = true
  })

  Object.values(study.series).forEach(series=>{
    series.instances.map(ins=>ins.url = (ins.url.search('.dcm')>0?DICOM_URL_PREFIX:URL_PREFIX) + ins.url)
  })

  if(studyErr){study = EMPTYSTUDYDR}
  
  let defaultSeries = Object.values(study.series)[0]
  let labels = await api.getLabels({
    taskID: study.taskID,
    labelType:study.modality=="CT"?"Nodule":"DX",
    seriesID: defaultSeries.seriesID,
    instanceID: defaultSeries.instances[0].instanceID
  }).catch((err)=>{
    message.error("API error, getLabels",60)
    labelsErr = true
  })
  let obj=await api.getDisease({
        taskID: study.taskID,
        seriesID: defaultSeries.seriesID,
        instanceID: defaultSeries.instances[0].instanceID
  })
 let diseaseList=obj.disease;
  let disease_state=obj.disease_state;
  if(labelsErr){labels = EMPTYDRLABELS}
  
  let tools = await api.getToolsData({
    studyUID,
    seriesID: defaultSeries.seriesID,
    instanceID: defaultSeries.instances[0].instanceID
  }).catch((err)=>{
    message.error("API error, getToolsData",60)
    toolsErr = true
  })

  if(toolsErr){tools = EMPTYTOOLS}

  let resps = [study, labels, tools]

  let studyData = resps[0]
  addDRMetaData(studyData)
  studyData.diseaseList= Array.from(new Set(diseaseList)).filter(function (item) {
     if(item){
         return item;
     }
  });
  studyData.disease_state=disease_state;
  studyData.labels = resps[1]
  studyData.tools = resps[2]
  studyData.seriesID=defaultSeries.seriesID;
  studyData.instanceID=defaultSeries.instances[0].instanceID;
  // studyData.report = resps[3]
  let report = await api.getReport({taskID: studyData.taskID}).catch((err)=>{message.error("API error, getReport",600);throw err})
  // if (user.type !== 'label' && (studyData.submitted || user.usergroup !== 'examiner')) {
  //     report = await api.getReport({taskID: studyData.taskID})
  // }
  studyData.report = report
  for(let series of Object.values(studyData.series)) {
    series.instances.forEach(ins => ins.seriesID = series.seriesID)
  }
  studyData.instances = _.flatten(Object.values(studyData.series).map(s => s.instances))
  return await studyData
}

DRViewerPage = LoadingWrapper(DRViewerPage, getData)

export default injectIntl(DRViewerPage)
