import React, { Component } from 'react'
import {Prompt} from 'react-router-dom'
import { Modal, Input, Layout, Menu, Alert, message } from 'antd'
const { Header } = Layout
import LoadingWrapper from '../../component/LoadingWrapper'
import SignoutButton from '../../component/SignoutButton'
import Logo from '../../component/Logo'
import Report from '../../component/ReportLiver'
import api from '../../api'
import Immutable from 'immutable'
import {getUser} from '../../auth'
import moment from 'moment'
import io from 'socket.io-client'
import domtoimage from 'dom-to-image';
import jsPDF from 'jspdf'


import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './index.less'

class ReportPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      report: Immutable.fromJS(Object.assign({
        patientName: '',
        gender: '',
        age: '',
        patientID: '',
        modality: '',
        studyDate: '',
        imagingFind: '',
        diagnosisAdvice: '',
        updateDate: '',
        urls: [],
        observations: {},
        imagingFind: '',
        diagnosisAdvice: '',
        procedure: '',
        indication: '',
        comparison: '',
        technique: '',
        meetsLirads: false,
        isCompromisedBy: '',
        intrContrAgent: '',
        volContrAgent: '',
        rateContrAgent: '',
        premedAdvEvents: '',
        hepaticFindings: '',
        extrahepaticFindingsImpr: '',
        liverGrossObs: '',
        hepaticVasc: '',
        biliarySys: '',
        extrahepaticFindings: '',
        submitted: false,
        examined: false,
      }, this.props.data.report)), // immutable
      templateList: this.props.data.template,
      labels: Object.assign(this.props.data.labels),
      template: {},
      modalVisible: false,
      newTemplateName: '', // save new template as this name
      unsave: false
    }
    this.allTemplate = this.props.data.template // store original template list
    this.currentUser = getUser()

  }

  componentDidMount() {
    this.socket = io(window.location.origin, {transports: ['websocket']})
    this.socket.connect()
    this.socket.on('connect', () => {
    })
    this.socket.on('reportUpdate', (payload) => {
      this.setState({
        report: this.state.report.merge(payload.data),
      })
    })
    this.socket.on('disconnect', () => {
    });
    this.socket.emit('open', {
      userID: getUser().account,
      studyUID: this.props.data.report.studyUID
    })
  }

  componentWillUnmount() {
    this.socket.close()
  }

  removeImage = (index) => {
    this.setState({
      report: this.state.report.update('urls', urls => urls.delete(index))
    })
  }

  save() {
    const data = this.state.report.merge({operation: 'update'}).toJSON()
    return (
      api.updateReportLiver(data, {studyUID: this.props.data.report.studyUID})
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLiver.message.saved"))
        this.setState({
          report: this.state.report.set('updateDate', moment().format('YYYYMMDD')),
          unsave: false
        })
      })
    )
  }

  submit = () => {
    return this.save()
      .then(() => api.updateReportLiver({operation: 'submit'}, {studyUID: this.props.data.report.studyUID}))
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLiver.message.submitted"))
        this.setState({
          report: this.state.report.set('submitted', true)
        })
      })
  }

  unsubmit = () => {
    return api.updateReportLiver({operation: 'unsubmit'}, {studyUID: this.props.data.report.studyUID})
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLiver.message.unsubmitted"))
        this.setState({
          report: this.state.report.set('submitted', false)
        })
      })
  }

  examinate = () => {
    return this.save()
      .then(() => api.updateReportLiver({operation: 'examined'}, {studyUID: this.props.data.report.studyUID}))
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLiver.message.examinated"))
        this.setState({
          report: this.state.report.set('examined', true)
        })
      })
  }

  // template related
  handleTemplateChange = (key, value) => {
    let template = Object.assign({}, this.state.template, {[key]: value})
    this.setState({template})
  }

  saveTemplateChange = () => {
    api.updateTemplate(this.state.template)
    .then(() => message.success(GetLocaledText(this, "ReportPageLiver.message.saved-successfully")))
  }

  applyTemplate = () => {
    if (this.isForbidEdit()) {
      return
    }
    this.setState({report: this.state.report.merge(this.state.template)})
  }

  searchTemplate = (kw) => {
    this.setState({
      templateList: this.allTemplate.filter(t => t.templateName.includes(kw) || t.templateID.includes(kw))
    })
  }

  filterTemplate = (category) => {
    let cats = category.split('/')
    let templateList
    const isModalityMatched = (t) => {
      let matched = t.modality === cats[1]
      if (cats[1] === 'DR') {
        matched = matched || (t.modality === 'DX')
      }
      return matched
    }
    if (cats.length === 1) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0])
    } else if (cats.length === 2) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0] && isModalityMatched(t))
    } else if (cats.length === 3) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0] && isModalityMatched(t) && t.bodyPartExamined === cats[2])
    }
    this.setState({templateList})
  }

  openModal = () => {
    this.setState({
      modalVisible: true,
      newTemplateName: '',
    })
  }

  handleModalOk = () => {
    if (!this.state.newTemplateName) {
      this.setState({modalErrorText: GetLocaledText(this, "ReportPageLiver.mandatory-name")})
      return
    }
    let data = Object.assign({}, this.state.template, {
      "operation": "update",
      "templateName": this.state.newTemplateName,
    })
    delete data.templateID
    api.updateTemplate(data)
    .then(resp => {
      resp.data.usergroup = 'USER'
      this.setState({
        templateList: [resp.data, ...this.state.templateList]
      })
      return api.getTemplate()
    })
    .then(data => {
      this.allTemplate = data
      this.setState({
        modalVisible: false
      })
    })
  }

  renderMenuItems() {
    let items = []
    if (this.currentUser.usergroup === 'reporter') {
      if (!this.state.report.get('submitted')) {
        items.push(<Menu.Item key="save"><FormattedMessage id='ReportPageLiver.save'/></Menu.Item>)
        items.push(<Menu.Item key="submit"><FormattedMessage id='ReportPageLiver.submit'/></Menu.Item>)
      } else if(!this.state.report.get('examined')) {
        items.push(<Menu.Item key="unsubmit"><FormattedMessage id='ReportPageLiver.unsubmit'/></Menu.Item>)
      }
    } else if (this.currentUser.usergroup === 'examiner'
      && this.state.report.get('submitted')
      && !this.state.report.get('examined'))
    {
      items.push(<Menu.Item key="save"><FormattedMessage id='ReportPageLiver.save'/></Menu.Item>)
      items.push(<Menu.Item key="examinate"><FormattedMessage id='ReportPageLiver.examinate'/></Menu.Item>)
    }
    items.push(<Menu.Item key="print"><FormattedMessage id='ReportPageLiver.print'/></Menu.Item>)
    return items
  }

  renderStamp() {
    if (this.state.report.get('submitted') && !this.state.report.get('examined') && getUser().usergroup === 'reporter') {
      return <img className='report-status-stamp' src={require('../../../static/images/submit-stamp.png')}/>
    } else if (this.state.report.get('examined')) {
      return <img className='report-status-stamp' src={require('../../../static/images/exam-stamp.png')}/>
    }
    return ''
  }

  isForbidEdit() {
    const hasExamined = this.state.report.get('examined')
    const hasSubmitted = this.state.report.get('submitted')
    const isReporter = this.currentUser.usergroup === 'reporter'
    const isExmainer = this.currentUser.usergroup === 'examiner'
    return hasExamined || (isReporter && hasSubmitted) || (isExmainer && !hasSubmitted)
  }

  showRawImage(url) {

  }

  onPrint = () => {
    const reportElement = document.querySelector('#to-snap')
    domtoimage.toPng(reportElement)
    .then(function (dataUrl) {
        reportElement.style.margin = "0"
        var pdf = new jsPDF('p', 'in', 'a4');
        pdf.addImage(dataUrl, 'JPEG', 0, 0);
        pdf.save("download.pdf");
    });
  }

  render() {
    const isForbidEdit = this.isForbidEdit()

    return (
      <div className='page' id='report-page-liver'>
        <Prompt
          when={this.state.unsave}
          message={location => GetLocaledText(this, "ReportPageLiver.message.unsaved-before-leave")}
        />
        <Header>
          <Logo/>
          <Menu
            theme="dark"
            mode="horizontal"
            style={{ lineHeight: '64px', borderBottom: 'none', display: 'inline-block' }}
            selectable={false}
            onClick={({item, key, keyPath}) => {
              if (key === 'submit') {this.submit()}
              else if (key === 'unsubmit') {this.unsubmit()}
              else if (key === 'save') {this.save()}
              else if (key === 'examinate') {this.examinate()}
              else if (key === 'print') {this.onPrint()}
            }}
          >
            {this.renderMenuItems()}
          </Menu>
          <SignoutButton/>
        </Header>
        <div className='content' >
          <div
            id='to-snap'
            style={{
              backgroundColor: 'white',
              width: '800px',
              margin: 'auto'
          }}
          >

          <Report
            isForbidEdit={isForbidEdit}
            report={this.state.report.toJS()}
            labels={this.state.labels}
            removeImage={this.removeImage}
            onChange={(key, value) => {
              this.setState({
                report: this.state.report.set(key, value)
              })
            }}
            liradsScore="LR-3"
            />
          </div>
        </div>

        <Modal
          title={GetLocaledText(this, "ReportPageLiver.title.save-template")}
          visible={this.state.modalVisible}
          onOk={this.handleModalOk}
          onCancel={() => this.setState({modalVisible: false})}>
          {
            this.state.modalErrorText ?
            <Alert message={this.state.modalErrorText} type="error"/> : ''
          }

          <Input onChange={(e) => this.setState({newTemplateName: e.target.value})}
            placeholder={GetLocaledText(this, "ReportPageLiver.input.template.name")}
            value={this.state.newTemplateName}
            />
        </Modal>
      </div>
    )
  }

}

async function getData(props) {
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let report = await api.getReportLiver({}, {studyUID})
  let template = await api.getTemplate()
  let labels = await api.getLabelsLiver({}, {studyUID})
  let data = {report, template, labels}
  return await data
}

ReportPage = LoadingWrapper(ReportPage, getData)

export default injectIntl(ReportPage)
