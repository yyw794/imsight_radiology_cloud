# -*- coding: utf-8 -*-
from request_interface.pacs_interface import (send_echo_request,send_find_request,send_move_request,store_listen)
from request_interface.request_image import (getYesterday)
from request_interface.initconfig import get_AppTitle,get_ListenPACSPort,get_if_push_model
from multiprocessing import Process

# patient = {'studyUID':'123','studyDate':'20180603','birthDate':'20180302','hospital':'aaa','bodyPartExamined':'CHEST',
# 'gender':"MALE/FEMALE",'modality':'CT','patientID':'11','patientName':'22'}


if __name__ == "__main__":
	print "######send echo to pacs server######"
	print send_echo_request()
	
	study_date = getYesterday()
	print "######get yesterday patient list from pacs server "+study_date+"######"
	condition = {'study_date': '20180807', 'patient_id': 'X1184740'}
	conditionlist = send_find_request(condition)
	for c in conditionlist:
		print c
