import React, { Component } from 'react'
import { Form, Icon, Input, Button, Checkbox, Layout, message } from 'antd'
import {Link} from 'react-router-dom'
const FormItem = Form.Item
const { Header, Content, Footer } = Layout
import {authenticate} from '../../auth'
import {Redirect} from 'react-router-dom'
import api from '../../api'
import {GetLocaledText} from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'
import './index.less'

class ResetPasswd extends Component {
  constructor(props) {
    super(props)
    this.account = this.GetQueryString('account')
    this.token = this.GetQueryString('token')
    let progress = 'send'
    if (!!this.account && !!this.token) 
    {
      progress = 'reset'
    }
    this.state = {
      progress
    }
  }
  
  GetQueryString(name) { 
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)","i"); 
    var r = window.location.search.substr(1).match(reg); 
    if (r!=null) return (r[2]); return null; 
  }

  handleSubmitEmail = (e) => {
      e.preventDefault()
      this.props.form.validateFields((err, values) => {
        if (!err) {
          api.sendResetEmail(values)
          .then(() => this.setState({progress: 'sended'}))
          .catch(err => {
            if (err.payload.statusCode === 1108) {
              message.error(GetLocaledText(this,"ResetPage.dialog.emailUnconfirm"))
            } else {
              message.error(GetLocaledText(this,"ResetPage.dialog.emailSentFailed"))
            }
          })
        }
      })
  }

  handleSubmitPassword = (e) => {
      e.preventDefault()
      this.props.form.validateFields((err, values) => {
        if (!err) {
          const data = {
            account: this.account,
            password: values.password,
            token: this.token
          }
          api.resetPassword(data).then(() => {
            message.success(GetLocaledText(this,"ResetPage.dialog.PasswordChanged"))
            setTimeout(() => authenticate(data, () => this.props.history.push('/')), 1000)
          })
        }
      })
  }

  renderEmailForm() {
    const { getFieldDecorator } = this.props.form
    return (
      <Form onSubmit={this.handleSubmitEmail} className="reset-form">
          <p className='title'>视见放射影像辅助筛查平台</p>
          <p className='engtitle'>Imsight Radiology Platform</p>
          <p className='version'>IRCP 1.4.20.181109</p>
          <FormItem className='register-form-item'>
            {getFieldDecorator('email', {
              rules: [
                { required: true, message: GetLocaledText(this,"ResetPage.form.input.email")},
                {
                  pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                  message: GetLocaledText(this,"ResetPage.form.validate.email")
                }
              ],
            })(
              <Input
                prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={GetLocaledText(this,"ResetPage.form.input.email")} />
            )}
          </FormItem>
          <Button type="primary" htmlType="submit" className="reset-form-button">
            <FormattedMessage id="ResetPage.form.submit"/>
          </Button>
      </Form>
    )
  }

  renderSendSuccess() {
    return (
      <div className='send-success'>
        <p className='title'>视见放射影像辅助筛查平台</p>
        <p className='engtitle'>Imsight Radiology Platform</p>
        <p className='version'>IRCP 0.2.12.180525</p>
        <Icon type="check-circle-o" />
        <h2><FormattedMessage id="ResetPage.dialog.success"/></h2>
        <p><FormattedMessage id="ResetPage.dialog.hint"/></p>
      </div>
    )
  }

  renderResetForm() {
    const { getFieldDecorator } = this.props.form
    return (
      <Form onSubmit={this.handleSubmitPassword} className="reset-form">
        <p className='title'>视见放射影像辅助筛查平台</p>
        <p className='engtitle'>Imsight Radiology Platform</p>
        <p className='version'>IRCP 0.2.12.180525</p>
          <FormItem className='register-form-item'>
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: GetLocaledText(this,"ResetPage.form.input.password") },
                { min: 3, message:GetLocaledText(this,"ResetPage.form.validate.password")},
              ],
            })(
              <Input
                onCopy={() => false}
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder={GetLocaledText(this,"ResetPage.form.placeholder.password")} />
            )}
          </FormItem>
          <FormItem className='register-form-item'>
            {getFieldDecorator('passwordConfirm', {
              rules: [
                { required: true, message: GetLocaledText(this,"ResetPage.form.input.passwordAgain") },
                {
                  validator: (rule, value, callback) => {
                    const { getFieldValue } = this.props.form
                    if (value && value !== getFieldValue('password')) {
                        callback(GetLocaledText(this,"ResetPage.form.validate.passwordAgain"))
                    }

                    // Note: 必须总是返回一个 callback，否则 validateFieldsAndScroll 无法响应
                    callback()
                  }
                }
              ],
            })(
              <Input
                onCopy={() => false}
                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder={GetLocaledText(this,"ResetPage.form.placeholder.passwordAgain")} />
            )}
          </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit" className="reset-form-button">
            <FormattedMessage id="ResetPage.form.submit"/>
          </Button>
        </FormItem>
      </Form>
    )
  }

  render() {
    // render login page
    return (
      <div className='page reset-page'>
        <div className="QRcode">
          <img src={require('../../../static/images/qrcode.png')}/>
          <span className="wechat"><FormattedMessage id="ResetPage.wechat"/></span>
        </div>
        <a className="website" href="http://www.imsightmed.com/">www.imsightmed.com</a>
        <Content className='content'>
          {this.state.progress === 'send' && this.renderEmailForm()}
          {this.state.progress === 'sended' && this.renderSendSuccess()}
          {this.state.progress === 'reset' && this.renderResetForm()}
        </Content>
      </div>
    )
  }

}

ResetPasswd = Form.create()(ResetPasswd)

export default injectIntl(ResetPasswd)
