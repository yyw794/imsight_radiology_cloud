#!/bin/python
# coding=utf-8
import datetime
import os
import sys

from flask import make_response, request, jsonify, abort
from flask_login import current_user

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_delete_one, db_insert_one, db_update_one
from models.response.response import response
from models.task.Task import parseTaskInfo
from models.study.Study import Study, parseStudyInfo

required_key_get = ['studyUID']
required_key_put = ['birthDate', 'studyDate', 'bodyPartExamined', 'gender', \
                    'hospital', 'modality', 'patientID', 'patientName', 'studyUID']
optional_key = ['token']


def on_study():
    """
    @api {get post} /api/study  获取DOCIM信息
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam (get) {String} studyUID 检测ID
    @apiParam (post) {String} birthDate 出生日期
    @apiParam (post) {String} studyDate 检查日期
    @apiParam (post) {String} bodyPartExamined 检查部位
    @apiParam (post) {String} gender 性别
    @apiParam (post) {String} hospital 医院
    @apiParam (post) {String} modality 模式
    @apiParam (post) {String} patientID 病历ID
    @apiParam (post) {String} patientName 病人名称
    @apiParamExample {string} get Request-Example:
        /api/study?studyUID=1.2.392.200036.9116.2.5.1.3268.2060439916.1525305902.560145
     @apiParamExample {json} post Request-Example:
        {
            "birthDate": "1966-06-01",
            "studyDate": "2018-12-03",
            "bodyPartExamined": "CHEST",
            "gender": "FEMALE",
            "hospital": "hosp",
            "modality": "CT",
            "patientID": "1001203",
            "patientName": "yyy",
            "studyUID": "1.2.392.200036.9116.2.5.1.3268.2060439916.1525305902.560145"
        }
    @apiSuccessExample {json} get Success-Response:
    {
      "data": {
        "age": 61,
        "birthDate": "1957-10-29",
        "bodyPartExamined": "CHEST",
        "gender": "FEMALE",
        "hospital": "hosp",
        "modality": "CT",
        "patientID": "1001203",
        "patientName": "YYY",
        "result": "ABNORMAL",
        "reverse": false,
        "series": {}
        "state": "file_only",
        "studyDate": "2018-12-03",
        "studyTime": "17:48:23",
        "studyUID": "1.2.392.200036.9116.2.5.1.3268.2060439916.1525305902.560145",
        "taskID": "d74e82a4-f2b2-11e8-b0fa-0242ac120003",
        "uploadDate": "2018-12-03 17:48:23",
        "user_privilege": [
          "reporter",
          "normal"
        ]
      },
    "message": "OK",
    "statusCode": 1000
   }
   @apiSuccessExample {json} post Success-Response:
   {
    "statusCode": 1000,
    "data": {
        "age": 61,
        "birthDate": "1957-10-29",
        "bodyPartExamined": "CHEST",
        "gender": "FEMALE",
        "hospital": "hosp",
        "modality": "CT",
        "patientID": "1001203",
        "patientName": "YYY",
        "result": "ABNORMAL",
        "reverse": false,
        "series": {}
        "state": "file_only",
        "studyDate": "2018-12-03",
        "studyTime": "17:48:23",
        "studyUID": "1.2.392.200036.9116.2.5.1.3268.2060439916.1525305902.560145",
        "taskID": "d74e82a4-f2b2-11e8-b0fa-0242ac120003",
        "uploadDate": "2018-12-03 17:48:23",
        "user_privilege": [
          "reporter",
          "normal"
        ]
      }
   }
    @apiErrorExample {json} get Error-Response:
    {
      "statusCode": 1200
    }
    @apiErrorExample {json} post Error-Response:
    {
      "statusCode": 1100
    }
    """
    res = response()
    userID = current_user.get_id()
    params = request.args.to_dict() if request.method == 'GET' else request.json

    if request.method == 'GET':
        if not all(key in params for key in required_key_get):
            res.statusCode = 1100
            return make_response(jsonify(res.to_dict()), 200)

        studyUID = params['studyUID']
        # check whether user possess data as reviewer
        task_info = db_find_one("task", {'studyUID': studyUID, "reporter": userID})
        if task_info is None:
            task_info = db_find_one('task', {
                "$or": [{'studyUID': studyUID, "reviewer": userID},
                        {'studyUID': studyUID, "reporter": "imsightmed"}]})
        if task_info is None:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        # get taskID from ownership collection by ownerID
        ownerID = task_info['reporter']
        ownership_info = db_find_one('ownership', {"userID": ownerID})
        shared = ownership_info['shared']
        if studyUID.replace('.', '-') in shared:
            taskID = shared[studyUID.replace('.', '-')]
            if taskID != task_info['taskID']:
                db_delete_one('task', {'taskID': task_info['taskID']})
                db_delete_one('label', {'taskID': task_info['taskID']})
                db_delete_one('report', {'taskID': task_info['taskID']})
                db_delete_one('tool', {'taskID': task_info['taskID']})
                task_info = db_find_one('task', {'taskID': taskID})
        else:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        task = parseTaskInfo(task_info)

        # get study data
        study_info = db_find_one("study", {"studyUID": studyUID})
        if study_info is None:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)
        new_study = parseStudyInfo(study_info)

        # get label once to identify malignant
        label_info = db_find_one("label", {"taskID": task_info["taskID"]})
        result = "NORMAL"
        if label_info is not None:
            result = "ABNORMAL"

        study_info = new_study.to_dict()
        study_info['taskID'] = task.id
        study_info['result'] = result
        study_info['user_privilege'] = task.get_user_privilege(userID)

        res.data = study_info
        res.statusCode = 1000
        return make_response(jsonify(res.to_dict()), 200)

    if request.method == 'POST':
        if not all(key in params for key in required_key_put):
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        new_study = Study(params['studyUID'])
        try:
            new_study.studyDate = datetime.datetime.strptime(params['studyDate'], "%Y%m%d")
        except:
            new_study.studyDate = datetime.datetime.today()

        try:
            new_study.birthDate = datetime.datetime.strptime(params['birthDate'], "%Y%m%d")
        except:
            new_study.birthDate = datetime.datetime.today()
        new_study.uploadDate = datetime.datetime.today()
        new_study.bodyPartExamined = params['bodyPartExamined']
        new_study.gender = params['gender']
        new_study.modality = params['modality']
        new_study.patientID = params['patientID']
        new_study.patientName = params['patientName']
        new_study.result = ""
        new_study.state = 'meta_only'

        if not db_find_one('study', {"studyUID": new_study.studyUID}):
            db_insert_one('study', new_study.to_db())
            ownership_info = db_find_one('ownership', {"userID": userID})
            shared = ownership_info['shared']
            shared[(new_study.studyUID).replace('.', '-')] = ""
            db_update_one('ownership', {"userID": userID}, ownership_info)
            res.statusCode = 1000
            res.data = new_study.to_dict(withSeries=False)
            return make_response(jsonify(res.to_dict()), 200)
        else:
            res.statusCode = 1102
        return make_response(jsonify(res.to_dict()), 200)
