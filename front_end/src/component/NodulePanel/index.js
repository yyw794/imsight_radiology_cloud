import React, { Component } from 'react';
import {Table, Row, Col, Icon, Modal, Select, Button, Affix} from 'antd'
import _ from 'underscore'
import Immutable from 'immutable'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import './index.less'
import { getUserRole } from '../../auth';
import api from '../../api';

/**
 * 肺结节位置用图标表示
 * @type {{"0", "1", "2", "3", "4"}}
 */
// const POS_IMG = {
//   0: require('../../../static/images/lungs_lt.png'),
//   1: require('../../../static/images/lungs_lb.png'),
//   2: require('../../../static/images/lungs_rt.png'),
//   3: require('../../../static/images/lungs_rb.png'),
//   4: require('../../../static/images/lungs_rm.png')
// }
const POS_IMG ={
  0:"左肺上叶",
  1:"左肺下叶",
  2:"右肺上叶",
  3:"右肺下叶",
  4:"右肺中叶",
}
/**
 * 风险对应的文字名
 * @private {Object}
 */
const RISK_EN2CN = {
  Benign: <FormattedMessage id = "NodulePanel.risk-text.benign"/>,
  Low: <FormattedMessage id = "NodulePanel.risk-text.low"/>,
  Middle: <FormattedMessage id = "NodulePanel.risk-text.medium"/>,
  High: <FormattedMessage id = "NodulePanel.risk-text.high"/>,
  emptyString:"-"
}

/**
 * 获取风险值即对应的name
 * @param bm
 * @returns {string}
 */
function getRiskText(bm) {
  let max = Math.max(bm.Benign,bm.Mainly_Benign,bm.Mainly_Malignant,bm.Malignant)
  if(bm.Benign === max){
    return 'Benign'
  }else if(bm.Mainly_Benign === max){
    return 'Low'
  }else if(bm.Mainly_Malignant === max){
    return 'Middle'
  }else if(bm.Malignant ===max){
    return 'High'
  }else{
    return "emptyString"
  }
}

/**
 * 风险初始化对应数字值
 * @param risk
 * @returns {number}
 */
function riskToInt(risk) {
  if(risk==='Benign') return 1;
  if(risk==='Low') return 2;
  if(risk==='Middle') return 3;
  if(risk==='High') return 4;
  return 0;
}

/**
 * 对应的风险颜色
 * @param bm
 * @returns {string}
 */
function getRiskColor(bm) {
  let max = Math.max(bm.Benign,bm.Mainly_Benign,bm.Mainly_Malignant,bm.Malignant);
  if(bm.Benign === max){
    return '#00ff00'
  }else if(bm.Mainly_Benign === max){
    return '#ffff00'
  }else if(bm.Mainly_Malignant === max){
    return '#ffa501'
  }else if(bm.Malignant ===max){
    return '#ff0000'
  }else{
    return '#00ff00'
  }
}

/**
 *亚类对应的文字表述
 * @private {Object}
 */
const TYPE_EN2CN = {
  GroundGlass: <FormattedMessage id = "NodulePanel.subclass.glass"/>,
  PartSolid: <FormattedMessage id = "NodulePanel.subclass.subsolid"/>,
  Solid: <FormattedMessage id = "NodulePanel.subclass.solid"/>,
  Calcification: <FormattedMessage id = "NodulePanel.subclass.calcified"/>
}

/**
 * 根据不同的HU对应不同的亚类
 * @param HU 深度
 * @returns {string} 亚类名
 */
function getTypeByHU(HU) {
  if (HU < -500) {
    return 'GroundGlass'
  } else if (HU >= -500 && HU < -200) {
    return 'PartSolid'
  } else if (HU >= -200 && HU < 150) {
    return 'Solid'
  } else {
    return 'Calcification'
  }
}

/**
 * 获取亚类
 * @param nodule 单个节点数据
 * @returns {string}
 */
function getNoduleType(nodule) {
  if(nodule.nodule_type)
  {
      let max = Math.max(nodule.nodule_type.Calcification,nodule.nodule_type.Ground_Glass,nodule.nodule_type.Part_Solid,nodule.nodule_type.Solid)
      if (nodule.nodule_type.Ground_Glass === max){
        return "GroundGlass"
      }else if(nodule.nodule_type.Part_Solid === max){
        return "PartSolid"
      }else if(nodule.nodule_type.Solid === max){
        return "Solid"
      }else if(nodule.nodule_type.Calcification === max){
        return "Calcification"
      }
  }else{
    return getTypeByHU(nodule.nodule_avgHU)
  }
}

/**
 * 将亚类转化为对应的数字
 * @param type 亚类
 * @returns {number} 亚类值
 */
function typeToInt(type) {
  if(type==="GroundGlass") return 1;
  if(type==="PartSolid") return 2;
  if(type==="Solid") return 3;
  if(type==="Calcification") return 4;
  return 0;
}

/**
 * 每个节点对应的详情
 * @param nodule 节点
 * @param maxZ
 * @param zReverse
 * @returns {*}
 */
function renderDetail(nodule,maxZ=0,zReverse=false) {
  if (!nodule) return ''
  return (
    <div className='nodule-detail'>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.size'/></Col>
        <Col span={12}>{nodule.nodule_diameter.toFixed(1)}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.malignancy'/></Col>
        <Col span={12} style={{color: getRiskColor(nodule.nodule_bm)}}>{RISK_EN2CN[getRiskText(nodule.nodule_bm)]}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.subclass'/></Col>
        <Col span={12}>{TYPE_EN2CN[getNoduleType(nodule)]}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.location'/></Col>
        <Col span={12}>{`[${nodule.coord.x}, ${nodule.coord.y}, ${zReverse?(maxZ-nodule.coord.z+1):nodule.coord.z}]`}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.anatomical-location'/></Col>
        <Col span={12}>
          <img src={POS_IMG[nodule.nodule_location]}/>
        </Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.average-density'/></Col>
        <Col span={12}>{nodule.nodule_avgHU.toFixed(1)}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.volume'/></Col>
        <Col span={12}>{nodule.nodule_volume.toFixed(1)}</Col>
      </Row>
    </div>
  )
}

/**
 * 节点面板组件
 */
class NodulePanel extends Component {
    /**
     * 参照生命周期函数
     * @param props
     */
  constructor(props) {
    super(props)
        /**
         * 数据初始化赋值
         */
    this.originData = Immutable.fromJS(this.props.data).sort(this.getSorter('initial', true))
    if (!props.fixedKey) {
      this.originData = this.originData.map((data, i) => data.set('key', i + 1))
    }
        /**
         *
         * @type {{
         * selectedNodule: null,选中的节点
         * selectedId: null,  选中的节点id
         * selectedRowKeys: any, 选中行key
         * data: *,  节点面板数据list
         * sortColumn: string,  比较列name
         * sortReverse: boolean,  是否逆序
         * addModalVisible: boolean
         * }}
         */
    this.state = {
      selectedNodule: null,
      selectedId: null,
      selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS(),
      data: this.originData,
      sortColumn: 'initial',
      sortReverse:true,
      addModalVisible: false
    }
        /**
         *
         * @private {number} 面板列表pageSize
         */
    this.pageSize = window.screen.width< window.screen.height ? 9:Math.round((window.screen.height-550)/62)
  }

    /**
     * 分页页数适配
     */
  onResize = () => {
    this.pageSize = window.screen.width< window.screen.height ? 9:Math.round((window.screen.height-550)/62)
  }

    /**
     * 参照react生命周期函数
     */
  componentDidMount() {
    window.addEventListener("resize", _.debounce(this.onResize, 100)) //Jackey: Why delay it by using _.debounce, but not simply passing this.onResize?
    //window.addEventListener("resize", this.onResize);
  }
    /**
     * 参照react生命周期函数
     */
  componentWillReceiveProps(nextProps) {
    this.originData = Immutable.fromJS(nextProps.data).sort(this.getSorter('initial', true))
    if (!nextProps.fixedKey) {
      this.originData = this.originData.map((data, i) => data.set('key', i + 1))
    }
    this.setState({
      selectedRowKeys:this.state.selectedRowKeys,
    })
    if(!this.originData.equals(this.state.data))
    {
      this.sortBy(this.state.sortColumn,this.state.sortReverse)
    }
  }
  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   this.originData = Immutable.fromJS(this.pops.data).sort(this.getSorter('coord.z', true))
  //   if (!this.props.fixedKey) {
  //     this.originData = this.originData.map((data, i) => data.set('key', i + 1))
  //   }
  //   // this.setState({
  //   //   selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS(),
  //   // });
  //   Object.assign(this.state, { selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS() });
  //   this.sortBy(this.state.sortColumn)
  // }
    /**
     * 获取列名排序值
     * @param col 列名
     * @param reverse 是否逆序
     * @returns {Function}
     */
  getSorter(col, reverse=false) {
    return (a, b) => {
      let v1 = a.get(col)
      let v2 = b.get(col)
      if(!v1 || !v2 )
      {
         //sort by coord
        v1=Number(a.get("coord").get("z"))*1000*1000 +Number(a.get("coord").get("x"))*1000+Number(a.get("coord").get("y"))
        v2=Number(b.get("coord").get("z"))*1000*1000 +Number(b.get("coord").get("x"))*1000+Number(b.get("coord").get("y"))
        
        //sort by nodule begin maglinant or tumor maglinant initially
        if(col==='initial')
        {
          v1 = riskToInt(getRiskText(a.get("nodule_bm").toJS()))*1000*1000*10000 + (v1 + 1000*1000*10000)
          v2 = riskToInt(getRiskText(b.get("nodule_bm").toJS()))*1000*1000*10000 + (v2 + 1000*1000*10000);
        }
      }
      if(col==="nodule_bm")
      {
        v1 = riskToInt(getRiskText(v1.toJS()));
        v2 = riskToInt(getRiskText(v2.toJS()));
      }
      if(col==="nodule_type")
      {
        v1 = typeToInt(getNoduleType(a.toJS()));
        v2 = typeToInt(getNoduleType(b.toJS()));
        console.log("---")
      }
      if (v1 < v2) {
        return reverse ? 1 : -1
      }
      if (v1 > v2) {
        return reverse ? -1 : 1
      }
      if (v1 === v2) {
        return 0
      }
    }
  }

    /**
     * 根据什么列名排序
     * @param col 列名
     * @param reverse 是否倒序
     */
  sortBy(col,reverse=false) {
    let data = this.originData.sort(this.getSorter(col, reverse))
    this.setState({
      data,
      sortColumn: col,
    })
  }

    /**
     * 选中面板列表行事件
     * @param selectedRowKeys 选中行的key
     */
  onSelectChange = (selectedRowKeys) => {
    let oldKeys = this.state.selectedRowKeys
    let newKeys = [...oldKeys];
    if(newKeys.indexOf(selectedRowKeys)==-1){
        newKeys.push(selectedRowKeys)
    }else{
        newKeys.splice(newKeys.indexOf(selectedRowKeys),1)
  }
    let add = newKeys.filter(x => !oldKeys.includes(x))
    if (add.length > 0) {
      let record = this.state.data.find(x => x.get('key') === add[0]).toJS()
      this.setState({selectedId: record.labelID})
      if (_.isFunction(this.props.onSelectNodule)) {
        // FIXME: to ensure a correct image is crop, must wait for the image to be display
        // so the promise in onSelectNodule will be resolved after 2 frames
          // this.props.onSelectNodule(record).then(() => _.isFunction(this.props.onAddScreenshot))
          console.log(record)
        this.props.onSelectNodule(record).then(() => _.isFunction(this.props.onAddScreenshot) && this.props.onAddScreenshot(record))
      }
    }
    let remove = oldKeys.filter(x => !newKeys.includes(x))
    if (remove.length > 0) {
      let record = this.state.data.find(x => x.get('key') === remove[0]).toJS()
      _.isFunction(this.props.onRemoveScreenshot) && this.props.onRemoveScreenshot(record)
    }
    this.setState({ selectedRowKeys:newKeys});
  }

    /**
     * 获取结节页数
     * @param nodule 节结
     * @returns {number}
     */
  getNodulePage(nodule) {
    let page = 1
    let idx = this.state.data.findIndex(n => n.get('labelID') === nodule.labelID)
    return Math.ceil((idx + 1) / this.pageSize)
  }

    /**
     * 获取选中的结节
     * @returns {*}
     */
  getSelectedNodule() {
    if (this.state.selectedId === null) {return null}
    const nod = this.state.data.find(n => n.get('labelID') === this.state.selectedId)
    if (!nod) {
      this.setState({selectedId: null})
      return null
    }
    return nod.toJS()
  }

    /**
     * 选中添加高亮效果
     * @param nodule
     */
  selectNodule(nodule) {
    let currentPage = this.getNodulePage(nodule)
    this.setState({selectedId: nodule.labelID, currentPage})
  }

    /**
     * 删除结节
     */
  deleteNodule = () => {
    if (this.state.selectedId === null) return
    Modal.confirm({
      title: GetLocaledText(this,"NodulePanel.dialog.deleteNodule"),
      onOk: () => {
        this.props.onDelete(this.state.selectedId)
        this.setState({selectedId: null})
      }
    })
  }

    /**
     * CT左侧肺结节列表组件
     * @returns {Component}
     */

  render() {
    const { loading, selectedRowKeys } = this.state;
    const that=this;
    console.log(selectedRowKeys)
    const columns = [
      {
        title: GetLocaledText(this,"NodulePanel.table-header.number"),
        dataIndex: 'key',
          align:"center",
          width:40,
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render: (value, record) => {
          let text = `${this.props.keyPrefix ? this.props.keyPrefix : 'N'}${value}`
          if (record.addByUser) {
            text += '*'
          }
          return text
        }
      },
      {
        title: GetLocaledText(this,"NodulePanel.table-header.diam"),
        dataIndex: 'nodule_diameter',
          align:"center",
          width:80,
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render: (value, record) => {
          if (value === null) {return '-'}
          return <div><span style={{color:"#5CACEF",fontWeight:"bold",fontSize:"16px"}}>{value.toFixed(1)+"mm"}</span><div style={{color:"#7E838F"}}>{record.nodule_volume.toFixed(1)+"mm"}<sup>3</sup></div></div>
        }
      },
        {
            title: GetLocaledText(this,"NodulePanel.table-header.subclass"),
            dataIndex: 'nodule_type',
            align:"center",
            onHeaderCell: (column) => {
                return {
                    onClick: () => {
                        this.sortBy(column.dataIndex,!this.state.sortReverse)
                        this.setState({sortReverse:!this.state.sortReverse})
                    }
                }
            },
            render: (value, record) => {
                if (record.subtTrue === null && record.subt === null) {return ''}
                return <div><span style={{color:"#ACDBF5",fontWeight:"bold",fontSize:"16px"}}>{TYPE_EN2CN[getNoduleType(record)]}</span><div style={{color:"#7E838F"}}>{record.nodule_avgHU.toFixed(1)+"(HU)"}</div></div>
            }
        },
      {
        title: GetLocaledText(this,"NodulePanel.table-header.malignancy"),
        dataIndex: 'nodule_bm',
        align:"center",
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render: (value, record) => {
          if (value === null) {return ''}
             return <div><span style={{color: getRiskColor(value),fontWeight:"bold",fontSize:"16px"}}>{RISK_EN2CN[getRiskText(value)]}</span><div style={{color:"#7E838F"}}>{POS_IMG[record.nodule_location]}</div></div>
        }
      },
        {
          title: GetLocaledText(this,"NodulePanel.table-header.operation"),
          align:"center",
            width:70,
          render:(value, record)  =>{
               return (<span style={{cursor:"pointer"}} onClick ={()=>{
                  that.onSelectChange(record.key)
                   }}>
                  {
                      that.state.selectedRowKeys.indexOf(record.key)==-1?
                      GetLocaledText(this,"LiverViewer.ToolBar.screenshot"):
                      GetLocaledText(this,"LiverViewer.ToolBar.cancelscreenshot")
                  }
                </span>)
          }
        }
      // {rowSelection={this.props.hideSelection ? null : rowSelection}
      //   title: GetLocaledText(this,"NodulePanel.table-header.z"),
      //   dataIndex: 'coord.z',
      //   onHeaderCell: (column) => {
      //     return {
      //       onClick: () => {
      //         this.sortBy(column.dataIndex,!this.state.sortReverse)
      //         this.setState({sortReverse:!this.state.sortReverse})
      //       }
      //     }
      //   },
      //   render:(value,record)=>{
      //     return this.props.zReverse?(Number(this.props.maxZ)-record.coord.z+1):record.coord.z
      //       //return record.coord.z;
      //   }
      // },
      // {
      //   title: GetLocaledText(this,"NodulePanel.table-header.avrgHU"),
      //   dataIndex: 'nodule_avgHU',
      //   onHeaderCell: (column) => {
      //     return {
      //       onClick: () => {
      //         this.sortBy(column.dataIndex,!this.state.sortReverse)
      //         this.setState({sortReverse:!this.state.sortReverse})
      //       }
      //     }
      //   },
      //   render: (value) => {
      //     if (value === null) {return '-'}
      //     return value.toFixed(1)
      //   }
      // }
    ]

    const nodule = this.getSelectedNodule()
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      columnWidth: 20,
      hideDefaultSelections: true,
    };

    const OptionSubvariety = Select.Option;
    const MalignantRisk = Select.Option;
    return (
      <div className='nodule-panel'>

        <div className='nodule-table'>
          <Table
            columns={columns}
            dataSource={this.state.data.toJS()}
            rowClassName={(record) => {
              return this.state.selectedId && record.labelID === this.state.selectedId ?
                'active' : 'noActive'
            }}
            size="small"
            pagination={{
              pageSize: this.pageSize,
              simple: true,
              showTotal: (total, range) => <span><FormattedMessage id='NodulePanel.page-handle.pre-total'/><span className='nodule-count'>{total}</span><FormattedMessage id='NodulePanel.page-handle.post-total'/></span>,
              onChange: (currentPage) => this.setState({currentPage}),
              current: this.state.currentPage
            }}
            onRow={record => {
              return {
                onClick: () => {
                  this.setState({selectedId: record.labelID})
                  _.isFunction(this.props.onSelectNodule) && this.props.onSelectNodule(record)
                }
              }
            }}/>
        {
           this.state.data.size > 0 ?
          <span className='ant-pagination-total-text'><FormattedMessage id='NodulePanel.page-handle.pre-total'/><span className='nodule-count'>{this.state.data.size}</span><FormattedMessage id='NodulePanel.page-handle.post-total'/></span>
          :
          ''
          }
          {
            !this.props.hideEditNoduleBtn ?
            <div className='action-btns'>
              <Icon type="plus-circle-o" onClick={this.props.onAdd}/>
              <Icon type="delete" onClick={this.deleteNodule}/>
            </div>
            :
            ''
          }

        </div>

        {
          _.isFunction(this.props.renderDetail) ?
          this.props.renderDetail(this.getSelectedNodule())
          :""
          // renderDetail(this.getSelectedNodule(),this.props.maxZ,this.props.zReverse)
        }

      </div>

    );
  }

}

export {getRiskText, getRiskColor, getNoduleType, POS_IMG, RISK_EN2CN, TYPE_EN2CN};
export default injectIntl(NodulePanel, {withRef: true});
