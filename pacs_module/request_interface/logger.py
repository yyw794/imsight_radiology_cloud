# -*- coding: UTF-8 -*-
import logging
import sys
from logging.handlers import TimedRotatingFileHandler
from logging.handlers import RotatingFileHandler
import os

logger = logging.getLogger("PACSModule")
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(filename)s - %(funcName)s - %(lineno)s - %(message)s')

# file_handler = logging.FileHandler("../logfile/test.log")
# file_handler.setFormatter(formatter)

if not os.path.exists('logfile'): os.makedirs('logfile')

file_handler = RotatingFileHandler("logfile/pacs.log", maxBytes=1024 * 1024 * 1024,
                                   backupCount=5)
file_handler.setFormatter(formatter)

console_handler = logging.StreamHandler(sys.stdout)
console_handler.formatter = formatter

logger.addHandler(file_handler)
logger.addHandler(console_handler)

logger.setLevel(logging.INFO)


def debug(test):
    logger.debug(test)


def info(test):
    logger.info(test)


def warn(test):
    logger.warn(test)


def error(test):
    logger.error(test)


def fatal(test):
    logger.fatal(test)


def critical(test):
    logger.critical(test)


''' 输出不同级别的log
logger.debug('this is debug info')
logger.info('this is information')
logger.warn('this is warning message')
logger.error('this is error message')
logger.fatal('this is fatal message, it is same as logger.critical')
logger.critical('this is critical message')'''
