#!/bin/python
# coding=utf-8
import math
import os
import shutil
import sys
import threading
import traceback
import uuid

import SimpleITK as sitk
import numpy as np
import scipy.misc

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from config import IMAGE_ROOT, USE_INSTANCE_NUMBER
import logging

logger = logging.getLogger('radiology')


class ResultThread(threading.Thread):
    def __init__(self, target, args=()):
        super(ResultThread, self).__init__()
        self.target = target
        self.args = args
        self.exitcode = 0
        self.exception = None
        self.exc_traceback = ''

    def run(self):
        try:
            self.result = self.target(*self.args)
        except Exception as e:
            self.exitcode = 1
            self.exception = e
            self.exc_traceback = ''.join(traceback.format_exception(*sys.exc_info()))

    def get_result(self):
        try:
            return self.result
        except Exception:
            return None


# def cal_hist_ct(source, window_level=-600, thresh=0.999):
#     source = source.ravel()
#     total = len(source)
#
#     s_values, bin_idx, s_counts = np.unique(source, return_inverse=True, return_counts=True)
#
#     histogram_dict = {}
#     for value, count in zip(s_values, s_counts):
#         histogram_dict[value] = count
#
#     current_count = 0
#     forward = 0
#     backward = 0
#     max_value = 0
#     min_value = 0
#     while float(current_count) / float(total) < thresh:
#         h_value = window_level + forward
#         l_value = window_level - backward
#
#         if h_value in histogram_dict:
#             max_value = h_value
#             current_count += histogram_dict[h_value]
#
#         if l_value in histogram_dict:
#             min_value = l_value
#             current_count += histogram_dict[l_value]
#
#         forward += 1
#         backward += 1
#
#     return min_value, max_value

def cal_hist_ct(data, m=3):
    data = data.ravel()
    mean = np.mean(data)
    std = np.std(data)
    data[abs(data - mean) > m * std] = 0
    return min(data), max(data)


def convert_CT_png(img_arr, data, start, end, meta_data, DCMfolder, PNGfolder, uid):
    origin_instance = data["instances"]
    data['instances'] = {}
    data['instances']['z'] = []
    data['instances']['x'] = []
    data['instances']['y'] = []
    spacing = meta_data['spacing']
    direction = meta_data['direction']
    z_shape, y_shape, x_shape = img_arr.shape
    z_reshape = int(round(z_shape * spacing[2] * 2))
    y_reshape = int(round(y_shape * spacing[1] * 2))
    x_reshape = int(round(x_shape * spacing[0] * 2))

    start_idx_z = int(math.floor(img_arr.shape[0] * start))
    end_idx_z = int(math.floor(img_arr.shape[0] * end) - 1)
    start_idx_x = int(math.floor(img_arr.shape[2] * start))
    end_idx_x = int(math.floor(img_arr.shape[2] * end) - 1)
    start_idx_y = int(math.floor(img_arr.shape[1] * start))
    end_idx_y = int(math.floor(img_arr.shape[1] * end) - 1)

    origin_x = int(-(x_shape / 2.0))
    origin_y = int(-(y_shape / 2.0))
    origin_z = int(-(z_shape / 2.0))
    origin = [origin_x, origin_y, origin_z]
    spacing_x = (float(x_reshape) * 0.5) / float(x_shape)
    spacing_y = (float(y_reshape) * 0.5) / float(y_shape)
    spacing_z = (float(z_reshape) * 0.5) / float(z_shape)
    spacing = [spacing_x, spacing_y, spacing_z]

    # gen x-y images
    for k in range(start_idx_z, end_idx_z + 1):
        url = origin_instance[k]["url"]
        data['instances']['z'].append({
            'instanceID': int(k + 1),
            'rowPixelSpacing': spacing[0],
            'columnPixelSpacing': spacing[1],
            'imagePositionPatient': meta_data["position_patient"],
            'rowCosines': direction[:3],
            'columnCosines': direction[3:6],
            'rows': y_reshape,  # img_arr.shape[1], # width
            'columns': x_reshape,  # img_arr.shape[2], # height
            'url': url
        })

    # gen z-y images
    PNGfilename = 'black.png'
    folder = os.path.join(IMAGE_ROOT, 'black')
    if not os.path.exists(folder):
        os.makedirs(folder)
    PNGpath = os.path.join(folder, PNGfilename)
    if not os.path.exists(PNGpath):
        black_img = np.zeros((500, 500), dtype=np.uint8)
        scipy.misc.imsave(PNGpath, black_img)

    for i in range(start_idx_x, end_idx_x + 1):
        PNGpath = os.path.join(folder, str(uuid.uuid1()) + '.png')
        url = "/api/study_image_black?uid={}".format(PNGpath)
        data['instances']['x'].append({
            'instanceID': int(i + 1),
            'rowPixelSpacing': 0.5,
            'columnPixelSpacing': 0.5,
            'imagePositionPatient': [origin[0] + i * spacing[0], origin[1], origin[2] + z_shape * spacing[2]],
            'rowCosines': direction[3:6],
            'columnCosines': [direction[6], direction[7], -direction[8]],
            'rows': z_reshape,  # width
            'columns': y_reshape,  # height
            'url': url
        })

    # gen z-x images
    for j in range(start_idx_y, end_idx_y + 1):
        PNGpath = os.path.join(folder, str(uuid.uuid1()) + '.png')
        url = "/api/study_image_black?uid={}".format(PNGpath)
        data['instances']['y'].append({
            'instanceID': int(j + 1),
            'rowPixelSpacing': 0.5,
            'columnPixelSpacing': 0.5,
            'imagePositionPatient': [origin[0], origin[1] + j * spacing[1], origin[2] + z_shape * spacing[2]],
            'rowCosines': direction[:3],
            'columnCosines': [direction[6], direction[7], -direction[8]],
            'rows': z_reshape,
            'columns': x_reshape,
            'url': url
        })


def check_dicom_reverse(series):
    """

    :param series:
    :return:
    """
    instances = series['instances']
    DCMfolder = str(os.path.dirname(instances[0]['filepath']))

    try:
        Reader = sitk.ImageSeriesReader()
        DicomNames = Reader.GetGDCMSeriesFileNames(DCMfolder)
        Reader.SetFileNames(DicomNames)
        Reader.Execute()

        instance_0 = DicomNames[0]
        instance_1 = DicomNames[1]
        image_path = DCMfolder
        file_0 = os.path.join(image_path, instance_0)
        file_1 = os.path.join(image_path, instance_1)
        image_0 = sitk.ReadImage(file_0)
        image_1 = sitk.ReadImage(file_1)
        keys_0 = image_0.GetMetaDataKeys()
        keys_1 = image_1.GetMetaDataKeys()

        instanceNumber_0 = 0 if '0020|0013' not in keys_0 else image_0.GetMetaData('0020|0013').rstrip()
        instanceNumber_1 = 1 if '0020|0013' not in keys_1 else image_1.GetMetaData('0020|0013').rstrip()

        reverse = True if int(instanceNumber_0) > int(instanceNumber_1) and USE_INSTANCE_NUMBER else False
        logger.info("reverse: {} image: {}".format(reverse, DCMfolder))
        return reverse
    except:
        for ins in instances:
            ins.pop('filename')
            ins.pop('filepath')
        return False


def insert_CT_instances(series):
    instances = series['instances']
    reverse = False
    DCMfolder = str(os.path.dirname(instances[0]['filepath']))

    try:
        Reader = sitk.ImageSeriesReader()
        DicomNames = Reader.GetGDCMSeriesFileNames(DCMfolder)
        Reader.SetFileNames(DicomNames)
        Image = Reader.Execute()

        instance_0 = DicomNames[0]
        instance_1 = DicomNames[1]
        image_path = DCMfolder
        file_0 = os.path.join(image_path, instance_0)
        file_1 = os.path.join(image_path, instance_1)
        image_0 = sitk.ReadImage(file_0)
        image_1 = sitk.ReadImage(file_1)
        keys_0 = image_0.GetMetaDataKeys()
        keys_1 = image_1.GetMetaDataKeys()

        instanceNumber_0 = 0 if '0020|0013' not in keys_0 else image_0.GetMetaData('0020|0013').rstrip()
        instanceNumber_1 = 1 if '0020|0013' not in keys_1 else image_1.GetMetaData('0020|0013').rstrip()

        reverse = True if int(instanceNumber_0) > int(instanceNumber_1) and USE_INSTANCE_NUMBER else False
        logger.info("reverse: {} image: {}".format(reverse, DCMfolder))
    except:
        for ins in instances:
            ins.pop('filename')
            ins.pop('filepath')
        return False, False

    img_arr = sitk.GetArrayFromImage(Image)
    if len(img_arr.shape) != 3:
        for ins in instances:
            ins.pop('filename')
            ins.pop('filepath')
        return False, False

    minPixelValue, maxPixelValue = cal_hist_ct(img_arr)
    logger.info('cal window [{}:{}]'.format(minPixelValue, maxPixelValue))
    series['minPixelValue'] = int(minPixelValue)
    series['maxPixelValue'] = int(maxPixelValue)
    pixelRange = abs(maxPixelValue - minPixelValue)

    img_arr[img_arr < minPixelValue] = minPixelValue
    img_arr[img_arr > maxPixelValue] = maxPixelValue
    img_arr = (img_arr.astype(np.float32) - minPixelValue) / pixelRange * 255.0
    spacing = list(Image.GetSpacing())
    origin = list(Image.GetOrigin())
    direction = list(Image.GetDirection())

    meta_data = {
        'spacing': spacing,
        'origin': origin,
        'direction': direction
    }

    if not os.path.exists(IMAGE_ROOT):
        os.makedirs(IMAGE_ROOT)
    uid = series['seriesID']
    PNGfolder = os.path.join(IMAGE_ROOT, uid)
    if os.path.exists(PNGfolder):
        shutil.rmtree(PNGfolder)
    os.makedirs(PNGfolder)

    logger.info("begin convert ct image {}".format(DCMfolder))
    # convert png multithread
    THREAD_NUM = 4
    threads = []
    step = 1.0 / THREAD_NUM
    for i in np.arange(0.0, 1.0, step):
        t = ResultThread(target=convert_CT_png,
                         args=(img_arr, series, i, i + step, meta_data, DCMfolder, PNGfolder, uid))
        t.start()
        threads.append(t)
    for t in threads:
        t.join()

    for t in threads:
        if t.exitcode != 0:
            logger.error(t.exc_traceback)
            return reverse, False

    series['instances']['x'] = sorted(series['instances']['x'], key=lambda i: int(i['instanceID']))
    series['instances']['y'] = sorted(series['instances']['y'], key=lambda i: int(i['instanceID']))
    series['instances']['z'] = sorted(series['instances']['z'], key=lambda i: int(i['instanceID']))

    return reverse, True


def insert_dicom_CT_instances2(series):
    instances = series['instances']
    DCMfolder = str(os.path.dirname(instances[0]['filepath']))

    try:
        Reader = sitk.ImageSeriesReader()
        DicomNames = Reader.GetGDCMSeriesFileNames(DCMfolder)
        Reader.SetFileNames(DicomNames)
        Reader.MetaDataDictionaryArrayUpdateOn()
        Reader.LoadPrivateTagsOn()
        Image = Reader.Execute()
        instance_0 = DicomNames[0]
        instance_1 = DicomNames[1]
        image_path = DCMfolder
        file_0 = os.path.join(image_path, instance_0)
        file_1 = os.path.join(image_path, instance_1)
        image_0 = sitk.ReadImage(file_0)
        image_1 = sitk.ReadImage(file_1)
        keys_0 = image_0.GetMetaDataKeys()
        keys_1 = image_1.GetMetaDataKeys()
        instanceNumber_0 = 0 if '0020|0013' not in keys_0 else image_0.GetMetaData('0020|0013').rstrip()
        instanceNumber_1 = 1 if '0020|0013' not in keys_1 else image_1.GetMetaData('0020|0013').rstrip()
        reverse = True if int(instanceNumber_0) > int(instanceNumber_1) and USE_INSTANCE_NUMBER else False
        logger.info("reverse: {} image: {}".format(reverse, DCMfolder))
    except Exception as exc:
        logger.error("read dicom series error. err=%s", exc)
        logger.error(traceback.format_exc())
        for ins in instances:
            ins.pop('filename')
            ins.pop('filepath')
        return False, False

    img_arr = sitk.GetArrayFromImage(Image)
    minPixelValue, maxPixelValue = cal_hist_ct(img_arr)
    logger.info('cal window [{}:{}]'.format(minPixelValue, maxPixelValue))
    series['minPixelValue'] = int(minPixelValue)
    series['maxPixelValue'] = int(maxPixelValue)

    size = Image.GetSize()
    spacing = Image.GetSpacing()
    origin = Image.GetOrigin()

    folder = os.path.join(IMAGE_ROOT, 'black')
    if not os.path.exists(folder):
        os.makedirs(folder)
    z_size = int(round(size[2] * spacing[2] / spacing[0]))
    x_png_path = os.path.join(folder, "black_%s_%s.png" % (z_size, size[0]))
    if not os.path.exists(x_png_path):
        x_black_img = np.zeros((z_size, size[0]), dtype=np.uint8)
        scipy.misc.imsave(x_png_path, x_black_img)
    y_png_path = os.path.join(folder, "black_%s_%s.png" % (z_size, size[1]))
    if not os.path.exists(y_png_path):
        y_black_img = np.zeros((z_size, size[1]), dtype=np.uint8)
        scipy.misc.imsave(y_png_path, y_black_img)

    new_instance = {"x": [], "y": [], "z": []}
    # z axis
    for z in range(size[2]):
        axis_info = {}
        axis_info["instanceID"] = z
        axis_info["rowPixelSpacing"] = spacing[0]
        axis_info["columnPixelSpacing"] = spacing[1]
        axis_info["rowCosines"] = [1, 0, 0]
        axis_info["columnCosines"] = [0, 1, 0]
        axis_info["rows"] = size[0]
        axis_info["columns"] = size[1]
        axis_info["url"] = Reader.GetFileNames()[z]
        axis_info["imagePositionPatient"] = [origin[0], origin[1], origin[2] + z * spacing[2]]
        new_instance["z"].append(axis_info)

    # x axis
    for x in range(size[0]):
        axis_info = {}
        axis_info["instanceID"] = x
        axis_info["rowPixelSpacing"] = spacing[1]
        axis_info["columnPixelSpacing"] = spacing[1]
        axis_info["rowCosines"] = [0, 1, 0]
        axis_info["columnCosines"] = [0, 0, -1]
        axis_info["rows"] = z_size
        axis_info["columns"] = size[1]
        folder = os.path.join(IMAGE_ROOT, 'x_black')
        uid = str(uuid.uuid1()) + '.png'
        axis_info["url"] = "/api/study_image_black?path={}&uid={}".format(x_png_path, uid)
        axis_info["imagePositionPatient"] = [origin[0] + x * spacing[0], origin[1], origin[2] + (size[2]) * spacing[2]]
        new_instance["x"].append(axis_info)

    # y axis
    for y in range(size[1]):
        axis_info = {}
        axis_info["instanceID"] = y
        axis_info["rowPixelSpacing"] = spacing[0]
        axis_info["columnPixelSpacing"] = spacing[0]
        axis_info["rowCosines"] = [1, 0, 0]
        axis_info["columnCosines"] = [0, 0, -1]
        axis_info["rows"] = z_size
        axis_info["columns"] = size[0]
        folder = os.path.join(IMAGE_ROOT, 'y_black')
        uid = str(uuid.uuid1()) + '.png'
        axis_info["url"] = "/api/study_image_black?path={}&uid={}".format(y_png_path, uid)
        # axis_info["imagePositionPatient"] =[origin[0], origin[1] + y * spacing[1], origin[2] + size[2] * spacing[2]]
        axis_info["imagePositionPatient"] = [origin[0], origin[1] + y * spacing[1], origin[2] + (size[2]) * spacing[2]]
        new_instance["y"].append(axis_info)
    series["instances"] = new_instance
    return reverse, True


def insert_dicom_CT_instances(series):
    instances = series['instances']
    reverse = False
    DCMfolder = str(os.path.dirname(instances[0]['filepath']))

    try:
        Reader = sitk.ImageSeriesReader()
        DicomNames = Reader.GetGDCMSeriesFileNames(DCMfolder)
        Reader.SetFileNames(DicomNames)
        Reader.MetaDataDictionaryArrayUpdateOn()
        Reader.LoadPrivateTagsOn()
        Image = Reader.Execute()

        instance_0 = DicomNames[0]
        instance_1 = DicomNames[1]
        image_path = DCMfolder
        file_0 = os.path.join(image_path, instance_0)
        file_1 = os.path.join(image_path, instance_1)
        image_0 = sitk.ReadImage(file_0)
        image_1 = sitk.ReadImage(file_1)
        keys_0 = image_0.GetMetaDataKeys()
        keys_1 = image_1.GetMetaDataKeys()

        instanceNumber_0 = 0 if '0020|0013' not in keys_0 else image_0.GetMetaData('0020|0013').rstrip()
        instanceNumber_1 = 1 if '0020|0013' not in keys_1 else image_1.GetMetaData('0020|0013').rstrip()

        reverse = True if int(instanceNumber_0) > int(instanceNumber_1) and USE_INSTANCE_NUMBER else False
        logger.info("reverse: {} image: {}".format(reverse, DCMfolder))
    except:
        for ins in instances:
            ins.pop('filename')
            ins.pop('filepath')
        return False, False

    img_arr = sitk.GetArrayFromImage(Image)
    if len(img_arr.shape) != 3:
        for ins in instances:
            ins.pop('filename')
            ins.pop('filepath')
        return False, False

    minPixelValue, maxPixelValue = cal_hist_ct(img_arr)
    logger.info('cal window [{}:{}]'.format(minPixelValue, maxPixelValue))
    series['minPixelValue'] = int(minPixelValue)
    series['maxPixelValue'] = int(maxPixelValue)
    pixelRange = abs(maxPixelValue - minPixelValue)

    img_arr[img_arr < minPixelValue] = minPixelValue
    img_arr[img_arr > maxPixelValue] = maxPixelValue
    img_arr = (img_arr.astype(np.float32) - minPixelValue) / pixelRange * 255.0
    spacing = list(Image.GetSpacing())
    origin = list(Image.GetOrigin())
    direction = list(Image.GetDirection())
    position_patient = Reader.GetMetaData(1, "0020|0032").strip().split("\\")
    position_patient = [float(x) for x in position_patient]

    meta_data = {
        'spacing': spacing,
        'origin': origin,
        'direction': direction,
        "position_patient": position_patient
    }

    if not os.path.exists(IMAGE_ROOT):
        os.makedirs(IMAGE_ROOT)
    uid = series['seriesID']
    PNGfolder = os.path.join(IMAGE_ROOT, uid)
    if os.path.exists(PNGfolder):
        shutil.rmtree(PNGfolder)
    os.makedirs(PNGfolder)
    logger.info("zzzzzz convert CT complete")
    convert_CT_png(img_arr, series, 0, 1, meta_data, DCMfolder, PNGfolder, uid)
    logger.info("xxxxx convert CT complete")
    series['instances']['x'] = sorted(series['instances']['x'], key=lambda i: int(i['instanceID']))
    series['instances']['y'] = sorted(series['instances']['y'], key=lambda i: int(i['instanceID']))
    series['instances']['z'] = sorted(series['instances']['z'], key=lambda i: int(i['instanceID']))

    return reverse, True
