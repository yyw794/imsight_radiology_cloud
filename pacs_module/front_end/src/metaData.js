import * as cornerstone from 'cornerstone-core'
import _ from 'underscore'

let generalSeriesMeta = new Map()
let imagePlaneMeta = new Map()

function metaDataProvider(type, imageId) {
  if (type === 'generalSeriesModule') {
    return generalSeriesMeta.get(imageId)
  }
  // image related meta data, like pixelSpacing, pixelRange
  if (type === 'imagePlaneModule') {
    return imagePlaneMeta.get(imageId)
  }
}

// Register this provider with CornerstoneJS
cornerstone.metaData.addProvider(metaDataProvider);

export function addDRMetaData(data) {
  for (let series of Object.values(data.series)) {
    for (let ins of _.flatten(Object.values(series.instances))) {
      let url = ins.url
      generalSeriesMeta.set(url, series)
      imagePlaneMeta.set(url, ins)
    }
  }
}

export function addLiverCTMetaData(data) {
  for (let series of data.series) {
    for (let ins in series.instances.z) {
      let url = series.instances.z[ins].url
      generalSeriesMeta.set(url, data.series)
      imagePlaneMeta.set(url, series.instances.z[ins])
    }
  }
}

export function addCTMetaData(data) {
  console.log(data);
  for (let series of Object.values(data.series)) {
    for (let ins of _.flatten(Object.values(series.instances))) {
      let url = ins.url
      generalSeriesMeta.set(url, series)
      imagePlaneMeta.set(url, ins)
    }
  }
  // for (let ins of _.flatten(Object.values(data.instances))) {
  //   let url = ins.url
  //   generalSeriesMeta.set(url, data)
  //   imagePlaneMeta.set(url, ins)
  // }
}
