#!/bin/python
#coding=utf-8
import json
import os
import sys
import uuid
import threading

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_update_one, db_find_one, db_insert_one, db_find_update_otherwise_insert, \
    db_delete
from models.label.Nodule_Label import format_nodule_label_info
from services.algo.algo_api import detect_dx, detect_lung_nodules, run_nodule_register, run_liver_seg, \
    detect_ribs
from config import DICOM_ROOT, IS_FILTER
import logging

logger = logging.getLogger('radiology')
from models.label.Liver_Label import Liver_Label

mutex = threading.Lock()


def format_DR_predict_new(algo_result, seriesID, instanceID):
    """

    :param algo_result:
    :param seriesID:
    :param instanceID:
    :return: state: 1, red bar; 2, yello bar; 0, green bar.
    """
    state = 0
    result = []
    detect_list = algo_result["detect_list"]
    detect_list = sorted(detect_list, key=lambda x: x["prob"], reverse=True)
    filter_detect_list = []
    if not detect_list:
        return [], state
    if detect_list[0]["prob"] >= 0.9:
        state = 1
        for label in detect_list:
            if label["prob"] < 0.9:
                break
            else:
                filter_detect_list.append(label)
    else:
        state = 2
        filter_detect_list = detect_list[:1]
    for label in filter_detect_list:
        result.append({
            'labelID': "{}-{}-{}-{}".format(label['x1'], label['y1'],
                                            label['x2'] - label['x1'],
                                            label['y2'] - label['y1']),
            'instanceID': instanceID,
            'seriesID': seriesID,
            'globalTag': False,
            'diseaseTag': label['disease'].strip().upper(),
            'prob': label['prob'],
            'x': label['x1'],
            'y': label['y1'],
            'w': label['x2'] - label['x1'],
            'h': label['y2'] - label['y1']
        })
    return result, state


def format_DR_predict(algo_result, seriesID, instanceID):
    result = []
    for label in algo_result:
        result.append({
            'labelID': "{}-{}-{}-{}".format(label['xStart'], label['yStart'],
                                            label['xEnd'] - label['xStart'],
                                            label['yEnd'] - label['yStart']),
            'instanceID': instanceID,
            'seriesID': seriesID,
            'globalTag': label['globalTag'],
            'diseaseTag': label['className'].strip().upper(),
            'prob': label['prob'],
            'x': label['xStart'],
            'y': label['yStart'],
            'w': label['xEnd'] - label['xStart'],
            'h': label['yEnd'] - label['yStart']
        })
    return result


def formant_LUNG_predict(algo_result, seriesID):
    for nodule in algo_result['nodules']:
        nodule['labelID'] = "{}-{}-{}".format(nodule['x'], nodule['y'], nodule['z'])
        nodule['instanceID'] = nodule['z']
        nodule['seriesID'] = seriesID
        nodule['addByUser'] = False
    return algo_result['nodules']


def format_Liver_Tumor(predict_list, seriesID, seriesNumber):
    result = []
    for predict in predict_list:
        label = Liver_Label()
        label.labelID = "{}-{}-{}".format(predict['coord']['x'],
                                          predict['coord']['y'],
                                          predict['coord']['z'])
        label.seriesID = seriesID
        label.state = 'assign'
        label.x = predict['coord']['x']
        label.y = predict['coord']['y']
        label.z = predict['coord']['z']
        label.width = predict['width']
        label.height = predict['height']
        label.volume = predict['vol']
        label.diameter = predict['diam']
        label.p1_x = predict['diam_p1']['x']
        label.p1_y = predict['diam_p1']['y']
        label.p2_x = predict['diam_p2']['x']
        label.p2_y = predict['diam_p2']['y']
        label.avgHU = predict["avgHU"]
        label.malg = predict["malg"]
        label.seriesNumber = seriesNumber
        result.append(label.to_db())
    return result


def format_ribs_predict(predict_list, series_uid):
    labels = []
    for predict in predict_list["data"]:
        labels.append(
            {'labelID': "{}-{}-{}-{}-{}-{}-{}".format(
                predict['x_min'], predict['x_max'],
                predict['y_min'], predict['y_max'],
                predict['z_min'], predict['z_max'],
                predict["z"]),
                "seriesUID": series_uid,
                "prob": predict["probs"],
                "x_min": predict['x_min'],
                "x_max": predict["x_max"],
                "y_min": predict["y_min"],
                "y_max": predict["y_max"],
                "z_min": predict["z_min"],
                "z_max": predict["z_max"],
                "z": predict["z"]
            })
    return labels


def nodule_register(study_fixed, moving_study):
    logger.info("begin nodule_register")
    data_fixed = {"dcm_path": os.path.join(DICOM_ROOT, study_fixed['series'][0]['seriesID']), "dicom": ""}
    data_moving = {"dcm_path": os.path.join(DICOM_ROOT, moving_study['series'][0]['seriesID']), "dicom": ""}

    taskID_moving = moving_study['taskID']
    taskID_fixed = study_fixed['taskID']
    logger.debug("nodule register. task_moving is %s, task_fixed is %s", taskID_moving, taskID_fixed)
    label_info_fixed = db_find_one('label', {'taskID': taskID_fixed})
    label_info_moving = db_find_one('label', {'taskID': taskID_moving})
    data = {
        'fixedNoduleList': label_info_fixed['labels'],
        'movingNoduleList': label_info_moving['labels']
    }

    registerNodules = {"fixed": [], "moving": []}
    if len(data['fixedNoduleList']) == 0 or \
            len(data['movingNoduleList']) == 0:
        logger.info("not noudle list found. task_id=%s, task_id=%s", taskID_fixed, taskID_moving)
        db_find_update_otherwise_insert('ai_label', {'taskID_fixed': taskID_fixed, 'taskID_moving': taskID_moving},
                                        {'taskID_fixed': taskID_fixed, 'taskID_moving': taskID_moving,
                                         'registerNodules': registerNodules})
        return registerNodules

    resp = run_nodule_register(data, data_fixed, data_moving)
    resp = resp.json()
    for coord_fixed, coord_mov in zip(resp['data']['fixed'], resp['data']['moving']):
        fixed = None
        moving = None
        for nod in data['fixedNoduleList']:
            if nod['x'] == coord_fixed['x'] and nod['y'] == coord_fixed['y'] and nod['z'] == coord_fixed['z']:
                fixed = nod
                break
        for nod in data['movingNoduleList']:
            if nod['x'] == coord_mov['x'] and nod['y'] == coord_mov['y'] and nod['z'] == coord_mov['z']:
                moving = nod
                break

        if fixed and moving:
            data['fixedNoduleList'].remove(fixed)
            data['movingNoduleList'].remove(moving)
            fixed["+/-"] = "/"
            moving["+/-"] = "/"
        else:
            continue
        registerNodules['fixed'].append(fixed)
        registerNodules['moving'].append(moving)

    for fixed_nod in data['fixedNoduleList']:
        fixed_nod["+/-"] = "-"
        registerNodules['fixed'].append(fixed_nod)
        registerNodules['moving'].append(fixed_nod)
    for mov_nod in data["movingNoduleList"]:
        mov_nod["+/-"] = "+"
        registerNodules['fixed'].append(mov_nod)
        registerNodules['moving'].append(mov_nod)

    if not registerNodules["fixed"] or not registerNodules["moving"]:
        logger.debug("match empty. run nodule register response body: %s", resp["data"])
    db_find_update_otherwise_insert('ai_label', {'taskID_fixed': taskID_fixed, 'taskID_moving': taskID_moving},
                                    {'taskID_fixed': taskID_fixed, 'taskID_moving': taskID_moving,
                                     'registerNodules': registerNodules})
    return registerNodules


def filter_exam_body_part(modality, bodypart, filter_modality, filter_bodypart):
    if IS_FILTER:
        if modality in filter_modality and bodypart == filter_bodypart:
            return True
        else:
            return False
    else:
        if modality in filter_modality:
            return True
        else:
            return False


def predict(study, userID, taskID):
    modality = study['modality']

    # already has result
    # seriesIDs = [s['seriesID'] for s in study['series']]

    # if len(seriesIDs) == 1:
    #     label_info = db_find_one('ai_label', {'studyUID': study['studyUID'], 'seriesID': seriesIDs[0]})
    # else:
    #     label_info = db_find_one('ai_label', {'studyUID': study['studyUID']})
    #
    # if label_info is not None and label_info.has_key('label_info'):
    #     logger.info("AI result already exist for studyUID {}".format(study['studyUID']))
    #     user_label_info = db_find_one('label', {"taskID": taskID})
    #     for label in label_info['labels']:
    #         label['state'] = 'assign'
    #         if label_info['labelType'] == 'Nodule':
    #             label['url'] = ''
    #         user_label_info['labels'].append(label)
    #     db_update_one('label', {"taskID": taskID}, user_label_info)
    #     return label_info
    db_delete('ai_label', {'studyUID': study['studyUID']})

    logger.debug("modality is %s, body exam is %s, task_id is %s", modality, study['bodyPartExamined'], taskID)
    # if modality in ['DX', 'DR', 'CR'] and 'CHEST' == study['bodyPartExamined']:
    if filter_exam_body_part(modality, study['bodyPartExamined'], ["DX", "DR", "CR"], "CHEST"):
        logger.info("userID {} studyUID {} invoke DX chest algorithm".format(userID, study['studyUID']))
        label_info = {}
        label_info['studyUID'] = study['studyUID']
        label_info['labelType'] = 'DX'
        label_info['labels'] = []
        label_info["disease"] = []
        algo_result = False

        if study["birthDate"]:
            age = (datetime.datetime.today() - study['birthDate']).days // 365
            if age < 10:
                logger.error("patient age is under 16. no supported")
                return None
        for series in study['series']:
            for instance in series['instances']:
                if instance['viewPosition'] == 'PA':
                    dcm = instance['url']
                    if os.path.exists(dcm):
                        logger.info("userID {} seriesUID {} invoke DX algorithm".format(userID, series['seriesID']))
                        resource = {"dcm_path": os.path.dirname(dcm), "dicom": os.path.basename(dcm)}
                        resp = detect_dx(resource)
                        if not resp:
                            logger.info("userID {} studyUID {} dx detect fail".format(userID, study['studyUID']))
                            continue
                        algo_result = True
                        resp = resp.json()
                        resp = resp['data']
                        # formated_result = format_DR_predict(resp['data'], series['seriesID'], instance['instanceID'])
                        db_update_one("study", {"studyUID": study["studyUID"]}, {"is_negative": resp["is_negative"]})
                        formated_result = format_DR_predict_new(resp, series['seriesID'], instance['instanceID'])
                        label_info['labels'].extend(formated_result[0])
                        label_info["disease_state"] = formated_result[1]
                        label_info["disease"].extend([x.upper() for x in resp["disease_list"]])
                        if not label_info["disease"]:
                            label_info["disease_state"] = 0
                        for label in label_info["labels"]:
                            if label["diseaseTag"] not in resp["disease_list"]:
                                label_info["disease"].append(label["diseaseTag"])
                    else:
                        logger.info("dx data not exist {}".format(dcm))
                else:
                    logger.info("invalid dx view positin {}".format(instance['viewPosition']))
        if algo_result is False:
            return None

        db_insert_one('ai_label', label_info)
        for label in label_info['labels']:
            label['state'] = 'assign'
        logger.info("save dx label to db userID {} taskID {}".format(userID, taskID))
        db_find_update_otherwise_insert('label', {"taskID": taskID, "labelType": "DX"}, label_info)
        return label_info

    # if study['modality'] == 'CT' and 'CHEST' in study['bodyPartExamined']:
    if filter_exam_body_part(modality, study['bodyPartExamined'], ["CT"], "CHEST"):
        logger.info("userID {} studyUID {} invoke CT chest algorithm".format(userID, study['studyUID']))
        label_info = {}
        label_info['studyUID'] = study['studyUID']
        label_info['labelType'] = 'Nodule'
        label_info['labels'] = []

        for series in study['series']:
            logger.info("userID {} seriesUID {} invoke lung nodule algorithm".format(userID, series['seriesID']))
            resource = {"dcm_path": os.path.join(DICOM_ROOT, series['seriesID']), "dicom": ""}

            # seg_resp = run_lung_seg(resource)
            # if not seg_resp:
            #     logger.info("lung seg fail")
            #     return None
            #
            # resource['lungBBox'] = seg_resp.json()['data']

            resp = detect_lung_nodules(resource)

            if not resp:
                logger.info("userID {} studyUID {} nodule detect fail".format(userID, study['studyUID']))
                return None

            resp = resp.json()
            formated_result = formant_LUNG_predict(resp['data'], series['seriesID'])
            label_info['labels'].extend(formated_result)
            logger.info("insert ai result for study {}".format(study['studyUID']))
            db_insert_one('ai_label', label_info)
            db_insert_one('ai_label', {
                'studyUID': study['studyUID'],
                'seriesID': series['seriesID'],
                'labelType': 'lungBBox',
                'leftLungBBox': resp['data']['leftLungBBox'],
                'rightLungBBox': resp['data']['rightLungBBox']
            })

            # only process longest series which should be first one
            break
        for label in label_info['labels']:
            label['state'] = 'assign'
            if label_info['labelType'] == 'Nodule':
                label['url'] = ''
        logger.info("save nodule label to db userID {} taskID {}".format(userID, taskID))
        label_format_info = format_nodule_label_info(label_info)
        db_find_update_otherwise_insert('label', {"taskID": taskID, "labelType": "Nodule"}, label_format_info)
        return label_info

    if study['modality'] == 'CT' and (study['bodyPartExamined'] == 'LIVER' or study['bodyPartExamined'] == "ABDOMEN"):
        logger.info("userID {} studyUID {} invoke liver algorithm".format(userID, study['studyUID']))

        label_info = {}
        label_info['studyUID'] = study['studyUID']
        label_info['labelType'] = 'LiverTumor'
        label_info['labels'] = []

        for series in study['series']:
            logger.info("checking liver study {} series {}".format(study['studyUID'], series['seriesID']))

            resource = {"dcm_path": os.path.join(DICOM_ROOT, series['seriesID']), "dicom": ""}
            if os.path.exists(resource['dcm_path']):
                seg_resp = run_liver_seg(resource['dcm_path'])

                if not seg_resp:
                    logger.info("run liver fail")
                    return None

                predict_list = json.loads(seg_resp.json()['data'])['predict']
                label_info['labels'].extend(
                    format_Liver_Tumor(predict_list, series['seriesID'], series['seriesNumber']))
        db_insert_one('ai_label', label_info)
        user_label_info = {}
        user_label_info["label"] = []
        user_label_info["labelType"] = "LiverTumor"
        for label in label_info['labels']:
            label['state'] = 'assign'
            user_label_info['labels'].append(label)
        db_update_one('label', {"taskID": taskID, "labelType": "LiverTumor"}, user_label_info)
        mutex.release()
        return label_info

    # unsupport modality or body part
    return None


def do_ribs_detect(study_uid, task_id):
    study_info = db_find_one("study", {"studyUID": study_uid})
    if not study_info:
        logger.error("not found study_uid in study table.")
    logger.info("do ribs detect. studyUID=%s", study_info["studyUID"])
    label_list = []
    for series_info in study_info["series"]:
        series_id = series_info["seriesID"]
        img_resource = os.path.join(DICOM_ROOT, series_id)
        resp = detect_ribs(img_resource)
        if not resp:
            logger.error("detect ribs error. studyUID=%s", study_info["studyUID"])
            return None
        else:
            result = resp.json()
            label_info = format_ribs_predict(result, series_id)
            label_list.extend(label_info)
        break
    label_info = {"labelType": "Rib", "labels": label_list, "studyUID": study_info["studyUID"]}
    db_insert_one('ai_label', label_info)
    db_find_update_otherwise_insert("label", {"taskID": task_id, "labelType": "Rib"}, label_info)
    return label_info

import datetime

if __name__ == '__main__':
    new_task = {
        "taskID": "888",
        "reporter": "user1",
        "reviewer": "user2",
        "type": "normal",
        "state": 'assign',
        "studyUID": "1.2.392.200036.9116.2.5.1.48.1221404901.1524567640.588109"
    }
    db_insert_one('task', new_task)

    new_label = {
        "studyUID": "1.2.392.200036.9116.2.5.1.48.1221404901.1524567640.588109",
        "taskID": "888",
        "labelType": "Nodule",
        "labels": []
    }
    db_insert_one('label', new_label)

    study_info = db_find_one('study', {"studyUID": "1.2.392.200036.9116.2.5.1.48.1221404901.1524567640.588108"})
    study_info['studyUID'] = "1.2.392.200036.9116.2.5.1.48.1221404901.1524567640.588109"
    study_info['studyDate'] = datetime.datetime.today()

    label_info = predict(study_info, "user1", "888")

    # print label_info
