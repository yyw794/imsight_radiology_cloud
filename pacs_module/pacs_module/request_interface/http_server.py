# -*- coding: UTF-8 -*-
from flask import Flask,make_response,request,jsonify
from pacs_interface import (send_echo_request,send_find_request,send_move_request,store_listen)
from request_image import doctor_high_priority_request,start_request_,get_insert_into_studyList
import initconfig as config
import json
from http_client import login_request,insert_a_patient_list
from request_interface.initconfig import get_if_push_model
import logging
import logger
from mongodb_interface import *
import thread
log = logging.getLogger("PACSModule.http_server")

app = Flask(__name__)


# todo request with patientID...
@app.route('/api/find_patient_list', methods=['GET'])
def find_patient_list():

    if get_if_push_model():
        request_list_json = {}
        request_list_json["data"] = []
        request_list_json["message"] = 'unsupport'
        request_list_json["statusCode"] = '1001'
        return json.dumps(request_list_json, ensure_ascii=False)

    studydate = request.values.get("studyDate")
    patientID = request.values.get("patientID")
    patientName = request.values.get("patientName")
    gender = request.values.get("gender")
    birthDate = request.values.get("birthDate")
    
    condition = {}
    same_number_pacs = False
    if studydate is not None:
        condition['study_date'] = studydate
        same_number_pacs = True
    else:
        if patientID is not None:
            condition['patient_id'] = patientID
        if patientName is not None:
            condition['patient_name'] = patientName        
        if gender is not None:
            if 'FEMALE' == gender:
                condition['patient_sex'] = 'F'
            else:
                condition['patient_sex'] = 'M'
        if birthDate is not None:
            condition['patient_birth_date'] = birthDate
        studydate = 'none'      
        
    conditionlist = send_find_request(condition)
    request_list = get_insert_into_studyList(studydate,conditionlist,same_number_pacs)
    
    request_list_json = {}
    request_list_json["data"] = request_list
    request_list_json["message"] = 'OK'
    request_list_json["statusCode"] = '1000'
    return json.dumps(request_list_json,ensure_ascii=False)
    

@app.route('/api/doctor_request', methods = ['GET'])
def doctor_request():
    
    if get_if_push_model():
        request_list_json = {}
        request_list_json["data"] = []
        request_list_json["message"] = 'unsupport'
        request_list_json["statusCode"] = '1001'
        return json.dumps(request_list_json,ensure_ascii=False)
    
    
    respose_json = {}
    study_instance_uid = request.values.get("studyUID")
    study_date = request.values.get("studyDate")
    
    if study_instance_uid is not None or study_date is not None:
        respose_json["message"] = 'INVALID PARAMETERS'
        respose_json["statusCode"] = '1001'
        
    study_instance_info = get_study_instance_list({'_id':study_instance_uid})
    
    # doctor request
    if study_instance_info is not None and study_instance_info['study_state'] == UnKonwType and study_instance_info['is_high_priority']:
        respose_json["message"] = 'TASK EXIT, PLEASE WAIT'
        respose_json["statusCode"] = '1010'
    elif study_instance_info is not None:
        study_instance_info['is_high_priority'] = True
        thread.start_new_thread(doctor_high_priority_request, (study_instance_info,))
        respose_json["message"] = 'OK'
        respose_json["statusCode"] = '1000'
    else:
        si = Study_Instance_List()
        si._id = study_instance_uid
        si.study_date = study_date
        si.is_high_priority = True
        thread.start_new_thread(doctor_high_priority_request, (si.to_dict(),))
        respose_json["message"] = 'OK'
        respose_json["statusCode"] = '1000'
    
    respose_json['data'] = {}

    return json.dumps(respose_json,ensure_ascii=False)


@app.route('/')
def other():
    respose_json = {}
    respose_json['data'] = {}
    respose_json["message"] = 'INVALID PARAMETERS'
    respose_json["statusCode"] = '1001'
    return json.dumps(respose_json,ensure_ascii=False)


def start_server():
    app.run(debug=True, threaded=True, host=config.get_http_listen_ip(), port = config.get_http_listen_port())










