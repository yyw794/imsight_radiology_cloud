import React, { Component } from 'react'
import { Form, Icon, Input, Button, Layout, message } from 'antd'
import {Link} from 'react-router-dom'
const FormItem = Form.Item
const { Content } = Layout
import {authenticate} from '../../auth'
import {Redirect} from 'react-router-dom'
import { injectIntl, FormattedMessage } from 'react-intl'

//import LanguageDropdownList from '../../component/LanguageDropdownList'
import LanguageSwitchBox from '../../component/LanguageSwitchBox'
import { GetLocaledText } from '../../localedFuncs'
import './index.less'

class LoginPage extends Component {
  // state = {
  //   redirectToReferrer: false
  // }
  constructor(props) {
    super(props)
    this.state = {
      redirectToReferrer: false,
    }
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  handleSubmit = (e) => {
      e.preventDefault()
      this.props.form.validateFields((err, values) => {
        if (!err) {
          authenticate(values, () => {
            this.setState({redirectToReferrer: true})
          }, err => {
            const { setFields } = this.props.form
            if (err.payload.statusCode === 1104) {
              setFields({
                account: {
                  errors:[new Error(GetLocaledText(this, "LoginPage.error.acc-not-exist"))],
                  value: values.account
                }
              })
            } else if (err.payload.statusCode === 1103) {
              setFields({
                password: {
                  errors:[new Error(GetLocaledText(this, "LoginPage.error.invalid-password"))],
                  value: values.password
                }
              })
            } else {
              message.error(GetLocaledText(this, "LoginPage.error.login-failure"))
            }
          })
        }
      })
  }
  componentDidMount()
  {
    authenticate('',()=>{
      console.log("auto login successed")
      this.setState({redirectToReferrer: true})
    },err=>{
      console.log("auto login failed")
    })
  }
  render() {
    // check redirect
    const { from } = this.props.location.state || { from: { pathname: "/" } };
    let redirectToReferrer = null
    if (this.state.redirectToReferrer) {
      redirectToReferrer = this.state.redirectToReferrer;
    }
    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }

    // render login page
    const { getFieldDecorator, setFields } = this.props.form
    return (
      <div className='page login-page'>
        <div className="QRcode">
          <img src={require('../../../static/images/qrcode.png')}/>
          <span className="wechat"><FormattedMessage id='LoginPage.wechatID'/></span>
        </div>
        <a className="website" href="http://www.imsightmed.com/">www.imsightmed.com</a>
        <Content className='content'>
          <Form onSubmit={this.handleSubmit} className="login-form" >
            <p className='title'>
              视见科技影像辅助诊断平台
            </p>
            <p className='engtitle'>Imsight Radiology Platform<sup>TM</sup></p>
            <p className='version'>IRCP{localStorage.getItem("version")}</p>
            <FormItem>
              {getFieldDecorator('account', {
                // rules: [{ required: true, message: GetLocaledText(this, "LoginPage.username.alert") }],
                rules: [{ required: true, message: GetLocaledText(this, "LoginPage.username.alert") }],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={GetLocaledText(this, "LoginPage.username")} />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: GetLocaledText(this, "LoginPage.password.alert") }],
              })(
                <Input onCopy={() => false} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder={GetLocaledText(this, "LoginPage.password")} />
              )}
            </FormItem>
            <div className='links'>
              <Link to='/reset'><FormattedMessage id='LoginPage.forgot-password'/></Link>
              <Link to='/register'><FormattedMessage id='LoginPage.register'/></Link>
            </div>
            <FormItem>
              <Button type="primary" htmlType="submit" className="login-form-button">
                <FormattedMessage id='LoginPage.submit.button'/>
              </Button>
              <LanguageSwitchBox className="language-button" setAppState={this.props.setAppState} />
            </FormItem>
            {/*<LanguageDropdownList setAppState={(state) => this.setState(state)}/>*/}
          </Form>
        </Content>
      </div>
    )
  }
}

LoginPage = Form.create()(LoginPage)

export default injectIntl(LoginPage);
