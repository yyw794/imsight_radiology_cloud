# -*- coding: utf-8 -*-
from pacs_interface import (send_echo_request,send_find_request,send_move_request,store_listen,thread_upload_image_path,_send_find_request_)
from multiprocessing import Process
from ConcurrentQueue import ConcurrentQueue
import time
import datetime
from mongodb_interface import *
import thread
import initconfig as config
import os
import shutil
import logging
import logger
from http_client import upload_image_path,insert_a_patient_list,insert_patient_info_list,insert_study_state
import operator
log = logging.getLogger("PACSModule.request_image")

un_request_queue = ConcurrentQueue(3000)


def getYesterday(): 
    today = datetime.date.today()
    oneday = datetime.timedelta(days=1)
    yesterday = today-oneday
    return yesterday.strftime('%Y%m%d')


def after_receive_complete(study_instance_uid, study_date):
    study_instance_uid_path = config.get_store_path_url()+"/"+study_date + "/" + study_instance_uid
    log.info('after_receive_complete: begin:' + study_instance_uid_path)
    
    study_instance_info = get_study_instance_list({'_id': study_instance_uid})
    if os.path.exists(study_instance_uid_path) and study_instance_info['assoc_ac'] != "" :
        request_state = study_instance_info['assoc_ac_state']
        if request_state != Abort:
            study_instance_info['study_state'] = ReceiveComplate
            study_instance_list_insert_update({'_id': study_instance_uid}, study_instance_info)
            thread.start_new_thread(thread_upload_image_path, (study_instance_uid_path,))
            log.info('upload_image_path end' + study_instance_uid_path)
        else:
            log.error("after_receive_complete receive data error, remove the folder"+str(request_state))
            shutil.rmtree(study_instance_uid_path)
            # un_request_queue.put(study_instance_uid_path)
            
    else:  # not ct or dx
        log.warn(study_instance_uid + " may be not support study type")
        study_instance_info['study_state'] = Unsupport
        study_instance_list_insert_update({'_id': study_instance_uid}, study_instance_info)
        insert_study_state(study_instance_info['study_date'], study_instance_uid, Unsupport)
    log.info('after_receive_complete: end:' + study_instance_uid)


def high_priority_after_receive_complete(study_instance_uid, study_date):
    study_instance_uid_path = config.get_store_path_url()+"/"+study_date + "/" + study_instance_uid
    
    study_instance_info = get_study_instance_list({'_id': study_instance_uid})
    if os.path.exists(study_instance_uid_path) and study_instance_info['assoc_ac'] != "":
        log.info(study_instance_uid + "receive OK")
        request_state = study_instance_info['assoc_ac_state']
        if request_state != Abort:
            study_instance_info['study_state'] = ReceiveComplate
            study_instance_list_insert_update({'_id': study_instance_uid}, study_instance_info)
            thread.start_new_thread(thread_upload_image_path, (study_instance_uid_path,))
            log.info('upload_image_path end ' + study_instance_uid_path)
        else:
            log.error("high_priority_after_receive_complete receive data error, remove the folder"+str(request_state))
            shutil.rmtree(study_instance_uid_path)
            # un_request_queue.put(study_instance_uid_path)
    else:
        log.info(study_instance_uid + "not support")
        study_instance_info['study_state'] = Unsupport
        study_instance_list_insert_update({'_id': study_instance_uid}, study_instance_info)
        insert_study_state(study_instance_info['study_date'], study_instance_uid, Unsupport)


# return: [{study_instance_uid:12334.45446.645,study_date:20180113,patient_id:011235,patient_name: zhang san,
# patient_sex:f/m,patient_birth_date:19920506}]
# patient = {'studyUID':'123','studyDate':'20180603','birthDate':'20180302','hospital':'aaa',
# 'bodyPartExamined':'CHEST','gender':"MALE/FEMALE",'modality':'CT','patientID':'11','patientName':'22'}
def get_patient_from_condition(condition):
    patient = {}
    patient['studyUID'] = condition['study_instance_uid']
    patient['studyDate'] = condition['study_date']
    patient['birthDate'] = condition['patient_birth_date']
    patient['patientID'] = condition['patient_id']
    patient['patientName'] = condition['patient_name'].decode("GBK")
    if condition['patient_sex'] == 'F':
        patient['gender'] = 'FEMALE'
    else:
        patient['gender'] = 'MALE'
    patient['bodyPartExamined'] = ''
    patient['hospital'] = ''
    patient['modality'] = condition['modality']
    patient['state'] = 'meta_only'
    if len(condition['study_time']) >= 6:
        patient['studyDate'] = patient['studyDate']+":"+ condition['study_time'][0:6]
    
    return patient


# get json data to flask
# {studyDate:studyDate,same_number_pacs:same_number_pacs,data_list[]}
def get_insert_into_studyList(date_time, condition_list, same_number_pacs):
    study_date = {}
    study_date['studyDate'] = date_time
    study_date['same_number_pacs'] = same_number_pacs
    data_list = []
    for condition_temp in condition_list:
        data_list.append(get_patient_from_condition(condition_temp))

    study_date['data_list'] = data_list
    return study_date


# 3 - 5
def request_history_list(last_hour):
    system_label_instance = get_system_label()
    install_date = datetime.datetime.strptime(str(system_label_instance['install_date']),'%Y%m%d')
    history_date = datetime.datetime.strptime(str(system_label_instance['list_back_date']),'%Y%m%d')
    history_image_date = datetime.datetime.strptime(str(system_label_instance['list_back_image_date']),'%Y%m%d')
    last_day = datetime.timedelta(days = 1)

    while (config.get_history_patient_list_days() > (install_date - history_date).days) and (int(datetime.datetime.now().strftime('%H')) < last_hour):
        last_datetime = history_date - last_day
        last_datetime_str = last_datetime.strftime('%Y%m%d')
        log.info('request_history_list begin request ' + last_datetime_str)
        
        condition = {'study_date': last_datetime_str}
        condition_list = send_find_request(condition)
        
        new_condition_list = add_conditionList_to_db(condition_list)  # add to db
        study_date = get_insert_into_studyList(last_datetime.strftime('%Y%m%d'), condition_list, True)  # add to flask
        log.info(insert_patient_info_list(study_date))

        update_current_list_back_date(last_datetime_str)
        history_date = last_datetime
        
    while (config.get_history_patient_list_image_days() > (install_date - history_image_date).days) and (int(datetime.datetime.now().strftime('%H')) < last_hour):
        last_datetime = history_image_date - last_day
        last_datetime_str = last_datetime.strftime('%Y%m%d')
        
        new_conditionlist = get_all_study_instance_list({'study_date': last_datetime_str, "study_state": UnKonwType})
        if new_conditionlist is not None:
            log.info("request_history_list_image begin request " + last_datetime_str)

            for new_condition in new_conditionlist:
                log.info(new_condition)
                send_move_request(new_condition['_id'])
                after_receive_complete(new_condition['_id'], new_condition['study_date'])
                if int(datetime.datetime.now().strftime('%H')) >= last_hour:
                    return

            update_current_list_back_image_date(last_datetime_str)
            history_image_date = last_datetime
        else:
            update_current_list_back_image_date(last_datetime_str)
            history_image_date = last_datetime


def do_un_request_queue():
    while un_request_queue.size() > 0:
        study_instance_uid = un_request_queue.get()
        log.info('do_un_request_queue begin request a study_instanceUID ' + study_instance_uid+" remain number is "+str(un_request_queue.size()))
        send_move_request(study_instance_uid)
        log.info('do_un_request_queue end '+ study_instance_uid)
        after_receive_complete(study_instance_uid)


support_modality = ['CT', 'CR', 'DR', 'DX']


def add_conditionList_to_db(condition_list):
    new_condition_list = []
    for condition in condition_list:
        
        if condition['modality'] != "" and condition['modality'] not in support_modality:
            continue
        if get_study_instance_list({'_id': condition['study_instance_uid']}) is not None:
            continue
            
        # 深圳市人民医院已登记的病例 每次返回的uid不同 因为还没有拍照
        if condition['study_time'] != "":
            current_time = datetime.datetime.now().strftime('%H%M%S').decode('utf-8')
            if condition['study_time'] > current_time:
                continue
        
            db_instance = get_study_instance_list({'patient_id': condition['patient_id']})
            if db_instance is not None and db_instance['study_time'] == condition['study_time']:
                continue
        
        new_condition_list.append(condition)
        si = Study_Instance_List()
        si._id = condition['study_instance_uid']
        si.assoc_ac = ""
        si.assoc_ac_state = ""
        si.study_date = condition['study_date']
        si.patientid = condition['patient_id']
        si.patientname = condition['patient_name'].decode("GBK")
        si.patientsex = condition['patient_sex']
        si.patientbirthdate = condition['patient_birth_date']
        si.study_state = UnKonwType
        si.is_high_priority = False
        si.modality = condition['modality']
        si.study_time = condition['study_time']

        try:
            study_instance_list_insert_update({'_id': condition['study_instance_uid']}, si.to_dict())
        except Exception as e:
            log.error(e)
            log.error("insert data to db error {}".format(si.to_dict()))
    return new_condition_list


# 每次提取前若干个数据
def update_unConditionlist(unConditionlist):
    if unConditionlist is None:
        return []
    
    sorted_condition_list = sorted(unConditionlist, key=operator.itemgetter('study_time'), reverse=True) 
    log.info('update_unConditionlist total unsend number is ' + str(len(sorted_condition_list)))

    return sorted_condition_list if len(sorted_condition_list) < 50 else sorted_condition_list[0:50]


def deal_this_day_list(study_date, same_number_pacs):
    condition = {'study_date': study_date}
    
    # step 1: find this day study_instance_uid list
    conditionlist = send_find_request(condition)
    log.info("find this day " + study_date + " list number is " + str(len(conditionlist)))

    # step 2: insert this day study_instance_uid into db
    new_condition_list = add_conditionList_to_db(conditionlist)
    log.info("add number this day "+study_date+" is "+str(len(new_condition_list)))
    
    if len(new_condition_list) > 0:
        study_date_list = get_insert_into_studyList(study_date, conditionlist, same_number_pacs)
        log.info(insert_patient_info_list(study_date_list))
    
    # step 3: get un_request study_instance_uid list
    unConditionlist = get_all_study_instance_list({"study_date": study_date, "study_state": UnKonwType})
    
    sorted_condition_list = update_unConditionlist(unConditionlist)
    
    log.info("get un_request number this day " + str(len(sorted_condition_list)))

    for index, sorted_condition in enumerate(sorted_condition_list):
        log.info('do_un_request_queue begin request a study_instanceUID ' +
                 sorted_condition['_id'] + " remain number is " + str(len(sorted_condition_list) - index))
        log.info(sorted_condition)
        send_move_request(sorted_condition['_id'])
        log.info('do_un_request_queue end ' + sorted_condition['_id'])
        after_receive_complete(sorted_condition['_id'], study_date)

    if len(sorted_condition_list) > 5:
        return True
    else:
        return False


def today_request_function():
    while True:
        today = datetime.datetime.now().strftime('%Y%m%d')
        log.info('begin request current day patient list')
        if False is deal_this_day_list(today, False):
            time.sleep(60 * 3)  # three minutes


def time_task_a_hour():
    # request_history_list(20)
    while True:
        current_hour = int(datetime.datetime.now().strftime('%H'))
        # last time do request yesterday
        if 0 == current_hour:
            yesterday = getYesterday()
            # deal_this_day_list(yesterday,True)
            log.info("time_task_a_hour last time to get yestday list " + yesterday)
            
            # insert yesterday to falsk
            condition = {'study_date': yesterday}
            conditionlist = send_find_request(condition)
            study_date_list = get_insert_into_studyList(yesterday, conditionlist, True)
            log.info(insert_patient_info_list(study_date_list))
            
        if 3 == current_hour:
            thread.start_new_thread(request_history_list, (5,))
        time.sleep(60 * 60)


def init_do_something():
    today = datetime.datetime.now().strftime('%Y%m%d')
    insert_system_label(today)
    
    if False is os.path.exists('logfile'):
        os.mkdir('logfile')
    return True


def doctor_high_priority_request(study_instance_info):
    log.info('doctor_high_priority_request begin request a study_instanceUID ')
    log.info(study_instance_info)
    study_instance_list_insert_update({'_id': study_instance_info['_id']}, study_instance_info)
    
    send_move_request(study_instance_info['_id'])
    log.info('doctor_high_priority_request receive end')
    high_priority_after_receive_complete(study_instance_info['_id'], study_instance_info['study_date'])


def start_request_():
    thread.start_new_thread(today_request_function, ())
    thread.start_new_thread(time_task_a_hour, ())
    
    try:
        listen_process = Process(target=store_listen, args=(config.get_AppTitle(), config.get_ListenPACSPort(), ""))
        listen_process.start()

        listen_process.join()
    except KeyboardInterrupt:
        log.error("create listen_process thread error")


def _test_function_():
    log.info((datetime.datetime.now() - datetime.timedelta(minutes=30)).strftime("%Y-%m-%d %H:%M:%S"))
    
    log.info('send_echo_request:' + str(send_echo_request()))
    
    condition = {'study_date': '20180807'}
    conditionlist = _send_find_request_(condition)
    for c in conditionlist:
        log.info(c)

#  https://github.com/pydicom/pynetdicom3

