/**
 *
 * @public {string}
 */
const SET_LANGUAGE = 'setLanguage'
/**
 * @public function setLanguage 设置语言函数
 * @param lang 语言类型
 * @returns {{type: string, lang: *}}
 */
export const setLanguage = lang => ({type: SET_LANGUAGE, lang})

export default {
  SET_LANGUAGE,
}
