import React from 'react';
import createReactClass from 'create-react-class'
import { Spin, Alert } from 'antd'

import './style.less';

/**
 * 进度条加载组件
 * @param Component  组件
 * @param provideData 填入组件的数据
 * @returns {Component}
 * @constructor
 */
export default function LoadingWrapper(Component, provideData) {
    return createReactClass({
        getInitialState() {
            return {
                readyState: 'loading',
                data: null,
                errorMessage:''
            };
        },
        componentDidMount() {
            provideData(this.props)
            .then(data => this.setState({
                readyState: 'loaded',
                data: data
            }))
            .catch(err => {
                console.log(err);
                this.setState({readyState: 'error',
                    errorMessage: err.name+' '+ (!!err.payload ? err.payload.message:"error undefined")
                });
            });
        },
        // componentDidMount() {
        //     provideData(this.props)
        //     .then(data => {
        //         this.setState((prevState, props) => {
        //             return {readyState: 'loaded', data: data};
        //         });
        //     })
        //     .catch(err => {
        //         console.log(err);
        //         this.setState((prevState, props) => {
        //             return {readyState: 'error'};
        //         });
        //     });
        // },
        // componentDidMount() {
        //     provideData(this.props)
        //     .then(data => {
        //         Object.assign(this.state, {readyState: 'loaded', data: data});
        //     })
        //     .catch(err => {
        //         console.log(err);
        //         Object.assign(this.state, {readyState: 'error'});
        //     });
        // },
        renderLoader() {
            return (
                <div style={{textAlign: 'center', paddingTop: '20px'}}>
                    <Spin size="large" />
                </div>
            );
        },
        renderError() {
            return (
              <div style={{padding: 20}}>
                <Alert message={this.state.errorMessage} type="error" showIcon/>
              </div>
            );
        },
        renderSuccess() {
            return <Component {...this.props} data={this.state.data}/>;
        },
        render() {
            let {readyState} = this.state;

            switch (readyState) {
                case 'loading':
                    return this.renderLoader();
                case 'loaded':
                    return this.renderSuccess();
                case 'error':
                    return this.renderError();
            }
        }
    });
};
