import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'
import './index.less'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {VIEW_MODALITY_URL, openWindow, openWindowBlank} from "../StudyTable";
import Immutable from 'immutable'
const allSkip=new Immutable.Map();
const typeClass = {
    CT: 'view_ct',
    DR: 'view_dr',
    DX: 'view_dr',
    CR: 'view_dr'
}
const style = {
  'cursor': 'pointer',
  'WebkitUserSelect': 'none',
  'MozUserSelect': 'none',
  'msUserSelect': 'none',
  'userSelect': 'none',
}

const ReturnCT = withRouter(
  ({ history }) => (
      <span>
      <Link to="/">
      <a style={{float:"left",color:"#fff",height:"30px"}}>
        <img style={style} className='logo-image'style={{
            cursor:"pointer",
            userSelect:"none",
            width:"30px",
            verticalAlign:"middle",
            marginRight:"10px",
            marginLeft:"20px"}} src={require('../../../static/images/returnList.png')} /*onClick={() => history.push('/')}*//>
          <span><FormattedMessage id='ReturnList.button.name'/></span>
      </a>
      </Link>
       <a style={{float:"left",color:"#fff",height:"30px"}} onClick={e => {
           e.stopPropagation()
          window.location.href=`/${typeClass[JSON.parse(localStorage.getItem("fellowParams")).modality]}/${JSON.parse(localStorage.getItem("fellowParams")).studyUID.replace(/\./g, '-')}`}}>
                <img style={style} className='logo-image'style={{
                    cursor:"pointer",
                    userSelect:"none",
                    width:"30px",
                    verticalAlign:"middle",
                    marginRight:"10px",
                    marginLeft:"20px"}} src={require('../../../static/images/fellow.png')} /*onClick={() => history.push('/')}*//>
       </a>
      </span>

  )
)

export default ReturnCT
