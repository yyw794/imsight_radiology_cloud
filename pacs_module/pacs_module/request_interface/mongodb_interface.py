# -*- coding: UTF-8 -*-
'''
数据库说明
数据库名称 pacs_db
表1：system_label{install_date:20180803,list_back_date:20180302}
    记录一些系统数据
    install_date装机日期
    list_back_date列表回滚拉取的日期，我们会先拉取一年的列表
表2: study_instance_list{
    study_instance_uid:study_instance_uid
    assoc_ac:assoc_ac
    assoc_ac_state:assoc_ac_state
    study_date:study_date
    patientid:patientid
    patientname:patientname
    patientsex:patientsex
    patientbirthdate:patientbirthdate
    study_state:study_state
}
'''

from pymongo import MongoClient
import logging
import logger
log = logging.getLogger("PACSModule.mongodb_interface")

# client = MongoClient('localhost', 27017,connect=False)
client = MongoClient('mongodb', username='work', password='work@imsightmed',authSource='radiology')
db = client['pacs_db']

UnKonwType = 'unknowtype'
Unsupport = 'unsupport'
ReceiveComplate = 'receivecomplate'


Accepted = 'accepted'
Receiving = 'receive'
Abort = 'abort'
Released = 'released'


class Study_Instance_List(object):
    def __init__(self):
        self._id = ""
        self.assoc_ac = ""
        self.assoc_ac_state = ""
        self.study_date = ""
        self.patientid = ""
        self.patientname = ""
        self.patientsex = ""
        self.patientbirthdate = ""
        self.study_state = UnKonwType
        self.is_high_priority = False
        self.study_time = ""
        self.modality = ""
        
    def to_dict(self):
        return {
            "_id": self._id,
            "assoc_ac": self.assoc_ac,
            "assoc_ac_state": self.assoc_ac_state,
            "study_date": self.study_date,
            "patientid": self.patientid,
            "patientname": self.patientname,
            "patientsex": self.patientsex,
            "patientbirthdate": self.patientbirthdate,
            "study_state": self.study_state,
            "is_high_priority":self.is_high_priority,
            'study_time':self.study_time,
            'modality':self.modality
        }


def study_instance_list_insert_update(query_value, update_value):
    instance = db.study_instance_list.find_one(query_value)
    if instance is not None:
        db.study_instance_list.update_one(query_value, {'$set': update_value})
        return False
    else:
        db.study_instance_list.insert_one(update_value)
        return True


def get_study_instance_list(query_value):
    instance = db.study_instance_list.find_one(query_value)
    return instance


def get_all_study_instance_list(query_value):
    instance = db.study_instance_list.find(query_value)
    return instance


# system_label{_id:'system_label',install_date:20180803,list_back_date:20180302}
def insert_system_label(current_date):
    instance = db.system_label.find_one({"_id": 'system_label'})
    if instance is None:
        instance = {"_id": 'system_label', 'install_date': current_date, "list_back_date": current_date, "list_back_image_date": current_date}
        db.system_label.insert_one(instance)


def get_system_label():
    instance = db.system_label.find_one({'_id': 'system_label'})
    if instance is None:
        return None
    else:
        return instance


def update_current_list_back_date(current_date):
    instance = db.system_label.find_one({'_id': 'system_label'})
    if instance is not None:
        db.system_label.update({"_id": 'system_label'}, {'$set': {'list_back_date': current_date}})


def update_current_list_back_image_date(current_date):
    instance = db.system_label.find_one({'_id': 'system_label'})
    if instance is not None:
        db.system_label.update({"_id": 'system_label'}, {'$set': {'list_back_image_date': current_date}})







