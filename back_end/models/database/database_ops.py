#!/bin/python
import os
import sys
import threading

from pymongo import MongoClient, ASCENDING, DESCENDING

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from config import MONGO_NAME, MONGO_USERNAME, MONGO_PASSWORD, MONGO_AUTHSOURCE, MONGO_DATABASE_NAME

client = MongoClient(MONGO_NAME, username=MONGO_USERNAME, password=MONGO_PASSWORD, authSource=MONGO_AUTHSOURCE)
db = client[MONGO_DATABASE_NAME]
insert_mutex = threading.Lock()
update_mutex = threading.Lock()
delete_mutex = threading.Lock()


# insert a new document to target collection
def db_insert_one(collection_name, insert_value):
    if collection_name not in db.collection_names():
        return False

    collection = db[collection_name]

    insert_mutex.acquire()
    collection.insert_one(insert_value)
    insert_mutex.release()
    if '_id' in insert_value:
        insert_value.pop('_id')
    return True


# insert multiple documents to target collection
def db_insert_many(collection_name, insert_value):
    if collection_name not in db.collection_names():
        return False

    collection = db[collection_name]

    insert_mutex.acquire()
    collection.insert_many(insert_value)
    insert_mutex.release()
    for i in insert_value:
        if '_id' in i:
            i.pop('_id')
    return True


# find all documents fit criteria from target collection
def db_find(collection_name, query_value, sort_value=None, reverse=False):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    cursors = collection.find(query_value, {'_id': False})

    if sort_value:
        if not reverse:
            cursors = cursors.sort(sort_value, DESCENDING)
        else:
            cursors = cursors.sort(sort_value, ASCENDING)

    return list(cursors)


# find one document in target collection
def db_find_one(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]

    cursors = collection.find_one(query_value, {'_id': False})
    return cursors


# find all documents in target collection
def db_find_all(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    cursors = collection.find(query_value, {'_id': False})

    return list(cursors)


def db_page_query(collection_name, query_value, page_size, page_no):
    skip = page_size * (page_no - 1)
    collection = db[collection_name]
    cursor = collection.find(query_value, {'_id': False})
    count = cursor.count()
    page_record = cursor.sort("uploadDate", DESCENDING).limit(page_size).skip(skip)

    return page_record, count


def db_update_one(collection_name, query_value, update_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    update_mutex.acquire()
    cursors = collection.update_one(query_value, {'$set': update_value})
    update_mutex.release()
    if cursors is not None:
        return True
    return False


def db_update_all(collection_name, query_value, update_value):
    if collection_name not in db.collection_names():
        return None
    collection = db[collection_name]
    update_mutex.acquire()
    collection.update(query_value, {'$set': update_value}, upsert=False)
    update_mutex.release()


def db_delete_one(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    delete_mutex.acquire()
    collection.delete_one(query_value)
    delete_mutex.release()
    return True


def db_delete(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    delete_mutex.acquire()
    collection.delete_many(query_value)
    delete_mutex.release()
    return True


def db_find_update_otherwise_insert(collection_name, query_value, update_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    collection.update_one(query_value, {"$set": update_value}, upsert=True)
    # document = collection.find_one(query_value)
    # if document:
    #     collection.update_one(query_value, {'$set': update_value})
    # else:
    #     collection.insert_one(update_value)

    if '_id' in update_value:
        update_value.pop('_id')


def db_count(collection_name):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]

    return collection.count()


if __name__ == '__main__':
    print db_find('user', {'account': 'user1'})
