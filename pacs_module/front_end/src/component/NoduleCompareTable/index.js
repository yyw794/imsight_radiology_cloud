import React, { Component } from 'react';
import {getRiskText, getRiskColor, getNoduleType, RISK_EN2CN, TYPE_EN2CN} from '../NodulePanel'
import './index.less'
import {FormattedMessage, injectIntl} from 'react-intl'

function isLeftNoduleRiskBigger(nLeft,nRight)
{
  let left = -1
  let right =-1
  if(getRiskText(nLeft) === "High") left = 3
  if(getRiskText(nLeft) === "Middle") left = 2
  if(getRiskText(nLeft) === "Low") left = 1
  if(getRiskText(nLeft) === "Benign") left = 0
  
  if(getRiskText(nRight) === "High") right = 3
  if(getRiskText(nRight) === "Middle") right = 2
  if(getRiskText(nRight) === "Low") right = 1
  if(getRiskText(nRight) === "Benign") right = 0
  return left > right
}

class NoduleCompareTable extends Component {

  renderMalgTrend(data) {
    if (data[0].nodule_bm === data[1].nodule_bm) {
      return '•'
    } else if ( isLeftNoduleRiskBigger(data[0].nodule_bm,data[1].nodule_bm)) {
      return <img className='malg-arrow' src={require('../../../static/images/arrow_increase.png')}/>
    } else {
      return <img className='malg-arrow' src={require('../../../static/images/arrow_reduce.png')}/>
    }
  }

  render() {
    const data = this.props.data
    if (!data) {
      return ''
    }

    // const {nodule_diameter, nodule_malg, avgHU, coord, vol} = this.props.data
    return (
      <table className='nodule-compare-table'>
        <thead>
          <tr>
            <th></th>
            <th><FormattedMessage id="NoduleComparePanel.nodule-compare-table.followUp1"/></th>
            <th><FormattedMessage id="NoduleComparePanel.nodule-compare-table.followUp2"/></th>
            <th><FormattedMessage id="NoduleComparePanel.nodule-compare-table.variant"/></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.diameter"/></td>
            <td>{data[0].nodule_diameter.toFixed(1)}</td>
            <td>{data[1].nodule_diameter.toFixed(1)}</td>
            <td>{Math.round((data[1].nodule_diameter/data[0].nodule_diameter - 1) * 100)}%</td>
          </tr>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.malignancy"/></td>
            <td style={{color: getRiskColor(data[0].nodule_bm)}}>{RISK_EN2CN[getRiskText(data[0].nodule_bm)]}</td>
            <td style={{color: getRiskColor(data[1].nodule_bm)}}>{RISK_EN2CN[getRiskText(data[1].nodule_bm)]}</td>
            <td className='malg'>
              {this.renderMalgTrend(data)}
            </td>
          </tr>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.subclass"/></td>
            <td>{TYPE_EN2CN[getNoduleType(data[0])]}</td>
            <td>{TYPE_EN2CN[getNoduleType(data[1])]}</td>
            <td>-</td>
          </tr>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.location"/></td>
            <td>{`[${data[0].coord.x}, ${data[0].coord.y}, ${data[0].coord.z}]`}</td>
            <td>{`[${data[1].coord.x}, ${data[1].coord.y}, ${data[1].coord.z}]`}</td>
            <td>-</td>
          </tr>
          <tr>
            <td><FormattedMessage id="NoduleComparePanel.nodule-compare-table.volume"/></td>
            <td>{data[0].nodule_volume.toFixed(1)}</td>
            <td>{data[1].nodule_volume.toFixed(1)}</td>
            <td>{Math.round((data[1].nodule_volume/data[0].nodule_volume - 1) * 100)}%</td>
          </tr>
        </tbody>
      </table>
    );
  }

}

export default NoduleCompareTable;
