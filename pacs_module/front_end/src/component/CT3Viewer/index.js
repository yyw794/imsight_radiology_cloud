import React, { Component } from 'react'
import * as cornerstone from 'cornerstone-core'
import * as cornerstoneWebImageLoader from "cornerstone-web-image-loader"
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import Hammer from "hammerjs"
import {Icon, message, Button, Modal, Spin, Menu, Dropdown, Row, Col, Select} from 'antd'
import Immutable from 'immutable'

// import ToolBar from '../CTToolBar'
// import NodulePanel from '../NodulePanel'
// import {getRiskText, getNoduleType, RISK_EN2CN, TYPE_EN2CN} from '../NodulePanel'
import noduleBox from '../tools/noduleBox'
import renderReferLine from '../tools/renderReferLine'
import zoomSynchronizer from '../tools/zoomSynchronizer'
import Synchronizer from '../tools/Synchronizer'
import scrollToIndex from '../tools/scrollToIndex'
import scaleTool from '../tools/scaleTool'
import stackPreloader from '../tools/stackPreloader'
import {deepCopy, dataURItoBlob, clip} from '../../util'
import {getUser} from '../../auth'
import api from '../../api'
import axios from 'axios'
import Indicator from '../Indicator'
import {scaleWWDynamic, scaleWCDynamic, scaleWWReverseDynamic, scaleWCReverseDynamic} from '../../util'
import {FormattedMessage, injectIntl} from 'react-intl'
import {angle} from '../tools/angleTool'
import {freehand} from '../tools/freehand'
import {length} from '../tools/length'
import {zoom} from '../tools/zoom'

const {Option} = Select

import _ from 'underscore'
// import loadImage from './loadImage'

import './index.less'

cornerstoneTools.external.cornerstone = cornerstone
cornerstoneTools.cornerstoneMath = cornerstoneMath
cornerstoneWebImageLoader.external.cornerstone = cornerstone
cornerstoneTools.external.Hammer = Hammer

const TOOLS_NAME = ['length', 'angle', 'freehand']
class Viewer extends Component {

  constructor(props) {
    super(props)
    this.state = {
      imgPosX: 0,
      imgPosY: 0,
      imgPosZ: 0,
      // labels: props.labels,
      // activeTool: [],
      imageX: null,
      imageY: null,
      imageZ: null,
      viewportX: null,
      viewportY: null,
      viewportZ: null,
      HU: 0,
      fullScreen: props.onlyZ ? 'z' : null,
    }
  }

  componentWillReceiveProps(nextProps) {
      console.log(this.zoomSynchronizer)
    if (nextProps.onlyZ) {
      this.zoomSynchronizer.disable()
      // this.synchronizer.disable()
    } else {
      this.zoomSynchronizer.enable()
      // this.synchronizer.enable()
    }
    if (nextProps.study.studyUID !== this.props.study.studyUID || nextProps.series.seriesID !== this.props.series.seriesID) {
      // study have changed
      this.reset(nextProps.study, nextProps.series)
    }
    if (nextProps.onlyZ != this.props.onlyZ) {
      this.setState({
        fullScreen: nextProps.onlyZ ? 'z' : null
      }, this.onResize)
    }
    if (!Immutable.is(nextProps.labels, this.props.labels)) {
      // labels have changed
      this.forceUpdateImage()
    }
  }

  getImgPos() {
    return {
      x: this.state.imgPosX,
      y: this.state.imgPosY,
      z: this.state.imgPosZ
    }
  }

  forceUpdateImage() {
    cornerstone.updateImage(this.xElement)
    cornerstone.updateImage(this.yElement)
    cornerstone.updateImage(this.zElement)
  }

  getCurrentImageObj(axis) {
    return cornerstone.getImage(this[`${axis}Element`])
  }

  getScreenshot = (coordImage, size) => {
    return new Promise((resolve, reject) => {
      const mimetype = 'image/png'
      // const canvas = document.querySelector('#ct-3-viewer-container .crop-canvas')
      // const viewCanvas = document.querySelector('#ct-3-viewer-container .z-view .cornerstone-canvas')
      this.viewCanvas.getContext('2d').setTransform(1, 0, 0, 1, 0, 0);
      let coord = cornerstone.pixelToCanvas(this.zElement, coordImage)
      let imageData = this.viewCanvas.getContext('2d').getImageData(coord.x - size / 2, coord.y - size / 2, size, size)
      this.cropCanvas.getContext('2d').putImageData(imageData, 0, 0)
      const image = dataURItoBlob(this.cropCanvas.toDataURL(mimetype, 1))
      resolve(image)
      /*this.cropCanvas.toBlob(img => {
        resolve(img)
      }, mimetype, 1)*/
    });
  }

  initZoomSynchronizer = () => {
    //this.zoomSynchronizer = new cornerstoneTools.Synchronizer("cornerstoneimagerendered", zoomSynchronizer);
    this.zoomSynchronizer = new Synchronizer("cornerstoneimagerendered", zoomSynchronizer)
    this.zoomSynchronizer.add(this.xElement)
    this.zoomSynchronizer.add(this.yElement)
    this.zoomSynchronizer.add(this.zElement)
    this.props.onlyZ && this.zoomSynchronizer.disable()

  }

  getPixelInCurrentPosition() {
    let {height, width, columns, getPixelData} = cornerstone.getImage(this.zElement)
    const imgNumX = this.props.series.stacks.x.imageIds.length
    const imgNumY = this.props.series.stacks.y.imageIds.length
    const imgNumZ = this.props.series.stacks.z.imageIds.length
    let x = this.state.imgPosX / imgNumX * width
    let y = this.state.imgPosY / imgNumY * height
    let pixelData = getPixelData()
    let idx = (Math.round(x) + Math.round(y) * columns) * 4
    return pixelData[idx]
  }

  initLabelTool() {
    const imgNumX = this.props.series.stacks.x.imageIds.length
    const imgNumY = this.props.series.stacks.y.imageIds.length
    const imgNumZ = this.props.series.stacks.z.imageIds.length
    noduleBox.enable(this.zElement, {
      axis: 'z',
      depth: Math.round(noduleBox.BOX_SIZE / cornerstone.getImage(this.xElement).height * imgNumZ), // in layers
      horizontalLayers: imgNumX,
      verticalLayers: imgNumY,
      mainAxisLayers: imgNumZ,
      getData: () => this.props.labels.toJS(),
      getCurrentIdx: () => {
        return {x: this.state.imgPosX, y: this.state.imgPosY, z: this.state.imgPosZ}
      },
      hideBoxes:false
    })
    noduleBox.activate(this.zElement, 1)
    noduleBox.activate(this.zElement, 3)

    noduleBox.enable(this.xElement, {
      axis: 'x',
      depth: Math.round(noduleBox.BOX_SIZE / cornerstone.getImage(this.yElement).width * imgNumX), // in layers
      horizontalLayers: imgNumY,
      verticalLayers: imgNumZ,
      mainAxisLayers: imgNumX,
      getData: () => this.props.labels.toJS(),
      getCurrentIdx: () => {
        return {x: this.state.imgPosX, y: this.state.imgPosY, z: this.state.imgPosZ}
      },
      hideBoxes:false
    })
    noduleBox.activate(this.xElement, 1)
    noduleBox.activate(this.xElement, 3)

    noduleBox.enable(this.yElement, {
      axis: 'y',
      depth: Math.round(noduleBox.BOX_SIZE / cornerstone.getImage(this.xElement).width * imgNumY), // in layers
      horizontalLayers: imgNumX,
      verticalLayers: imgNumZ,
      mainAxisLayers: imgNumY,
      getData: () => this.props.labels.toJS(),
      getCurrentIdx: () => {
        return {x: this.state.imgPosX, y: this.state.imgPosY, z: this.state.imgPosZ}
      },
      hideBoxes:false
    })
    noduleBox.activate(this.yElement, 1)
    noduleBox.activate(this.yElement, 3)
  }

  async reset(study, series) {
    const {x: xStack, y: yStack, z: zStack} = series.stacks
    for (let axis of ['x', 'y', 'z']) {
      let element = this[axis + 'Element']
      let image = await cornerstone.loadImage(series.stacks[axis].imageIds[0])
      cornerstoneTools.clearToolState(element, 'stack')
      cornerstoneTools.addToolState(element, 'stack', series.stacks[axis])
      cornerstone.displayImage(element, image)
      cornerstoneTools.wwwc.deactivate(element, 1)
      cornerstoneTools.pan.activate(element, 1)
      // baseScale
      let baseScale = cornerstone.getViewport(element).scale
      cornerstoneTools.clearToolState(element, 'zoom')
      cornerstoneTools.addToolState(element, 'zoom', {baseScale})
      this['baseScale' + axis.toUpperCase()] = baseScale
      this.scrollTo(axis, 0.5)
    }
    this.setWWWC(1200, -600)
    this.enableReferLines()
    this.setState({
      HU: scaleWCDynamic(this.getPixelInCurrentPosition(),this.props.series.minPixelValue, this.props.series.maxPixelValue),
      fullScreen: null
    })
  }

  initEventListener() {
    this.zElement.addEventListener(cornerstoneTools.EVENTS.MOUSE_CLICK, (e) => {
      const eventData = e.detail
      const coord = eventData.currentPoints.image
      const {columns, rows} = eventData.image
      // rgba
      let idx = (Math.round(coord.x) + Math.round(coord.y) * eventData.image.columns) * 4
      let pixelData = eventData.image.getPixelData()
      // // WARNING: since it's a gray scale image stored in rgba, rgb value will be equal to gray scale
      let pixel = pixelData[idx]
      let HU = scaleWCDynamic(pixel,this.props.series.minPixelValue, this.props.series.maxPixelValue)
      this.setState({HU})
      Promise.all([
        this.scrollTo('x', coord.x / columns),
        this.scrollTo('y', coord.y / rows)
      ]).then(() => {
        this.props.onClick(e)
      })
    })
    this.xElement.addEventListener(cornerstoneTools.EVENTS.MOUSE_CLICK, (e) => {
      const eventData = e.detail
      const coord = eventData.currentPoints.image
      const {columns, rows} = eventData.image
      Promise.all([
        this.scrollTo('y', coord.x / columns),
        this.scrollTo('z', 1 - coord.y / rows)
      ]).then(()=>{
        this.props.onClick(e)
      })
    })
    this.yElement.addEventListener(cornerstoneTools.EVENTS.MOUSE_CLICK, (e) => {
      const eventData = e.detail
      const coord = eventData.currentPoints.image
      const {columns, rows} = eventData.image
      Promise.all([
        this.scrollTo('x', coord.x / columns),
        this.scrollTo('z', 1 - coord.y / rows)
      ]).then(()=>{
        this.props.onClick(e)
      })
    })
    document.addEventListener('keydown', this.onKeyDown)
    window.addEventListener("resize", _.debounce(this.onResize, 500))
  }

  componentDidMount() {
    const {x: xStack, y: yStack, z: zStack} = this.props.series.stacks
    this.synchronizer = new Synchronizer("cornerstonenewimage", cornerstoneTools.updateImageSynchronizer)

    let initXStack = this.initStack(this.xElement, xStack, this.synchronizer, 'X')
    let initYStack = this.initStack(this.yElement, yStack, this.synchronizer, 'Y')
    let initZStack = this.initStack(this.zElement, zStack, this.synchronizer, 'Z')
   console.log(initZStack)
      console.log(initXStack)
      console.log(initYStack)
    Promise.all([initZStack, initXStack, initYStack])
    .then(() => {
      return Promise.all([
        this.scrollTo('x', 0.5),
        this.scrollTo('y', 0.5),
        this.scrollTo('z', 0.5)
      ])
    })
    .then(() => {
      // preload images
      // stackPreloader.enable(this.xElement)
      // stackPreloader.enable(this.yElement)
      // stackPreloader.enable(this.zElement)

      scaleTool.enable(this.zElement)

      // default lung
      this.setWWWC(1200, -600)

      this.initZoomSynchronizer()
      this.initLabelTool()
      this.enableReferLines()
      this.setState({HU: scaleWCDynamic(this.getPixelInCurrentPosition(),this.props.series.minPixelValue, this.props.series.maxPixelValue)})

      // document.addEventListener('keydown', this.onKeyDown)
      // window.addEventListener("resize", _.debounce(this.onResize, 500))
      this.initEventListener()
      // this.initSocket()
    })
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.onResize)
    document.removeEventListener('keydown', this.onKeyDown)
    // stackPreloader.disable(this.xElement)
    // stackPreloader.disable(this.yElement)
    // stackPreloader.disable(this.zElement)
  }

  onKeyDown = (e) => {
    const KEYS_WWWC = {
      'F1': {wc: -600, ww: 1200},
      'F2': {wc: 40, ww: 400},
      'F3': {wc: 40, ww: 300},
      'F4': {wc: 800, ww: 2600},
      'F5': {wc: 45, ww: 300},
      'F6': {wc: 35, ww: 80}
    }
    if (e.key in KEYS_WWWC) {
      this.setWWWC(KEYS_WWWC[e.key].ww,KEYS_WWWC[e.key].wc)
    }
    switch (e.key) {
      case 'ArrowRight':
        this.scrollTo('x', null, this.state.imgPosX + 1)
          .then(() => this.setState({HU: scaleWCDynamic(this.getPixelInCurrentPosition(),this.props.series.minPixelValue, this.props.series.maxPixelValue)}))
        break;
      case 'ArrowLeft':
        this.scrollTo('x', null, this.state.imgPosX - 1)
          .then(() => this.setState({HU: scaleWCDynamic(this.getPixelInCurrentPosition(),this.props.series.minPixelValue, this.props.series.maxPixelValue)}))
        break;
      case 'ArrowUp':
        this.scrollTo('y', null, this.state.imgPosY - 1)
          .then(() => this.setState({HU: scaleWCDynamic(this.getPixelInCurrentPosition(),this.props.series.minPixelValue, this.props.series.maxPixelValue)}))
        break;
      case 'ArrowDown':
        this.scrollTo('y', null, this.state.imgPosY + 1)
          .then(() => this.setState({HU: scaleWCDynamic(this.getPixelInCurrentPosition(),this.props.series.minPixelValue, this.props.series.maxPixelValue)}))
        break;
      default:
    }
  }

  onResize = () => {
    this.synchronizer.disable()
    this.zoomSynchronizer.disable()
    if (!this.state.fullScreen) {
      for (let axis of ['x', 'y', 'z']) {
        let element = this[axis + 'Element']
        cornerstone.resize(element, true)
        let baseScale = cornerstone.getViewport(element).scale
        cornerstoneTools.clearToolState(element, 'zoom')
        cornerstoneTools.addToolState(element, 'zoom', {baseScale})
        this['baseScale' + axis.toUpperCase()] = baseScale
      }
    } else {
      let axis = this.state.fullScreen
      let element = this[axis + 'Element']
      cornerstone.resize(element, true)
      let baseScale = cornerstone.getViewport(element).scale
      cornerstoneTools.clearToolState(element, 'zoom')
      cornerstoneTools.addToolState(element, 'zoom', {baseScale})
      this['baseScale' + axis.toUpperCase()] = baseScale
    }
    this.synchronizer.enable()
    this.zoomSynchronizer.enable()
  }

  /**
   * [scrollTo description]
   * @param  {[type]}  axis          [x, y, z]
   * @param  {[type]}  ratio         [ratio to calculate index]
   * @param  {Boolean} [exact=false] [exact index]
   * @return {[Promise]} resolve when image is loaded
   */
  scrollTo(axis, ratio, exact=false) {
    let idx
    let imgNum = this.props.series.stacks[axis].imageIds.length
    if (exact !== false) {
      idx = clip(exact, 0, imgNum - 1)
    } else {
      idx = clip(Math.ceil(imgNum * ratio) - 1, 0, imgNum - 1)
    }
    return scrollToIndex(this[axis + 'Element'], idx)
  }

  initStack(element, stackData, synchronizer, direction) {
    cornerstone.enable(element)
    // Enable mouse inputs
    cornerstoneTools.mouseInput.enable(element)
    cornerstoneTools.mouseWheelInput.enable(element)

    return cornerstone.loadImage(stackData.imageIds[0])
      .then((image) => {
        cornerstone.displayImage(element, image)
        // set the stack as tool state
        cornerstoneTools.addStackStateManager(element, ['stack', 'referenceLines'])
        cornerstoneTools.addToolState(element, 'stack', stackData)
        // Enable all tools we want to use with this element
        cornerstoneTools.pan.activate(element, 1)
        zoom.activate(element, 4)
        cornerstoneTools.stackScrollWheel.activate(element)

        element.addEventListener("cornerstonenewimage", (e) => {
          const stackData = cornerstoneTools.getToolState(element, 'stack')
          this.setState({
            ['imgPos' + direction.toUpperCase()]: stackData.data[0].currentImageIdIndex,
            ['image' + direction.toUpperCase()]: e.detail.image
          })
          cornerstoneTools.addToolState(element, 'zoom', {baseScale: this['baseScale' + direction.toUpperCase()]})
        })

        this.setState({['image' + direction.toUpperCase()]: image})

        // baseScale
        let baseScale = cornerstone.getViewport(element).scale
        cornerstoneTools.addToolState(element, 'zoom', {baseScale})
        this['baseScale' + direction.toUpperCase()] = baseScale
        element.addEventListener('cornerstoneimagerendered', () => {
          this.setState({
            ['viewport' + direction.toUpperCase()]: cornerstone.getViewport(element)
          })
        })

        synchronizer.add(element)
        // this.props.onlyZ && synchronizer.disable()
      })
  }

  activateTool(name) {
    switch (name) {
      case 'wwwc':
        cornerstoneTools.wwwc.activate(this.xElement, 1)
        cornerstoneTools.wwwc.activate(this.yElement, 1)
        cornerstoneTools.wwwc.activate(this.zElement, 1)
        cornerstoneTools.pan.deactivate(this.xElement, 1)
        cornerstoneTools.pan.deactivate(this.yElement, 1)
        cornerstoneTools.pan.deactivate(this.zElement, 1)
          this.disableLine()
          this.disableAngle()
          this.disableFreehand()
        break;
      case 'referenceLines':
          this.disableLine()
          this.disableAngle()
          this.disableFreehand()
          this.enableReferLines()
        break;
      case 'hideBoxes':
          this.disableLine()
          this.disableAngle()
          this.disableFreehand()
        this.hideBoxes()
        break;
      case 'length':
          this.disableAngle()
          this.disableFreehand()
        this.enableLine()
        break;
      case 'angle':
          this.disableLine()
          this.disableFreehand()
        this.enableAngle()
        break;
      case 'freehand':
         this.disableLine()
          this.disableAngle()
        this.enableFreehand()
        break;
      default:
        break
    }
  }
  clearAllTool(){
    this.clearLine()
    this.clearAngle()
    this.clearFreehand()
  }
  deactivateTool(name) {
    switch (name) {
      case 'wwwc':
        cornerstoneTools.wwwc.deactivate(this.xElement, 1)
        cornerstoneTools.wwwc.deactivate(this.yElement, 1)
        cornerstoneTools.wwwc.deactivate(this.zElement, 1)
        cornerstoneTools.pan.activate(this.xElement, 1)
        cornerstoneTools.pan.activate(this.yElement, 1)
        cornerstoneTools.pan.activate(this.zElement, 1)
        break;
      case 'referenceLines':
        this.disableReferLines()
        break;
      case 'hideBoxes':
        this.showBoxes()
        break;
      case 'length':
         this.disableLine();
         break;
       case 'angle':
         this.disableAngle()
          break;
      case 'freehand':
          this.disableFreehand()
         break;
      default:
        break
    }
  }

  setWWWC(windowWidth, windowCenter) {
    let ww = scaleWWReverseDynamic(windowWidth,this.props.series.minPixelValue,this.props.series.maxPixelValue)
    let wc = scaleWCReverseDynamic(windowCenter,this.props.series.minPixelValue,this.props.series.maxPixelValue)
    for (let axis of ['x', 'y', 'z']) {
      let elem = this[axis + 'Element']
      let viewport = cornerstone.getViewport(elem)
      viewport.voi.windowWidth = ww
      viewport.voi.windowCenter = wc
      cornerstone.updateImage(elem)
    }
  }

  showBoxes(){
    noduleBox.updateOptions(this.zElement,{hideBoxes:false})
    noduleBox.updateOptions(this.xElement,{hideBoxes:false})
    noduleBox.updateOptions(this.yElement,{hideBoxes:false})
    this.forceUpdateImage()
  }

  hideBoxes(){
    noduleBox.updateOptions(this.zElement,{hideBoxes:true})
    noduleBox.updateOptions(this.xElement,{hideBoxes:true})
    noduleBox.updateOptions(this.yElement,{hideBoxes:true})
    this.forceUpdateImage()
  }

  enableReferLines() {
    cornerstoneTools.referenceLines.tool.enable(this.xElement, this.synchronizer, renderReferLine)
    cornerstoneTools.referenceLines.tool.enable(this.yElement, this.synchronizer, renderReferLine)
    cornerstoneTools.referenceLines.tool.enable(this.zElement, this.synchronizer, renderReferLine)
    // this.setState({
    //   // activeTool: [...this.state.activeTool, 'referenceLines'],
    //   activeTool: _.without([...this.state.activeTool], 'referenceLines').concat(['referenceLines'])
    // })
  }
  enableLine(){
      length.activate(this.xElement, 1)
      length.activate(this.yElement, 1)
      length.activate(this.zElement, 1)
  }
  clearLine(){
      cornerstoneTools.clearToolState(this.xElement,  "length")
      cornerstoneTools.clearToolState(this.yElement,  "length")
      cornerstoneTools.clearToolState(this.zElement,  "length")
     length.enable(this.xElement)
     length.enable(this.yElement)
     length.enable(this.zElement)
  }
  enableAngle(){
      angle.activate(this.xElement, 1)
      angle.activate(this.yElement, 1)
      angle.activate(this.zElement, 1)

  }
  clearAngle(){
      cornerstoneTools.clearToolState(this.xElement,  "angle")
      cornerstoneTools.clearToolState(this.yElement,  "angle")
      cornerstoneTools.clearToolState(this.zElement,  "angle")
      angle.enable(this.xElement,1)
      angle.enable(this.yElement,1)
      angle.enable(this.zElement,1)
  }
  enableFreehand(){
   freehand.activate(this.xElement, 1)
   freehand.activate(this.yElement, 1)
   freehand.activate(this.zElement, 1)
    }
  clearFreehand(){
      cornerstoneTools.clearToolState(this.xElement,  "freehand")
      cornerstoneTools.clearToolState(this.yElement,  "freehand")
      cornerstoneTools.clearToolState(this.zElement,  "freehand")
        freehand.enable(this.xElement, 1)
        freehand.enable(this.yElement, 1)
        freehand.enable(this.zElement, 1)
    }
  disableFreehand(){
   freehand.deactivate(this.xElement, 1)
   freehand.deactivate(this.yElement, 1)
   freehand.deactivate(this.zElement, 1)
  }
  disableAngle(){
  angle.deactivate(this.xElement, 1)
  angle.deactivate(this.yElement, 1)
  angle.deactivate(this.zElement, 1)
  }
  disableLine(){
    length.deactivate(this.xElement, 1)
    length.deactivate(this.yElement, 1)
    length.deactivate(this.zElement, 1)
  }
  disableReferLines() {
    cornerstoneTools.referenceLines.tool.disable(this.xElement, this.synchronizer, renderReferLine)
    cornerstoneTools.referenceLines.tool.disable(this.yElement, this.synchronizer, renderReferLine)
    cornerstoneTools.referenceLines.tool.disable(this.zElement, this.synchronizer, renderReferLine)
    // this.setState({
    //   activeTool: _.without([...this.state.activeTool], 'referenceLines')
    // })
  }

  toggleFullScreen(axis) {
    if (this.state.fullScreen === axis) {
      this.setState({
        fullScreen: null
      }, () => window.requestAnimationFrame(this.onResize))
    } else {
      this.setState({
        fullScreen: axis
      }, () => window.requestAnimationFrame(this.onResize))
    }
  }

  render() {
    const nodulePopoverBtn = this.state.nodulePopoverBtn
    return (
      <div style={this.props.style} className='ct-viewer-container'>
        <canvas className='crop-canvas' width='300' height='300' ref={input => this.cropCanvas = input}/>
        <div className={this.state.fullScreen ? `views full-screen-${this.state.fullScreen}` : 'views'}>
          <div className='view z-view'>
            <div ref={input => {this.zElement = input}} className='cornerstone-elm'>
              <canvas className="cornerstone-canvas"
                ref={input => this.viewCanvas = input}
                onContextMenu={e => e.preventDefault()}/>
              <div className='info-box'>
                <p>HU: {Math.round(this.state.HU)}</p>
                <p>WW: {
                    this.state.viewportZ ?
                    Math.round(scaleWWDynamic(this.state.viewportZ.voi.windowWidth,this.props.series.minPixelValue, this.props.series.maxPixelValue)) : 0}</p>
                <p>WC: {this.state.viewportZ ?
                    Math.round(scaleWCDynamic(this.state.viewportZ.voi.windowCenter,this.props.series.minPixelValue, this.props.series.maxPixelValue)) : 0}</p>
              </div>
            </div>
            <Indicator
              image={this.state.imageZ}
              viewport={this.state.viewportZ}
              onMouseDrag={translation => {
                let viewport = Object.assign({}, this.state.viewportZ)
                viewport.translation = translation
                cornerstone.setViewport(this.zElement, viewport)
              }}
              baseScale={this.baseScaleZ}/>

            <Button
              ghost
              hidden={this.props.onlyZ}
              icon={this.state.fullScreen === 'z' ? 'shrink' : 'arrows-alt'}
              className='toggle-full-screen'
              onClick={() => this.toggleFullScreen('z')}>
            </Button>
            <div className='view-footer'>
              <Button ghost onClick={() => {
                this.zoomSynchronizer.disable()
                let viewport = cornerstone.getViewport(this.zElement)
                viewport.scale = this.baseScaleZ
                viewport.translation = {x: 0, y: 0}
                cornerstone.setViewport(this.zElement, viewport)
                // cornerstone.reset(this.zElement)
                this.zoomSynchronizer.enable()

              }}><FormattedMessage id="CTViewer.autoFit"/></Button>
              <span className='img-pos'>{this.props.study.reverse?(this.props.series.stacks.z.imageIds.length - this.state.imgPosZ):(this.state.imgPosZ+1)} / {this.props.series.stacks.z.imageIds.length}</span>
            </div>
          </div>

          <div className='view x-view'>
            <div ref={input => {this.xElement = input}} className='cornerstone-elm'>
              <canvas className="cornerstone-canvas"
                onContextMenu={e => e.preventDefault()}/>
            </div>
            <Indicator
              image={this.state.imageX}
              viewport={this.state.viewportX}
              onMouseDrag={translation => {
                let viewport = Object.assign({}, this.state.viewportX)
                viewport.translation = translation
                cornerstone.setViewport(this.xElement, viewport)
              }}
              baseScale={this.baseScaleX}/>
            <Button
              ghost
              hidden={this.props.onlyZ}
              icon={this.state.fullScreen === 'x' ? 'shrink' : 'arrows-alt'}
              className='toggle-full-screen'
              onClick={() => this.toggleFullScreen('x')}>
            </Button>
            <div className='view-footer'>
              <span className='img-pos'>{this.state.imgPosX + 1} / {this.props.series.stacks.x.imageIds.length}</span>
            </div>
          </div>

          <div className='view y-view'>
            <div ref={input => {this.yElement = input}} className='cornerstone-elm'>
              <canvas className="cornerstone-canvas"
                onContextMenu={e => e.preventDefault()}/>
            </div>
            <Indicator
              image={this.state.imageY}
              viewport={this.state.viewportY}
              onMouseDrag={translation => {
                let viewport = Object.assign({}, this.state.viewportY)
                viewport.translation = translation
                cornerstone.setViewport(this.yElement, viewport)
              }}
              baseScale={this.baseScaleY}/>
            <Button
              ghost
              hidden={this.props.onlyZ}
              icon={this.state.fullScreen === 'y' ? 'shrink' : 'arrows-alt'}
              className='toggle-full-screen'
              onClick={() => this.toggleFullScreen('y')}>
            </Button>
            <div className='view-footer'>
              <span className='img-pos'>{this.state.imgPosY + 1} / {this.props.series.stacks.y.imageIds.length}</span>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

export default Viewer
