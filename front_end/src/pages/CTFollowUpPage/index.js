import React, { Component } from 'react'
import CTFollowUp from '../../component/CTFollowUp'
import api from '../../api'
import LoadingWrapper from '../../component/LoadingWrapper'
import axios from 'axios'
import { Card, Layout,message } from 'antd'
import {addCTMetaData} from '../../metaData.js'
import {getUser} from '../../auth'
import _ from 'underscore'
import {EMPTYNODULEREGISTER,EMPTYSTUDYCT,EMPTYNODULELABELS} from '../../component/DefaultData'
import {DICOM_URL_PREFIX,URL_PREFIX} from "../../component/tools/config";

import './index.less'

const { Header, Content, Footer } = Layout;

class CTViewerPage extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    // let {age, gender, patientName, studyDate, studyTime} = this.props.data.study
    let user = getUser()
    let userID = ''
    if (user) {
      userID = user.account
    }
    return (
      <div style={{height: '100%', backgroundColor: '#000'}} id='ct-viwer-page'>
        <CTFollowUp
          stack1={this.props.data.fixedStudy.stacks}
          stack2={this.props.data.movingStudy.stacks}
          labels1={this.props.data.fixedStudy.labels}
          labels2={this.props.data.movingStudy.labels}
          study1={this.props.data.fixedStudy}
          study2={this.props.data.movingStudy}
          studylist={this.props.data.studies}
          compareNodules={this.props.data.compareNodules}
          studyUIDs={this.props.match.params.ids.replace(/-/g, '.').split(',')}
          userID={userID}
          style={{height: '100%', position: 'relative'}}/>
      </div>
    )
  }

}

function keysort(key,sortType) {
    return function(a,b){
        return sortType ?(a[key] < b[key]) :(a[key] > b[key]);
    }
}
async function getData(props) {
  let studylistErr= false
  let studyErr= false
  let labelsErr= false
  let noduleRegisterErr = false 

  let patientID = props.match.params.patientID
  let studyUIDs = props.match.params.ids.replace(/-/g, '.').split(',')
  let studiesRef = await api.getStudylist({patientID, needData: 'True'})
    .catch((err)=>{ 
    message.error("API error, getStudylist",60)
    studylistErr = true
  })

  let studies=[]
  if(studylistErr)
  {
    let studyRef0 = EMPTYSTUDYCT
    let studyRef1 = EMPTYSTUDYCT
    studyRef0.studyUID = studyUIDs[0]
    studyRef1.studyUID = studyUIDs[1]
    studiesRef = [studyRef0,studyRef1]
  }

  for(let i in studyUIDs)
  {
    const studyUID = studyUIDs[i]

    let studyDetail = await api.getStudyDetail({studyUID})
    .catch((err)=>{
      message.error("API error, getStudyDetail",60)
      studyErr = true
    })
    Object.values(studyDetail.series).forEach(series=>{
      series.instances.x.map(ins=> ins.url = URL_PREFIX + ins.url)
      series.instances.y.map(ins=> ins.url = URL_PREFIX + ins.url)
      series.instances.z.map(ins=> ins.url = DICOM_URL_PREFIX + ins.url)
    })
    if(studyErr)
    {
      studyDetail = studyRef
    }
    studies.push(studyDetail)

  }

  let reports = []
  for (let study of studies) {
    const series = Object.values(study.series)[0]
    study.stacks = {
      x: {
        currentImageIdIndex: 0,
        imageIds: !!series.instances.x ? series.instances.x.map(ins => ins.url):Object.values(EMPTYSTUDYCT.series)[0].instances.x.map(ins => ins.url)
      },
      z: {
        currentImageIdIndex: 0,
        imageIds: !!series.instances.z ? series.instances.z.map(ins => ins.url):Object.values(EMPTYSTUDYCT.series)[0].instances.z.map(ins => ins.url)
      },
      y: {
        currentImageIdIndex: 0,
        imageIds: !!series.instances.y ? series.instances.y.map(ins => ins.url):Object.values(EMPTYSTUDYCT.series)[0].instances.y.map(ins => ins.url)
      },
    }
    let labels = await api.getLabels({taskID: study.taskID, labelType: study.modality=="CT"?"Nodule":"DX"})
    .catch((err)=>{ 
      message.error("API error, getLabels",60)
      labelsErr = true
    })
    if(labelsErr)
    {
      labels = EMPTYNODULELABELS
    }
    labels.sort((a, b) => b.malg - a.malg)
    study.labels = labels.map((label, key) => Object.assign(label, {key: key + 1}))
    addCTMetaData(study)
  }

  studies.sort((a, b) => a.studyDate + a.studyTime > b.studyDate + b.studyTime)
  let fixedStudy = studies.find(s => s.studyUID === studyUIDs[0])
  let movingStudy = studies.find(s => s.studyUID === studyUIDs[1])
  let noduleRegister = await api.getNoduleRegister({taskID_fixed: fixedStudy.taskID, taskID_moving: movingStudy.taskID})
  .catch((err)=>{ 
    message.error("API error, getNoduleRegister",60)
    noduleRegisterErr = true
  })
  if(noduleRegisterErr)
  {
    noduleRegister = EMPTYNODULEREGISTER
  }
  // let noduleRegister = registerResp.registerNodules
  //console.log(noduleRegister,studies);
  let compareNodules = _.zip(noduleRegister.fixed, noduleRegister.moving)
  compareNodules.forEach(pair => {
    if(fixedStudy.labels.find(label => label.labelID === pair[0].labelID)){
        pair[0].key = fixedStudy.labels.find(label => label.labelID === pair[0].labelID).key
    }
    if(movingStudy.labels.find(label => label.labelID === pair[1].labelID)){
        pair[1].H=false;
        pair[1].key = movingStudy.labels.find(label => label.labelID === pair[1].labelID).key
    }else{
        pair[1].H=true;
        pair[1].key = fixedStudy.labels.find(label => label.labelID === pair[0].labelID).key
    }
  })
  return await {studies, compareNodules, fixedStudy, movingStudy}
}

CTViewerPage = LoadingWrapper(CTViewerPage, getData)

export default CTViewerPage
