import React from 'react'
import { BrowserRouter, Route, IndexRoute, Redirect, Switch } from 'react-router-dom'
import { Layout, LocaleProvider,message } from 'antd';
const { Header, Content, Footer } = Layout;
import StudyListPage from './pages/StudyListPage'
import DRViewerPage from './pages/DRViewerPage'
import CTViewerPage from './pages/CTViewerPage'
import CTFollowUpPage from './pages/CTFollowUpPage'
import LiverViewerPage from './pages/LiverViewerPage'
import ReportPage from './pages/ReportPage'
import ReportPageLiver from './pages/ReportPageLiver'
import ReportPrintPage from  './pages/ReportPrintPage'
import LoginPage from './pages/LoginPage'
import RegisterPage from './pages/RegisterPage'
import ResetPage from './pages/ResetPage'
import UploadPage from './pages/UploadPage'
import NotFoundPage from './pages/NotFoundPage'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import {isAuthenticated} from './auth'
import { instanceOf } from 'prop-types';
import { connect } from 'react-redux'
import { Provider } from 'react-redux'
import zhCN_antd from 'antd/lib/locale-provider/zh_CN';
import enUS_antd from 'antd/lib/locale-provider/en_US';
// import moment from 'moment';
// import 'moment/locale/zh-cn';
// import 'moment/locale/en-gb';

// moment.locale('zh-cn');
message.config({
    top: 100
});
import {addLocaleData, IntlProvider, FormattedMessage} from 'react-intl'
import zh from 'react-intl/locale-data/zh';
import en from 'react-intl/locale-data/en';
// import zhCN from '../locales/zh-cn.json'
import zhCN from '../locales/zh-cn.json'
import enUS from '../locales/en.json'
addLocaleData([...zh, ...en])

import './App.less'

const PrivateRoute = ({component: Component, ...rest}) => (

  <Route
    {...rest}
    render={props => {

      return isAuthenticated() ? (
        <Component {...props} setAppState={(state) => rest.setAppState(state)}/>
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }}
  />
)

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLocale: props.lang
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({currentLocale: nextProps.lang})
  }

  render() {
    const antd_locale = this.state.currentLocale === 'zh-cn' ? zhCN_antd : enUS_antd
    const locale = this.state.currentLocale === 'zh-cn' ? zhCN : enUS
    return (
      <IntlProvider messages={locale} locale={this.state.currentLocale}>
        <LocaleProvider locale={antd_locale}>
        <Layout className="layout" style={{ height: '100%' }}>
          <Switch>
            <PrivateRoute exact path="/" component={StudyListPage}/>
            <Route path='/login' component={LoginPage}/>
            <Route path='/register' component={RegisterPage}/>
            <Route path='/reset' component={ResetPage}/>
            <PrivateRoute path='/upload' component={UploadPage}/>
            <PrivateRoute path='/study_list' component={StudyListPage}/>
            <PrivateRoute path='/report/:id' component={ReportPage}/>
            <PrivateRoute path='/report_print/:id' component={ReportPrintPage}/>
            <PrivateRoute path='/view_dr/:id/:labeler?' component={DRViewerPage}/>
            <PrivateRoute path='/view_ct/:id/:labeler?' component={CTViewerPage}/>
            <PrivateRoute path='/view_ct_follow_up/:patientID/:ids' component={CTFollowUpPage}/>
            <PrivateRoute path='/view_liv/:id' component={LiverViewerPage}/>
            <Route component={NotFoundPage}/>
          </Switch>
        </Layout>
        </LocaleProvider>
      </IntlProvider>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return state
}
App = connect(mapStateToProps)(App)

// DragDropContext is required for drag&drop
export default DragDropContext(HTML5Backend)(App)
// export default App;
