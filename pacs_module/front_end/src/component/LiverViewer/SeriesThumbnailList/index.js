import React, { Component } from 'react';
import _ from 'underscore'
import {Icon} from 'antd'
import './index.less'
import {VIEW_POSITION} from '../../../data'
import SeriesThumbnailItem from './SeriesThumbnailItem'

class SeriesThumbnailList extends Component {
  constructor(props){
    super(props);
    this.state = {
      currentSeriesIdx: this.props.defaultIdx,
      collapsed: false
    }
  }
  onItemClick = (iter, id, seriesNumber) => {
      if(_.isFunction(this.props.onChange)){
        //this.props.onChange(seriesNumber, this.props.series.find(function(series) {return series.seriesNumber == String(seriesNumber);}).instances.z["1"])
        this.props.onChange(seriesNumber, null, function(){
          this.setState({}, function(){
            //this.props.viewer.initNoduleBoxTool_SeriesCell(this.props.viewer.currentActiveGridCell);
          }.bind(this));
        }.bind(this));
      }
      this.setState({iter});
  }
  componentDidMount() {
    this.props.setThisComponentToParent(this)
  }
  componentDidMount() {
    this.props.setThisComponentToParent(this)
  }
  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }
  render() {
    //console.log("SeriesThumbnailList is rendered!!!!!!!");
    return (
      <div className={this.state.collapsed ? 'series-thumbnail-list-container collapsed' : 'series-thumbnail-list-container'}>
        <ul className='series-thumbnail-list'>
          {
            this.props.series.map((series, i) => {
              return (
                <SeriesThumbnailItem
                viewer={this.props.viewer}
                data={this.props}
                key={i+1}
                iter={i}
                image={series.instances.z[1]}
                totalSlices={Object.keys(series.instances.z).length}
                currentSeriesIdx={this.state.currentSeriesIdx}
                onItemClick={this.onItemClick}
                setCurrentSeriesNumber={seriesNumber => this.props.setCurrentSeriesNumber(seriesNumber)}
                series={series}
                seriesNumber={series.seriesNumber}
                />
              )
            })
          }
        </ul>
        <div className='toggle-collapsed horizontal' onClick={this.toggleCollapsed}>
          {
            this.state.collapsed ?
            <Icon type="left" />
            :
            <Icon type="right" />
          }
        </div>
        <div className='toggle-collapsed vertical' onClick={this.toggleCollapsed}>
          {
            this.state.collapsed ?
            <Icon type="up" />
            :
            <Icon type="down" />
          }
        </div>
      </div>
    );
  }

}

export default SeriesThumbnailList;
