import React, { Component } from 'react'
import {withRouter} from 'react-router'
import './index.less'

/**
 *
 * @type {{
 * cursor: string,
 * WebkitUserSelect: string,
 * MozUserSelect: string,
 * msUserSelect: string,
 * userSelect: string}}
 */
const style = {
  'cursor': 'pointer',
  'WebkitUserSelect': 'none',
  'MozUserSelect': 'none',
  'msUserSelect': 'none',
  'userSelect': 'none',
}

/**
 * logo组件
 */
const Logo = withRouter(
  ({ history }) => (
    <div className="logo" style={{height:"3rem"}}>
      <img style={{height:"2rem"}} className='logo-image' src={require('../../../static/images/logo.png')} /*onClick={() => history.push('/')}*//>
{/*<span className="company-name" style={{fontSize:"1rem"}}>Imsight Radiology Platform<sup>TM</sup></span>*/}
    </div>
  )
)

export default Logo
