#!/bin/python
#coding=utf-8
from flask import make_response, request, jsonify, abort
from flask_login import current_user
import sys, os, datetime

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_delete_one, db_insert_one, db_update_one
from models.response.response import response
from models.task.Task import Task, parseTaskInfo
from models.study.Study import Study, parseStudyInfo
from config import PACS_PLUGIN_TOEKN

required_key_get = ['studyUID']
required_key_put = ['birthDate', 'studyDate', 'bodyPartExamined', 'gender',\
                     'hospital', 'modality', 'patientID', 'patientName', 'studyUID']
optional_key = ['token']

def on_study():
    if not current_user.is_authenticated:
        return abort(401)

    res = response()
    userID = current_user.get_id()
    params = request.args.to_dict() if request.method == 'GET' else request.json

    if request.method == 'GET':
        if not all(key in params for key in required_key_get):
            res.statusCode = 1100
            return make_response(jsonify(res.to_dict()), 200)

        studyUID = params['studyUID']
        # check whether user possess data as reviewer 
        task_info = db_find_one('task', {"$or": [{'studyUID': studyUID, "reviewer": userID}, {'studyUID': studyUID, "reporter": userID}]})
        if task_info is None:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        # get taskID from ownership collection by ownerID
        ownerID = task_info['reporter']
        ownership_info = db_find_one('ownership', {"userID": ownerID})
        shared = ownership_info['shared']
        if studyUID.replace('.', '-') in shared:
            taskID = shared[studyUID.replace('.', '-')]
            if taskID != task_info['taskID']:
                db_delete_one('task', {'taskID': task_info['taskID']})
                db_delete_one('label', {'taskID': task_info['taskID']})
                db_delete_one('report', {'taskID': task_info['taskID']})
                db_delete_one('tool', {'taskID': task_info['taskID']})
                task_info = db_find_one('task', {'taskID': taskID})
        else:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        task = parseTaskInfo(task_info)

        # get study data
        study_info = db_find_one("study", {"studyUID": studyUID})
        if study_info is None:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)
        new_study = parseStudyInfo(study_info)

        # get label once to identify malignant
        label_info = db_find_one("label", {"taskID": task_info["taskID"]})
        result = "NORMAL"
        if label_info is not None:
            result = "ABNORMAL"

        study_info = new_study.to_dict()
        study_info['taskID'] = task.id
        study_info['result'] = result
        study_info['user_privilege'] = task.get_user_privilege(userID)

        res.data = study_info
        res.statusCode = 1000
        return make_response(jsonify(res.to_dict()), 200)

    if request.method == 'POST':
        if not all(key in params for key in required_key_put):
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        new_study = Study(params['studyUID'])
        try:
            new_study.studyDate = datetime.datetime.strptime(params['studyDate'], "%Y%m%d")
        except:
            new_study.studyDate = datetime.datetime.today()

        try:
            new_study.birthDate = datetime.datetime.strptime(params['birthDate'], "%Y%m%d")
        except:
            new_study.birthDate = datetime.datetime.today()
        new_study.uploadDate = datetime.datetime.today()
        new_study.bodyPartExamined = params['bodyPartExamined']
        new_study.gender = params['gender']
        new_study.modality = params['modality']
        new_study.patientID = params['patientID']
        new_study.patientName = params['patientName']
        new_study.result = ""
        new_study.state = 'meta_only'

        if not db_find_one('study', {"studyUID": new_study.studyUID}):
            db_insert_one('study', new_study.to_db())
            ownership_info =  db_find_one('ownership', {"userID": userID})
            shared = ownership_info['shared']
            shared[(new_study.studyUID).replace('.','-')] = ""
            db_update_one('ownership', {"userID": userID}, ownership_info)
            res.statusCode = 1000
            res.data = new_study.to_dict(withSeries=False)
            return make_response(jsonify(res.to_dict()), 200)
        else:
            res.statusCode = 1102
        return make_response(jsonify(res.to_dict()), 200)
