import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {getUser, signout,onClickModel} from '../../auth'
import { GetLocaledText } from '../../localedFuncs'
import './index.less'
import {Link} from "react-router-dom";

/**
 * 退出系统button组件
 * @returns {Component}
 */
const SignoutButton = withRouter(
  ({ history }) => (
      <span>
            <span className='signout-btn' style={{height:"3rem"}}>
              <img className='user-icon' src={require('../../../static/images/user.png')}/>
              <span className='current-user'>{getUser()?getUser().account:""}</span>
              <Link to="/login"><img
                className='signout-icon'
                onClick={() => {
                    signout(() => history.push('/login'))
                  }}
                src={require('../../../static/images/logout.png')}/></Link>
            </span>
      </span>
  )
)

export default SignoutButton
