#!/bin/python
# coding=utf-8
import os
import sys

from flask import make_response, request, jsonify, abort
from flask_login import current_user

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_update_one
from models.response.response import response
from models.tool.Tool import Tool, parseToolInfo, saveToollist

required_key_all = ['studyUID', 'seriesID', 'instanceID']
required_key_post_put = ['data']


def on_tool():
    """
    @api {get post put delete} /api/tool  标注工具
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} studyUID 检查ID
    @apiParam {String} seriesID 序列ID
    @apiParam {String} instanceID 实例ID
    """
    res = response()
    userID = current_user.get_id()
    params = request.args.to_dict() if request.method == 'GET' else request.json
    if not all(key in params for key in required_key_all):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
    studyUID = params['studyUID']
    seriesID = params['seriesID']
    instanceID = params['instanceID']

    tool_info = db_find_one("tool", {"userID": {"$in": [userID, "imsightmed"]}, "studyUID": studyUID})
    if tool_info is None:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)
    toollist = parseToolInfo(tool_info)
    tool = filter(lambda t: t.seriesID == seriesID and t.instanceID == instanceID, toollist)

    if request.method == "GET":
        if tool:
            res.statusCode = 1000
            res.data = tool[0].to_dict()
            return make_response(jsonify(res.to_dict()), 200)
        else:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

    if request.method == 'DELETE':
        if tool:
            toollist.remove(tool[0])
            db_update_one("tool", {"userID": userID, "studyUID": studyUID}, saveToollist(toollist, studyUID, userID))

            res.statusCode = 1000
            res.data = tool[0].to_dict()
            return make_response(jsonify(res.to_dict()), 200)
        else:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

    if not all(key in params for key in required_key_post_put):
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)
    data = params['data']

    '''
    if request.method == "POST":
        if tool:
            res.statusCode = 1102
            return make_response(jsonify(res.to_dict()), 200)
        else:
            new_tool = Tool(studyUID, seriesID, instanceID, data)
            toollist.append(new_tool)
            db_update_one("tool", {"userID": userID, "studyUID": studyUID}, saveToollist(toollist, studyUID, userID))

            res.statusCode = 1000
            res.data = new_tool.to_dict()
            return make_response(jsonify(res.to_dict()), 200)
    '''

    if request.method == "PUT":
        if tool:
            tool[0].data = data
        else:
            new_tool = Tool(studyUID, seriesID, instanceID, data)
            tool = [new_tool]
            toollist.append(new_tool)

        db_update_one("tool", {"userID": userID, "studyUID": studyUID}, saveToollist(toollist, studyUID, userID))

        res.statusCode = 1000
        res.data = tool[0].to_dict()
        return make_response(jsonify(res.to_dict()), 200)
