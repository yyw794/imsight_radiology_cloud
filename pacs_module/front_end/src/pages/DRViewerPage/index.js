import React, { Component } from 'react'
import DRViewer from '../../component/DRViewer'
import api from '../../api'
import LoadingWrapper from '../../component/LoadingWrapper'
import UserRoleButtons from '../../component/UserRoleButtons'
import ToolBar from '../../component/DRToolBar'
import ThumbnailList from '../../component/ThumbnailList'
import axios from 'axios'
import { Card, Layout, message } from 'antd'
import {addDRMetaData} from '../../metaData.js'
import {getUser, getUserRole} from '../../auth'
import _ from 'underscore'

import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './index.less'

const { Header, Content, Footer } = Layout;

class DRViewerPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTool: 'pan',
      study: props.data,
      labels: props.data.labels,
      tools: props.data.tools.data,
      displayInstance: this.props.data.instances[0],
    }

  }

  componentDidMount() {
    // this.setState({});
  }

  componentWillUnmount() {
    // this.setState({});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.id.replace(/-/g, '.') !== this.state.study.studyUID) {
      console.log('DRViewerPage reload data')
      getData(nextProps).then(data => {
        this.setState({
          study: data,
          labels: data.labels,
          tools: data.tools
        })
        this.exitToolMode()
      })
    }
  }

  async saveScreenshotToReport() {
    let image = await this.viewer.getWrappedInstance().getScreenshot()
    var data = new FormData()
    data.append('file', image, 'image.png')
    data.append('taskID',this.state.study.taskID)
    var config = {
      onUploadProgress: function(progressEvent) {
        var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total )
        console.log(percentCompleted)
      }
    }
    return axios.post(`/api/upload_screenshot`, data, config)
      .then((res) => {
        message.success(GetLocaledText(this,"DRViewerPage.dialog.insertSuccess"))
        // message.success(GetLocaledText(this, "DRViewer.screenshot.success"))
      })
      .catch((err) => {
        message.success(GetLocaledText(this,"DRViewerPage.dialog.insertFail"))
        // message.error(GetLocaledText(this, "DRViewer.screenshot.failure"))
      })
  }

  exitToolMode() {
      let activeTool = this.state.activeTool
      if (activeTool === 'pan') {return}
      this.viewer.getWrappedInstance().deactivateTool(activeTool, 1)
      this.viewer.getWrappedInstance().deactivateTool('pan', 2)
      this.viewer.getWrappedInstance().activateTool('pan', 1)
      this.setState({activeTool: 'pan'})
  }

  onKeyDown = (e) => {
    if (e.key === 'Escape') {
      this.exitToolMode()
    }
  }

  onToolClick(name) {
    if (name === 'invert') {
      // this.setState({activeTool: null})
      return this.viewer.getWrappedInstance().toggleInvert()
    }
    if(name==='eraser'){
        this.setState({
            activeTool:"pan",
        })
        return this.viewer.getWrappedInstance().clearTool()
    }
    if (name === 'screenshot') {
      // this.setState({activeTool: null})
      return this.saveScreenshotToReport()
    }
    if (name === 'recover') {
      return this.viewer.getWrappedInstance().resetViewport()
    }
    if(name === 'report'){
      return  window.open("/report/"+JSON.parse(localStorage.getItem("fellowParams")).studyUID.split(".").join("-"));
    }
    let activeTool = this.state.activeTool
           if (activeTool === name) {
               this.exitToolMode()
           } else {
               this.viewer.getWrappedInstance().deactivateTool(activeTool, 1)
               this.viewer.getWrappedInstance().activateTool(name, 1)
               this.viewer.getWrappedInstance().activateTool('pan', 2) // use scroll button on mouse to pan
               this.setState({activeTool: name})
           }
  }
    onToolClickShow(name){
        if(this.state.activeTool=="pan"){
            this.viewer.getWrappedInstance().disableTool("lesionsMarker",1)
            this.setState({activeTool: name})
        }else{
            this.viewer.getWrappedInstance().enableTool(name)
            this.setState({activeTool: 'pan'})
        }
        // let activeTool = this.state.activeTool;
        // if(activeTool!=="hideBoxes"){
        //     this.setState({activeTool:name})
        //     return this.viewer.getWrappedInstance().deactivateTool(activeTool, 1)
        // }
        // if(activeTool=="hideBoxes"){
        //     this.viewer.getWrappedInstance().disableTool("lesionsMarker")
        //     this.setState({activeTool: 'pan'})
        // }else{
        //     this.viewer.getWrappedInstance().enableTool("lesionsMarker")
        //     this.setState({activeTool: name})
        // }
        // if(activeTool===name){
        //     this.viewer.getWrappedInstance().activateTool('pan', 2)
        //     this.viewer.getWrappedInstance().enableTool("lesionsMarker")
        //     this.setState({activeTool: 'pan'})
        // }else{
        //     this.viewer.getWrappedInstance().disableTool("lesionsMarker")
        //     this.setState({activeTool: name})
        // }


   }

  isForbidEdit() {
    const study = this.state.study
    const report = this.state.study.report
    if (!report) {return true}
    return (
      report.examined
      || (report.submitted && study.user_privilege.includes('reporter'))
      || (!report.submitted && study.user_privilege.includes('reviewer'))
    )
  }

  /**
   * display another instance
   * @param  {json} instance [instance data]
   * @return {undefined}
   */
  onInstanceClick = (instance) => {
    if (instance.instanceID === this.state.displayInstance.instanceID) {
      return
    }
    let {seriesID, instanceID} = instance
    let {taskID, studyUID} = this.state.study
    this.viewer.getWrappedInstance().saveToolsData()
    this.viewer.getWrappedInstance().saveLabelsData()
    Promise.all([
      api.getLabels({
        taskID,
        seriesID,
        instanceID
      }),
      api.getToolsData({
        studyUID,
        seriesID,
        instanceID
      }),
    ]).then(resp => {
      let [labels, tools] = resp
      tools = tools.data
      this.setState({
        displayInstance: instance,
        tools,
        labels
      })
      this.exitToolMode()
    })
  }

  render() {
    let {age, gender, patientName, studyDate, studyTime} = this.state.study
    let user = getUser()
    let userID = ''
    if (user) {
      userID = user.account
    }
    const studyUID = this.props.match.params.id.replace(/-/g, '.');
    return (
      <div style={{height: '100%', backgroundColor: '#000'}} id='dr-viewer-page'>
        <ToolBar
          onToolClick={this.onToolClick.bind(this)}
          onToolClickShow={this.onToolClickShow.bind(this)}
          activeTool={this.state.activeTool}
          forbidEdit={this.isForbidEdit()}
          forbidScreenshot={this.state.mode === 'label'}/>
        <DRViewer
          ref={input => {this.viewer = input}}
          study={this.state.study}
          instance={this.state.displayInstance}
          tools={this.state.tools}
          labels={this.state.labels}
          onLabelsChange={labels => this.setState({labels})}
          onToolsChange={tools=>this.setState({tools})}
          style={{height: '100%', position: 'relative'}}/>
        {
          this.props.data.instances.length > 1 ?
          <ThumbnailList
            data={this.props.data.instances.map(ins => {
              return Object.assign({id: ins.instanceID}, ins)
            })}
            activeIds={[this.state.displayInstance.instanceID]}
            renderItem={(ins, i) => (
              <div>
                <img src={ins.url}/>
                <p className='info'>{ins.viewPosition}</p>
              </div>
            )}
            onItemClick={this.onInstanceClick}/>
          :
          ''
        }
      </div>
    )
  }

}

async function getData(props) {
  const user = getUser()
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let study = await api.getStudyDetail({studyUID})
  let defaultSeries = Object.values(study.series)[0]
  let labels = await api.getLabels({
    taskID: study.taskID,
    seriesID: defaultSeries.seriesID,
    instanceID: defaultSeries.instances[0].instanceID
  })
  let tools = await api.getToolsData({
    studyUID,
    seriesID: defaultSeries.seriesID,
    instanceID: defaultSeries.instances[0].instanceID
  })

  let resps = [study, labels, tools]

  let studyData = resps[0]
  addDRMetaData(studyData)

  studyData.labels = resps[1]
  studyData.tools = resps[2]
  // studyData.report = resps[3]
  let report = await api.getReport({taskID: studyData.taskID})
  // if (user.type !== 'label' && (studyData.submitted || user.usergroup !== 'examiner')) {
  //     report = await api.getReport({taskID: studyData.taskID})
  // }
  studyData.report = report
  for(let series of Object.values(studyData.series)) {
    series.instances.forEach(ins => ins.seriesID = series.seriesID)
  }
  studyData.instances = _.flatten(Object.values(studyData.series).map(s => s.instances))
  return await studyData
}

DRViewerPage = LoadingWrapper(DRViewerPage, getData)

export default injectIntl(DRViewerPage)
