import configparser

config = configparser.ConfigParser()
config.read("config.ini")


def get_AppTitle():
	return str(config.get(section='pacs', option='AppTitle'))


def get_MoveAppTitle():
	return str(config.get(section='pacs', option='MoveAppTitle'))


def get_ListenPACSPort():
	return config.getint(section='pacs', option='ListenPACSPort')


def get_PeerHost():
	return str(config.get(section='pacs', option='PeerHost'))


def get_PeerPort():
	return config.getint(section='pacs', option='PeerPort')
	
def get_PeerAppTitle():
	return str(config.get(section='pacs', option='PeerAppTitle'))


def get_support_type():
	return str(config.get(section='pacs', option='support_type'))


def get_history_patient_list_days():
	return config.getint(section='pacs', option='history_patient_list_days')

def get_store_image_days():
	return config.getint(section='pacs', option='store_images_days')

def get_store_path_url():
	return str(config.get(section='server', option='store_path_url'))


def get_server_url():
	return str(config.get(section='server', option='server_url'))


def get_server_account():
	return str(config.get(section='server', option='server_account'))


def get_server_password():
	return str(config.get(section='server', option='server_password'))


def get_http_listen_ip():
	return str(config.get(section='server', option='http_listen_ip'))


def get_http_listen_port():
	return config.getint(section='server', option='http_listen_port')


def get_if_push_model():
	if_push = str(config.get(section='server', option='if_push_module'))
	if if_push == 'true':
		return True
	else:
		return False


def get_pull_data_method():
	return str(config.get(section='server', option='pull_data_method'))


def get_history_patient_list_image_days():
	return config.getint(section='pacs', option='history_patient_list_image_days')


def get_dcmtk_ip():
	return config.get(section='http', option='listen_http_IP')


def get_dcmtk_port():
	return config.get(section='http', option='listen_http_port')


def get_is_ris_support():
	return str(config.get(section='risDB', option='is_ris_support')) == 'true'


def get_hospital_index():
	return config.getint(section='risDB', option='ris_hospital_index')


def get_ris_database_type():
	return config.get(section='risDB', option='database_type')


def get_ris_host_IP():
	return config.get(section='risDB', option='host_IP')


def get_ris_username():
	return config.get(section='risDB', option='username')


def get_ris_password():
	return config.get(section='risDB', option='password')


def get_ris_database_name():
	return config.get(section='risDB', option='database_name')


def get_ris_table_name():
	return config.get(section='risDB', option='table_name')


def get_ris_patientID_name():
	return config.get(section='risDB', option='patientID_name')


def get_ris_modality_name():
	return config.get(section='risDB', option='modality_name')


def get_ris_studyUID_name():
	return config.get(section='risDB', option='studyUID_name')


def get_ris_studyDate_name():
	return config.get(section='risDB', option='studyDate_name')


def get_ris_studyPart_name():
	return config.get(section='risDB', option='studyPart_name')


def get_thread_number():
	return config.getint(section='server', option='pull_data_thread_number')
