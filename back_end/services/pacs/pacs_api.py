# coding=utf-8
import logging

logger = logging.getLogger("radiology")
from config import ADDRESS
import httplib2
import json
import sys
import os
import datetime

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from flask import make_response, request, jsonify, abort
from models.database.database_ops import *
from models.response.response import response
from flask_login import current_user
from services.basic.basic_api import socket_emit
import urllib
from urllib import urlencode
import thread

HOST = "http://{}:{}".format('pacs', 8000)


# 向dcmtk发送查找列表消息
def dcmtk_find_patient_list(condition):
    try:
        url = "{}/pacsAPI/find".format(HOST)
        http = httplib2.Http(timeout=300)
        headers = {'content-type': 'application/json'}
        data = urlencode(condition)
        resp, content = http.request(url + "?" + data, 'GET', body=json.dumps({}), headers=headers)
        if resp['status'] == '200':
            return content
        else:
            return None
    except Exception as e:
        logger.error(e)
        return None


# 向dcmtk发送move请求数据消息
def dcmtk_move_study_uid(studyUID, studyDate):
    try:
        url = "{}/pacsAPI/move?studyUID={}&studyDate={}".format(HOST, studyUID, studyDate)
        http = httplib2.Http(timeout=3000)
        headers = {'content-type': 'application/json'}
        resp, content = http.request(url, 'GET', body=json.dumps({}), headers=headers)
        if resp['status'] == '200':
            return content
        else:
            return None
    except Exception as e:
        logger.error(e)
    return None


# 向dcmtk发送get请求数据消息
def dcmtk_get_study_uid(studyUID, studyDate):
    try:
        url = "{}/pacsAPI/get?studyUID={}&studyDate={}".format(HOST, studyUID, studyDate)
        http = httplib2.Http(timeout=3000)
        headers = {'content-type': 'application/json'}
        resp, content = http.request(url, 'GET', body=json.dumps({}), headers=headers)
        if resp['status'] == '200':
            return content
        else:
            return None
    except Exception as e:
        logger.error(e)
    return None


# 获取详细列表信息 用于医生点击搜索功能
def unsend_studylist_from_pacs(query_value):

    result_list = []
    study_info_list = None
    condition = {}
    system_info = db_find_one("system_info",{})  # 配置的是推的模式直接退出
    if system_info is None or system_info["is_push"]:
        return result_list

    if len(query_value) == 0:  # 什么参数都不带
        return result_list

    if 'studyDate' in query_value:
        study_list_state = db_find_one("study_info_list_state",{'studyDate':query_value['studyDate']})
        # search from db study_info_list
        if study_list_state is not None and study_list_state['is_same']:
            query_value['state'] = {'$in': ['meta_only', 'pull_data', 'modality_unsupport']}
            study_info_list = db_find('study_info_list',query_value)
        else:
            condition['studyDate'] = query_value['studyDate']
    else:
        if 'patientName' in query_value:
            condition['patientName'] = urllib.quote(query_value['patientName'].encode('utf-8'))
        if 'patientID' in query_value:
            condition['patientID'] = query_value['patientID']
        if 'gender' in query_value:
            condition['gender'] = query_value['gender']
        if 'birthDate' in query_value:
            condition['birthDate'] = query_value['birthDate']

    # 本地数据库不存在则需要从pacs搜索
    if study_info_list is None:
        content_result = dcmtk_find_patient_list(condition)
        if content_result is None:
            logger.error("find patient list from condition {} exception ".format(condition))
            return []

        content_result = json.loads(content_result)
        if content_result['result'] != 'SUCCESS':
            logger.error("find patient list {} error, message {} ".format(condition, content_result['message']))
            return []

        if 'studyDate' in query_value:  # 插入改天数据到数据库
            content_study_list = content_result['data']
            db_find_update_otherwise_insert('study_info_list_state',{'studyDate': query_value['studyDate']},
                                            {'studyDate': query_value['studyDate'],"is_same": True})
            for si in content_study_list:
                try:
                    si['state'] = "meta_only"
                    si['bodyPartExamined'] = ""
                    si['doctorRequest'] = False
                    si['match_ris'] = False
                    if db_find_one("study_info_list", {'studyUID': si['studyUID']}) is not None:
                        continue
                    db_insert_one('study_info_list', si)
                except Exception as e:
                    logger.error("insert data to db error {} {}".format(si,e))

            # 从本地数据库搜索数据
            query_value['state'] = {'$in': ['meta_only', 'pull_data', 'modality_unsupport']}
            study_info_list = db_find('study_info_list', query_value)
        else:  # 按照特定的病人信息搜索
            study_info_list = []
            for sil in content_result['data']:
                try:
                    studyInfoTemp_pacs = db_find_one("study_info_list", {'studyUID': sil['studyUID']})
                    studyInfoTemp_flask = db_find_one("studylist", {'studyUID': sil['studyUID']})
                    sil['state'] = "meta_only"
                    sil['bodyPartExamined'] = ""
                    sil['doctorRequest'] = False
                    sil['match_ris'] = False
                    if studyInfoTemp_pacs is None:
                        db_insert_one('study_info_list', sil)

                    if studyInfoTemp_flask is None:
                        if studyInfoTemp_pacs is None:
                            study_info_list.append(sil)
                        else:
                            study_info_list.append(studyInfoTemp_pacs)

                except Exception as e:
                    logger.error("insert data to db error {} {}".format(sil,e))

    for data_temp in study_info_list:
        new_study = {}
        new_study['studyUID'] = data_temp['studyUID']
        new_study['birthDate'] = data_temp['birthDate']
        new_study['bodyPartExamined'] = data_temp['bodyPartExamined']
        new_study['gender'] = data_temp['gender']
        new_study['hospital'] = ''
        new_study['modality'] = data_temp['modality']
        new_study['patientID'] = data_temp['patientID']
        new_study['patientName'] = data_temp['patientName']
        new_study['studyDate'] = data_temp['studyDate']
        new_study['studyTime'] = data_temp['studyTime']
        new_study['uploadDate'] = None
        new_study['state'] = data_temp['state']
        new_study['labelType'] = ""
        new_study['result'] = ""
        new_study['labelNum'] = 0
        new_study['submitted'] = False
        new_study['diagnosisAdvice'] = ''
        new_study['user_privilege'] = []
        new_study['taskID'] = ''
        new_study['reverse'] = None
        new_study['age'] = 0
        result_list.append(new_study)
    return result_list


def get_patientInfo(study_uid):
    return db_find_one("study_info_list", {"studyUID": study_uid})


# 从studylist这个表中往node发送有结果的socket
def send_socketIO_to_node(study_uid,userID):
    study_info = db_find_one("studylist", {"studyUID": study_uid})
    socket_emit('UpdatestudyList', {'data': study_info, 'userID': userID,'taskID':'studyList'})


def update_pacs_list_state(study_uid,userID):
    study_info = db_find_one("studylist", {"studyUID": study_uid})
    study_info_pacs = db_find_one("study_info_list", {"studyUID": study_uid})

    if study_info is None:
        return

    if study_info_pacs is not None:
        study_info_pacs['state'] = study_info['state']
        db_update_one("study_info_list", {'studyUID': study_uid}, study_info_pacs)
        if study_info_pacs['doctorRequest']:
            socket_emit('UpdatestudyList', {'data': study_info, 'userID': userID, 'taskID': 'studyList'})


def doctor_request_image(studyUID, studyDate):
    system_info = db_find_one("system_info", {})
    if system_info is not None and system_info["pull_data_method"] == 'get':
        move_result = dcmtk_get_study_uid(studyUID, studyDate)
    else:
        move_result = dcmtk_move_study_uid(studyUID, studyDate)

    logger.info("doctor_request_image end {}".format(move_result))


def fetch_doctor_request():
    """
    @api {GET} /api/doctor_request
    @apiDescription   doctor manually request a meta-only record
    @apiVersion 1.0.0
    @apiParam {string} studyDate example format like 2018-11-12
    @apiParam {string} studyUID study instance uid
    @apiParamExample {json} Request-Example:
                          {"studyUID":"1.22.123587953523524525.23545.5623",
                          "studyDate":"2018-11-12"}
    @apiSuccessExample {json} Success-Response:
                          {"data"{},"statusCode":"1000"}
    @apiErrorExample {json} Success-Response:
                          {"data"{},"statusCode":"1100"}
    """
    res = response()
    userID = current_user.get_id()

    params = request.args.to_dict() if request.method == 'GET' else request.json
    logger.info("fetch_doctor_request {}".format(params))

    if not all(key in params for key in ['studyUID','studyDate']):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    studyuid = params['studyUID']
    study_date = params["studyDate"]

    study_info = db_find_one("study_info_list", {"studyUID": studyuid})
    if study_info is not None:
        study_info['doctorRequest'] = True
        study_info['state'] = 'pull_data'
        db_update_one("study_info_list", {'studyUID': studyuid}, study_info)
        socket_emit('UpdatestudyList', {'data': study_info, 'userID': userID, 'taskID': 'studyList'})
        thread.start_new_thread(doctor_request_image, (studyuid, study_date))
    else:
        logger.error("fetch_doctor_request studyuid {} not in db study_info_list".format(studyuid))

    if request.method == "GET":
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)
    else:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)


# 这里只针对医生手动点击拉取数据之后的图像不支持类型的做处理，只发送socket,数据库由pacs模块更新
def update_study_state():
    """
    @api {GET} /api/update_study_state
    @apiDescription   update a record to modality_unsupport 图像不支持
    @apiVersion 1.0.0
    @apiParam {string} studyUID study instance uid
    @apiParamExample {json} Request-Example:
                          {"studyUID":"1.22.123587953523524525.23545.5623"}
    @apiSuccessExample {json} Success-Response:
                          {"data"{},"statusCode":"1000"}
    @apiErrorExample {json} Success-Response:
                          {"data"{},"statusCode":"1100"}
    """
    res = response()
    userID = current_user.get_id()

    params = request.args.to_dict() if request.method == 'GET' else request.json
    logger.info("update_study_state {}".format(params))

    if not all(key in params for key in ['studyUID']):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    study_info = db_find_one("study_info_list", {"studyUID": params['studyUID']})
    socket_emit('UpdatestudyList', {'data': study_info, 'userID': userID, 'taskID': 'studyList'})

    if request.method == "GET":
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)
    else:
        res.statusCode = 1100
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)
