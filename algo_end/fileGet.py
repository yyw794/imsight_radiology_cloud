import os
import shutil
import gzip
import base64
import urllib


def getfile(data_json, target_path):
    """

    :param data_json:
    :param target_path:
    :return:
    """
    fileMethod = data_json['fileMethod']
    dataPath = data_json['dataPath']

    if fileMethod == "local":
        for path in dataPath:
            baseName = os.path.basename(path)
            shutil.copyfile(path, os.path.join(target_path, baseName))
    elif fileMethod == 'http':
        for path in dataPath:
            # TODO: fix it!
            urllib.urlretrieve(path, os.path.join(target_path, os.path.basename(path)))
    return os.listdir(target_path)


def getFileStr(target):
    info = ''
    with open(target, 'rb') as f:
        while True:
            block = f.read()
            if not block:
                break
            else:
                info += block
    return info


def saveAsComprass(src, target):
    with open(src, 'rb') as f_in, gzip.open(target, 'wb') as f_out:
        shutil.copyfileobj(f_in, f_out)


def encodeb64(input):
    return base64.b64encode(input)


def decodeb64(input):
    return base64.b64decode(input)
