#!/bin/python
#coding=utf-8
from Label import Label

class Nodule_Label(Label):
    def __init__(self):
        Label.__init__(self)
        self.x = 0
        self.y = 0
        self.z = 0
        self.url = ""
        self.nodule_prob = 0.0
        self.nodule_type = ""
        self.nodule_type_prob = 0.0
        self.nodule_bm = ""
        self.nodule_bm_prob = 0.0
        self.nodule_volume = 0
        self.nodule_diameter = 0
        self.nodule_avgHU = 0
        self.nodule_location = 0
        self.nodule_slice = []
        self.addByUser = False

    def to_dict(self):
        return{
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "coord": {"x": self.x, "y": self.y, "z": self.z},
            "width": 20,
            "height": 20,
            "nodule_prob": self.nodule_prob,
            "nodule_type": self.nodule_type,
            "nodule_type_prob": self.nodule_type_prob,
            "nodule_bm": self.nodule_bm,
            "nodule_bm_prob": self.nodule_bm_prob,
            "nodule_volume": self.nodule_volume,
            "nodule_diameter": self.nodule_diameter,
            "nodule_avgHU": self.nodule_avgHU,
            "nodule_location": self.nodule_location,
            "state": self.state,
            "url": self.url,
            "addByUser": self.addByUser
        }

    def to_db(self):
        return{
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "state": self.state,
            "url": self.url,
            "x": self.x,
            "y": self.y,
            "z": self.z,
            "nodule_prob": self.nodule_prob,
            "nodule_type": self.nodule_type,
            "nodule_type_prob": self.nodule_type_prob,
            "nodule_bm": self.nodule_bm,
            "nodule_bm_prob": self.nodule_bm_prob,
            "nodule_volume": self.nodule_volume,
            "nodule_diameter": self.nodule_diameter,
            "nodule_avgHU": self.nodule_avgHU,
            "nodule_location": self.nodule_location,
            "nodule_slice": self.nodule_slice,
            "addByUser": self.addByUser
        }

required_key = ["taskID", "labels", "studyUID", "labelType"]
required_key_label = ["labelID", "seriesID", "state", "x", "y", "z",\
                        "nodule_prob", "nodule_type", "nodule_type_prob", "nodule_bm", "nodule_bm_prob", \
                        "nodule_volume", "nodule_diameter", "nodule_avgHU", "nodule_location", "nodule_slice", "url", "addByUser"]

def parseNoduleLabelist(labels):
    labelist = []
    for label in labels:
        if not all(key in label for key in required_key_label):
            raise ValueError("Noule label must contain {}, while input contains {}".format(required_key_label, label.keys()))

        new_Nodulelabel = Nodule_Label()
        new_Nodulelabel.labelID = label['labelID']
        new_Nodulelabel.seriesID = label['seriesID']
        new_Nodulelabel.state = label['state']
        new_Nodulelabel.x = label['x']
        new_Nodulelabel.y = label['y']
        new_Nodulelabel.z = label['z']
        new_Nodulelabel.nodule_prob = label['nodule_prob']
        new_Nodulelabel.nodule_type = label['nodule_type']
        new_Nodulelabel.nodule_type_prob = label['nodule_type_prob']
        new_Nodulelabel.nodule_bm = label['nodule_bm']
        new_Nodulelabel.nodule_bm_prob = label['nodule_bm_prob']
        new_Nodulelabel.nodule_volume = label['nodule_volume']
        new_Nodulelabel.nodule_diameter = label['nodule_diameter']
        new_Nodulelabel.nodule_avgHU = label['nodule_avgHU']
        new_Nodulelabel.nodule_location = label['nodule_location']
        new_Nodulelabel.nodule_slice = label['nodule_slice']
        new_Nodulelabel.url = label['url']
        new_Nodulelabel.addByUser = label['addByUser']

        labelist.append(new_Nodulelabel)

    return labelist
