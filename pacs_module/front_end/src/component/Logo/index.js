import React, { Component } from 'react'
import {withRouter} from 'react-router'
import './index.less'

const style = {
  'cursor': 'pointer',
  'WebkitUserSelect': 'none',
  'MozUserSelect': 'none',
  'msUserSelect': 'none',
  'userSelect': 'none',
}

const Logo = withRouter(
  ({ history }) => (
    <div className="logo">
      <img style={style} className='logo-image' src={require('../../../static/images/logo.png')} /*onClick={() => history.push('/')}*//>
      <span className="company-name">Imsight Radiology Platform</span>
    </div>
  )
)

export default Logo
