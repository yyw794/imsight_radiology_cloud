# backend production mode
#/usr/local/bin/gunicorn -c /app/app_gunicorn_http.cfg app:app & /usr/bin/python /app/app_socket.py

# backend debug mode
/usr/bin/python /app/app.py & /usr/bin/python /app/app_socket.py
