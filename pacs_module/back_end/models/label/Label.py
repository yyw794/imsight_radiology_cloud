#!/bin/python
#coding=utf-8
import uuid

statelist = ["assign", "diagnosis", "review", "pass"]

class Label(object):
    def __init__(self):
        self.seriesID = ""
        self.instanceID = ""
        self.labelID = str(uuid.uuid4())
        self.globalTag = True
        self.diseaseTag = ""
        self._state = "diagnosis"

    def to_dict(self):
        return {
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "instanceID": self.instanceID,
            "globalTag": self.globalTag,
            "diseaseTag": self.diseaseTag,
            "state": self.state
        }

    def to_db(self):
        return {
            "labelID": self.labelID,
            "seriesID": self.seriesID,
            "instanceID": self.instanceID,
            "globalTag": self.globalTag,
            "diseaseTag": self.diseaseTag,
            "state": self.state
        }

    def hello(self):
        if self.state == 'assign':
            self.state = 'diagnosis'
            return True
        return False

    def submit(self):
        self.state = statelist[2]

    def unsubmit(self):
        self.state = statelist[1]

    def confirm(self):
        self.state = statelist[3]

    def reject(self):
        self.state = statelist[1]

    def unconfirm(self):
        self.state = statelist[2]

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        if state not in statelist:
            raise ValueError("Label state must be one of {}, while input is {}".format(statelist, state))
        self._state = state

required_key_label = ["labelID", "seriesID", "instanceID", "globalTag", "diseaseTag", "state"]

def parseLabelist(labels):
    for label in labels:
        if not all(key in label for key in required_key_label):
            raise ValueError("label must contain {}, while input contains {}".format(required_key_label, label.keys()))

        new_label = Label()
        new_label.labelID = label['labelID']
        new_label.seriesID = label['seriesID']
        new_label.instanceID = label['instanceID']
        new_label.globalTag = label['globalTag']
        new_label.diseaseTag = label['diseaseTag']
        new_label.state = label['state']
        labelist.append(new_label)

    return labelist