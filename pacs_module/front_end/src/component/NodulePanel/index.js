import React, { Component } from 'react';
import {Table, Row, Col, Icon, Modal, Select, Button, Affix} from 'antd'
import _ from 'underscore'
import Immutable from 'immutable'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import './index.less'
import { getUserRole } from '../../auth';
import api from '../../api';


const POS_IMG = {
  0: require('../../../static/images/lungs_lt.png'),
  1: require('../../../static/images/lungs_lb.png'),
  2: require('../../../static/images/lungs_rt.png'),
  3: require('../../../static/images/lungs_rb.png'),
  4: require('../../../static/images/lungs_rm.png')
}

const RISK_EN2CN = {
  Benign: <FormattedMessage id = "NodulePanel.risk-text.benign"/>,
  Low: <FormattedMessage id = "NodulePanel.risk-text.low"/>,
  Middle: <FormattedMessage id = "NodulePanel.risk-text.medium"/>,
  High: <FormattedMessage id = "NodulePanel.risk-text.high"/>,
  emptyString:"-"
}

function getRiskText(bm) {
  if(bm === "Benign"){
    return 'Benign'
  }else if(bm=== "Mainly-Benign"){
    return 'Low'
  }else if(bm=== "Mainly-Malignant"){
    return 'Middle'
  }else if(bm=== "Malignant"){
    return 'High'
  }else{
    return "emptyString"
  }
}

function getRiskColor(bm) {  
  if(bm === "Benign"){
    return '#00ff00'
  }else if(bm=== "Mainly-Benign"){
    return '#ffff00'
  }else if(bm=== "Mainly-Malignant"){
    return '#ffa501'
  }else if(bm=== "Malignant"){
    return '#ff0000'
  }else{
    return '#00ff00'
  }
}

const TYPE_EN2CN = {
  GroundGlass: <FormattedMessage id = "NodulePanel.subclass.glass"/>,
  PartSolid: <FormattedMessage id = "NodulePanel.subclass.subsolid"/>,
  Solid: <FormattedMessage id = "NodulePanel.subclass.solid"/>,
  Calcification: <FormattedMessage id = "NodulePanel.subclass.calcified"/>
}

function getTypeByHU(HU) {
  if (HU < -500) {
    return 'GroundGlass'
  } else if (HU >= -500 && HU < -200) {
    return 'PartSolid'
  } else if (HU >= -200 && HU < 150) {
    return 'Solid'
  } else {
    return 'Calcification'
  }
}

function getNoduleType(nodule) {
  if(nodule.nodule_type)
  {
      if (nodule.nodule_type === "Ground-Glass"){
        return "GroundGlass"
      }else if(nodule.nodule_type === "Part-Solid"){
        return "PartSolid"
      }else if(nodule.nodule_type === "Solid"){
        return "Solid"
      }else if(nodule.nodule_type === "Calcification"){
        return "Calcification"
      }
  }else{
    return getTypeByHU(nodule.nodule_avgHU)
  }
}

function renderDetail(nodule,maxZ=0,zReverse=false) {
  if (!nodule) return ''
  return (
    <div className='nodule-detail'>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.size'/></Col>
        <Col span={12}>{nodule.nodule_diameter.toFixed(1)}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.malignancy'/></Col>
        <Col span={12} style={{color: getRiskColor(nodule.nodule_bm)}}>{RISK_EN2CN[getRiskText(nodule.nodule_bm)]}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.subclass'/></Col>
        <Col span={12}>{TYPE_EN2CN[getNoduleType(nodule)]}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.location'/></Col>
        <Col span={12}>{`[${nodule.coord.x}, ${nodule.coord.y}, ${zReverse?(maxZ-nodule.coord.z+1):nodule.coord.z}]`}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.anatomical-location'/></Col>
        <Col span={12}>
          <img src={POS_IMG[nodule.nodule_location]}/>
        </Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.average-density'/></Col>
        <Col span={12}>{nodule.nodule_avgHU.toFixed(1)}</Col>
      </Row>
      <Row>
        <Col span={12}><FormattedMessage id='NodulePanel.nodule-detail.volume'/></Col>
        <Col span={12}>{nodule.nodule_volume.toFixed(1)}</Col>
      </Row>
    </div>
  )
}

class NodulePanel extends Component {
  constructor(props) {
    super(props)
    this.originData = Immutable.fromJS(this.props.data).sort(this.getSorter('coord.z', true))
    if (!props.fixedKey) {
      this.originData = this.originData.map((data, i) => data.set('key', i + 1))
    }
    this.state = {
      selectedNodule: null,
      selectedId: null,
      selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS(),
      data: this.originData,
      sortColumn: 'coord.z',
      sortReverse:true,
      addModalVisible: false
    }
    this.pageSize = window.screen.width< window.screen.height ? 16:Math.round((window.screen.height-550)/25)
  }
  onResize = () => {
    this.pageSize = window.screen.width< window.screen.height ? 16:Math.round((window.screen.height-550)/25)
  }
  componentDidMount() {
    window.addEventListener("resize", _.debounce(this.onResize, 100)) //Jackey: Why delay it by using _.debounce, but not simply passing this.onResize?
    //window.addEventListener("resize", this.onResize);
  }
  componentWillReceiveProps(nextProps) {
    this.originData = Immutable.fromJS(nextProps.data).sort(this.getSorter('coord.z', true))
    if (!nextProps.fixedKey) {
      this.originData = this.originData.map((data, i) => data.set('key', i + 1))
    }
    this.setState({
      selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS(),
    })
        
    if(!this.originData.equals(this.state.data))
    {
      this.sortBy(this.state.sortColumn,this.state.sortReverse)
    }
  }
  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   this.originData = Immutable.fromJS(this.pops.data).sort(this.getSorter('coord.z', true))
  //   if (!this.props.fixedKey) {
  //     this.originData = this.originData.map((data, i) => data.set('key', i + 1))
  //   }
  //   // this.setState({
  //   //   selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS(),
  //   // });
  //   Object.assign(this.state, { selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS() });
  //   this.sortBy(this.state.sortColumn)
  // }

  getSorter(col, reverse=false) {
    return (a, b) => {
      let v1 = a.get(col)
      let v2 = b.get(col)
      if(!v1 || !v2 )
      {
        v1=Number(a.get("coord").get("z"))*1000*1000 +Number(a.get("coord").get("x"))*1000+Number(a.get("coord").get("y"))
        v2=Number(b.get("coord").get("z"))*1000*1000 +Number(b.get("coord").get("x"))*1000+Number(b.get("coord").get("y"))
      }
      if (v1 < v2) {
        return reverse ? 1 : -1
      }
      if (v1 > v2) {
        return reverse ? -1 : 1
      }
      if (v1 === v2) {
        return 0
      }
    }
  }

  sortBy(col,reverse=false) {
    let data = this.originData.sort(this.getSorter(col, reverse))
    this.setState({
      data,
      sortColumn: col,
    })
  }

  onSelectChange = (selectedRowKeys) => {
    let oldKeys = this.state.selectedRowKeys
    let newKeys = selectedRowKeys
    let add = newKeys.filter(x => !oldKeys.includes(x))
    if (add.length > 0) {
      let record = this.state.data.find(x => x.get('key') === add[0]).toJS()
      this.setState({selectedId: record.labelID})
      if (_.isFunction(this.props.onSelectNodule)) {
        // FIXME: to ensure a correct image is crop, must wait for the image to be display
        // so the promise in onSelectNodule will be resolved after 2 frames
        this.props.onSelectNodule(record).then(() => _.isFunction(this.props.onAddScreenshot) && this.props.onAddScreenshot(record))
      }
    }
    let remove = oldKeys.filter(x => !newKeys.includes(x))
    if (remove.length > 0) {
      let record = this.state.data.find(x => x.get('key') === remove[0]).toJS()
      _.isFunction(this.props.onRemoveScreenshot) && this.props.onRemoveScreenshot(record)
    }

    this.setState({ selectedRowKeys });
  }

  getNodulePage(nodule) {
    let page = 1
    let idx = this.state.data.findIndex(n => n.get('labelID') === nodule.labelID)
    return Math.ceil((idx + 1) / this.pageSize)
  }

  getSelectedNodule() {
    if (this.state.selectedId === null) {return null}
    const nod = this.state.data.find(n => n.get('labelID') === this.state.selectedId)
    if (!nod) {
      this.setState({selectedId: null})
      return null
    }
    return nod.toJS()
  }

  selectNodule(nodule) {
    let currentPage = this.getNodulePage(nodule)
    this.setState({selectedId: nodule.labelID, currentPage})
  }

  deleteNodule = () => {
    if (this.state.selectedId === null) return
    Modal.confirm({
      title: GetLocaledText(this,"NodulePanel.dialog.deleteNodule"),
      onOk: () => {
        this.props.onDelete(this.state.selectedId)
        this.setState({selectedId: null})
      }
    })
  }

  render() {
    const columns = [
      {
        title: GetLocaledText(this,"NodulePanel.table-header.number"),
        dataIndex: 'key',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render: (value, record) => {
          let text = `${this.props.keyPrefix ? this.props.keyPrefix : 'N'}${value}`
          if (record.addByUser) {
            text += '*'
          }
          return text
        }
      },
      {
        title: GetLocaledText(this,"NodulePanel.table-header.diam"),
        dataIndex: 'nodule_diameter',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render: (value, record) => {
          if (value === null) {return '-'}
          return value.toFixed(1)
        }
      },
      {
        title: GetLocaledText(this,"NodulePanel.table-header.malignancy"),
        dataIndex: 'nodule_bm',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render: (value, record) => {
          if (value === null) {return ''}
          return <span style={{color: getRiskColor(value)}}>{RISK_EN2CN[getRiskText(value)]}</span>
        }
      },
      {
        title: GetLocaledText(this,"NodulePanel.table-header.subclass"),
        dataIndex: 'nodule_type',
        width:'55px',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render: (value, record) => {
          if (record.subtTrue === null && record.subt === null) {return ''}
          return TYPE_EN2CN[getNoduleType(record)]
        }
      },
      {
        title: GetLocaledText(this,"NodulePanel.table-header.z"),
        dataIndex: 'coord.z',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render:(value,record)=>{
          return this.props.zReverse?(Number(this.props.maxZ)-record.coord.z+1):record.coord.z
        }
      },
      {
        title: GetLocaledText(this,"NodulePanel.table-header.avrgHU"),
        dataIndex: 'nodule_avgHU',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex,!this.state.sortReverse)
              this.setState({sortReverse:!this.state.sortReverse})
            }
          }
        },
        render: (value) => {
          if (value === null) {return '-'}
          return value.toFixed(1)
        }
      },
    ]

    const nodule = this.getSelectedNodule()
    const { loading, selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      columnWidth: 20,
      hideDefaultSelections: true,
    };

    const OptionSubvariety = Select.Option;
    const MalignantRisk = Select.Option;

    return (
      <div className='nodule-panel'>

        <div className='nodule-table'>

          <Table
            rowSelection={this.props.hideSelection ? null : rowSelection}
            columns={columns}
            dataSource={this.state.data.toJS()}
            rowClassName={(record) => {
              return this.state.selectedId && record.labelID === this.state.selectedId ?
                'active' : ''
            }}
            size="small"
            pagination={{
              pageSize: this.pageSize,
              simple: true,
              showTotal: (total, range) => <span><FormattedMessage id='NodulePanel.page-handle.pre-total'/><span className='nodule-count'>{total}</span><FormattedMessage id='NodulePanel.page-handle.post-total'/></span>,
              onChange: (currentPage) => this.setState({currentPage}),
              current: this.state.currentPage
            }}
            onRow={record => {
              return {
                onClick: () => {
                  this.setState({selectedId: record.labelID})
                  _.isFunction(this.props.onSelectNodule) && this.props.onSelectNodule(record)
                }
              }
            }}/>
          {
            this.state.data.size > 0 ?
            <span className='ant-pagination-total-text'><FormattedMessage id='NodulePanel.page-handle.pre-total'/><span className='nodule-count'>{this.state.data.size}</span><FormattedMessage id='NodulePanel.page-handle.post-total'/></span>
            :
            ''
          }
          {
            !this.props.hideEditNoduleBtn ?
            <div className='action-btns'>
              <Icon type="plus-circle-o" onClick={this.props.onAdd}/>
              <Icon type="delete" onClick={this.deleteNodule}/>
            </div>
            :
            ''
          }

        </div>

        {
          _.isFunction(this.props.renderDetail) ?
          this.props.renderDetail(this.getSelectedNodule())
          :
          renderDetail(this.getSelectedNodule(),this.props.maxZ,this.props.zReverse)
        }

      </div>

    );
  }

}

export {getRiskText, getRiskColor, getNoduleType, POS_IMG, RISK_EN2CN, TYPE_EN2CN};
export default injectIntl(NodulePanel, {withRef: true});
