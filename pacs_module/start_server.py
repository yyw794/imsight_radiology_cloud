from request_interface.http_server import start_server, do_upload_image_path,login_request
import thread
import logging
from request_interface.logger import *

log = logging.getLogger("PACSModule")


if __name__ == "__main__":
	try:

		log.info("******************** start_server success ********************")
		thread.start_new_thread(do_upload_image_path, ())

		start_server()
	except KeyboardInterrupt:
		log.error("create listen_process thread error")
