import {
  EVENTS,
  toolColors,
  addToolState,
  getToolState,
  clearToolState,
  setToolOptions,
  getToolOptions,
  isMouseButtonEnabled,
} from 'cornerstone-tools';
import * as cornerstone from 'cornerstone-core'
import {point} from 'cornerstone-math'
import _ from 'underscore'

const toolType = 'CT-noduleBox'
const BOX_SIZE = 40
let configuration = {}

function mouseClickCallback(e) {
  let eventData = Object.assign({}, e.detail);
  const element = eventData.element;
  const options = getToolOptions(toolType, element);

  if (options.mouseButtonMask.has(eventData.which)) {
    if (_.isFunction(options.onClick)) {
      const coord = eventData.currentPoints.image
      const {columns, rows} = eventData.image
      const coordCanvas = eventData.currentPoints.canvas

      // rgba
      let idx = (Math.round(coord.x) + Math.round(coord.y) * eventData.image.columns) * 4
      let pixelData = eventData.image.getPixelData()

      // // WARNING: since it's a gray scale image stored in rgba, rgb value will be equal to gray scale
      let grayScale = pixelData[idx]

      let horizontalRatio = Math.min(Math.max(coord.x / columns, 0), 1)
      let verticalRatio = Math.min(Math.max(coord.y / rows, 0), 1)
      let clickedNodules = getClickedNodule(element, horizontalRatio, verticalRatio)
      eventData.currentPoints.ratio = {x: horizontalRatio, y: verticalRatio}
      eventData.clickedNodules = clickedNodules
      eventData.grayScale = grayScale
      options.onClick(eventData)
      // options.onClick(horizontalRatio, verticalRatio, clickedNodules, grayScale, coord)
    }
  }
}

function onImageRendered(e) {
  const eventData = e.detail;
  const element = eventData.element
  // console.log(element, toolData, stack);

  // const toolData = getToolState(element, toolType);
  // if (toolData === undefined) {
  //   return;
  // }
    const stack = getToolState(element, 'stack')
  if (stack === undefined) {
    return
  }

  const context = eventData.canvasContext.canvas.getContext('2d');
  // const color = toolColors.getToolColor();
  let color = 'yellow'
  const options = getToolOptions(toolType, element)
  // const nodules = toolData.data
  const nodules = options.getData()
  const axis = options.axis
  const depth = options.depth
  const {horizontalLayers, verticalLayers} = options

  context.save()
  context.setTransform(1, 0, 0, 1, 0, 0);
  const viewport = cornerstone.getViewport(element)

  const currentIdx = stack.data[0].currentImageIdIndex

  // let insideNodule = false
  let insideNodules = getInsideNodules(element, options.getCurrentIdx())
  for (let i = 0; i < nodules.length; i++) {
    let nod = nodules[i]

    // if (nod.coord[axis] !== currentImageIdIndex + 1) {
    if (currentIdx + 1 < nod.coord[axis] - depth / 2 || currentIdx + 1 > nod.coord[axis] + depth / 2) {
      continue
    }
    let coord = {x: 0, y: 0}
    // HACK: dont't know why the -1 here, but the box's position will not be accurate without it
    if (axis === 'z') {
      coord.x = (nod.coord.x - 1) / options.horizontalLayers
      coord.y = (nod.coord.y - 1) / options.verticalLayers
    } else if (axis === 'x') {
      coord.x = (nod.coord.y - 1) / options.horizontalLayers
      coord.y = 1 - nod.coord.z / options.verticalLayers
    } else if (axis === 'y') {
      coord.x = (nod.coord.x - 1) / options.horizontalLayers
      coord.y = 1 - nod.coord.z / options.verticalLayers
    }
    // if (!insideNodule) {
    //   insideNodule = point.insideRect(
    //     {x: box.x + BOX_SIZE / 2, y: box.y + BOX_SIZE / 2},
    //     {left: coord.x * imgWidth, top: coord.y * imgHeight, height: BOX_SIZE, width: BOX_SIZE}
    //   )
    // }
    if(options.hideBoxes){
      color='transparent'
    }
    else if (insideNodules.length && insideNodules[0].labelID === nod.labelID) {
      color = 'red'
    } else {
      color = 'yellow'
    }
    coord.x *= eventData.image.width
    coord.y *= eventData.image.height
    context.beginPath();
    context.strokeStyle = color;
    context.setLineDash([5, 2])
    context.lineWidth = 1;
    // context.rect(x - BOX_SIZE, y - BOX_SIZE, 40, 40);
    coord = cornerstone.pixelToCanvas(eventData.element, coord)
    const size = BOX_SIZE * viewport.scale
    context.rect(coord.x - size / 2, coord.y - size / 2, size, size);
    context.stroke();
  }

  // draw select box
  // context.save()
  // box = cornerstone.pixelToCanvas(eventData.element, box)
  // box.size = BOX_SIZE * viewport.scale
  // context.strokeStyle = toolColors.getActiveColor();
  // if (!insideNodule) {
  //   context.beginPath();
  //   context.lineWidth = 2;
  //   context.setLineDash([5, 2])
  //   context.rect(box.x - box.size / 2, box.y - box.size / 2, box.size, box.size);
  //   context.stroke();
  // } else {
  //   context.beginPath();
  //   context.arc(box.x, box.y, 1, 0, 2 * Math.PI, true);
  //   context.stroke();
  // }
  context.restore()
}

function getClickedNodule(element, horizontalRatio, verticalRatio) {
  const options = getToolOptions(toolType, element)

  const {
    horizontalLayers, verticalLayers, mainAxisLayers,
    axis, getData, getCurrentIdx
  } = options
  const nodules = getData()
  const currentIdx = getCurrentIdx()

  const imgWidth = cornerstone.getImage(element).width
  const imgHeight = cornerstone.getImage(element).height

  let currentX
  let currentY
  let currentZ

  if (axis === 'x') {
    currentX = currentIdx.x
    currentY = Math.floor(horizontalRatio * horizontalLayers)
    currentZ = Math.floor((1 - verticalRatio) * verticalLayers)
  } else if (axis === 'y') {
    currentX = Math.floor(horizontalRatio * horizontalLayers)
    currentY = currentIdx.y
    currentZ = Math.floor((1 - verticalRatio) * verticalLayers)
  } else if (axis === 'z') {
    currentX = Math.floor(horizontalRatio * horizontalLayers)
    currentY = Math.floor(verticalRatio * verticalLayers)
    currentZ = currentIdx.z
  }

  return getInsideNodules(element, {x: currentX, y: currentY, z: currentZ})
  // if (candidates.length > 0) {
  //   this.nodulePanel.selectNodule(candidates[0])
  // }
}

function getInsideNodules(element, currentIdx) {
  const options = getToolOptions(toolType, element)

  const {
    horizontalLayers, verticalLayers, mainAxisLayers,
    axis, getData, getCurrentIdx, depth
  } = options
  const nodules = getData()
  // const currentIdx = getCurrentIdx()
  const imgWidth = cornerstone.getImage(element).width
  const imgHeight = cornerstone.getImage(element).height

  let xDepth
  let yDepth
  let zDepth
  if (axis === 'x') {
    // xDepth = Math.round(BOX_SIZE / imgWidth * mainAxisLayers)
    xDepth = depth
    yDepth = Math.round(BOX_SIZE / imgWidth * horizontalLayers)
    zDepth = Math.round(BOX_SIZE / imgHeight * verticalLayers)
  } else if (axis === 'y') {
    xDepth = Math.round(BOX_SIZE / imgWidth * horizontalLayers)
    // yDepth = Math.round(BOX_SIZE / imgWidth * mainAxisLayers)
    yDepth = depth
    zDepth = Math.round(BOX_SIZE / imgHeight * verticalLayers)
  } else if (axis === 'z') {
    xDepth = Math.round(BOX_SIZE / imgWidth * horizontalLayers)
    yDepth = Math.round(BOX_SIZE / imgWidth * verticalLayers)
    // zDepth = Math.round(BOX_SIZE / imgHeight * mainAxisLayers)
    zDepth = depth
  }
  let candidates = []
  nodules.forEach(nodule => {
    let {x, y, z} = nodule.coord

    if (currentIdx.x + 1 >= x - xDepth / 2 && currentIdx.x + 1 <= x + xDepth / 2
      && currentIdx.y + 1 >= y - yDepth / 2 && currentIdx.y + 1 <= y + yDepth / 2
      && currentIdx.z + 1 >= z - zDepth / 2 && currentIdx.z + 1 <= z + zDepth / 2) {
      candidates.push(nodule)
    }
  })
  // if the number of candidates is more than one, pick the nearest one
  candidates.sort((nod1, nod2) => {
    let coord1 = nod1.coord
    let coord2 = nod2.coord
    let dist1 = Math.pow(coord1.x - currentIdx.x - 1, 2) + Math.pow(coord1.y - currentIdx.y - 1, 2) + Math.pow(coord1.z - currentIdx.z - 1, 2)
    let dist2 = Math.pow(coord2.x - currentIdx.x - 1, 2) + Math.pow(coord2.y - currentIdx.y - 1, 2) + Math.pow(coord2.z - currentIdx.z - 1, 2)
    return dist1 - dist2
  })
  return candidates
}

function onNewImage(e) {
  // const eventData = e.detail;
  // const element = eventData.element
  // const options = getToolOptions(toolType, element)
  // clearToolState(element, toolType)
  // for (let data of options.nodules) {
  //   addToolState(element, toolType, Object.assign({}, data))
  // }
}

export default {
  activate (element, mouseButton) {
    let options = getToolOptions(toolType, element)
    options.mouseButtonMask.add(mouseButton)
    setToolOptions(toolType, element, options);

    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
    element.addEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  disable (element) {
    let options = getToolOptions(toolType, element)
    options.clear()
    element.removeEventListener(EVENTS.IMAGE_RENDERED, onImageRendered)
    element.removeEventListener('cornerstonenewimage', onNewImage)
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  enable (element, options) {
    options.mouseButtonMask = new Set()
    setToolOptions(toolType, element, options);
    element.addEventListener(EVENTS.IMAGE_RENDERED, onImageRendered)
    element.addEventListener('cornerstonenewimage', onNewImage)
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  deactivate (element) {
    element.removeEventListener(EVENTS.MOUSE_CLICK, mouseClickCallback);
  },
  getConfiguration () {
    return configuration;
  },
  setConfiguration (config) {
    configuration = config;
  },
  updateOptions (element, options) {
    let oldOptions = getToolOptions(toolType, element)
    setToolOptions(toolType, element, Object.assign({}, oldOptions, options));
  },
  getInsideNodules,
  BOX_SIZE
}
