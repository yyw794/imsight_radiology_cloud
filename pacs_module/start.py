import thread, sys, time
from request_interface.request_image import init_do_something, time_task_a_hour, today_request_function
from request_interface.logger import *
from request_interface.risconnect import check_ris_config
import logging
from request_interface.initconfig import get_if_push_model
log = logging.getLogger("PACSModule")


if __name__ == "__main__":
	try:
		log.info("******************** pacs module start ********************")
		init_do_something()

		if get_if_push_model() is False:
			time.sleep(5)
			thread.start_new_thread(today_request_function, ())

		if check_ris_config() is False:
			log.error("******************** ris config error exit********************")
			sys.exit()

		time_task_a_hour()
	except KeyboardInterrupt:
		log.error("******************** pacs module quit ********************")

