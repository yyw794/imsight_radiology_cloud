#!/bin/python
# coding=utf-8
import os
import sys

from flask import make_response, request, jsonify, abort
from flask_login import current_user

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_update_one
from models.response.response import response
from models.report.Report import parseReportInfo
from models.label.Label_IO import updateLabel, parseLabelInfo, saveLabelist
from services.algo.nodule_service import run_lung_nodule_analyzer
import logging

logger = logging.getLogger("radiology")
from services.algo.on_radiomics import create_radiomics
from services.basic.basic_api import socket_emit

required_key_all = ["taskID", "labelType"]
disease_key_all = ["taskID"]
required_key_put = ['labels', 'seriesID', "labelType"]
disease_key_put = ["taskID", "disease"]
optional_key_instance_label_type = ["seriesID", "instanceID"]
optional_key_series_label_type = ["seriesID"]
instance_label_type = ['General', 'DX']
series_label_type = ['Nodule', 'LiverTumor', "Rib"]


def on_label():
    """
    @api {get put} /api/label  标签操作
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} taskID 任务ID
    @apiParam {String} [labelType] 标签类型
    @apiParamExample {string} get Request-Example:
        /api/label?taskID=c06b1942-f700-11e8-9ed5-0242c0a8e004&seriesID=1.2.840.113619.2.334.3.313263218.304.1505378168.818.3&labelType=Nodule
    @apiParamExample {json} put Request-Example:
        {"taskID":"c06b1942-f700-11e8-9ed5-0242c0a8e004","seriesID":"1.2.840.113619.2.334.3.313263218.304.1505378168.818.3",
        "labels":[{"addByUser":false,"coord":{"x":184,"y":336,"z":41},"height":20,"labelID":"b2655e4e-f7ae-11e8-ac80-0242c0a8f004",
        "nodule_avgHU":76,"nodule_bm":{"Benign":0.23903326690197,"Mainly_Benign":0.463744938373566,"Mainly_Malignant":0.288795381784439,
        "Malignant":0.00842644833028316},"nodule_diameter":7,"nodule_location":2,"nodule_prob":0.629735291004181,
        "nodule_type":{"Calcification":0.0290025752037764,"Ground_Glass":0.00483044842258096,"Part_Solid":0.171227499842644,
        "Solid":0.794939517974854},
        "nodule_volume":261,"seriesID":"1.2.840.113619.2.334.3.313263218.304.1505378168.818.3","state":"diagnosis","url":"","width":20}
        ]}
    @apiSuccess {bool} addByUser 是否由用户添加
    @apiSuccess {dict} coord  结节坐标 {'x': 361 'y' 332, 'z': 19}
    @apiSuccess {Num} height 标签的长度
    @apiSuccess {Num} width 标签的宽度
    @apiSuccess {String} labelID 标签ID
    @apiSuccess {float} nodule_avgHU 结节平均密度
    @apiSuccess {dict} nodule_bm Benign: 良性概率， Mainly_Benign: 偏良性概率，Mainly_Malignant: 偏恶心概率，Malignant: 恶性概率
    @apiSuccess {Num} nodule_diameter 结节直径
    @apiSuccess {Num} nodule_location 结节位置，0: 左上肺，1: 左下肺，2: 右上肺，3: 右下肺，4: 右中肺
    @apiSuccess {float} nodule_prob 结节恶心风险
    @apiSuccess {dict} nodule_type 结节亚类，Calcification: 钙化概率，Ground_Glass: 磨玻璃概率，Part_Solid: 部分实性概率，Solid: 实性概率
    @apiSuccess {String} state 诊断状态，assign: 待诊断, diagnosis: 诊断中, review: 审核中, pass: 完成
    @apiSuccessExample {json} Success-Response:
    {
      "data": [
        {
          "addByUser": false,
          "coord": {
            "x": 361,
            "y": 332,
            "z": 19
          },
          "height": 20,
          "labelID": "a22b9858-f769-11e8-8cc3-0242c0a8e004",
          "nodule_avgHU": 92.80000305175781,
          "nodule_bm": {
            "Benign": 0.3952668607234955,
            "Mainly_Benign": 0.5465527772903442,
            "Mainly_Malignant": 0.05797053501009941,
            "Malignant": 0.00020988033793400973
          },
          "nodule_diameter": 8,
          "nodule_location": 1,
          "nodule_prob": 0.29196715354919434,
          "nodule_type": {
            "Calcification": 0.017011120915412903,
            "Ground_Glass": 0.04750533774495125,
            "Part_Solid": 0.4360564053058624,
            "Solid": 0.4994271397590637
          },
          "nodule_volume": 281,
          "seriesID": "1.2.840.113619.2.334.3.313263218.304.1505378168.818.3",
          "state": "diagnosis",
          "url": "",
          "width": 20
        },
      ],
      "message": "OK",
      "statusCode": 1000
    }

    @apiErrorExample {json} Error-Response:
    {
      "statusCode": 1100
    }
    """
    res = response()
    userID = current_user.get_id()
    params = request.args.to_dict() if request.method == 'GET' else request.json
    if params is None:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    if not all(key in params for key in required_key_all):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
    taskID = params['taskID']
    task_info = db_find_one("task", {"taskID": taskID})
    if task_info is None:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)
    studyUID = task_info['studyUID']
    if userID != task_info['reporter'] and task_info['reporter'] != "imsightmed" and userID != task_info['reviewer']:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)
    isReporter = True if userID == task_info['reporter'] else False
    isReviewer = True if userID == task_info['reviewer'] else False
    if 'labelType' in params:
        label_info = db_find_one("label", {"taskID": taskID, 'labelType': params['labelType']})
    else:
        label_info = db_find_one("label", {"taskID": taskID})
    if label_info is None:
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)
    if 'labelType' in label_info:
        labelType = label_info['labelType']
    else:
        labelType = None
    rest_labelist = []
    if labelType in instance_label_type:
        if all(key in params for key in optional_key_instance_label_type):
            seriesID = params['seriesID']
            instanceID = params['instanceID']
            all_labelist = parseLabelInfo(label_info)
            labelist = filter(lambda l: l.seriesID == seriesID and l.instanceID == instanceID, all_labelist)
            rest_labelist = [l for l in all_labelist if l not in labelist]
            if "disease" in label_info:
                if not label_info["disease"]:
                    rest_labelist = []
        else:
            labelist = parseLabelInfo(label_info)
    elif labelType in series_label_type:
        if all(key in params for key in optional_key_series_label_type):
            seriesID = params['seriesID']
            all_labelist = parseLabelInfo(label_info)
            labelist = filter(lambda l: l.seriesID == seriesID, all_labelist)
            rest_labelist = [l for l in all_labelist if l not in labelist]
        else:
            labelist = parseLabelInfo(label_info)

    else:
        labelist = []
        res.data = labelist
        res.statusCode = 1000
        return make_response(jsonify(res.to_dict()), 200)

    if request.method == 'GET':
        res.statusCode = 1000
        hellolist = [label.hello() for label in labelist]
        if True in hellolist:
            rest_labelist.extend(labelist)
            db_update_one("label", {"taskID": taskID}, saveLabelist(rest_labelist, studyUID, taskID, labelType))
        res.data = [label.to_dict() for label in labelist]
        return make_response(jsonify(res.to_dict()), 200)

    if not all(key in params for key in required_key_put):
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    if request.method == 'PUT':
        params['labelType'] = labelType
        new_labelist = []
        for label in params['labels']:
            label['labelType'] = labelType
            isCreate = False if 'labelID' in label else True
            new_labelist.append(updateLabel(label, 'diagnosis', isCreate=isCreate))

        if len(new_labelist) < len(labelist):
            pass
        elif len(new_labelist) == len(labelist):
            if labelType == 'Nodule' or labelType == 'LiverTumor':
                report_info = db_find_one("report", {"taskID": taskID})
                report = parseReportInfo(report_info)
                report.screenshotURLs = [label.url for label in new_labelist if label.url != '']
                db_update_one("report", {"taskID": taskID}, report.to_db())
                # ret = report.to_dict()
                # ret['labelist'] = [label.to_dict() for label in new_labelist if label.url != '']
                socket_emit('updateReport', {'data': "done", 'userID': userID, 'taskID': taskID})
        else:
            if labelType == 'Nodule' and task_info['type'] == "normal":
                new_labels = [new_label for new_label in new_labelist if new_label.addByUser]
                for new_label in new_labels:
                    nodule_info = new_label.to_dict()
                    nodule_info['taskID'] = taskID
                    analyzed_label = run_lung_nodule_analyzer(nodule_info)
                    new_labelist.remove(new_label)
                    analyzed_label.seriesID = params['seriesID']
                    analyzed_label.addByUser = True
                    new_labelist.append(analyzed_label)

        rest_labelist.extend(new_labelist)
        new_label_info = saveLabelist(rest_labelist, studyUID, taskID, labelType)
        if labelType == "DX":
            for label in new_label_info["labels"]:
                if "disease" in label_info:
                    if label["diseaseTag"] not in label_info["disease"]:
                        label_info["disease"].append(label["diseaseTag"])
                    new_label_info["disease"] = label_info["disease"]
                else:
                    new_label_info["disease"] = [label["diseaseTag"]]
        db_update_one("label", {"taskID": taskID, "labelType": labelType}, new_label_info)

        studylist_info = db_find_one('studylist', {"studyUID": studyUID})
        studylist_info['result'] = "ABNORMAL" if len(rest_labelist) else "NORMAL"
        studylist_info['labelType'] = label_info['labelType']
        studylist_info['labelNum'] = len(rest_labelist)
        db_update_one("studylist", {"studyUID": studyUID}, studylist_info)

        create_radiomics(saveLabelist(new_labelist, studyUID, taskID, labelType)['labels'], studyUID, labelType, taskID)
        res.data = [label.to_dict() for label in new_labelist]
        res.statusCode = 1000
        return make_response(jsonify(res.to_dict()), 200)


def on_disease():
    """

    :return:
    """
    res = response()
    userID = current_user.get_id()
    if request.method == "GET":
        params = request.args.to_dict()
        if params is None:
            res.statusCode = 1100
            return make_response(jsonify(res.to_dict()), 200)
        if not all(key in params for key in disease_key_all):
            res.statusCode = 1100
            return make_response(jsonify(res.to_dict()), 200)
        taskID = params['taskID']
        task_info = db_find_one("task", {"taskID": taskID})
        if task_info is None:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        if userID != task_info['reporter'] and task_info['reporter'] != "imsightmed" and userID != task_info[
            'reviewer']:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        label_info = db_find_one("label", {"taskID": taskID, "labelType": "DX"})
        if label_info is None:
            res.statusCode = 1000
            res.data = {"disease": [], "disease_state": 0}
            return make_response(jsonify(res.to_dict()), 200)
        if "disease" in label_info:
            if "disease_state" in label_info:
                disease_state = label_info["disease_state"]
            else:
                disease_state = 1
            res.data = {"disease": label_info["disease"],
                        "disease_state": disease_state}
        else:
            res.data = {"disease": [], "disease_state": 0}
        return make_response(jsonify(res.to_dict()), 200)

    elif request.method == "PUT":
        params = request.json
        if params is None:
            res.statusCode = 1100
            return make_response(jsonify(res.to_dict()), 200)
        taskID = params['taskID']
        if not all(key in params for key in disease_key_put):
            res.statusCode = 1100
            return make_response(jsonify(res.to_dict()), 200)
        label_info = db_find_one("label", {"taskID": taskID})
        if not label_info:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)
        label_info["disease"] = params["disease"]
        db_update_one("label", {"taskID": taskID}, label_info)
        studylist_info = db_find_one('studylist', {"taskID": taskID})
        studylist_info['result'] = "ABNORMAL" if len(params["disease"]) > 0 else "NORMAL"
        studylist_info['labelNum'] = len(params["disease"])
        db_update_one("studylist", {"taskID": taskID}, studylist_info)
        res.statusCode = 1000
        res.data["disease"] = params["disease"]
        return make_response(jsonify(res.to_dict()), 200)
