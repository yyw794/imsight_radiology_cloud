import React, { Component } from 'react';
import { Spin, Icon, message, Progress } from 'antd';
import './index.less'
import './directoryUploadPolyfill'
import {uploadFileList} from './fileUpload'
import api from '../../api'
import {FormattedMessage, injectIntl} from 'react-intl'

const MAX_FILE_SIZE = 1000000000
const DICOM_FILE_REGX = new RegExp(/^.+\.(dcm|ima)$|^[^.]+$/)

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

function isChrome()
{
  let ua=navigator.userAgent.toLocaleLowerCase();
  if(ua.match(/chrome/)!=null)
  {
    return true
  }
  return false
}

function getUploadText()
{
  if(isChrome())
  {
    return <FormattedMessage id="Upload.uploadText"/>
  }
  else{
    return <FormattedMessage id="Upload.uploadText.notChrome"/>
  }
}

class Upload extends Component {
  constructor(props) {
    super(props)
    this.state = {
      uploading: false,
      processing: false,
      timeout:false,
      progress: 0,
      finishCount: 0,
      failCount: 0,
      taskCount: 0,
    }
    this.fileKeys = []
  }

  componentDidMount() {
    const dragHandler = (e) => {
      // e.stopPropagation();
      e.preventDefault();
      if (e.type === 'dragover' || e.type === 'dragenter') {
        e.target.classList.add('drag-over')
      } else {
        e.target.classList.remove('drag-over')
      }
    }
    
    if(isChrome())
    {
      this.dropElm.addEventListener('dragenter', dragHandler)
      this.dropElm.addEventListener('dragover', dragHandler)
      this.dropElm.addEventListener('dragleave', dragHandler)
      this.dropElm.addEventListener('drop', this.onDrop)
    }
    this.inputElm.setAttribute('webkitdirectory', 'true')
    this.inputElm.setAttribute('directory', 'true')
  }

  onProgress = (percent) => {
    if (percent === 100) {
      this.setState({
        processing: true,
        uploading: false,
        timeout:false
      })
    }
    this.setState({progress: percent})
  }

  onSuccess = (data) => {
    console.log(data);
    this.props.onSuccess(data)
    this.setState({
      processing: false
    })
  }

  onFail = (err) => {
    if(!!err.response)
    {
      this.props.onFail(err)
      this.setState({
        processing: false
      })
    }else{
      this.setState({
        timeout:true,
        processing: false
      })
    }
  }

  uploadFiles(fileList) {
    if (this.state.uploading || this.state.processing) {
      return
    }
    let validFiles = []
    for (let file of fileList) {
      if (file.size > MAX_FILE_SIZE) continue
      if (!DICOM_FILE_REGX.test(file.name.toLowerCase())) continue
      validFiles.push(file)
    }
    // console.log('valid files', validFiles);
    this.fileKeys = []
    if (validFiles.length) {
      uploadFileList(validFiles, this.onProgress, this.onSuccess, this.onFail)
    }
    this.setState({
      uploading: !!validFiles.length,
      taskCount: validFiles.length,
      finishCount: 0,
      progress: 0,
      failCount: 0
    })
  }

  onDrop = (e) => {
    e.preventDefault();

    var fileList = []
    var iterateFilesAndDirs = async function(filesAndDirs, path) {
      for (var i = 0; i < filesAndDirs.length; i++) {
        if (typeof filesAndDirs[i].getFilesAndDirectories === 'function') {
          var path = filesAndDirs[i].path;

          // this recursion enables deep traversal of directories
          let subFilesAndDirs = await filesAndDirs[i].getFilesAndDirectories()
          // iterate through files and directories in sub-directory
          await iterateFilesAndDirs(subFilesAndDirs, path);
        } else {
          fileList.push(filesAndDirs[i])
        }
      }
    };
    // begin by traversing the chosen files and directories
    if ('getFilesAndDirectories' in e.dataTransfer) {
      e.dataTransfer.getFilesAndDirectories().then((filesAndDirs) => {
        iterateFilesAndDirs(filesAndDirs, '/').then(() => this.uploadFiles(fileList))
      });
    }
  }

  onFileInputChange = (e) => {
    if (!e.target.files.length) return
    this.uploadFiles(e.target.files)
  }

  render() {
    return (
      <div className='upload'>
        <div
          className="drop-zone"
          ref={input => {this.dropElm = input}}
          onClick={() => this.inputElm.click()}>
          <p className="ant-upload-drag-icon">
            <Icon type="inbox" />
          </p>
          <p className="ant-upload-text">{getUploadText()}</p>
          <p className="ant-upload-hint"><FormattedMessage id="Upload.uploadHint"/></p>
        </div>
        <input
          className='hidden'
          ref={input => {this.inputElm = input}}
          onChange={this.onFileInputChange}
          type="file"
          webkitdirectory directory multiple/>
        {
          this.state.uploading ?
          <Progress percent={this.state.progress} status="active"
            format={(percent) => Math.round(percent) + '%'}/>
          :
          ''
        }
        {
          this.state.uploading ?
          <p className='upload-tips'><Spin indicator={antIcon} /><FormattedMessage id="Upload.uploading-pre"/>{this.state.taskCount}<FormattedMessage id="Upload.uploading-post"/></p>
          :
          ''
        }
        {
          this.state.processing?
          <p className='upload-tips'><Spin indicator={antIcon} /><FormattedMessage id="Upload.finished-processing"/></p>
          :
          ''
        }
        {
          this.state.timeout ?
          <p className='upload-tips'><FormattedMessage id="Upload.finished-timeout"/></p>
          :
          ''
        }
      </div>
    );
  }

}

export default Upload;
