import React from 'react'
import { BrowserRouter, Route, IndexRoute, Redirect, Switch } from 'react-router-dom'
import { Layout, LocaleProvider,message } from 'antd';
/**
* 布局结构Header为头部 Content 为中间部分 Footer为尾部部分
 */
const { Header, Content, Footer } = Layout;
import StudyListPage from './pages/StudyListPage'
import StatisticsPage from './pages/StatisticsPage'
import DRViewerPage from './pages/DRViewerPage'
import CTViewerPage from './pages/CTViewerPage'
import CTFollowUpPage from './pages/CTFollowUpPage'
import ReportPage from './pages/ReportPage'
import ReportPrintPage from  './pages/ReportPrintPage'
import ReportPageModel from './pages/ReportPrintModel'
import LoginPage from './pages/LoginPage'
import RegisterPage from './pages/RegisterPage'
import ResetPage from './pages/ResetPage'
import UploadPage from './pages/UploadPage'
import NotFoundPage from './pages/NotFoundPage'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import {isAuthenticated} from './auth'
import { instanceOf } from 'prop-types';
import { connect } from 'react-redux'
import { Provider } from 'react-redux'
import zhCN_antd from 'antd/lib/locale-provider/zh_CN';
import enUS_antd from 'antd/lib/locale-provider/en_US';
// import moment from 'moment';
// import 'moment/locale/zh-cn';
// import 'moment/locale/en-gb';

// moment.locale('zh-cn');
message.config({
    top: 100
});
import {addLocaleData, IntlProvider, FormattedMessage} from 'react-intl'
import zh from 'react-intl/locale-data/zh';
import en from 'react-intl/locale-data/en';
// import zhCN from '../locales/zh-cn.json'
import zhCN from '../locales/zh-cn.json'
import enUS from '../locales/en.json'
addLocaleData([...zh, ...en])

import './App.less'

/**
 * 路由判断是否授权登陆然后渲染
 * @param Component
 * @param rest
 * @returns {boolean} 是否授权
 * @constructor
 */
const PrivateRoute = ({component: Component, ...rest}) => (

  <Route
    {...rest}
    render={props => {

      return isAuthenticated() ? (
        <Component {...props} setAppState={(state) => rest.setAppState(state)}/>
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }}
  />
)
/**
 * 全局组件App
 * 类APP是通过class关键字申明，并且继承于类React组件
 */
class App extends React.Component {
    /**
     * react生命周期函数 这是ES6对类的默认方法，通过 new 命令生成对象实例时自动调用该方法。并且，该方法是类中必须有的，如果没有显示定义，则会默认添加空的
     */
  constructor(props) {
    super(props);
        /**
         *
         * @private {{currentLocale: 当前选择的语言环境}}
         */
    this.state = {
      currentLocale: props.lang
    }
  }
    /**
     * react生命周期函数 当props发生变化时执行，初始化render时不执行，在这个回调函数里面，你可以根据属性的变化，通过调用this.setState()来更新你的组件状态，旧的属性还是可以通过this.props来获取,这里调用更新状态是安全的，并不会触发额外的render调用
     */
  componentWillReceiveProps(nextProps) {
    this.setState({currentLocale: nextProps.lang})
  }

    /**
     *
     * @returns {boolean}
     */
  render() {
    const antd_locale = this.state.currentLocale === 'zh-cn' ? zhCN_antd : enUS_antd
    const locale = this.state.currentLocale === 'zh-cn' ? zhCN : enUS
    return (
      <IntlProvider messages={locale} locale={this.state.currentLocale}>
        <LocaleProvider locale={antd_locale}>
        <Layout className="layout" style={{ height: '100%' }}>
          <Switch>
            <PrivateRoute exact path="/" component={StudyListPage}/>
            <PrivateRoute exact path="/statistics" component={StatisticsPage}/>
            <Route path='/login' component={LoginPage}/>
            <Route path='/register' component={RegisterPage}/>
            <Route path='/reset' component={ResetPage}/>
            <PrivateRoute path='/upload' component={UploadPage}/>
            <PrivateRoute path='/study_list' component={StudyListPage}/>
            <PrivateRoute path='/report/:id' component={ReportPage}/>
            <PrivateRoute path='/report_print/:id' component={ReportPrintPage}/>
            <PrivateRoute path='/report_print_model/:id' component={ReportPageModel}/>
            <PrivateRoute path='/view_dr/:id/:labeler?' component={DRViewerPage}/>
            <PrivateRoute path='/view_ct/:id/:labeler?' component={CTViewerPage}/>
            <PrivateRoute path='/view_ct_follow_up/:patientID/:ids' component={CTFollowUpPage}/>
            <Route component={NotFoundPage}/>
          </Switch>
        </Layout>
        </LocaleProvider>
      </IntlProvider>
    )
  }
}

/**
 * state作为props绑定到组件上。因此，在组件中你就可以通过this.props.selectedSubreddit, this.props.posts 来访问这些数据了。
 * @param state
 * @param ownProps
 * @returns {*}
 */
const mapStateToProps = (state, ownProps) => {
  return state
}
App = connect(mapStateToProps)(App)

// DragDropContext is required for drag&drop
export default DragDropContext(HTML5Backend)(App)
// export default App;
