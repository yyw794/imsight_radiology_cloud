import React, { Component } from 'react'
import { Form, Icon, Input, Button, Checkbox, Layout, Select, message, Modal } from 'antd'
const FormItem = Form.Item
const { Header, Content, Footer } = Layout
const Option = Select.Option
import {authenticate} from '../../auth'
import {withRouter} from 'react-router'
import {Redirect, Link} from 'react-router-dom'
import api from '../../api'
import {GetLocaledText} from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'

import './index.less'

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validateAccount(account) {
  let re = /^[a-zA-Z0-9_-]+$/
  return re.test(account)
}
 function validatePhone(val) {
     var isPhone = /^([0-9]{3,4}-)?[0-9]{7,8}$/;//手机号码
     var isMob= /^0?1[3|4|5|8][0-9]\d{8}$/;// 座机格式
     return isMob.test(val)||isPhone.test(val)
 }

class RegisterPage extends Component {
  state = {
    redirectToReferrer: false,
    disable: false,
    password: ""
  }

  constructor(props) {
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit = (e) => {
      e.preventDefault()
      this.props.form.validateFields((err, values) => {
        if (!err) {
          delete values.passwordConfirm
          this.setState({disable: true})
          api.register(values)
            .then(() => {
              Modal.success({
                title: GetLocaledText(this,"RegisterPage.dialog.hint.registerSucceeded"),
                onOk: () => authenticate(values, () => this.props.history.push('/')),
                maskClosable: true,
                okText: GetLocaledText(this,"RegisterPage.dialog.headToIndex"),
              })
            })
            .catch(err => {
              if (err.payload && err.payload.statusCode === 1106) {
                message.error(GetLocaledText(this,"RegisterPage.dialog.userExisted"))
              } else if (err.payload && err.payload.statusCode === 1107) {
                message.error(GetLocaledText(this,"RegisterPage.dialog.emailUsed"))
              } else if (err.payload && err.payload.statusCode === 1111) {
                message.error(GetLocaledText(this,"RegisterPage.dialog.emailSentFailed"))
              } else {
                message.error(GetLocaledText(this,"RegisterPage.dialog.registerFailed"))
              }
              this.setState({disable: false})
            })
        }
      })
  }

  render() {
    // render register page
    const { getFieldDecorator } = this.props.form
    return (
      <div className='page register-page'>
        <div className="QRcode">
          <img src={require('../../../static/images/qrcode.png')}/>
          <span className="wechat"><FormattedMessage id="RegisterPage.qrcode"/></span>
        </div>
        <a className="website" href="http://www.imsightmed.com/">www.imsightmed.com</a>
        <Content className='content'>
          <Form onSubmit={this.handleSubmit} className="register-form">
            <p className='title'>视见科技影像辅助诊断平台</p>
            <p className='engtitle'>Imsight Radiology Platform<sup>TM</sup></p>
            <p className='version'>IRCP{localStorage.getItem("version")}</p>
            <FormItem className='register-form-item'>
              {getFieldDecorator('account', {
                rules: [
                  { required: true, message: GetLocaledText(this,"RegisterPage.form.input.username") },
                  {
                    validator: (rule, value, callback) => {
                      if (!value) {return callback()}
                      if (!validateAccount(value)) {return callback(GetLocaledText(this,"RegisterPage.form.validate.usernameRule"))}
                      api.checkAccount({account: value}).then(() => callback())
                      .catch(err => {
                        if (err.payload.statusCode === 1106) {
                          callback(GetLocaledText(this,"RegisterPage.form.validate.usernameUsed"))
                        } else {
                          callback()
                        }
                      })
                    }
                  }
                ],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={GetLocaledText(this,"RegisterPage.form.placeholder.username")} />
              )}
            </FormItem>
            <FormItem className='register-form-item'>
              {getFieldDecorator('password', {
                rules: [
                  { required: true, message: GetLocaledText(this,"RegisterPage.form.input.password") },
                  { min: 3, message: GetLocaledText(this,"RegisterPage.form.validate.passwordMin") }
                ],
              })(
                <Input
                  onCopy={() => false}
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder={GetLocaledText(this,"RegisterPage.form.placeholder.password")} />
              )}
            </FormItem>
            <FormItem className='register-form-item'>
              {getFieldDecorator('passwordConfirm', {
                rules: [
                  { required: true, message: GetLocaledText(this,"RegisterPage.form.input.passwordAgain") },
                  {
                    validator: (rule, value, callback) => {
                      const { getFieldValue } = this.props.form
                      if (value && value !== getFieldValue('password')) {
                          callback(GetLocaledText(this,"RegisterPage.form.validate.diffPassword"))
                      }

                      // Note: 必须总是返回一个 callback，否则 validateFieldsAndScroll 无法响应
                      callback()
                    }
                  }
                ],
              })(
                <Input
                  onCopy={() => false}
                  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder={GetLocaledText(this,"RegisterPage.form.placeholder.passwordAgain")} />
              )}
            </FormItem>
            <FormItem className='register-form-item'>
              {getFieldDecorator('nickname', {
                rules: [{ required: true, message: GetLocaledText(this,"RegisterPage.form.input.realName") }],
              })(
                <Input
                  prefix={<Icon type="idcard" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={GetLocaledText(this,"RegisterPage.form.placeholder.realName")} />
              )}
            </FormItem>
            <FormItem className='register-form-item'>
              {getFieldDecorator('email', {
                rules: [
                  { required: true, message: GetLocaledText(this,"RegisterPage.form.input.email")},
                  {
                    pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    message: GetLocaledText(this,"RegisterPage.form.validate.emailCheck")
                  },
                  {
                    validator: (rule, value, callback) => {
                      if (!value) {return callback()}
                      api.checkEmail({email: value}).then(() => callback())
                      .catch(err => {
                        if (err.payload.statusCode === 1107) {
                          callback(GetLocaledText(this,"RegisterPage.form.validate.emailUsed"))
                        } else {
                          callback()
                        }
                      })
                    }
                  }
                ],
              })(
                <Input
                  prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={GetLocaledText(this,"RegisterPage.form.placeholder.email")} />
              )}
            </FormItem>
            <FormItem className='register-form-item'>
              {getFieldDecorator('usergroup', {
                rules: [{ required: true, message: GetLocaledText(this,"RegisterPage.form.input.userType") }],
              })(
                <Select placeholder={GetLocaledText(this,"RegisterPage.form.placeholder.userType")} >
                  <Option value="reporter"><FormattedMessage id="RegisterPage.form.options.userType.reporter"/></Option>
                </Select>
              )}
            </FormItem>
            <FormItem className='register-form-item'>
              {getFieldDecorator('hospital', {
                rules: [{ required: true, message: GetLocaledText(this,"RegisterPage.form.input.hospital") }],
              })(
                <Select placeholder={GetLocaledText(this,"RegisterPage.form.placeholder.hospital")} >
                  <Option value="深圳市人民医院"><FormattedMessage id="RegisterPage.form.options.hospital.shenzhen"/></Option>
                  <Option value="309 HOSPITAL OF BEIJING"><FormattedMessage id="RegisterPage.form.options.hospital.beijing"/></Option>
                  <Option value="individual"><FormattedMessage id="RegisterPage.form.options.hospital.personal"/></Option>
                </Select>
              )}
            </FormItem>
            <FormItem className='register-form-item'>
              {getFieldDecorator('contact')(
                <Input
                  prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder={GetLocaledText(this,"RegisterPage.form.placeholder.telephone")} />
              )}
            </FormItem>
            <FormItem className='register-form-item'>
              <Button type="primary" htmlType="submit" className="register-form-button" disabled={this.state.disable}>
                <FormattedMessage id="RegisterPage.button.register"/>
              </Button>
              <div className='links'>
                <Link to='/login'><FormattedMessage id="RegisterPage.button.login"/></Link>
              </div>
            </FormItem>
          </Form>
        </Content>
      </div>
    )
  }

}

RegisterPage = withRouter(RegisterPage)
RegisterPage = Form.create()(RegisterPage)

export default injectIntl(RegisterPage)
