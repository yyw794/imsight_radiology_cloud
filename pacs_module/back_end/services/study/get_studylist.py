#!/bin/python
#coding=utf-8
from flask import make_response, request, jsonify, abort
from flask_login import current_user
import sys, os, logging

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_find, db_page_query
from models.response.response import response
from models.task.Task import Task, parseTaskInfo
from models.study.Study import Study, parseStudyInfo
from models.report.Report import Report, parseReportInfo
from config import log_file_handler
from services.pacs.pacs_api import unsend_studylist_from_pacs
optional_key = ['needData']

logger = logging.getLogger('back_end.get_studylist')
logger.addHandler(log_file_handler)

def get_studylist():
    params = request.args.to_dict()
    res = response()

    if not current_user.is_authenticated:
        return abort(401)

    userID = current_user.get_id()

    ownership_info = db_find_one("ownership", {"userID": userID})
    if ownership_info is None:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)
    assets = ownership_info['shared']

    query_value = {}
    if 'patientID' in params and params['patientID']:
        query_value['petientID'] = params['patientID']
    if 'petietName' in params and params['patientName']:
        query_value['patientName'] = params['patientName']
    if 'gender' in params and params['gender']:
        query_value['gender'] = params['gender']
    if 'studyDate' in params and params['studyDate']:
        query_value['studyDate'] = params['studyDate']
    if 'birthDate' in params and params['birthDate']:
        query_value['birthDate'] = params['birthDate']

    if 'page_size' in params and 'page_no' in params:
        page_size = params['page_size']
        page_no = params['page_no']
        study_infolist = db_page_query('study', query_value, page_size, page_no)
    else:
        study_infolist = db_find('study', query_value)

    studylist = []
    for study_info in study_infolist:
        new_study = parseStudyInfo(study_info)
        if new_study:
            studylist.append(new_study)
    unsend_data_list = unsend_studylist_from_pacs(params,studylist)
    studylist.extend(unsend_data_list)

    study_infolist = []
    for study in studylist:
        withSeries = params['needData'] == 'True' if 'needData' in params else False
        new_dict = study.to_dict(withSeries=withSeries)
        taskID = ''
        if new_dict['studyUID'].replace('.', '-') in assets:
            taskID = assets[new_dict['studyUID'].replace('.', '-')]
        new_dict['taskID'] = taskID
        new_dict['user_privilege'] = []
        new_dict["diagnosisAdvice"] = ""
        new_dict['submitted'] = False
        new_dict['labelNum'] = 0
        new_dict['result'] = ""

        new_dict['labelType'] = ''
        if taskID != "":
            label_info = db_find_one("label", {"taskID": taskID})
            if label_info is None or new_dict['state'] != 'completed':
                new_dict['result'] = ""
                new_dict['labelNum'] = 0
            else:
                new_dict['result'] = "ABNORMAL" if len(label_info['labels']) else "NORMAL"
                new_dict['labelType'] = label_info['labelType']
                new_dict['labelNum'] = len(label_info['labels'])
                
            report_info = db_find_one("report", {"taskID": taskID})
            new_report = parseReportInfo(report_info)
            new_dict["diagnosisAdvice"] = new_report.diagnosisAdvice
            new_dict['submitted'] = new_report.submitted()
            task_info = db_find_one('task', {"taskID": taskID})
            if task_info is None:
                res.statusCode = 1200
                return make_response(jsonify(res.to_dict()), 200)
            task = parseTaskInfo(task_info)
            new_dict['user_privilege'] = task.get_user_privilege(userID)

        study_infolist.append(new_dict)

    study_infolist = sorted(study_infolist, key=lambda s: s['studyDate'], reverse=True)
    if 'page_no' in params:
        study_infolist['page_no'] = params['page_no']

    res.statusCode = 1000
    res.data = study_infolist
    return make_response(jsonify(res.to_dict()), 200)
