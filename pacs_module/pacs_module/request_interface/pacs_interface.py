# -*- coding: utf-8 -*-

import sys
import os
from pynetdicom3 import (
            AE,QueryRetrievePresentationContexts,
            StoragePresentationContexts,
            PYNETDICOM_IMPLEMENTATION_UID,
            PYNETDICOM_IMPLEMENTATION_VERSION,
            VerificationPresentationContexts)
from pynetdicom3.sop_class import VerificationSOPClass
from pydicom.uid import (
    ExplicitVRLittleEndian, ImplicitVRLittleEndian, ExplicitVRBigEndian,DeflatedExplicitVRLittleEndian
)
from pydicom.tag import Tag
from pydicom.dataelem import DataElement
from pydicom import dcmread
from pydicom.dataset import Dataset,FileDataset
from pynetdicom3.pdu_primitives import SCP_SCU_RoleSelectionNegotiation
import initconfig as config
from mongodb_interface import *
import time
import logger
import logging
import thread
from http_client import upload_image_path
from initconfig import get_if_push_model
from ConcurrentQueue import ConcurrentQueue
import datetime
import threading
sys.path.append("..")

log = logging.getLogger("PACSModule.pacs_interface")

study_inatance_uid_path = ConcurrentQueue(3000)
g_send_thread_count = 0
_G_MAX_THREAD_COUNT_ = 5
_cond = threading.Condition()


# 最大几个线程给flask传输数据
def thread_upload_image_path_temp(study_path):
    global g_send_thread_count
    log.info("*************** " + study_path + " begin remain size is "+str(study_inatance_uid_path.size())+" thread number is "+str(g_send_thread_count))
    starttime = datetime.datetime.now()
    upload_image_path(study_path)
    endtime = datetime.datetime.now()
    log.info("*************** " + study_path + " depends on " + str((endtime - starttime).seconds))
    if _cond.acquire():
        g_send_thread_count = g_send_thread_count - 1
        _cond.notify()
        _cond.release()


def do_upload_image_path(send_type): # single queue send to flask
    global g_send_thread_count
    while True:
        study_path = study_inatance_uid_path.get()   # 等待需要处理的数据
        logger.info("do_upload_image_path: " + send_type + " get a study_path " + study_path)
        
        if _cond.acquire():
            while g_send_thread_count >= _G_MAX_THREAD_COUNT_: # 判断是否已经超过了最大的线程数量
                _cond.wait()
            g_send_thread_count += 1
            thread.start_new_thread(thread_upload_image_path_temp, (study_path,))
            _cond.notify()
            _cond.release()


def thread_upload_image_path(studyinstanceuid):
    # log.info("thread_upload_image_path"+str(studyinstanceuid))
    # upload_image_path(studyinstanceuid)
    # log.info("thread_upload_image_path end"+str(studyinstanceuid))
    study_inatance_uid_path.put(studyinstanceuid)


def after_receive_complete_push_(study_instance_uid, study_date):
    study_instance_uid_path = config.get_store_path_url()+"/"+study_date + "/" + study_instance_uid
    log.info('after_receive_complete_push_: begin:' + study_instance_uid_path)
    
    study_instance_info = get_study_instance_list({'_id':study_instance_uid})
    
    if os.path.exists(study_instance_uid_path) and study_instance_info['assoc_ac'] != "":
        request_state = study_instance_info['assoc_ac_state']
        if request_state != Abort:
            study_instance_info['study_state'] = ReceiveComplate
            study_instance_list_insert_update({'_id':study_instance_uid},study_instance_info)
            log.info("after_receive_complete_push_ upload_image_path"+str(request_state)+",number is [[[" + str(len(os.listdir(study_instance_uid_path)))+"]]]")
            thread.start_new_thread(thread_upload_image_path, (study_instance_uid_path,))
        else:
            log.error("after_receive_complete_push_ receive data error, remove the folder"+str(request_state))
            shutil.rmtree(study_instance_uid_path)
            # un_request_queue.put(study_instance_uid_path)
            
    else:  # not ct or dx
        log.warn(study_instance_uid + " may be not support study type")
    log.info('after_receive_complete_push_: end:' + study_instance_uid)


def send_echo_request():
    # Propose extra transfer syntaxes
    transfer_syntax = [ImplicitVRLittleEndian,ExplicitVRLittleEndian,ExplicitVRBigEndian]
    
    # Create local AE
    # Binding to port 0, OS will pick an available port
    ae = AE(config.get_AppTitle())
    
    ae.add_requested_context(VerificationSOPClass, transfer_syntax)
    # Set timeouts
    ae.network_timeout = None
    ae.acse_timeout = 60
    ae.dimse_timeout = None
    
    # Request association with remote AE
    assoc = ae.associate(config.get_PeerHost(),
                         config.get_PeerPort(),
                         ae_title=config.get_PeerAppTitle(),
                         max_pdu=16384)

    status = assoc.send_c_echo()
    result = (status.Status == 0)
    if result:
        assoc.release()
    else:
        assoc.abort()
    return result


# Parameters: {study_date:20180113,patient_id:011235,patient_name:zhang san,patient_sex:f/m,patient_birth_date:19920506}
# return: [{study_instance_uid:12334.45446.645,study_date:20180113,patient_id:011235,
# patient_name: zhang san,patient_sex:f/m,patient_birth_date:19920506}]
def send_find_request(condition):
    # Create application entity
    # Binding to port 0 lets the OS pick an available port
    ae = AE(config.get_AppTitle())
    ae.requested_contexts = QueryRetrievePresentationContexts
    
    # Request association with remote
    assoc = ae.associate(config.get_PeerHost(), config.get_PeerPort(), ae_title=config.get_PeerAppTitle())
    
    conditionList = []
    if False is assoc.is_established:
        log.error("can not connect pacs server")
        return conditionList
    
    # Create identifier data set
    identifier = Dataset()
    identifier.StudyInstanceUID = ''
    identifier.QueryRetrieveLevel = "STUDY"
    if 'study_date' in condition and condition['study_date'] is not None:
        identifier.StudyDate = condition['study_date']
    else:
        identifier.StudyDate= ''
        
    if 'patient_id' in condition and condition['patient_id'] is not None:
        identifier.PatientID = condition['patient_id']
    else:
        identifier.PatientID = ''
        
    if 'patient_name' in condition and condition['patient_name'] is not None:
        identifier.PatientName = (condition['patient_name']).encode("gbk")
    else:
        identifier.PatientName = ''
        
    if 'patient_sex' in condition and condition['patient_sex'] is not None:
        identifier.PatientSex = condition['patient_sex']
    else:
        identifier.PatientSex = ''
        
    if 'patient_birth_date' in condition and condition['patient_birth_date'] is not None:
        identifier.PatientBirthDate = condition['patient_birth_date']
    else:
        identifier.PatientBirthDate = ''
            
    # Query/Retrieve Information Models
    query_model = 'S'
    
    # Send query, yields (status, identifier)
    # If `status` is one of the 'Pending' statuses then `identifier` is the
    #   C-FIND response's Identifier dataset, otherwise `identifier` is None
    # identifier.AccessionNumber = ''
    # identifier.SeriesDescription = ''
    # identifier.ViewPosition = ''
    # identifier.PatientOrientation = ''
    # identifier.ProtocolName = ''
    # identifier.StudyDescription = ''
    # identifier.BodyPartExamined = ''
    identifier.Modality = ''
    identifier.StudyTime = ''
    response = assoc.send_c_find(identifier, query_model=query_model)

    for status, identifier in response:
        if identifier is not None:
            conditon_temp = {}
            conditon_temp['study_instance_uid'] = identifier.StudyInstanceUID
            conditon_temp['study_date'] = identifier.StudyDate
            conditon_temp['patient_id'] = identifier.PatientID
            conditon_temp['patient_name'] = identifier.PatientName
            conditon_temp['patient_sex'] = identifier.PatientSex
            
            if identifier.get(Tag(0x0010, 0x0030)):
                conditon_temp['patient_birth_date'] = identifier[0x0010, 0x0030].value
            else:
                conditon_temp['patient_birth_date'] = ''
            
            if identifier.get(Tag(0x0008, 0x0030)):
                conditon_temp['study_time'] = identifier[0x0008, 0x0030].value
            else:
                conditon_temp['study_time'] = ''
                
            if identifier.get(Tag(0x0008, 0x0060)):
                conditon_temp['modality'] = identifier[0x0008, 0x0060].value
            elif identifier.get(Tag(0x0008, 0x0061)):
                conditon_temp['modality'] = identifier[0x0008, 0x0061].value
            else:
                conditon_temp['modality'] = ''
            conditionList.append(conditon_temp)
    assoc.release()
    return conditionList


def send_move_request(study_instance_uid):    
    # Create application entity
    # Binding to port 0 lets the OS pick an available port
    ae = AE(config.get_AppTitle())
    ae.requested_contexts = QueryRetrievePresentationContexts

    # Request association with remote AE
    assoc = ae.associate(config.get_PeerHost(), config.get_PeerPort(), ae_title=config.get_PeerAppTitle())
    
    if assoc.is_established:
        ds = Dataset()
        ds.StudyInstanceUID = study_instance_uid
        ds.QueryRetrieveLevel = "STUDY"
        query_model = 'S'
        response = assoc.send_c_move(ds, config.get_MoveAppTitle(), query_model=query_model)
        for (status, identifier) in response:
            pass
        assoc.release()


def send_get_request(study_instance_uid):
    # Create application entity
    # Binding to port 0 lets the OS pick an available port
    ae = AE(config.get_AppTitle())
    
    for context in QueryRetrievePresentationContexts:
        ae.add_requested_context(context.abstract_syntax)
    for context in StoragePresentationContexts:
        ae.add_requested_context(context.abstract_syntax)

    # Add SCP/SCU Role Selection Negotiation to the extended negotiation
    # We want to act as a Storage SCP
    ext_neg = []
    for context in StoragePresentationContexts:
        role = SCP_SCU_RoleSelectionNegotiation()
        role.sop_class_uid = context.abstract_syntax
        role.scp_role = True
        role.scu_role = False
        ext_neg.append(role)

    # Create query data set
    d = Dataset()
    d.StudyInstanceUID = study_instance_uid
    d.QueryRetrieveLevel = "STUDY"
    
    query_model = 'S'
    
    ae.on_c_store = on_c_store
    # Request association with remote
    assoc = ae.associate(config.get_PeerHost(), config.get_PeerPort(), ae_title=config.get_PeerAppTitle(),ext_neg=ext_neg)
    
    # Send query
    if assoc.is_established:
        response = assoc.send_c_get(d, query_model=query_model)

    for status, identifier in response:
        #print(status)
        pass
    assoc.release()


def on_c_store(dataset, context, info, assoc_ac, first_come_in):
    """
    Function replacing ApplicationEntity.on_store(). Called when a dataset is
    received following a C-STORE. Write the received dataset to file

    Parameters
    ----------
    dataset : pydicom.Dataset
        The DICOM dataset sent via the C-STORE
    context : pynetdicom3.presentation.PresentationContextTuple
        Details of the presentation context the dataset was sent under.
    info : dict
        A dict containing information about the association and DIMSE message.

    Returns
    -------
    status : pynetdicom.sop_class.Status or int
        A valid return status code, see PS3.4 Annex B.2.3 or the
        StorageServiceClass implementation for the available statuses
    """
    mode_prefix = 'UN'
    mode_prefixes = {'CT Image Storage': 'CT',
                     'Enhanced CT Image Storage': 'CTE',
                     'MR Image Storage': 'MR',
                     'Enhanced MR Image Storage': 'MRE',
                     'Positron Emission Tomography Image Storage': 'PT',
                     'Enhanced PET Image Storage': 'PTE',
                     'RT Image Storage': 'RI',
                     'RT Dose Storage': 'RD',
                     'RT Plan Storage': 'RP',
                     'RT Structure Set Storage': 'RS',
                     'Computed Radiography Image Storage': 'CR',
                     'Ultrasound Image Storage': 'US',
                     'Enhanced Ultrasound Image Storage': 'USE',
                     'X-Ray Angiographic Image Storage': 'XA',
                     'Enhanced XA Image Storage': 'XAE',
                     'Nuclear Medicine Image Storage': 'NM',
                     'Secondary Capture Image Storage': 'SC',
                     'Digital X-Ray Image Storage - For Presentation': 'DX'}

    try:
        mode_prefix = mode_prefixes[dataset.SOPClassUID.name]
    except KeyError:
        log.error('can not find ' + dataset.SOPClassUID.name)
        mode_prefix = 'UN'

    filename = '{0!s}.{1!s}'.format(mode_prefix, dataset.SOPInstanceUID)
    
    floder_path = config.get_store_path_url()+"/"+dataset.StudyDate+"/"+dataset.StudyInstanceUID
    if False is os.path.exists(floder_path):
        os.makedirs(floder_path)
    filename = floder_path + "/" + filename+".dcm"
    if first_come_in:
        if os.path.exists(filename):
            log.info('DICOM file already exists, overwriting')
            
        if get_if_push_model() is True:
            si = Study_Instance_List()
            si._id = dataset.StudyInstanceUID
            si.assoc_ac = str(assoc_ac)
            si.assoc_ac_state = Receiving
            si.study_date = dataset.StudyDate
            si.patientid = dataset.PatientID
            si.patientname = dataset.PatientName.decode("GBK")
            si.patientsex = dataset.PatientSex
            si.patientbirthdate = dataset.PatientBirthDate
            si.study_state = UnKonwType
            si.is_high_priority = False
            log.info(si.to_dict())
            study_instance_list_insert_update({'_id': dataset.StudyInstanceUID}, si.to_dict())
        else:
            study_instance_info = get_study_instance_list({'_id': dataset.StudyInstanceUID})
            study_instance_info['assoc_ac'] = str(assoc_ac)
            study_instance_info['assoc_ac_state'] = Receiving
            study_instance_info['study_state'] = UnKonwType  # pull data again
            study_instance_list_insert_update({'_id': dataset.StudyInstanceUID}, study_instance_info)
        log.info(str(assoc_ac) + " begin receive...")

    # DICOM File Format - File Meta Information Header
    # If a DICOM dataset is to be stored in the DICOM File Format then the
    # File Meta Information Header is required. At a minimum it requires:
    #   * (0002,0000) FileMetaInformationGroupLength, UL, 4
    #   * (0002,0001) FileMetaInformationVersion, OB, 2
    #   * (0002,0002) MediaStorageSOPClassUID, UI, N
    #   * (0002,0003) MediaStorageSOPInstanceUID, UI, N
    #   * (0002,0010) TransferSyntaxUID, UI, N
    #   * (0002,0012) ImplementationClassUID, UI, N
    # (from the DICOM Standard, Part 10, Section 7.1)
    # Of these, we should update the following as pydicom will take care of
    #   the remainder
    meta = Dataset()
    meta.MediaStorageSOPClassUID = dataset.SOPClassUID
    meta.MediaStorageSOPInstanceUID = dataset.SOPInstanceUID
    meta.ImplementationClassUID = PYNETDICOM_IMPLEMENTATION_UID
    meta.TransferSyntaxUID = context.transfer_syntax
    
    # The following is not mandatory, set for convenience
    meta.ImplementationVersionName = PYNETDICOM_IMPLEMENTATION_VERSION

    ds = FileDataset(filename, {}, file_meta=meta, preamble=b"\0" * 128)
    ds.update(dataset)
    ds.is_little_endian = context.transfer_syntax.is_little_endian
    ds.is_implicit_VR = context.transfer_syntax.is_implicit_VR

    status_ds = Dataset()
    status_ds.Status = 0x0000

    # Try to save to output-directory
    # if args.output_directory is not None:
        # filename=os.path.join(args.output_directory, filename)

    try:
        # We use `write_like_original=False` to ensure that a compliant
        #   File Meta Information Header is written
        ds.save_as(filename, write_like_original=False)
        status_ds.Status = 0x0000  # Success

    except IOError:
            # print('Could not write file to specified directory:')
            # print("    {0!s}".format(os.path.dirname(filename)))
            log.info('Directory may not exist or you may not have write ''permission '+filename)
            # Failed - Out of Resources - IOError
            status_ds.Status = 0xA700
    except Exception as E:
        log.info('Could not write file to specified directory:')
        log.info(E)
        # print("    {0!s}".format(os.path.dirname(filename)))
        # Failed - Out of Resources - Miscellaneous error
        status_ds.Status = 0xA701
    return status_ds


def on_c_echo(context, info):
    """Optional implementation of the AE.on_c_echo callback."""
    # Return a Success response to the peer
    # We could also return a pydicom Dataset with a (0000, 0900) Status
    #   element
    return 0x0000


def on_listen_requested(arg):
    log.info("on_listen_requested " + str(arg))


def on_listen_accepted(arg):
    log.info("on_listen_accepted " + str(arg))


def on_listen_rejected(arg):
    log.info("on_listen_rejected " + str(arg))


def on_listen_released(arg):
    log.info("on_listen_released " + str(arg))

    study_instance_info = get_study_instance_list({'assoc_ac':str(arg),'study_state':UnKonwType})
    if study_instance_info is None:
        log.info("can not support modality")
        return
    study_instance_info['assoc_ac_state'] = Released
    study_instance_list_insert_update({'_id':study_instance_info['_id']},study_instance_info)

    # if push model
    if get_if_push_model() is True:
        after_receive_complete_push_(study_instance_info['_id'],study_instance_info['study_date'])


def on_listen_abort(arg):
    log.info("on_listen_abort " + str(arg))
    study_instance_info = get_study_instance_list({'assoc_ac':str(arg),'study_state':UnKonwType})
    if study_instance_info is None:
        log.info("can not support modality")
        return
    study_instance_info['assoc_ac_state'] = Abort
    study_instance_list_insert_update({'_id':study_instance_info['_id']},study_instance_info)


def store_listen(ae_title,listen_port,ip_address):

    if get_if_push_model() is True:
        thread.start_new_thread(do_upload_image_path, ("doctor_push",))

    # Set Transfer Syntax options
    # transfer_syntax = [ImplicitVRLittleEndian,
                   # ExplicitVRLittleEndian,
                   # DeflatedExplicitVRLittleEndian,
                   # ExplicitVRBigEndian]

    transfer_syntax = ["1.2.840.10008.1.2.4.91", "1.2.840.10008.1.2.4.90","1.2.840.10008.1.2.4.51",
                    "1.2.840.10008.1.2.4.50", "1.2.840.10008.1.2.4.70","1.2.840.10008.1.2.4.81",
                    "1.2.840.10008.1.2.4.80", "1.2.840.10008.1.2.5","1.2.840.10008.1.2.4.100",
                    "1.2.840.10008.1.2.4.101", "1.2.840.10008.1.2.4.102","1.2.840.10008.1.2.4.103",
                    "1.2.840.10008.1.2.4.104", "1.2.840.10008.1.2.4.105","1.2.840.10008.1.2.4.106",
                    "1.2.840.10008.1.2.4.107", "1.2.840.10008.1.2.4.108","1.2.840.10008.1.2.1.99",
                    "1.2.840.10008.1.2.1", "1.2.840.10008.1.2.2","1.2.840.10008.1.2"]

    # print(transfer_syntax)
    # Create application entity
    ae = AE(ae_title=ae_title, port=listen_port)
    ae.bind_addr = ip_address
    support_type = config.get_support_type()
    if 'CT' in support_type:
        ae.add_supported_context("1.2.840.10008.1.1", transfer_syntax)
        ae.add_supported_context("1.2.840.10008.5.1.4.1.1.2", transfer_syntax)
        ae.add_supported_context("1.2.840.10008.5.1.4.1.1.2.1", transfer_syntax)
        ae.add_supported_context("1.2.840.10008.5.1.4.1.1.7", transfer_syntax)
        ae.add_supported_context("1.2.840.10008.5.1.4.1.1.88.22", transfer_syntax)
        
    if 'DX' in support_type:
        ae.add_supported_context("1.2.840.10008.5.1.4.1.1.1.1", transfer_syntax)
        ae.add_supported_context("1.2.840.10008.5.1.4.1.1.1", transfer_syntax)
    """    
    # Add presentation contexts with specified transfer syntaxes
    for context in StoragePresentationContexts:
        ae.add_supported_context(context.abstract_syntax, transfer_syntax)
    for context in VerificationPresentationContexts:
        ae.add_supported_context(context.abstract_syntax, transfer_syntax)
    """
    
    ae.maximum_pdu_size = 16384

    # Set timeouts
    ae.network_timeout = None
    ae.acse_timeout = 60
    ae.dimse_timeout = None
    # ae.on_association_aborted = on_c_store_abort

    ae.on_association_requested = on_listen_requested
    ae.on_association_accepted = on_listen_accepted
    ae.on_association_rejected = on_listen_rejected
    ae.on_association_released = on_listen_released
    ae.on_association_aborted = on_listen_abort
    
    ae.on_c_echo = on_c_echo
    
    ae.on_c_store = on_c_store
    log.info("wait listen client with AE:"+ae_title+" port:"+str(listen_port)+" ip:"+ip_address)
    try:
        ae.start()
    except:
        log.error("start listen-process fail")
        # print("\033[1;31;40mstart listen-process fail\033[0m")


def _send_find_request_(condition):
    # Create application entity
    # Binding to port 0 lets the OS pick an available port
    ae = AE(config.get_AppTitle())
    ae.requested_contexts = QueryRetrievePresentationContexts
    
    # Request association with remote
    assoc = ae.associate(config.get_PeerHost(), config.get_PeerPort(), ae_title=config.get_PeerAppTitle())
    
    conditionList = []
    if assoc.is_established is False:
        log.error("can not connect pacs server")
        return conditionList
    
    # Create identifier data set
    identifier = Dataset()
    identifier.StudyInstanceUID = ''
    identifier.QueryRetrieveLevel = "STUDY"
    if 'study_date' in condition and condition['study_date'] is not None:
        identifier.StudyDate = condition['study_date']
    else:
        identifier.StudyDate = ''
        
    if 'patient_id' in condition and condition['patient_id'] is not None:
        identifier.PatientID = condition['patient_id']
    else:
        identifier.PatientID = ''
        
    if 'patient_name' in condition and condition['patient_name'] is not None:
        identifier.PatientName = condition['patient_name']
    else:
        identifier.PatientName = ''
        
    if 'patient_sex' in condition and condition['patient_sex'] is not None:
        identifier.PatientSex = condition['patient_sex']
    else:
        identifier.PatientSex = ''
        
    if 'patient_birth_date' in condition and condition['patient_birth_date'] is not None:
        identifier.PatientBirthDate = condition['patient_birth_date']
    else:
        identifier.PatientBirthDate = ''
            
    # Query/Retrieve Information Models
    query_model = 'S'
    
    # Send query, yields (status, identifier)
    # If `status` is one of the 'Pending' statuses then `identifier` is the
    #   C-FIND response's Identifier dataset, otherwise `identifier` is None
    identifier.AccessionNumber = ''
    identifier.SeriesDescription = ''
    identifier.ViewPosition = ''
    identifier.PatientOrientation = ''
    identifier.ProtocolName = ''
    identifier.StudyDescription = ''
    identifier.BodyPartExamined = ''
    identifier.Modality = ''
    identifier.StudyTime = ''
    response = assoc.send_c_find(identifier, query_model=query_model)

    for status, identifier in response:
        log.info("*************************** test: pacs list tag **************************")
        log.info(identifier)
        log.info("**************************************************************************")
        return []
        if identifier is not None:
            conditon_temp = {}
            conditon_temp['study_instance_uid'] = identifier.StudyInstanceUID
            conditon_temp['study_date'] = identifier.StudyDate
            conditon_temp['patient_id'] = identifier.PatientID
            conditon_temp['patient_name'] = identifier.PatientName
            conditon_temp['patient_sex'] = identifier.PatientSex
            conditon_temp['patient_birth_date'] = identifier.PatientBirthDate
            conditon_temp['study_time'] = identifier.StudyTime
            conditon_temp['modality'] = identifier.Modality
            conditionList.append(conditon_temp)
    assoc.release()
    return conditionList
