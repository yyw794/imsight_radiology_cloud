import React, { Component } from 'react'
import LiverViewer from '../../component/LiverViewer'
import api from '../../api'
import LoadingWrapper from '../../component/LoadingWrapper'
import axios from 'axios'
import { Card, Layout } from 'antd'
import {addLiverCTMetaData} from '../../metaData.js'
import {getUser} from '../../auth'
import _ from 'underscore'

import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './index.less'

const { Header, Content, Footer } = Layout;

class LiverViewerPage extends Component {
  constructor(props) {
    super(props)
    
  }
  componentDidMount() {
    this.setState({});
  }
  componentWillUnmount() {
    this.setState({});
  }
  render() {
    let {age, gender, patientName, studyDate, studyTime} = this.props.data
    let user = getUser()
    let userID = ''
    if (user) {
      userID = user.account
    }
    return (
      <div style={{height: '100%', backgroundColor: '#000'}} id='liver-viewer-page' >
        <LiverViewer
          instances={this.props.data.instances}
          series={this.props.data.series}
          labels={this.props.data.labels}
          tools={this.props.data.tools}
          report={this.props.data.report}
          studyUID={this.props.match.params.id.replace(/-/g, '.')}
          userID={userID}
          data={this.props.data}
          style={{height: '100%', position: 'relative'}}
          stacks={this.props.data.stacks}
          patientInfoBox={
            <div className='patient-info' style={{pointerEvents: 'none'}}>
              <p>{GetLocaledText(this, "LiverViewerPage.patient-name")}：{patientName}</p>
              <p>{GetLocaledText(this, "LiverViewerPage.gender")}：{gender}</p>
              <p>{GetLocaledText(this, "LiverViewerPage.age")}：{age}</p>
              <p>{GetLocaledText(this, "LiverViewerPage.study-date")}：{studyDate}</p>
              <p>{GetLocaledText(this, "LiverViewerPage.study-time")}：{studyTime}</p>
            </div>
          }/>
      </div>
    )
  }

}

function removeDuplicates(originalArray, prop) {
  var newArray = [];
  var lookupObject  = {};

  for(var i in originalArray) {
     lookupObject[originalArray[i][prop]] = originalArray[i];
  }

  for(i in lookupObject) {
      newArray.push(lookupObject[i]);
  }
   return newArray;
}

async function getData(props) {
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let resps = await Promise.all([
    api.getStudyDetail({}, {studyUID}),
    api.getLabelsLiver({}, {studyUID}),
    api.getToolsLiverData({}, {studyUID}),
    api.getReportLiver({}, {studyUID}),
    api.getStudySeries({}, {studyUID}),
  ])

  let studyData = resps[0]

  studyData.labels = resps[1]
  studyData.tools = resps[2]
  studyData.report = resps[3]
  studyData.series = resps[4]

  studyData.series = removeDuplicates(studyData.series, 'seriesNumber')

  addLiverCTMetaData(studyData)

  let stacks = studyData.series.map(series => {
    const serNum = series.seriesNumber;
    let objToRet = {}
    objToRet[serNum] = {
      currentImageIdIndex: 0,
      imageIds: Object.keys(series.instances.z).map((key, ind) => series.instances.z[key].url),
      seriesNumber: serNum
    }
    return objToRet[serNum]
  })

  studyData.stacks = stacks

  return await studyData
}

LiverViewerPage = LoadingWrapper(LiverViewerPage, getData)

export default injectIntl(LiverViewerPage)
