import React, { Component } from 'react'
import {Prompt} from 'react-router-dom'
import { Modal, Button, Row, Col, Input, Icon, TreeSelect, Layout, Menu, Alert, message } from 'antd'
import { List } from 'antd'
const { TextArea, Search } = Input
const { Header, Content, Footer } = Layout
const TreeNode = TreeSelect.TreeNode
import Template from './Template'
import LoadingWrapper from '../../component/LoadingWrapper'
import SignoutButton from '../../component/SignoutButton'
import Logo from '../../component/Logo'
import ReportLungPage from '../../component/ReportLungPage'
import api from '../../api'
import _ from 'underscore'
import Immutable from 'immutable'
import {getUser} from '../../auth'
import moment from 'moment'
import io from 'socket.io-client'
import {CSVLink, CSVDownload} from 'react-csv'
import templateList from './data'

import LanguageDropdownList from '../../component/LanguageDropdownList'
import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {EMPTYSTUDYCT,EMPTYNODULELABELS,EMPTYRADIOMICS,EMPTYREPORT,EMPTYTEMPLATE} from '../../component/DefaultData'

import './index.less'
import Return from "../../component/ReturnList";
import {URL_PREFIX} from "../../component/tools/config";

class ReportPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
        visible:false,
        headers:this.props.data.radiomicsJS[0].headers,
      report: Immutable.fromJS(Object.assign({
        patientName: '',
        gender: '',
        age: '',
        patientID: '',
        modality: '',
        studyDate: '',
        imagingFind: '',
        diagnosisAdvice: '',
        updateDate: '',
        screenshotURLs: []
      }, this.props.data.report)), // immutable
      labels: this.props.data.labels,
      radiomics: this.props.data.radiomics,
      templateList: this.props.data.template,
      zReverse:this.props.data.study.reverse,
      maxZ:!!this.props.data.study.series[Object.keys(this.props.data.study.series)[0]].instances['z'] ?
            this.props.data.study.series[Object.keys(this.props.data.study.series)[0]].instances['z'].length
            :
            1,
      template: {},
      modalVisible: false,
      newTemplateName: '', // save new template as this name
      unsave: false      
    }
    this.allTemplate = this.props.data.template // store original template list
    this.currentUser = getUser()
    this.save=this.save.bind(this);
    this.onPrint=this.onPrint.bind(this);
    this.onClickModel=this.onClickModel.bind(this);
    this.handleOkModel=this.handleOkModel.bind(this);
    this.handleCancelModel=this.handleCancelModel.bind(this);
  }

  componentDidMount() {
    this.socket = io(window.location.origin, {transports: ['websocket']})
    this.socket.connect()
    this.socket.on('connect', () => {
      console.log('connected');
    })
    this.socket.on('reportUpdate', (payload) => {
      console.log('reportUpdated', payload);
      api.getReport({taskID:this.props.data.study.taskID})
          .then((report)=>{
            console.log("report",report)
              this.setState({
                  report: this.state.report.merge(report)
              })
      }).catch(err=>console.log(err))

    })
    this.socket.on('labelUpdate', (payload) => {
      console.log('labelUpdated', payload);
      api.getLabels({
            taskID:this.props.data.study.taskID,
            labelType:this.props.data.study.modality=="CT"?"Nodule":"DX",
            seriesID:Object.values(this.props.data.study.series)[0].seriesID
      }).then((labels)=>{
          this.setState({
              labels
          })
      }).catch((err)=>{
          message.error("API error, getLabels",60)
          console.log(err)
      })
    })
    this.socket.on('disconnect', () => {
      console.log('disconnect');
    });
    this.socket.emit('open', {
      userID: getUser().account,
      taskID: this.props.data.report.taskID
    })
  }

  componentWillUnmount() {
    this.socket.emit('close',{
      userID: getUser().account,
      taskID: this.props.data.report.taskID
    })
    this.socket.close()
  }

  removeImage = (index) => {
    if(!!this.state.report.toJS().labelist)
    {
      this.setState({
        report: this.state.report
        .update('labelist',labelist=>labelist.delete(index))
        .update('screenshotURLs', screenshotURLs => screenshotURLs.delete(index))
      })
    }else{
      this.setState({
        report: this.state.report
        .update('screenshotURLs', screenshotURLs => screenshotURLs.delete(index))
      })
    }
  }

  save() {
    // const data = Object.assign({operation: 'update'}, this.state.report)
    const data = this.state.report.toJSON()
    console.log('save report', data)
    return (
      api.updateReport(data)
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.saved"))
        this.setState({
          report: this.state.report.set('updateDate', moment().format('YYYYMMDD')),
          unsave: false
        })
      })
      .catch((err)=>{
        message.error("API error, updateReport",600)
        throw err
      })
    )
  }

  radiomics(){
    console.log("downloaded radiomics")
    return true
  }

  submit = () => {
    const data = this.state.report.merge({action: 'submit'}).toJSON()
    return this.save()
      .then(() => api.updateReportStatus(data))
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.submitted"))
        this.setState({

          report: this.state.report.set('submitted', true)
        })
      })
      .catch((err)=>{
      message.error("API error, updateReportStatus",600)
      throw err
    })
  }

  unsubmit = () => {
    // this.save()
    const data = this.state.report.merge({action:'unsubmit'}).toJSON()
    return api.updateReportStatus(data)
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.unsubmitted"))
        this.setState({
          report: this.state.report.set('submitted', false)
        })
      })
      .catch((err)=>{
      message.error("API error, updateReportStatus",600)
      throw err
    })
  }

  examinate = () => {
    // this.save()
    const data = this.state.report.merge({action:'confirm'}).toJSON()
    return this.save()
      .then(() => api.updateReportStatus(data).catch((err)=>{message.error("API error, updateReportStatus",600);throw err}))
      .then(() => {
        message.success(GetLocaledText(this, "ReportPageLung.message.examinated"))
        this.setState({
          report: this.state.report.set('examined', true)
        })
      })
  }

  // template related
  handleTemplateChange = (key, value) => {
    let template = Object.assign({}, this.state.template, {[key]: value})
    this.setState({template})
  }

  saveTemplateChange = () => {
    api.updateTemplate(this.state.template)
    .then(() => message.success(GetLocaledText(this, "ReportPageLung.message.saved-successfully")))
    .catch((err)=>{
      message.error("API error, updateTemplate",600)
      throw err
    })
  }

  applyTemplate = () => {
    if (this.isForbidEdit()) {
      return
    }
    // don't change report's modality
    let template = Object.assign({}, this.state.template)
    delete template.modality
    this.setState({report: this.state.report.merge(template)})
  }

  searchTemplate = (kw) => {
    this.setState({
      templateList: this.allTemplate.filter(t => t.templateName.includes(kw) || t.templateID.includes(kw))
    })
  }

  filterTemplate = (category) => {
    let cats = category.split('/')
    let templateList
    const isModalityMatched = (t) => {
      let matched = t.modality === cats[1]
      if (cats[1] === 'DR') {
        matched = matched || (t.modality === 'DX')
      }
      return matched
    }
    if (cats.length === 1) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0])
    } else if (cats.length === 2) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0] && isModalityMatched(t))
    } else if (cats.length === 3) {
      templateList = this.allTemplate.filter(t => t.usergroup === cats[0] && isModalityMatched(t) && t.bodyPartExamined === cats[2])
    }
    this.setState({templateList})
  }

  openModal = () => {
    this.setState({
      modalVisible: true,
      newTemplateName: '',
    })
  }

  handleModalOk = () => {
    // validate input
    if (!this.state.newTemplateName) {
      // empty
      this.setState({modalErrorText: GetLocaledText(this, "ReportPageLung.mandatory-name")})
      return
    }
    let data = Object.assign({}, this.state.template, {
      "templateName": this.state.newTemplateName,
    })
    delete data.templateID
    api.addTemplate(data)
    .then(data => {
      data.usergroup = 'USER'
      this.setState({
        templateList: [data, ...this.state.templateList]
      })
      return api.getTemplate()  
        .catch((err)=>{
        message.error("API error, getTemplate",600)
        throw err
      })
    })
    .then(data => {
      this.allTemplate = data
      this.setState({
        modalVisible: false
      })
    })
    .catch((err)=>{
      message.error("API error, addTemplate",600)
      throw err
    })
  }

  renderMenuItems() {
    let items = []
    const privilege = this.state.report.get('user_privilege').toJS()
    if (privilege.includes('reporter')) {
      if (!this.state.report.get('submitted')) {
        // items.push(<Menu.Item key="save"><FormattedMessage id='ReportPageLung.save'/></Menu.Item>)
        // items.push(<Menu.Item key="submit"><FormattedMessage id='ReportPageLung.submit'/></Menu.Item>)
        //items.push(<Menu.Item key="print"><FormattedMessage id='ReportPageLung.print'/></Menu.Item>)
      } else if(!this.state.report.get('examined')) {
        items.push(<Menu.Item key="unsubmit"><FormattedMessage id='ReportPageLung.unsubmit'/></Menu.Item>)
      }
    } else if (privilege.includes('reviewer')
      && this.state.report.get('submitted')
      && !this.state.report.get('examined'))
    {
      items.push(<Menu.Item key="save"><FormattedMessage id='ReportPageLung.save'/></Menu.Item>)
      items.push(<Menu.Item key="examinate"><FormattedMessage id='ReportPageLung.examinate'/></Menu.Item>)
    }
    if(this.state.radiomics.length>0){
      items.push(<Menu.Item key="radiomics">
                   <CSVLink data={this.state.radiomics}
                             headers={this.state.headers}
                            filename={"radiomics.csv"} 
                            className="download-radiomics-button"
                            >
                      <FormattedMessage id='ReportPageLung.radiomics'/>
                   </CSVLink>
                 </Menu.Item>)
    }
    return items
  }

  renderStamp() {
    if (this.state.report.get('submitted') && !this.state.report.get('examined') && getUser().usergroup === 'reporter') {
      return <img className='report-status-stamp' src={require('../../../static/images/submit-stamp.png')}/>
    } else if (this.state.report.get('examined')) {
      return <img className='report-status-stamp' src={require('../../../static/images/exam-stamp.png')}/>
    }
    return ''
  }

  isForbidEdit() {
    const hasExamined = this.state.report.get('examined')
    const hasSubmitted = this.state.report.get('submitted')
    const isReporter = this.props.data.report.user_privilege.includes('reporter')
    const isExmainer = this.props.data.report.user_privilege.includes('reviewer')
    return hasExamined || (isReporter && hasSubmitted) || (isExmainer && !hasSubmitted)
  }

  showRawImage(url) {

  }
  onPrint(){
    this.save();
    window.open("/report_print/"+this.getUrlParams())
  }
  getUrlParams(){
      let htmlHref = window.location.href;
      htmlHref = htmlHref.replace(/^http:\/\/[^/]+/, "");
      let addr = htmlHref.substr(htmlHref.lastIndexOf('/', htmlHref.lastIndexOf('/') - 1) + 1);
      let index = addr.lastIndexOf("\/");
      //js 获取字符串中最后一个斜杠后面的内容
      let str = decodeURI(addr.substring(index + 1, addr.length));
      // js 获取字符串中最后一个斜杠前面的内容
      // let str = decodeURI(addr.substring(0, index));
      return str;
  }
    //帮助文档
    onClickModel(){
        this.setState({
            visible:true,
        })
    }
    handleOkModel(){
        this.setState({
            visible:false,
        })
    }
    handleCancelModel(){
        this.setState({
            visible:false,
        })
    }
    render() {
    const isForbidEdit = this.isForbidEdit()
    return (
      <div className='page' id='report-page'>
        <Prompt
          when={this.state.unsave}
          message={location => GetLocaledText(this, "ReportPageLung.message.unsaved-before-leave")}
        />
        <Header style={{height:"3rem",lineHeight:"3rem"}}>
          <Logo/>
          <Return  />
            <Menu
            theme="dark"
            mode="horizontal"
            style={{ lineHeight: '3rem', borderBottom: 'none', display: 'inline-block',height:"3rem"}}
            selectable={false}
            onClick={({item, key, keyPath}) => {
                if (key === 'submit') {this.submit()}
                else if (key === 'unsubmit') {this.unsubmit()}
                else if (key === 'save') {this.save()}
                else if (key === 'radiomics'){this.radiomics()}
                else if (key === 'examinate') {this.examinate()}
                else if (key === 'print') {this.onPrint()}
            }}
        >
            {this.renderMenuItems()}
        </Menu>
          <SignoutButton/>
            <span style={{color:"#fff",right:"180px",cursor:"pointer",position:"absolute",marginTop:"-0.3rem"}} onClick={this.onClickModel}><img src={require('../../../static/images/help_nor.png')}/></span>
          {/*<LanguageDropdownList setAppState={state => {this.props.setAppState(state)}}/>*/}
        </Header>
        <div className='content' style={{height:"95%"}}>
          <div className='template-aside'>
            <Template
              list={this.state.templateList}
              template={this.state.template}
              onSearch={this.searchTemplate}
              onCategoryChange={this.filterTemplate}
              onSelected={template => this.setState({template})}
              onApply={this.applyTemplate}
              onTemplateChange={this.handleTemplateChange}
              onSave={this.saveTemplateChange}
              onSaveAs={this.openModal}
              showSaveBtn={this.state.template.usergroup === 'USER'}
              />
          </div>
          <div  style={{  flexGrow: 1,
              height: "100%",
              position:'relative',
              overflow:"auto"}}>
            <ReportLungPage
            isForbidEdit={isForbidEdit}
            report={this.state.report.toJS()}
            labels={this.state.labels}
            removeImage={this.removeImage}
            zReverse={this.state.zReverse}
            maxZ = {this.state.maxZ}
            onChange={(key, value) => {
              this.setState({
                report: this.state.report.set(key, value)
              })
             }}/>
          </div>
        </div>
        <Modal
          title={GetLocaledText(this, "ReportPageLung.title.save-template")}
          visible={this.state.modalVisible}
          onOk={this.handleModalOk}
          onCancel={() => this.setState({modalVisible: false, modalErrorText: ''})}>
          <Input onChange={(e) => this.setState({newTemplateName: e.target.value})}
            placeholder={GetLocaledText(this, "ReportPageLung.input.template.name")}
            value={this.state.newTemplateName}
            />
          {
            this.state.modalErrorText && !this.state.newTemplateName.length ?
            <Alert message={this.state.modalErrorText} type="error" style={{marginTop: 5}}/> : ''
          }
        </Modal>
          <Modal
              title={GetLocaledText(this, "HelpDocument")}
              visible={this.state.visible}
              onOk={this.handleOkModel}
              onCancel={this.handleCancelModel}
              width={840}
              bodyStyle={{background:"#000",color:"#fff"}}
              footer={null}
          >
              <img src={require('../../../static/images/helpDocument.png')} style={{width:"794px"}} />
          </Modal>
           <div style={{position:"absolute",bottom:"10px",left:"55%"}}>
            {
                this.state.report.get('submitted')?<Button type="primary" onClick={this.unsubmit}><FormattedMessage id='ReportPageLung.unsubmit' style={{marginRight:"20px"}}  /></Button>:<span><Button type="primary" style={{marginRight:"20px"}} onClick={this.save}><FormattedMessage id='ReportPageLung.save'/></Button>
                <Button type="primary"  onClick={this.submit}><FormattedMessage id='ReportPageLung.submit'/></Button></span>
            }

             <Button type="primary" style={{marginLeft:"20px"}} onClick={this.onPrint}><FormattedMessage id='ReportPageLung.print'/></Button>
            </div>
      </div>
    )
  }

}

async function getData(props) {
  let studyErr = false
  let reportErr = false
  let labelsErr = false
  let radiomicsErr = false
  let templateErr = false
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let study= await api.getStudyDetail({studyUID})
  .catch((err)=>{
    message.error("API error, getStudyDetail",600)
    studyErr = true
  })

  if(studyErr)
  {
    study = EMPTYSTUDYCT
  }
  
  let report = await api.getReport({taskID:study.taskID})  
  .catch((err)=>{
    message.error("API error, getReport",60)
    reportErr = true
  });
  Object.values(report.screenshotURLs).forEach(url=>{url=URL_PREFIX+url;console.log(url)})

  if(reportErr)
  {
    report = EMPTYREPORT
  }
  
  let labels = await api.getLabels({
    taskID:study.taskID,
    labelType:study.modality=="CT"?"Nodule":"DX",
    seriesID:Object.values(study.series)[0].seriesID
  })
  .catch((err)=>{
    message.error("API error, getLabels",60)
    labelsErr = true
  })

  if(labelsErr)
  {
    labels = EMPTYNODULELABELS
  }
  
  let radiomicsJS = await api.getRadiomics({
    taskID:study.taskID,
    seriesID:Object.values(study.series)[0].seriesID
  })
  .catch((err)=>{
    message.error("API error, getRadiomics",60)
    radiomicsErr = true
  })
  
  if(radiomicsErr)
  {
    radiomicsJS = EMPTYRADIOMICS
  }
    
  let radiomics = []
  if(radiomicsJS.length!==0)
  {
    radiomicsJS[0].data_list.forEach(function(r,i){
        let arr=[]
        radiomicsJS[0].headers.forEach(function (item) {
            arr.push(r[item])
        })
      radiomics.push(arr)
    })
  }
  
  let template = await api.getTemplate()      
  .catch((err)=>{
    message.error("API error, getTemplate",60)
    templateErr = true
  })
  if(templateErr)
  {
    template = EMPTYTEMPLATE
  }
  let data = {report, template, labels,radiomics,study,radiomicsJS}
  return await data
}

ReportPage = LoadingWrapper(ReportPage, getData)

export default injectIntl(ReportPage)
