import React, { Component } from 'react';
import { Spin, Icon, message, Progress } from 'antd';
import './index.less'
import './directoryUploadPolyfill'
import {uploadFileList} from './fileUpload'
import api from '../../api'
import {FormattedMessage, injectIntl} from 'react-intl'

/**
 * 最大上传文件数
 * @type {number}
 */
const MAX_FILE_SIZE = 1000000000
/**
 * 正则匹配所有的以dcm和ima结尾的文件
 * @type {RegExp}
 */
const DICOM_FILE_REGX = new RegExp(/^.+\.(dcm|ima)$|^[^.]+$/)

/**
 * 上传缓冲条
 */
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

/**
 * 判断是否为谷歌浏览器
 * @return {boolean}
 */
function isChrome()
{
  let ua=navigator.userAgent.toLocaleLowerCase();
  if(ua.match(/chrome/)!=null)
  {
    return true
  }
  return false
}

/**
 * 根据上传浏览器判断从而获取获取上传文字描述
 */
function getUploadText()
{
  if(isChrome())
  {
    return <FormattedMessage id="Upload.uploadText"/>
  }
  else{
    return <FormattedMessage id="Upload.uploadText.notChrome"/>
  }
}

/**
 * 上传组件
 */
class Upload extends Component {
    /**
     * 参照生命周期构造函数
     * @param props
     */
  constructor(props) {
    super(props)
        /**
         *
         * @type {{
         * uploading: boolean, 是否上传
         * processing: boolean,  是否显示上传进度
         * timeout: boolean,  是否超时
         * progress: number, 上传进度值
         * finishCount: number, 上传完成数量
         * failCount: number,  上传失败数量
         * taskCount: number 任务数量
         * }}
         */
    this.state = {
      uploading: false,
      processing: false,
      timeout:false,
      progress: 0,
      finishCount: 0,
      failCount: 0,
      taskCount: 0,
    }
        /**
         * 文件key列表
         * @type {Array}
         */
    this.fileKeys = []
  }

    /**
     * 参照react生命周期函数
     */
  componentDidMount() {
    const dragHandler = (e) => {
      // e.stopPropagation();
      e.preventDefault();
      if (e.type === 'dragover' || e.type === 'dragenter') {
        e.target.classList.add('drag-over')
      } else {
        e.target.classList.remove('drag-over')
      }
    }
    
    if(isChrome())
    {
      this.dropElm.addEventListener('dragenter', dragHandler)
      this.dropElm.addEventListener('dragover', dragHandler)
      this.dropElm.addEventListener('dragleave', dragHandler)
      this.dropElm.addEventListener('drop', this.onDrop)
    }
    this.inputElm.setAttribute('webkitdirectory', 'true')
    this.inputElm.setAttribute('directory', 'true')
  }

    /**
     * 进度百分比值
     * @param percent
     */
  onProgress = (percent) => {
    if (percent === 100) {
      this.setState({
        processing: true,
        uploading: false,
        timeout:false
      })
    }
    this.setState({progress: percent})
  }

    /**
     * 上传成功回调
     * @param data
     */
  onSuccess = (data) => {
    this.props.onSuccess(data)
    this.setState({
      processing: false
    })
  }
    /**
     * 上传失败回调
     * @param err
     */
  onFail = (err) => {
    if(!!err.response)
    {
      this.props.onFail(err)
      this.setState({
        processing: false
      })
    }else{
      this.setState({
        timeout:true,
        processing: false
      })
    }
  }

    /**
     * 上传文件函数
     * @param fileList 文件列表
     */
  uploadFiles(fileList) {
    if (this.state.uploading || this.state.processing) {
      return
    }
    let validFiles = []
    for (let file of fileList) {
      if (file.size > MAX_FILE_SIZE) continue
      if (!DICOM_FILE_REGX.test(file.name.toLowerCase())) continue
      validFiles.push(file)
    }
    // console.log('valid files', validFiles);
    this.fileKeys = []
    if (validFiles.length) {
      uploadFileList(validFiles, this.onProgress, this.onSuccess, this.onFail)
    }
    this.setState({
      uploading: !!validFiles.length,
      taskCount: validFiles.length,
      finishCount: 0,
      progress: 0,
      failCount: 0
    })
  }

    /**
     * 拖动上传事件
     * @param e
     */
  onDrop = (e) => {
    e.preventDefault();

    var fileList = []
    var iterateFilesAndDirs = async function(filesAndDirs, path) {
      for (var i = 0; i < filesAndDirs.length; i++) {
        if (typeof filesAndDirs[i].getFilesAndDirectories === 'function') {
          var path = filesAndDirs[i].path;

          // this recursion enables deep traversal of directories
          let subFilesAndDirs = await filesAndDirs[i].getFilesAndDirectories()
          // iterate through files and directories in sub-directory
          await iterateFilesAndDirs(subFilesAndDirs, path);
        } else {
          fileList.push(filesAndDirs[i])
        }
      }
    };
    // begin by traversing the chosen files and directories
    if ('getFilesAndDirectories' in e.dataTransfer) {
      e.dataTransfer.getFilesAndDirectories().then((filesAndDirs) => {
        iterateFilesAndDirs(filesAndDirs, '/').then(() => this.uploadFiles(fileList))
      });
    }
  }

    /**
     * 文件输入事件切换
     * @param e
     */
  onFileInputChange = (e) => {
    if (!e.target.files.length) return
    this.uploadFiles(e.target.files)
  }

    /**
     * 上传组件
     * @return {Component}
     */
  render() {
    return (
      <div className='upload'>
        <div
          className="drop-zone"
          ref={input => {this.dropElm = input}}
          onClick={() => this.inputElm.click()}>
          <p className="ant-upload-drag-icon">
            <Icon type="inbox" />
          </p>
          <p className="ant-upload-text">{getUploadText()}</p>
          <p className="ant-upload-hint"><FormattedMessage id="Upload.uploadHint"/></p>
        </div>
        <input
          className='hidden'
          ref={input => {this.inputElm = input}}
          onChange={this.onFileInputChange}
          type="file"
          webkitdirectory directory multiple/>
        {
          this.state.uploading ?
          <Progress percent={this.state.progress} status="active"
            format={(percent) => Math.round(percent) + '%'}/>
          :
          ''
        }
        {
          this.state.uploading ?
          <p className='upload-tips'><Spin indicator={antIcon} /><FormattedMessage id="Upload.uploading-pre"/>{this.state.taskCount}<FormattedMessage id="Upload.uploading-post"/></p>
          :
          ''
        }
        {
          this.state.processing?
          <p className='upload-tips'><Spin indicator={antIcon} /><FormattedMessage id="Upload.finished-processing"/></p>
          :
          ''
        }
        {
          this.state.timeout ?
          <p className='upload-tips'><FormattedMessage id="Upload.finished-timeout"/></p>
          :
          ''
        }
      </div>
    );
  }

}

export default Upload;
