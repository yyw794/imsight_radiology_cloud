#!/bin/python
# coding=utf-8
import logging
import os
import sys
import re
from flask import make_response, request, jsonify, abort
from flask_login import current_user

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find, db_page_query, db_find_one
from models.response.response import response
from services.pacs.pacs_api import unsend_studylist_from_pacs

optional_key = ['needData']

logger = logging.getLogger('radiology')


def get_studylist_count():
    params = request.args.to_dict()
    res = response()

    query_value = {}
    if 'patientID' in params and params['patientID']:
        query_value['patientID'] = params['patientID']
    if 'petietName' in params and params['patientName']:
        query_value['patientName'] = params['patientName']
    if 'gender' in params and params['gender']:
        query_value['gender'] = params['gender']
    if 'studyDate' in params and params['studyDate']:
        query_value['studyDate'] = params['studyDate']
    if 'birthDate' in params and params['birthDate']:
        query_value['birthDate'] = params['birthDate']

    page_no = 0
    page_size = 0
    count = 0
    if 'page_size' in params and 'page_no' in params:
        page_size = int(params['page_size'])
        page_no = int(params['page_no'])
        _, count = db_page_query('study', query_value, page_size, page_no)

    res.statusCode = 1000
    res.data = {"page_no": page_no, "page_size": page_size, "page_count_total": count}
    return make_response(jsonify(res.to_dict()), 200)


def get_ownership_by_user(user_id):
    """

    :param user:
    :return:
    """
    ownership_info = db_find_one('ownership', {"userID": user_id})
    if not ownership_info:
        shared_list = []
    else:
        try:
            shared_list = ownership_info["shared"].keys()
            shared_list = [i.replace("-", ".") for i in shared_list]
        except KeyError as exc:
            logger.error("col ownership query error. err=%s", exc)
            shared_list = []
    return shared_list


def get_studylist():
    """
    @api {get} /api/studylist  获取DOCIM信息列表
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} [patientID] 病历ID
    @apiParam {String} [patientName] 病人名称
    @apiParam {String} [gender] 性别
    @apiParam {String} [birthDate] 出生日期
    @apiParamExample {string} Request-Example:
        /api/studylist?patientID=&patientName=&gender=&birthDate=
    @apiSuccessExample {json} Success-Response:
    {
  "data": [
    {
      "age": 61,
      "birthDate": "1957-10-29",
      "bodyPartExamined": "CHEST",
      "diagnosisAdvice": "",
      "gender": "FEMALE",
      "hospital": "hosp",
      "labelNum": 0,
      "modality": "CT",
      "patientID": "1001203",
      "patientName": "YYY",
      "result": "pending",
      "reverse": false,
      "state": "file_only",
      "studyDate": "2018-12-03",
      "studyTime": "17:48:23",
      "studyUID": "1.2.392.200036.9116.2.5.1.3268.2060439916.1525305902.560145",
      "submitted": false,
      "taskID": "d74e82a4-f2b2-11e8-b0fa-0242ac120003",
      "uploadDate": "2018-12-03 17:48:23",
      "user_privilege": [
        "reporter",
        "normal"
       ]
     }
    ],
   "message": "OK",
    "statusCode": 1000
  }
   @apiSuccess {String} diagnosisAdvice 诊断意见
   @apiSuccess {String} labelNum 结节数量
   @apiSuccess {String} state meta-only: 无图像， file_only: 已转PNG， png_unsupport:
    不支持转PNG， complete: 检测完成， algo_unsupport: 算法不支持
   @apiSuccess {String} result pending: 处理中，normal: 正常， abnormal: 异常
   @apiSuccess {bool} submitted 是否提交
    """
    params = request.args.to_dict()
    res = response()

    user_id = current_user.get_id()
    shared_list = get_ownership_by_user(user_id)
    default_list = get_ownership_by_user("imsightmed")
    shared_list.extend(default_list)
    query_value = {}
    if shared_list:
        query_value["studyUID"] = {"$in": shared_list}
    if 'patientID' in params and params['patientID']:
        query_value['patientID'] = params['patientID']
        if 'plugin' in params:
            re_patient_id = '^%s$' % query_value['patientID']
            re_patient_id = re.compile(re_patient_id, re.IGNORECASE)
            query_value['patientID'] = re_patient_id

    if 'patientName' in params and params['patientName']:
        query_value['patientName'] = params['patientName']
    if 'gender' in params and params['gender']:
        query_value['gender'] = params['gender']
    if 'studyDate' in params and params['studyDate']:
        query_value['studyDate'] = params['studyDate']
    if 'birthDate' in params and params['birthDate']:
        query_value['birthDate'] = params['birthDate']

    if 'page_size' in params and 'page_no' in params:
        page_size = int(params['page_size'])
        page_no = int(params['page_no'])
        study_infolist, count = db_page_query('studylist', query_value, page_size, page_no)
    else:
        study_infolist = db_find('studylist', query_value, 'uploadDate')

    if 'plugin' not in params and 'needData' not in params:
        if "studyUID" in query_value:
            del query_value["studyUID"]
        unsend_data_list = unsend_studylist_from_pacs(query_value)
        for d in unsend_data_list:
            study_infolist.append(d)

    data = {}
    if 'page_no' in params:
        data['page_no'] = int(params['page_no'])
        data['page_no_total'] = count
    # else:
    #     if study_infolist:
    #         study_infolist = sorted(study_infolist, key=lambda s: s['studyDate'], reverse=True)
    #     else:
    #         study_infolist = []
    data = study_infolist

    res.statusCode = 1000
    res.data = data
    return make_response(jsonify(res.to_dict()), 200)
