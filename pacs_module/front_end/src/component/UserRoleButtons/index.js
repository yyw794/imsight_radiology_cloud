import React, { Component } from 'react';
import {withRouter} from 'react-router'
import {Button, Affix, Row, Modal} from 'antd'
import { getUserRole } from '../../auth';
import api from '../../api'
import {clip} from '../../util'
import _ from 'underscore'
import {GetLocaledText} from '../../localedFuncs'
import './index.less'

class UserRoleButtons extends React.Component{
  onNext = () => {
    if (_.isFunction(this.props.beforeSwitch)) {
      return this.props.beforeSwitch().then(() => this.moveIdx(1))
    }
    this.moveIdx(1)
  }

  onPrev = () => {
    if (_.isFunction(this.props.beforeSwitch)) {
      return this.props.beforeSwitch().then(() => this.moveIdx(-1))
    }
    this.moveIdx(-1)
  }

  allowPrev() {
    const studyUID = this.props.study.studyUID
    const labelTasks = this.props.labelTasks
    let curIdx = labelTasks.findIndex(l => l.studyUID === studyUID)
    return curIdx !== null && curIdx >= 1
  }

  allowNext() {
    const studyUID = this.props.study.studyUID
    const labelTasks = this.props.labelTasks
    let curIdx = labelTasks.findIndex(l => l.studyUID === studyUID)
    return curIdx !== null && curIdx < labelTasks.length - 1
  }

  moveIdx(offset) {
    const studyUID = this.props.study.studyUID
    const labelTasks = this.props.labelTasks
    let curIdx = labelTasks.findIndex(l => l.studyUID === studyUID)
    let idx = clip(curIdx + offset, 0, labelTasks.length - 1)
    if (idx === curIdx) {
      Modal.confirm({
        title: GetLocaledText(this,"UserRoleButtons.dialog.question.return"),
        onOk: () => this.props.history.push('/'),
        onCancel: () => {},
        maskClosable: true,
        okText: GetLocaledText(this,"UserRoleButtons.dialog.answer.return"),
        cancelText: GetLocaledText(this,"UserRoleButtons.dialog.answer.stay"),
      })
      return
    }
    let pathname = this.props.location.pathname
    let parts = pathname.split('/')
    parts[2] = labelTasks[idx].studyUID.replace(/\./g, '-')
    this.props.history.replace(parts.join('/'))
    // TODO: refreshing the whole page sucks
    // window.location.reload()
  }

  submit = () => {
    api.submitLabelTask({studyUID: this.props.study.studyUID}).then(this.onNext)
  }

  review(isPass) {
    api.reviewLabelTask({studyUID: this.props.study.studyUID, isPass}).then(this.onNext)
  }

  render(){
    const study = this.props.study
    return(
    <div className='user-role-buttons'>
      <Row>
        <Button type="primary" disabled={!this.allowPrev()} ghost className='previous' onClick={this.onPrev}>上一个</Button>
        {
          "labeler" == getUserRole() && (study.labelStatus === 'unlabeled' || study.labelStatus === 'failed') ?
          <Button type="primary" disabled={this.props.disableSubmit} ghost className='submit' onClick={this.submit}>提交</Button> : ''
        }
        <Button type="primary" disabled={!this.allowNext()} ghost className='next' onClick={this.onNext}>下一个</Button>
      </Row>
      <Row hidden = {"reviewer" != getUserRole() || study.labelStatus !== 'labeled'}>
        <Button type="primary"  className='pass' onClick={() => this.review(true)}>通过</Button>
        <Button type="primary"  className='fail' onClick={() => this.review(false)}>不通过</Button>
      </Row>
    </div>
    );
  }
}

UserRoleButtons = withRouter(UserRoleButtons)

export default UserRoleButtons;
