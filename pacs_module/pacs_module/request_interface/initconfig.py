import configparser

config = configparser.ConfigParser()
config.read("config.ini")


def get_AppTitle():
	return str(config.get(section='pacs', option='AppTitle'))


def get_MoveAppTitle():
	return str(config.get(section='pacs', option='MoveAppTitle'))


def get_ListenPACSPort():
	return config.getint(section='pacs', option='ListenPACSPort')


def get_PeerHost():
	return str(config.get(section='pacs', option='PeerHost'))


def get_PeerPort():
	return config.getint(section='pacs', option='PeerPort')
	
def get_PeerAppTitle():
	return str(config.get(section='pacs', option='PeerAppTitle'))


def get_support_type():
	return str(config.get(section='pacs', option='support_type'))


def get_history_patient_list_days():
	return config.getint(section='pacs', option='history_patient_list_days')


def get_store_path_url():
	return str(config.get(section='server', option='store_path_url'))


def get_server_url():
	return str(config.get(section='server', option='server_url'))


def get_server_account():
	return str(config.get(section='server', option='server_account'))


def get_server_password():
	return str(config.get(section='server', option='server_password'))


def get_http_listen_ip():
	return str(config.get(section='server', option='http_listen_ip'))


def get_http_listen_port():
	return config.getint(section='server', option='http_listen_port')


def get_if_push_model():
	if_push = str(config.get(section='server', option='if_push_module'))
	if if_push == 'true':
		return True
	else:
		return False


def get_history_patient_list_image_days():
	return config.getint(section='pacs', option='history_patient_list_image_days')
