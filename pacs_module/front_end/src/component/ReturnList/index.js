import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'
import './index.less'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {VIEW_MODALITY_URL, openWindow, openWindowBlank} from "../StudyTable";
import Immutable from 'immutable'
const allSkip=new Immutable.Map();
const style = {
  'cursor': 'pointer',
  'WebkitUserSelect': 'none',
  'MozUserSelect': 'none',
  'msUserSelect': 'none',
  'userSelect': 'none',
}
console.log()
const Return = withRouter(
  ({ history }) => (
      <span>
      <Link to="/">
      <a style={{float:"left",color:"#fff",height:"30px"}}>
        <img style={style} className='logo-image'style={{
            cursor:"pointer",
            userSelect:"none",
            width:"30px",
            verticalAlign:"middle",
            marginRight:"10px",
            marginLeft:"20px"}} src={require('../../../static/images/returnList.png')} /*onClick={() => history.push('/')}*//>
          <span><FormattedMessage id='ReturnList.button.name'/></span>
      </a>
      </Link>
          {
              JSON.parse(localStorage.getItem("fellowParams"))?JSON.parse(localStorage.getItem("fellowParams")).history?window.location.pathname.split("/")[1]!=="report"&& JSON.parse(localStorage.getItem("fellowParams")).history.length>1?<a style={{float:"left",color:"#fff",height:"30px"}} onClick={e => {
                  e.stopPropagation()
                  let patientID = JSON.parse(localStorage.getItem("fellowParams")).patientID
                  const arr=[];
                  JSON.parse(localStorage.getItem("fellowParams")).history.reverse().map(function (item,index) {
                      arr.push(item.studyUID.split(".").join("-"))
                  });
                  window.location.href=`/view_ct_follow_up/${patientID}/${arr.join(",")}`}}>
                  <img style={style} className='logo-image'style={{
                      cursor:"pointer",
                      userSelect:"none",
                      width:"30px",
                      verticalAlign:"middle",
                      marginRight:"10px",
                      marginLeft:"20px"}} src={require('../../../static/images/fellow.png')} /*onClick={() => history.push('/')}*//>
              </a>:"":"":""
          }

      </span>

  )
)

export default Return
