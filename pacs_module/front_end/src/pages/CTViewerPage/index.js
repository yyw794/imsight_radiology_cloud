import React, { Component } from 'react'
import CT3Viewer from '../../component/CT3Viewer'
import api from '../../api'
import LoadingWrapper from '../../component/LoadingWrapper'
import ToolBar from '../../component/CTToolBar'
import NodulePanel from '../../component/NodulePanel'
import Snapshot from '../../component/Snapshot'
import ThumbnailList from '../../component/ThumbnailList'
import {getRiskText, getNoduleType, RISK_EN2CN, TYPE_EN2CN} from '../../component/NodulePanel'
import NodulePanelLiver from '../../component/LiverViewer/NodulePanelLiver'
import noduleBox from '../../component/tools/noduleBox'
import axios from 'axios'
// import {Icon, message, Button, Modal, Spin, Menu, Dropdown, Row, Col, Select} from 'antd'
import { Card, Layout, Button, Dropdown, Row, Col, Select, message, Tabs,Modal } from 'antd'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {addCTMetaData} from '../../metaData.js'
import {getUser} from '../../auth'
import AddNoduleModal from './AddNoduleModal'
import RunDetectionModal from './RunDetectionModal'
import _ from 'underscore'
import io from 'socket.io-client'
import itemTypes from '../../component/dragDropItemTypes'
import DropZoom from './DropZoom'
import { connect } from 'react-redux'
import Immutable from 'immutable'
import {openWindow,canOpenReport,openWindowBlank,openWindowBlankTab} from '../../component/StudyTable'

import './index.less'
import Logo from "../../component/Logo";
import Return from "../../component/ReturnList";
import SignoutButton from "../../component/SignoutButton";
import DRViewer from "../../component/DRViewer";

const { Header, Content, Footer } = Layout;
const TabPane = Tabs.TabPane

class CTViewerPage extends Component {
  constructor(props) {
    super(props)
    const defaultSeriesNum = Object.keys(props.data.study.series)[0]
      console.log("props.data.study tumorLabels",props.data.study)
    this.state = {
      visible:false,
      study: props.data.study,
      series: props.data.study.series,
      lungNodules: props.data.study.noduleLabels.map(label => Object.assign({}, label)),
      liverTumors: props.data.study.tumorLabels.map(label => Object.assign({}, label)),
      targetSeriesID: defaultSeriesNum,
      activeTabKey: props.data.study.tumorLabels.length===0?"chest":"liver",
      activeTool: ['referenceLines'],
      addModalVisible: false,
      addModalStatus: 'confirm',
      detectionModalVisible:false,
      detectionStatus:'selection',
      detectionList:[],
      nodulePopoverBtn: {
        action: 'add',
        show: false,
        left: 0,
        top: 0
      },
      displaySeries: [defaultSeriesNum]
    }
    if (this.props.data.report) {
      this.report = this.props.data.report
    }
    this.initSocket()
    this.viewers = {}
      this.onClickModel=this.onClickModel.bind(this);
      this.handleOkModel=this.handleOkModel.bind(this);
      this.handleCancelModel=this.handleCancelModel.bind(this);
    // this.nodulePanels = {}
  }

  setGridLayout(num) {
    console.log("this.state.displaySeries",this.state.displaySeries)
    if (num === this.state.displaySeries.length) {
      return
    } else if (num < this.state.displaySeries.length) {
      this.setState({
        displaySeries: this.state.displaySeries.slice(0, num)
      }, () => {
        this.state.displaySeries.filter(sn => sn !== null).forEach(sn => this.viewers[sn].onResize())
      })
    } else {
      let arr = new Array(num - this.state.displaySeries.length)
      arr.fill(null)
      this.setState({
        displaySeries: [...this.state.displaySeries, ...arr]
      }, () => {
        this.state.displaySeries.filter(sn => sn !== null).forEach(sn => this.viewers[sn].onResize())
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.id.replace(/-/g, '.') !== this.state.study.studyUID) {
      console.log('CTViewerPage reload data')
      getData(nextProps).then(data => {
        this.setState({
          study: data.study,
          lungNodules: data.study.noduleLabels.map(label => Object.assign({}, label)),
          liverTumors: data.study.tumorLabels.map(label => Object.assign({}, label)),
          addModalVisible: false,
          addModalStatus: 'confirm',
          detectionModalVisible:false,
          detectionStatus:'selection',
          detectionList:[],
          nodulePopoverBtn: {
            action: 'add',
            show: false,
            left: 0,
            top: 0
          }
        })
        if (data.report) {
          this.report = data.report
        }
      })
    }
  }

  initSocket() {
    this.socket = io(window.location.origin, {transports: ['websocket']})
    this.socket.connect()
    this.socket.on('connect', () => {
      console.log('connected');
    })
    this.socket.on('labelUpdate', (payload) => {
      if(this.state.activeTabKey==="liver")
      {
        console.log('labelUpdate', payload.data);
        console.log("socket data inject to liverTumors")
        this.setState({
          liverTumors:payload.data
        })
      }else if(this.state.activeTabKey==="chest"){
        console.log('labelUpdate', payload.data);
        console.log("socket data inject to lungNodules")
        this.setState({
          lungNodules:payload.data
        })
      }
    })
    this.socket.on('disconnect', () => {
      console.log('disconnect');
    });
    this.socket.emit('open', {
      userID: getUser().account,
      taskID: this.props.data.study.taskID
    })
  }

  onToolClick = (name, extra) => {
    if(name==='detect'){
      return this.setState({detectionModalVisible:true})
    }

    if(name==='report'){
      return openWindowBlankTab("report",this.props.match.params.id.replace(/-/g, '.'),"")
    }
    if (name === 'grid') {
      return this.setGridLayout(extra)
    }

    for (let viewer of Object.values(this.viewers)) {
      if(viewer===null){
        continue
      }
      if(name==='eraser'){
          this.setState({
              activeTool: ['referenceLines']
          })
            return   viewer.clearAllTool()
        }
      if (name === 'wwwc' && extra) {
        viewer.setWWWC(extra.ww, extra.wc)
      } else if (!this.state.activeTool.includes(name)) {

         viewer.activateTool(name)
          if(name==="length"||name==="angle"||name==="freehand"){
              this.setState({
                  activeTool:name
              })
          }else{
              this.setState({
                  activeTool: [name, ...this.state.activeTool]
              })
          }
      } else {
        viewer.deactivateTool(name)
        this.setState({
          activeTool: _.without([...this.state.activeTool], name)
        })
      }
    }
  }

  saveScreenshotToReport = (label) => {
    const viewer = this.viewers[this.state.targetSeriesID]
    const imgPos = viewer.getImgPos()
    const size = 300
    const image = viewer.getCurrentImageObj('z')
    const stacks = this.state.series[this.state.targetSeriesID].stacks
      console.log("stacks.x.imageIds",stacks.x.imageIds)
    let coordImage = {
      x: image.width * (imgPos.x + 1) / stacks.x.imageIds.length,
      y: image.width * (imgPos.y + 1) / stacks.y.imageIds.length
    }
    viewer.getScreenshot(coordImage, size).then(image => {
      var data = new FormData()
      data.append('file', image, 'image.png')
      data.append('labelID', label.labelID)
      data.append('seriesID',this.state.targetSeriesID)
      data.append('taskID',this.state.study.taskID)
      var config = {
        onUploadProgress: function(progressEvent) {
          var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total )
          console.log(percentCompleted)
        }
      }
      axios.post(`/api/upload_screenshot`, data, config)
        .then((res) => {
          message.success(GetLocaledText(this,"CTViewerPage.alert.screenshot.success"))
        })
        .catch(function (err) {
          message.error(GetLocaledText(this,"CTViewerPage.alert.screenshot.failed"))
        })
    })
  }

  removeScreenshotInReport = (label) => {
    // this.report.screenshotURLs = this.report.screenshotURLs.filter(url => url !== nodule.url)
    // api.updateReport(this.report)
    let idx = this.state.lungNodules.findIndex(l => l.labelID === label.labelID)
    if (idx !== -1) {
      let lungNodules = [...this.state.lungNodules]
      // delete lungNodules[idx].url
      lungNodules[idx].url = ''
      let data = {
        taskID: this.state.study.taskID,
        seriesID: this.state.targetSeriesID,
        labels: lungNodules
      }
      api.saveLabels(data).then(() => this.setState({lungNodules}))
    }
    idx = this.state.liverTumors.findIndex(l => l.labelID === label.labelID)
    if (idx !== -1) {
      let liverTumors = [...this.state.liverTumors]
      // delete liverTumors[idx].url
      liverTumors[idx].url = ''
      let data = {
        taskID: this.state.study.taskID,
        seriesID: this.state.targetSeriesID,
        labels: liverTumors
      }
      api.saveLabels(data).then(() => this.setState({liverTumors}))
    }
  }

  onAddNoduleClick = () => {
    this.setState({
      addModalVisible: true,
      addModalStatus: 'confirm'
    })
  }

  /**
   * analyze nodule using algorithm
   * @return {[Promise]} request
   */
  analyzeNodule = () => {
    const viewer = this.viewers[this.state.targetSeriesID]
    const imgPos = viewer.getImgPos()
    let coord = {
      x: imgPos.x + 1,
      y: imgPos.y + 1,
      z: imgPos.z + 1
    }
    let noduleToAdd = {
      coord,
      seriesID:this.state.targetSeriesID
    }
    let data = {
      taskID: this.state.study.taskID,
      seriesID: this.state.targetSeriesID,
      labels:[noduleToAdd,...this.state.lungNodules]
    }
    return api.saveLabels(data)
    .then(resp => {
      if (_.isEmpty(resp)) {return}
      if (this.state.lungNodules.find(label => label.labelID === resp.labelID)) {return}
      this.setState({
        lungNodules: resp
      }, () => {
        let select = [...this.state.lungNodules].find(label => label.coord.z === coord.z && label.coord.y === coord.y && label.coord.x===label.coord.x)
        this.nodulePanel.getWrappedInstance().selectNodule(select)
        viewer.forceUpdateImage()
        console.log('added nodule')
      })
    })
  }

  updateTumor(labelID, isAPE, mfsWashout, mfsEnhancing,mfsThresholdGrowth){
    let changeTumor = [...this.state.liverTumors].find(l=>l.labelID === labelID)
    changeTumor.isAPE = isAPE
    changeTumor.mfsWashout = mfsWashout
    changeTumor.mfsEnhancing = mfsEnhancing
    changeTumor.mfsThresholdGrowth = mfsThresholdGrowth
    let stillTumor = [...this.state.liverTumors].filter(l=>l.labelID !== labelID)
    let data = {
      seriesID:this.state.targetSeriesID,
      taskID:this.state.study.taskID,
      labels:[changeTumor, ...stillTumor],
      labelType:"LiverTumor"
    }
    api.saveLabels(data)
    .then(resp => {
      this.setState({
        liverTumors:resp
      },()=>{
        console.log(resp)
      })
    })
  }

  removeNodule = (labelID) => {
    const viewer = this.viewers[this.state.targetSeriesID]
    let removedList = [...this.state.lungNodules].filter(label => label.labelID !== labelID)
    let data = {
      seriesID:this.state.targetSeriesID,
      taskID:this.state.study.taskID,
      labels:removedList
    }
    api.saveLabels(data)
    .then(resp => {
      this.setState({
      lungNodules:resp
      }, () => {
        viewer.forceUpdateImage()
        message.success(GetLocaledText(this,"CTViewerPage.remove-label"))
      })
    })
  }

  removeTumor = (labelID)=>{
    const viewer = this.viewers[this.state.targetSeriesID]
    let removedList = [...this.state.liverTumors].filter(label => label.labelID !== labelID)
    let data = {
      seriesID:this.state.targetSeriesID,
      taskID:this.state.study.taskID,
      labels:removedList
    }
    console.log("before remove:")
    console.log(data)
    api.saveLabels(data)
    .then(resp => {
      console.log("after removed:")
      console.log(resp)
      this.setState({
      liverTumors:resp
      }, () => {
        viewer.forceUpdateImage()
        message.success(GetLocaledText(this,"CTViewerPage.remove-label"))
      })
    })
  }

  isForbidLabeling() {
    return false
  }

  renderNoduleInfoEditor = (nodule) => {
    if (!nodule) {return ''}
    const RISK_PROB = {
      Benign: 0,
      Low: 0.5,
      Middle: 0.8,
      High: 1
    }
    let risk
    let type
    if (nodule.malg !== null) {
      risk = getRiskText(nodule.malg)
    }
    if (nodule.subtTrue !== null) {
      type = getNoduleType(nodule)
    }
    return (
      <div className='nodule-info-editor'>
        <Row>
          <Col span={12}><FormattedMessage id='CTViewerPage.nodule-info-editor.subclass'/></Col>
          <Col span={12}>
            <Select value={type} disabled={this.isForbidLabeling()} className='selector-list' onChange={v => {
                this.updateNodule(this.nodulePanel.getWrappedInstance().getSelectedNodule().labelID, {subtTrue: v})
              }}>
              <Option value='GroundGlass'> {TYPE_EN2CN.GroundGlass} </Option>
              <Option value='PartSolid'> {TYPE_EN2CN.PartSolid} </Option>
              <Option value='Solid'> {TYPE_EN2CN.Solid} </Option>
              <Option value='Calcification'> {TYPE_EN2CN.Calcification} </Option>
            </Select>
          </Col>
        </Row>
        <Row>
          <Col span={12}><FormattedMessage id='CTViewerPage.nodule-info-editor.malignancy'/></Col>
          <Col span={12}>
            <Select value={risk} disabled={this.isForbidLabeling()} className='selector-list' onChange={v => {
                this.updateNodule(this.nodulePanel.getWrappedInstance().getSelectedNodule().labelID, {malg: RISK_PROB[v]})
              }}>
              <Option value='Benign'> {RISK_EN2CN.Benign} </Option>
              <Option value='Low'> {RISK_EN2CN.Low} </Option>
              <Option value='Middle'> {RISK_EN2CN.Middle} </Option>
              <Option value='High'> {RISK_EN2CN.High} </Option>
            </Select>
          </Col>
        </Row>

      </div>
    )
  }

  renderGrid() {
    let rows
    let series = this.state.displaySeries.map(seriesID => {
      return seriesID ? this.state.series[seriesID] : null
    })
    if (series.length <= 3) {
      rows = [series]
    } else {
      rows = _.chunk(series, 2)
    }
    let idx = 0
    return rows.map((row, i) => {
      const span = 24 / row.length
      return (
        <Row>
          {row.map((data, j) => {
            let pos = idx++
            if (data) {
              // const id = `r${i}c${j}`
              const id = data.seriesID
              let labels = this.state.activeTabKey === 'chest' ? this.state.lungNodules : this.state.liverTumors
              labels = labels.filter(l => l.seriesID === id)
              return (
                <Col style={{height:'100%',width:`${span/24*100}%`}} span={span} className='grid-cell'>
                  <DropZoom onDrop={data => {
                      console.log(data);
                      let displaySeries = [...this.state.displaySeries]
                      displaySeries[pos] = data.id
                      this.setState({
                        displaySeries,
                        targetSeriesID:data.id
                      })
                    }}>
                  <CT3Viewer
                    ref={input => {this.viewers[id] = input}}
                    id={id}
                    study={this.state.study}
                    series={data}
                    labels={Immutable.fromJS(labels)}
                    instance={this.state.displayInstance}
                    tools={this.state.tools}
                    onLabelsChange={labels => this.setState({labels})}
                    onToolsChange={tools=>this.setState({tools})}
                    onlyZ={series.length > 1}
                    onClick={e => {
                      // TODO: liver label
                      if (this.state.activeTabKey === 'liver') {
                        const imgPos = this.viewers[id].getImgPos()
                        const clickedTumors = noduleBox.getInsideNodules(e.detail.element, imgPos)
                        if (clickedTumors.length) {
                          this.setState({targetSeriesID: id}, () => this.nodulePanelLiver.getWrappedInstance().selectNodule(clickedTumors[0]))
                        }

                      }else{
                        const imgPos = this.viewers[id].getImgPos()
                        const clickedNodules = noduleBox.getInsideNodules(e.detail.element, imgPos)
                        if (clickedNodules.length) {
                          this.setState({targetSeriesID: id}, () => this.nodulePanel.getWrappedInstance().selectNodule(clickedNodules[0]))
                        }
                        if (e.detail.which === 3) {
                          this.setState({
                            nodulePopoverBtn: {
                              action: clickedNodules.length > 0 ? 'remove' : 'add',
                              show: true,
                              left: e.detail.currentPoints.page.x,
                              top: e.detail.currentPoints.page.y
                            }
                          })
                        } else if (e.detail.which === 1) {
                          this.setState({
                            nodulePopoverBtn: Object.assign({}, this.state.nodulePopoverBtn, {show: false})
                          })
                        }
                      }
                      this.setState({targetSeriesID: id})
                    }}
                    style={{height: '100%', position: 'relative'}}/>
                  </DropZoom>
                </Col>
              )
            } else {
              return (
                <Col style={{height:'100%',width:`${span/24*100}%`}} span={span} className='grid-cell'>
                  <DropZoom onDrop={data => {
                      console.log(data);
                      let displaySeries = [...this.state.displaySeries]
                      displaySeries[pos] = data.id
                      this.setState({displaySeries})
                    }}/>
                </Col>
              )
            }
          })}
        </Row>
      )
    })
  }
    //帮助文档
    onClickModel(){
        this.setState({
            visible:true,
        })
    }
    handleOkModel(){
        this.setState({
            visible:false,
        })
    }
    handleCancelModel(){
        this.setState({
            visible:false,
        })
    }
  render() {
    let user = getUser()
    let userID = ''
    if (user) {
      userID = user.account
    }
    const studyUID = this.props.match.params.id.replace(/-/g, '.')
    const nodulePopoverBtn = this.state.nodulePopoverBtn
    let {age, gender, patientName, studyDate, studyTime} = this.state.study;
    let toolsArr=['wwwc', 'referenceLines','hideBoxes','grid','length','angle','freehand','eraser'/*'detect'*/]
    if(localStorage.getItem("fellowParams")){
      if(JSON.parse(localStorage.getItem("fellowParams")).state==='completed'){
          toolsArr.push("report")
      }
    }
console.log(toolsArr)
    return (
            <div style={{height: '100%', backgroundColor: '#000'}} id='ct-viwer-page'>
              <div style={{ position:"absolute",
                  width:"100%",
                  borderBottom:"1px solid #fff",
                  zIndex:"111111"}}>
                <Header>
                     <Logo/>
                     <Return  />
                     <div style={{float:"left"}}><ToolBar  tools={toolsArr} activeTool={this.state.activeTool} forbidEdit={false} onToolClick={this.onToolClick}/></div>
                    <SignoutButton/>
                    <span style={{color:"#fff",float:"right",marginRight:"10px",cursor:"pointer",marginTop:"1px"}} onClick={this.onClickModel}><img src={require('../../../static/images/help_nor.png')}/></span>
                </Header>
              </div>
              <div style={{width:"433px",overflowX:"hidden"}}>
                <div className='aside' style={{paddingTop:"67px",overflowY:"scroll"}}>
                    <Tabs defaultActiveKey='chest'
                          type="card"
                          activeKey={this.state.activeTabKey}
                          onChange={(activeTabKey) => this.setState({activeTabKey})}>
                        <TabPane tab={GetLocaledText(this, "CTViewerPage.lung")+ " "+(this.state.lungNodules.filter(l=>l.seriesID===this.state.targetSeriesID).length >99?"99+":this.state.lungNodules.filter(l=>l.seriesID===this.state.targetSeriesID).length)} key='chest'>
                            <NodulePanel data={this.state.lungNodules.filter(l=>l.seriesID===this.state.targetSeriesID)}
                                         ref={input => {this.nodulePanel = input}}
                                         onSelectNodule={(nodule) => {
                                             const {seriesID} = nodule
                                             if (!(seriesID in this.viewers)) {
                                                 return
                                             }
                                             this.viewers[seriesID].scrollTo('x', null, nodule.coord.x - 1)
                                             this.viewers[seriesID].scrollTo('y', null, nodule.coord.y - 1)
                                             this.viewers[seriesID].setState({HU: nodule.avgHU})
                                             return this.viewers[seriesID].scrollTo('z', null, nodule.coord.z - 1)
                                         }}
                                         renderDetail={null}
                                         maxZ={this.state.series[this.state.targetSeriesID].stacks.z.imageIds.length}
		               		 zReverse={this.state.study.reverse}
                                         hideSelection={!this.props.data.report}
                                         hideEditNoduleBtn={this.isForbidLabeling()}
                                         onAddScreenshot={this.saveScreenshotToReport}
                                         onRemoveScreenshot={this.removeScreenshotInReport}
                                         onAdd={this.onAddNoduleClick}
                                         onDelete={this.removeNodule}/>
                        </TabPane>
                        <TabPane tab={GetLocaledText(this, "CTViewerPage.liver")+" "+(this.state.liverTumors.length>99?"99+":this.state.liverTumors.length)} key='liver'>
                            <NodulePanelLiver data={this.state.liverTumors}
                                              ref={input => {this.nodulePanelLiver = input}}
                                              onSelectNodule={(tumor) => {
                                                  const {seriesID} = tumor
                                                  if (!(seriesID in this.viewers)) {
                                                      return
                                                  }
                                                  this.viewers[seriesID].scrollTo('x', null, tumor.coord.x - 1)
                                                  this.viewers[seriesID].scrollTo('y', null, tumor.coord.y - 1)
                                                  this.viewers[seriesID].setState({HU: tumor.avgHU,
                                                      imgPosZ: tumor.coord.z,
                                                      imgPosY: tumor.coord.y,
                                                      imgPosX: tumor.coord.x,
                                                  })
                                                  return this.viewers[seriesID].scrollTo('z', null, tumor.coord.z - 1)
                                              }}
                                              renderDetail={null}
                                              maxZ={this.state.series[this.state.targetSeriesID].stacks.z.imageIds.length}
					      zReverse={this.state.study.reverse}
				              hideSelection={!this.props.data.report}
                                              hideEditNoduleBtn={this.isForbidLabeling()}
                                              onAddScreenshot={this.saveScreenshotToReport}
                                              onRemoveScreenshot={this.removeScreenshotInReport}
                                              onAdd={this.onAddNoduleClick}
                                              onDelete={this.removeTumor}
                                              onTumorChanged = {this.updateTumor.bind(this)}
                            />
                        </TabPane>
                    </Tabs>
                </div>
                {
                    <div className='patient-info' style={{marginTop:"50px"}}>
                        <p><FormattedMessage id='CTViewerPage.patient-name'/>：{patientName}</p>
                        <p><FormattedMessage id='CTViewerPage.gender'/>：{gender==="MALE"?GetLocaledText(this,"CTViewerPage.gender.male"):GetLocaledText(this,"CTViewerPage.gender.female")}</p>
                        <p><FormattedMessage id='CTViewerPage.age'/>：{age}</p>
                        <p><FormattedMessage id='CTViewerPage.study-date'/>：{studyDate}</p>
                        <p><FormattedMessage id='CTViewerPage.study-time'/>：{studyTime}</p>
                    </div>
                }
              </div>
                <div className='content'  style={{paddingTop:"67px"}}>
                    {this.renderGrid()}
                    {
                        Object.keys(this.state.series).length > 1 ?
                            <ThumbnailList
                                activeIds={this.state.displaySeries.filter(s => !!s)}
                                data={Object.values(this.state.series).map(series => {
                                    return Object.assign({id: series.seriesID, stacks: series.stacks}, this.state.study)
                                })}
                                renderItem={study => <Snapshot study={study}/>}/>
                            :
                            ''
                    }
                </div>
                <AddNoduleModal
                    visible={this.state.addModalVisible}
                    status={this.state.addModalStatus}
                    handleOk={() => {
                        this.setState({
                            addModalStatus: 'adding'
                        })
                        this.analyzeNodule().finally(() => {
                            this.setState({
                                addModalStatus: 'finished',
                                addModalVisible: false
                            })
                        })
                    }}
                    handleCancel={() => this.setState({addModalVisible: false})}
                />
                {/*<RunDetectionModal
          visible={this.state.detectionModalVisible}
          status={this.state.detectionStatus}
          handleOk={()=>this.setState({detectionStatus:'detecting'})}
          handleCheck={(checkedValues)=>{
                       this.setState({detectionList:checkedValues})
                      }}
          handleCancel={()=>this.setState({detectionModalVisible:false})}
        />*/}
                <Button style={{
                    position: 'fixed',
                    top: nodulePopoverBtn.top,
                    left: nodulePopoverBtn.left,
                    display: !this.isForbidLabeling() && nodulePopoverBtn.show ? 'block' : 'none'
                }}
                        onClick={() => {
                            this.state.nodulePopoverBtn.action === 'add' ? this.onAddNoduleClick() : this.removeNodule(this.nodulePanel.getWrappedInstance().getSelectedNodule().labelID)
                            this.setState({
                                nodulePopoverBtn: Object.assign({}, this.state.nodulePopoverBtn, {show: false})
                            })
                        }}
                >
                    {nodulePopoverBtn.action === 'add' ? GetLocaledText(this, "CTViewerPage.add-label") : GetLocaledText(this, "CTViewerPage.remove-label")}
                </Button>
                <Modal
                    title={GetLocaledText(this, "HelpDocument")}
                    visible={this.state.visible}
                    onOk={this.handleOkModel}
                    onCancel={this.handleCancelModel}
                    width={840}
                    bodyStyle={{background:"#000",color:"#fff"}}
                    footer={null}
                >
                    <img src={require('../../../static/images/helpDocument.png')} style={{width:"794px"}} />
                </Modal>
            </div>
    )
  }

}

async function getData(props) {
  const user = getUser()
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let study
  let labels
  study = await api.getStudyDetail({studyUID})
  study.tumorLabels = []
  study.noduleLabels = []
  // study.noduleLabels = await api.getLabels({
  //   taskID:study.taskID,
  //   seriesID:Object.values(study.series)[0].seriesID,
  //   labelType:'Nodule'
  // })
  // study.tumorLabels = await api.getLabels({
  //   taskID:study.taskID,
  //   seriesID:Object.values(study.series)[0].seriesID,
  //   labelType:'LiverTumor'
  // })
  let tempStudyList = await api.getStudylist({})
  let tempThisStudy = tempStudyList.find(s=>s.studyUID === studyUID)
  let tempTaskID = tempThisStudy.taskID
  
  for(let seriesID in study.series)
  {
     if(study.noduleLabels)
     {     
       let labels = await api.getLabels({
           taskID:study.taskID,
           seriesID:seriesID,
           labelType:"Nodule"})    
       study.noduleLabels=[...labels,...study.noduleLabels]
     }else
     {
       study.noduleLabels = await api.getLabels({
           taskID:study.taskID,
           seriesID:seriesID,
           labelType:"Nodule"})
     }
  }
    
  if(!!study.noduleLabels && study.noduleLabels.length ===0)
  {
      for(let seriesID in study.series)
      {
        if(study.noduleLabels)
        {     
            let labels = await api.getLabels({
            taskID:tempTaskID,
            seriesID:seriesID,
            labelType:"Nodule"})    
            study.noduleLabels=[...labels,...study.noduleLabels]
        }else
        {
            study.noduleLabels = await api.getLabels({
              taskID:tempTaskID,
              seriesID:seriesID,
              labelType:"Nodule"})
        }
      }
    console.log("may be bug: taskID in study.series is:"+study.taskID+" found no nodules, taskID in studyList is"+tempTaskID)
  }
   
  for(let seriesID in study.series)
  {
     if(study.tumorLabels)
     {     
       let labels = await api.getLabels({
           taskID:study.taskID,
           seriesID:seriesID,
           labelType:"LiverTumor"})    
       study.tumorLabels=[...labels,...study.tumorLabels]
     }else
     {
       study.tumorLabels = await api.getLabels({
           taskID:study.taskID,
           seriesID:seriesID,
           labelType:"LiverTumor"})
     }
  }
    
  if(!!study.tumorLabels && study.tumorLabels.length===0)
  {
      for(let seriesID in study.series)
      {
        if(study.tumorLabels)
        {     
            let labels = await api.getLabels({
            taskID:tempTaskID,
            seriesID:seriesID,
            labelType:"LiverTumor"})    
            study.tumorLabels=[...labels,...study.tumorLabels]
        }else
        {
            study.tumorLabels = await api.getLabels({
              taskID:tempTaskID,
              seriesID:seriesID,
              labelType:"LiverTumor"})
        }
      }
    console.log("may be bug: taskID in study.series is:"+study.taskID+" found no tumors, taskID in studyList is"+tempTaskID)
  }

  addCTMetaData(study)
  Object.values(study.series).forEach(series => {
    series['stacks'] = {
      x: {
        currentImageIdIndex: 0,
        imageIds: series.instances.x.map(ins => ins.url)
      },
      z: {
        currentImageIdIndex: 0,
        imageIds: series.instances.z.map(ins => ins.url)
      },
      y: {
        currentImageIdIndex: 0,
        imageIds: series.instances.y.map(ins => ins.url)
      },
    }
  })

  let report 
  if(canOpenReport(study))
  {
    report = await api.getReport({taskID: study.taskID})
  }
  let data = {study, report}
  // get label task list
  console.log(data);
  return await data
}

const mapStateToProps = (state, ownProps) => {
  // console.log(state, ownProps);
  // NOTICE: use redux to control state
  return Object.assign({}, ownProps, state)
}
CTViewerPage = connect(mapStateToProps)(CTViewerPage)
CTViewerPage = LoadingWrapper(CTViewerPage, getData)
CTViewerPage = injectIntl(CTViewerPage)
export default CTViewerPage
