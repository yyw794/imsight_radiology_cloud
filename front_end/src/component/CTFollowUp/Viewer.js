import React, { Component } from 'react'
import * as cornerstone from 'cornerstone-core'
import * as cornerstoneWebImageLoader from "cornerstone-web-image-loader"
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import Hammer from "hammerjs"
import {Icon, message, Button, Tabs} from 'antd'

/**
 * antd-design组件
 * @type {React.ClassicComponentClass<TabPaneProps>}
 */
const TabPane = Tabs.TabPane
import Immutable from 'immutable'
import _ from 'underscore'
import { DropTarget } from 'react-dnd';

import noduleBox from '../tools/noduleBox'
import referenceLine2D from '../tools/referenceLine2D'
import {zoom} from '../tools/zoom'
import scaleTool from '../tools/scaleTool'
import scrollToIndex from '../tools/scrollToIndex'
import stackPreloader from '../tools/stackPreloader'
import itemTypes from '../dragDropItemTypes'
import {GetLocaledText} from '../../localedFuncs'
import {FormattedMessage,injectIntl} from 'react-intl'

/**
 * 当前视图
 * @type {{drop(*, *, *): void}}
 */
const viewerTarget = {
  drop(props, monitor, component) {
    const study = monitor.getItem()
    props.onChangeStudy(study)
    component.replaceStackData(study.stacks.z)
  }
};

/**
 *
 * @param connect
 * @param monitor
 * @returns {{connectDropTarget: ConnectDropTarget, isOver: boolean}}
 */
function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

/**
 * 随访视图组件
 */
class Viewer extends Component {
    /**
     * 参照react生命周期函数
     * @param props
     */
  constructor(props) {
    super(props)
      /**
       *
       * @type {{
       * position: {
       * x: number,坐标x值
       * y: number, 坐标值
       * z: number 坐标z值
       * },
       * viewport: null,
       * HU: number, 层数
       * minPixelValue: number, 最小像素值
       * maxPixelValue: number 最大像素值
       * }}
       */
    this.state = {
      position: {x: 0, y: 0, z: 0},
      viewport: null,
      HU: 0,
      minPixelValue:0,
      maxPixelValue:0
    }
    this.onKeyDown=this.onKeyDown.bind(this);
    this.commonFunction=this.commonFunction.bind(this);
  }

    /**
     *参照生命周期函数
     */
  componentDidMount() {
    let seriesID = 0
    for(var s in this.props.study.series)
    {
      seriesID = s
    }
    this.setState({
      minPixelValue:this.props.study.series[seriesID].minPixelValue,
      maxPixelValue:this.props.study.series[seriesID].maxPixelValue
    })
    this.initStack()
    .then(() => {
      // stackPreloader.enable(this.elem)
      this.initNoduleBoxTool()
      scaleTool.enable(this.elem)
      this.setWWWC(1500, -550)
    })
    document.addEventListener('keydown', this.onKeyDown, false)
    window.addEventListener("resize", _.debounce(this.onResize, 500))

  }

    /**
     *参照生命周期函数
     */
  componentWillUnmount() {
    window.removeEventListener("resize", this.onResize)
    document.removeEventListener('keydown', this.onKeyDown)
  }

    /**
     * 获取当前elem dom
     * @returns {string}
     */
  getElement() {
    return this.elem
  }

    /**
     *
     * @param axis
     * @param ratio
     * @returns {number}
     */
  fromRatioToIdx(axis, ratio) {
    let imgNum = this.props.study.stacks[axis].imageIds.length
    let idx = Math.min(Math.ceil(imgNum * ratio) - 1, imgNum - 1)
    return Math.max(idx, 0)
  }
 commonFunction(){
     let viewport = cornerstone.getViewport(this.elem)
     viewport.scale = this.baseScale
     viewport.translation = {x: 0, y: 0}
     viewport.voi.windowWidth = 1500
     viewport.voi.windowCenter = -550
     cornerstone.setViewport(this.elem, viewport)
 }

  onKeyDown(e){
      switch (e.key) {
          case 'Escape':
              this.commonFunction()
            break;
      }
      if (!!window.ActiveXObject || "ActiveXObject" in window) {
          if (e.key === 'Esc') {
              this.commonFunction()
          }
      }
  }
    /**
     * 设置位置
     * @param pos
     * @returns {Promise<T | never>}
     */
  setPosition(pos) {
    this.setState({
      position: pos
    })
    return scrollToIndex(this.elem, pos.z).then(() => cornerstone.updateImage(this.elem))
    /*
    this.setState({
      position: pos
    }, () => {
      scrollToIndex(this.elem, pos.z).then(() => cornerstone.updateImage(this.elem))
    })
    */
    // return scrollToIndex(this[`element${idx}`], pos.z).then(() => cornerstone.updateImage(this[`element${idx}`]))
  }

    /**
     * 初始化结节工具
     */
  initNoduleBoxTool = () => {
    const stack = this.props.study.stacks
    const imgNumX = stack.x.imageIds.length
    const imgNumY = stack.y.imageIds.length
    const imgNumZ = stack.z.imageIds.length
    const element = this.elem
    noduleBox.enable(element, {
      axis: 'z',
      depth: 2, // in layers
      horizontalLayers: imgNumX,
      verticalLayers: imgNumY,
      mainAxisLayers: imgNumZ,
      getData: () => this.props.labels,
      getCurrentIdx: () => {
        return this.state.position
      },
      onClick: (e) => {
        let ratio = e.currentPoints.ratio
        let clickedNodules = e.clickedNodules
        let coordImage = e.currentPoints.image
        if (clickedNodules.length > 0) {
          let nodule = clickedNodules[0]
          this.props.onNoduleClick(nodule)
        }
        let pos = Object.assign({}, this.state.position)
        pos.x = this.fromRatioToIdx('x', ratio.x)
        pos.y = this.fromRatioToIdx('y', ratio.y)
        this.setPosition(pos)
        referenceLine2D.setConfiguration({intersection: coordImage})
        this.setState({
          HU: this.getPixelInCurrentPosition()
        })
      }
    })
    noduleBox.activate(element, 1)
  }

    /**
     * 初始化随访视图
     * @returns {Promise}
     */
  initStack() {
    const {study} = this.props
    let element = this.elem
    const zStack = study.stacks.z
    cornerstone.enable(element)
    // Enable mouse inputs
    cornerstoneTools.mouseInput.enable(element)
    cornerstoneTools.mouseWheelInput.enable(element)

    return cornerstone.loadImage(zStack.imageIds[0])
      .then((image) => {
        cornerstone.displayImage(element, image)
        // set the stack as tool state
        // cornerstoneTools.addStackStateManager(element, ['stack', 'referenceLines'])
        cornerstoneTools.addStackStateManager(element, ['stack'])
        cornerstoneTools.addToolState(element, 'stack', zStack)
        // Enable all tools we want to use with this element
        cornerstoneTools.pan.activate(element, 1)
        zoom.activate(element, 4)
        cornerstoneTools.stackScrollWheel.activate(element)

        element.addEventListener("cornerstonenewimage", (e) => {
          const stackData = cornerstoneTools.getToolState(element, 'stack')
          const z = stackData.data[0].currentImageIdIndex
          this.setState({
            position: Object.assign({}, this.state.position, {z}),
          })
          cornerstoneTools.addToolState(element, 'zoom', {baseScale: this.baseScale})
        })

        // baseScale
        let baseScale = cornerstone.getViewport(element).scale
        cornerstoneTools.addToolState(element, 'zoom', {baseScale})
          /**
           * 缩放比
           */
        this.baseScale = baseScale
        element.addEventListener('cornerstoneimagerendered', () => {
          this.setState({
            viewport: cornerstone.getViewport(element)
          })
        })

        // synchronizer.add(element)
      })
  }

    /**
     * 切换视图
     * @param stackData
     * @returns {Object}
     */
  replaceStackData(stackData) {
    const element = this.elem
    return cornerstone.loadImage(stackData.imageIds[0])
      .then((image) => {
        cornerstone.displayImage(element, image)
        cornerstoneTools.clearToolState(element, 'stack')
        cornerstoneTools.addToolState(element, 'stack', stackData)
        cornerstone.reset(element)
        let viewport = cornerstone.getViewport(element)
        let baseScale = viewport.scale
        this.baseScale = baseScale
        cornerstoneTools.removeToolState(element, 'zoom')
        cornerstoneTools.addToolState(element, 'zoom', {baseScale})
        viewport.voi.windowWidth = 80
        viewport.voi.windowCenter = 28
        cornerstone.updateImage(element)
      })
  }

    /**
     * 全屏切换
     */
  onResize = () => {
    if (this.props.hide) {return}
    cornerstone.resize(this.elem, true)
  }

    /**
     *
     * @returns {*}
     */
    getPixelInCurrentPosition() {
        let {height, width, columns, getPixelData} = cornerstone.getImage(this.element)
        const imgNumX = this.props.series.stacks.x.imageIds.length
        const imgNumY = this.props.series.stacks.y.imageIds.length
        const imgNumZ = this.props.series.stacks.z.imageIds.length
        let x = this.state.imgPosX / imgNumX * width
        let y = this.state.imgPosY / imgNumY * height
        let pixelData = getPixelData()
        let idx = (Math.round(x) + Math.round(y) * columns)
        return pixelData[idx]
    }
    /**
     * 调窗
     * @param windowWidth
     * @param windowCenter
     */
  setWWWC(ww,wc) {
    let elem = this.elem
    let viewport = cornerstone.getViewport(elem)
    viewport.voi.windowWidth = ww
    viewport.voi.windowCenter = wc
    cornerstone.updateImage(elem)
  }

    /**
     * 参照react周期函数
     * @returns {Component}
     */
  render() {
    const {study, title, fullScreen, hide} = this.props
    const { connectDropTarget, isOver, active } = this.props;
    let className = `view ${this.props.className}`
    if (fullScreen) {className += ' full-screen'}
    if (hide) {className += ' hide'}
    if (isOver || active) {className += ' active'}
    return connectDropTarget(
      <div className={className}>
        <p className='view-title'>{title}</p>
            /**
             * 视图元素
             */
        <div ref={input => {this.elem = input}} className='cornerstone-elm'  onDoubleClick={() => {
            this.props.toggleFullScreen().then(this.onResize)
        }}>
          <canvas className="cornerstone-canvas"
            onContextMenu={e => e.preventDefault()}/>
          <div className='info-box'>
            <p>HU: {this.state.HU}</p>
            <p>WW: {
                this.state.viewport ?
                Math.round(this.state.viewport.voi.windowWidth) : 0}</p>
            <p>WL: {this.state.viewport ?
                Math.round(this.state.viewport.voi.windowCenter) : 0}</p>
          </div>
        </div>
        <div className='view-footer'>
            {/*<Button ghost onClick={() => {*/}
             {/*let viewport = cornerstone.getViewport(this.elem)*/}
             {/*viewport.scale = this.baseScale*/}
             {/*viewport.translation = {x: 0, y: 0}*/}
             {/*cornerstone.setViewport(this.elem, viewport)*/}
             {/*// cornerstone.reset(this.elem)*/}
        {/* }}><FormattedMessage id="CTFollowUp.viewer.autoFit"/></Button>*/}
        <span className='img-pos'>{study.reverse?(study.stacks.z.imageIds.length-this.state.position.z):(this.state.position.z+1)} / {study.stacks.z.imageIds.length}</span>
        </div>
        <div className='patient-info'>
          <p><FormattedMessage id="CTFollowUp.viewer.id"/>{study.patientID}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.name"/>{study.patientName}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.gender"/>{study.gender}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.age"/>{study.age}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.studyDate"/>{study.studyDate}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.studyTime"/>{study.studyTime}</p>
        </div>
      </div>
    );
  }

}

export default DropTarget(itemTypes.CT_STUDY, viewerTarget, collect)(Viewer);
