#!/bin/python
#coding=utf-8
from flask import make_response, request, jsonify, abort
from flask_login import current_user
import os, sys, six, thread
import numpy as np
import SimpleITK as sitk
from radiomics import featureextractor
from scipy.ndimage.morphology import binary_fill_holes

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_find, db_update_one, db_insert_one,db_find_update_otherwise_insert
from models.response.response import response
from config import DICOM_ROOT, RADIOMICS_PARAMS, RADIOMICS_HEADER
from models.radiomics.Radiomics import Radiomics
import logging
logger = logging.getLogger("back_end.algo.on_radiomics")

def calRadiomics(image, label_list):
    image_array = sitk.GetArrayFromImage(image)
    image_array = image_array.transpose(2, 1, 0)

    result_list = []
    for label in label_list:
        mask_contour = np.zeros_like(image_array)
        fill_mask = np.zeros_like(image_array)

        for slice in label['nodule_slice']:
            z_pixel = slice['z']
            for point in slice['contour']:
                x_edge = np.int(point[0])
                y_edge = np.int(point[1])
                mask_contour[x_edge, y_edge, z_pixel] = 1
            fill_mask[:, :, z_pixel] = binary_fill_holes(mask_contour[:, :, z_pixel])

        x_center = label['x']
        y_center = label['y']
        z_center = label['z']

        fill_mask = np.asarray(fill_mask, dtype=np.int16)
        mask = sitk.GetImageFromArray(fill_mask)

        extractor = featureextractor.RadiomicsFeaturesExtractor(RADIOMICS_PARAMS)
        result = extractor.execute(sitk.GetImageFromArray(image_array), mask)

        feature_names = ['x_center', 'y_center', 'z_center']
        features = [x_center, y_center, z_center]
        for k, v in six.iteritems(result):
            if k.startswith('original_'):
                feature_names.append(k.strip('original_'))
                features.append(v)

        result_list.append(dict(zip(feature_names, features)))
    return result_list

def thread_create_radiomics(label_list,studyUID,labelType,taskID):
    seriesID = label_list[0]['seriesID']
    dcm_folder_path = os.path.join(DICOM_ROOT, seriesID)

    # find existed radiomices info
    radiomics_info = db_find_one("radiomics", {"taskID": taskID, 'seriesID': seriesID})
    old_data_list = []
    if radiomics_info is not None:
        old_data_list = radiomics_info['data_list']
        old_label_list = [(d['x_center'], d['y_center'], d['z_center']) for d in old_data_list]
        new_label_list = [(l['x'], l['y'], l['z']) for l in label_list]
        # filter new add label when it not in old data list
        label_list = filter(lambda l: (l['x'], l['y'], l['z']) not in old_label_list, label_list)
        # filter old data list when label in new label list
        old_data_list = filter(lambda l:(l['x_center'], l['y_center'], l['z_center']) in new_label_list, old_data_list)

    radiomics = Radiomics()
    radiomics.taskID = taskID
    radiomics.studyUID = studyUID
    radiomics.seriesID = seriesID
    radiomics.data_list.extend(old_data_list)
    result_state = ""
    try:
        series_names = sitk.ImageSeriesReader.GetGDCMSeriesFileNames(str(dcm_folder_path))
        series_reader = sitk.ImageSeriesReader()
        series_reader.SetFileNames(series_names)
        image = series_reader.Execute()
        if len(label_list) > 0:
            result_list = calRadiomics(image, label_list)
            radiomics.data_list.extend(result_list)
        result_state = "success"
    except Exception as e:
        logger.info('create radiomics error')
        logger.info(e)
        for label in label_list:
            f = open(RADIOMICS_HEADER, 'r')
            lines = f.readlines()
            radiomics_head = [l.strip() for l in lines]
            data_temp = dict(zip(radiomics_head, [0]*len(radiomics_head)))
            data_temp['x_center'] = label['x']
            data_temp['y_center'] = label['y']
            data_temp['z_center'] = label['z']
            result_state = "fail"
            radiomics.data_list.append(data_temp)
    db_find_update_otherwise_insert("radiomics", {"taskID": radiomics.taskID,'seriesID':radiomics.seriesID}, radiomics.to_db())
    logger.info("radiomics update "+result_state)

def create_radiomics(label_list,studyUID,labelType,taskID):
    if labelType != 'Nodule': return
    if label_list == None: return
    if len(label_list) == 0:return
    thread.start_new_thread(thread_create_radiomics, (label_list,studyUID,labelType,taskID))
    
required_key_all = ['taskID','seriesID']
def fetch_radiomics():
    if not current_user.is_authenticated:
        abort(401)
        
    res = response()

    params = request.args.to_dict() if request.method == 'GET' else request.json

    if not all(key in params for key in required_key_all):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
        
    taskID = params['taskID']
    seriesID = params['seriesID']
    
    radiomics_list = db_find("radiomics", {"taskID": taskID,'seriesID':seriesID})
    if radiomics_list is None: radiomics_list = []
        
    if request.method == "GET":
        res.statusCode = 1000
        res.data = radiomics_list
        return make_response(jsonify(res.to_dict()), 200)

if __name__ == "__main__":
    image_path = ""
    label_list = []
    calRadiomics(image_path, label_list)
