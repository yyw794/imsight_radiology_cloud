
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './LayoutChooser.less';
import classNames from 'classnames';

export default class LayoutChooser extends Component {

    constructor(props) {
        super(props)

        this.state = {
            currentHoverRow: 0,
            currentHoverCol: 0,
        }
    }

    highlightCells = (currentCell) => {
        var cells = $('.layoutChooser table td');
        cells.removeClass('hover');
    
        currentCell = $(currentCell);
        var table = currentCell.parents('.layoutChooser table').get(0);
        var rowIndex = currentCell.closest('tr').index();
        var columnIndex = currentCell.index();
    
        // Loop through the table row by row
        // and cell by cell to apply the highlighting
        for (var i = 0; i < table.rows.length; i++) {
            row = table.rows[i];
            if (i <= rowIndex) {
               for (var j = 0; j < row.cells.length; j++) {
                    if (j <= columnIndex) {
                        cell = row.cells[j];
                        cell.classList.add('hover');
                    }
               }
            }
        }   
    }

    render() {
        let rows = this.props.rows;
        let columns = this.props.columns;
        let layoutChooserClasses = classNames({
            'displayNone': this.props.isDisplayNone ? false : true,
            'layoutChooser': true
        });

        let currentState = this.state
        
        const setLayoutChooserState = (state) => {
            this.setState(state)
        }

        const onChooseNewLayout = (cols, rows) => {
            this.props.onLayoutSelected(cols, rows)
        }

        return (
            <div className={layoutChooserClasses} role="menu" style={{width: '75px', height: '75px'}}>
                <table id='layout-cooser-table'>
                    <tbody>
                    {[...Array(rows)].map(function(row, i) {
                        return (
                            <tr key={i}>
                                {[...Array(columns)].map(function(column, j) {
                                    // create your own td react component here
                                    return <td 
                                            col={j}
                                            row={i}
                                            style={{width: '20px',
                                                    height: '20px',
                                                    margin: '2px',
                                                    border: 'solid 1px black',
                                                    backgroundColor: (j <= currentState.currentHoverCol &&
                                                        i <= currentState.currentHoverRow) ? 'yellow' : '#3399ff'  ,
                                                    }}
                                            key={j}
                                            onMouseEnter={(e) => {
                                                setLayoutChooserState({
                                                    currentHoverRow: i,
                                                    currentHoverCol: j,
                                                })
                                                e.target.style.backgroundColor = (j <= currentState.currentHoverCol ||
                                                    i <= currentState.currentHoverRow) ? 'yellow' : '#3399ff' 
                                            }}
                                            onClick={(e) => {
                                                onChooseNewLayout(j+1,i+1)
                                                }
                                            }
                                            ></td>;
                                })}
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        )
    }
}

LayoutChooser.propTypes = {
    rows: PropTypes.number.isRequired,
    columns: PropTypes.number.isRequired
};

LayoutChooser.defaultProps = {
    rows: 4,
    columns: 4
};