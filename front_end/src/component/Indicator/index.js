import React, { Component } from 'react'
import {EVENTS} from 'cornerstone-tools'
import * as cornerstone from 'cornerstone-core'
import * as cornerstoneWebImageLoader from "cornerstone-web-image-loader"
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import _ from 'underscore'

import './index.less'

cornerstoneTools.external.cornerstone = cornerstone
cornerstoneTools.cornerstoneMath = cornerstoneMath
cornerstoneWebImageLoader.external.cornerstone = cornerstone
cornerstoneTools.external.Hammer = Hammer

class Indicator extends Component {

  componentWillReceiveProps(nextProps) {
      if(this.element&&this.state.showWidth){
          this.element.children[0].setAttribute("width",148);
          this.element.children[0].setAttribute("height",148);
          this.element.children[0].style.width="148px"
          this.element.children[0].style.height="148px"
          this.setState({
              showWidth:false
          })
      }
    nextProps.image && cornerstone.displayImage(this.element, nextProps.image)
    nextProps.image && cornerstone.updateImage(this.element)
  }
  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   this.props.image && cornerstone.displayImage(this.element, this.props.image)
  //   this.props.image && cornerstone.updateImage(this.element)
  // }
    constructor(props){
      super(props)
      this.state={
          showWidth:true,
      }
  }
  componentDidMount() {
    cornerstone.enable(this.element)
    this.props.image && cornerstone.displayImage(this.element, this.props.image)
    cornerstoneTools.mouseInput.enable(this.element)
    this.element.addEventListener(EVENTS.IMAGE_RENDERED, this.onImageRendered)
    this.element.addEventListener(EVENTS.MOUSE_DOWN, this.onMouseDown)
    this.element.addEventListener(EVENTS.MOUSE_DRAG, this.onMouseDrag)
  }

  onMouseDown = (e) => {
    console.log('down');
    this.oldTranslation = Object.assign(this.props.viewport.translation)
  }

  onMouseDrag = (e) => {
    const eventData = e.detail
    const coords = eventData.currentPoints.image
    if (!eventData.startPoints) return
    const startPoint = eventData.startPoints.image
    const currentPoint = eventData.currentPoints.image
    let offset = {x: currentPoint.x - startPoint.x, y: currentPoint.y - startPoint.y}
    let translation = {
      x: this.oldTranslation.x - offset.x,
      y: this.oldTranslation.y - offset.y,
    }
    _.isFunction(this.props.onMouseDrag) && this.props.onMouseDrag(translation)
    // cornerstone.updateImage(eventData.element)
  }

  onImageRendered = (e) => {
    if (!this.props.viewport) {
      return
    }
    // console.log(this.props.viewport);
    const eventData = e.detail;
    const element = eventData.element
    const {width, height} = this.props.image
    const context = eventData.canvasContext.canvas.getContext('2d');
    const viewport = cornerstone.getViewport(element)
    const srcViewport = this.props.viewport
    context.strokeStyle = 'yellow';
    context.lineWidth = 1;
    context.fillStyle = 'rgba(255, 255, 0, 0.2)'
    context.setTransform(1, 0, 0, 1, 0, 0);
    context.beginPath();
    // let coord = {x: -srcViewport.translation.x, y: -srcViewport.translation.y}
    let coord = {x: width / 2 - srcViewport.translation.x, y: height / 2 - srcViewport.translation.y}
    let scale = srcViewport.scale / this.props.baseScale // scale of viewer
    // coord.x *= scale
    // coord.y *= scale
    if(height===660){
        viewport.scale=0.22424242424242424;
    }
    coord = cornerstone.pixelToCanvas(eventData.element, coord)
    let canvasW = width * viewport.scale / scale
    let canvasH = height * viewport.scale / scale
    context.rect(coord.x - canvasW / 2, coord.y - canvasH / 2, canvasW, canvasH);
    context.fill()
    context.stroke();
  }

  render() {
    if(this.element&&this.state.showWidth){
          this.element.children[0].setAttribute("width",148);
          this.element.children[0].setAttribute("height",148);
          this.element.children[0].style.width="148px"
          this.element.children[0].style.height="148px"
          this.setState({
              showWidth:false
          })
      }
    const hide = !this.props.viewport || (this.props.viewport.scale / this.props.baseScale < 1.5)
    const style = hide ? {zIndex: -1} : {}
    return (
      <div ref={input => {this.element = input}} className='indicator' style={style}>
        <canvas className="cornerstone-canvas"
          onContextMenu={e => e.preventDefault()}/>
      </div>
    )
  }

}

export default Indicator
