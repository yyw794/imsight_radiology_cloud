#!/usr/bin/env python
# -*- coding: utf-8 -*-
import hashlib
import os
import sys
import time

from flask import make_response, request, jsonify, Flask
from flask_mail import Message, Mail

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_insert_one, db_update_one
from models.response.response import response
from models.user.User import parseUserInfo
from config import MAIL_VERIFY_REQUIRE, LABEL_PLATFORM, MAIL_SERVER, MAIL_PORT, MAIL_USE_SSL, MAIL_USERNAME, \
    MAIL_PASSWORD, MAIL_DEFAULT_SENDER

app = Flask(__name__)
# email
app.config['MAIL_SERVER'] = MAIL_SERVER
app.config['MAIL_PORT'] = MAIL_PORT
app.config['MAIL_USE_SSL'] = MAIL_USE_SSL
# app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = MAIL_USERNAME
app.config['MAIL_PASSWORD'] = MAIL_PASSWORD
app.config['MAIL_DEFAULT_SENDER'] = MAIL_DEFAULT_SENDER
mail = Mail(app)


def send_confirm_email(to, account, token):
    html = u'''
    <html>

    <head>
    <title>验证邮箱</title>
    </head>

    <body>
    <p>您的账号已经注册成功，请复制以下链接到浏览器打开完成邮箱验证</p>
    <p>http://%s/auth/register/confirm?account=%s&token=%s</p>
    </body>

    </html>
    ''' % (HOST, account, token)
    # print html.encode('utf-8')
    msg = Message(
        '确认您的邮箱-视见科技',
        recipients=[to],
        html=html,
        sender=app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)


def confirm_email_handler():
    """
    @api {get} /auth/register/confirm  邮箱确认
    @apiVersion 0.0.1
    @apiGroup Auth
    @apiParam {String} account 用户名
    @apiParam {String} token 令牌
    @apiParamExample {json} Request-Example:
    {"account":"example","token":"b32d015a90fe436ba742127cb1cce701"}
    @apiSuccessExample {string} Success-Response:
    verification success
    @apiErrorExample {string} Error-Response:
    verification fail
    """
    account = request.args.get('account')
    token = request.args.get('token')

    ret = db_find_one("user", {"account": account})
    if ret is None or ret['confirmEmail'] != token:
        return make_response('verification fail')

    db_update_one('user', {"account": account}, {'confirmEmail': 'true'})
    return make_response('verification success')


required_key = [u'account', u'password', u'nickname', u'email', u'hospital']


def register_handler():
    """
    @api {post} /auth/register  注册用户
    @apiVersion 0.0.1
    @apiGroup Auth
    @apiParam {String} account 用户名
    @apiParam {String} password 密码
    @apiParam {String} nickname 用户昵称
    @apiParam {String} email 用户邮箱
    @apiParam {String} usergroup 用户角色
    @apiParam {String} hospital 医院名称
    @apiParam {String} [contact] 联系方式
    @apiParamExample {json} Request-Example:
    {"account":"example","password":"example","nickname":"example","email":"example@imsightmed.com",
    "usergroup":"reporter","hospital":"example", "contact":"11111"}
    @apiSuccessExample {json} Success-Response:
    {
      "data": {
        "account": "example",
        "contact": "",
        "email": "example@imsightmed.com",
        "hospital": "example",
        "nickname": "example",
        "type": "normal"
      },
      "message": "OK",
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "message": "",
      "statusCode": 1103
    }
    """
    res = response()
    params = request.get_json()

    if not all(key in params for key in required_key):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    ret = db_find_one("user", {"account": params['account']})
    if ret is not None:
        res.statusCode = 1106
        return make_response(jsonify(res.to_dict()), 200)

    ret = db_find_one("user", {"email": params['email']})
    if ret is not None:
        res.statusCode = 1107
        return make_response(jsonify(res.to_dict()), 200)

    params['password'] = hashlib.sha256(params['password']).hexdigest()
    md5 = hashlib.md5()
    md5.update(params['email'] + str(time.time()))
    params['confirmEmail'] = md5.hexdigest()

    if MAIL_VERIFY_REQUIRE:
        try:
            send_confirm_email(params['email'], params['account'], params['confirmEmail'])
        except:
            res.statusCode = 1111
            return make_response(jsonify(res.to_dict()), 200)

    new_user = parseUserInfo(params)
    if 'contact' in params:
        new_user.contact = params['contact']
    new_user.type = 'label' if LABEL_PLATFORM else 'normal'
    db_insert_one('user', new_user.to_db())
    if params.get('usergroup') == 'reporter':
        db_insert_one('liability', {"reporter": new_user.id, "reviewer": 'imsight'})
    else:
        db_insert_one('liability', {"reporter": 'imsight', "reviewer": new_user.id})
    db_insert_one('ownership', {"userID": new_user.id, "shared": {}})

    res.statusCode = 1000
    res.data = new_user.to_dict()
    return make_response(jsonify(res.to_dict()), 200)


def check_account():
    """
    @api {get} /auth/register/check_account  用户名检查
    @apiVersion 0.0.1
    @apiGroup Auth
    @apiParam {String} account 用户名
    @apiParamExample {json} Request-Example:
                 {"account":"example"}
    @apiSuccessExample {json} Success-Response:
    {
      "data": {
        "account": "example",
        "contact": "example",
        "email": "example",
        "hospital": "example",
        "nickname": "example",
        "type": "normal"
      },
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "data": {},
      "statusCode": 1107
    }
    """
    res = response()
    account = request.args.get('account')
    ret = db_find_one("user", {"account": account})
    res.statusCode = 1107 if ret is not None else 1000
    return make_response(jsonify(res.to_dict()), 200)


def check_email():
    """
    @api {get} /auth/register/check_email  邮箱检查
    @apiVersion 0.0.1
    @apiGroup Auth
    @apiParam {String} email 邮箱
    @apiParamExample {json} Request-Example:
                 {"email":"example@imsightmed.com"}
    @apiSuccessExample {json} Success-Response:
    {
      "data": {
        "account": "example",
        "contact": "example",
        "email": "example",
        "hospital": "example",
        "nickname": "example",
        "type": "normal"
      },
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "data": {},
      "statusCode": 1107
    }
    """
    res = response()
    email = request.args.get('email')
    ret = db_find_one("user", {"email": email})
    res.statusCode = 1107 if ret is not None else 1000
    return make_response(jsonify(res.to_dict()), 200)
