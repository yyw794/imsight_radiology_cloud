#!/bin/python
from Label import Label, parseLabelist
from DX_Label import DX_Label, parseDXLabelist
from Liver_Label import Liver_Label, parseLiverLabelist
from Nodule_Label import Nodule_Label, parseNoduleLabelist
import uuid

typelist = ['General', 'DX', 'Nodule', 'LiverTumor']
required_key_ALL = ["labelType"]
create_requied_key_label = ['seriesID', 'instanceID', 'globalTag', 'diseaseTag']
create_requied_key_dx_label = ['seriesID', 'instanceID', 'globalTag', 'diseaseTag']
create_optional_key_dx_label = ['coord', 'width', 'height']
create_requied_key_nodule_label = ["seriesID", "coord"]
create_requied_key_liver_label = ['coord']

def updateLabel(info, state='assign', isCreate=True):
    if type(info) is not dict:
        raise ValueError("createLabel input must be dict type, while type is {}".format(type(info)))

    if not all(key in info for key in required_key_ALL):
        raise ValueError("label db must contain {}, while input contatins {}".format(required_key_ALL, info.keys()))

    labelType = info['labelType']

    if labelType not in typelist:
        raise ValueError("createLabel accept labelType: {}, intput is {}".format(typelist, labelType))

    if labelType == 'General':
        if not all(key in info for key in create_requied_key_label):
            return None
        label = Label()
        label.labelID = str(uuid.uuid1()) if isCreate else info['labelID']
        label.seriesID = info['seriesID']
        label.instanceID = info['instanceID']
        label.globalTag = info['globalTag']
        label.diseaseTag = info['diseaseTag']
        label.state = state
        return label

    elif labelType == 'DX':
        if not all(key in info for key in create_requied_key_label):
            raise ValueError("DX must contain create_requied_key_label: {}, intput is {}".format(create_requied_key_label, info))
        label = DX_Label()
        label.labelID = str(uuid.uuid1()) if isCreate else info['labelID']
        label.seriesID = info['seriesID']
        label.instanceID = info['instanceID']
        label.globalTag = True
        label.diseaseTag = info['diseaseTag']
        label.state = state
        if all(key in info for key in create_optional_key_dx_label):
            label.globalTag = False
            label.x = info['coord']['x']
            label.y = info['coord']['y']
            label.w = info['width']
            label.h = info['height']

        return label

    elif labelType == 'Nodule':
        if not all(key in info for key in create_requied_key_nodule_label):
            return None
        label = Nodule_Label()
        label.labelID = str(uuid.uuid1()) if isCreate else info['labelID']
        label.seriesID = info['seriesID']
        label.state = state
        label.x = info['coord']['x']
        label.y = info['coord']['y']
        label.z = info['coord']['z']
        label.addByUser = True
        if not isCreate:
            label.nodule_prob = info['nodule_prob']
            label.nodule_type = info['nodule_type']
            label.nodule_type_prob = info['nodule_type_prob']
            label.nodule_bm = info['nodule_bm']
            label.nodule_bm_prob = info['nodule_bm_prob']
            label.nodule_volume = info['nodule_volume']
            label.nodule_diameter = info['nodule_diameter']
            label.nodule_avgHU = info['nodule_avgHU']
            label.nodule_location = info['nodule_location']
            label.url = info['url']
            label.addByUser = info['addByUser']

        return label

    elif labelType == 'LiverTumor':
        if not all(key in info for key in create_requied_key_liver_label):
            return None
        label = Liver_Label()
        label.labelID = str(uuid.uuid1()) if isCreate else info['labelID']
        label.seriesID = info['seriesID']
        label.state = state
        label.x = info['coord']['x']
        label.y = info['coord']['y']
        label.z = info['coord']['z']
        label.height = info['height']
        label.width = info['width']
        label.volume = info['volume']
        label.diameter = info['diameter']
        label.p1_x = info['p1']['x']
        label.p1_y = info['p1']['y']
        label.p2_x = info['p2']['x']
        label.p2_y = info['p2']['y']
        label.avgHU = info['avgHU']
        label.isAPE = info['isAPE']
        label.mfsEnhancing = info['mfsEnhancing']
        label.mfsThresholdGrowth = info['mfsThresholdGrowth']
        label.mfsWashout = info['mfsWashout']
        label.LI_RADS = info['LI_RADS']
        label.url = info['url']
        label.malg = info['malg']
        label.seriesNumber = info['seriesNumber']
        return label


def parseLabelInfo(info):
    if type(info) is not dict:
        raise ValueError("parseLabelInfo input must be dict type, while type is {}".format(type(info)))

    if not all(key in info for key in required_key_ALL):
        raise ValueError("label db must contain {}, while input contatins {}".format(required_key, info.keys()))

    studyUID = info['studyUID']
    taskID = info['taskID']
    labels = info['labels']
    labelType = info['labelType']
    labelist = []

    if labelType not in typelist:
        raise ValueError("parseLabelInfo accept labelType: {}, intput is {}".format(typelist, labelType))

    if labelType == 'General':
        return parseLabelist(labels)

    elif labelType == 'DX':
        return parseDXLabelist(labels)

    elif labelType == 'Nodule':
        return parseNoduleLabelist(labels)

    elif labelType == 'LiverTumor':
        return parseLiverLabelist(labels)


def saveLabelist(labelist, studyUID, taskID, labelType):
    if type(labelist) is not list:
        raise ValueError("saveDXLabelist input must be list type, while type is {}".format(type(labelist)))

    if labelType not in typelist:
        raise ValueError("parseLabelInfo accept labelType: {}, intput is {}".format(typelist, labelType))

    ret = {"studyUID": studyUID, "taskID": taskID, "labels": [], "labelType": labelType}

    for label in labelist:
        if labelType == 'General' and not isinstance(label, Label):
            raise ValueError("label is NOT label {}".format(type(label)))

        elif labelType == 'DX' and not isinstance(label, DX_Label):
            raise ValueError("label is NOT DX label {}".format(type(label)))

        elif labelType == 'Nodule' and not isinstance(label, Nodule_Label):
            raise ValueError("label is NOT Nodule label {}".format(type(label)))

        elif labelType == 'LiverTumor' and not isinstance(label, Liver_Label):
            raise ValueError("label is NOT LiverTumor label {}".format(type(label)))

        ret['labels'].append(label.to_db())

    return ret
