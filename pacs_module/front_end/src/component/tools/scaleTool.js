import {
  displayTool,
  EVENTS,
  drawTextBox,
  external
} from 'cornerstone-tools';

function onImageRendered (e) {
  const eventData = e.detail;

  const context = eventData.canvasContext.canvas.getContext('2d');
  const { image, viewport } = eventData;
  const cornerstone = external.cornerstone;

  const imagePlane = cornerstone.metaData.get('imagePlaneModule', image.imageId);
  let rowPixelSpacing;
  let colPixelSpacing;

  if (imagePlane) {
    rowPixelSpacing = imagePlane.rowPixelSpacing || imagePlane.rowImagePixelSpacing;
    colPixelSpacing = imagePlane.columnPixelSpacing || imagePlane.colImagePixelSpacing;
  } else {
    rowPixelSpacing = image.rowPixelSpacing;
    colPixelSpacing = image.columnPixelSpacing;
  }

  // Check whether pixel spacing is defined
  if (!rowPixelSpacing || !colPixelSpacing) {
    return;
  }

  const canvasSize = {
    width: context.canvas.width,
    height: context.canvas.height
  };
  const imageSize = {
    width: image.width,
    height: image.height
  };

  const lengthInPixels = 100

  // Distance between intervals is 10mm
  const length = Math.round(lengthInPixels * rowPixelSpacing / viewport.scale)

  context.save();
  // const color = 'white'
  const color = '#d19a66'
  const bottom = 55
  const right = 80
  context.setTransform(1, 0, 0, 1, 0, 0);
  context.strokeStyle = color;
  context.lineWidth = 2;
  context.setLineDash([]);
  context.beginPath();
  context.moveTo(canvasSize.width - right - lengthInPixels, canvasSize.height - bottom - 5)
  context.lineTo(canvasSize.width - right - lengthInPixels, canvasSize.height - bottom)
  context.lineTo(canvasSize.width - right, canvasSize.height - bottom)
  context.lineTo(canvasSize.width - right, canvasSize.height - bottom - 5)
  drawTextBox(context, `${length}mm`, canvasSize.width - right - 70, canvasSize.height - bottom - 25, color)
  context.stroke()
  context.restore();
}

function disable (element) {
  // TODO: displayTool does not have cornerstone.updateImage(element) method to hide tool
  element.removeEventListener(EVENTS.IMAGE_RENDERED, onImageRendered);
  external.cornerstone.updateImage(element);
}

// Module exports
const scaleTool = displayTool(onImageRendered);

scaleTool.disable = disable;

export default scaleTool;
