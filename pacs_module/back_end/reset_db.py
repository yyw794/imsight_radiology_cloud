#!/bin/python

from pymongo import MongoClient
from dummy.ai_label_db import reset_ai_label
from dummy.task_db import reset_task
from dummy.label_db import reset_label
from dummy.liability_db import reset_liability
from dummy.ownership_db import reset_ownership
from dummy.report_db import reset_report
from dummy.resource_db import reset_resource
from dummy.study_db import reset_study
from dummy.template_db import reset_template
from dummy.tool_db import reset_tool
from dummy.user_db import reset_user
from config import MONGO_NAME, MONGO_USERNAME, MONGO_PASSWORD, MONGO_AUTHSOURCE, MONGO_DATABASE_NAME

#client = MongoClient()
client = MongoClient(MONGO_NAME, username=MONGO_USERNAME, password=MONGO_PASSWORD, authSource=MONGO_AUTHSOURCE)
db = client['radiology']

reset_user(db)
print("reset user db")

reset_tool(db)
print("reset tool db")

reset_template(db)
print("reset template db")

reset_study(db)
print("reset study db")

reset_resource(db)
print("reset resource db")

reset_report(db)
print("reset report db")

reset_ownership(db)
print("reset ownership db")

reset_liability(db)
print("reset liability db")

reset_task(db)
print("reset task db")

reset_label(db)
print("reset label db")

reset_ai_label(db)
print("reset ai label db")

print("ALL done")
