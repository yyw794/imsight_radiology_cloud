import React, { Component } from 'react'
import {Modal, Button, Spin,Checkbox} from 'antd'
import { GetLocaledText } from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'
import api from '../../api'
import {getUser} from '../../auth'
import io from 'socket.io-client'

const allOptions = [{label:<FormattedMessage id="RunDetectionModal.options.detect.liver"/>, value:'liver'},
                    {label:<FormattedMessage id="RunDetectionModal.options.detect.lung"/>, value:'lung'},
		    {label:<FormattedMessage id="RunDetectionModal.options.detect.dr"/>, value:'dr'}]

const CTOptions =[{label:<FormattedMessage id="RunDetectionModal.options.detect.liver"/>, value:'liver'},
                          {label:<FormattedMessage id="RunDetectionModal.options.detect.lung"/>, value:'lung'}]

const DXOptions =[{label:<FormattedMessage id="RunDetectionModal.options.detect.dr"/>, value:'dr'}]


class RunDetectionModal extends Component{
	constructor(props){
		super(props)
		this.state={
			detectionStatus:'selection',
			studyToDetect:props.studyToDetect,
			detectionList:[],
			detectionOptions:props.studyToDetect.modality==="CT"?
						CTOptions:
						props.studyToDetect.modality!=="CR"&&
						props.studyToDetect.modality!=="DR"&&
						props.studyToDetect.modality!=="DX"?
						allOptions:
						DXOptions,
		}
	}
	
	componentDidMount(){
		this.initSocket()
	}
	
	componentWillReceiveProps(nextProps){
		if(!this.props.visible){
			let status= this.state.detectionStatus
			if(nextProps.studyToDetect.state==="algo_unsupport"||nextProps.studyToDetect.state==="completed")
			{
				status='selection'
			}else if(nextProps.studyToDetect.state==='file_only')
			{
				status='detecting'
			}
			this.setState({
				detectionStatus:status,
				studyToDetect:nextProps.studyToDetect,
				detectionOptions:nextProps.studyToDetect.modality==="CT"?
							CTOptions:
							nextProps.studyToDetect.modality!=="CR"&&
							nextProps.studyToDetect.modality!=="DR"&&
							nextProps.studyToDetect.modality!=="DX"?
							allOptions:
							DXOptions,
			})
		}
	}
	
	initSocket(){
		this.socket = io(window.location.origin,{transports: ['websocket']})
		this.socket.connect()
		this.socket.on('connect',()=>{ console.log("study list socket connected") })
		this.socket.on('studyListUpdate',(payload)=>{
		  let status=''
		  if(this.state.studyToDetect.studyUID===payload.data.studyUID)
		  {
			if(payload.data.state==="completed")
			{
			  status='finished'
			}else if(payload.data.state==="algo_unsupport"){
			  status='finished'
			}else if(payload.data.state==="file_only"){
			  status='detecting'
			}
			this.setState({studyToDetect:payload.data,detectionStatus:status})
		  }
		})
		this.socket.on('disconnect',()=>{ console.log("study list socket disconnected")})
		this.socket.emit('studyListOpen',{
		  userID:getUser().account,
		  taskID:'studyList'
		})
	}
	
	runDetectionAgain=()=>{
		if(this.state.detectionList.length>0)
		{
			let labelType=''
			if(this.state.detectionList[0]==="liver")
			{
				labelType="LiverTumor"
			}else if(this.state.detectionList[0]==="lung")
			{
				labelType="Nodule"
			}else if(this.state.detectionList[0]==="dr")
			{
				labelType="DX"
			}
			
			this.setState({detectionStatus:"detecting",detectionList:[]})
			
			api.manualDetection({studyUID:this.state.studyToDetect.studyUID,
							   labelType:labelType,
							   taskID:this.state.studyToDetect.taskID})
			.then(res=>{
				console.log("run detection success",res)
			})
			.catch(err=>{
				console.log("run detection err",err)
			})
		}    
	}
	
	detectionResult(record){
	  if (record.state === 'meta_only') {
		return <FormattedMessage id='StudyPage.state.meta_only'/>
	  }
	  if(record.state === 'pull_data')
	  {
		return <FormattedMessage id='StudyPage.state.pull_data'/>
	  }
	  if(record.state === 'file_only')
	  {
		return <FormattedMessage id='StudyPage.state.file_only'/>
	  }
	  if(record.state === 'algo_unsupport')
	  {
		return <FormattedMessage id='StudyPage.state.algo_unsupport'/>
	  }
	  if(record.state === 'modality_unsupport')
	  {
		return <FormattedMessage id='StudyPage.state.modality_unsupport'/>
	  }
	  if(record.state === 'completed'){
		if(!!record.labelType)
		{
		  if(record.labelType==="LiverTumor" && record.labelNum>0)
		  {
			if(record.labelNum == 1){return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.tumor"/></span>}
			else{return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.tumor"/></span>}
		  }
		  else if(record.labelType==="Nodule" && record.labelNum>0)
		  {
			if(record.labelNum == 1){return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.nodule"/></span>}
			else{return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.nodules"/></span>}
		  }
		}
		  
		if (record.modality === 'CT' && record.bodyPartExamined === 'ABDOMEN' && record.labelNum > 0) 
		{
		  if(record.labelNum == 1){return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.tumor"/></span>}
		  else{return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.tumor"/></span>}
		}
		else if (record.modality === 'CT' && record.bodyPartExamined === 'CHEST' && record.labelNum > 0) 
		{
		  if(record.labelNum == 1){return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.nodule"/></span>}
		  else{return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.nodules"/></span>}
		}
		else if (record.modality === 'CT' && record.bodyPartExamined === 'LIVER' && record.labelNum > 0)
		{
		  if(record.labelNum == 1){return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.tumor"/></span>}
		  else{return <span>{record.labelNum}<FormattedMessage id="StudyTable.diagnosis-result.tumors"/></span>}
		}
		else if (record.result == 'ABNORMAL') {
		  return <span><FormattedMessage id='StudyPage.diagnosis-result.abnormal'/></span>
		}
		else if (record.result == 'NORMAL') {
		  if (record.modality === 'CT' && record.bodyPartExamined === 'CHEST') {
			return <span><FormattedMessage id='StudyPage.diagnosis-result.normal.chestNodule'/></span>
		  }
		  return <span><FormattedMessage id='StudyPage.diagnosis-result.normal'/></span>
		}else {
		  return ''
		}
	  }	
	}
	
	render(){
		return(
			<Modal
				title={GetLocaledText(this,"RunDetectionModal.title")}
				visible={this.props.visible}
				onOk={this.runDetectionAgain}
				onCancel={this.props.handleCancel}
				style={{textAlign:'center'}}
				footer={[
					<Button key="cancel" disabled={this.state.detectionStatus!=='selection'} onClick={this.props.handleCancel}>
						<FormattedMessage id="RunDetectionModal.cancel"/>
					</Button>,
					<Button key="ok" type="primary" disabled={this.state.detectionStatus !== 'selection'} onClick={this.runDetectionAgain}>
						<FormattedMessage id="RunDetectionModal.button.startDetection"/>
					</Button>
				]}
			>
				{
					this.state.detectionStatus === 'selection'?
					<Checkbox.Group options={this.state.detectionOptions} defaultValue={null} 
						onChange={(checkedValues)=>{this.setState({detectionList:checkedValues})}}
					/>
					:""
				}
				{
					this.state.detectionStatus === 'detecting'?
					<div>
						<Spin size="large"/>
						<p><FormattedMessage id="RunDetectionModal.processing"/></p>
					</div>
					:""
				}
				{	
					this.state.detectionStatus === 'finished'?
					<div style={{fontSize:'15px'}}>
						{this.detectionResult(this.state.studyToDetect)}
					</div>
					:""
				}
			</Modal>
		)
	}
}

export default injectIntl(RunDetectionModal)
