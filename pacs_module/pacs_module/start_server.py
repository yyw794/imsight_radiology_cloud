from request_interface.request_image import today_request_function, time_task_a_hour, init_do_something, request_history_list
from request_interface.http_server import start_server
import thread
from request_interface.initconfig import get_AppTitle, get_ListenPACSPort, get_if_push_model
from request_interface.pacs_interface import (store_listen, do_upload_image_path)
from multiprocessing import Process
import logging
from request_interface.logger import *
from request_interface.http_client import login_request, upload_image_path
import time
import sys

log = logging.getLogger("PACSModule.start_server")


if __name__ == "__main__":
	try:
		result_state = login_request()
		number = 0
		while result_state is False and number < 5:
			number = number + 1
			result_state = login_request()
			time.sleep(3)
		
		if result_state is False:
			log.error("five number login_request error")
			sys.exit()
			
		if get_if_push_model() is False:
			thread.start_new_thread(do_upload_image_path, ("doctor_high_level",))

		start_server()
	except KeyboardInterrupt:
		log.error("create listen_process thread error")
