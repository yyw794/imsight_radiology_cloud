import React, { Component } from 'react'
import { DropTarget } from 'react-dnd';
import itemTypes from '../../component/dragDropItemTypes'

const dropTarget = {
  drop(props, monitor, component) {
    const study = monitor.getItem()
    // console.log('drop:', study);
    props.onDrop(study)
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

const DropZoom = props => {
  const { connectDropTarget, isOver, active } = props;
  return connectDropTarget(
    <div style={{width: '100%', height: '100%'}}>
      {props.children}
    </div>
  )
}

export default DropTarget(itemTypes.CT_STUDY, dropTarget, collect)(DropZoom);
