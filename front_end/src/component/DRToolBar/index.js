import React, { Component } from 'react';
import Logo from '../Logo'
import './index.less'
import {Divider,Icon} from 'antd'

import LanguageDropdownList from '../../component/LanguageDropdownList'
import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import Return from "../ReturnList";

/**
 * DR 图片button
 * @param props
 * @returns {boolean}
 * @constructor
 */
const ImageButton = (props) => {
    let  style=""
    props["id"]?style = {
        width:"3rem",
        height:"3rem",
        backgroundSize:"80%"
    }:style = {
        width:"3rem",
        height:"3rem",
        backgroundSize:"80%"
    }
  if (props.active) style.backgroundColor = '#1890ff'
  return (
    <a href="javaScript:void(0)" title={props.word}><div className='image-button'
      style={style}
      onClick={props.onClick}>
      <img src={props.bgImg} />
      <span>{props.text}</span>
    </div>
    </a>
  )
}

/**
 * DR工具组件
 */
class ToolBar extends Component {
    /**
     * 参照生命周期构造函数
     * @param props
     */
  constructor(props) {
    super(props)
        /**
         *
         * @private {{}}
         */
    this.state = {};
    
  }

    /**
     * DR工具组件
     * @returns {Component}
     */
  render() {
    return (
      <div className='viewer-toolbar' style={{lineHeight:"3rem"}}>
        <Logo style={{marginTop:"-0.45rem"}}/>
        <Return  />
        <div className='tool-button-wrapper'>
          <ImageButton
            bgImg={require('../../../static/images/tool/contrast.png')}
            active={this.props.activeTool.includes('wwwc')}
            word={GetLocaledText(this, "DRViewer.ToolBar.wwwc")}
            text=""
            id="wwwc"
            onClick={() => this.props.onToolClick('wwwc')}/>
          <ImageButton
            bgImg={require('../../../static/images/tool/inverse.png')}
            active={this.props.activeTool.includes('invert')}
            word={GetLocaledText(this, "DRViewer.ToolBar.invert")}
            text=""
            onClick={() => this.props.onToolClick('invert')}/>
          <ImageButton
            bgImg={require('../../../static/images/tool/refresh.png')}
            active={this.props.activeTool==='recover'}
            word={GetLocaledText(this, "DRViewer.ToolBar.recover")}
            text=""
            onClick={() => this.props.onToolClick('recover')}/>
            <ImageButton
                bgImg={require('../../../static/images/tool/hidden.png')}
                active={this.props.activeTool.includes('hideBoxes')}
                word={GetLocaledText(this,"CTToolBar.lesion.hideBoxes")}
                text=""
                onClick={()=>this.props.onToolClickShow('hideBoxes')}
            />
          <Divider type="vertical" style={{height:"1.5rem",top:"-1.3rem"}}/>
          <ImageButton
            bgImg={require('../../../static/images/tool/ruler.png')}
            active={this.props.activeTool.includes('length')}
            text=""
            word={GetLocaledText(this, "DRViewer.ToolBar.length")}
            onClick={() => this.props.onToolClick('length')}/>
          <ImageButton
            bgImg={require('../../../static/images/tool/angle.png')}
            active={this.props.activeTool.includes('angle')}
            text=""
            word={GetLocaledText(this, "DRViewer.ToolBar.angle")}
            onClick={() => this.props.onToolClick('angle')}/>
          <ImageButton
            bgImg={require('../../../static/images/tool/polygon.png')}
            active={this.props.activeTool.includes('freehand')}
            text=""
            id="freehand"
            word={GetLocaledText(this, "DRViewer.ToolBar.freehand")}
            onClick={() => this.props.onToolClick('freehand')}/>
            <ImageButton
                bgImg={require('../../../static/images/tool/delete.png')}
                active={this.props.activeTool.includes('eraser')}
                text=""
                word={GetLocaledText(this, "DRViewer.ToolBar.eraser")}
                onClick={() => this.props.onToolClick('eraser')}/>
          {
            this.props.forbidEdit ? '' :
            <Divider type="vertical" style={{height:"1.5rem",top:"-1.3rem"}} />
          }
          {
            this.props.forbidEdit ? '' :
            <ImageButton
              bgImg={require('../../../static/images/tool/mark.png')}
              active={this.props.activeTool.includes('lesionsMarker')}
              text=""
              word={GetLocaledText(this, "DRViewer.ToolBar.lesionsMarker")}
              onClick={() => this.props.onToolClick('lesionsMarker')}/>
          }
          {
            this.props.forbidEdit || this.props.forbidScreenshot ? '' :
            <ImageButton
              bgImg={require('../../../static/images/tool/screenshot.png')}
              active={this.props.activeTool.includes('screenshot')}
              text=""
              word={GetLocaledText(this, "DRViewer.ToolBar.screenshot")}
              onClick={() => this.props.onToolClick('screenshot')}/>
          }
          {
            JSON.parse(localStorage.getItem("fellowParams"))? JSON.parse(localStorage.getItem("fellowParams")).state==="completed"?
              <ImageButton
                  bgImg={require('../../../static/images/tool/report.png')}
                  active={this.props.activeTool === 'report'}
                  text=""
                  word={GetLocaledText(this, "DRViewer.ToolBar.report")}
                  onClick={() => this.props.onToolClick('report')}/>:"":""
          }
          {/*<LanguageDropdownList setAppState={(state) => this.setState(state)}/>*/}
        </div>
      </div>
    );
  }

}

export default injectIntl(ToolBar);
