import React, { Component } from 'react';
import _ from 'underscore'
import {Icon} from 'antd'
import './index.less'
import {VIEW_POSITION} from '../../data'
import SeriesThumbnailItem from './SeriesThumbnailItem'

class SeriesThumbnailList extends Component {

  componentDidMount() {
    console.log('printing images', this.props.images);
  }
  
  state = {
    currentIdx: this.props.defaultIdx,
    collapsed: false
  }

  onItemClick = (id) => {
    const currentIdx = _.findIndex(this.props.images, img => img.instanceID === id)
    if (currentIdx !== this.state.currentIdx) {
      _.isFunction(this.props.onChange) && this.props.onChange(this.props.images[currentIdx], currentIdx)
      this.setState({
        currentIdx
      })
    }
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  render() {
    return (
      <div className={this.state.collapsed ? 'thumbnail-list-container collapsed' : 'thumbnail-list-container'}>
        <ul className='thumbnail-list'>
          {
            this.props.images.map((img, i) => (
              <SeriesThumbnailItem
              key={img.instanceID}
              iter={i}
              image={img} 
              currentIdx={this.state.currentIdx}
              onItemClick={this.onItemClick}
              />
            ))
          }
        </ul>
        <div className='toggle-collapsed horizontal' onClick={this.toggleCollapsed}>
          {
            this.state.collapsed ?
            <Icon type="left" />
            :
            <Icon type="right" />
          }
        </div>
        <div className='toggle-collapsed vertical' onClick={this.toggleCollapsed}>
          {
            this.state.collapsed ?
            <Icon type="up" />
            :
            <Icon type="down" />
          }
        </div>
      </div>
    );
  }

}

export default SeriesThumbnailList;
