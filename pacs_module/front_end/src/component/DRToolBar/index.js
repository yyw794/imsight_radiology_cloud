import React, { Component } from 'react';
import Logo from '../Logo'
import './index.less'
import {Divider,Icon} from 'antd'

import LanguageDropdownList from '../../component/LanguageDropdownList'
import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import Return from "../ReturnList";


const ImageButton = (props) => {
  let style = {
    width: props.width,
    height: props.height,
    backgroundImage: `url(${props.bgImg})`
  }
  if (props.active) style.backgroundColor = '#1890ff'
  return (
    <div className='image-button'
      style={style}
      onClick={props.onClick}>
      <span>{props.text}</span>
    </div>
  )
}

class ToolBar extends Component {
  constructor(props) {
    super(props)
    this.state = {};
    
  }
  render() {
    return (
      <div className='viewer-toolbar'>
        <Logo/>
        <Return  />
        <div className='tool-button-wrapper'>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/contrast.png')}
            active={this.props.activeTool === 'wwwc'}
            text={GetLocaledText(this, "DRViewer.ToolBar.wwwc")}
            onClick={() => this.props.onToolClick('wwwc')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/inverse.png')}
            active={this.props.activeTool === 'invert'}
            text={GetLocaledText(this, "DRViewer.ToolBar.invert")}
            onClick={() => this.props.onToolClick('invert')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/refresh.png')}
            active={this.props.activeTool === 'recover'}
            text={GetLocaledText(this, "DRViewer.ToolBar.recover")}
            onClick={() => this.props.onToolClick('recover')}/>
            <ImageButton
                width={64} height={64}
                bgImg={require('../../../static/images/hideBoxes.png')}
                active={this.props.activeTool === 'hideBoxes'}
                text={GetLocaledText(this,"CTToolBar.lesion.hideBoxes")}
                onClick={()=>this.props.onToolClickShow('hideBoxes')}
            />
          <Divider type="vertical" />
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/ruler.png')}
            active={this.props.activeTool === 'length'}
            text={GetLocaledText(this, "DRViewer.ToolBar.length")}
            onClick={() => this.props.onToolClick('length')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/protractor.png')}
            active={this.props.activeTool === 'angle'}
            text={GetLocaledText(this, "DRViewer.ToolBar.angle")}
            onClick={() => this.props.onToolClick('angle')}/>
          <ImageButton
            width={64} height={64}
            bgImg={require('../../../static/images/polygon.png')}
            active={this.props.activeTool === 'freehand'}
            text={GetLocaledText(this, "DRViewer.ToolBar.freehand")}
            onClick={() => this.props.onToolClick('freehand')}/>
            <ImageButton
                width={64} height={64}
                bgImg={require('../../../static/images/clear-tool.png')}
                active={this.props.activeTool === 'eraser'}
                text={GetLocaledText(this, "DRViewer.ToolBar.eraser")}
                onClick={() => this.props.onToolClick('eraser')}/>
          {
            this.props.forbidEdit ? '' :
            <Divider type="vertical" />
          }
          {
            this.props.forbidEdit ? '' :
            <ImageButton
              width={64} height={64}
              bgImg={require('../../../static/images/rect.png')}
              active={this.props.activeTool === 'lesionsMarker'}
              text={GetLocaledText(this, "DRViewer.ToolBar.lesionsMarker")}
              onClick={() => this.props.onToolClick('lesionsMarker')}/>
          }
          {
            this.props.forbidEdit || this.props.forbidScreenshot ? '' :
            <ImageButton
              width={64} height={64}
              bgImg={require('../../../static/images/screenshot.png')}
              active={this.props.activeTool === 'screenshot'}
              text={GetLocaledText(this, "DRViewer.ToolBar.screenshot")}
              onClick={() => this.props.onToolClick('screenshot')}/>
          }
          {
            JSON.parse(localStorage.getItem("fellowParams"))? JSON.parse(localStorage.getItem("fellowParams")).state==="completed"?
              <ImageButton
                  width={64} height={64}
                  bgImg={require('../../../static/images/report.png')}
                  active={this.props.activeTool === 'report'}
                  text={GetLocaledText(this, "DRViewer.ToolBar.report")}
                  onClick={() => this.props.onToolClick('report')}/>:"":""
          }
          {/*<LanguageDropdownList setAppState={(state) => this.setState(state)}/>*/}
        </div>
      </div>
    );
  }

}

export default injectIntl(ToolBar);
