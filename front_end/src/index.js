import "babel-core/register"
import "babel-polyfill"
import "./App.less"
import enUS from 'antd/lib/locale-provider/en_US';


import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter, Route, IndexRoute } from 'react-router-dom'
import store from './store'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import App from './App'
import {isLoggedIn} from './auth'
import { createBrowserHistory } from 'history'
const history = createBrowserHistory()

render((
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
  ), document.getElementById('app'))
