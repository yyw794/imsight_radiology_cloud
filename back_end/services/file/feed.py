#!/bin/python
# coding=utf-8
import os
import sys
import traceback
import uuid

from flask import make_response, jsonify, request
from flask_login import current_user
from werkzeug.utils import secure_filename

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import *
from models.response.response import response
from models.report.Report import Report, parseReportInfo
from models.tool.Tool import Tool, saveToollist
from models.study.Study import parseStudyInfo
from models.label.Label_IO import parseLabelInfo, saveLabelist
from models.task.Task import parseTaskInfo

from services.file.on_file import on_file, process_study_image
from services.algo.algo_service import predict, nodule_register, do_ribs_detect
from services.algo.on_radiomics import create_radiomics
from config import SCREENSHOT_ROOT, CACHE_ROOT
import logging
import numpy as np
import scipy.misc
import functools

logger = logging.getLogger('radiology')
from services.pacs.pacs_api import update_pacs_list_state, send_socketIO_to_node
from services.basic.basic_api import socket_emit

import SimpleITK as sitk


class ProcessThread(threading.Thread):
    def __init__(self, target, args=()):
        super(ProcessThread, self).__init__()
        self.target = target
        self.args = args
        self.exitcode = 0
        self.exception = None
        self.exc_traceback = ''

    def run(self):
        try:
            self.result = self.target(*self.args)
        except Exception as e:
            self.exitcode = 1
            self.exception = e
            self.exc_traceback = ''.join(traceback.format_exception(*sys.exc_info()))

    def get_result(self):
        try:
            return self.result
        except Exception:
            return None


required_key = ['taskID']
required_file = ['file']
required_key_nodule = ['labelID', 'seriesID']
mutex = threading.Lock()


def allowed_dcm_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in DCM_EXTENSIONS


def combined_URL(uid):
    return "/api/screenshot?uid=" + uid


def feed_screenshot():
    """
    @api {post} /api/upload_screenshot  上传截图
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} taskID 任务ID
    @apiParam {String} file 截图文件
    @apiParam {String} labelID 标签ID
    @apiParam {String} seriesID 序列ID
    @apiParamExample {json} Request-Example:
       multipart/form-data
    @apiSuccessExample {json} Success-Response:
    {
      "data": "http://192.168.1.157:5000/api/screenshot?uid=bb3f2034-f706-11e8-9ea4-0242c0a8e004",
      "message": "OK",
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "statusCode": 1200
    }
    """
    res = response()
    userID = current_user.get_id()
    taskID = request.form.get('taskID', '')
    if taskID == '':
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    report_info = db_find_one("report", {"taskID": taskID})
    if not report_info:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    report = parseReportInfo(report_info)
    studyUID = report.studyUID

    file_feild = request.files
    if not all(key in file_feild for key in required_file):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    if not os.path.exists(SCREENSHOT_ROOT):
        os.makedirs(SCREENSHOT_ROOT)

    file = request.files['file']
    filename = secure_filename(file.filename)
    uid = str(uuid.uuid1())
    folderpath = os.path.join(SCREENSHOT_ROOT, uid)
    os.makedirs(folderpath)
    filepath = os.path.join(folderpath, filename)
    file.save(filepath)

    if report.modality == 'CT':
        label_info = db_find_one("label", {"taskID": taskID})
        if label_info is None:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        labelID = request.form.get('labelID', '')
        seriesID = request.form.get('seriesID', '')
        if labelID == '' or seriesID == '':
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        labelist = parseLabelInfo(label_info)
        labelist = filter(lambda l: l.seriesID == seriesID, labelist)
        target_labelist = filter(lambda l: l.labelID == labelID, labelist)
        if len(target_labelist) == 0:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)
        label = target_labelist[0]
        labelist.remove(label)
        uid = label.labelID
        label.url = combined_URL(uid)
        labelist.append(label)
        db_update_one("label", {"taskID": taskID}, saveLabelist(labelist, studyUID, taskID, label_info["labelType"]))
        # socket_emit('updateLabel',
        #             {'data': [label.to_dict() for label in labelist], 'userID': userID, 'taskID': taskID})
        socket_emit('updateLabel', {'data': "done", 'userID': userID, 'taskID': taskID})

    new_resource = {
        "uid": uid,
        "img_path": folderpath,
        "filename": filename
    }
    db_insert_one("resource", new_resource)
    if report.modality == 'DX':
        report.screenshotURLs = [combined_URL(uid)]
    else:
        report.screenshotURLs.append(combined_URL(uid))

    db_update_one("report", {"taskID": taskID}, report.to_db())
    # ret = report.to_dict()
    # if report.modality == 'CT':
    # label_info = db_find_one("label", {"taskID": taskID})
    # labelist = parseLabelInfo(label_info)
    # ret['labelist'] = [label.to_dict() for label in labelist if label.url != '']
    socket_emit('updateReport', {'data': "done", 'userID': userID, 'taskID': taskID})

    res.data = combined_URL(uid)
    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)


required_key_manual_detection = ['studyUID', 'labelType', 'taskID']


def fetch_manual_detection():
    """
    @api {post} /api/manual_detection  疾病检测
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} studyUID 检测ID
    @apiParam {String} labelType 检测类型
    @apiParam {String} taskID 任务ID
    @apiParamExample {json} Request-Example:
        {"studyUID":"1.2.840.86.755.86226839.811.3692510","labelType":"Nodule","taskID":"5beb65ce-f6d5-11e8-905a-0242c0a8d004"}
    @apiSuccessExample {json} Success-Response:
    {
      "data": {
        "account": "example",
        "contact": "example",
        "email": "example",
        "hospital": "example",
        "nickname": "example",
        "type": "normal"
      },
      "message": "OK",
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "statusCode": 1100
    }
    """
    res = response()
    userID = current_user.get_id()

    params = request.args.to_dict() if request.method == 'GET' else request.json

    logger.info("manual detect. params=%s", params)
    if not all(key in params for key in required_key_manual_detection):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    studyUID = params['studyUID']
    labelType = params['labelType']
    taskID = params['taskID']
    mutex.acquire()
    study_info = db_find_one("study", {"studyUID": studyUID})
    if study_info is None:
        logger.error("invalid studyUID. studyUID=%s", studyUID)
        res.statusCode = 1100
        mutex.release()
        return make_response(jsonify(res.to_dict()), 200)
    else:
        if study_info["state"] == "file_only":
            logger.error("the study is detecting, ignore this. studyUID=%s", studyUID)
            res.statusCode = 1300
            mutex.release()
            return make_response(jsonify(res.to_dict()), 200)
        else:
            study_info['state'] = 'file_only'
            db_find_update_otherwise_insert("study", {"studyUID": studyUID}, study_info)
            mutex.release()
    db_delete_one('ai_label', {'studyUID': studyUID})

    user_label_infos = db_find_all("label", {"studyUID": studyUID})
    for label_data in user_label_infos:
        label_data['labels'] = []
    db_update_all('label', {"taskID": taskID}, user_label_infos)

    studylist_info = db_find_one('studylist', {"studyUID": studyUID})
    studylist_info['state'] = 'file_only'
    db_update_one("studylist", {"studyUID": studyUID}, studylist_info)

    send_socketIO_to_node(studyUID, userID)
    study_info_temp = study_info.copy()
    if labelType == 'DX':
        study_info_temp['modality'] = 'DX'
        study_info_temp['bodyPartExamined'] = 'CHEST'
        for series_index in range(len(study_info_temp['series'])):
            instances = study_info_temp['series'][series_index]['instances']
            for instance_index in range(len(instances)):
                instances[instance_index]['viewPosition'] = 'PA'
    elif labelType == 'Nodule':
        study_info_temp['modality'] = 'CT'
        study_info_temp['bodyPartExamined'] = 'CHEST'
    elif labelType == 'LiverTumor':
        study_info_temp['modality'] = 'CT'
        study_info_temp['bodyPartExamined'] = 'LIVER'
    task_info = predict(study_info_temp, userID, taskID)

    studylist_info = db_find_one('studylist', {"studyUID": studyUID})
    if task_info:
        create_radiomics(task_info['labels'], task_info['studyUID'], task_info['labelType'], taskID)
        study_info['state'] = 'completed'

        user_label_info = db_find_one('label', {"taskID": taskID})
        user_label_info['labelType'] = labelType
        db_update_one('label', {"taskID": taskID}, user_label_info)

        studylist_info['result'] = "ABNORMAL" if len(task_info['labels']) else "NORMAL"
        studylist_info['labelType'] = labelType
        studylist_info['labelNum'] = len(task_info['labels'])
        studylist_info['state'] = 'completed'
    else:
        study_info['state'] = 'algo_unsupport'
        studylist_info['result'] = ""
        studylist_info['labelType'] = labelType
        studylist_info['labelNum'] = 0
        studylist_info['state'] = 'algo_unsupport'
    db_find_update_otherwise_insert("study", {"studyUID": studyUID}, study_info)
    db_update_one("studylist", {"studyUID": studyUID}, studylist_info)

    new_study = parseStudyInfo(study_info)
    new_report = Report()
    new_report.taskID = taskID
    new_report.studyUID = new_study.studyUID
    new_report.studyDate = new_study.studyDate
    new_report.modality = new_study.modality
    new_report.birthDate = new_study.birthDate
    new_report.patientID = new_study.patientID
    new_report.patientName = new_study.patientName
    new_report.gender = new_study.gender
    db_find_update_otherwise_insert("report", {"taskID": taskID}, new_report.to_db())

    if new_study.modality in ['DX', 'DR', 'CR']:
        tool_list = []
        for series in study_info['series']:
            for instance in series['instances']:
                new_tool = Tool(new_study.studyUID, series['seriesID'], instance['instanceID'], None)
                tool_list.append(new_tool)
        logger.info("userID {} update tool {}".format(userID, taskID))
        db_find_update_otherwise_insert("tool", {"userID": userID, "studyUID": new_study.studyUID},
                                        saveToollist(tool_list, new_study.studyUID, userID))

    # nodule follow up
    if study_info['state'] == 'completed' and labelType == 'Nodule':
        do_nodule_matching(studylist_info['studyUID'], userID)

    send_socketIO_to_node(studyUID, userID)  # update to front after nodule registration

    # TODO: change manual detection to return immediately and send socket to front_end
    if request.method == "POST":
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)


def exception_wrap(func):
    """
    exception wrap
    :param func:
    :return:
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as exc:
            func_name = func.__name__
            logger.exception("calling function occurs a exception. function_name=%s, err=%s", func_name, exc)
            return
    return wrapper


@exception_wrap
def process_prediction(studyUID, study_info, userID, taskID, isNotify, doFollowUp):
    study_info, flag = process_study_image(study_info)
    logger.debug("update studylist. studyUID=%s", studyUID)
    study_info['state'] = 'file_only' if flag else 'png_unsupport'
    db_find_update_otherwise_insert("study", {"studyUID": studyUID}, study_info)

    state = 'file_only' if flag else 'png_unsupport'
    study_info['state'] = state
    db_update_one("studylist", {"studyUID": studyUID}, {"state": state})

    if (isNotify):
        send_socketIO_to_node(studyUID, userID)
    else:
        update_pacs_list_state(studyUID, userID)

    if flag:
        task_info = predict(study_info, userID, taskID)
        if task_info:
            state = 'completed'
            if "disease" in task_info:
                result = len(task_info['labels']) or len(task_info["disease"])
            else:
                result = len(task_info['labels'])
            db_update_one("studylist", {"studyUID": studyUID},
                          {
                              "result": "ABNORMAL" if result else "NORMAL",
                              "labelType": task_info['labelType'],
                              "labelNum": len(task_info['labels']),
                              "state": state
                          }
                          )

            logger.info("userID {} invoke algorithm success".format(userID))
            study_info['state'] = 'completed'
            socket_emit('updateLabel', {'data': "done", 'userID': userID, 'taskID': taskID})
        else:
            state = 'algo_unsupport'
            db_update_one("studylist", {"studyUID": studyUID}, {"state": state})
            study_info['state'] = 'algo_unsupport'
            return

    if db_find_one("study", {"studyUID": studyUID}):
        db_update_one("study", {"studyUID": studyUID}, {"state": state, "labelType": task_info['labelType']})
    else:
        logger.error("update study algo result while study not exist, UID: {}".format(studyUID))
    # db_find_update_otherwise_insert("study", {"studyUID": studyUID}, {"state"})

    if (isNotify):
        send_socketIO_to_node(studyUID, userID)
    else:
        update_pacs_list_state(studyUID, userID)

    if study_info['state'] == 'completed':
        if doFollowUp:
            do_nodule_matching(study_info['studyUID'], userID)
        if task_info:
            create_radiomics(task_info['labels'], task_info['studyUID'], task_info['labelType'], taskID)
        do_ribs_detect(studyUID, taskID)


def do_nodule_matching(studyUID, userID):
    logger.info("check matching {}".format(studyUID))
    study_info_current = db_find_one("study", {"studyUID": studyUID})
    if "labelType" not in study_info_current:
        study_info_current["labelType"] = None
    if study_info_current == None:
        logger.info("end matching, not exist")
    elif study_info_current['state'] != 'completed':
        logger.info("end matching, not completed state")
    elif study_info_current['labelType'] != 'Nodule':
        logger.info("end matching, not Nodule labelType")
    else:
        logger.info("start matching patientID {}".format(study_info_current['patientID']))

        one_patient_studies = db_find('study', {'patientID': study_info_current['patientID']})
        patient_studies = filter(lambda s: s['modality'] == 'CT' and 'CHEST' in s['bodyPartExamined'],
                                 one_patient_studies)

        if len(patient_studies) <= 1:
            logger.info("no more then 1 study existed for patientID: {}".format(study_info_current['patientID']))
            return
        # get closet datetime study
        patient_studies = sorted(patient_studies, key=lambda p: p['studyDate'], reverse=True)
        ownership_info = db_find_one('ownership', {"userID": userID})
        shared_info = ownership_info['shared']

        for idx, fixed_study in enumerate(patient_studies[1:]):
            moving_study = patient_studies[idx]
            fixed_studyUID = fixed_study['studyUID'].replace('.', '-')
            moving_studyUID = moving_study['studyUID'].replace('.', '-')
            if fixed_studyUID not in shared_info:
                logger.info("fixed task not in shared list. fixed_study_uid=%s", fixed_studyUID)
                continue
            if moving_studyUID not in shared_info:
                logger.info("moving task not in shared list. moving_study_uid=%s", moving_studyUID)
                continue
            moving_study['taskID'] = shared_info[moving_studyUID]
            fixed_study['taskID'] = shared_info[fixed_studyUID]
            nodule_matching_result = db_find_one("ai_label",
                                                 {'taskID_fixed': fixed_study['taskID'],
                                                  'taskID_moving': moving_study['taskID']})

            logger.info("fixed study UID:{} studyDate: {}".format(fixed_study['studyUID'],
                                                                  fixed_study['studyDate'].strftime(
                                                                      '%Y-%m-%d %H:%M:%S')))
            logger.info("moving study UID:{} studyDate: {}".format(moving_study['studyUID'],
                                                                   moving_study['studyDate'].strftime(
                                                                       '%Y-%m-%d %H:%M:%S')))

            def update_followUp(fixedUID, movingUID):
                db_update_one("studylist", {"studyUID": fixedUID},
                              {"followUp": "{}?moving".format(movingUID)})

                db_update_one("studylist", {"studyUID": movingUID},
                              {"followUp": "{}?fixed".format(fixedUID)})

                db_update_one("study", {"studyUID": fixedUID},
                              {"followUp": "{}?moving".format(movingUID)})

                db_update_one("study", {"studyUID": movingUID},
                              {"followUp": "{}?fixed".format(fixedUID)})

            if nodule_matching_result:
                if len(nodule_matching_result['registerNodules']['fixed']) > 0 and \
                        len(nodule_matching_result['registerNodules']['moving']) > 0:
                    update_followUp(fixed_study['studyUID'], moving_study['studyUID'])
                    logger.info("nodule matching result existed, continue. fixed_task=%s, "
                                "moving_task=%s", fixed_study['taskID'], moving_study['taskID'])
                    continue

            if type(fixed_study['series']) == dict:
                fixed_study['series'] = fixed_study['series'].values()

            if type(moving_study['series']) == dict:
                moving_study['series'] = moving_study['series'].values()

            nodule_register(fixed_study, moving_study)
            update_followUp(fixed_study['studyUID'], moving_study['studyUID'])

            logger.info("end matching fixedDate: {} movingDate: {}".format(
                fixed_study['studyDate'].strftime('%Y-%m-%d %H:%M:%S'),
                moving_study['studyDate'].strftime('%Y-%m-%d %H:%M:%S')
            ))
            break


def process_user_filepaths(filepaths, userID):
    study_infolist = on_file(filepaths)
    shared = db_find_one("ownership", {"userID": userID})
    if not shared:
        shared = {"shared": []}
    liability_info = db_find_one("liability", {"reporter": userID})

    studylist = []
    for study_info in study_infolist:
        new_study = parseStudyInfo(study_info)
        if (new_study.studyUID).replace(".", "-") not in shared['shared']:
            new_taskID = str(uuid.uuid1())
            logger.info(
                "userID {} studyUID {} NOT in shared new taskID {}".format(userID, new_study.studyUID, new_taskID))
        else:
            new_taskID = shared['shared'][(new_study.studyUID).replace(".", "-")]
            logger.info("userID {} studyUID {} in shared taskID {}".format(userID, new_study.studyUID, new_taskID))
        new_report = Report()
        new_report.taskID = new_taskID
        new_report.studyUID = new_study.studyUID
        new_report.studyDate = new_study.studyDate
        new_report.modality = new_study.modality
        new_report.birthDate = new_study.birthDate
        new_report.patientID = new_study.patientID
        new_report.patientName = new_study.patientName
        new_report.gender = new_study.gender
        logger.info("userID {} update report {}".format(userID, new_taskID))
        db_find_update_otherwise_insert("report", {"taskID": new_taskID}, new_report.to_db())

        if new_study.modality in ['DX', 'DR', 'CR']:
            tool_list = []
            for series in study_info['series']:
                for instance in series['instances']:
                    new_tool = Tool(new_study.studyUID, series['seriesID'], instance['instanceID'], None)
                    tool_list.append(new_tool)
            logger.info("userID {} update tool {}".format(userID, new_taskID))
            db_find_update_otherwise_insert("tool", {"userID": userID, "studyUID": new_study.studyUID},
                                            saveToollist(tool_list, new_study.studyUID, userID))

        new_task = {
            "taskID": new_taskID,
            "reporter": liability_info['reporter'],
            "reviewer": liability_info['reviewer'],
            "state": "assign",
            "type": current_user.type,
            "studyUID": new_study.studyUID
        }
        logger.info("userID {} update task {} ".format(userID, new_taskID))
        db_delete_one("task", {"studyUID": new_study.studyUID})
        db_find_update_otherwise_insert("task", {"taskID": new_taskID}, new_task)
        task = parseTaskInfo(new_task)

        study_info['state'] = 'pull_data'
        new_study.state = 'pull_data'
        logger.info("userID {} update studyUID {} taskID {} state {}".format(userID, new_study.studyUID, new_taskID,
                                                                             study_info['state']))
        db_find_update_otherwise_insert("study", {"studyUID": study_info['studyUID']}, study_info)

        mutex.acquire()
        new_shared = db_find_one("ownership", {"userID": userID})
        new_shared['shared'][(new_study.studyUID).replace(".", "-")] = new_taskID
        db_update_one("ownership", {"userID": userID}, new_shared)
        mutex.release()

        new_dict = new_study.to_dict(withSeries=False)
        new_dict["diagnosisAdvice"] = new_report.diagnosisAdvice
        new_dict['submitted'] = new_report.submitted()
        new_dict['labelNum'] = 0
        new_dict['taskID'] = new_taskID
        new_dict["result"] = "pending"
        new_dict['user_privilege'] = task.get_user_privilege(userID)
        new_dict['state'] = 'pull_data'
        db_find_update_otherwise_insert('studylist', {"studyUID": new_dict['studyUID']}, new_dict)

        # update this label after get ai result
        db_delete('ai_label', {'studyUID': new_study.studyUID})
        db_delete("label", {'studyUID': new_study.studyUID})
        if current_user.type == 'normal':
            # turn off nodule register
            t = ProcessThread(target=process_prediction,
                              args=(new_study.studyUID, study_info, userID, task.id, True, True))
            t.start()

        studylist.append(new_dict)

    return studylist


def feed_study_image():
    """
    @api {post} /api/upload_image  上传study图片
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParamExample {json} Request-Example:
       multipart/form-data
    @apiSuccessExample {json} Success-Response:
    {
      "data": [
        {
          "age": 61,
          "birthDate": "1957-10-29",
          "bodyPartExamined": "CHEST",
          "diagnosisAdvice": "",
          "gender": "FEMALE",
          "hospital": "hosp",
          "labelNum": 0,
          "modality": "CT",
          "patientID": "1001203",
          "patientName": "YYY",
          "result": "pending",
          "reverse": false,
          "state": "pull_data",
          "studyDate": "2018-12-05",
          "studyTime": "10:23:02",
          "studyUID": "1.2.392.200036.9116.2.5.1.3268.2060439916.1525305902.560145",
          "submitted": false,
          "taskID": "d74e82a4-f2b2-11e8-b0fa-0242ac120003",
          "uploadDate": "2018-12-05 10:23:02",
          "user_privilege": [
            "reporter",
            "normal"
          ]
        }
      ],
      "message": "OK",
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "statusCode": 1200
    }
    """
    filepaths = []

    uploaded_files = request.files.getlist("file[]")
    if not uploaded_files:
        uploaded_files = request.files.values()

    if not os.path.exists(CACHE_ROOT):
        os.makedirs(CACHE_ROOT)

    for file in uploaded_files:
        filename = secure_filename(file.filename)
        filepath = os.path.join(CACHE_ROOT, filename)
        if filepath in filepaths:
            logger.info("already save file {}".format(filepath))
            continue
        file.save(filepath)
        filepaths.append(filepath)

    logger.info("userID {} upload {} file ".format(current_user.get_id(), len(filepaths)))

    res = response()
    userID = current_user.get_id()

    logger.info("process files userID {}".format(userID))
    if len(filepaths) > 0:
        studylist = process_user_filepaths(filepaths, userID)
    else:
        studylist = []

    res.statusCode = 1000
    res.data = studylist
    ret = jsonify(res.to_dict())

    return make_response(ret, 200)


def process_pacs_filepaths(filepaths, userID):
    study_infolist = on_file(filepaths)
    shared = db_find_one("ownership", {"userID": userID})
    liability_info = db_find_one("liability", {"reporter": userID})

    studylist = []
    for study_info in study_infolist:
        new_study = parseStudyInfo(study_info)
        if (new_study.studyUID).replace(".", "-") not in shared['shared']:
            new_taskID = str(uuid.uuid1())
            logger.info(
                "userID {} studyUID {} NOT in shared new taskID {}".format(userID, new_study.studyUID, new_taskID))
        else:
            new_taskID = shared['shared'][(new_study.studyUID).replace(".", "-")]
            logger.info("userID {} studyUID {} in shared taskID {}".format(userID, new_study.studyUID, new_taskID))
        new_report = Report()
        new_report.taskID = new_taskID
        new_report.studyUID = new_study.studyUID
        new_report.studyDate = new_study.studyDate
        new_report.modality = new_study.modality
        new_report.birthDate = new_study.birthDate
        new_report.patientID = new_study.patientID
        new_report.patientName = new_study.patientName
        new_report.gender = new_study.gender
        logger.info("userID {} update report {}".format(userID, new_taskID))
        db_find_update_otherwise_insert("report", {"taskID": new_taskID}, new_report.to_db())

        if new_study.modality in ['DX', 'DR', 'CR']:
            tool_list = []
            for series in study_info['series']:
                for instance in series['instances']:
                    new_tool = Tool(new_study.studyUID, series['seriesID'], instance['instanceID'], None)
                    tool_list.append(new_tool)
            logger.info("userID {} update tool {}".format(userID, new_taskID))
            db_find_update_otherwise_insert("tool", {"taskID": new_taskID},
                                            saveToollist(tool_list, new_study.studyUID, userID))

        new_task = {
            "taskID": new_taskID,
            "reporter": liability_info['reporter'],
            "reviewer": liability_info['reviewer'],
            "state": "assign",
            "type": current_user.type,
            "studyUID": new_study.studyUID
        }
        logger.info("userID {} update task {} ".format(userID, new_taskID))
        db_find_update_otherwise_insert("task", {"taskID": new_taskID}, new_task)
        task = parseTaskInfo(new_task)

        new_dict = new_study.to_dict(withSeries=False)
        new_dict["diagnosisAdvice"] = new_report.diagnosisAdvice
        new_dict['submitted'] = new_report.submitted()
        new_dict['labelNum'] = 0
        new_dict['taskID'] = new_taskID
        new_dict["result"] = "pending"
        new_dict['user_privilege'] = task.get_user_privilege(userID)
        new_dict['state'] = 'pull_data'

        db_find_update_otherwise_insert('studylist', {"studyUID": new_dict['studyUID']}, new_dict)
        studylist.append(new_dict)

        logger.info("userID {} update studyUID {} taskID {} state {}".format(userID, new_study.studyUID, new_taskID,
                                                                             study_info['state']))
        db_find_update_otherwise_insert("study", {"studyUID": study_info['studyUID']}, study_info)

        mutex.acquire()
        new_shared = db_find_one("ownership", {"userID": userID})
        new_shared['shared'][(new_study.studyUID).replace(".", "-")] = new_taskID
        db_update_one("ownership", {"userID": userID}, new_shared)
        mutex.release()

        # update this label after get ai result
        db_delete('ai_label', {'studyUID': new_study.studyUID})
        db_delete("label", {'studyUID': new_study.studyUID})

        if current_user.type == 'normal' and new_study.state == 'meta_only':
            logger.info("userID {} invoke algorithm".format(userID))
            # not opening another thread,not sending socket message, not Follow up
            process_prediction(new_study.studyUID, study_info, userID, task.id, False, True)

    return studylist


def feed_pacs_study_image():
    """
    @api {post} /api/upload_pacs_image  通过PACS拉取图片
    @apiVersion 0.0.1
    @apiGroup Study
    @apiParam {String} filepaths PACS中图片地址
    @apiSuccessExample {json} Success-Response:
    {
      "data": [
        {
          "age": 61,
          "birthDate": "1957-10-29",
          "bodyPartExamined": "CHEST",
          "diagnosisAdvice": "",
          "gender": "FEMALE",
          "hospital": "hosp",
          "labelNum": 0,
          "modality": "CT",
          "patientID": "1001203",
          "patientName": "YYY",
          "result": "pending",
          "reverse": false,
          "state": "pull_data",
          "studyDate": "2018-12-05",
          "studyTime": "10:23:02",
          "studyUID": "1.2.392.200036.9116.2.5.1.3268.2060439916.1525305902.560145",
          "submitted": false,
          "taskID": "d74e82a4-f2b2-11e8-b0fa-0242ac120003",
          "uploadDate": "2018-12-05 10:23:02",
          "user_privilege": [
            "reporter",
            "normal"
          ]
        }
      ],
      "message": "OK",
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "statusCode": 1200
    }
    """
    res = response()
    params = request.json
    if 'filepaths' not in params:
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)
    filepaths = params['filepaths']

    logger.info("PACS module upload file")
    res = response()
    userID = current_user.get_id()
    logger.info("process files userID {}(PACS)".format(userID))
    studylist = process_pacs_filepaths(filepaths, userID)

    res.statusCode = 1000
    res.data = studylist
    return make_response(jsonify(res.to_dict()), 200)


def do_screenshot(img_file, output_path, center, size, ww, wc):
    image = sitk.ReadImage(img_file)
    img_arr = sitk.GetArrayFromImage(image)
    min_grey = int(wc - ww / 2)
    max_grey = int(wc + ww / 2)
    grey_range = max_grey - min_grey
    img_arr[img_arr < min_grey] = min_grey
    img_arr[img_arr > max_grey] = max_grey
    img_arr = img_arr - min_grey
    img_arr = img_arr.astype(np.float32) / float(grey_range)
    img_arr = img_arr * 255
    img_arr = img_arr.squeeze()
    img_arr = img_arr.astype(np.uint8)
    length = min(center[0], int(size / 2), img_arr.shape[0] - center[0],
                 center[1], int(size / 2), img_arr.shape[1] - center[1])
    x_min = center[0] - length
    x_max = center[0] + length
    y_min = center[1] - length
    y_max = center[1] + length
    img_arr = img_arr[y_min:y_max, x_min:x_max]
    scipy.misc.imsave(output_path, img_arr)


def capture_screenshot():
    """

    :return:
    """
    res = response()
    userID = current_user.get_id()
    taskID = request.form.get('taskID', '')
    if taskID == '':
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    report_info = db_find_one("report", {"taskID": taskID})
    if not report_info:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    report = parseReportInfo(report_info)
    studyUID = report.studyUID

    if not os.path.exists(SCREENSHOT_ROOT):
        os.makedirs(SCREENSHOT_ROOT)
    uid = str(uuid.uuid1())
    url = request.form.get("url", None)
    if not url:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
    url = str(url)
    if report.modality == 'CT':
        label_info = db_find_one("label", {"taskID": taskID})
        if label_info is None:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)

        labelID = request.form.get('labelID', '')
        seriesID = request.form.get('seriesID', '')
        size = request.form.get("screenshotSize", 0)
        ww = request.form.get("ww", 2)
        wc = request.form.get("wc", 0)

        try:
            size = int(size)
            ww = float(ww)
            wc = float(wc)
        except ValueError:
            res.statusCode = 1100
            return make_response(jsonify(res.to_dict()), 200)
        if labelID == '' or seriesID == '':
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        labelist = parseLabelInfo(label_info)
        labelist = filter(lambda l: l.seriesID == seriesID, labelist)
        target_labelist = filter(lambda l: l.labelID == labelID, labelist)
        if len(target_labelist) == 0:
            res.statusCode = 1200
            return make_response(jsonify(res.to_dict()), 200)
        label = target_labelist[0]
        labelist.remove(label)

        uid = label.labelID
        folderpath = os.path.join(SCREENSHOT_ROOT, uid)
        screenshot_name = os.path.join(folderpath, (taskID + ".png"))
        if not os.path.exists(folderpath):
            os.makedirs(folderpath)
        center = (label.x, label.y)
        do_screenshot(url, screenshot_name, center, size, ww, wc)
        label.url = screenshot_name
        labelist.append(label)
        db_update_one("label", {"taskID": taskID}, saveLabelist(labelist, studyUID, taskID, label_info["labelType"]))
        socket_emit('updateLabel', {'data': "done", 'userID': userID, 'taskID': taskID})

    new_resource = {
        "uid": uid,
        "img_path": folderpath,
        "filename": url
    }
    db_insert_one("resource", new_resource)
    if report.modality == 'DX':
        report.screenshotURLs.append(url)
    else:
        report.screenshotURLs.append(label.url)

    db_update_one("report", {"taskID": taskID}, report.to_db())
    socket_emit('updateReport', {'data': "done", 'userID': userID, 'taskID': taskID})

    res.data = ""
    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)
