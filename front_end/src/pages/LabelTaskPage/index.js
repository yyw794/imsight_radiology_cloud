import React, { Component } from 'react'
import {withRouter} from 'react-router'
import api from '../../api'
import {Layout, Button, Table, Menu, Icon} from 'antd'
import Logo from '../../component/Logo'
import SignoutButton from '../../component/SignoutButton'
import LoadingWrapper from '../../component/LoadingWrapper'
import {VIEW_MODALITY_URL} from '../../component/StudyTable'
import { GetLocaledText } from '../../localedFuncs'
import {FormattedMessage, injectIntl} from 'react-intl'

const { Header, Content, Footer } = Layout
const SubMenu = Menu.SubMenu

import './index.less'

const STATUS_MAP = {
  'unlabeled': <FormattedMessage id = "LabelTaskPage.labelStatus.unlabeled"/>,
  'labeled': <FormattedMessage id = "LabelTaskPage.labelStatus.unchecked"/>,
  'reviewed': <FormattedMessage id = "LabelTaskPage.labelStatus.checked"/>,
  'failed': <FormattedMessage id = "LabelTaskPage.labelStatus.failed"/>
}

const columns = [
  {
    title: <FormattedMessage id = "LabelTaskPage.table-header.index"/>,
    dataIndex: 'studyUID',
    key: 'studyUID',
  },
  {
    title: <FormattedMessage id = "LabelTaskPage.table-header.labelTime"/>,
    dataIndex: 'labelTime',
    key: 'labelTime',
    sorter: (a, b) => {
      let timeA = a.labelTime ? a.labelTime : '0000-00-00 00:00:00'
      let timeB = b.labelTime ? b.labelTime : '0000-00-00 00:00:00'
      if (timeA > timeB) {
        return 1
      } else if (timeA < timeB) {
        return -1
      } else {
        return 0
      }
    },
    render: value => value ? value : '-'
  },
  {
    title: <FormattedMessage id = "LabelTaskPage.table-header.status"/>,
    dataIndex: 'labelStatus',
    key: 'labelStatus',
    filters: [{
      text:<FormattedMessage id = "LabelTaskPage.labelStatus.unlabeled"/>,
      value: 'unlabeled',
    }, {
      text: <FormattedMessage id = "LabelTaskPage.labelStatus.unchecked"/>,
      value: 'labeled',
    }, {
      text: <FormattedMessage id = "LabelTaskPage.labelStatus.checked"/>,
      value: 'reviewed',
    }, {
      text: <FormattedMessage id = "LabelTaskPage.labelStatus.failed"/>,
      value: 'failed',
    }],
    onFilter: (value, record) => record.labelStatus === value,
    render: value => STATUS_MAP[value]
  },
]

class LabelTaskPage extends Component {
  constructor(props) {
    super(props)
    if (props.userRole === 'labeler') {
      this.taskList = props.data.tasks
      this.state = {
        tasks: this.taskList.filter(task => task.modality === props.data.defaultKey),
        selectedKeys: [props.data.defaultKey],
      }
    } else {
      this.state = {
        tasks: props.data.tasks,
        selectedKeys: [props.data.defaultKey],
      }
    }
  }

  applyData = () => {
    api.applyLabelTasks({modality: this.state.selectedKeys[0].split('/')[0]})
    .then(data => {
      this.taskList = this.taskList.concat(data)
      this.setState({
        tasks: this.taskList.filter(task => task.modality === this.state.selectedKeys[0].split('/')[0])
      })
    })
  }

  renderMenu() {
    if (this.props.userRole === 'labeler') {
      return (
        <Menu
          className='task-menu'
          mode="inline"
          theme="dark"
          selectedKeys={this.state.selectedKeys}
          onSelect={(v) => this.setState({
            selectedKeys: [v.key],
            tasks: this.taskList.filter(task => task.modality === v.key),
          })}
        >
          <Menu.Item key="DX">
            <Icon type="folder" />
            <span>DX</span>
          </Menu.Item>
          <Menu.Item key="CT">
            <Icon type="folder" />
            <span>CT</span>
          </Menu.Item>
        </Menu>
      )
    } else {
      return (
        <Menu
          className='task-menu'
          mode="inline"
          theme="dark"
          defaultOpenKeys={['DX']}
          selectedKeys={this.state.selectedKeys}
          onSelect={(v) => {
            let [modality, userID] = v.key.split('/')
            api.getLabelTasks({modality, userID})
            .then(tasks => {
              this.setState({
                selectedKeys: [v.key],
                tasks,
              })
            })
            .catch(err => message.error(GetLocaledText(this,"LabelTaskPage.task-menu.getListFailed")))
          }}
        >
          <SubMenu key="DX" title={<span><Icon type="folder" /><span>DX</span></span>}>
            {this.props.data.labelers.map(labeler => <Menu.Item key={`DX/${labeler}`}><Icon type="user" />{labeler}</Menu.Item>)}
          </SubMenu>
          <SubMenu key="CT" title={<span><Icon type="folder" /><span>CT</span></span>}>
            {this.props.data.labelers.map(labeler => <Menu.Item key={`CT/${labeler}`}><Icon type="user" />{labeler}</Menu.Item>)}
          </SubMenu>
        </Menu>
      )
    }
  }

  render() {
    const role = this.props.userRole
    const tasks = this.state.tasks
    return (
      <div id='label-tasks-page' className='page'>
        <Header>
          <Logo/>
          <SignoutButton/>
        </Header>
        <Content className='content'>
          <div className='content-title'>
            <h3 style={{float: 'left'}}>
              {role === 'labeler' ? GetLocaledText(this,"LabelTaskPage.label-tasks-page.myLabels"): GetLocaledText(this,"LabelTaskPage.label-tasks-page.checkLabels")}
            </h3>
            {
              role === 'labeler' ?
              <Button type="primary" style={{float: 'right'}}
                disabled={this.state.tasks.some(task => task.labelStatus !== 'reviewed')}
                onClick={this.applyData}>
                <FormattedMessage id="LabelTaskPage.applyData"/>
              </Button> : ''
            }
          </div>
          <div className='content-body'>
            {this.renderMenu()}
            <div className='table-wrapper'>
              <Table
                columns={columns}
                className='label-tasks-table'
                dataSource={tasks}
                onRow={(record) => {
                  return {
                    onClick: () => {
                      const {modality, studyUID} = record
                      let pathname
                      if (role === 'reviewer') {
                        let labeler = this.state.selectedKeys[0].split('/')[1]
                        pathname = `/${VIEW_MODALITY_URL[modality]}/${studyUID.replace(/\./g, '-')}/${labeler}`
                      } else {
                        pathname = `/${VIEW_MODALITY_URL[modality]}/${studyUID.replace(/\./g, '-')}`
                      }
                      this.props.history.push(pathname)
                    }
                  }
                }}
              />
              <div className='statistics'>
                <FormattedMessage id="LabelTaskPage.applied"/> <span className='num'>{tasks.length}</span> <FormattedMessage id="LabelTaskPage.count"/>
                <FormattedMessage id="LabelTaskPage.labeled"/> <span className='num'>{tasks.filter(t => t.labelStatus !== 'unlabeled').length}</span> <FormattedMessage id="LabelTaskPage.count"/>
                <FormattedMessage id="LabelTaskPage.examinated"/><span className='num'>{tasks.filter(t => t.labelStatus === 'reviewed').length}</span> <FormattedMessage id="LabelTaskPage.count"/>
              </div>
            </div>
          </div>
        </Content>
      </div>
    )
  }

}

async function getData(props) {
  if (props.userRole === 'labeler') {
    let tasks = await api.getLabelTasks({})
    return await {tasks, defaultKey: 'DX'}
  } else {
    let labelers = await api.getLabelers({})
    if (labelers.length < 1) {
      return await {tasks: [], labelers}
    }
    let tasks = await api.getLabelTasks({modality: 'DX', userID: labelers[0]})
    return await {tasks, labelers, defaultKey: `DX/${labelers[0]}`}
  }

}

LabelTaskPage = withRouter(LabelTaskPage)
LabelTaskPage = LoadingWrapper(LabelTaskPage, getData)

export default injectIntl(LabelTaskPage)
