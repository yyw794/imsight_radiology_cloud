#!/bin/python
# coding=utf-8
import hashlib
import os
import sys

from flask import make_response, jsonify, request, abort, session
from flask_login import login_user, logout_user, current_user

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one
from models.response.response import response
from models.user.User import parseUserInfo, validateUserInfo
from config import NEED_AUTH


def load_user(user_id):
    user_db = db_find_one("user", {"account": user_id})
    user_info = validateUserInfo(user_db)
    user = None if user_info is None else parseUserInfo(user_info)
    return user


def login():
    """
    @api {post} /auth/login  用户登陆
    @apiVersion 0.0.1
    @apiGroup Auth
    @apiParam {String} account 用户名
    @apiParam {String} password 密码
    @apiParamExample {json} Request-Example:
                 {"account":"example","password":"example"}
    @apiSuccessExample {json} Success-Response:
    {
      "data": {
        "account": "example",
        "contact": "example",
        "email": "example",
        "hospital": "example",
        "nickname": "example",
        "type": "normal"
      },
      "message": "OK",
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "data": {},
      "message": "INVALID PASSWORD",
      "statusCode": 1103
    }
    """
    res = response()
    if not NEED_AUTH:
        user = load_user("imsightmed")
        login_user(user)
        res.data = user.to_dict()
        res.statusCode = 1000
        return make_response(jsonify(res.to_dict()), 200)
    if not request.json:
        res.statusCode = 1112
        return make_response(jsonify(res.to_dict()), 200)

    user_info = validateUserInfo(request.json)
    if user_info is None:
        res.statusCode = 1101
        return make_response(jsonify(res.to_dict()), 200)

    user = load_user(user_info['account'])
    if user is None:
        res.statusCode = 1104
        return make_response(jsonify(res.to_dict()), 200)

    hashed_pwd = hashlib.sha256(user_info['password']).hexdigest()
    if user.password != hashed_pwd:
        res.statusCode = 1103
        return make_response(jsonify(res.to_dict()), 200)

    session.permanent = True
    login_user(user)
    res.data = user.to_dict()
    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)


def logout():
    """
    @api {post} /auth/logout  用户退出
    @apiVersion 0.0.1
    @apiGroup Auth
    @apiSuccessExample {json} Success-Response:
    {
      "data": {},
      "message": "OK",
      "statusCode": 1000
    }
    """
    res = response()
    logout_user()
    res.data = {}
    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)
