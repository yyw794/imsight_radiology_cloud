# -*- coding: utf-8 -*-
import httplib2
import json
import initconfig as config
import logging
import logger
import os
from urllib import urlencode
log = logging.getLogger("PACSModule")


# 向dcmtk发送查找列表消息
def dcmtk_find_patient_list(condition):
	try:
		url = "http://{}:{}/pacsAPI/find".format(config.get_dcmtk_ip(), config.get_dcmtk_port())
		http = httplib2.Http(timeout=300)
		headers = {'content-type': 'application/json'}
		data = urlencode(condition)
		resp, content = http.request(url+"?"+data, 'GET', body=json.dumps({}), headers=headers)
		if resp['status'] == '200':
			return content
		else:
			return None
	except Exception as e:
		log.error(e)
		return None


# 向dcmtk发送请求数据消息
def dcmtk_move_study_uid(studyUID,studyDate):
	try:
		url = "http://{}:{}/pacsAPI/move?studyUID={}&studyDate={}".\
			format(config.get_dcmtk_ip(), config.get_dcmtk_port(), studyUID, studyDate)
		http = httplib2.Http(timeout=3000)
		headers = {'content-type': 'application/json'}
		resp, content = http.request(url, 'GET', body=json.dumps({}), headers=headers)
		if resp['status'] == '200':
			return content
		else:
			return None
	except Exception as e:
		log.error(e)
		return None


# 向dcmtk发送get请求数据消息
def dcmtk_get_study_uid(studyUID,studyDate):
	try:
		url = "http://{}:{}/pacsAPI/get?studyUID={}&studyDate={}".\
			format(config.get_dcmtk_ip(), config.get_dcmtk_port(), studyUID, studyDate)
		http = httplib2.Http(timeout=3000)
		headers = {'content-type': 'application/json'}
		resp, content = http.request(url, 'GET', body=json.dumps({}), headers=headers)
		if resp['status'] == '200':
			return content
		else:
			return None
	except Exception as e:
		log.error(e)
		return None


# 向dcmtk发送握手消息
def dcmtk_echo():
	try:
		url = "http://{}:{}/pacsAPI/echo".format(config.get_dcmtk_ip(),config.get_dcmtk_port())
		http = httplib2.Http(timeout=30)
		headers = {'content-type': 'application/json'}
		resp, content = http.request(url, 'GET', body=json.dumps({}), headers=headers)
		if resp['status'] == '200':
			return content
		else:
			return None
	except Exception as e:
		log.error(e)
		return None
