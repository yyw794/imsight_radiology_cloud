import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import {Table, Row, Col, Icon, Modal, Button, Checkbox, message} from 'antd'
import _ from 'underscore'
import Immutable from 'immutable'

import { calcLiradsScore } from '../../../util'

import api from '../../../api'

import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../../localedFuncs'

import './index.less'

function getRiskText(component, prob) {
  if (prob < 0.5) {
    return GetLocaledText(component, "NodulePanel.risk-text.benign")
  } else if (prob >= 0.5 && prob < 0.8) {
    return GetLocaledText(component, "NodulePanel.risk-text.low")
  } else if (prob >= 0.8 && prob < 0.9) {
    return GetLocaledText(component, "NodulePanel.risk-text.medium")
  } else if(prob>=0.9&&prob<1.0) {
    return GetLocaledText(component, "NodulePanel.risk-text.high")
  } else {
    return "HCC"
  }
}

function getRiskColor(prob) {
  if (prob < 0.5) {
    return '#00ff00'
  } else if (prob >= 0.5 && prob < 0.8) {
    return '#ffff00'
  } else if (prob >= 0.8 && prob < 0.9) {
    return '#ffa501'
  } else {
    return '#ff0000'
  }
}

function renderDetail(component, nodule, nodulePanelState, setNodulePanelState, propsData,maxZ,zReverse) {
  if (nodule) {
    let lvPropsNodule = propsData.find(inst => inst.labelID == nodule.labelID)
    let labelIDcurrent = nodule.labelID

    if (lvPropsNodule.mfsWashout == undefined || lvPropsNodule.mfsEnhancing  == undefined ||
      lvPropsNodule.mfsThresholdGrowth  == undefined || lvPropsNodule.isAPE == undefined ) {
       lvPropsNodule.mfsWashout = false
       lvPropsNodule.mfsEnhancing = false
       lvPropsNodule.mfsThresholdGrowth = false
       lvPropsNodule.isAPE = false
      }

    const studyUIDcurrent = propsData[0].studyUID

    let majorFeatures = 0
    if (lvPropsNodule.mfsWashout) ++majorFeatures
    if (lvPropsNodule.mfsEnhancing) ++majorFeatures
    if (lvPropsNodule.mfsThresholdGrowth) ++majorFeatures

    function handleMfsWashoutChange() {
      lvPropsNodule.mfsWashout = !lvPropsNodule.mfsWashout
      updateDbLabelData()
    }

    function handleAPEChange() {
      lvPropsNodule.isAPE = !lvPropsNodule.isAPE
      updateDbLabelData()
    }

    function handleMfsEnhancingChange() {
      lvPropsNodule.mfsEnhancing = !lvPropsNodule.mfsEnhancing
      updateDbLabelData()
    }

    function handleMfsThresholdGrowthChange() {
      lvPropsNodule.mfsThresholdGrowth = !lvPropsNodule.mfsThresholdGrowth
      updateDbLabelData()
    }
    
    function updateDbLabelData(){
      if(_.isFunction(component.props.onTumorChanged))
      {
        component.props.onTumorChanged(labelIDcurrent,lvPropsNodule.isAPE,lvPropsNodule.mfsWashout,lvPropsNodule.mfsEnhancing,lvPropsNodule.mfsThresholdGrowth)
      }
    }

    function getClassNames(cellNum) {
      if (cellNum == 0) {
        if (!lvPropsNodule.isAPE) {
          if (majorFeatures == 0) {
            if (nodule.diameter.toFixed(1) < 20) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 1) {
        if (!lvPropsNodule.isAPE) {
          if (majorFeatures == 0) {
            if (nodule.diameter.toFixed(1) >= 20) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 2) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures == 0) {
            if (nodule.diameter.toFixed(1) < 10) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 3) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures == 0) {
            if (nodule.diameter.toFixed(1) >= 10 && nodule.diameter.toFixed(1) <= 19) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 4) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures == 0) {
            if (nodule.diameter.toFixed(1) >= 20) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 5) {
        if (!lvPropsNodule.isAPE) {
          if (majorFeatures == 1) {
            if (nodule.diameter.toFixed(1) < 20) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 6) {
        if (!lvPropsNodule.isAPE) {
          if (majorFeatures == 1) {
            if (nodule.diameter.toFixed(1) >= 20) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 7) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures == 1) {
            if (nodule.diameter.toFixed(1) < 10) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 8) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures == 1) {
            if (nodule.diameter.toFixed(1) >= 10 && nodule.diameter.toFixed(1) <= 19) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 9) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures == 1) {
            if (nodule.diameter.toFixed(1) >= 20) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 10) {
        if (!lvPropsNodule.isAPE) {
          if (majorFeatures >= 2) {
            if (nodule.diameter.toFixed(1) < 20) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 11) {
        if (!lvPropsNodule.isAPE) {
          if (majorFeatures >= 2) {
            if (nodule.diameter.toFixed(1) >= 20) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 12) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures >= 2) {
            if (nodule.diameter.toFixed(1) < 10) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 13) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures >= 2) {
            if (nodule.diameter.toFixed(1) >= 10 && nodule.diameter.toFixed(1) <= 19) {
              return 'highlight'
            }
          }
        }
      }
      if (cellNum == 14) {
        if (lvPropsNodule.isAPE) {
          if (majorFeatures >= 2) {
            if (nodule.diameter.toFixed(1) >= 20) {
              return 'highlight'
            }
          }
        }
      }
      return ''
    }


    if (!nodule) return ''

    nodule.LI_RADS = calcLiradsScore(nodule.isAPE, majorFeatures, nodule.diameter.toFixed(1))
    propsData.find(inst => inst.labelID == nodule.labelID).LI_RADS = nodule.LI_RADS

    return (
      <div className='nodule-detail'>
        <Row>
          <Col span={12}>{GetLocaledText(component, "NodulePanel.nodule-detail.size")}</Col>
          <Col span={12}>{nodule.diameter.toFixed(1)}</Col>
        </Row>
        {
        <Row>
          <Col span={12}>{GetLocaledText(component, "NodulePanel.nodule-detail.malignancy")}</Col>
          <Col span={12} style={{color:getRiskColor(nodule.malg)}}>{getRiskText(component,nodule.malg)}</Col>
        </Row>
        }
        <Row>
          <Col span={12}>{GetLocaledText(component, "NodulePanel.nodule-detail.location")}</Col>
          <Col span={12}>{"["+nodule.coord.x + ","+nodule.coord.y+","+zReverse?(maxZ-nodule.coord.z+1):nodule.coord.z+"]"}</Col>
        </Row>
        <Row>
          <Col span={12}>{GetLocaledText(component, "NodulePanel.nodule-detail.average-density")}</Col>
          <Col span={12}>{nodule.avgHU.toFixed(1)}</Col>
        </Row>
        <Row>
          <Col span={12}>{GetLocaledText(component, "NodulePanel.nodule-detail.volume")}</Col>
          <Col span={12}>{nodule.volume.toFixed(1)}</Col>
        </Row>
        <table className="tg"
          style={{
            MozUserSelect:'none',
            WebkitUserSelect:'none',
            msUserSelect:'none',
          }}
        >
          <tbody>
            <tr>
              <td className="tg-yw4l" rowSpan="5">
                <div>
                  <Row>{GetLocaledText(component, "NodulePanelLiver.lirads.count-major-features.title")}</Row>
                  <Row>
                      <Checkbox
                        className='lirads-table-checkbox'
                        name="washout"
                        type="checkbox"
                        checked={lvPropsNodule.mfsWashout}
                        onChange={handleMfsWashoutChange}
                        style={{color: 'white', width: '100%'}}>
                        {GetLocaledText(component, "NodulePanelLiver.lirads.major-features.washout(not-peripheral)")}
                      </Checkbox>
                  </Row>
                  <Row>
                    <Checkbox
                      className='lirads-table-checkbox'
                      name="enhancing"
                      type="checkbox"
                      checked={lvPropsNodule.mfsEnhancing}
                      onChange={handleMfsEnhancingChange}
                      style={{color: 'white', width: '100%'}}>
                      {GetLocaledText(component, "NodulePanelLiver.lirads.major-features.enchancing-capsule")}
                    </Checkbox>
                  </Row>
                  <Row>
                    <Checkbox
                      className='lirads-table-checkbox'
                      name="threshold-growth"
                      type="checkbox"
                      checked={lvPropsNodule.mfsThresholdGrowth}
                      onChange={handleMfsThresholdGrowthChange}
                      style={{color: 'white', width: '100%'}}>
                      {GetLocaledText(component, "NodulePanelLiver.lirads.major-features.threshold-growth")}
                    </Checkbox>
                  </Row>
                  </div>
              </td>
              <td className="tg-yw4l" colSpan="1"></td>
              <td className="tg-yw4l" colSpan="2" style={{width: '35px'}}>{GetLocaledText(component, "NodulePanelLiver.lirads.APHE.false")}</td>
              <td className="tg-yw4l" colSpan="3">
                  <Checkbox
                    className='lirads-table-checkbox'
                    name="APE"
                    type="checkbox"
                    checked={lvPropsNodule.isAPE}
                    onChange={handleAPEChange}
                    style={{color: 'white', width: '100%'}}>
                    {GetLocaledText(component, "NodulePanelLiver.lirads.APHE.checkbox")}
                  </Checkbox>
              </td>
            </tr>
            <tr>
              <td className="tg-yw4l" colSpan="1">{GetLocaledText(component, "NodulePanelLiver.lirads.size")}</td>
              <td className="tg-yw4l" >&lt; 20</td>
              <td className="tg-yw4l" >≥ 20<br/></td>
              <td className="tg-yw4l" >&lt; 10</td>
              <td className="tg-yw4l" >10-19</td>
              <td className="tg-yw4l" >≥ 20</td>
            </tr>
            <tr>
              <td className="tg-yw4l">{GetLocaledText(component, "NodulePanelLiver.lirads.count-major-features.none")}</td>
              <td className={"tg-yw4l no-aphe none-mf less-20 lr-3 " + getClassNames(0)}><span>LR-3</span></td>
              <td className={"tg-yw4l no-aphe none-mf more-20 lr-3 " + getClassNames(1)}><span>LR-3</span></td>
              <td className={"tg-yw4l aphe none-mf less-10 lr-3 " + getClassNames(2)}><span>LR-3</span></td>
              <td className={"tg-yw4l aphe none-mf between-10-19 lr-3 " + getClassNames(3)}><span>LR-3</span></td>
              <td className={"tg-yw4l aphe none-mf more-20 lr-4 " + getClassNames(4)}><span>LR-4</span></td>
            </tr>
            <tr>
              <td className="tg-yw4l">{GetLocaledText(component, "NodulePanelLiver.lirads.count-major-features.one")}</td>
              <td className={"tg-yw4l no-aphe one-mf lr-3 " + getClassNames(5)}><span>LR-3</span></td>
              <td className={"tg-yw4l no-aphe one-mf lr-4 " + getClassNames(6)}><span>LR-4</span></td>
              <td className={"tg-yw4l aphe one-mf lr-4 " + getClassNames(7)}><span>LR-4</span></td>
              <td className={"tg-yw4l aphe one-mf lr-4 " + getClassNames(8)}><span>LR-4</span></td>
              <td className={"tg-yw4l aphe one-mf lr-5 " + getClassNames(9)}><span>LR-5</span></td>
            </tr>
            <tr>
              <td className="tg-yw4l">≥ {GetLocaledText(component, "NodulePanelLiver.lirads.count-major-features.two-or-above")}</td>
              <td className={"tg-yw4l no-aphe two-or-more-mf lr-4 " + getClassNames(10)}><span>LR-4</span></td>
              <td className={"tg-yw4l no-aphe two-or-more-mf lr-4 " + getClassNames(11)}><span>LR-4</span></td>
              <td className={"tg-yw4l aphe two-or-more-mf lr-4 " + getClassNames(12)}><span>LR-4</span></td>
              <td className={"tg-yw4l aphe two-or-more-mf lr-5 " + getClassNames(13)}><span>LR-5</span></td>
              <td className={"tg-yw4l aphe two-or-more-mf lr-5 " + getClassNames(14)}><span>LR-5</span></td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }

}

class NodulePanelLiver extends Component {
  constructor(props) {
    super(props)
    this.originData = Immutable.fromJS(this.props.data).sort(this.getSorter('malg', true))
    // this.originData = this.props.data)
    if (!props.fixedKey) {
      this.originData = this.originData.map((data, i) => data.set('key', i + 1))
      this.originData = this.originData.map(function(data){
        let majorFeatures = 0;
        if (data.get('mfsWashout')) ++majorFeatures;
        if (data.get('mfsEnhancing')) ++majorFeatures;
        if (data.get('mfsThresholdGrowth')) ++majorFeatures;
        return data.set('LI_RADS', calcLiradsScore(data.get('isAPE'), majorFeatures, data.get('diameter').toFixed(1)))
      })
      this.originData = this.originData.map(function(data){
        let LI_RADS = data.get('LI_RADS');
        if(LI_RADS==='LR-5') return data.set('malg',1.222);
        else if(LI_RADS==='LR-4') return data.set('malg',0.9522);
        else if(LI_RADS==='LR-3') return data.set('malg',0.8522);
        else if(LI_RADS==='LR-2') return data.set('malg',0.622);
        else return data.set('malg',0.322);
      })
    }
    this.state = {
      selectedNodule: null,
      selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS(),
      data: this.originData,
      sortColumn: 'malg',
    }
    this.pageSize = window.screen.width< window.screen.height ? 16:Math.round((window.screen.height-720)/25)

  }
  updateDataBasedOnLiradsScoreParams = (data) => {
      data.map(n => {
        let majorFeatures = 0
        if (n.mfsWashout) ++majorFeatures
        if (n.mfsEnhancing) ++majorFeatures
        if (n.mfsThresholdGrowth) ++majorFeatures
        n.LI_RADS = calcLiradsScore(n.isAPE, majorFeatures, n.diameter.toFixed(1))
      })
  }
  onResize = () => {
    this.pageSize = window.screen.width< window.screen.height ? 16:Math.round((window.screen.height-720)/25);
  }
  componentDidMount() {
    window.addEventListener("resize", _.debounce(this.onResize, 100)) //Jackey: Why delay it by using _.debounce, but not simply passing this.onResize?
    //window.addEventListener("resize", this.onResize);
  }

  componentWillMount() {

    this.setState({
      selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS(),
      mfsThresholdGrowth: false,
      mfsEnhancing: false,
      mfsWashout: false,
      isAPE: false
    })
  }

  componentWillReceiveProps(nextProps) {
    this.originData = Immutable.fromJS(nextProps.data).sort(this.getSorter('malg', true))
    if (!nextProps.fixedKey) {
      this.originData = this.originData.map((data, i) => data.set('key', i + 1))
      this.originData = this.originData.map(function(data){
        let majorFeatures = 0;
        if (data.get('mfsWashout')) ++majorFeatures;
        if (data.get('mfsEnhancing')) ++majorFeatures;
        if (data.get('mfsThresholdGrowth')) ++majorFeatures;
        return data.set('LI_RADS',  calcLiradsScore(data.get('isAPE'), majorFeatures, data.get('diameter').toFixed(1)))
      })
      this.originData = this.originData.map(function(data){
        let LI_RADS = data.get('LI_RADS');
        if(LI_RADS==='LR-5') return data.set('malg',1.222);
        else if(LI_RADS==='LR-4') return data.set('malg',0.9522);
        else if(LI_RADS==='LR-3') return data.set('malg',0.8522);
        else if(LI_RADS==='LR-2') return data.set('malg',0.622);
        else return data.set('malg',0.322);
      })
    }
    this.setState({
      selectedRowKeys: this.originData.filter(d => d.get('url')).map(d => d.get('key')).toJS(),
      data:this.originData,
      sortColumn: 'malg',
    })
  }

  getSorter(col, reverse=false) {
    return (a, b) => {
      let v1 = a.get(col)
      let v2 = b.get(col)
      if (v1 < v2) {
        return reverse ? 1 : -1
      }
      if (v1 > v2) {
        return reverse ? -1 : 1
      }
      if (v1 === v2) {
        return 0
      }
    }
  }

  sortBy(col) {
    let reverse = false
    if (['diameter'/* , 'malg', 'avgHU' */].includes(col)) {
      reverse = true
    }
    let data = this.props.data.sort(this.getSorter(col, reverse))
    this.props.data = data

    this.setState({
      data,
      sortColumn: col
    })
  }

  onSelectChange = (selectedRowKeys) => {
    let oldKeys = this.state.selectedRowKeys;
    let newKeys = selectedRowKeys;
    let add = newKeys.filter(x => !oldKeys.includes(x));
    if (add.length > 0) {
      let record = this.state.data.find(x => x.get('key') === add[0]).toJS()
      if (_.isFunction(this.props.onSelectNodule)) {
        this.props.onSelectNodule(record).then(() => _.isFunction(this.props.onAddScreenshot) && this.props.onAddScreenshot(record))
      }
    }
    let remove = oldKeys.filter(x => !newKeys.includes(x));
    if (remove.length > 0) {
      let record = this.state.data.find(x => x.get('key') === remove[0]).toJS()
      _.isFunction(this.props.onRemoveScreenshot) && this.props.onRemoveScreenshot(record)
    }

    this.setState({ selectedRowKeys });
  }

  getNodulePage(nodule) {
    let page = 1
    let idx = this.state.data.findIndex(n => n.get('labelID') === nodule.labelID)
    return Math.ceil((idx + 1) / this.pageSize)
  }

  selectNodule(selectedNodule) {
    let currentPage = this.getNodulePage(selectedNodule)
    this.setState({selectedNodule: selectedNodule})
  }

  addNodule = () => {
    Modal.confirm({
      title: GetLocaledText(this, "NodulePanelLiver.message.add-nodule"),
      onOk: this.props.onStartDetect
    })
  }

  deleteNodule = () => {
    if (!this.state.selectedNodule) return
    Modal.confirm({
      title: GetLocaledText(this, "NodulePanelLiver.message.delete-nodule"),
      onOk: () => {
        this.props.onDelete(this.state.selectedNodule.labelID)
        this.setState({selectedNodule: null})
      }
    })
  }

  render() {

    this.updateDataBasedOnLiradsScoreParams(this.props.data)

    const columns = [
      {
        title: GetLocaledText(this, "NodulePanel.table-header.number"),
        dataIndex: 'key',
        align: 'center',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          let text = `${this.props.keyPrefix ? this.props.keyPrefix : 'N'}${value}`
          if (record.new) {
            text += '*'
          }
          return text
        }
      },
      {
        title: GetLocaledText(this, "NodulePanel.table-header.diam"),
        dataIndex: 'diameter',
        align: 'center',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return value.toFixed(1)
        }
      },
      {
        title: GetLocaledText(this, "NodulePanel.table-header.malignancy"),
        dataIndex: 'malg',
        align: 'center',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return <span style={{color: getRiskColor(value)}}>{getRiskText(this,value)}</span>
        }
      },
      {
        title: GetLocaledText(this, "NodulePanelLiver.table-header.series-number"),
        dataIndex: 'seriesNumber',
        align: 'center',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return value
        }
      },
      {
        title: GetLocaledText(this, "NodulePanel.table-header.layer-index"),
        dataIndex: 'coord.z',
        align: 'center',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
	  return this.props.zReverse?(Number(this.props.maxZ)-record.coord.z+1):(record.coord.z+1)
        }
      },
      {
        title: 'LI-RADS',
        dataIndex: 'LI_RADS',
        align: 'center',
        onHeaderCell: (column) => {
          return {
            onClick: () => {
              this.sortBy(column.dataIndex)
            }
          }
        },
        render: (value, record) => {
          return (
            <span
              style={{
                color: value == 'LR-5' ? 'red' : value == 'LR-4' ? 'orange' : 'yellow'
              }}>{value}
            </span>
          )
        }
      },
    ]

    const { selectedRowKeys } = this.state;
    const rowSelection = {
      selectedRowKeys,
      onChange: this.onSelectChange,
      columnWidth: 20,
      hideDefaultSelections: true,
    };

    return (
        <div className={'nodule-panel'} >
          <div className='nodule-table-liver'>
            <Table
              rowSelection={this.props.hideSelection ? null : rowSelection}
              align='center'
              columns={columns}
              dataSource={this.state.data.toJS()}
              rowClassName={(record) => {
                const selectedNodule = this.state.selectedNodule
                return selectedNodule && record.labelID === selectedNodule.labelID ?
                  'active' : ''
              }}
              size="small"
              pagination={{
                pageSize: this.pageSize,
                simple: true,
                showTotal: (total, range) => {
                <span>
                {GetLocaledText(this, "NodulePanelLiver.page-handle.pre-total")}
                <span className='nodule-count'>{total}</span>
                {GetLocaledText(this, "NodulePanelLiver.page-handle.post-total")}
                </span>
                },
                onChange: (currentPage) => this.setState({currentPage}),
                current: this.state.currentPage
              }}
              onRow={record => {
                return {
                  onClick: () => {
                    this.setState({selectedNodule: record});
                    if(_.isFunction(this.props.onSelectNodule)){this.props.onSelectNodule(record)}
                  }
                }
              }}/>
            {
              this.state.data.size > 0 ? (
                <span className='ant-pagination-total-text'>
                  {GetLocaledText(this, "NodulePanelLiver.page-handle.pre-total")}
                <span className='nodule-count'>{this.state.data.size}</span>
                  {GetLocaledText(this, "NodulePanelLiver.page-handle.post-total")}
                </span>
              )
              :
              ''
            }
            {
              !this.props.hideEditNoduleBtn ?
              <div className='action-btns'>
                <Icon type="delete" onClick={this.deleteNodule}/>
              </div>
              :
              ''
           }
          </div>
          {renderDetail(this, this.state.selectedNodule, this.state, (state) => {this.setState(state)}, this.props.data,this.props.maxZ,this.props.zReverse)}

        </div>
    );
  }

}

export {getRiskText, getRiskColor};
export default injectIntl(NodulePanelLiver,{withRef: true});
