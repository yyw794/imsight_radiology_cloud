#!/bin/python
#coding=utf-8

import datetime

class Template():
    def __init__(self):
        self.id = ""
        self.bodyPartExamined = ""
        self.templateName = ""
        self.createDate = datetime.datetime.today()
        self.userID = ""
        self.modality = ""
        self.imagingFind = ""
        self.diagnosisAdvice = ""

    def set_id(self, index, modality, bodyPartExamined):
        self.id = "USR" + str(modality).upper() + str(bodyPartExamined).upper() + '{0:03d}'.format(index)

    def set_sys_id(self, index, modality, bodyPartExamined):
        self.id = "SYS" + str(modality).upper() + str(bodyPartExamined).upper() + '{0:03d}'.format(index)

    def to_dict(self):
        return {
            "templateID": self.id,
            "bodyPartExamined": self.bodyPartExamined,
            "templateName": self.templateName,
            "createDate": (self.createDate).strftime('%Y%m%d'),
            "modality": self.modality,
            "imagingFind": self.imagingFind,
            "diagnosisAdvice": self.diagnosisAdvice
        }

    def to_db(self):
        return {
            "templateID": self.id,
            "bodyPartExamined": self.bodyPartExamined,
            "templateName": self.templateName,
            "createDate": self.createDate,
            "modality": self.modality,
            "imagingFind": self.imagingFind,
            "diagnosisAdvice": self.diagnosisAdvice
        }

required_key = ['templateID', 'templateName', 'bodyPartExamined', 'createDate', 'modality', \
                'imagingFind', 'diagnosisAdvice']

def parseTemplateInfo(info):
    if type(info) is not dict:
        raise ValueError("parseTemplateInfo input must be dict type, while type is {}".format(type(info)))

    if not all(key in info for key in required_key):
        raise ValueError("parseTemplateInfo key mismatch", required_key, info.keys())

    new_template = Template()
    new_template.id = info['templateID']
    new_template.templateName = info['templateName']
    new_template.bodyPartExamined = info['bodyPartExamined']
    new_template.createDate = info['createDate']
    new_template.modality = info['modality']
    new_template.imagingFind = info['imagingFind']
    new_template.diagnosisAdvice = info['diagnosisAdvice']

    return new_template


def saveTemplatelist(templatelist, group, userID):
    if type(templatelist) is not list:
        raise ValueError("saveToollist input must be list type, while type is {}".format(type(templatelist)))

    ret = {'group': group, 'userID': userID, 'templates': []}
    for template in templatelist:
        ret['templates'].append(template.to_db())
    return ret
