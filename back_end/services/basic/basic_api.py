#!/bin/python
# coding=utf-8

import logging
import shutil

import uuid
from socketIO_client import SocketIO, LoggingNamespace

from config import SOCKET_PROTOCOL, DICOM_ROOT
from models.database.database_ops import *

logger = logging.getLogger("radiology")
socketIO_ = None


def socket_emit(key, value):
    global socketIO_
    logger.info('socket_emit:{}'.format(key))
    if socketIO_ is None:
        if SOCKET_PROTOCOL == 'https':
            socketIO_ = SocketIO('https://flask', 6000, LoggingNamespace, verify=False)
        else:
            socketIO_ = SocketIO('http://flask', 6000, LoggingNamespace)
    value["uid"] = uuid.uuid4().get_hex()
    logger.debug("socket_emit: uid=%s, value=%s", value["uid"], value)
    socketIO_.emit(key, value)
