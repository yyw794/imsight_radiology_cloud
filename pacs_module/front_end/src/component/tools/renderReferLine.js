import * as cornerstone from 'cornerstone-core'
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import noduleBox from './noduleBox'

const {projectPatientPointToImagePlane, planePlaneIntersection, toolColors, toolStyle} = cornerstoneTools

function convertToVector3 (arrayOrVector3) {
  if (arrayOrVector3 instanceof cornerstoneMath.Vector3) {
    return arrayOrVector3;
  }

  return new cornerstoneMath.Vector3(arrayOrVector3[0], arrayOrVector3[1], arrayOrVector3[2]);
}

function calculateReferenceLine(targetImagePlane, referenceImagePlane) {
  const points = planePlaneIntersection(targetImagePlane, referenceImagePlane);

  if (!points) {
    return;
  }

  return {
    start: projectPatientPointToImagePlane(points.start, targetImagePlane),
    end: projectPatientPointToImagePlane(points.end, targetImagePlane)
  };
}

// custom renference line renderer
const renderReferLine = (context, eventData, targetElement, referenceElement) => {
    const targetImage = cornerstone.getEnabledElement(targetElement).image;
    const referenceImage = cornerstone.getEnabledElement(referenceElement).image;

    // Make sure the images are actually loaded for the target and reference
    if (!targetImage || !referenceImage) {
      return;
    }
    console.log(targetImage.imageId)
    const targetImagePlane = cornerstone.metaData.get('imagePlaneModule',targetImage.imageId);
    const referenceImagePlane = cornerstone.metaData.get('imagePlaneModule', referenceImage.imageId);
    // Make sure the target and reference actually have image plane metadata
    if (!targetImagePlane ||
          !referenceImagePlane ||
          !targetImagePlane.rowCosines ||
          !targetImagePlane.columnCosines ||
          !targetImagePlane.imagePositionPatient ||
          !referenceImagePlane.rowCosines ||
          !referenceImagePlane.columnCosines ||
          !referenceImagePlane.imagePositionPatient) {
      return;
    }

    // The image planes must be in the same frame of reference
    if (targetImagePlane.frameOfReferenceUID !== referenceImagePlane.frameOfReferenceUID) {
      return;
    }

    targetImagePlane.rowCosines = convertToVector3(targetImagePlane.rowCosines);
    targetImagePlane.columnCosines = convertToVector3(targetImagePlane.columnCosines);
    targetImagePlane.imagePositionPatient = convertToVector3(targetImagePlane.imagePositionPatient);
    referenceImagePlane.rowCosines = convertToVector3(referenceImagePlane.rowCosines);
    referenceImagePlane.columnCosines = convertToVector3(referenceImagePlane.columnCosines);
    referenceImagePlane.imagePositionPatient = convertToVector3(referenceImagePlane.imagePositionPatient);

    // The image plane normals must be > 30 degrees apart
    const targetNormal = targetImagePlane.rowCosines.clone().cross(targetImagePlane.columnCosines);
    const referenceNormal = referenceImagePlane.rowCosines.clone().cross(referenceImagePlane.columnCosines);
    let angleInRadians = targetNormal.angleTo(referenceNormal);

    angleInRadians = Math.abs(angleInRadians);

    if (angleInRadians < 0.5) { // 0.5 radians = ~30 degrees
      return;
    }

    const referenceLine = calculateReferenceLine(targetImagePlane, referenceImagePlane);

    if (!referenceLine) {
      return;
    }

    // don't draw until get two cross referenceLines
    const toolData = cornerstoneTools.getToolState(targetElement, 'referenceLines');
    let intersection
    let line1
    let line2
    if (!toolData.anotherReferLine) {
      toolData.anotherReferLine = referenceLine
    } else {
      line1 = Object.assign({}, referenceLine)
      line2 = Object.assign({}, toolData.anotherReferLine)
      intersection = cornerstoneMath.lineSegment.intersectLine(line1, line2)
      toolData.anotherReferLine = null
    }

    // const color = toolColors.getActiveColor();
    // const lineWidth = toolStyle.getToolWidth();
    const color = 'yellow'
    const lineWidth = 1

    if (intersection && !isNaN(intersection.x) && !isNaN(intersection.y)) {
      // console.log(eventData.element, 'referline');
      // context.rect(intersection.x - 10, intersection.y - 10, 20, 20);
      const intersectionCanvas = cornerstone.pixelToCanvas(eventData.element, intersection);
      if (line1.start.y !== line1.end.y) {
        let tmp = line1
        line1 = line2
        line2 = tmp
      }
      const start1Canvas = cornerstone.pixelToCanvas(eventData.element, line1.start);
      const end1Canvas = cornerstone.pixelToCanvas(eventData.element, line1.end);
      const start2Canvas = cornerstone.pixelToCanvas(eventData.element, line2.start);
      const end2Canvas = cornerstone.pixelToCanvas(eventData.element, line2.end);
      const viewport = cornerstone.getViewport(eventData.element)
      const size = noduleBox.BOX_SIZE * viewport.scale
      // Draw the referenceLines
      context.setTransform(1, 0, 0, 1, 0, 0);

      context.save();
      context.beginPath();
      context.strokeStyle = color;
      context.lineWidth = lineWidth;
      context.setLineDash([5, 2])

      context.moveTo(start1Canvas.x, start1Canvas.y);
      context.lineTo(Math.max(intersectionCanvas.x - size / 2, start1Canvas.x), start1Canvas.y);
      context.moveTo(intersectionCanvas.x + size / 2, start1Canvas.y);
      context.lineTo(end1Canvas.x, end1Canvas.y);

      context.moveTo(start2Canvas.x, start2Canvas.y);
      context.lineTo(start2Canvas.x, Math.max(intersectionCanvas.y - size / 2, start2Canvas.y));
      context.moveTo(start2Canvas.x, intersectionCanvas.y + size / 2);
      context.lineTo(end2Canvas.x, end2Canvas.y);
      context.stroke();

      // context.rect(intersectionCanvas.x - size / 2, intersectionCanvas.y - size / 2, size, size)
      context.beginPath()
      context.fillStyle = color
      context.arc(intersectionCanvas.x, intersectionCanvas.y, 1, 0, 2 * Math.PI, true);
      context.fill();
      context.stroke();
      context.restore();
    }
}

export default renderReferLine
