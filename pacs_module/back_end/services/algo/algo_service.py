#!/bin/python
#coding=utf-8
import uuid
import os
import sys
import json

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_update_one, db_find_one, db_insert_one,db_find_update_otherwise_insert
from services.algo.algo_api import detect_dx, run_lung_seg, detect_lung_nodules, run_nodule_register,run_liver_seg
from config import DICOM_ROOT
import logging
logger = logging.getLogger('back_end.service.algo_service')
from models.label.Liver_Label import Liver_Label


def format_DR_predict(algo_result, seriesID, instanceID):
    result = []
    for label in algo_result:
        result.append({
            'labelID': str(uuid.uuid1()),
            'instanceID': instanceID,
            'seriesID': seriesID,
            'globalTag': label['globalTag'],
            'diseaseTag': label['className'].strip().upper(),
            'prob': label['prob'],
            'x': label['xStart'],
            'y': label['yStart'],
            'w': label['xEnd'] - label['xStart'],
            'h': label['yEnd'] - label['yStart']
        })
    return result

def formant_LUNG_predict(algo_result,  seriesID):
    for nodule in algo_result['nodules']:
        nodule['labelID'] = str(uuid.uuid1())
        nodule['instanceID'] = nodule['z']
        nodule['seriesID'] = seriesID
        nodule['addByUser'] = False
    return algo_result['nodules']

def format_Liver_Tumor(predict_list,seriesID,seriesNumber):
    result = []
    for predict in predict_list:
        label = Liver_Label()
        label.labelID = str(uuid.uuid1())
        label.seriesID = seriesID
        label.state = 'assign'
        label.x = predict['coord']['x']
        label.y = predict['coord']['y']
        label.z = predict['coord']['z']
        label.width = predict['width']
        label.height = predict['height']
        label.volume = predict['vol']
        label.diameter = predict['diam']
        label.p1_x = predict['diam_p1']['x']
        label.p1_y = predict['diam_p1']['y']
        label.p2_x = predict['diam_p2']['x']
        label.p2_y = predict['diam_p2']['y']
        label.avgHU = predict["avgHU"]
        label.malg = predict["malg"]
        label.seriesNumber = seriesNumber
        result.append(label.to_db())
    return result


def nodule_register(study_fixed, study_moving, userID):
    logger.info("begin nodule_register")
    data_fixed = {"dcm_path": os.path.join(DICOM_ROOT, study_fixed['series'][0]['seriesID']), "dicom": ""}
    data_moving = {"dcm_path": os.path.join(DICOM_ROOT, study_moving['series'][0]['seriesID']), "dicom": ""}
    taskID_moving = study_moving['taskID']
    taskID_fixed = study_fixed['taskID']

    label_info_fixed = db_find_one('label', {'taskID' : taskID_fixed})
    label_info_moving = db_find_one('label', {'taskID' : taskID_moving})
    data = {
        'fixedNoduleList': label_info_fixed['labels'],
        'movingNoduleList': label_info_moving['labels']
    }

    resp = run_nodule_register(data, data_fixed, data_moving)
    resp = resp.json()
    registerNodules = {"fixed": [], "moving": []}
    for coord_fixed, coord_mov in zip(resp['data']['fixed'], resp['data']['moving']):
        fixed = None
        moving = None
        for nod in data['fixedNoduleList']:
            if nod['x'] == coord_fixed['x'] and nod['y'] == coord_fixed['y'] and nod['z'] == coord_fixed['z']:
                fixed = nod
                break
        for nod in data['movingNoduleList']:
            if nod['x'] == coord_mov['x'] and nod['y'] == coord_mov['y'] and nod['z'] == coord_mov['z']:
                moving = nod
                break
        if not fixed or not moving:
            continue
        registerNodules['fixed'].append(fixed)
        registerNodules['moving'].append(moving)

    db_find_update_otherwise_insert('ai_label',{'taskID_fixed': taskID_fixed, 'taskID_moving': taskID_moving}, {'taskID_fixed': taskID_fixed, 'taskID_moving': taskID_moving,'registerNodules': registerNodules})
    return registerNodules


def predict(study, userID, taskID):
    modality = study['modality']

    # alrady has result
    seriesIDs = [s['seriesID'] for s in study['series']]

    if len(seriesIDs) == 1:
        label_info = db_find_one('ai_label', {'studyUID': study['studyUID'], 'seriesID': seriesIDs[0]})
    else:
        label_info = db_find_one('ai_label', {'studyUID': study['studyUID']})

    if label_info is not None and label_info.has_key('label_info'):
        logger.info("AI result already exist for studyUID {}".format(study['studyUID']))
        user_label_info = db_find_one('label', {"taskID": taskID})
        for label in label_info['labels']:
            label['state'] = 'assign'
            if label_info['labelType'] == 'Nodule':
                label['url'] = ''
            user_label_info['labels'].append(label)
        db_update_one('label', {"taskID": taskID}, user_label_info)
        return label_info

    if modality in ['DX', 'DR', 'CR'] and 'CHEST' == study['bodyPartExamined']:
        label_info = {}
        label_info['studyUID'] = study['studyUID']
        label_info['labelType'] = 'DX'
        label_info['labels'] = []
        algo_result = False
        for series in study['series']:
            for instance in series['instances']:
                if instance['viewPosition'] == 'PA':
                    uid = instance['url'].split('uid=')[1]
                    resource = db_find_one('resource', {"uid": uid})
                    if resource is not None:
                        logger.info("userID {} studyUID {} invoke DX algorithm".format(userID, study['studyUID']))
                        resp = detect_dx(resource)
                        if not resp:
                            logger.info("userID {} studyUID {} dx detect fail".format(userID, study['studyUID']))
                            continue
                        algo_result = True
                        resp = resp.json()
                        formated_result = format_DR_predict(resp['data'], series['seriesID'], instance['instanceID'])
                        label_info['labels'].extend(formated_result)
        if False == algo_result: return None

        # print json.dumps(label_info, indent=4)
        db_insert_one('ai_label', label_info)

        user_label_info = db_find_one('label', {"taskID": taskID})
        for label in label_info['labels']:
            label['state'] = 'assign'
            user_label_info['labels'].append(label)
        logger.info("save dx label to db userID {} taskID {}".format(userID, taskID))
        db_update_one('label', {"taskID": taskID}, user_label_info)

        return label_info

    if study['modality'] == 'CT' and 'CHEST' in study['bodyPartExamined']:
        label_info = {}
        label_info['studyUID'] = study['studyUID']
        label_info['labelType'] = 'Nodule'
        label_info['labels'] = []

        for series in study['series']:
            logger.info("userID {} studyUID {} invoke lung nodule algorithm".format(userID, study['studyUID']))
            resource = {"dcm_path": os.path.join(DICOM_ROOT, series['seriesID']), "dicom": ""}

            seg_resp = run_lung_seg(resource)
            if not seg_resp:
                logger.info("lung seg fail")
                return None

            resource['lungBBox'] = seg_resp.json()['data']

            resp = detect_lung_nodules(resource)

            if not resp:
                logger.info("userID {} studyUID {} nodule detect fail".format(userID, study['studyUID']))
                return None

            resp = resp.json()
            formated_result = formant_LUNG_predict(resp['data'], series['seriesID'])
            label_info['labels'].extend(formated_result)
            logger.info("insert ai result for study {}".format(study['studyUID']))
            db_insert_one('ai_label', label_info)
            db_insert_one('ai_label', {
                'studyUID': study['studyUID'],
                'seriesID': series['seriesID'],
                'labelType': 'lungBBox',
                'leftLungBBox': resp['data']['leftLungBBox'],
                'rightLungBBox': resp['data']['rightLungBBox']
            })

        user_label_info = db_find_one('label', {"taskID": taskID})
        for label in label_info['labels']:
            label['state'] = 'assign'
            if label_info['labelType'] == 'Nodule':
                label['url'] = ''
            user_label_info['labels'].append(label)
        logger.info("save nodule label to db userID {} taskID {}".format(userID, taskID))
        db_update_one('label', {"taskID": taskID}, user_label_info)

        return label_info

    if study['modality'] == 'CT' and (study['bodyPartExamined'] == 'LIVER' or study['bodyPartExamined'] == "ABDOMEN"):
        logger.info("userID {} studyUID {} invoke liver algorithm".format(userID, study['studyUID']))

        label_info = {}
        label_info['studyUID'] = study['studyUID']
        label_info['labelType'] = 'LiverTumor'
        label_info['labels'] = []
        
        
        for series in study['series']:
            logger.info("checking liver study {} series {}".format(study['studyUID'], series['seriesID']))

            resource = {"dcm_path": os.path.join(DICOM_ROOT, series['seriesID']), "dicom": ""}
            if os.path.exists(resource['dcm_path']):
                seg_resp = run_liver_seg(resource['dcm_path'])
                
                if not seg_resp:
                    logger.info("run liver fail")
                    return None
            
                predict_list = json.loads(seg_resp.json()['data'])['predict']
                label_info['labels'].extend(format_Liver_Tumor(predict_list,series['seriesID'],series['seriesNumber']))
        db_insert_one('ai_label', label_info)
        
        user_label_info = db_find_one('label', {"taskID": taskID})
        for label in label_info['labels']:
            label['state'] = 'assign'
            user_label_info['labels'].append(label)
        db_update_one('label', {"taskID": taskID}, user_label_info)
        
        return label_info

import datetime
if __name__ == '__main__':
    new_task = {
        "taskID": "888",
        "reporter": "user1",
        "reviewer": "user2",
        "type": "normal",
        "state": 'assign',
        "studyUID": "1.2.392.200036.9116.2.5.1.48.1221404901.1524567640.588109"
    }
    db_insert_one('task', new_task)

    new_label = {
        "studyUID": "1.2.392.200036.9116.2.5.1.48.1221404901.1524567640.588109",
        "taskID": "888",
        "labelType": "Nodule",
        "labels": []
    }
    db_insert_one('label', new_label)

    study_info = db_find_one('study', {"studyUID": "1.2.392.200036.9116.2.5.1.48.1221404901.1524567640.588108"})
    study_info['studyUID'] = "1.2.392.200036.9116.2.5.1.48.1221404901.1524567640.588109"
    study_info['studyDate'] = datetime.datetime.today()


    label_info = predict(study_info, "user1", "888")

    # print label_info
