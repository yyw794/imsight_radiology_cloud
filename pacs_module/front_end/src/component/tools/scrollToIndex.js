import * as cornerstone from 'cornerstone-core'
import {
  EVENTS,
  getToolState,
  requestPoolManager,
  loadHandlerManager,
} from 'cornerstone-tools';
import triggerEvent from './util/triggerEvent.js';
import {afterFrames} from '../../util'
// import external from '../externalModules.js';
// import { getToolState } from '../stateManagement/toolState.js';
// import requestPoolManager from '../requestPool/requestPoolManager.js';
// import loadHandlerManager from '../stateManagement/loadHandlerManager.js';
// import triggerEvent from '../util/triggerEvent.js';

export default function (element, newImageIdIndex) {
  return new Promise(function(resolve, reject) {
    //console.log("@@@@@@@@@@@@@@@@@ ScrollToIndex !!!!!!!!!!!!!!");
    const toolData = getToolState(element, 'stack');

    if (!toolData || !toolData.data || !toolData.data.length) {
      return;
    }

    // const cornerstone = external.cornerstone;
    // If we have more than one stack, check if we have a stack renderer defined
    let stackRenderer;

    if (toolData.data.length > 1) {
      const stackRendererData = getToolState(element, 'stackRenderer');

      if (stackRendererData && stackRendererData.data && stackRendererData.data.length) {
        stackRenderer = stackRendererData.data[0];
      }
    }

    const stackData = toolData.data[0];

    // Allow for negative indexing
    // if (newImageIdIndex < 0) {
    //   newImageIdIndex += stackData.imageIds.length;
    // }
    let maxLoopCount = 20;
    if(newImageIdIndex<0){
      while((maxLoopCount > 0) && (newImageIdIndex < 0)){
        newImageIdIndex += stackData.imageIds.length;
        --maxLoopCount;
      }
    }
    else if(newImageIdIndex>0){
      while((maxLoopCount > 0) && (newImageIdIndex >= stackData.imageIds.length)){
        newImageIdIndex -= stackData.imageIds.length;
        --maxLoopCount;
      }
    }
    //Now @newImageIdIndex is guaranteed to be in the range [0,stackData.imageIds.length] if @maxLoopCount does not break through

    const startLoadingHandler = loadHandlerManager.getStartLoadHandler();
    const endLoadingHandler = loadHandlerManager.getEndLoadHandler();
    const errorLoadingHandler = loadHandlerManager.getErrorLoadingHandler();

    function doneCallback (image) {
      if (stackData.currentImageIdIndex !== newImageIdIndex) {
        //console.log("currentImageIdIndex="+stackData.currentImageIdIndex+", newImageIdIndex=" + newImageIdIndex);	//!*!*!*! Jackey Console Log
        reject()
        return;
      }

      // Check if the element is still enabled in Cornerstone,
      // If an error is thrown, stop here.
      try {
        // TODO: Add 'isElementEnabled' to Cornerstone?
        cornerstone.getEnabledElement(element);
      } catch(error) {
        return;
      }

      if (stackRenderer) {
        stackRenderer.currentImageIdIndex = newImageIdIndex;
        stackRenderer.render(element, toolData.data);
      } else {
        cornerstone.displayImage(element, image);
        // Delay 2 frames to ensure display
        afterFrames(2).then(resolve)
      }

      if (endLoadingHandler) {
        endLoadingHandler(element, image);
      }
    }

    function failCallback (error) {
      const imageId = stackData.imageIds[newImageIdIndex];

      if (errorLoadingHandler) {
        errorLoadingHandler(element, imageId, error);
      }
      reject()
    }

    if (newImageIdIndex === stackData.currentImageIdIndex) {
      resolve()
      return;
    }

    if (startLoadingHandler) {
      startLoadingHandler(element);
    }

    const eventData = {
      newImageIdIndex,
      direction: newImageIdIndex - stackData.currentImageIdIndex
    };

    stackData.currentImageIdIndex = newImageIdIndex;
    const newImageId = stackData.imageIds[newImageIdIndex];

    // Retry image loading in cases where previous image promise
    // Was rejected, if the option is set
    /*

      Const config = stackScroll.getConfiguration();

      TODO: Revisit this. It appears that Core's imageCache is not
      keeping rejected promises anywhere, so we have no way to know
      if something was previously rejected.

      if (config && config.retryLoadOnScroll === true) {
      }
    */

    // Convert the preventCache value in stack data to a boolean
    const preventCache = Boolean(stackData.preventCache);

    let imagePromise;

    if (preventCache) {
      imagePromise = cornerstone.loadImage(newImageId);
    } else {
      imagePromise = cornerstone.loadAndCacheImage(newImageId);
    }

    imagePromise.then(doneCallback, failCallback);
    // Make sure we kick off any changed download request pools
    requestPoolManager.startGrabbing();

    triggerEvent(element, EVENTS.STACK_SCROLL, eventData);
  });
}
