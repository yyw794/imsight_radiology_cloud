import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import Cookies from 'js-cookie'
import actionTypes from './actions'

const history = createBrowserHistory()

let lang = Cookies.get('lang')
const SUPPORT_LOCALES = ['zh-cn', 'en']
if (!lang || !SUPPORT_LOCALES.includes(lang)) {
  lang = 'zh-cn'
}
const initialState = {lang}

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_LANGUAGE:
      return Object.assign({}, state, {lang: action.lang})
      break;
    default:
      return state
  }
}

const store = createStore(
  connectRouter(history)(appReducer), // new root reducer with router state
  initialState,
  compose(
    applyMiddleware(
      routerMiddleware(history), // for dispatching history actions
    ),
  ),
)

export default store
