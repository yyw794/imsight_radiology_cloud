#!/bin/python
#coding=utf-8
from flask import make_response, request, jsonify, abort
from flask_login import current_user
from socketIO_client import SocketIO, LoggingNamespace
import datetime, sys, os

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_update_one
from models.response.response import response
from models.report.Report import Report, parseReportInfo
from models.task.Task import Task, parseTaskInfo
from models.label.Label_IO import parseLabelInfo
from services.basic.basic_api import socket_emit

legal_action = ["reset", 'submit', 'unsubmit', 'confirm', 'unconfirm', "reject"]
required_key_put = ['screenshotURLs', 'imagingFind', 'diagnosisAdvice']
required_key_all = ['taskID']
required_key_post = ['action']

def on_report():
    if not current_user.is_authenticated:
        abort(401)

    res = response()
    userID = current_user.get_id()
    params = request.args.to_dict() if request.method == 'GET' else request.json

    if not all(key in params for key in required_key_all):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
    taskID = params['taskID']

    task_info = db_find_one("task", {"taskID": taskID})
    if task_info is None:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    task = parseTaskInfo(task_info)
    user_role = task.get_role(userID)
    if user_role == "anonoymous":
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    if request.method == "GET":
        report_info = db_find_one("report", {"taskID": taskID})
        report = parseReportInfo(report_info)

        if report.hello():
            db_update_one("report", {"taskID": taskID}, report.to_db())

        ret = report.to_dict()
        ret['user_privilege'] = task.get_user_privilege(userID)

        if report.modality == 'CT':
            label_info = db_find_one("label", {"taskID": taskID})
            labelist = parseLabelInfo(label_info)
            ret['labelist'] = [label.to_dict() for label in labelist if label.url != '']

        res.statusCode = 1000
        res.data = ret
        return make_response(jsonify(res.to_dict()), 200)

# remove nodule screenshot
    if request.method == "PUT":
        if not all(key in params for key in required_key_put):
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        report_info = db_find_one("report", {"taskID": taskID})
        report = parseReportInfo(report_info)
        if not report.editable(user_role):
            res.statusCode = 1102
            return make_response(jsonify(res.to_dict()), 200)

        old_screenshotURLs = report.screenshotURLs
        report.screenshotURLs = params["screenshotURLs"]
        report.imagingFind = params['imagingFind']
        report.diagnosisAdvice = params["diagnosisAdvice"]
        report.updateDate = datetime.datetime.today()

        db_update_one("report", {"taskID": taskID}, report.to_db())

        if len(old_screenshotURLs) > len(params["screenshotURLs"]):
            label_info = db_find_one("label", {"taskID": taskID})
            if label_info['labelType'] == 'Nodule':
                removed_screenshotURLs = [screenshotURL for screenshotURL in old_screenshotURLs if screenshotURL not in report.screenshotURLs]
                labelID_update = [screenshotURL.split('uid=')[1] for screenshotURL in removed_screenshotURLs]
                labels_url_update = [label for label in label_info['labels'] if label['labelID'] in labelID_update ]
                for label in labels_url_update:
                    label['url'] = ''

                labelist = parseLabelInfo(label_info)
                socket_emit('updateLabel', {'data': [label.to_dict() for label in labelist], 'userID': userID, 'taskID': taskID})
                db_update_one('label', {"taskID": taskID}, label_info)

        ret = report.to_dict()
        ret['user_privilege'] = task.get_user_privilege(userID)
        if report.modality == 'CT':
            label_info = db_find_one("label", {"taskID": taskID})
            labelist = parseLabelInfo(label_info)
            ret['labelist'] = [label.to_dict() for label in labelist if label.url != '']

        res.statusCode = 1000
        res.data = ret
        return make_response(jsonify(res.to_dict()), 200)


    if request.method == "POST":
        if not all(key in params for key in required_key_post):
            res.statusCode = 1101
            return make_response(jsonify(res.to_dict()), 200)

        if params['action'] not in legal_action:
            res.statusCode = 1102
            return make_response(jsonify(res.to_dict()), 200)

        report_info = db_find_one("report", {"taskID": taskID})
        report = parseReportInfo(report_info)

        action = params['action']
        if not report.legalAction(user_role, action):
            res.statusCode = 1102
            return make_response(jsonify(res.to_dict()), 200)

        if action == 'reset':
            report.reset()

        if action == 'submit':
            report.submit()

        if action == 'unsubmit':
            report.unsubmit()

        if action == 'confirm':
            report.confirm()

        if action == 'unconfirm':
            report.unconfirm()

        if action == 'reject':
            report.reject()

        db_update_one("report", {"taskID": taskID}, report.to_db())

        res.statusCode = 1000
        res.data = report.to_dict()
        return make_response(jsonify(res.to_dict()), 200)