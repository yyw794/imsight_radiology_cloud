import React, { Component } from 'react';
import {withRouter} from 'react-router'
import './index.less'
import {Divider, Menu, Dropdown, Icon, Form, InputNumber, Modal, Button,Checkbox} from 'antd'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
const FormItem = Form.Item

const ImageButton = (props) => {
  let style = {
    width: props.width,
    height: props.height,
    // backgroundImage: `url(${props.bgImg})`
  }
  console.log(props)
  if (props.active) style.backgroundColor = '#1890ff'
  return (
    <div className='image-button'
      style={style}
      onClick={props.onClick}
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}>
      {
        typeof(props.bgImg) === 'string' ?
        <img src={props.bgImg}/>
        :
        props.bgImg
      }
      {
        props.text.constructor.name === 'String' ?//should be 'string'?---------------------------------------------------------
        <span>{props.text}</span>
        :
        props.text
      }
    </div>
  )
}

const Logo = withRouter(
  ({ history }) => (
    <div className="logo">
      <img className='logo-image' src={require('../../../static/images/logo_corner.png')} /*onClick={() => history.push('/')}*//>
    </div>
  )
)

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

let WWWCForm = (props) => {
  const { getFieldDecorator, getFieldsError, getFieldError, setFieldsValue } = props.form;
  return (
    <Form layout="inline" onSubmit={(e) => {
        e.preventDefault()
        props.form.validateFields((err, values) => {
          if (!err) {
            console.log('Received values of form: ', values);
            props.onSubmit(values)
          }
        });
      }}>
      <FormItem
        label={GetLocaledText({props}, "CTToolBar.window-width")}
      >
        {getFieldDecorator('ww', {
          rules: [{ required: true, message: GetLocaledText({props}, "CTToolBar.window-width-input") }],
          initialValue: Math.round(1200)
        })(
          <InputNumber placeholder={GetLocaledText({props}, "CTToolBar.window-width")}
            max={6000} min={-6000} step={100}/>
        )}
      </FormItem>
      <FormItem
        label={GetLocaledText({props}, "CTToolBar.window-center")}
      >
        {getFieldDecorator('wc', {
          rules: [{ required: true, message: GetLocaledText({props}, "CTToolBar.window-center-input") }],
          initialValue: Math.round(-600)
        })(
          <InputNumber placeholder={GetLocaledText({props}, "CTToolBar.window-center")}
            max={6000} min={-6000} step={100}/>
        )}
      </FormItem>
      <FormItem>
        <Button
          type="primary"
          style={{marginRight: 10}}
          onClick={() => setFieldsValue({ww: Math.round(1200), wc: Math.round(-600)})}
        >
          <FormattedMessage id='default'/>
        </Button>
        <Button
          type="primary"
          htmlType="submit"
          disabled={hasErrors(getFieldsError())}
        >
          <FormattedMessage id='confirm'/>
        </Button>
      </FormItem>
    </Form>
  )
}

WWWCForm = injectIntl(WWWCForm)

WWWCForm = Form.create()(WWWCForm);

class ToolBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showModalWWWC: false,
      WWWCMenuVisible: false,
      gridMenuVisible: false,
      detectionList:null
    }
  }

  renderGridMenu() {
    return (
      <Menu className="grid-menu">
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('grid', 1)
            }}>1</a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('grid', 2)
            }}>2</a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('grid', 3)
            }}>3</a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('grid', 4)
            }}>4</a>
        </Menu.Item>
      </Menu>
    )
  }

  renderWWWCMenu() {
    return (
      <Menu className="wwwc-menu">
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('wwwc', {wc: -600, ww: 1200})
            }}><FormattedMessage id='CTToolBar.lung-window'/><span className="short-cut">F1</span></a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('wwwc', {wc: 40, ww: 400})
            }}><FormattedMessage id='CTToolBar.mediastinal-window'/><span className="short-cut">F2</span></a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('wwwc', {wc: 40, ww: 300})
            }}><FormattedMessage id='CTToolBar.abdomen-window'/><span className="short-cut">F3</span></a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('wwwc', {wc: 800, ww: 2600})
            }}><FormattedMessage id='CTToolBar.bone-window'/><span className="short-cut">F4</span></a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('wwwc', {wc: 45, ww:300})
            }}><FormattedMessage id='CTToolBar.spine-window'/><span className="short-cut">F5</span></a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.props.onToolClick('wwwc', {wc: 35, ww: 80})
            }}><FormattedMessage id='CTToolBar.brain-window'/><span className="short-cut">F6</span></a>
        </Menu.Item>
        <Menu.Item>
          <a target="_blank"
            rel="noopener noreferrer"
            onClick={(e) => {
              e.stopPropagation()
              this.setState({showModalWWWC: true})
            }}><FormattedMessage id='CTToolBar.mannul-input'/></a>
        </Menu.Item>
      </Menu>
    )
  }

  renderButton = (name) => {
    const menu = this.renderWWWCMenu()
    if (name === 'wwwc') {
      return (
        <ImageButton
          width={64} height={64}
          bgImg={require('../../../static/images/contrast.png')}
          active={this.props.activeTool.includes('wwwc')}
          onMouseEnter={() => this.setState({WWWCMenuVisible: true})}
          onMouseLeave={() => this.setState({WWWCMenuVisible: false})}
          text={
            <Dropdown overlay={menu} trigger={[]} visible={this.state.WWWCMenuVisible}>
              <span>{GetLocaledText(this, "CTToolBar.wwwc")} <Icon type="down" /></span>
            </Dropdown>
          }
          onClick={() => this.props.onToolClick('wwwc')}/>
      )
    } else if (name === 'referenceLines') {
      return (
        <ImageButton
          width={64} height={64}
          bgImg={require('../../../static/images/reference_lines.png')}
          active={this.props.activeTool.includes('referenceLines')}
          text={GetLocaledText(this, "CTToolBar.referenceLines")}
          onClick={() => this.props.onToolClick('referenceLines')}/>
      )
    } else if (name === 'viwerSync') {
      return (
        <ImageButton
          width={64} height={64}
          bgImg={<Icon type="link" />}
          active={this.props.activeTool.includes('viwerSync')}
          text={GetLocaledText(this, "CTToolBar.viwerSync")}
          onClick={() => this.props.onToolClick('viwerSync')}/>
      )
    } else if (name === 'grid') {
      return (
        <ImageButton
          width={64} height={64}
          bgImg={require('../../../static/images/layout.png')}
          active={this.props.activeTool.includes('grid')}
          onMouseEnter={() => this.setState({gridMenuVisible: true})}
          onMouseLeave={() => this.setState({gridMenuVisible: false})}
          text={
            <Dropdown overlay={this.renderGridMenu()} trigger={[]} visible={this.state.gridMenuVisible}>
              <span>{GetLocaledText(this, "CTToolBar.layout")} <Icon type="down" /></span>
            </Dropdown>
          }
          onClick={() => this.props.onToolClick('grid')}/>
      )
    } else if(name ==='report'){
      return (
        <ImageButton
          width={64} height={64}
          bgImg={require('../../../static/images/report.png')}
          active={this.props.activeTool.includes('report')}
          text={GetLocaledText(this, "CTToolBar.report")}
          onClick={() => this.props.onToolClick('report')}/>
      )
    }else if(name === 'detect'){
      return(
        <ImageButton
          width={44} height={64}
          bgImg={require('../../../static/images/detect.png')}
          active={this.props.activeTool.includes('detect')}
          text={GetLocaledText(this,"CTToolBar.detect")}
          onClick={()=>this.props.onToolClick('detect')}
        />
      )
    }else if(name==='hideBoxes'){
      return(
        <ImageButton
          width={44} height={64}
          bgImg={require('../../../static/images/hideBoxes.png')}
          active={this.props.activeTool.includes('hideBoxes')}
          text={GetLocaledText(this,"CTToolBar.lesion.hideBoxes")}
          onClick={()=>this.props.onToolClick('hideBoxes')}
        />        
      )
    }else if(name==='length') {
        return(
            <ImageButton
                width={64} height={64}
                bgImg={require('../../../static/images/ruler.png')}
                active={this.props.activeTool.includes('length')}
                text={GetLocaledText(this, "DRViewer.ToolBar.length")}
                onClick={() => this.props.onToolClick('length')}/>
        )
    } else if(name==='angle') {
        return(
            <ImageButton
                width={64} height={64}
                bgImg={require('../../../static/images/protractor.png')}
                active={this.props.activeTool.includes('angle')}
                text={GetLocaledText(this, "DRViewer.ToolBar.angle")}
                onClick={() => this.props.onToolClick('angle')}/>
        )
    }else if(name==='freehand'){
        return (
            <ImageButton
                width={64} height={64}
                bgImg={require('../../../static/images/polygon.png')}
                active={this.props.activeTool.includes('freehand')}
                text={GetLocaledText(this, "DRViewer.ToolBar.freehand")}
                onClick={() => this.props.onToolClick('freehand')}/>
        )
    }else if(name==='eraser') {
        return(
            <ImageButton
                width={64} height={64}
                bgImg={require('../../../static/images/clear-tool.png')}
                active={this.props.activeTool.includes('eraser')}
                text={GetLocaledText(this, "DRViewer.ToolBar.eraser")}
                onClick={() => this.props.onToolClick('eraser')}/>
            )
    }
  }

  render() {
    return (
      <div className='ct-viewer-toolbar' style={{padding:"0px"}}>
        {/*<Logo/>*/}
        <div className='tool-button-wrapper' style={{background:"#001529",paddingLeft:"40px"}} >
          {this.props.tools.map(this.renderButton)}
        </div>
        <Modal
          title={GetLocaledText(this, "CTToolBar.wwwc")}
          visible={this.state.showModalWWWC}
          onOk={this.handleOk}
          onCancel={() => this.setState({showModalWWWC: false})}
          footer={null}
        >
          <WWWCForm onSubmit={values => {
            values.ww = values.ww
            values.wc = values.wc
            this.props.onToolClick('wwwc', values)
            this.setState({showModalWWWC: false})
          }}/>
        </Modal>
      </div>
    );
  }

}

export default injectIntl(ToolBar);
