import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {getUser, signout,onClickModel} from '../../auth'
import { GetLocaledText } from '../../localedFuncs'
import './index.less'
const SignoutButton = withRouter(
  ({ history }) => (
      <span>
            <span className='signout-btn'>
              <img className='user-icon' src={require('../../../static/images/user.png')}/>
              <span className='current-user'>{getUser().account}</span>
              <img
                className='signout-icon'
                onClick={() => {
                    signout(() => history.push('/login'))
                  }}
                src={require('../../../static/images/logout.png')}/>
            </span>
      </span>
  )
)

export default SignoutButton
