#!/bin/python
#coding=utf-8
import os
import sys

import requests

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from config import ADDRESS, PORT, ALGO_ADDRESS, ALGO_PORT, FILEMETHOD, HOST_ROOT
import logging
logger = logging.getLogger('radiology')
fileMethod = FILEMETHOD

HOST = "http://{}:{}".format(ADDRESS, PORT)
ALGO_HOST = "http://{}:{}".format(ALGO_ADDRESS, ALGO_PORT)

def to_host_path(vm_path):
    return vm_path.replace('/app', HOST_ROOT) 

def detect_lung_nodules(params):
    url = ALGO_HOST + '/api/lung/pred'
    data = {}
    # data['lungBBox'] = params['lungBBox']
    data['fileMethod'] = fileMethod
    if data['fileMethod'] == 'http':
        data['imageSource'] = map(lambda k: '{0}/api/fetch_study_dicom?uid={1}'.format(HOST, k), data['uids'])
    elif data['fileMethod'] == 'local':
        data['imageSource'] = [os.path.join(to_host_path(params['dcm_path']), params['dicom'])]

    try:
        resp = requests.post(url, json=data)
        resp.raise_for_status()
    except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as err:
        resp = None
        logger.error("run detect lung nodule error. err=%s", err)
    return resp


def add_lung_nodule(data):
    url = ALGO_HOST + '/api/lung/nodule'
    data['fileMethod'] = fileMethod
    if data['fileMethod'] == 'http':
        data['imageSource'] = map(lambda k: '{0}/api/fetch_study_dicom?uid={1}'.format(HOST, k), data['uids'])
    elif data['fileMethod'] == 'local':
        data['imageSource'] = [os.path.join(to_host_path(data['dcm_path']), data['dicom'])]
    try:
        resp = requests.post(url, json=data)
        resp.raise_for_status()
    except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as err:
        resp = None
        logger.error("run lung nodule error. err=%s", err)
    return resp


def run_lung_seg(params):
    url = ALGO_HOST + '/api/lung/seg'
    data = {}
    data['fileMethod'] = fileMethod
    if data['fileMethod'] == 'http':
        data['imageSource'] = map(lambda k: '{0}/api/fetch_study_dicom?uid={1}'.format(HOST, k), params['uids'])
    elif data['fileMethod'] == 'local':
        data['imageSource'] = [os.path.join(to_host_path(params['dcm_path']), params['dicom'])]
    try:
        resp = requests.post(url, json=data)
        resp.raise_for_status()
    except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as err:
        resp = None
        logger.error("run_lung_seg error. err=%s", err)
    return resp


def detect_dx(params):
    url = ALGO_HOST + '/api/dx/pred'
    data = {}
    data['fileMethod'] = fileMethod
    if data['fileMethod'] == 'http':
        data['imageSource'] = '{0}/api/fetch_study_dicom?uid={1}'.format(HOST, params['uid'])
    elif data['fileMethod'] == 'local':
        data['imageSource'] = os.path.join(to_host_path(params['dcm_path']), params['dicom'])
    try:
        resp = requests.post(url, json=data)
        resp.raise_for_status()
    except requests.exceptions.HTTPError as err:
        resp = None
        logger.error("detect dx error. host=%s, err=%s", url, err)
    except Exception as exc:
        resp = None
        logger.error("detect dx error. host=%s, err=%s", url, exc)
    return resp


def run_nodule_register(data, data1, data2):
    url = ALGO_HOST + '/api/lung/register'
    data['fileMethod'] = fileMethod
    if data['fileMethod'] == 'http':
        data['fixedImageSource'] = map(lambda k: '{0}/api/fetch_study_dicom?uid={1}'.format(HOST, k), data1['uids'])
        data['movingImageSource'] = map(lambda k: '{0}/api/fetch_study_dicom?uid={1}'.format(HOST, k), data2['uids'])
    elif data['fileMethod'] == 'local':
        data['fixedImageSource'] = [os.path.join(to_host_path(data1['dcm_path']), data1['dicom'])]
        data['movingImageSource'] = [os.path.join(to_host_path(data2['dcm_path']), data2['dicom'])]
    try:
        resp = requests.post(url, json=data)
        resp.raise_for_status()
    except requests.exceptions.HTTPError as err:
        logger.error("lung nodule register call error. err=%s", err)
        resp = None
    except Exception as exc:
        logger.error("lung nodule register call error, err=%s", exc)
        resp = None
    return resp

def run_liver_seg(dcmPath):
    url = ALGO_HOST + '/api/liver/seg'
    data = {}
    data['fileMethod'] = fileMethod

    datapath = []
    parents = os.listdir(dcmPath)
    for parent in parents:
        datapath.append(to_host_path(os.path.join(dcmPath,parent)))
    
    if fileMethod == 'http':
        data['dataPath'] = map(lambda k: '{0}/api/download_raw/{1}'.format(HOST, k),
                               data['obs_keys'])
    elif fileMethod == 'local':
        data['dataPath'] = datapath
    try:
        resp = requests.post(url, json=data)
        resp.raise_for_status()
    except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as err:
        resp = None
        logger.error("run liver seg error. err=%s", err)
    return resp


def detect_ribs(dcm_path):
    url = ALGO_HOST + "/api/lung/ribs"
    data = {"fileMethod": fileMethod}
    if fileMethod == "http":
        data["imageSource"] = '{0}/api/fetch_study_dicom?uid={1}'.format(HOST, data['uid'])
    elif fileMethod == "local":
        data["imageSource"] = dcm_path
    else:
        logger.error("unsupported filemethod. filemethod=%s", fileMethod)
        return None
    try:
        resp = requests.post(url, json=data)
        resp.raise_for_status()
    except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as err:
        resp = None
        logger.error("run detect ribs error. err=%s", err)
    return resp
