#!/bin/python
# coding=utf-8

import os
import shutil
import sys

import SimpleITK as sitk
import numpy as np
import scipy.misc
from scipy.misc import imresize

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from config import ADDRESS, PORT, IMAGE_ROOT, PROTOCOL
import logging

logger = logging.getLogger('radiology')


def cal_hist_dx(source, start_level=0, thresh=0.9995):
    source = source.ravel()
    total = len(source)

    s_values, bin_idx, s_counts = np.unique(source, return_inverse=True, return_counts=True)

    histogram_dict = {}
    for value, count in zip(s_values, s_counts):
        histogram_dict[value] = count

    current_count = 0
    forward = 0
    min_value = start_level
    max_value = 0
    while float(current_count) / float(total) < thresh:
        value = start_level + forward

        if value in histogram_dict:
            max_value = value
            current_count += histogram_dict[value]

        forward += 1

    return min_value, max_value


def insert_x_ray_instances(series):
    count = 0
    instances = series['instances']
    uid = series['seriesID']
    PNGfolder = os.path.join(IMAGE_ROOT, uid)
    if os.path.exists(PNGfolder):
        shutil.rmtree(PNGfolder)
    os.makedirs(PNGfolder)

    for ins in instances:
        # uid = str(uuid.uuid1())
        image = sitk.ReadImage(str(ins['filepath']))
        PNGfilename = '{:04}.png'.format(count)
        count += 1
        PNGpath = os.path.join(PNGfolder, PNGfilename)
        # convert to png
        if not os.path.exists(PNGpath):
            img_arr = sitk.GetArrayFromImage(image)
            minPixelValue, maxPixelValue = cal_hist_dx(img_arr, np.min(img_arr), 0.999)
            logger.info('cal window [{}:{}]'.format(minPixelValue, maxPixelValue))
            series['minPixelValue'] = int(minPixelValue)
            series['maxPixelValue'] = int(maxPixelValue)
            pixelValueRange = maxPixelValue - minPixelValue
            img_arr[img_arr < minPixelValue] = minPixelValue
            img_arr[img_arr > maxPixelValue] = maxPixelValue
            img_arr = img_arr - minPixelValue
            img_arr = img_arr.astype(np.float32) / float(pixelValueRange)
            img_arr = img_arr * 255
            img_arr = img_arr.squeeze()
            img_arr = img_arr.astype(np.uint8)
            img_arr = img_arr.reshape(img_arr.shape[0]/4, img_arr.shape[1]/4)
            scipy.misc.imsave(PNGpath, img_arr)

            ins['url'] = "{}://{}:{}/api/study_image?dcm={}&uid={}".format(PROTOCOL, ADDRESS, PORT,
                                                                           str(ins['filepath']), PNGpath)
            ins.pop('filename')
            ins.pop('filepath')

    return True


def insert_x_ray_instances_dicom(series, modality):
    instances = series['instances']
    uid = series['seriesID']
    PNGfolder = os.path.join(IMAGE_ROOT, uid)
    if not os.path.exists(PNGfolder):
        os.makedirs(PNGfolder)
    for count, ins in enumerate(instances):
        image = sitk.ReadImage(str(ins['filepath']))
        PNGfilename = '{:04}.png'.format(count)
        PNGpath = os.path.join(PNGfolder, PNGfilename)
        # convert to png
        img_arr = sitk.GetArrayFromImage(image)
        minPixelValue, maxPixelValue = cal_hist_dx(img_arr, np.min(img_arr), 0.999)
        logger.info('cal window [{}:{}]'.format(minPixelValue, maxPixelValue))
        series['minPixelValue'] = int(minPixelValue)
        series['maxPixelValue'] = int(maxPixelValue)
        pixelValueRange = maxPixelValue - minPixelValue
        img_arr[img_arr < minPixelValue] = minPixelValue
        img_arr[img_arr > maxPixelValue] = maxPixelValue
        img_arr = img_arr - minPixelValue
        img_arr = img_arr.astype(np.float32) / float(pixelValueRange)
        img_arr = img_arr * 255
        img_arr = img_arr.squeeze()
        img_arr = img_arr.astype(np.uint8)
        brev_img_arr = imresize(img_arr, (img_arr.shape[0] / 4, img_arr.shape[1] / 4))
        scipy.misc.imsave(PNGpath, brev_img_arr)
        ins['pngurl'] = "/api/study_image?dcm={}&uid={}".format(str(ins['filepath']), PNGpath)
        if modality == "CR":
            origin_img_arr = img_arr.reshape(img_arr.shape[0], img_arr.shape[1])
            CRPNGpath = os.path.join(PNGfolder, "{:04}_CR.png".format(count))
            scipy.misc.imsave(CRPNGpath, origin_img_arr)
            ins['url'] = CRPNGpath
        else:
            ins['url'] = ins["filepath"]
        ins.pop('filename')
        ins.pop('filepath')

    return True
