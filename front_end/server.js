const express = require('express')
const https = require('https')
const http = require('http')
const history = require('connect-history-api-fallback')
const proxy = require('http-proxy-middleware')
const path = require('path')
const fs = require('fs')
const ini = require('ini')
//const port = process.env.PORT || 80
//const port = process.env.PORT || 8000
let privateKey  = fs.readFileSync('/home/node/app/cert/server.key', 'utf8')
let certificate = fs.readFileSync('/home/node/app/cert/server.cert', 'utf8')
let credentials = {key: privateKey, cert: certificate}
// const port = 443
const useHttps = ini.parse(fs.readFileSync('/home/node/app/config.ini','utf8')).front_end.https || false
const app = express()

const HOST = 'flask'

//加载指定目录静态资源
const PATH = '/home/node/app/build'
app.use(express.static(PATH))

app.use(history({
  index: '/',
  verbose: true,
}))

app.get('/', function (request, response){
  response.sendFile(path.resolve(PATH, 'index.html'))
})


let server = app.listen(4000, function () {
  console.log("server started on port " + 80)
})
