#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import make_response, request, jsonify, Flask
from flask_login import current_user
from flask_mail import Message, Mail
from datetime import datetime
import time
import hashlib
import logging
import traceback
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find, db_find_one, db_find_all, db_insert_one, db_update_one
from models.response.response import response
from models.user.User import User, parseUserInfo
from config import MAIL_VERIFY_REQUIRE, LABEL_PLATFORM, MAIL_SERVER, MAIL_PORT, MAIL_USE_SSL, MAIL_USERNAME, MAIL_PASSWORD, MAIL_DEFAULT_SENDER, HOSTNAME, PORT

app = Flask(__name__)
# email
app.config['MAIL_SERVER'] = MAIL_SERVER
app.config['MAIL_PORT'] = MAIL_PORT
app.config['MAIL_USE_SSL'] = MAIL_USE_SSL
#app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = MAIL_USERNAME
app.config['MAIL_PASSWORD'] = MAIL_PASSWORD
app.config['MAIL_DEFAULT_SENDER'] = MAIL_DEFAULT_SENDER
mail = Mail(app)

def send_confirm_email(to, account, token):
    html = u'''
    <html>

    <head>
    <title>验证邮箱</title>
    </head>

    <body>
    <p>您的账号已经注册成功，请复制以下链接到浏览器打开完成邮箱验证</p>
    <p>http://%s/auth/register/confirm?account=%s&token=%s</p>
    </body>

    </html>
    ''' % (HOST, account, token)
    # print html.encode('utf-8')
    msg = Message(
        '确认您的邮箱-视见科技',
        recipients=[to],
        html=html,
        sender=app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)

def confirm_email_handler():
    account = request.args.get('account')
    token = request.args.get('token')

    ret = db_find_one("user", {"account": account})
    if ret is None or ret['confirmEmail'] != token:
        return make_response('verification fail')

    db_update_one('user', {"account": account}, {'confirmEmail': 'true'})
    return make_response('verification success')

required_key = [u'account', u'password', u'nickname', u'email', u'hospital']

def register_handler():
    res = response()
    params = request.get_json()

    if not all(key in params for key in required_key):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    ret = db_find_one("user", {"account": params['account']})
    if ret is not None:
        res.statusCode = 1106
        return make_response(jsonify(res.to_dict()), 200)

    ret = db_find_one("user", {"email": params['email']})
    if ret is not None:
        res.statusCode = 1107
        return make_response(jsonify(res.to_dict()), 200)

    params['password'] = hashlib.sha256(params['password']).hexdigest()
    md5 = hashlib.md5()
    md5.update(params['email'] + str(time.time()))
    params['confirmEmail'] = md5.hexdigest()

    if MAIL_VERIFY_REQUIRE:
        try:
            send_confirm_email(params['email'], params['account'], params['confirmEmail'])
        except:
            res.statusCode = 1111
            return make_response(jsonify(res.to_dict()), 200)

    new_user =  parseUserInfo(params)
    if 'contact' in params:
        new_user.contact = params['contact']
    new_user.type = 'label' if LABEL_PLATFORM else 'normal'
    db_insert_one('user', new_user.to_db())
    if params.get('usergroup') == 'reporter':
        db_insert_one('liability', {"reporter": new_user.id, "reviewer": 'imsight'})
    else:
        db_insert_one('liability', {"reporter": 'imsight', "reviewer": new_user.id})
    db_insert_one('ownership', {"userID": new_user.id, "shared": {}})

    res.statusCode = 1000
    res.data = new_user.to_dict()
    return make_response(jsonify(res.to_dict()), 200)

def check_account():
    res = response()
    account = request.args.get('account')
    ret = db_find_one("user", {"account": account})
    res.statusCode = 1107 if ret is not None else 1000
    return make_response(jsonify(res.to_dict()), 200)

def check_email():
    res = response()
    email = request.args.get('email')
    ret = db_find_one("user", {"email": email})
    res.statusCode = 1107 if ret is not None else 1000
    return make_response(jsonify(res.to_dict()), 200)
