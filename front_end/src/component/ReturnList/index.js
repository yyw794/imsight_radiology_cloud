import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'
import './index.less'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {VIEW_MODALITY_URL, openWindow, openWindowBlank} from "../StudyTable";
import Immutable from 'immutable'

/**
 *
 * @type {Immutable.Map | Immutable.Map<(value: Immutable.Map<K, V>) => Immutable.Map<K, V>, V> | Immutable.Map<string, (value: V) => V>}
 */
const allSkip=new Immutable.Map();
function getUrlParams(){
    let htmlHref = window.location.href;
    htmlHref = htmlHref.replace(/^http:\/\/[^/]+/, "");
    let addr = htmlHref.substr(htmlHref.lastIndexOf('/', htmlHref.lastIndexOf('/') - 1) + 1);
    let index = addr.lastIndexOf("\/");
    //js 获取字符串中最后一个斜杠后面的内容
    let str = decodeURI(addr.substring(index + 1, addr.length));
    // js 获取字符串中最后一个斜杠前面的内容
    // let str = decodeURI(addr.substring(0, index));
    return str;
}
/**
 *
 * @type {{cursor: string, WebkitUserSelect: string, MozUserSelect: string, msUserSelect: string, userSelect: string}}
 */
const style = {
  'cursor': 'pointer',
  'WebkitUserSelect': 'none',
  'MozUserSelect': 'none',
  'msUserSelect': 'none',
  'userSelect': 'none',
}
/**
 * 返回列表组件
 * @return {Component}
 */
const Return = withRouter(
  ({ history }) => (
      <span>
      <Link to="/">
      <a style={{float:"left",color:"#fff",height:"3rem"}}>
        <img style={style} className='logo-image'style={{
            cursor:"pointer",
            userSelect:"none",
            width:"1rem",
            verticalAlign:"middle",
            marginRight:"10px",
            marginLeft:"20px"}} src={require('../../../static/images/returnList.png')} /*onClick={() => history.push('/')}*//>
          <span style={{fontSize:"12px",float:"right",marginTop:"1px"}}><FormattedMessage id='ReturnList.button.name'/></span>
      </a>
      </Link>
          {
              JSON.parse(localStorage.getItem("fellowParams"))?JSON.parse(localStorage.getItem("fellowParams")).followUp?window.location.pathname.split("/")[1]!=="report"&& JSON.parse(localStorage.getItem("fellowParams")).followUp&&JSON.parse(localStorage.getItem("fellowParams")).state=="completed"&&JSON.parse(localStorage.getItem("fellowParams")).modality!=="CR"?<a title="随访" style={{float:"left",color:"#fff",height:"30px"}} onClick={e => {
                  e.stopPropagation()
                  let patientID = JSON.parse(localStorage.getItem("fellowParams")).patientID
                  var arr=[]
                  if(JSON.parse(localStorage.getItem("fellowParams")).followUp.split("?")[1]==="fixed"){
                       arr=[JSON.parse(localStorage.getItem("fellowParams")).followUp.split("?")[0].split(".").join("-"),getUrlParams()];
                  }else{
                       arr=[getUrlParams(),JSON.parse(localStorage.getItem("fellowParams")).followUp.split("?")[0].split(".").join("-")];
                  }
                  window.location.href=`/view_ct_follow_up/${patientID}/${arr.join(",")}`}}>
                  <img style={style} className='logo-image'style={{
                      cursor:"pointer",
                      userSelect:"none",
                      width:"46px",
                      verticalAlign:"middle",
                      marginRight:"10px",
                      marginLeft:"20px"}} src={require('../../../static/images/tool/followup.png')} /*onClick={() => history.push('/')}*//>
              </a>:"":"":""
          }
      </span>

  )
)

export default Return
