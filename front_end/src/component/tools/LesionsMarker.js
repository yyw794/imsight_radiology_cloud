import * as cornerstone from 'cornerstone-core'
import {EVENTS, toolColors} from 'cornerstone-tools'
import * as cornerstoneMath from 'cornerstone-math'
// import {isFunc} from '../../../util'
import {isFunction} from 'underscore'
import {EventEmitter} from 'events'
import md5 from 'blueimp-md5'
import {CHEST_DISEASES_EN} from '../../data'
import moment from 'moment'
import { GetLocaledText } from '../../localedFuncs'

class LesionsMarker extends EventEmitter {
  static EVENTS = {
    DATA_MODIFIED: 'LesionsMarkerDataChange',
    DATA_ADDED: 'LesionsMarkerDataAdded',
    DATA_DELETED: 'LesionsMarkerDataDeleted',
  }

  constructor(element, component) {
    super()
    this.lesions = []
    this.element = element

    this.dragRecOld = null
    this.resizeId = null
    this.moveId = null
    this.selectId = null
    this.isAddingLesion = false
    this.moveStartPoint = null
    this.options = {}

    // this._showPopoverCallback = null
    this.onKeyPress = this.onKeyPress.bind(this)
    this.onImageRendered = this.onImageRendered.bind(this)
    this.onMouseDrag = this.onMouseDrag.bind(this)
    this.onMouseDown = this.onMouseDown.bind(this)
    this.onMouseUp = this.onMouseUp.bind(this)
    this.onMouseClick = this.onMouseClick.bind(this)
    this.onDoubleClick = this.onDoubleClick.bind(this)
    this.component = component
  }

  resetState() {
    this.dragRecOld = null
    this.resizeId = null
    this.moveId = null
    this.selectId = null
    this.isAddingLesion = false
    this.moveStartPoint = null
    cornerstone.updateImage(this.element)
  }

  setState(lesions) {
    this.lesions = lesions
    cornerstone.updateImage(this.element)
  }

  getState() {
    return JSON.parse(JSON.stringify(this.lesions))
  }

  getROIById(labelID) {
    return  this.lesions.find(d => d.labelID === labelID)
  }

  onImageRendered(e) {
    const eventData = e.detail
    const context = eventData.canvasContext.canvas.getContext('2d')
    // context.setTransform(1, 0, 0, 1, 0, 0)
    let i = 0
    for (let dis of this.lesions) {
            let labelID = dis.labelID
            const active = labelID === this.selectId || labelID === this.moveId || labelID === this.resizeId
            const color = active ? '#2fa8e6' : '#ffff00'
            context.beginPath();
            context.strokeStyle = color;
            context.lineWidth = 8;
            context.font = "50px serif";
            context.rect(dis.coord.x, dis.coord.y, dis.width, dis.height);
            context.stroke();

            context.fillStyle = color
            //context.fillText(GetLocaledText(this.component, CHEST_DISEASES_EN[dis.diseaseTag]), dis.coord.x + dis.width + 10, dis.coord.y + dis.height / 2)
            // handle
            context.beginPath();
            // context.rect(dis.coord.x + dis.width - 25, dis.coord.y + dis.height - 25, 50, 50)
            context.arc(dis.coord.x + dis.width, dis.coord.y + dis.height, 15, 0, 2*Math.PI)
            context.fill()
            i++
    }

  }

  pointWithinLabel(point, label) {
    // FIXME: Should we ensure width and height > 0?
    let rect = {
      left: label.width > 0 ? label.coord.x : label.coord.x + label.width,
      top: label.height > 0 ? label.coord.y : label.coord.y + label.height,
      width: Math.abs(label.width),
      height: Math.abs(label.height)
    }
    return cornerstoneMath.point.insideRect(point, rect)
    // add 10px padding to make sure zero area rect can be selected
    // return (
    //   rect.topLeft.x < point.x &&
    //   rect.topLeft.y < point.y &&
    //   rect.rightBottom.x > point.x &&
    //   rect.rightBottom.y> point.y
    // )
  }

  // giving a click point, return index of rectangle that is clicked on
  getActiveRect(point) {
    for (let i = 0; i < this.lesions.length; i++) {
      if (this.pointWithinLabel(point, this.lesions[i])) {
        // select
        return this.lesions[i].labelID
      }
    }
    return null
  }

  // giving a click point, return index of rectangle whose handle is clicked
  getRectWithHandleClicked(point) {
    for (let i = 0; i < this.lesions.length; i++) {
      let lesion = this.lesions[i]
      let handleCoord = {x: lesion.coord.x + lesion.width, y: lesion.coord.y + lesion.height}
      if (cornerstoneMath.point.distance(handleCoord, point) < 15) {
        // drag handle to resize
        return lesion.labelID
      }
    }
    return null
  }

  onMouseDrag(e) {
    if ((!this.resizeId||!this.moveStartPoint)&&this.dragRecOld) {
        const eventData = e.detail
        const coords = eventData.currentPoints.image
        if (!eventData.startPoints) return
        const startPoint = eventData.startPoints.image
      // move
      // let w = this.dragRecOld.width
      // let h = this.dragRecOld.height
      let offset = {x: startPoint.x - this.dragRecOld.coord.x, y: startPoint.y - this.dragRecOld.coord.y}
      let data = this.getROIById(this.moveId)
      data.coord = {x: coords.x - offset.x, y: coords.y - offset.y}
      cornerstone.updateImage(eventData.element)
      // data.rightBottom = {x: coords.x - offset.x + w, y: coords.y - offset.y + h}
    } else {
        if (!this.resizeId || !this.moveStartPoint) {return}
        const eventData = e.detail
        const coords = eventData.currentPoints.image
        if (!eventData.startPoints) return
        const startPoint = this.moveStartPoint

        let targetLabel = this.getROIById(this.resizeId)
        if (!targetLabel) {return}
        targetLabel.width = coords.x - startPoint.x
        targetLabel.height = coords.y - startPoint.y

        cornerstone.updateImage(eventData.element)
    }

  }

  isTargetButton(e) {
    const eventData = e.detail
    return !this.options.mouseButtonMask || eventData.which === this.options.mouseButtonMask
  }

  onMouseDown(e) {
    console.log('onMouseDown');
    if (!this.isTargetButton(e)) {return}
    const eventData = e.detail
    const coords = eventData.currentPoints.image
    this.moveId = this.getActiveRect(coords)
   if (this.resizeId) {
       document.body.style.cursor="ne-resize";
       return;
   }
    if (this.moveId !== null) {
      // store rectangle's initial state
      this.dragRecOld = Object.assign({}, this.getROIById(this.moveId))
      this.selectId = this.moveId
      document.body.style.cursor="move";
      return
    }else{
        document.body.style.cursor="default";
        let lid = md5(Date.now())
        //let defaultDisease = Object.keys(CHEST_DISEASES_EN)[0]
        let newLesion = {
            labelID: lid,
            diseaseTag: "",
            coord: coords,
            width:0,
            height: 0,
            seriesID: 0, // FIXME: hardcode
            modality: 'DX', // FIXME: hardcode
            createDate: moment().format('YYYYMMDD')
        }
        this.lesions.push(newLesion)
        this.resizeId = lid
        this.selectId = lid
        this.isAddingLesion = true
        this.moveStartPoint = coords
        console.log('create new lesion', this.resizeId);
        cornerstone.updateImage(eventData.element)
    }

  }

  onMouseUp(e) {
    // NOTICE: without drag, onMouseUp will not be triggered..
    document.body.style.cursor="default";
    for (let i = 0; i < this.lesions.length; i++){
          if(this.lesions[i].width==0||this.lesions[i].height==0){
              this.lesions.splice(i,1)
          }
    }
    if (!this.isTargetButton(e)) {return}
    const eventData = e.detail
    const coords = eventData.currentPoints.image
    const pageCoords = eventData.currentPoints.page
    if (this.moveId !== null) {
      this.selectId = this.moveId
      this.emit(LesionsMarker.EVENTS.DATA_MODIFIED)
    }
    if (this.isAddingLesion) {
          // this._showPopoverCallback(this.resizeId, pageCoords, this.getROIById(this.resizeId).diseaseTag, this.element)
         // this.emit(LesionsMarker.EVENTS.DATA_MODIFIED)
          this.emit(LesionsMarker.EVENTS.DATA_ADDED, this.resizeId, pageCoords, this.getROIById(this.resizeId)?this.getROIById(this.resizeId).diseaseTag:"", this.element)
          this.isAddingLesion = false
          this.resizeId = null
          this.moveStartPoint = null
          return
    }
   // end resizing
    if (this.resizeId) {
          this.resizeId = null
          this.moveStartPoint = null
          // this.selectId = null
          this.emit(LesionsMarker.EVENTS.DATA_MODIFIED)
          return
      }
    this.dragRecOld = null
    cornerstone.updateImage(eventData.element)
  }
  endResizing(){
      this.resetState()

  }
  onMouseClick(e) {
    console.log('onMouseClick');
    document.body.style.cursor="default";
    if (!this.isTargetButton(e)) {return}
      for (let i = 0; i < this.lesions.length; i++){
          if(this.lesions[i].width==0||this.lesions[i].height==0){
              this.lesions.splice(i,1)
          }
      }
    const eventData = e.detail
    const coords = eventData.currentPoints.image
    const pageCoords = eventData.currentPoints.page
    // check if is adding a new lesion
    // if (this.isAddingLesion) {
    //
    //   // this._showPopoverCallback(this.resizeId, pageCoords, this.getROIById(this.resizeId).diseaseTag, this.element)
    //   this.emit(LesionsMarker.EVENTS.DATA_MODIFIED)
    //     console.log("this.resizeId",this.resizeId)
    //   this.emit(LesionsMarker.EVENTS.DATA_ADDED, this.resizeId, pageCoords, this.getROIById(this.resizeId)?this.getROIById(this.resizeId).diseaseTag:"", this.element)
    //   this.isAddingLesion = false
    //   this.resizeId = null
    //   this.moveStartPoint = null
    //   return
    // }

    // end resizing
      console.log("this.isAddingLesion",this.isAddingLesion)
    if (this.resizeId) {
      this.resizeId = null
      this.moveStartPoint = null
      // this.selectId = null
      this.emit(LesionsMarker.EVENTS.DATA_MODIFIED)
      return
    }

    // check if click on handle
    this.resizeId = this.getRectWithHandleClicked(coords)
    if (this.resizeId !== null) {
      this.selectId = this.resizeId
      this.moveStartPoint =this.getROIById(this.resizeId)? this.getROIById(this.resizeId).coord:""
      cornerstone.updateImage(eventData.element)
      return
    }

    // check if click on rectangle
    this.selectId = this.getActiveRect(coords)
    if (this.selectId !== null) {
      cornerstone.updateImage(eventData.element)
      return
    }

    // create a new lesion

    // // let lid = 'tmp_' + Date.now()
    // let lid = md5(Date.now())
    // let defaultDisease = Object.keys(CHEST_DISEASES_EN)[0]
    // let newLesion = {
    //   labelID: lid,
    //   diseaseTag: defaultDisease,
    //   coord: coords,
    //   width:0,
    //   height: 0,
    //   seriesID: 0,
    //   modality: 'DX',
    //   createDate: moment().format('YYYYMMDD')
    // }
    // this.lesions.push(newLesion)
    // this.resizeId = lid
    // this.selectId = lid
    // this.isAddingLesion = true
     //this.moveStartPoint = coords
    // console.log('create new lesion', this.resizeId);

    cornerstone.updateImage(eventData.element)
  }

  onMouseMove = (e) => {
      const eventData = e.detail
      for (let i = 0; i < this.lesions.length; i++){
          if(this.lesions[i].width==0||this.lesions[i].height==0){
              this.lesions.splice(i,1)
          }
      }
      cornerstone.updateImage(eventData.element)
    // if (!this.resizeId || !this.moveStartPoint) {return}
    //
    // const eventData = e.detail
    // const coords = eventData.currentPoints.image
    // if (!eventData.startPoints) return
    // const startPoint = this.moveStartPoint
    //
    // let targetLabel = this.getROIById(this.resizeId)
    // if (!targetLabel) {return}
    // targetLabel.width = coords.x - startPoint.x
    // targetLabel.height = coords.y - startPoint.y
    //
    // cornerstone.updateImage(eventData.element)
  }

  onDoubleClick(e) {
    if (!this.isTargetButton(e)) {return}
    const eventData = e.detail
    const pageCoords = e.detail.currentPoints.page
    const coords = e.detail.currentPoints.image
    let labelID = this.getActiveRect(coords)
    this.emit(LesionsMarker.EVENTS.DATA_ADDED, labelID, pageCoords, this.getROIById(labelID)?this.getROIById(labelID).diseaseTag:"", e.detail.element)
  }

  onKeyPress(e) {
    if (e.key === 'Delete'||e.key==='Del') {//'Del' IE11, chrome firefox 'Delete'
      console.log(this.selectId,e.key,e.keyCode);
      this.lesions = this.lesions.filter(d => d.labelID !== this.selectId)
      //this.emit(LesionsMarker.EVENTS.DATA_MODIFIED)
      this.emit(LesionsMarker.EVENTS.DATA_DELETED)
      cornerstone.updateImage(this.element)
    }
  }

  // setShowPopoverCallback(func) {
  //   this._showPopoverCallback = func
  // }

  /**
   * set this.shoulStopAdding
   * @param {[function]} func [a function that return a bool value, indicating if it should stop adding lesion]
   */
  setShouldStopAdding(func) {
    this.shoulStopAdding = func
  }

  setName(labelID, diseaseTag) {
    let item = this.lesions.find(d => d.labelID === labelID)
    item.diseaseTag = diseaseTag?diseaseTag:""
    this.emit(LesionsMarker.EVENTS.DATA_MODIFIED)
    cornerstone.updateImage(this.element)
  }

  getData() {
    return [...this.lesions]
  }

  enable(element) {
     this.element.addEventListener(EVENTS.IMAGE_RENDERED, this.onImageRendered)
      this.element.removeEventListener(EVENTS.MOUSE_DOWN, this.onMouseDown)
      this.element.removeEventListener(EVENTS.MOUSE_DRAG, this.onMouseDrag)
      this.element.removeEventListener(EVENTS.MOUSE_UP, this.onMouseUp)
      this.element.removeEventListener(EVENTS.MOUSE_CLICK, this.onMouseClick)
      this.element.removeEventListener(EVENTS.MOUSE_MOVE, this.onMouseMove)
      this.element.removeEventListener(EVENTS.MOUSE_DOUBLE_CLICK, this.onDoubleClick)
      document.removeEventListener('keydown', this.onKeyPress, false)
  }

  disable() {
    this.element.removeEventListener(EVENTS.IMAGE_RENDERED, this.onImageRendered)
    this.element.removeEventListener(EVENTS.MOUSE_DOWN, this.onMouseDown)
    this.element.removeEventListener(EVENTS.MOUSE_DRAG, this.onMouseDrag)
    this.element.removeEventListener(EVENTS.MOUSE_UP, this.onMouseUp)
    this.element.removeEventListener(EVENTS.MOUSE_CLICK, this.onMouseClick)
    this.element.removeEventListener(EVENTS.MOUSE_MOVE, this.onMouseMove)
    this.element.removeEventListener(EVENTS.MOUSE_DOUBLE_CLICK, this.onDoubleClick)
    document.removeEventListener('keydown', this.onKeyPress, false)
    this.resetState()
  }

  activate(element, mouseButtonMask) {
    this.options = {mouseButtonMask}
    this.element.addEventListener(EVENTS.MOUSE_DOWN, this.onMouseDown)
    this.element.addEventListener(EVENTS.MOUSE_DRAG, this.onMouseDrag)
    this.element.addEventListener(EVENTS.MOUSE_UP, this.onMouseUp)
    this.element.addEventListener(EVENTS.MOUSE_CLICK, this.onMouseClick)
    this.element.addEventListener(EVENTS.MOUSE_MOVE, this.onMouseMove)
    this.element.addEventListener(EVENTS.MOUSE_DOUBLE_CLICK, this.onDoubleClick)
    document.addEventListener('keydown', this.onKeyPress, false)
  }

  deactivate() {
    this.element.removeEventListener(EVENTS.MOUSE_DOWN, this.onMouseDown)
    this.element.removeEventListener(EVENTS.MOUSE_DRAG, this.onMouseDrag)
    this.element.removeEventListener(EVENTS.MOUSE_UP, this.onMouseUp)
    this.element.removeEventListener(EVENTS.MOUSE_CLICK, this.onMouseClick)
    this.element.removeEventListener(EVENTS.MOUSE_MOVE, this.onMouseMove)
    this.element.removeEventListener(EVENTS.MOUSE_DOUBLE_CLICK, this.onDoubleClick)
    document.removeEventListener('keydown', this.onKeyPress, false)
    this.resetState()
  }
}

export default LesionsMarker
