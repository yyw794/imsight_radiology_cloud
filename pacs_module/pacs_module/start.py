from request_interface.request_image import today_request_function, time_task_a_hour, init_do_something, request_history_list, _test_function_
from request_interface.http_server import start_server
import thread
from request_interface.initconfig import get_AppTitle, get_ListenPACSPort, get_if_push_model
from request_interface.pacs_interface import (store_listen, do_upload_image_path)
from multiprocessing import Process
import logging
from request_interface.logger import *
from request_interface.http_client import login_request, upload_image_path
import time
import sys

log = logging.getLogger("PACSModule.start")


if __name__ == "__main__":
	try:
		log.info("*****pacs module start*****")
		
		if init_do_something() is False:
			log.error("init_do_something error")
			sys.exit()
			
		result_state = login_request()
		number = 0
		while result_state is False and number < 5:
			number = number + 1
			result_state = login_request()
			time.sleep(3)
		
		if result_state is False:
			log.error("five number login_request error")
			sys.exit()

		if get_if_push_model() is False:
			_test_function_()
		
		store_listen_process = Process(target=store_listen, args=(get_AppTitle(), get_ListenPACSPort(), ""))
		store_listen_process.start()
		if get_if_push_model() is False:
			thread.start_new_thread(today_request_function, ())
			thread.start_new_thread(time_task_a_hour, ())
			thread.start_new_thread(do_upload_image_path, ("system_pull",))
			
		store_listen_process.join()
	except KeyboardInterrupt:
		log.error("create listen_process thread error")

