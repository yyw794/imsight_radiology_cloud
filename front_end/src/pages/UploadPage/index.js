import React, { Component } from 'react'
import { Upload, Layout, Button, Form, message,Modal } from 'antd';

const { Header, Content, Footer } = Layout;
import Immutable from 'immutable'
const FormItem = Form.Item;
const Dragger = Upload.Dragger;
import {CODE_MESSAGE} from '../../codeMessage'
import Uploader from '../../component/Upload'

import SignoutButton from '../../component/SignoutButton'
import StudyTable from '../../component/StudyTable'
import Logo from '../../component/Logo'

import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import io from 'socket.io-client'
import {getUser} from '../../auth'

import './index.less'

class UploadPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      studyList: Immutable.List(),
      visible:false,
    }
    this.initSocket()
    this.onClickModel = this.onClickModel.bind(this);
    this.handleOkModel = this.handleOkModel.bind(this);
    this.handleCancelModel = this.handleCancelModel.bind(this);
  }

  componentWillUnmount(){
    this.socket.emit('studyListClose',{
      userID:getUser().account,
      taskID:'studyList'     
    })
    this.socket.close()
  }

  addToStudyList(data) {
    data.map((study)=>{
      let idx = this.state.studyList.findIndex((value,index,array)=>{
        return value.toJS().studyUID === study.studyUID
      })
      if(idx>=0){
        this.setState({studyList:this.state.studyList.updateIn([idx,'state'],state=>state=study.state)}) 
      }
      else{
        let listUpdated = this.state.studyList.toJS().concat(study)
        this.setState({
          studyList: Immutable.fromJS(listUpdated)
        })
      }
    })
  }

  initSocket(){
    this.socket = io(window.location.origin,{transports: ['websocket']})
    this.socket.connect()
    this.socket.on('connect',()=>{ console.log("upload page socket connected") })
    this.socket.on('studyListUpdate',(payload)=>{
      let idx = this.state.studyList.findIndex((value,index,array)=>{
       return value.toJS().studyUID===payload.data.studyUID
      })
      console.log('studyList update',payload.data,this.state.studyList,idx)
      if(idx>=0)
      {
        this.setState({studyList:this.state.studyList.updateIn([idx,'patientID'],patientID=>patientID=payload.data.patientID)
                       .updateIn([idx,'birthDate'],birthDate=>birthDate=payload.data.birthDate)
                       .updateIn([idx,'bodyPartExamined'],bodyPartExamined=>bodyPartExamined=payload.data.bodyPartExamined)
                       .updateIn([idx,'modality'],modality=>modality=payload.data.modality)
                       .updateIn([idx,'gender'],gender=>gender=payload.data.gender)
                       .updateIn([idx,'hospital'],hospital=>hospital=payload.data.hospital)
                       .updateIn([idx,'state'],state=>state=payload.data.state)
                       .updateIn([idx,'patientName'],patientName=>patientName=payload.data.patientName)
                       .updateIn([idx,'labelNum'],labelNum=>labelNum=payload.data.labelNum)
                       .updateIn([idx,'taskID'],taskID=>taskID=payload.data.taskID)
                       .updateIn([idx,'result'],result=>result=payload.data.result)                  
                       })
      }
    })
    this.socket.on('disconnect',()=>{ console.log("upload page socket disconnected")})
    this.socket.emit('studyListOpen',{
      userID:getUser().account,
      taskID:'studyList'
    })
  }

    //帮助文档
    onClickModel() {
        this.setState({
            visible: true,
        })
    }

    handleOkModel() {
        this.setState({
            visible: false,
        })
    }

    handleCancelModel() {
        this.setState({
            visible: false,
        })
    }
  render() {
    return (
      <div id='upload-page' className='page'>
        <Header style={{height:"3rem",lineHeight:"3rem",padding:"0 2rem"}}>
          <Logo/>
          <SignoutButton/>
          <span style={{color:"#fff",float:"right",marginRight:"10px",cursor:"pointer",height:"3rem",marginTop:"-0.3rem"}} onClick={this.onClickModel}><img src={require('../../../static/images/help_nor.png')}/></span>
      {/*<LanguageDropdownList setAppState={state => {this.props.setAppState(state)}}/>*/}
        </Header>
        <Content className='content'>
          <div className='content-title'>
            <h3 style={{float: 'left'}}><FormattedMessage id='UploadPage.content-title.upload'/></h3>
            <Button type="primary" icon="rollback"
              style={{float: 'right'}}
              onClick={() => this.props.history.push('/')}>
              <FormattedMessage id="UploadPage.button.return"/>
            </Button>
          </div>
          <Uploader
            onSuccess={(data) => {
              if (data.length < 1) return
              message.success(GetLocaledText(this,"UploadPage.dialog.finished"));
              this.addToStudyList(data)
            }}
            onFail={(err) => {
              let response = err.response
              let errorMsg = GetLocaledText(this,"UploadPage.dialog.failed")
              if (CODE_MESSAGE.has(response.statusCode)) {
                errorMsg = `${CODE_MESSAGE.get(response.statusCode)}`
              }
              message.error(errorMsg);
            }}/>
          <h3><FormattedMessage id='UploadPage.content-title.uploaded'/></h3>
          <div className='table-wrapper'>
            <StudyTable dataSource={this.state.studyList.toJS()} showDetectionOption={false}/>
          </div>
        </Content>
          <Modal
              title={GetLocaledText(this, "HelpDocument")}
              visible={this.state.visible}
              onOk={this.handleOkModel}
              onCancel={this.handleCancelModel}
              width={840}
              bodyStyle={{background:"#000",color:"#fff"}}
              footer={null}
          >
             <img src={require('../../../static/images/helpDocument.png')} style={{width:"794px"}} />
          </Modal>
      </div>
    )
  }

}

export default injectIntl(UploadPage)
