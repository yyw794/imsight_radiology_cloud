import React, { Component } from 'react'
import CTFollowUp from '../../component/CTFollowUp'
import api from '../../api'
import LoadingWrapper from '../../component/LoadingWrapper'
import axios from 'axios'
import { Card, Layout } from 'antd'
import {addCTMetaData} from '../../metaData.js'
import {getUser} from '../../auth'
import _ from 'underscore'

import './index.less'

const { Header, Content, Footer } = Layout;

class CTViewerPage extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    // let {age, gender, patientName, studyDate, studyTime} = this.props.data.study
    let user = getUser()
    let userID = ''
    if (user) {
      userID = user.account
    }
    return (
      <div style={{height: '100%', backgroundColor: '#000'}} id='ct-viwer-page'>
        <CTFollowUp
          stack1={this.props.data.fixedStudy.stacks}
          stack2={this.props.data.movingStudy.stacks}
          labels1={this.props.data.fixedStudy.labels}
          labels2={this.props.data.movingStudy.labels}
          study1={this.props.data.fixedStudy}
          study2={this.props.data.movingStudy}
          studylist={this.props.data.studies}
          compareNodules={this.props.data.compareNodules}
          studyUIDs={this.props.match.params.ids.replace(/-/g, '.').split(',')}
          userID={userID}
          style={{height: '100%', position: 'relative'}}/>
      </div>
    )
  }

}

async function getData(props) {
  let patientID = props.match.params.patientID
  let studyUIDs = props.match.params.ids.replace(/-/g, '.').split(',')
  let studies = await api.getStudylist({patientID, needData: 'True'})
  let reports = []
  for (let study of studies) {
    // let study = await api.getStudyDetail({}, {studyUID})
    const studyUID = study.studyUID
    const series = Object.values(study.series)[0]
    study.stacks = {
      x: {
        currentImageIdIndex: 0,
        imageIds: series.instances.x.map(ins => ins.url)
      },
      z: {
        currentImageIdIndex: 0,
        imageIds: series.instances.z.map(ins => ins.url)
      },
      y: {
        currentImageIdIndex: 0,
        imageIds: series.instances.y.map(ins => ins.url)
      },
    }
    let labels = await api.getLabels({taskID: study.taskID})
    labels.sort((a, b) => b.malg - a.malg)
    study.labels = labels.map((label, key) => Object.assign(label, {key: key + 1}))

    addCTMetaData(study)
    // studies.push(study)
  }

  studies.sort((a, b) => a.studyDate + a.studyTime > b.studyDate + b.studyTime)
  let fixedStudy = studies.find(s => s.studyUID === studyUIDs[0])
  let movingStudy = studies.find(s => s.studyUID === studyUIDs[1])
  let noduleRegister = await api.getNoduleRegister({taskID_fixed: fixedStudy.taskID, taskID_moving: movingStudy.taskID})

  // let noduleRegister = registerResp.registerNodules
  //console.log(noduleRegister,studies);
  let compareNodules = _.zip(noduleRegister.fixed, noduleRegister.moving)
  compareNodules.forEach(pair => {
    pair[0].key = fixedStudy.labels.find(label => label.labelID === pair[0].labelID).key
    pair[1].key = movingStudy.labels.find(label => label.labelID === pair[1].labelID).key
  })
  return await {studies, compareNodules, fixedStudy, movingStudy}
}

CTViewerPage = LoadingWrapper(CTViewerPage, getData)

export default CTViewerPage
