#!/bin/python
#coding=utf-8
from flask import send_from_directory, request, abort
from flask_login import current_user

import sys, os

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one

required_key = ['uid']

def fetch_screenshot():
    params = request.args.to_dict()
    if not all(key in params for key in required_key):
        return send_from_directory("./data/image/notFound", "notFound.png")

    uid = params['uid']
    resource_info = db_find_one("resource", {"uid": uid})
    if resource_info != None:
        path = resource_info['img_path']
        filename = resource_info['filename']
        return send_from_directory(path, filename)
    else:
        return send_from_directory("./data/image/notFound", "notFound.png")

def fetch_study_image():
    params = request.args.to_dict()

    if not all(key in params for key in required_key):
        return send_from_directory("./data/image/notFound", "notFound.png")

    uid = params['uid']
    resource_info = db_find_one("resource", {"uid": uid})
    if resource_info != None:
        path = resource_info['img_path']
        filename = resource_info['filename']
        return send_from_directory(path, filename)
    else:
        return send_from_directory("./data/image/notFound", "notFound.png")

def fetch_study_dicom():
    params = request.args.to_dict()

    if not all(key in params for key in required_key):
        return send_from_directory("./data/image/notFound", "notFound.png")

    uid = params['uid']
    resource_info = db_find_one("resource", {"uid": uid})

    path = resource_info['dcm_path']
    filename = resource_info['dicom']
    return send_from_directory(path, filename)