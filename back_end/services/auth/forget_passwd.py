#!/bin/python
# -*- coding: utf-8 -*-
import hashlib
import os
import sys
import time

from flask import make_response, request, jsonify, Flask, session
from flask_mail import Message, Mail

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_find_one, db_update_one
from models.response.response import response
from config import MAIL_SERVER, MAIL_PORT, MAIL_USE_SSL, MAIL_USERNAME, MAIL_PASSWORD, MAIL_DEFAULT_SENDER, HOSTNAME

app = Flask(__name__)
# email
app.config['MAIL_SERVER'] = MAIL_SERVER
app.config['MAIL_PORT'] = MAIL_PORT
app.config['MAIL_USE_SSL'] = MAIL_USE_SSL
# app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = MAIL_USERNAME
app.config['MAIL_PASSWORD'] = MAIL_PASSWORD
app.config['MAIL_DEFAULT_SENDER'] = MAIL_DEFAULT_SENDER
mail = Mail(app)


def send_reset_email(to, account, token):
    html = u'''
    <html>

    <head>
    <title>重置密码</title>
    </head>

    <body>
    <p>您的账号{account}正在申请重置密码，请复制以下链接到浏览器打开完成重置。</p>
    <p>http://{HOST}/reset?account={account}&token={token}</p>
    </body>

    </html>
    '''.format(**{
        'account': account,
        'HOST': HOSTNAME,
        'token': token
    })
    msg = Message(
        '重置密码-视见科技',
        recipients=[to],
        html=html,
        sender=app.config['MAIL_DEFAULT_SENDER']
    )
    mail.send(msg)


def reset_password_handler():
    """
    @api {post} /auth/reset_password  重置密码
    @apiVersion 0.0.1
    @apiGroup Auth
    @apiParam {String} account 用户名
    @apiParam {String} password 密码
    @apiParam {String} token 令牌
    @apiParamExample {json} Request-Example:
                 {"account":"example","password":"example", "token":"b32d015a90fe436ba742127cb1cce701"}
    @apiSuccessExample {json} Success-Response:
    {
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "statusCode": 1200
    }
    """
    res = response()
    json_data = request.get_json()
    required_key = ['account', 'password', 'token']

    if not all(key in json_data for key in required_key):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    account = json_data['account']
    token = json_data['token']
    print session
    if account not in session or session[account] != token:
        res.statusCode = 1200
        return make_response(jsonify(res.to_dict()), 200)

    session.pop(account)
    new_password = hashlib.sha256(json_data['password']).hexdigest()
    db_update_one('user', {"account": account}, {'password': new_password})

    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)


def reset_email_handler():
    """
    @api {post} /auth/send_reset_email  重置邮箱
    @apiVersion 0.0.1
    @apiGroup Auth
    @apiParam {String} email 邮箱
    @apiParamExample {json} Request-Example:
                 {"email":"example@imsightmed.com"}
    @apiSuccessExample {json} Success-Response:
    {
      "data": {
        "account": "example",
        "contact": "example",
        "email": "example",
        "hospital": "example",
        "nickname": "example",
        "type": "normal"
      },
      "statusCode": 1000
    }
    @apiErrorExample {json} Error-Response:
    {
      "data": {},
      "statusCode": 1100
    }
    """
    res = response()
    json_data = request.get_json()
    required_key = ['email']

    if not all(key in json_data for key in required_key):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    ret = db_find_one("user", {"email": json_data['email']})
    if ret is None:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
    # if ret['confirmEmail'] != 'true':
    #    return make_response(jsonify(response['1108'])) 

    md5 = hashlib.md5()
    md5.update(json_data['email'] + str(time.time()))
    token = md5.hexdigest()
    account = ret['account']
    session[account] = token
    send_reset_email(json_data['email'], account, token)

    res.statusCode = 1000
    return make_response(jsonify(res.to_dict()), 200)
