import api from './api'
import Cookies from 'js-cookie'
import {isFunc} from './util'
import {CODE_MESSAGE} from './codeMessage'
import {message} from 'antd'
import {clearState} from './pages/StudyListPage/state'

export function isAuthenticated() {
  return !!Cookies.get('currentUser')
}

export function authenticate(user, onSuccess, onFail) {
  return api.login(user)
  .then(data => {
    Cookies.set('currentUser', data, { expires: 7 })
    clearState()
    isFunc(onSuccess) && onSuccess()
  })
  .catch(err => {
    isFunc(onFail) && onFail(err)
  })
}


export function signout(cb) {
  api.logout()
  .then(response => {
    Cookies.remove('currentUser')
    isFunc(cb) && cb()
  })
  .catch((err) => {
    console.log(err);
  })
}

export function getUser() {
  try {
    return JSON.parse(Cookies.get('currentUser'))
  } catch (e) {
    false
  }
}

export function getUserRole(privilege) {
  let user = getUser();
  if (user.type === 'label' && privilege.includes('reviewer')) {
    return 'reviewer'
  }
  if (user.type === 'label' && privilege.includes('label')) {
    return 'labeler'
  }
  return 'user'
}
