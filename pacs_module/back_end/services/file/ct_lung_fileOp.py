#!/bin/python
#coding=utf-8
import SimpleITK as sitk
import numpy as np
from scipy.misc import imresize
import scipy.misc
import os, sys, shutil, traceback, math, threading
import uuid

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from models.database.database_ops import db_insert_one
from config import ADDRESS, PORT, IMAGE_ROOT, PROTOCOL, USE_INSTANCE_NUMBER
import logging
logger = logging.getLogger()

class ResultThread(threading.Thread):
    def __init__(self, target, args=()):
        super(ResultThread, self).__init__()
        self.target = target
        self.args = args
        self.exitcode = 0
        self.exception = None
        self.exc_traceback = ''

    def run(self):
        try:
            self.result = self.target(*self.args)
        except Exception as e:
            self.exitcode = 1
            self.exception = e
            self.exc_traceback = ''.join(traceback.format_exception(*sys.exc_info()))

    def get_result(self):
        try:
            return self.result
        except Exception:
            return None

def cal_hist_ct(source, window_level=-600, thresh=0.999):
    source = source.ravel()
    total = len(source)

    s_values, bin_idx, s_counts = np.unique(source, return_inverse=True, return_counts=True)

    histogram_dict = {}
    for value, count in zip(s_values, s_counts):
        histogram_dict[value] = count

    current_count = 0
    forward = 0
    backward = 0
    max_value = 0
    min_value = 0
    while float(current_count) / float(total) < thresh:
        h_value = window_level + forward
        l_value = window_level - backward

        if h_value in histogram_dict:
            max_value = h_value
            current_count += histogram_dict[h_value]

        if l_value in histogram_dict:
            min_value = l_value
            current_count += histogram_dict[l_value]

        forward += 1
        backward += 1

    return min_value, max_value

def convert_CT_png(img_arr, data, start, end, meta_data, DCMfolder, PNGfolder, uid):
    spacing = meta_data['spacing']
    origin = meta_data['origin']
    direction = meta_data['direction']
    z_shape, y_shape, x_shape = img_arr.shape
    z_reshape = z_shape * spacing[2] * 2
    y_reshape = y_shape * spacing[1] * 2
    x_reshape = x_shape * spacing[0] * 2

    start_idx_z = int(math.floor(img_arr.shape[0] * start))
    end_idx_z = int(math.floor(img_arr.shape[0] * end) - 1)
    start_idx_x = int(math.floor(img_arr.shape[2] * start))
    end_idx_x = int(math.floor(img_arr.shape[2] * end) - 1)
    start_idx_y = int(math.floor(img_arr.shape[1] * start))
    end_idx_y = int(math.floor(img_arr.shape[1] * end) - 1)

    # gen x-y images
    for k in range(start_idx_z, end_idx_z + 1):
        PNGfilename = 'z_{:03}.png'.format(k)
        file_uid = "{}-z-{}".format(uid, k)
        url         = "{}://{}:{}/api/study_image?uid={}".format(PROTOCOL, ADDRESS, PORT, file_uid)
        PNGpath = os.path.join(PNGfolder, PNGfilename)

        if not os.path.exists(PNGpath):
            img_2d = img_arr[k,:,:]
            if len(img_2d.shape) == 3 and img_2d.shape[0] == 1:
                img_2d = np.squeeze(img_2d, axis=0)
            img_2d = img_2d.astype(np.uint8)
            img_2d = imresize(img_2d, (int(x_reshape), int(y_reshape)))
            scipy.misc.imsave(PNGpath, img_2d)

            data['instances']['z'].append({
                'instanceID': str(k+1),
                'rowPixelSpacing': 0.5,#spacing[0],
                'columnPixelSpacing': 0.5,#spacing[1],
                'imagePositionPatient': [origin[0], origin[1], origin[2]+k*spacing[2]],
                'rowCosines': direction[:3],
                'columnCosines': direction[3:6],
                'rows': y_reshape,#img_arr.shape[1], # width
                'columns': x_reshape,#img_arr.shape[2], # height
                'url': url
            })

            new_resource = {
                "uid": file_uid,
                "img_path": os.path.dirname(PNGpath),
                "filename": os.path.basename(PNGpath),
                "dcm_path": DCMfolder,
                "dicom": "",
            }
            db_insert_one("resource", new_resource)

    # gen z-y images
    for i in range(start_idx_x, end_idx_x + 1):
        PNGfilename = 'x_{:03}.png'.format(i)
        file_uid = "{}-x-{}".format(uid, i)
        url         = "{}://{}:{}/api/study_image?uid={}".format(PROTOCOL, ADDRESS, PORT, file_uid)
        PNGpath = os.path.join(PNGfolder, PNGfilename)

        if not os.path.exists(PNGpath):
            img_2d = img_arr[::-1,:,i]
            if len(img_2d.shape) == 3 and img_2d.shape[2] == 1:
                img_2d = np.squeeze(img_2d, axis=2)
            img_2d = img_2d.astype(np.uint8)
            img_2d = imresize(img_2d, (int(z_reshape), int(y_reshape)))
            scipy.misc.imsave(PNGpath, img_2d)

            data['instances']['x'].append({
                'instanceID': str(i+1),
                'rowPixelSpacing': 0.5,
                'columnPixelSpacing': 0.5,
                'imagePositionPatient': [origin[0]+i*spacing[0], origin[1], origin[2]+(img_arr.shape[0]-1)*spacing[2]],
                'rowCosines': direction[3:6],
                'columnCosines': [0, 0, -1],
                'rows': z_reshape, # width
                'columns': y_reshape, # height
                'url': url
            })

            new_resource = {
                "uid": file_uid,
                "img_path": os.path.dirname(PNGpath),
                "filename": os.path.basename(PNGpath),
                "dcm_path": DCMfolder,
                "dicom": "",
            }
            db_insert_one("resource", new_resource)

    # gen z-x images
    for j in range(start_idx_y, end_idx_y + 1):
        PNGfilename = 'y_{:03}.png'.format(j)
        file_uid = "{}-y-{}".format(uid, j)
        url         = "{}://{}:{}/api/study_image?uid={}".format(PROTOCOL, ADDRESS, PORT, file_uid)
        PNGpath = os.path.join(PNGfolder, PNGfilename)

        if not os.path.exists(PNGpath):
            img_2d = img_arr[::-1,j,:]
            if len(img_2d.shape) == 3 and img_2d.shape[1] == 1:
                img_2d = np.squeeze(img_2d, axis=1)
            img_2d = img_2d.astype(np.uint8)
            img_2d = imresize(img_2d, (int(z_reshape), int(x_reshape)))
            scipy.misc.imsave(PNGpath, img_2d)

            data['instances']['y'].append({
                'instanceID': str(j+1),
                'rowPixelSpacing': 0.5,
                'columnPixelSpacing': 0.5,
                'imagePositionPatient': [origin[0], origin[1]+j*spacing[1], origin[2]+(img_arr.shape[0]-1)*spacing[2]],
                'rowCosines': direction[:3],
                'columnCosines': [0, 0, -1],
                'rows': z_reshape,
                'columns': x_reshape,
                'url': url
            })

            new_resource = {
                "uid": file_uid,
                "img_path": os.path.dirname(PNGpath),
                "filename": os.path.basename(PNGpath),
                "dcm_path": DCMfolder,
                "dicom": "",
            }
            db_insert_one("resource", new_resource)

def insert_CT_instances(series):
    instances = series['instances']
    reverse = False

    try:
        DCMfolder = str(os.path.dirname(instances[0]['filepath']))
        Reader = sitk.ImageSeriesReader()
        DicomNames=Reader.GetGDCMSeriesFileNames(DCMfolder)
        Reader.SetFileNames(DicomNames)
        Image = Reader.Execute()

        instance_0 = DicomNames[0]
        instance_1 = DicomNames[1]
        image_path = DCMfolder
        file_0 = os.path.join(image_path, instance_0)
        file_1 = os.path.join(image_path, instance_1)
        image_0 = sitk.ReadImage(file_0)
        image_1 = sitk.ReadImage(file_1)
        keys_0 = image_0.GetMetaDataKeys()
        keys_1 = image_1.GetMetaDataKeys()

        instanceNumber_0 = 0 if '0020|0013' not in keys_0 else image_0.GetMetaData('0020|0013').rstrip()
        instanceNumber_1 = 1 if '0020|0013' not in keys_1 else image_1.GetMetaData('0020|0013').rstrip()

        reverse = True if int(instanceNumber_0) > int(instanceNumber_1) and USE_INSTANCE_NUMBER else False
        logger.info("reverse: {}".format(reverse))
    except:
        for ins in instances:
            ins.pop('filename')
            ins.pop('filepath')
        return False, False

    img_arr  = sitk.GetArrayFromImage(Image)
    if len(img_arr.shape) != 3:
        for ins in instances:
            ins.pop('filename')
            ins.pop('filepath')
        return False, False

    minPixelValue, maxPixelValue = cal_hist_ct(img_arr)
    series['minPixelValue'] = int(minPixelValue)
    series['maxPixelValue'] = int(maxPixelValue)
    pixelRange = abs(maxPixelValue - minPixelValue)

    img_arr[img_arr<minPixelValue] = minPixelValue
    img_arr[img_arr>maxPixelValue] = maxPixelValue
    img_arr = (img_arr.astype(np.float32) - minPixelValue) / pixelRange * 255.0
    spacing = list(Image.GetSpacing())
    origin  = list(Image.GetOrigin())
    direction = list(Image.GetDirection())

    meta_data = {
        'spacing': spacing,
        'origin': origin,
        'direction': direction
    }

    series['instances'] = {}
    series['instances']['z'] = []
    series['instances']['x'] = []
    series['instances']['y'] = []

    if not os.path.exists(IMAGE_ROOT):
        os.makedirs(IMAGE_ROOT)
    uid = str(uuid.uuid1())
    PNGfolder = os.path.join(IMAGE_ROOT, uid)
    if os.path.exists(PNGfolder):
        shutil.rmtree(PNGfolder)
    os.makedirs(PNGfolder)

    # convert png multithread
    THREAD_NUM = 10
    threads = []
    step = 1.0 / THREAD_NUM
    for i in np.arange(0.0, 1.0, step):
        t = ResultThread(target=convert_CT_png, args=(img_arr, series, i, i + step, meta_data, DCMfolder, PNGfolder, uid))
        t.start()
        threads.append(t)
    for t in threads:
        t.join()

    for t in threads:
        if t.exitcode != 0:
            logger.error(t.exc_traceback)
            raise t.exception

    series['instances']['x'] = sorted(series['instances']['x'], key=lambda i: int(i['instanceID']))
    series['instances']['y'] = sorted(series['instances']['y'], key=lambda i: int(i['instanceID']))
    series['instances']['z'] = sorted(series['instances']['z'], key=lambda i: int(i['instanceID']))

    return reverse, True
