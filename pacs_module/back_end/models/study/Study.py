#!/bin/python
import datetime

def safe_cast(val, to_type=int, default=None):
    try:
        return to_type(val)
    except (ValueError, TypeError):
        return to_type(datetime.datetime.today().strftime('%Y%m%d'))

class Study():
    def __init__(self, studyUID):
        self.studyUID = studyUID
        self.bodyPartExamined = ""
        self.gender = ""
        self.hospital = ""
        self.modality = ""
        self.patientID = ""
        self.patientName = ""
        self.uploadDate = None
        self.studyDate = None
        self.birthDate = None
        self.series = None
        self.result = ""
        self.state = ""
        self.reverse = False

    def to_dict(self, withSeries=True):
        age = 0
        birthDate = ""
        uploadTime = ""

        if self.birthDate is not None:
            # birthDate = (self.birthDate).strftime('%Y-%m-%d')
            birthDate = '{0.year:4d}-{0.month:02d}-{0.day:02d}'.format(self.birthDate)
            age = (datetime.datetime.today() - self.birthDate).days // 365

        if self.uploadDate is not None:
            # uploadTime = (self.uploadDate).strftime('%Y-%m-%d %H:%M:%S')
            uploadTime = '{0.year:4d}-{0.month:02d}-{0.day:02d} {0.hour:02d}:{0.minute:02d}:{0.second:02d}'.format(
                self.uploadDate)

        # studyDate = (self.studyDate).strftime('%Y-%m-%d')
        studyDate = '{0.year:4d}-{0.month:02d}-{0.day:02d}'.format(self.studyDate)
        # studyTime = (self.studyDate).strftime('%H:%M:%S')
        studyTime = '{0.hour:02d}:{0.minute:02d}:{0.second:02d}'.format(self.studyDate)

        ret = {
            "studyUID": self.studyUID,
            "bodyPartExamined": self.bodyPartExamined,
            "gender": self.gender,
            "hospital": self.hospital,
            "modality": self.modality,
            "patientID": self.patientID,
            "patientName": self.patientName,
            "age": age,
            "uploadDate": uploadTime,
            "studyTime": studyTime,
            "studyDate": studyDate,
            "birthDate": birthDate,
            "result": self.result,
            "state": self.state,
            "reverse": self.reverse
        }

        if withSeries and self.series:
            new_dict = {}
            for series in self.series:
                new_dict[series['seriesID']] = series
            ret['series'] = new_dict
            return ret
        return ret

    def to_db(self):
        return {
            "uploadDate": self.uploadDate,
            "studyDate": self.studyDate,
            "birthDate": self.birthDate,
            "bodyPartExamined": self.bodyPartExamined,
            "gender": self.gender,
            "hospital": self.hospital,
            "modality": self.modality,
            "patientID": self.patientID,
            "patientName": self.patientName,
            "studyUID": self.studyUID,
            "series": self.series,
            "result": self.result,
            "state": self.state,
            "reverse": self.reverse
        }

required_key = ['birthDate', 'bodyPartExamined', 'gender', 'hospital', 'modality',\
                'patientID', 'patientName', 'studyDate', 'studyUID', 'uploadDate', 'series', 'result', 'state']

def parseStudyInfo(info):
    if type(info) is not dict:
        # raise ValueError("parseStudyInfo input must be dict type, while type is {}".format(type(info)))
        return None

    if 'reverse' not in info:
        info['reverse'] = False

    if not all(key in info for key in required_key):
        # raise ValueError("parseStudyInfo key mismatch", info.keys(), required_key)
        return None

    new_study = Study(info['studyUID'])
    new_study.birthDate = info['birthDate']
    new_study.bodyPartExamined = info['bodyPartExamined']
    new_study.gender = info['gender']
    new_study.hospital = info['hospital']
    new_study.modality = info['modality']
    new_study.patientID = info['patientID']
    new_study.patientName = info['patientName']
    new_study.studyDate = info['studyDate']
    new_study.uploadDate = info['uploadDate']
    new_study.series = info["series"]
    new_study.result = info["result"]
    new_study.state = info['state']
    new_study.reverse = info['reverse']

    return new_study

def saveStudylist(studylist):
    ret = []
    for study in studylist:
        ret.appent(study.to_db())

    return ret