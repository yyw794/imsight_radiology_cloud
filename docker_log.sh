#!/bin/sh
mkdir docker_logs
mkdir docker_logs/imsightmed
#docker 日志
file_names=$(sudo ls /var/lib/docker/containers/)
for fn in $file_names
do
container_id=$(sudo cat /var/lib/docker/containers/$fn/hostname)
container_name=$(sudo docker inspect -f {{.Config.Image}} $container_id)
log_names=$(sudo find /var/lib/docker/containers/$fn -name *-json.log)
if [ ${#log_names[*]} != 0 ];then 
sudo cp $log_names docker_logs/$container_name
fi
done
sudo chmod 777 -R docker_logs

mkdir imsight_log
sudo mv docker_logs imsight_log

mkdir imsight_log/pacs_module
mkdir imsight_log/back_end
mkdir imsight_log/algo_end

cp -r pacs_module/logfile imsight_log/pacs_module
cp -r back_end/logs imsight_log/back_end
cp -r algo_end/logs imsight_log/algo_end


logs=$(find /var/lib/docker/containers/ -name *-json.log) 
for log in $logs 
do 
cat /dev/null > $log 
done 


current_time=`date +%Y-%m-%d_%H%m%s`
mv imsight_log imsight_log_$current_time
sudo chmod 777 -R imsight_log_$current_time
echo "^_^  download log success in folder imsight_log_$current_time"
