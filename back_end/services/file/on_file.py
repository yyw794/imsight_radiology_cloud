#!/bin/python
# coding=utf-8
import SimpleITK as sitk
import chardet

import datetime
import os
import shutil
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from services.file.x_ray_fileOp import insert_x_ray_instances_dicom
from services.file.ct_lung_fileOp import insert_CT_instances, insert_dicom_CT_instances2
from config import DICOM_ROOT, ERROR_ROOT
import logging

LOGGER = logging.getLogger("radiology")
from services.file.filter import get_body_part_examined, get_DX_PA_ViewPosition
from services.pacs.pacs_api import get_patientInfo


def char2utf8(char):
    """
    string encode utf8
    :param char:
    :return:
    """
    det = chardet.detect(char)
    if det["encoding"]:
        if det['encoding'].lower().find("utf") != -1:
            utf_char = char
        elif det["encoding"].lower().find("jp") != -1:
            try:
                utf_char = char.decode(det["encoding"]).encode("utf8")
            except UnicodeEncodeError:
                LOGGER.error("char decode error. char=%s", [char])
                utf_char = ""
        else:
            try:
                utf_char = char.decode("gbk").encode("utf8")
            except UnicodeEncodeError:
                LOGGER.error("char decode error. char=%s", [char])
                utf_char = ""
    else:
        try:
            utf_char = char.decode("gbk").encode("utf8")
        except UnicodeEncodeError:
            LOGGER.error("char decode error. char=%s", [char])
            utf_char = ""
    return utf_char


def parse_meta(file_path):
    """

    :param file_path:
    :return:
    """
    if not os.path.exists(file_path):
        return None
    try:
        image = sitk.ReadImage(file_path)
        all_keys = image.GetMetaDataKeys()
    except Exception as exc:
        LOGGER.error('get meta data fail. err=%s', exc)
        if not os.path.exists(ERROR_ROOT):
            os.makedirs(ERROR_ROOT)
        filename = os.path.basename(file_path)
        err_filename = os.path.join(ERROR_ROOT, filename)
        shutil.move(file_path, err_filename)
        return None

    size = list(image.GetSize())
    spacing = list(image.GetSpacing())

    modality = '' if '0008|0060' not in all_keys else image.GetMetaData('0008|0060').rstrip()
    study_uid = '' if '0020|000d' not in all_keys else image.GetMetaData('0020|000d').rstrip()
    series_number = '' if '0020|0011' not in all_keys else image.GetMetaData('0020|0011').rstrip()
    series_id = '' if '0020|000e' not in all_keys else image.GetMetaData('0020|000e').rstrip()
    instance_id = '' if '0008|0018' not in all_keys else image.GetMetaData('0008|0018').rstrip()
    study_date = '' if '0008|0020' not in all_keys else image.GetMetaData('0008|0020').rstrip()
    patient_id = '' if '0010|0020' not in all_keys else image.GetMetaData('0010|0020').rstrip()
    patient_name = '' if '0010|0010' not in all_keys else image.GetMetaData('0010|0010').rstrip()
    body_part_examined = '' if '0018|0015' not in all_keys \
        else image.GetMetaData('0018|0015').rstrip()
    study_description = '' if '0008|1030' not in all_keys \
        else image.GetMetaData('0008|1030').rstrip()
    study_time = '' if '0008|0030' not in all_keys \
        else image.GetMetaData('0008|0030').rsplit('.', 1)[0]
    gender = '' if '0010|0040' not in all_keys else image.GetMetaData('0010|0040').rstrip()
    series_description = '' if '0008|103E' not in all_keys \
        else image.GetMetaData('0008|103E').rstrip().upper()
    protocol_name = '' if '0018|1030' not in all_keys \
        else image.GetMetaData('0018|1030').rstrip().upper()
    image_comments = '' if '0020|4000' not in all_keys \
        else image.GetMetaData('0020|4000').rstrip().upper()

    protocol_name = char2utf8(protocol_name)
    series_description = char2utf8(series_description)
    study_description = char2utf8(study_description)
    image_comments = char2utf8(image_comments)
    body_part_examined = char2utf8(body_part_examined)
    body_part_examined = get_body_part_examined(body_part_examined, protocol_name,
                                                series_description, study_description,
                                                image_comments)

    if study_uid == '' or instance_id == '' or modality == '' or series_id == '':
        LOGGER.error('key meta missing, pass file:{}'.format(file_path))
        os.remove(file_path)
        return None
    gender = char2utf8(gender).upper()
    if gender in ["M", "MALE", "男", "男性"]:
        gender = "MALE"
    else:
        gender = "FEMALE"
    birth_date = '' if '0010|0030' not in all_keys else image.GetMetaData('0010|0030').rstrip()
    hospital = '' if '0008|0080' not in all_keys else image.GetMetaData('0008|0080').rstrip()
    view_position = '' if '0018|5101' not in all_keys else image.GetMetaData('0018|5101').rstrip()
    lut = '' if '2050|0020' not in all_keys else image.GetMetaData('2050|0020').rstrip()
    patient_orientation = '' if '0020|0020' not in all_keys \
        else image.GetMetaData('0020|0020').rstrip()
    view_position = get_DX_PA_ViewPosition(view_position, patient_orientation, protocol_name,
                                           series_description, study_description)
    try:
        study_date = datetime.datetime.strptime(study_date + study_time.split('.')[0],
                                                "%Y%m%d%H%M%S")
    except ValueError:
        study_date = datetime.datetime.today()

    try:
        birth_date = datetime.datetime.strptime(birth_date, "%Y%m%d")
    except ValueError:
        birth_date = None

    patient_name = char2utf8(patient_name)
    hospital = char2utf8(hospital)

    return {
        'studyUID': study_uid,
        'seriesID': series_id,
        'instanceID': instance_id,
        'studyDate': study_date,
        'birthDate': birth_date,
        'uploadDate': datetime.datetime.today(),
        'patientID': patient_id,
        'patientName': patient_name,
        'modality': modality,
        'bodyPartExamined': body_part_examined,
        'gender': gender,
        'hospital': hospital,
        'LUT': lut,
        'viewPosition': view_position,
        'columnPixelSpacing': spacing[1],
        "rowPixelSpacing": spacing[0],
        "minPixelValue": 0,
        "maxPixelValue": 0,
        'seriesNumber': series_number,
        "width": size[0],
        "height": size[1]
    }


def get_longest_group(group):
    """
    :param group:
    :return:
    """
    target = {}
    for series_uid, mata in group.iteritems():
        tmp_length = 0
        tmp_list = None
        for _, images in mata.iteritems():
            if len(images) > tmp_length:
                tmp_length = len(images)
                tmp_list = images
        target[series_uid] = tmp_list
    return target


def group_by_series(images):
    """
    dicom group by series
    :param images:
    :return:
    """
    series_group = {}
    for image in images:
        width = image["width"]
        height = image["height"]
        size_key = "%s_%s" % (width, height)
        series_id = image['seriesID']
        if series_id not in series_group:
            series_group[series_id] = {}

        if size_key not in series_group[series_id]:
            series_group[series_id][size_key] = []
        series_group[series_id][size_key].append(image)

    group_data = get_longest_group(series_group)
    series = []
    min_pixel_value = 99999
    max_pixel_value = -99999
    for group in group_data.values():
        instance_id_list = []
        new_series = dict(seriesID=group[0]['seriesID'],
                          columnPixelSpacing=group[0]['columnPixelSpacing'],
                          rowPixelSpacing=group[0]['rowPixelSpacing'],
                          bodyPartExamined=group[0]["bodyPartExamined"],
                          instances=[])
        for image in group:
            if image['instanceID'] in instance_id_list:
                LOGGER.info('repeat image {} in seriesID {}'.format(image['filepath'],
                                                                    new_series['seriesID']))
                if os.path.exists(image['filepath']):
                    os.remove(image['filepath'])
                else:
                    LOGGER.info('file not exist {}'.format(image['filepath']))
            else:
                new_instance = dict(instanceID=image['instanceID'],
                                    url="",
                                    filepath=image['filepath'],
                                    filename=image['filename'])
                if image['modality'] in ['DR', 'DX', "CR"]:
                    new_instance['viewPosition'] = image['viewPosition']

                new_series['instances'].append(new_instance)
                instance_id_list.append(new_instance['instanceID'])
                min_pixel_value = image['minPixelValue'] if \
                    image['minPixelValue'] < min_pixel_value else min_pixel_value
                max_pixel_value = image['maxPixelValue'] if \
                    image['maxPixelValue'] > max_pixel_value else max_pixel_value
        new_series['maxPixelValue'] = int(max_pixel_value)
        new_series['minPixelValue'] = int(min_pixel_value)
        new_series['seriesNumber'] = group[0]['seriesNumber']
        series.append(new_series)
    return series


def group_by_study(images):
    group_data = {}
    series_group = {}
    for image in images:
        study_uid = image['studyUID']
        series_id = image["seriesID"]
        if study_uid in group_data:
            group_data[study_uid].append(image)
        else:
            group_data[study_uid] = [image]
        if series_id in series_group:
            series_group[series_id].append(image)
        else:
            series_group[series_id] = [image]
    studies = []
    series_group = series_group.values()
    series_group = sorted(series_group, key=lambda l: len(l), reverse=True)
    target_group = series_group[0]
    body_part_exam = target_group[0]['bodyPartExamined']
    for series_list in target_group:
        if series_list["bodyPartExamined"] in ["CHEST", "ABDOMEN"]:
            body_part_exam = series_list["bodyPartExamined"]
    for group in group_data.values():
        patient_name = group[0]['patientName']
        patient_info = get_patientInfo(group[0]['studyUID'])
        if patient_info is not None:
            patient_name = patient_info['patientName']
        new_study = dict(birthDate=target_group[0]['birthDate'],
                         studyDate=target_group[0]['studyDate'],
                         uploadDate=target_group[0]['uploadDate'],
                         bodyPartExamined=body_part_exam,
                         gender=target_group[0]['gender'],
                         hospital=target_group[0]['hospital'],
                         modality=target_group[0]['modality'],
                         patientID=target_group[0]['patientID'],
                         patientName=patient_name,
                         result="",
                         state="meta_only")
        new_study['birthDate'] = target_group[0]['birthDate']
        new_study['studyDate'] = target_group[0]['studyDate']
        new_study['uploadDate'] = target_group[0]['uploadDate']
        new_study['bodyPartExamined'] = body_part_exam
        new_study['gender'] = target_group[0]['gender']
        new_study['hospital'] = target_group[0]['hospital']
        new_study['modality'] = target_group[0]['modality']
        new_study['patientID'] = target_group[0]['patientID']
        new_study['patientName'] = patient_name
        new_study['result'] = ""
        new_study['state'] = "meta_only"
        new_study['studyUID'] = target_group[0]['studyUID']
        new_study['series'] = group_by_series(group)
        studies.append(new_study)

    return studies


def move_into_folders(studies):
    if not os.path.exists(DICOM_ROOT):
        os.makedirs(DICOM_ROOT)

    for study in studies:
        series = study['series']
        LOGGER.info("move_into_folders studyUID is {}".format(study['studyUID']))
        for s in series:
            LOGGER.info("seriesID {} series_number {} instances len {} "
                        "in studyUID {}".format(s['seriesID'], s['seriesNumber'],
                                                len(s['instances']), study['studyUID']))
            seriesID = s['seriesID']
            folder = os.path.join(DICOM_ROOT, seriesID)
            if os.path.exists(folder):
                shutil.rmtree(folder)
            os.makedirs(folder)
            for ins in s['instances']:
                src = ins['filepath']
                dest = os.path.join(folder, ins['filename'])
                shutil.move(src, dest)
                ins['filepath'] = dest
                ins["url"] = dest
                ins['filename'] = os.path.basename(dest)
    return studies


def process_study_image(study):
    modality = study['modality']
    generate_file = False

    if modality == 'DX' or modality == 'DR' or modality == 'CR':
        LOGGER.info('convert DX series')
        for series in study['series']:
            flag = insert_x_ray_instances_dicom(series, modality)
            if flag:
                generate_file = True
            else:
                study['series'].remove(series)

    elif modality == 'CT':
        study['series'] = sorted(study['series'], key=lambda l: len(l['instances']), reverse=True)
        LOGGER.info('convert longest CT series, '
                    'len {}, seriesID {} '.format(len(study['series'][0]['instances']),
                                                  study['series'][0]['seriesID']))
        reverse, flag = insert_dicom_CT_instances2(study['series'][0])
        LOGGER.info("convert CT flag {}".format(flag))
        study['reverse'] = reverse
        study["series"] = [study['series'][0]]
        generate_file = True if flag else False

    elif modality == 'MR':
        study['series'] = sorted(study['series'], key=lambda l: len(l['instances']), reverse=True)
        LOGGER.info('convert longest CT series, len {}, '
                    'seriesID {} '.format(len(study['series'][0]['instances']),
                                          study['series'][0]['seriesID']))
        reverse, flag = insert_CT_instances(study['series'][0])
        study['reverse'] = reverse
        generate_file = True if flag else False
    else:
        LOGGER.info('modality not supported %s' % modality)
        for series in study['series']:
            instances = series['instances']
            for ins in instances:
                ins.pop('filename')
                ins.pop('filepath')
        generate_file = False

    return study, generate_file


def on_file(filepaths):
    parsed_dicom_files = []
    for filepath in filepaths:
        meta_info = parse_meta(str(filepath))
        if meta_info is None:
            continue
        filename = os.path.basename(filepath)
        meta_info['filepath'] = filepath
        meta_info['filename'] = filename
        parsed_dicom_files.append(meta_info)
    if not parsed_dicom_files:
        LOGGER.error("dicom files is empty. filepath=%s", filepaths)
        return []
    studyInfo_list = group_by_study(parsed_dicom_files)
    studyInfo_list = move_into_folders(studyInfo_list)
    for study in studyInfo_list:
        study['state'] = 'meta_only'
    return studyInfo_list


if __name__ == "__main__":
    fileroot = '/home/imsight/data/storeimage/1.2.840.113704.1.111.5840.1533635850.1'
    filepaths = []
    for fle in os.listdir(fileroot):
        if fle.endswith(".dcm"):
            filepaths.append(os.path.join(fileroot, fle))

    studyInfo_list = on_file(filepaths)
    print studyInfo_list
