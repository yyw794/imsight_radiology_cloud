import React, { Component } from 'react';
import { Card, Select, Icon } from 'antd';
import {CHEST_DISEASES_EN, CHEST_DISEASES_ZN} from '../../data'
const Option = Select.Option;

class LabelPopover extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Card
        style={{
          position: 'fixed', top: this.props.top, left: this.props.left,
          display: this.props.show ? 'block' : 'none'
        }}>
        <p>病灶名称
          <Icon type="close-circle-o"
            style={{float: 'right'}}
            onClick={this.props.onClose}/>
        </p>
        <Select
          value={this.props.value}
          style={{ width: 120 }}
          onChange={(value) => {
            this.props.onChange(this.props.id, value)
          }}
          >
          {
            Object.keys(CHEST_DISEASES_EN).map((name, i) => <Option value={name} key={i}>{CHEST_DISEASES_EN[name]}</Option>)
          }
        </Select>
      </Card>
    );
  }

}

export default LabelPopover;
