import React, { Component } from 'react'
import { DatePicker } from 'antd';
import { injectIntl } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'


import './index.less'

class DateRange extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      startValue: props.value ? props.value[0] : null,
      endValue: props.value ? props.value[1] :null,
      endOpen: false,
    };

  }
  
  componentWillReceiveProps(nextProps){
    this.setState({
      startValue: nextProps.value ? nextProps.value[0] : null,
      endValue: nextProps.value ? nextProps.value[1] :null,
      endOpen: false
    })
  }

  disabledStartDate = (startValue) => {
    const endValue = this.state.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.isAfter(endValue,"day");
  }

  disabledEndDate = (endValue) => {
    const startValue = this.state.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.isBefore(startValue,"day");
  }

  onChange = (field, value) => {
    this.setState({
      [field]: value,
    }, () => {
      this.props.onChange([this.state.startValue, this.state.endValue])
    });
  }

  onStartChange = (value) => {
    this.onChange('startValue', value);
  }

  onEndChange = (value) => {
    this.onChange('endValue', value);
  }

  handleStartOpenChange = (open) => {
    if (!open && !this.state.endValue) {
      this.setState({ endOpen: true });
    }
  }

  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  }

  render() {
    const { startValue, endValue, endOpen } = this.state;
    return (
      <div className='date-range-picker'>
        <DatePicker
          disabledDate={this.disabledStartDate}
          showTime={false}
          format="YYYY-MM-DD"
          value={startValue}
          placeholder={GetLocaledText(this, "DateRange.start")}
          onChange={this.onStartChange}
          onOpenChange={this.handleStartOpenChange}
        />
        <DatePicker
          disabledDate={this.disabledEndDate}
          showTime={false}
          format="YYYY-MM-DD"
          value={endValue}
          placeholder={GetLocaledText(this, "DateRange.end")}
          onChange={this.onEndChange}
          open={endOpen}
          onOpenChange={this.handleEndOpenChange}
        />
      </div>
    );
  }
}

export default injectIntl(DateRange)
