import React, { Component } from 'react'
import {withRouter} from 'react-router'
import {Link} from 'react-router-dom'
import './index.less'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
const style = {
  'cursor': 'pointer',
  'WebkitUserSelect': 'none',
  'MozUserSelect': 'none',
  'msUserSelect': 'none',
  'userSelect': 'none',
}
const goBack=()=>{
    window.history.back()
}
const UpStep = withRouter(
  ({ history }) => (
      <a style={{float:"left",color:"#fff",height:"30px"}} onClick={goBack}>
        <img style={style} className='logo-image'style={{
            cursor:"pointer",
            userSelect:"none",
            width:"30px",
            verticalAlign:"middle",
            marginRight:"10px",
            marginLeft:"20px"}} src={require('../../../static/images/up.png')} /*onClick={() => history.push('/')}*//>
          <span><FormattedMessage id='UpStep.button.name'/></span>
      </a>
   )
)

export default UpStep
