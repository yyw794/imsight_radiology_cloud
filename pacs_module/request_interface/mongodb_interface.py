# -*- coding: UTF-8 -*-
'''
study_info_list
study_info_list_state
system_info
'''

from pymongo import MongoClient
import logging, logger
log = logging.getLogger("PACSModule")

client = MongoClient('mongodb', username='work', password='work@imsightmed',authSource='radiology')
db = client['radiology']


def db_find_update_otherwise_insert(collection_name, query_value, update_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    document = collection.find_one(query_value)
    if document:
        collection.update_one(query_value, {'$set': update_value})
    else:
        collection.insert_one(update_value)


def db_find(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    cursors = collection.find(query_value, {'_id': False})

    return list(cursors)


def db_find_specific(collection_name, query_value, specific_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    cursors = collection.find(query_value, specific_value)

    return list(cursors)


def db_insert_one(collection_name, insert_value):
    if collection_name not in db.collection_names():
        return False

    collection = db[collection_name]
    collection.insert_one(insert_value)


def db_find_one(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    cursors = collection.find_one(query_value, {'_id': False})
    return cursors


def db_delete_one(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    collection.delete_one(query_value)
    return True


def db_delete_many(collection_name, query_value):
    if collection_name not in db.collection_names():
        return None

    collection = db[collection_name]
    collection.delete_many(query_value)
    return True