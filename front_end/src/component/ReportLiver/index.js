import React, { Component } from 'react';
import { Row, Col, Input, Icon, Radio, Footer } from 'antd'

/**
 * antd-design组件
 * @type {TextArea}
 */
const TextArea = Input.TextArea
import {getUser} from '../../auth'

import { calcLiradsScore } from '../../util'

import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './index.less'

/**
 * antd-design单选组件
 * @type {RadioGroup}
 */
const RadioGroup = Radio.Group;

/**
 * 肝节点报告组件
 */
class Report extends Component {
    /**
     * 参照生命周期函数
     * @param props
     */
  constructor(props) {
    super(props)
        /**
         *医学报告数据
         * @private {{meetsLiradsValue: boolean}}
         */
    this.state = {
      meetsLiradsValue: this.props.report.meetsLirads
    }


    this.onMeetsLiradsChange = this.onMeetsLiradsChange.bind(this)
        /**
         * 单选框样式
         * @type {{marginLeft: string}}
         */
    this.radioStyle = {
      marginLeft: '10px'
    };
  }


    /**
     * 获取报告页面数据
     * @returns {Array}
     */
  getPageData() {
    let pages = []
    let urls = this.props.report.urls
    let observations = this.props.report.observations

      let pageIdx = 0
      pages.push({
        pageIdx: pageIdx,
        sections: [{name: 'Info'}, {name: 'Procedure'}, {name: 'Indication'}, {name: 'Comparison'}, {name: 'Technique'}, {name: 'MeetsLirads'}, {name: 'Findings'}]
      })
      ++pageIdx

      Object.keys(observations).map(function(key, index) {
        if (pageIdx == 1) {
           pages.push({
            pageIdx: pageIdx,
            sections: [{name: 'FocalHepaticHeader'}, {name: 'Observation', urls: urls.slice(0, 9), observation: observations[key], offset: 2, idx: index+1}, {name: 'ObservationDetails', labelID: key}]
          })
        } else {
          pages.push({
            pageIdx: pageIdx,
            sections: [{name: 'Observation', urls: urls.slice(0, 9), observation: observations[key], offset: 2, idx: index+1}, {name: 'ObservationDetails', labelID: key}]
          })
        }

        ++pageIdx
        return pages;
      });


      pages.push({
        pageIdx: pageIdx,
        sections: [{name: 'HepaticVasculature'}, {name: 'BiliarySystem'}, {name: 'ExtrahepaticFindings'}, {name: 'Impression'}]
      })
    pages.forEach(page => {
      page.total = pages.length
    })
    return pages
  }

    /**
     *
     * @returns {string}
     */
  renderStamp() {
    if (this.props.report.submitted && !this.props.report.examined && getUser().usergroup === 'reporter') {
      return <img className='report-status-stamp' src={require('../../../static/images/submit-stamp.png')}/>
    } else if (this.props.report.examined) {
      return <img className='report-status-stamp' src={require('../../../static/images/exam-stamp.png')}/>
    }
    return ''
  }

    /**
     *
     * @returns {boolean}
     */
  renderFindingsSection() {
    return (
      <div className='report-findings-section report-page-section' key='Findings-Section'>
        <h3>5. {GetLocaledText(this, "ReportLiver.text.findings")}</h3>
        <h3>5.1 {GetLocaledText(this, "ReportLiver.text.liver-gross-observations")}</h3>
        <TextArea
          autosize={ {minRows: 3, maxRows: 3} }
          rows={3}
          value={this.props.report.liverGrossObs}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('liverGrossObs', e.target.value)}}/>
      </div>
    )
  }

  renderFocalHepaticHeaderSection() {
    return (
      <div className='report-focal-hepatic-header-section report-page-section' key='Focal-Hepatic-Header-Section'>
       <h3>5.2 {GetLocaledText(this, "ReportLiver.text.focal-hepatic-observations")}</h3>
      </div>
    )
  }

  renderImageSection(section) {
    return (
      <div className='report-image-section report-page-section' key='ReportLiver-Image-Section'>
        {section.urls.map((url, i) => (
          <div className='report-image-wrapper' key={i}>
            {
              !this.props.isForbidEdit ? <Icon type="close" onClick={() => this.props.removeImage(section.offset + i)}/> : ''
            }
            <img src={url} onClick={() => this.showRawImage(url)}/>
          </div>
        ))}
      </div>
    )
  }

  renderObservationSection(section) {
    return (
      <div className='report-observation-section report-page-section' key='Observation-Section'>
        <h3>5.2.{section.idx} {GetLocaledText(this, "ReportLiver.text.observation")} {section.idx}</h3>
        {section.observation.map((inst, i) => (
          <div className='report-image-wrapper' key={i}>
            {
              !this.props.isForbidEdit ? <Icon type="close" onClick={() => this.props.removeImage(section.offset + i)}/> : ''
            }
            <div>
              <img src={inst[1]} onClick={() => this.showRawImage(inst[1])}/>
              <div className="img-text" style={{position: 'absolute', bottom: '0', backgroundColor: 'white', width: '100%'}}>
                <p>{GetLocaledText(this, "ReportLiver.text.series-number")}{inst[0]}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    )
  }

  renderObservationDetailsSection(section) {
    const label = this.props.labels.find(inst => inst.labelID == section.labelID)
    const isAPE = label.isAPE
    const diam = label.diam
    const vol = label.vol

    let majorFeatures = 0
    let mfArr = []
    if (label.mfsWashout) {++majorFeatures; mfArr.push(GetLocaledText(this, "ReportLiver.text.observation.details.washout(not-peripheral)"))}
    if (label.mfsEnhancing) {++majorFeatures; mfArr.push(GetLocaledText(this, "ReportLiver.text.observation.details.enhancing-capsule"))}
    if (label.mfsThresholdGrowth) {++majorFeatures; mfArr.push(GetLocaledText(this, "ReportLiver.text.observation.details.threshold-growth"))}
    const mfArrToRender = (<span>{mfArr.toString()}</span>)

    const lrScor = calcLiradsScore(isAPE, majorFeatures, diam)
    const liradsScoreSpan =
    (
      <span
        style={{
          backgroundColor: lrScor == 'LR-5' ? 'red' : lrScor == 'LR-4' ? 'orange' : 'yellow'
          }}>
          {lrScor}
      </span>
    )

    return (
      <div className='report-observation-details-section report-page-section' key='Observation-Details-Section'>
        <Row>
          <Col span={4}>
            <span><FormattedMessage id='ReportLiver.text.lirads-score'/></span>
          </Col>
          <Col span={20}>
            {liradsScoreSpan}
            <span>{}</span>
          </Col>
        </Row>
        <Row>
          <Col span={4}>
            <span><FormattedMessage id='ReportLiver.text.diameter'/></span>
          </Col>
          <Col span={20}>
            {diam.toFixed(1).toString() + ' '}<span><FormattedMessage id='ReportLiver.text.diameter.unit'/></span>
          </Col>
        </Row>
        <Row>
          <Col span={4}>
            <span><FormattedMessage id='ReportLiver.text.observation.volume'/></span>
          </Col>
          <Col span={20}>
            {vol.toFixed(1).toString() + ' '}<span><FormattedMessage id='ReportLiver.text.observation.volume.unit'/><sup>3</sup></span>
          </Col>
        </Row>
        <Row>
          <Col span={4}>
            <span><FormattedMessage id='ReportLiver.text.hepatic-segment'/></span>
          </Col>
          <Col span={1}>
            <Input size="small"/>
          </Col>
        </Row>
        <Row>
          <Col span={4}>
            <span><FormattedMessage id='ReportLiver.text.major-features'/></span>
          </Col>
          <Col span={20}>
            {mfArrToRender}
          </Col>
        </Row>
        <Row>
          <Col span={4}>
            <span><FormattedMessage id='ReportLiver.text.vascular-involvement'/></span>
          </Col>
          <Col span={20}>
            <Input size="small"/>
          </Col>
        </Row>
      </div>
    )
  }

  renderInfoSection() {
    return (
      <div className='report-info-section report-page-section' key='ReportLiver-Info-Section'>
        <Row>
          <Col span={6}>{GetLocaledText(this, "ReportLiver.patientName")}：{this.props.report.patientName}</Col>
          <Col span={6}>{GetLocaledText(this, "ReportLiver.gender")}：{this.props.report.gender}</Col>
          <Col span={6}>{GetLocaledText(this, "ReportLiver.dob")} : {this.props.report.dob}</Col>
          <Col span={6}>{GetLocaledText(this, "ReportLiver.age")}：{this.props.report.age}</Col>
        </Row>
        <Row>
          <Col span={6}>{GetLocaledText(this, "ReportLiver.patientID")}：{this.props.report.patientID}</Col>
          <Col span={6}>
          {GetLocaledText(this, "ReportLiver.study-item")}：
            <Input
              size="small"
              value={this.props.report.modality}
              style={{display: 'inline-block', width: 100, marginBottom: '5px'}}
              disabled={this.props.isForbidEdit}
              onChange={e => {this.props.onChange('modality', e.target.value)}}/>
          </Col>
          <Col span={6}>{GetLocaledText(this, "ReportLiver.study-time")}：{this.props.report.studyDate}</Col>
        </Row>
      </div>
    )
  }

  renderProcedureSection() {
    return (
      <div className='report-procedure-section report-page-section' key='Procedure-Section'>
        <h3>1. {GetLocaledText(this, "ReportLiver.text.procedure")}</h3>
        <TextArea
          autosize={ {minRows: 3, maxRows: 3} } // temp rows size fixed
          rows={3}
          value={this.props.report.procedure}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('procedure', e.target.value)}}/>
      </div>
    )
  }

  renderHepaticVasculatureSection() {
    return (
      <div className='report-hepatic-vasculature-section report-page-section' key='Hepatic-Vasculature-Section'>
        <h3>5.3 {GetLocaledText(this, "ReportLiver.text.hepatic-vasculature")}</h3>
        <TextArea
          autosize={ {minRows: 5, maxRows: 5} } // temp rows size fixed
          rows={5}
          value={this.props.report.hepaticVasc}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('hepaticVasc', e.target.value)}}/>
      </div>
    )
  }

  renderBiliarySystemSection() {
    return (
      <div className='report-biliary-system-section report-page-section' key='Biliary-System-Section'>
        <h3>5.4 {GetLocaledText(this, "ReportLiver.text.biliary-system")}</h3>
        <TextArea
          autosize={ {minRows: 5, maxRows: 5} } // temp rows size fixed
          rows={5}
          value={this.props.report.biliarySys}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('biliarySys', e.target.value)}}/>
      </div>
    )
  }

  renderExtrahepaticFindingsSection() {
    return (
      <div className='report-extrahepatic-findings-section report-page-section' key='Extrahepatic-Findings-Section'>
        <h3>5.5 {GetLocaledText(this, "ReportLiver.text.extrahepatic-findings")}</h3>
        <TextArea
          autosize={ {minRows: 5, maxRows: 5} } // temp rows size fixed
          rows={5}
          value={this.props.report.extrahepaticFindings}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('extrahepaticFindings', e.target.value)}}/>
      </div>
    )
  }

  renderImpressionSection() {
    return (
      <div className='report-impression-section report-page-section' key='Impression-Section'>
        <h3>5.6 {GetLocaledText(this, "ReportLiver.text.impression")}</h3>
        <h3>5.6.1 {GetLocaledText(this, "ReportLiver.text.impression.hepatic-findings")}</h3>
        <TextArea
          autosize={ {minRows: 3, maxRows: 3} } // temp rows size fixed
          rows={3}
          value={this.props.report.hepaticFindings}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('hepaticFindings', e.target.value)}}/>
        <h3>5.6.2 {GetLocaledText(this, "ReportLiver.text.impression.extrahepatic-findings")}</h3>
        <TextArea
          autosize={ {minRows: 3, maxRows: 3} } // temp rows size fixed
          rows={3}
          value={this.props.report.extrahepaticFindingsImpr}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('extrahepaticFindingsImpr', e.target.value)}}/>
      </div>
    )
  }

  renderIndicationSection() {
    return (
      <div className='report-indication-section report-page-section' key='Indication-Section'>
        <h3>2. {GetLocaledText(this, "ReportLiver.text.indication")}</h3>
        <TextArea
          autosize={ {minRows: 3, maxRows: 3} } // temp rows size fixed
          rows={3}
          value={this.props.report.indication}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('indication', e.target.value)}}/>
      </div>
    )
  }

  renderComparisonSection() {
    return (
      <div className='report-comparison-section report-page-section' key='Comparison-Section'>
        <h3>3. {GetLocaledText(this, "ReportLiver.text.comparison")}</h3>
        <TextArea
          autosize={ {minRows: 3, maxRows: 3} } // temp rows size fixed
          rows={3}
          value={this.props.report.comparison}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('comparison', e.target.value)}}/>
      </div>
    )
  }

  renderTechniqueSection() {
    return (
      <div className='report-technique-section report-page-section' key='Technique-Section'>
        <h3>4. {GetLocaledText(this, "ReportLiver.text.technique")}</h3>
        <TextArea
          autosize={ {minRows: 3, maxRows: 3} } // temp rows size fixed
          rows={3}
          value={this.props.report.technique}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('technique', e.target.value)}}/>
      </div>
    )
  }

  onMeetsLiradsChange() {
    this.setState({
      meetsLiradsValue: !this.state.meetsLiradsValue
    })
  }

  renderMeetsLiradsSection() {
    return (
      <div className='report-meets-lirads-section report-page-section' key='Meets-Lirads-Section'>
        <h3>
          {GetLocaledText(this, "ReportLiver.text.exam-meet-lirads")}
          <RadioGroup onChange={this.onMeetsLiradsChange} value={this.state.meetsLiradsValue}>
            <Radio style={this.radioStyle} value={true}><FormattedMessage id='ReportLiver.text.exam-meet-lirads.yes'/></Radio>
            <Radio style={this.radioStyle} value={false}><FormattedMessage id='ReportLiver.text.exam-meet-lirads.no'/></Radio>
          </RadioGroup>
        </h3>
        <div className='meets-lirads-wrapper'
          style={this.state.meetsLiradsValue ? {visibility: 'hidden'} : {visibility: 'visible'}}
        >
          <Row type="flex" justify="space-around" align="middle">
            <Col span={10}>
              <span><FormattedMessage id='ReportLiver.text.is-compromised'/></span>
            </Col>
            <Col span={14}>
              <Input
                size="small"
                placeholder={GetLocaledText(this, "ReportLiver.text.is-compromised.display-msg")}
                value={this.props.report.isCompromisedBy}
                onChange={e => {this.props.onChange('isCompromisedBy', e.target.value)}}
              />
            </Col>
          </Row>
        </div>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={10}>
              <span><FormattedMessage id='ReportLiver.text.intravenous-contrast-agent'/></span>
            </Col>
            <Col span={14}>
              <Input
                size="small"
                placeholder={GetLocaledText(this, "ReportLiver.text.intravenous-contrast-agent.display-msg")}
                value={this.props.report.intrContrAgent}
                onChange={e => {this.props.onChange('intrContrAgent', e.target.value)}}
              />
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={10}>
              <span><FormattedMessage id='ReportLiver.text.agent.volume'/></span>
            </Col>
            <Col span={14}>
              <Row>
                <Col span={4}>
                  <Input
                    size="small"
                    placeholder=""
                    value={this.props.report.volContrAgent}
                    onChange={e => {this.props.onChange('volContrAgent', e.target.value)}}
                  />
                </Col>
                <Col span={20}>
                  <span style={{marginLeft: '5px'}}><FormattedMessage id='ReportLiver.text.agent.volume.unit'/></span>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={10}>
              <span><FormattedMessage id='ReportLiver.text.rate'/></span>
            </Col>
            <Col span={14}>
              <Row >
                <Col span={4}>
                  <Input
                    size="small"
                    placeholder=""
                    value={this.props.report.rateContrAgent}
                    onChange={e => {this.props.onChange('rateContrAgent', e.target.value)}}
                  />
                </Col>
                <Col span={20}>
                  <span style={{marginLeft: '5px'}}><FormattedMessage id='ReportLiver.text.rate.unit'/></span>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row type="flex" justify="space-around" align="middle">
            <Col span={10}>
              <span><FormattedMessage id='ReportLiver.text.premedication'/></span>
            </Col>
            <Col span={14}>
              <Input
                size="small"
                placeholder={GetLocaledText(this, "ReportLiver.text.premedication.display-msg")}
                value={this.props.report.premedAdvEvents}
                onChange={e => {this.props.onChange('premedAdvEvents', e.target.value)}}
              />
            </Col>
          </Row>
      </div>

    )
  }

  renderImageFindSection() {
    return (
      <div className='report-diagnosis-section report-page-section' key='ReportLiver-Diagnosis-Section-ImageFind'>
        <h3>Lirads Score: {GetLocaledText(this, "ReportLiver.text.")}{this.props.liradsScore}</h3>
        <p><FormattedMessage id='ReportLiver.diagnosis.image-find'/></p>
        <TextArea
          autosize={ {minRows: 6, maxRows: 6} } // temp rows size fixed
          rows={6}
          value={this.props.report.imagingFind}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('imagingFind', e.target.value)}}/>
      </div>
    )
  }

  renderDiagnosisAdviceSection() {
    return (
      <div className='report-diagnosis-section report-page-section' key='ReportLiver-Diagnosis-Section-Advice'>
        <p><FormattedMessage id='ReportLiver.diagnosis.advice'/></p>
        <TextArea
          autosize={ {minRows: 2, maxRows: 2} } // temp rows size fixed
          rows={2}
          value={this.props.report.diagnosisAdvice}
          disabled={this.props.isForbidEdit}
          onChange={e => {this.props.onChange('diagnosisAdvice', e.target.value)}}/>
      </div>
    )
  }

  renderPage = (page) => {
    const {sections, total, pageIdx} = page
    let sectionsElems = []
    if (pageIdx === 0) {
      sectionsElems.push(
        <div className='report-header' key='ReportLiver-Header'>
          <h2>
            <img src={require('../../../static/images/logo.png')}/>
            {GetLocaledText(this, "ReportLiver.ImsightMed.Title")}
          </h2>
          {/*<p>{SUB_TITLE[this.props.report.modality]}</p>*/}
          <p><FormattedMessage id='ReportLiver.liver-diagnosis-report'/></p>
        </div>
      )
    }
    sectionsElems = sectionsElems.concat(sections.map(sect => {
      return this[`render${sect.name}Section`](sect)
    }))
    sectionsElems.push(
      <div className='report-footer' key='ReportLiver-Footer'>
        <Row>
          <Col span={8}>{GetLocaledText(this, "ReportLiver.submit-doctor")}: {this.props.report.submitted ? this.props.report.userID : ''}</Col>
          <Col span={8}>{GetLocaledText(this, "ReportLiver.examine-doctor")}: {this.props.report.examined ? this.props.report.examiner : ''}</Col>
          <Col span={8}>{GetLocaledText(this, "ReportLiver.report-date")}: {this.props.report.updateDate}</Col>
        </Row>
        <Row>
          <Col span={8}><FormattedMessage id='ReportLiver.just-for-clinicians-ref-only'/></Col>
          <Col span={8}><FormattedMessage id='ReportLiver.ImsightMed.fullName-with-url'/></Col>
          <Col span={8}>{GetLocaledText(this, "ReportLiver.page-no")}{pageIdx + 1}/{total}</Col>
        </Row>
      </div>
    )
    return (
      <div className='report' key={pageIdx}>
        {pageIdx === 0 ? this.renderStamp() : ''}
        {sectionsElems}
      </div>
    )
  }

  render() {
    return (
      <div className={`report-wrapper-liver ${this.props.report.modality}`}>
        {this.getPageData().map(this.renderPage)}
      </div>
    );
  }

}

export default injectIntl(Report);
