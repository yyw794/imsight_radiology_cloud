import React, { Component } from 'react';
import { Row, Col, Input, Icon, TreeSelect, List, Form, Button } from 'antd';
const { TextArea, Search } = Input;
const TreeNode = TreeSelect.TreeNode;
const FormItem = Form.Item

import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'

import './template.less'

class Template extends Component {
  // state = {
  //   keyword: '',
  //   category: ''
  // }

  constructor(props) {
    super(props)
    this.state = {
      keyword: '',
      category: ''
    }

  }

  onSearch = (keyword) => {
    this.setState({
      keyword,
      category: ''
    })
    this.props.onSearch(keyword)
  }

  onCategoryChange = (category) => {
    this.setState({
      keyword: '',
      category
    })
    this.props.onCategoryChange(category)
  }

  render() {
    return (
      <div className='tempalte-wrapper'>
        <Form>
          <FormItem>
            <Search
              value={this.state.keyword}
              placeholder={GetLocaledText(this, "ReportPageLung-Template.SearchInput")}
              onSearch={this.onSearch}
              onChange={e => this.setState({keyword: e.target.value})}
              style={{ width: '100%' }}
            />
          </FormItem>
          <FormItem>
            <TreeSelect
              showSearch
              value={this.state.category}
              style={{ width: '100%' }}
              dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
              placeholder={GetLocaledText(this, "ReportPageLung-Template.choose-category")}
              onChange={this.onCategoryChange}
              treeDefaultExpandAll>
              <TreeNode value="SYSTEM" title={GetLocaledText(this, "ReportPageLung-Template.system")} key="0">
                <TreeNode value="SYSTEM/CT" title={GetLocaledText(this, "ReportPageLung-Template.system-CT")} key="0-1">
                  <TreeNode value="SYSTEM/CT/CHEST" title={GetLocaledText(this, "ReportPageLung-Template.system-CT-chest")} key="0-1-1" />
                </TreeNode>
                <TreeNode value="SYSTEM/DR" title={GetLocaledText(this, "ReportPageLung-Template.system-DR")} key="0-2">
                  <TreeNode value="SYSTEM/DR/CHEST" title={GetLocaledText(this, "ReportPageLung-Template.system-DR-chest")} key="0-2-1" />
                </TreeNode>
              </TreeNode>
              <TreeNode value="USER" title={GetLocaledText(this, "ReportPageLung-Template.custom")} key="1"></TreeNode>
            </TreeSelect>
          </FormItem>
        </Form>
        <div className='template-list-container'>
          <Row className='template-list-header'>
            <Col span={12}><FormattedMessage id='ReportPageLung-Template.name'/></Col>
            <Col span={12}><FormattedMessage id='ReportPageLung-Template.ID'/></Col>
          </Row>
          <div className='template-list-content'>
            <List
              itemLayout="horizontal"
              dataSource={this.props.list}
              size='small'
              renderItem={item => (
                <List.Item className={this.props.template === item ? 'selected' : ''}>
                  <Row style={{width: '100%'}}
                    onClick={() => this.props.onSelected(item)}>
                    <Col span={12}>{item.templateName}</Col>
                    <Col span={12}>{item.templateID}</Col>
                  </Row>
                </List.Item>
              )}
            />
          </div>
        </div>
        <div id='template-edit-box'>
          <Form>
            <FormItem label={GetLocaledText(this, "ReportPageLung-Template.Item")}>
              <Input onChange={(e) => {
                  this.props.onTemplateChange('modality', e.target.value)
                }}
                value={this.props.template.modality || ''}
              />
            </FormItem>
            <FormItem label={GetLocaledText(this, "ReportPageLung-Template.ImageFind")}>
              <TextArea rows={4}
                onChange={(e) => {
                  this.props.onTemplateChange('imagingFind', e.target.value)
                }}
                value={this.props.template.imagingFind || ''}
              />
            </FormItem>
            <FormItem className='zhenduan' label={GetLocaledText(this, "ReportPageLung-Template.advice")}>
              <TextArea rows={2}
                onChange={(e) => {
                  this.props.onTemplateChange('diagnosisAdvice', e.target.value)
                }}
                value={this.props.template.diagnosisAdvice || ''}
              />
            </FormItem>
            <div className='button-group'>
              <Button type="primary" onClick={this.props.onSaveAs}><FormattedMessage id='ReportPageLung-Template.save-as'/></Button>
              {
                this.props.showSaveBtn ? <Button type="primary" onClick={this.props.onSave}><FormattedMessage id='ReportPageLung-Template.save'/></Button> : ''
              }
              <Button type="primary" onClick={this.props.onApply}><FormattedMessage id='ReportPageLung-Template.apply'/></Button>
            </div>
          </Form>
        </div>
      </div>
    );
  }

}

export default injectIntl(Template);
