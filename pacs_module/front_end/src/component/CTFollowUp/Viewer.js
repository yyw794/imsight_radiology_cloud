import React, { Component } from 'react'
import * as cornerstone from 'cornerstone-core'
import * as cornerstoneWebImageLoader from "cornerstone-web-image-loader"
import * as cornerstoneTools from "cornerstone-tools"
import * as cornerstoneMath from "cornerstone-math"
import Hammer from "hammerjs"
import {Icon, message, Button, Tabs} from 'antd'
const TabPane = Tabs.TabPane
import Immutable from 'immutable'
import _ from 'underscore'
import { DropTarget } from 'react-dnd';

import noduleBox from '../tools/noduleBox'
import referenceLine2D from '../tools/referenceLine2D'
import {zoom} from '../tools/zoom'
import scaleTool from '../tools/scaleTool'
import scrollToIndex from '../tools/scrollToIndex'
import stackPreloader from '../tools/stackPreloader'
import {scaleWWDynamic, scaleWCDynamic, scaleWWReverseDynamic, scaleWCReverseDynamic} from '../../util'
import itemTypes from '../dragDropItemTypes'
import {GetLocaledText} from '../../localedFuncs'
import {FormattedMessage,injectIntl} from 'react-intl'

const viewerTarget = {
  drop(props, monitor, component) {
    const study = monitor.getItem()
    props.onChangeStudy(study)
    component.replaceStackData(study.stacks.z)
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

class Viewer extends Component {

  constructor(props) {
    super(props)
    this.state = {
      position: {x: 0, y: 0, z: 0},
      viewport: null,
      HU: 0,
      minPixelValue:0,
      maxPixelValue:0
    }
  }

  componentDidMount() {
    let seriesID = 0
    for(var s in this.props.study.series)
    {
      seriesID = s
    }
    this.setState({
      minPixelValue:this.props.study.series[seriesID].minPixelValue,
      maxPixelValue:this.props.study.series[seriesID].maxPixelValue
    })
    this.initStack()
    .then(() => {
      // stackPreloader.enable(this.elem)
      this.initNoduleBoxTool()
      scaleTool.enable(this.elem)
      this.setWWWC(1200, -600)
    })
    window.addEventListener("resize", _.debounce(this.onResize, 500))
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.onResize)
  }

  getElement() {
    return this.elem
  }

  fromRatioToIdx(axis, ratio) {
    let imgNum = this.props.study.stacks[axis].imageIds.length
    let idx = Math.min(Math.ceil(imgNum * ratio) - 1, imgNum - 1)
    return Math.max(idx, 0)
  }

  setPosition(pos) {
    this.setState({
      position: pos
    })
    return scrollToIndex(this.elem, pos.z).then(() => cornerstone.updateImage(this.elem))
    /*
    this.setState({
      position: pos
    }, () => {
      scrollToIndex(this.elem, pos.z).then(() => cornerstone.updateImage(this.elem))
    })
    */
    // return scrollToIndex(this[`element${idx}`], pos.z).then(() => cornerstone.updateImage(this[`element${idx}`]))
  }

  initNoduleBoxTool = () => {
    const stack = this.props.study.stacks
    const imgNumX = stack.x.imageIds.length
    const imgNumY = stack.y.imageIds.length
    const imgNumZ = stack.z.imageIds.length
    const element = this.elem
    noduleBox.enable(element, {
      axis: 'z',
      depth: 2, // in layers
      horizontalLayers: imgNumX,
      verticalLayers: imgNumY,
      mainAxisLayers: imgNumZ,
      getData: () => this.props.labels,
      getCurrentIdx: () => {
        return this.state.position
      },
      onClick: (e) => {
        let ratio = e.currentPoints.ratio
        let clickedNodules = e.clickedNodules
        let pixel = e.grayScale
        let coordImage = e.currentPoints.image
        if (clickedNodules.length > 0) {
          let nodule = clickedNodules[0]
          this.props.onNoduleClick(nodule)
        }
        let pos = Object.assign({}, this.state.position)
        pos.x = this.fromRatioToIdx('x', ratio.x)
        pos.y = this.fromRatioToIdx('y', ratio.y)
        this.setPosition(pos)
        referenceLine2D.setConfiguration({intersection: coordImage})
        this.setState({
          HU: Math.round(scaleWCDynamic(pixel,this.state.minPixelValue,this.state.maxPixelValue))
        })
      }
    })
    noduleBox.activate(element, 1)
  }

  initStack() {
    const {study} = this.props
    let element = this.elem
    const zStack = study.stacks.z
    cornerstone.enable(element)
    // Enable mouse inputs
    cornerstoneTools.mouseInput.enable(element)
    cornerstoneTools.mouseWheelInput.enable(element)

    return cornerstone.loadImage(zStack.imageIds[0])
      .then((image) => {
        cornerstone.displayImage(element, image)
        // set the stack as tool state
        // cornerstoneTools.addStackStateManager(element, ['stack', 'referenceLines'])
        cornerstoneTools.addStackStateManager(element, ['stack'])
        cornerstoneTools.addToolState(element, 'stack', zStack)
        // Enable all tools we want to use with this element
        cornerstoneTools.pan.activate(element, 1)
        zoom.activate(element, 4)
        cornerstoneTools.stackScrollWheel.activate(element)

        element.addEventListener("cornerstonenewimage", (e) => {
          const stackData = cornerstoneTools.getToolState(element, 'stack')
          const z = stackData.data[0].currentImageIdIndex
          this.setState({
            position: Object.assign({}, this.state.position, {z}),
          })
          cornerstoneTools.addToolState(element, 'zoom', {baseScale: this.baseScale})
        })

        // baseScale
        let baseScale = cornerstone.getViewport(element).scale
        cornerstoneTools.addToolState(element, 'zoom', {baseScale})
        this.baseScale = baseScale
        element.addEventListener('cornerstoneimagerendered', () => {
          this.setState({
            viewport: cornerstone.getViewport(element)
          })
        })

        // synchronizer.add(element)
      })
  }

  replaceStackData(stackData) {
    const element = this.elem
    return cornerstone.loadImage(stackData.imageIds[0])
      .then((image) => {
        cornerstone.displayImage(element, image)
        cornerstoneTools.clearToolState(element, 'stack')
        cornerstoneTools.addToolState(element, 'stack', stackData)
        cornerstone.reset(element)
        let viewport = cornerstone.getViewport(element)
        let baseScale = viewport.scale
        this.baseScale = baseScale
        cornerstoneTools.removeToolState(element, 'zoom')
        cornerstoneTools.addToolState(element, 'zoom', {baseScale})
        viewport.voi.windowWidth = 80
        viewport.voi.windowCenter = 28
        cornerstone.updateImage(element)
      })
  }

  onResize = () => {
    if (this.props.hide) {return}
    cornerstone.resize(this.elem, true)
  }

  setWWWC(windowWidth, windowCenter) {
    let ww = scaleWWReverseDynamic(windowWidth,this.state.minPixelValue,this.state.maxPixelValue)
    let wc = scaleWCReverseDynamic(windowCenter,this.state.minPixelValue,this.state.maxPixelValue)
    let elem = this.elem
    let viewport = cornerstone.getViewport(elem)
    viewport.voi.windowWidth = ww
    viewport.voi.windowCenter = wc
    cornerstone.updateImage(elem)
  }

  render() {
    const {study, title, fullScreen, hide} = this.props
    const { connectDropTarget, isOver, active } = this.props;
    let className = `view ${this.props.className}`
    if (fullScreen) {className += ' full-screen'}
    if (hide) {className += ' hide'}
    if (isOver || active) {className += ' active'}
    return connectDropTarget(
      <div className={className}>
        <p className='view-title'>{title}</p>
        <div ref={input => {this.elem = input}} className='cornerstone-elm'>
          <canvas className="cornerstone-canvas"
            onContextMenu={e => e.preventDefault()}/>
          <div className='info-box'>
            <p>HU: {this.state.HU}</p>
            <p>WW: {
                this.state.viewport ?
                Math.round(scaleWWDynamic(this.state.viewport.voi.windowWidth,this.state.minPixelValue,this.state.maxPixelValue)) : 0}</p>
            <p>WC: {this.state.viewport ?
                Math.round(scaleWCDynamic(this.state.viewport.voi.windowCenter,this.state.minPixelValue,this.state.maxPixelValue)) : 0}</p>
          </div>
        </div>
        <Button
          ghost
          icon={this.props.fullScreen ? 'shrink' : 'arrows-alt'}
          className='toggle-full-screen'
          onClick={() => {
            this.props.toggleFullScreen().then(this.onResize)
          }}>
        </Button>
        <div className='view-footer'>
          <Button ghost onClick={() => {
            let viewport = cornerstone.getViewport(this.elem)
            viewport.scale = this.baseScale
            viewport.translation = {x: 0, y: 0}
            cornerstone.setViewport(this.elem, viewport)
            // cornerstone.reset(this.elem)
          }}><FormattedMessage id="CTFollowUp.viewer.autoFit"/></Button>
        <span className='img-pos'>{study.reverse?(study.stacks.z.imageIds.length-this.state.position.z):(this.state.position.z+1)} / {study.stacks.z.imageIds.length}</span>
        </div>
        <div className='patient-info'>
          <p><FormattedMessage id="CTFollowUp.viewer.name"/>{study.patientName}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.gender"/>{study.gender}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.age"/>{study.age}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.studyDate"/>{study.studyDate}</p>
          <p><FormattedMessage id="CTFollowUp.viewer.studyTime"/>{study.studyTime}</p>
        </div>
      </div>
    );
  }

}

export default DropTarget(itemTypes.CT_STUDY, viewerTarget, collect)(Viewer);
