import React, { Component } from 'react'
import {withRouter} from 'react-router'
import { Divider, Layout,Modal, Form, Input, Select, Button, DatePicker, message } from 'antd';

const { Header, Content, Footer } = Layout;
const Search = Input.Search
const FormItem = Form.Item;
const Option = Select.Option;
const {RangePicker} = DatePicker

import api from '../../api'
import LoadingWrapper from '../../component/LoadingWrapper'
import SignoutButton from '../../component/SignoutButton'
import StudyTable from '../../component/StudyTable'
import StudyHistoryTable from '../../component/StudyHistoryTable'
import DateRange from '../../component/DateRange'
import Logo from '../../component/Logo'
import {onClickModel} from '../../auth'
import Immutable from 'immutable'
import _ from 'underscore'
import {saveState, getState} from './state'
import EMPTYSTUDYLIST from '../../component/DefaultData'

import { injectIntl, FormattedMessage } from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import moment from 'moment'
import RunDetectionModal from '../CTViewerPage/RunDetectionModal'
import io from 'socket.io-client'
import {getUser} from '../../auth'
// 如果使用createBrowserHistory
import history from 'history/createBrowserHistory'

import './index.less'
import UpStep from "../../component/UpStep";
const confirm = Modal.confirm;
const FormItemStyle = {
  width: 120
}

const DEFAULT_CONDITION = {
  patientID: '',
  patientName: '',
  modality: '',
  bodyPartExamined: '',
  gender: '',
  studyDate: [moment(new Date().getTime()),moment(new Date().getTime())],
  birthDate: '',
  result: ''
}

const RESET_CONDITION = {
  patientID: '',
  patientName: '',
  modality: '',
  bodyPartExamined: '',
  gender: '',
  studyDate: [],
  birthDate: '',
  result: ''
}

function compareByUploadDate(a, b) {
  if (a.uploadDate > b.uploadDate) {
    return -1
  } else if (a.uploadDate < b.uploadDate) {
    return 1
  } else {
    return 0
  }
}

function groupByPatient(studyList) {
  let result = []
  let groupResult = _.groupBy(studyList, (d) => d.patientID+d.modality.toUpperCase()+d.bodyPartExamined.toUpperCase()+d.state.toUpperCase())
  for (let studies of _.values(groupResult)) {
    studies = _.sortBy(studies, (study) => study.studyDate+study.studyTime ).reverse()
    let first = _.clone(studies[0])
    first.history = [...studies]
    // let first = studies[0]
    // first.history = studies.slice(1)
    result.push(first)
  }
  result.sort(compareByUploadDate)
    result.map(function (item,index) {
      item.history.map(function (i,d) {
          if(item.modality=="CR"||item.modality=="DX"){
             result[index].history=[]
          }
          if(item.key==i.key){
              result[index].history.splice(d,1)
          }
      })
    })
  return result
  // return _.sortBy(result, study => study.studyTime).reverse()
}

function ungroupHistories(studyList){
  let result =[]
  for(let study of _.values(studyList))
  {
    let history = _.clone(study.history)
    result.push(history)  
  }
  return _.flatten(result)
}

class StudyListPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      studyList: Immutable.fromJS([]),
      filterStudyList:[],
      condition: DEFAULT_CONDITION,
      currentPage:localStorage.getItem("currentPage")?Number(localStorage.getItem("currentPage")):1 ,
      historySelections: new Immutable.Map(),
      detectionModalVisible:false,
      studyToDetect:'',
      numSelect:"",
      conditionSearch:false,
      searching:false,
      visible:false,
    }
    this.initSocket()
    this.onClickModel=this.onClickModel.bind(this);
    this.handleOkModel=this.handleOkModel.bind(this);
    this.handleCancelModel=this.handleCancelModel.bind(this);
    this.saveConditionGet=this.saveConditionGet.bind(this);
    this.dateSerach=this.dateSerach.bind(this);
  }

  componentDidMount() {
    this.saveConditionGet()
    let savedState = getState()
    if (!_.isEmpty(savedState)) {
      this.setState({condition: savedState.condition}, () => {
        this.updateTableDisplay(savedState.pagination, null, savedState.sorter)
      })
    }else
    {
      this.setState({}, () => {
        this.updateTableDisplay(savedState.pagination, null, savedState.sorter)
      })
    }
    if(!localStorage.getItem("serachConditions")){
        this.dateSerach(1)
    }else{
        this.initData()
    }
    //wlq edit
  }
  saveConditionGet(){
      if(localStorage.getItem("serachConditions")){
          if(JSON.parse(localStorage.getItem("serachConditions")).studyDate.length>0){
              if((JSON.parse(localStorage.getItem("serachConditions")).studyDate)[0]==null&&(JSON.parse(localStorage.getItem("serachConditions")).studyDate)[1]!=null){
                  const obj=[moment(new Date( (JSON.parse(localStorage.getItem("serachConditions")).studyDate)[1]).getTime()),moment(new Date( (JSON.parse(localStorage.getItem("serachConditions")).studyDate)[1]).getTime())]
                  const conditionObj=JSON.parse(localStorage.getItem("serachConditions"));
                  conditionObj.studyDate=obj;
                  this.setState({
                      condition:conditionObj,
                  })
              }else if((JSON.parse(localStorage.getItem("serachConditions")).studyDate)[0]!=null&&(JSON.parse(localStorage.getItem("serachConditions")).studyDate)[1]==null){
                  const obj=[moment(new Date( (JSON.parse(localStorage.getItem("serachConditions")).studyDate)[0]).getTime()),moment(new Date( (JSON.parse(localStorage.getItem("serachConditions")).studyDate)[0]).getTime())]
                  const conditionObj=JSON.parse(localStorage.getItem("serachConditions"));
                  conditionObj.studyDate=obj;
                  this.setState({
                      condition:conditionObj,
                  })
              }else{
                  const obj=[moment(new Date( (JSON.parse(localStorage.getItem("serachConditions")).studyDate)[0]).getTime()),moment(new Date( (JSON.parse(localStorage.getItem("serachConditions")).studyDate)[1]).getTime())]
                  const conditionObj=JSON.parse(localStorage.getItem("serachConditions"));
                  conditionObj.studyDate=obj;
                  this.setState({
                      condition:conditionObj,
                  })
              }
          }else{
              this.setState({
                  condition:JSON.parse(localStorage.getItem("serachConditions")),
              })
          }

      }else{
          this.setState({
              condition:RESET_CONDITION
          })
      }

      if(localStorage.getItem("currentPage")){
          this.setState({
              currentPage:localStorage.getItem("currentPage")
          })
      }
      //this.initData()
  }

  initSocket(){
    this.socket = io(window.location.origin,{transports: ['websocket']})
    this.socket.connect()
    this.socket.on('connect',()=>{ console.log("study list socket connected") })
    this.socket.on('studyListUpdate',(payload)=>{
      console.log('studyList update',payload.data)
      let flattenStudyList = Immutable.fromJS(ungroupHistories(this.state.studyList.toJS()))
      let idx = flattenStudyList.findIndex((value,index,array)=>{
       return value.toJS().studyUID===payload.data.studyUID
      })
      if(idx>=0)
      {
        this.setState({studyList:Immutable.fromJS(groupByPatient(flattenStudyList.updateIn([idx,'patientID'],patientID=>patientID=payload.data.patientID)
                       .updateIn([idx,'birthDate'],birthDate=>birthDate=payload.data.birthDate)
                       .updateIn([idx,'bodyPartExamined'],bodyPartExamined=>bodyPartExamined=payload.data.bodyPartExamined)
                       .updateIn([idx,'modality'],modality=>modality=payload.data.modality)
                       .updateIn([idx,'gender'],gender=>gender=payload.data.gender)
                       .updateIn([idx,'hospital'],hospital=>hospital=payload.data.hospital)
                       .updateIn([idx,'state'],state=>state=payload.data.state)
                       .updateIn([idx,'patientName'],patientName=>patientName=payload.data.patientName)
                       .updateIn([idx,'labelNum'],labelNum=>labelNum=payload.data.labelNum)
                       .updateIn([idx,'taskID'],taskID=>taskID=payload.data.taskID)
                       .updateIn([idx,'result'],result=>result=payload.data.result).toJS()))                  
                       },this.filterList)
      }
      if(this.state.studyToDetect.studyUID===payload.data.studyUID)
		  {
        this.setState({studyToDetect:payload.data})
		  }
    })
    this.socket.on('disconnect',()=>{ console.log("study list socket disconnected")})
    this.socket.emit('studyListOpen',{
      userID:getUser().account,
      taskID:'studyList'
    })
  }
  //日期快捷查询
  dateSerach(num){
      this.setState({
          numSelect:num,
      })
      this.state.condition=JSON.parse(localStorage.getItem("serachConditions"))?JSON.parse(localStorage.getItem("serachConditions")):DEFAULT_CONDITION
       switch (num) {
          case 1:
               this.state.condition.studyDate= [moment(new Date().getTime()),moment(new Date().getTime())];
               this.setState({
                   condition: this.state.condition,
               })
           break;
          case 2:
              var day=new Date();
              day.setDate(day.getDate()-2)
              this.state.condition.studyDate=[moment(day),moment(new Date().getTime())];
              this.setState({
                  condition: this.state.condition,
              })
              break;
          case 3:
              var day=new Date();
              day.setDate(day.getDate()-4)
              this.state.condition.studyDate=[moment(day),moment(new Date().getTime())];
              this.setState({
                  condition: this.state.condition,
              })
              break;
          case 4:
              var day=new Date();
              day.setDate(day.getDate()-6)
              this.state.condition.studyDate=[moment(day),moment(new Date().getTime())];
              this.setState({
                  condition: this.state.condition,
              })
              break;
          case 5:
              var day=new Date();
              day.setDate(day.getDate()-29)
              this.state.condition.studyDate=[moment(day),moment(new Date().getTime())];
              this.setState({
                  condition: this.state.condition,
              })
              break;
      }
      this.SearchByConditions()
  }
  getFilterList() {
    const SEARCH_COLUMNS = ['patientID', 'patientName']
    const SELECT_COLUMNS = ['gender']
    let condition = JSON.parse(localStorage.getItem("serachConditions"));
    let filterList = this.state.studyList.toJS().filter(study => {
      const fulfillSearch = SEARCH_COLUMNS.every(col => study[col].toUpperCase().includes(condition[col].toUpperCase()))
      const fulfillSelect = SELECT_COLUMNS.every(col => !condition[col] || study[col] === condition[col])
      let fullfillModality=true
      if(condition.modality === "CT")
      {
        fullfillModality = study.modality.toUpperCase()==="CT"
      }else if(condition.modality=== "DX"){
        fullfillModality = study.modality.toUpperCase()==="DR"||study.modality.toUpperCase()==="CR"||study.modality.toUpperCase()==="DX"
      }
      let fullfillBodyPart=true
      switch(condition.bodyPartExamined )
      {
        case "CHEST":
          fullfillBodyPart = study.bodyPartExamined.toUpperCase()==="CHEST"
          break;
        case "ABDOMEN":
          fullfillBodyPart = study.bodyPartExamined.toUpperCase()==="ABDOMEN" 
                              || study.bodyPartExamined.toUpperCase()==="LIVER"
          break;
        default:
      }
      let fullfillResult = true
      switch(condition.result)
      {
        case "NORMAL":
          fullfillResult = study.result.toUpperCase()==="NORMAL"&&study.state.toUpperCase()==="COMPLETED"
          break;
        case "ABNORMAL":
          fullfillResult = study.result.toUpperCase()==="ABNORMAL"&&study.state.toUpperCase()==="COMPLETED"
          break;
        case "ALGO_UNSUPPORT":
          fullfillResult = study.state.toUpperCase()==="ALGO_UNSUPPORT"
          break;
        case "PNG_UNSUPPORT":
              fullfillResult = study.state.toUpperCase()==="PNG_UNSUPPORT"
              break;
        case "META_ONLY":
          fullfillResult = study.state.toUpperCase()==="META_ONLY"
          break;
        default:
      }
      const studyDateRange =condition.studyDate>0?[moment(new Date(condition.studyDate[0].getTime())),moment(new Date(condition.studyDate[1].getTime()))]:""
      const fulfillStudyDate = studyDateRange.length === 0 || 
            ((!!studyDateRange[0]?study.studyDate >= studyDateRange[0].format('YYYY-MM-DD'):true) &&
             (!!studyDateRange[1]?study.studyDate <= studyDateRange[1].format('YYYY-MM-DD'):true))
      const fulfillBirtDate = !condition.birthDate || condition.birthDate.format('YYYY-MM-DD') === study.birthDate
      return fulfillSearch && fulfillSelect && fulfillStudyDate && fulfillBirtDate && fullfillBodyPart && fullfillResult && fullfillModality
    })
    return filterList
  }

  filterList = () => {
    this.setState({
      filterStudyList: this.getFilterList()
    })
  }

  onSelect(key, value) {
    // this.condition = this.condition.set(key, value)
    this.setState(
      {condition: Object.assign({}, this.state.condition, {[key]: value})},
      this.filterList
     )
      localStorage.setItem("currentPage",1);
      this.setState({
          currentPage:1
      })
      localStorage.setItem("serachConditions",JSON.stringify(Object.assign({}, this.state.condition, {[key]: value})))
  }

  SearchByConditions(){
    let patientID = this.state.condition.patientID
    let patientName= this.state.condition.patientName
    let gender = this.state.condition.gender
    let birthDate = !!this.state.condition.birthDate ? this.state.condition.birthDate.format("YYYY-MM-DD"):''
    let days=[]
    let dayRange = this.state.condition.studyDate
    let start=""
    let end=""
    this.commonFunction(end,start,dayRange,days,birthDate,gender,patientName,patientID)
      localStorage.setItem("serachConditions",JSON.stringify(this.state.condition));
      localStorage.setItem("currentPage",1);
      this.setState({
          currentPage:1
      })
  }
  initData(){
      this.state.condition= localStorage.getItem("serachConditions")?JSON.parse(localStorage.getItem("serachConditions")):RESET_CONDITION;
      this.saveConditionGet();
      let patientID =  this.state.condition?this.state.condition.patientID:""
      let patientName= this.state.condition.patientName
      let gender = this.state.condition.gender
      let birthDate = !!this.state.condition.birthDate ? this.state.condition.birthDate.format("YYYY-MM-DD"):''
      let days=[]
      let dayRange = this.state.condition.studyDate
      let start="";
      let end="";
      this.commonFunction(end,start,dayRange,days,birthDate,gender,patientName,patientID)
  }

  commonFunction(end,start,dayRange,days,birthDate,gender,patientName,patientID){
      if(dayRange.length===2&&!!dayRange[0]&&!!dayRange[1]){
          start = moment(dayRange[0])
          end = moment(dayRange[1])
      }
      else if(!!dayRange[0]){
          start = moment(dayRange[0])
          end= moment(new Date().getTime())
      }
      else if(!!dayRange[1]){
          end = moment(dayRange[1])
          start = moment(end)
      }
      else{
          start = null
          end =null
      }

      this.setState({
          studyList:this.state.studyList.clear()
      })

      if( !start || !end)
      {
          this.setState({searching:true})

          api.getStudylist({patientID:patientID,
              patientName:patientName,
              gender:gender,
              birthDate:birthDate
          })
              .then(res=>{
                  res.forEach((study, i) => study.key = study.studyUID)
                  this.setState({
                      studyList:Immutable.fromJS(groupByPatient(res)),
                      searching:false},this.filterList)
              })
              .catch(function (err){
                  this.setState({
                      searching:false})
              })
      }else{
          let daysCount = Math.ceil(end.diff(start,'hours')/24.0)+1

          if(daysCount>365*2)
          {
              message.error(GetLocaledText(this,"StudyPage.search.error.long"))
              return
          }
          let dayStart = start.subtract(1,'days')
          for(var i=0;i<daysCount;i++)
          {
              days.push(dayStart.add(1,'days').format("YYYY-MM-DD"))
          }

          let allP=[]
          days.map((day)=>{
                  let aP =new Promise((resolve,reject)=>{
                      api.getStudylist({studyDate:day,
                          patientID:patientID,
                          patientName:patientName,
                          gender:gender,
                          birthDate:birthDate
                      })
                          .then(res=>{
                              res.forEach((study, i) => study.key = study.studyUID)
                              let listUpdated = this.state.studyList.toJS().concat(res)
                              this.setState({
                                      studyList:Immutable.fromJS(listUpdated)}
                                  ,this.filterList)
                              resolve(res)
                          })
                          .catch(function (err){
                              reject(err)
                          })
                  })
                  allP.push(aP)
                  return allP;
              }
          )
          this.setState({searching:true})
          Promise.all(allP).then(res=>{
              this.setState({studyList:Immutable.fromJS(groupByPatient(this.state.studyList.toJS())),searching:false},this.filterList)
          }).catch(err=>{
              this.setState({studyList:Immutable.fromJS(groupByPatient(this.state.studyList.toJS())),searching:false},this.filterList)
          })
      }
  }


  onFilterChange(key, e) {
    // this.condition = this.condition.set(key, e.target.value)
    this.setState(
      {condition: Object.assign({}, this.state.condition, {[key]: e.target.value})},
      this.filterList
    )
      localStorage.setItem("currentPage",1);
      this.setState({
          currentPage:1
      })
      localStorage.setItem("serachConditions",JSON.stringify(Object.assign({}, this.state.condition, {[key]: e.target.value})))
  }

  resetCondition = () => {
    this.state.condition={
        patientID: '',
        patientName: '',
        modality: '',
        bodyPartExamined: '',
        gender: '',
        studyDate: [],
        birthDate: '',
        result: ''
    }
    localStorage.setItem("serachConditions",JSON.stringify(this.state.condition))
    this.setState({
      condition: this.state.condition,
      numSelect:""
    },()=>{
        //that.SearchByConditions()
    })
  }
  onTableChange = (pagination, filters, sorter) => {
    //console.log(pagination, filters, sorter);
    saveState({pagination})
    this.updateTableDisplay(pagination, filters, sorter)
  }
  
  onRunDetection = (data)=>{
    let idx = this.state.studyList.findIndex((value,index,array)=>{
      return value.toJS().studyUID===data.studyUID
    })
    if(data.state==="meta_only")
    { 
      api.doctorRequest({studyDate:data.studyDate,studyUID:data.studyUID})
        .then(res=>{this.setState({studyList:this.state.studyList.updateIn([idx,'state'],state=>state="pull_data")},this.filterList)})
    }else if(data.state==="algo_unsupport"||data.state==="completed")
    {
      this.setState({
        detectionModalVisible:true,
        studyToDetect:this.state.studyList.toJS()[idx]
      })
    }
  }

  onDeleteStudyListPage=(data)=>{
      confirm({
          title:GetLocaledText(this, "StudyTable.deleteText"),
          onOk() {

          },
          onCancel() {

          },
      });
   }

  updateTableDisplay = (pagination, filters, sorter) => {
    let filterStudyList = this.getFilterList()
    if (!_.isEmpty(sorter)) {
      saveState({sorter})
      filterStudyList = filterStudyList.sort(sorter.column.sorter)
    }
    pagination?localStorage.setItem("currentPage", pagination.current):"";
    let currentPage = localStorage.getItem("currentPage") ?Number(localStorage.getItem("currentPage")) : 1;
    this.setState({
      filterStudyList,
      currentPage
    })
  }

  componentWillUnmount() {
    this.socket.emit('studyListClose',{
      userID:getUser().account,
      taskID:'studyList'
    })
    this.socket.close()
    this.saveConditionGet()
  }
  //return list

    //帮助文档
    onClickModel(){
      this.setState({
          visible:true,
      })
    }
    handleOkModel(){
        this.setState({
            visible:false,
        })
    }
    handleCancelModel(){
        this.setState({
            visible:false,
        })
    }

  render() {
      if(this.state.currentPage){
          localStorage.setItem("currentPage",this.state.currentPage);
      }
     return (
      <div id='study-list-page' className='page'>
        <Header style={{height:"3rem",lineHeight:"3rem"}}>
          <Logo/>
          <UpStep/>
          <SignoutButton />
          <span style={{color:"#fff",float:"right",marginRight:"10px",cursor:"pointer"}} onClick={this.onClickModel}><img style={{height:"3rem"}} src={require('../../../static/images/help_nor.png')}/></span>
        </Header>
        <Content className='content'>
          <RunDetectionModal
            visible={this.state.detectionModalVisible}
            studyToDetect={this.state.studyToDetect}
            handleCancel={()=>this.setState({detectionModalVisible:false})}
          />
          <div className='content-title'>
            <h3 style={{float: 'left'}}><FormattedMessage id='StudyPage.title'/></h3>
            <Button type="primary" icon="upload" style={{float: 'right'}} onClick={() => this.props.history.push('/upload')}>
              <FormattedMessage id='StudyPage.upload.button'/>
            </Button>
          </div>
          <div className='table-wrapper'>
            <Form layout="inline" className='condition-form'>
              <FormItem label={GetLocaledText(this, "StudyPage.studyID")} labelCol={{span: 24}} wrapperCol={{span: 24}} className='patient-id'>
                <Search onSearch={this.filterList}
                  value={this.state.condition?this.state.condition.patientID:""}
                  onChange={this.onFilterChange.bind(this, 'patientID')}/>
              </FormItem>
              <FormItem label={GetLocaledText(this, "StudyPage.patient-name")} labelCol={{span: 24}} wrapperCol={{span: 24}} className='patient-name'>
                <Search onSearch={this.filterList}
                  value={this.state.condition?this.state.condition.patientName:""}
                  onChange={this.onFilterChange.bind(this, 'patientName')}/>
              </FormItem>
              <FormItem label={GetLocaledText(this, "StudyPage.gender")} labelCol={{span: 24}} wrapperCol={{span: 24}} className='gender'>
                <Select onSelect={this.onSelect.bind(this, 'gender')} value={this.state.condition?this.state.condition.gender:""}>
                  <Option value="MALE"><FormattedMessage id='StudyPage.gender.male'/></Option>
                  <Option value="FEMALE"><FormattedMessage id='StudyPage.gender.female'/></Option>
                  <Option value=""><FormattedMessage id='StudyPage.select.all'/></Option>
                </Select>
              </FormItem>
              <FormItem label={GetLocaledText(this, "StudyPage.birthday")} labelCol={{span: 24}} wrapperCol={{span: 24}} className='birthdate'>
                <DatePicker
                  value={this.state.condition?this.state.condition.birthDate:""}
                  onChange={(date, dateString) => {
                    this.setState(
                      {condition: Object.assign({}, this.state.condition, {birthDate: date})},
                      this.filterList
                    )
                  }} format='YYYY-MM-DD' />
              </FormItem>
              <FormItem label={GetLocaledText(this, "StudyPage.modality")} labelCol={{span: 24}} wrapperCol={{span: 24}} className='modality'>
                <Select onSelect={this.onSelect.bind(this, 'modality')} value={this.state.condition?this.state.condition.modality:""}>
                  <Option value="CT">CT</Option>
                  <Option value="DX">DX</Option>
                  <Option value=""><FormattedMessage id='StudyPage.select.all'/></Option>
                </Select>
              </FormItem>
              <FormItem label={GetLocaledText(this, "StudyPage.body-part")} labelCol={{span: 24}} wrapperCol={{span: 24}} className='body-part'>
                <Select onSelect={this.onSelect.bind(this, 'bodyPartExamined')} value={this.state.condition?this.state.condition.bodyPartExamined:""}>
                  {
                    _.uniq(this.state.studyList.map(study => study.bodyPartExamined)).filter(part => part.trim()).map((part, i) => {
                      return <Option value={part} key={i}>{part}</Option>
                    })
                  }
                  <Option value="CHEST">CHEST</Option>
                  <Option value="ABDOMEN">ABDOMEN</Option>
                  <Option value=""><FormattedMessage id='StudyPage.select.all'/></Option>
                </Select>
              </FormItem>
              <FormItem label={GetLocaledText(this, "StudyPage.study-time")} labelCol={{span: 24}} wrapperCol={{span: 24}} className='study-time'>
                <DateRange
                  format="YYYY-MM-DD"
                  value={this.state.condition?this.state.condition.studyDate:[]}
                  onChange={(date) => {
                    this.setState(
                      {condition: Object.assign({}, this.state.condition, {studyDate: date}),
                       conditionSearch:true},
                      this.filterList
                    )
                }}/>
                  <a className={this.state.numSelect==1?"activeClass":"spanClass"} onClick={()=>{this.dateSerach(1)}}>{GetLocaledText(this, "StudyPage.today")}</a>
                  <a className={this.state.numSelect==2?"activeClass":"spanClass"} onClick={()=>{this.dateSerach(2)}}>{GetLocaledText(this, "StudyPage.three.days")}</a>
                  <a className={this.state.numSelect==3?"activeClass":"spanClass"} onClick={()=>{this.dateSerach(3)}}>{GetLocaledText(this, "StudyPage.five.days")}</a>
                  <a className={this.state.numSelect==4?"activeClass":"spanClass"} onClick={()=>{this.dateSerach(4)}}>{GetLocaledText(this, "StudyPage.week")}</a>
                 <a className={this.state.numSelect==5?"activeClass":"spanClass"} onClick={()=>{this.dateSerach(5)}}>{GetLocaledText(this, "StudyPage.month")}</a>
              </FormItem>
              <FormItem label={GetLocaledText(this, "StudyPage.diagnosis-result")} labelCol={{span: 24}} wrapperCol={{span: 24}} className='result'>
                <Select onSelect={this.onSelect.bind(this, 'result')} value={this.state.condition?this.state.condition.result:""}>
                  <Option value="NORMAL"><FormattedMessage id='StudyPage.diagnosis-result.normal'/></Option>
                  <Option value="ABNORMAL"><FormattedMessage id='StudyPage.diagnosis-result.abnormal'/></Option>
                  <Option value="ALGO_UNSUPPORT"><FormattedMessage id='StudyPage.state.algo_unsupport'/></Option>
                  <Option value="PNG_UNSUPPORT"><FormattedMessage id='StudyPage.state.png_unsupport'/></Option>
                  <Option value="META_ONLY"><FormattedMessage id='StudyPage.state.meta_only'/></Option>
                  <Option value=""><FormattedMessage id='StudyPage.select.all'/></Option>
                </Select>
              </FormItem>
              <Button className='condition-button search-button' type="primary" onClick={this.SearchByConditions.bind(this)}>
                <FormattedMessage id='StudyPage.filter.search-btn'/>
              </Button>
              <Button className='condition-button clear-button' type="primary" onClick={this.resetCondition}>
                <img src={require('../../../static/images/brush.png')}/>
                <FormattedMessage id='StudyPage.clear-conditions'/>
              </Button>
            </Form>
            <Divider />
            <StudyTable
              dataSource={this.state.filterStudyList}
              onChange={this.onTableChange}
              showDetectionOption={true}
              runDetection ={this.onRunDetection}
              deleteStudyListpage={this.onDeleteStudyListPage}
              rowClassName={(record) => !!record.history ? (record.history.length >=1 ? 'expandable' : '') : ''}
              loading={this.state.searching}
              expandedRowRender={record => {
                return (
                  <StudyHistoryTable
                    onSelectChange={(selections) => this.setState({historySelections: this.state.historySelections.set(record.patientID, selections)})}
                    dataSource={!!record.history ? (record.history.length >=1 ? record.history:null) : null}/>
                )
              }}
              isFollowUpDisable={(record) => {
                let selections = this.state.historySelections.get(record.patientID)
                let notCTchest = true
                let notCompleted = true
                if(record.labelType==="Nodule") notCTchest = false
                if(record.modality==="CT" && record.bodyPartExamined.toUpperCase()==="CHEST") notCTchest = false 
                if(record.state==="completed") notCompleted = false               
                return !selections || selections.length !== 2 || notCTchest || notCompleted
              }}
              onFollowUpClick={(record) => {
                let patientID = record.patientID
                let studyUIDs = this.state.historySelections.get(record.patientID)
                //console.log(studyUIDs)
                if (studyUIDs.length !== 2) return
                studyUIDs = studyUIDs.reverse().join(',').replace(/\./g, '-')
                this.props.history.push(`/view_ct_follow_up/${patientID}/${studyUIDs}`)
              }}
              getFollowUpStudyIDs={(record)=>{
                let studyUIDs = this.state.historySelections.get(record.patientID)
                return studyUIDs
              }}
              pagination={{
                pageSize: 15,
                current: this.state.currentPage,
                showQuickJumper: true,
                showTotal: (total, range) => {
                  return (
                    <span>
                      <FormattedMessage id='StudyPage.page-handle.pre-total'/>
                      {total}
                      <FormattedMessage id='StudyPage.page-handle.post-total'/>
                    </span>
                    )
                  }
              }}/>
          </div>
         </Content>
          <Modal
              title={GetLocaledText(this, "HelpDocument")}
              visible={this.state.visible}
              onOk={this.handleOkModel}
              onCancel={this.handleCancelModel}
              width={840}
              bodyStyle={{background:"#000",color:"#fff"}}
              footer={null}
          >
              <img src={require('../../../static/images/helpDocument.png')} style={{width:"794px"}} />
          </Modal>
      </div>
    )
  }

}

StudyListPage = withRouter(StudyListPage)

async function getData(props) {
  let today = moment(new Date().getTime()).format("YYYY-MM-DD")
  let studyListErr = false
  let studyList =[]
  //     await api.getStudylist({studyDate:today}).catch((err)=>{
  //     message.error("API error, getstudylist",600)
  //     studyList = true
  // })
  if(studyListErr)
  {
      studyList = EMPTYSTUDYLIST
  }
  studyList.forEach((study, i) => study.key = study.studyUID)
  let initData = groupByPatient(studyList)
  //console.log(initData);
  return await {studyList, initData}
}

StudyListPage = LoadingWrapper(StudyListPage,getData)

export default injectIntl(StudyListPage)
