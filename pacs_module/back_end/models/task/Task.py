#!/bin/python
import uuid

taskTypelist = ['label', 'normal']
statelist = ['assign', 'diagnosis', 'review', 'pass']

# add task state control

class Task(object):
    def __init__(self, reporter, reviewer, studyUID):
        self.id = str(uuid.uuid4())
        self.reporter = reporter
        self.reviewer = reviewer
        self.studyUID = studyUID
        self.type = "demo"
        self._state = 'assign'

    def get_role(self, userID):
        if userID == self.reporter:
            return "reporter"
        if userID == self.reviewer:
            return "reviewer"
        return "anonymous"

    def get_user_privilege(self, userID):
        user_privilege = []
        if userID == self.reporter:
            user_privilege.append("reporter")
        if userID == self.reviewer:
            user_privilege.append("reviewer")
        if self.type == 'label':
            user_privilege.append("label")
        if self.type == 'normal':
            user_privilege.append("normal")
        return user_privilege

    def hello(self):
        if self.state == statelist[0]:
            self.state = statelist[1]
            return True
        return False

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        if state not in statelist:
            raise ValueError("Report state must be one of {}, while input is {}".format(statelist, state))
        self._state = state

    def submit(self):
        self.state = statelist[2]

    def unsubmit(self):
        self.state = statelist[1]

    def confirm(self):
        self.state = statelist[3]

    def reject(self):
        self.state = statelist[1]

    def unconfirm(self):
        self.state = statelist[2]


required_key = ['taskID', 'reporter', 'reviewer', 'studyUID', "type", 'state']

def parseTaskInfo(info):
    if type(info) is not dict:
        raise ValueError("parseJobInfo input must be dict type, while type is {}".format(type(info)))

    if not all(key in info for key in required_key):
        raise ValueError("parseTaskInfo key mismatch", info.keys(), required_key)

    new_task = Task(info['reporter'], info['reviewer'], info['studyUID'])
    new_task.id = info['taskID']
    new_task.type = info['type']
    new_task.state = info['state']
    return new_task
    

if __name__ == '__main__':
    t = Task("111", "222", "333")
    t.state = "a"
    print t.state