#!/bin/python

status_message = {
    "1000": "OK",
    "1100": "BAD PARAMETERS",
    "1101": "INVALID PARAMETERS",
    "1102": "INVALID OPERATION",
    "1103": "INVALID PASSWORD",
    "1104": "INVALID ACCOUNT",
    "1105": "INVALID FILE",
    "1106": "USER EXIST",
    "1107": "EMAIL USED",
    "1108": "EMAIL IS NOT CONFIRMED",
    "1109": "REQUIRED INFO MISSING",
    "1110": "UPSTREAM ERROR",
    "1111": "SEND EMAIL ERROR",
    "1200": "ACCESS DENIED",
    "1301": "MULTI SERIES",
    "1302": "MODALITY NOT SUPPORTED"
}


class response(object):
    def __init__(self):
        self._data = {}
        self._statusCode = 1000
        self._message = status_message['1000']

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    @property
    def statusCode(self):
        return self._statusCode

    @statusCode.setter
    def statusCode(self, statusCode):
        if str(statusCode) not in status_message.keys():
            raise ValueError('could not find {} in statusCode'.format(statusCode))

        self._statusCode = statusCode
        self._message = status_message[str(statusCode)]

    def to_dict(self):
        return {
            "data": self._data,
            "statusCode": self._statusCode,
            "message": self._message
        }