#coding=utf-8
import logging
logger = logging.getLogger("back_end.services.pacs.pacs_api")
from config import ADDRESS, PORT, ALGO_ADDRESS, ALGO_PORT, FILEMETHOD, HOST_ROOT
import httplib2
import json
import sys
import os
import datetime
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from flask import make_response, request, jsonify, abort
from flask_login import current_user
from models.database.database_ops import db_find_one, db_find, db_update_one, db_insert_one,db_find_update_otherwise_insert
from models.response.response import response
from models.study.Study import Study,parseStudyInfo
from socketIO_client import SocketIO, LoggingNamespace
from flask_login import current_user
from models.report.Report import Report, parseReportInfo
from models.task.Task import Task, parseTaskInfo
from services.basic.basic_api import socket_emit

import urllib
HOST = "http://{}:{}".format(ADDRESS, 7000)

def getYesterday(): 
    today=datetime.date.today() 
    oneday=datetime.timedelta(days=1) 
    yesterday=today-oneday  
    return yesterday.strftime('%Y%m%d')

state_support_list = ['meta_only','pull_data','modality_unsupport']
def unsend_studylist_from_pacs(params,studylist):

    result_list = []

    study_info_list = None
    study_date = None
    url = HOST + '/api/find_patient_list'
    if 'studyDate' in params and params['studyDate'] != '': 
        study_date = params['studyDate']
        study_date = study_date.replace('-','')
        url = url + '?studyDate=' + study_date
        study_info_list  = db_find_one("study_info_list", {"studyDate": study_date})
    else:
        is_first_param = True
        if 'patientID' in params and params['patientID'] != '':
            url = url + ('?' if is_first_param else '&') + 'patientID=' + params['patientID']
            is_first_param = False
        if 'patientName' in params and params['patientName'] != '':
            url = url + ('?' if is_first_param else '&') + 'patientName=' + urllib.quote(params['patientName'].encode('utf-8'))
            is_first_param = False        
        if 'gender' in params and params['gender'] != '':
            url = url + ('?' if is_first_param else '&') + 'gender=' + params['gender']
            is_first_param = False            
        if 'birthDate' in params and params['birthDate'] != '':
            birthDate = params['birthDate'].replace('-','')
            url = url + ('?' if is_first_param else '&') + 'birthDate=' + birthDate
            is_first_param = False
        if is_first_param:   return result_list
    
    
    need_filter = False        
    if study_info_list == None: #requst from pacs_module
        try:
            http = httplib2.Http()
            headers = { 'content-type' : 'application/json' }
            resp, content = http.request(url, 'GET',body = json.dumps({}),headers = headers)
            content_json = json.loads(content)
            if content_json['message'] == "OK":
                study_info_list = content_json['data']
            else:
                return result_list
            
            if study_date != None and study_info_list['same_number_pacs']:
                db_find_update_otherwise_insert("study_info_list", {"studyDate": study_date},study_info_list)
                
            need_filter = True
        except Exception as e:
            logger.error("unsend_studylist_from_pacs error")
            logger.error(e)
            return result_list

    studyUID_list = []
    if need_filter:
        for study_temp in studylist:
            studyUID_list.append(study_temp.studyUID)

    data_list = study_info_list['data_list']
    for data_temp in data_list:
        if data_temp['state'] not in state_support_list or data_temp['studyUID'] in studyUID_list:
            continue
        new_study = Study(data_temp['studyUID'])
        new_study.birthDate = None if data_temp['birthDate'] == '' else datetime.datetime.strptime(data_temp['birthDate'],'%Y%m%d')
        new_study.bodyPartExamined = data_temp['bodyPartExamined']
        new_study.gender = data_temp['gender']
        new_study.hospital = data_temp['hospital']
        new_study.modality = data_temp['modality']
        new_study.patientID = data_temp['patientID']
        new_study.patientName = data_temp['patientName']
        
        if len(data_temp['studyDate']) > 8: date_time = datetime.datetime.strptime(data_temp['studyDate'],'%Y%m%d:%H%M%S')
        else: date_time = datetime.datetime.strptime(data_temp['studyDate'],'%Y%m%d')
        new_study.studyDate = date_time
        
        new_study.uploadDate = None
        new_study.series = None
        new_study.result = ""
        new_study.state = data_temp['state']
        result_list.append(new_study)
    return result_list

def get_patientInfo(studydate,instanceUID):
    study_info_list  = db_find_one("study_info_list", {"studyDate": studydate})
    if study_info_list == None: return None
    data_list = study_info_list['data_list']
    for data_temp in data_list:
        if data_temp['studyUID'] == instanceUID: return data_temp
    return None
    
def get_new_study_dict(study,userID):
    ownership_info = db_find_one("ownership", {"userID": userID})
    taskID = ownership_info['shared'][study.studyUID.replace(".", "-")]
    new_dict = study.to_dict(False)
    new_dict['taskID'] = taskID
    new_dict['user_privilege'] = []
    new_dict["diagnosisAdvice"] = ""
    new_dict['submitted'] = False
    new_dict['labelNum'] = 0
    new_dict['result'] = ""
    new_dict['labelType'] = ''
    if taskID != "":
        label_info = db_find_one("label", {"taskID": taskID})
        if label_info is None and new_dict['state'] != 'completed':
            new_dict['result'] = ""
        else:
            new_dict['result'] = "ABNORMAL" if len(label_info['labels']) else "NORMAL"
            new_dict['labelType'] = label_info['labelType']
                        
        report_info = db_find_one("report", {"taskID": taskID})
        new_report = parseReportInfo(report_info)
        new_dict["diagnosisAdvice"] = new_report.diagnosisAdvice
        new_dict['submitted'] = new_report.submitted()
        new_dict['labelNum']  = len(label_info['labels'])
        task_info = db_find_one('task', {"taskID": taskID})
        task = parseTaskInfo(task_info)
        new_dict['user_privilege'] = task.get_user_privilege(userID)
    return new_dict

study_state_list = ['file_only','algo_unsupport','completed']
def update_study_instance_state(study_date,study_uid,study_state):
    study_info_list = db_find_one("study_info_list", {"studyDate": study_date})
    if study_info_list == None: return
    
    data_list = study_info_list['data_list']
    new_data = None
    for data in data_list:
        if data['studyUID'] == study_uid:
            data['state'] = study_state
            new_data = data
            break
    db_find_update_otherwise_insert("study_info_list", {"studyDate": study_date},study_info_list)

    if new_data != None:
        userID = current_user.get_id()
        if study_state in study_state_list:
            send_socketIO_to_node(study_uid,userID)
        else:
            socket_emit('UpdatestudyList', {'data': new_data, 'userID': userID,'taskID':'studyList'})


def send_socketIO_to_node(study_uid,userID):
    study_info = db_find_one("study", {"studyUID": study_uid})
    new_study = parseStudyInfo(study_info)
    ret_data = get_new_study_dict(new_study,userID)
    socket_emit('UpdatestudyList', {'data': ret_data, 'userID': userID,'taskID':'studyList'})
    

def fetch_study_info_list():
    if not current_user.is_authenticated:
        abort(401)
        
    res = response()
    userID = current_user.get_id()

    params = request.args.to_dict() if request.method == 'GET' else request.json
    
    studydate = params['studyDate']
    today = datetime.datetime.now().strftime('%Y%m%d').decode('utf-8')
    yesterday = getYesterday().decode('utf-8')
    
    study_info_list = db_find_one("study_info_list", {"studyDate": studydate})
    
    if study_info_list == None:
        db_find_update_otherwise_insert("study_info_list", {"studyDate": studydate},params)
    elif studydate == today or studydate == yesterday:
        result = {}
        result['studyDate'] = params['studyDate']
        result['same_number_pacs'] = params['same_number_pacs']
        result_data_list = []
        
        data_list_new = params['data_list']
        data_list_old = study_info_list['data_list']
        
        for data_old in data_list_old:
            if data_old['state'] == 'meta_only': continue
            for data_new in data_list_new:
                if data_old['studyUID'] == data_new['studyUID']: 
                    data_new['state'] = data_old['state']
                    break

        result['data_list'] = data_list_new
        db_find_update_otherwise_insert("study_info_list", {"studyDate": studydate},result)
    elif study_info_list['same_number_pacs']:
        logger.warning("fetch_study_info_list data exit")
        pass
    else:
        logger.warning("fetch_study_info_list error")
        pass
            
    if request.method == "POST":
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)


required_key_doctor_request = ['studyUID','studyDate']
def fetch_doctor_request():
    if not current_user.is_authenticated:
        abort(401)
        
    res = response()
    userID = current_user.get_id()

    params = request.args.to_dict() if request.method == 'GET' else request.json
    logger.info(params)

    if not all(key in params for key in required_key_doctor_request):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)

    studyuid = params['studyUID']
    study_date = params["studyDate"]
    study_date = study_date.replace('-','')
    try:
        http = httplib2.Http()
        headers = { 'content-type' : 'application/json' }
        url = HOST + '/api/doctor_request?studyUID=' + studyuid + "&studyDate="+study_date
        logger.info(url)
        update_study_instance_state(study_date,studyuid,'pull_data')
        resp, content = http.request(url, 'GET',body = json.dumps({}),headers = headers)
        content_json = json.loads(content)
        logger.info(content_json)
    except Exception as e:
        logger.error("fetch_doctor_request error")
        logger.error(e)

    if request.method == "GET":
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)
    else:
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
    

required_key_update_study_state = ['studyUID','studyDate','study_state']
def update_study_state():
    if not current_user.is_authenticated:
        abort(401)
        
    res = response()
    userID = current_user.get_id()

    params = request.args.to_dict() if request.method == 'GET' else request.json
    logger.info(params)

    if not all(key in params for key in required_key_update_study_state):
        res.statusCode = 1100
        return make_response(jsonify(res.to_dict()), 200)
        
    studyuid = params['studyUID']
    study_date = params['studyDate']
    study_state = params['study_state']
        
    update_study_instance_state(study_date,studyuid,'modality_unsupport')
    if request.method == "GET":
        res.statusCode = 1000
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)
    else:
        res.statusCode = 1100
        res.data = []
        return make_response(jsonify(res.to_dict()), 200)
    
        
