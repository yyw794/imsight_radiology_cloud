import React, { Component } from 'react'
import CT3Viewer from '../../component/CT3Viewer'
import api from '../../api'
import LoadingWrapper from '../../component/LoadingWrapper'
import ToolBar from '../../component/CTToolBar'
import NodulePanel from '../../component/NodulePanel'
import Snapshot from '../../component/Snapshot'
import ThumbnailList from '../../component/ThumbnailList'
import {getRiskText, getNoduleType, RISK_EN2CN, TYPE_EN2CN} from '../../component/NodulePanel'
import NodulePanelLiver from '../../component/NodulePanelLiver'
import NodulePanelRibs from '../../component/NodulePanelRibs'
import noduleBox from '../../component/tools/noduleBox'
import axios from 'axios'
// import {Icon, message, Button, Modal, Spin, Menu, Dropdown, Row, Col, Select} from 'antd'
import {Card, Layout, Button, Dropdown, Row, Col, Select, message, Tabs, Modal, Form, TreeSelect} from 'antd'
import {FormattedMessage, injectIntl} from 'react-intl'
import { GetLocaledText } from '../../localedFuncs'
import {addCTMetaData} from '../../metaData.js'
import ReportForm from '../../component/ReportForm'
import {getUser} from '../../auth'
import AddNoduleModal from './AddNoduleModal'
import RunDetectionModal from './RunDetectionModal'
import _ from 'underscore'
import io from 'socket.io-client'
import itemTypes from '../../component/dragDropItemTypes'
import DropZoom from './DropZoom'
import { connect } from 'react-redux'
import Immutable from 'immutable'
import {openWindow,canOpenReport,openWindowBlank,openWindowBlankTab} from '../../component/StudyTable'

import './index.less'
import Logo from "../../component/Logo";
import Return from "../../component/ReturnList";
import SignoutButton from "../../component/SignoutButton";
import {EMPTYSTUDYCT,EMPTYREPORT,EMPTYNODULELABELS,EMPTYTUMORLABELS} from "../../component/DefaultData"
import {DICOM_URL_PREFIX, URL_PREFIX} from "../../component/tools/config";

const { Header, Content, Footer } = Layout;
const TabPane = Tabs.TabPane

class CTViewerPage extends Component {
  constructor(props) {
    super(props)
    const defaultSeriesNum = Object.keys(props.data.study.series)[0]
    this.state = {
      visible:false,
      study: props.data.study,
      series: props.data.study.series,
      lungNodules: props.data.study.noduleLabels.map(label => Object.assign({}, label)),
      liverTumors: props.data.study.tumorLabels.map(label => Object.assign({}, label)),
      ribsNodules:props.data.study.ribsLables.map(label => Object.assign({}, label)),
      targetSeriesID: defaultSeriesNum,
      activeTabKey: props.data.study.tumorLabels.length===0?"chest": props.data.study.ribsLables.length===0?"liver":"ribs",
      activeTool: ['wwwc'],
      addModalVisible: false,
      visiblePdf:false,
      addModalStatus: 'confirm',
      detectionModalVisible:false,
      detectionStatus:'selection',
      detectionList:[],
      nodulePopoverBtn: {
        action: 'add',
        show: false,
        left: 0,
        top: 0
      },
      displaySeries: [defaultSeriesNum]
    }
    if (this.props.data.report) {
      this.report = this.props.data.report
    }
      this.initSocket()
      this.viewers = {}
      this.onClickModel=this.onClickModel.bind(this);
      this.handleOkModel=this.handleOkModel.bind(this);
      this.handleCancelModel=this.handleCancelModel.bind(this);
      this.pdfPre=this.pdfPre.bind(this);
      this.handleOkPdfModel=this.handleOkPdfModel.bind(this);
      this.handleCancelPdfModel=this.handleCancelPdfModel.bind(this);
      this.changeToolState=this.changeToolState.bind(this);
      this.onBlurSetww=this.onBlurSetww.bind(this);
      this.initObj=this.initObj.bind(this);
    // this.nodulePanels = {}
  }
  setGridLayout(num) {
    if(num==4){
        num=3;
        this.setState({
            displaySeries: this.state.displaySeries.slice(0, num)
        }, () => {
            this.state.displaySeries.filter(sn => sn !== null).forEach(sn => this.viewers[sn].onResize())
        })
        num=4
        let arr = new Array(num - 1)
        arr.fill(null)
        this.setState({
            displaySeries:[ this.state.displaySeries[0], ...arr]
        }, () => {
            this.state.displaySeries.filter(sn => sn !== null).forEach(sn => this.viewers[sn].onResize())
        })
        return;
    }
    if (num === this.state.displaySeries.length) {
      return
    } else if (num < this.state.displaySeries.length) {
      this.setState({
        displaySeries: this.state.displaySeries.slice(0, num)
      }, () => {
        this.state.displaySeries.filter(sn => sn !== null).forEach(sn => this.viewers[sn].onResize())
      })
    } else {
      let arr = new Array(num - this.state.displaySeries.length)
      arr.fill(null)
      this.setState({
        displaySeries: [...this.state.displaySeries, ...arr]
      }, () => {
        this.state.displaySeries.filter(sn => sn !== null).forEach(sn => this.viewers[sn].onResize())
      })
    }
    // console.log( this.state.displaySeries)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.id.replace(/-/g, '.') !== this.state.study.studyUID) {
      console.log('CTViewerPage reload data')
      getData(nextProps).then(data => {
        this.setState({
          study: data.study,
          lungNodules: data.study.noduleLabels.map(label => Object.assign({}, label)),
          liverTumors: data.study.tumorLabels.map(label => Object.assign({}, label)),
          addModalVisible: false,
          addModalStatus: 'confirm',
          detectionModalVisible:false,
          detectionStatus:'selection',
          detectionList:[],
          nodulePopoverBtn: {
            action: 'add',
            show: false,
            left: 0,
            top: 0
          }
        })
        if (data.report) {
          this.report = data.report
        }
      })
    }
  }

  componentWillUnmount(){
    this.socket.emit('close',{
      userID: getUser().account,
      taskID: this.props.data.study.taskID
    })
    this.socket.close()
  }

  initSocket() {
    this.socket = io(window.location.origin, {transports: ['websocket']})
    this.socket.connect()
    this.socket.on('connect', () => {
      console.log('connected');
    })
    this.socket.on('labelUpdate', (payload) => {
      if(this.state.activeTabKey==="liver")
      {
        console.log("socket data inject to liverTumors");
        let tumorLabels=[];
        api.getLabels({
                      taskID:this.state.study.taskID,
                      seriesID:seriesID,
                      labelType:"LiverTumor"})
                      .then((labels)=>{
                          tumorLabels=labels
                          this.setState({
                              liverTumors:tumorLabels.map(label => Object.assign({}, label))
                          })
                      })
                      .catch((err)=>{
                          message.error("API error, getLabels",60)
                          console.log("update tumor label socket, get labels ",err)
                      })
      }else if(this.state.activeTabKey==="chest"){
        let noduleLabels=[];
        api.getLabels({
                taskID:this.state.study.taskID,
                seriesID:this.state.targetSeriesID,
                labelType:"Nodule"})
                .then((labels)=>{
                    noduleLabels=labels
                    this.setState({
                        lungNodules:noduleLabels.map(label => Object.assign({}, label))
                    })
                })
                .catch((err)=>{
                    message.error("API error, getLabels",60)
                    console.log("update nodule label socket, get labels ",err)
                })
      }else if(this.state.activeTabKey==="ribs"){

          let noduleLabels=[];
          api.getLabels({
              taskID:this.state.study.taskID,
              seriesID:this.state.targetSeriesID,
              labelType:"Rib"})
              .then((labels)=>{
                  noduleLabels=labels
                  let arr=[];
                  noduleLabels.forEach(function (item,index) {
                      let x=0,y=0,z=0;
                      item.coord.forEach(function (item1,index1) {
                          x+=item1[0];
                          y+=item1[1];
                          z+=item1[2];
                      })
                      const centerLocation={
                          coord: {x:x/8, y: y/8, z: z/8},
                          labelID: item.labelID,
                          seriesID: item.seriesID,
                          url: item.url,
                          nodule_avgHU:-(z/8),
                      }
                      item=centerLocation;
                      arr.push(item)
                  })
                  this.setState({
                      ribsNodules:arr.map(label => Object.assign({}, label))
                  })
              })
              .catch((err)=>{
                  message.error("API error, getLabels",60)
                  console.log("update nodule label socket, get labels ",err)
              })
      }
    })

    this.socket.on('xyConvert',(payload)=>{
      console.log('socket xyConvert',payload.data);
      if(payload.data!=="done") return;
      api.getStudyDetail({studyUID:this.state.study.studyUID})
          .then((study)=>{
              Object.values(study.series).forEach(series =>{
 		    series.instances.x.forEach(ins=>ins.url = URL_PREFIX + ins.url)
		    series.instances.y.forEach(ins=>ins.url = URL_PREFIX + ins.url)
		    series.instances.z.forEach(ins=>ins.url = DICOM_URL_PREFIX + ins.url)
	      })

              addCTMetaData(study)
              Object.values(study.series).forEach(series => {
                  series['stacks'] = {
                      x: {
                          currentImageIdIndex: 0,
                          imageIds: series.instances.x.map(ins => ins.url)
                      },
                      z: {
                          currentImageIdIndex: 0,
                          imageIds: series.instances.z.map(ins => ins.url)
                      },
                      y: {
                          currentImageIdIndex: 0,
                          imageIds: series.instances.y.map(ins => ins.url)
                      },
                  }
              })
              study.noduleLabels = this.state.study.noduleLabels
              study.tumorLabels = this.state.study.tumorLabels
              this.setState({
                      study,
                  series:study.series
              })

          })
          .catch((err)=>{console.log("receive socket xyConvert, get studydetail",err)})
    })

    this.socket.on('disconnect', () => {
      console.log('disconnect');
    });
    this.socket.emit('open', {
      userID: getUser().account,
      taskID: this.props.data.study.taskID
    })
  }

  onToolClick = (name,extra) => {
      console.log(extra)
      if(!extra){
          if(name === 'wwwc'){
              this.setState({
                  activeTool:["wwwc"]
              })
              for (let viewer of Object.values(this.viewers)) {
                  viewer.clearAllTool()
              }
              return;
          }
      }
    if(name==='detect'){
      return this.setState({detectionModalVisible:true})
    }

    if(name==='report'){
      return openWindowBlankTab("report",this.props.match.params.id.replace(/-/g, '.'),"")
    }
    if (name === 'grid') {
        if(extra){
            return this.setGridLayout(extra)
        }
    }

    for (let viewer of Object.values(this.viewers)) {
      if(viewer===null){
        continue
      }
      if(name==='MPR'){
         viewer.clearAllTool()
         viewer.onMPR()
      }
      if(name==='eraser'){
          if(this.state.activeTool.indexOf("MPR")!=-1){
              this.setState({
                  activeTool:["wwwc","MPR"]
              })
          }else{
              this.setState({
                  activeTool:["wwwc"]
              })
          }
          viewer.clearAllTool()
          return;
      }
      if (name === 'wwwc' && extra) {
        viewer.setWWWC(extra.ww, extra.wc)
      } else if (!this.state.activeTool.includes(name)) {
         viewer.activateTool(name)
          if(name==="length"||name==="angle"||name==="freehand"){
              if(this.state.activeTool.indexOf("MPR")!=-1){
                  this.setState({
                      activeTool:[name,"wwwc","MPR"]
                  })
              }else{
                  this.setState({
                      activeTool:[name,"wwwc"]
                  })
              }
          }else{
              this.setState({
                  activeTool: [name, ...this.state.activeTool]
              })
          }
      } else {
        viewer.deactivateTool(name)
        this.setState({
          activeTool: _.without([...this.state.activeTool], name)
        })
      }
    }
  }

  saveScreenshotToReport = (label) => {
    var data = new FormData();
    const instancesZ = this.state.series[this.state.targetSeriesID].instances.z;
    const instanceIndex = label.coord.z < instancesZ.length? (label.coord.z -1 ):0
    data.append('screenshotSize',300)
    data.append('lessionSize',50)
    data.append('url',instancesZ[instanceIndex].url.replace(DICOM_URL_PREFIX,""))
    data.append('ww',this.viewers[this.state.targetSeriesID].getWW())
    data.append('wc',this.viewers[this.state.targetSeriesID].getWC())
    data.append('labelID', label.labelID)
    data.append('seriesID',this.state.targetSeriesID)
    data.append('taskID',this.state.study.taskID)
    api.dicomShot(data)
      .then((res) => {
        message.success(GetLocaledText(this,"CTViewerPage.alert.screenshot.success"))
      })
      .catch(function (err) {
        message.error(GetLocaledText(this,"CTViewerPage.alert.screenshot.failed"))
      })
  }

  removeScreenshotInReport = (label) => {
    // this.report.screenshotURLs = this.report.screenshotURLs.filter(url => url !== nodule.url)
    // api.updateReport(this.report)
    let idx = this.state.lungNodules.findIndex(l => l.labelID === label.labelID)
    if (idx !== -1) {
      let lungNodules = [...this.state.lungNodules]
      // delete lungNodules[idx].url
      lungNodules[idx].url = ''
      let data = {
        taskID: this.state.study.taskID,
        seriesID: this.state.targetSeriesID,
        labels: lungNodules,
        labelType:"Nodule",
      }
      api.saveLabels(data).then(() =>{
          this.setState({lungNodules})
          //message.success(GetLocaledText(this,"CTViewerPage.alert.screenshot.success"))
      })
        .catch((err)=>{
        message.error("API error, saveLabels",60)
        throw err
      })
    }
    idx = this.state.liverTumors.findIndex(l => l.labelID === label.labelID)
    if (idx !== -1) {
      let liverTumors = [...this.state.liverTumors]
      // delete liverTumors[idx].url
      liverTumors[idx].url = ''
      let data = {
        taskID: this.state.study.taskID,
        seriesID: this.state.targetSeriesID,
        labels: liverTumors,
        labelType:"LiverTumor"
      }
      api.saveLabels(data).then(() => this.setState({liverTumors}))
        .catch((err)=>{
        message.error("API error, saveLabels",60)
        throw err
      })
    }
  }

  onAddNoduleClick = () => {
    if(this.state.activeTabKey=="ribs"){
        return;
    }
    if(this.state.study.state!=="algo_unsupport"){
        this.setState({
            addModalVisible: true,
            addModalStatus: 'confirm'
        })
    }else{
        message.warn("此CT片不允许添加结节");
    }
  }

  /**
   * analyze nodule using algorithm
   * @return {[Promise]} request
   */
  analyzeNodule = () => {
    const viewer = this.viewers[this.state.targetSeriesID]
    const imgPos = viewer.getImgPos()
    let coord = {
      x: imgPos.x + 1,
      y: imgPos.y + 1,
      z: imgPos.z + 1
    }
    let noduleToAdd = {
      coord,
      nodule_cross:[],
      seriesID:this.state.targetSeriesID
    }
    let data = {
      taskID: this.state.study.taskID,
      seriesID: this.state.targetSeriesID,
      labels:[noduleToAdd,...this.state.lungNodules],
      labelType:"Nodule"
    }
    return api.saveLabels(data)
    .then(resp => {
      if (_.isEmpty(resp)) {return}
      if (this.state.lungNodules.find(label => label.labelID === resp.labelID)) {return}
      this.setState({
        lungNodules: resp
      }, () => {
        let select = [...this.state.lungNodules].find(label => label.coord.z === coord.z && label.coord.y === coord.y && label.coord.x===label.coord.x)
        this.nodulePanel.getWrappedInstance().selectNodule(select)
        viewer.forceUpdateImage()
        console.log('added nodule')
      })
    })
      .catch((err)=>{
      message.error("API error, saveLabels",60)
      throw err
    })
  }

  updateTumor(labelID, isAPE, mfsWashout, mfsEnhancing,mfsThresholdGrowth){
    let changeTumor = [...this.state.liverTumors].find(l=>l.labelID === labelID)
    changeTumor.isAPE = isAPE
    changeTumor.mfsWashout = mfsWashout
    changeTumor.mfsEnhancing = mfsEnhancing
    changeTumor.mfsThresholdGrowth = mfsThresholdGrowth
    let stillTumor = [...this.state.liverTumors].filter(l=>l.labelID !== labelID)
    let data = {
      seriesID:this.state.targetSeriesID,
      taskID:this.state.study.taskID,
      labels:[changeTumor, ...stillTumor],
      labelType:"LiverTumor"
    }
    api.saveLabels(data)
    .then(resp => {
      this.setState({
        liverTumors:resp
      },()=>{
        console.log(resp)
      })
    })
      .catch((err)=>{
      message.error("API error, saveLabels",60)
      throw err
    })
  }

  removeNodule = (labelID) => {
    const viewer = this.viewers[this.state.targetSeriesID]
    let removedList = [...this.state.lungNodules].filter(label => label.labelID !== labelID)
    let data = {
      seriesID:this.state.targetSeriesID,
      taskID:this.state.study.taskID,
      labels:removedList,
      labelType:"Nodule"
    }
    api.saveLabels(data)
    .then(resp => {
      this.setState({
      lungNodules:resp
      }, () => {
        viewer.forceUpdateImage()
        message.success(GetLocaledText(this,"CTViewerPage.remove-label"))
      })
    })
      .catch((err)=>{
      message.error("API error, saveLabels",60)
      throw err
    })
  }

  removeTumor = (labelID)=>{
    const viewer = this.viewers[this.state.targetSeriesID]
    let removedList = [...this.state.liverTumors].filter(label => label.labelID !== labelID)
    let data = {
      seriesID:this.state.targetSeriesID,
      taskID:this.state.study.taskID,
      labels:removedList,
      labelType:"LiverTumor"
    }
    console.log("before remove:")
    api.saveLabels(data)
    .then(resp => {
      console.log("after removed:")
      console.log(resp)
      this.setState({
      liverTumors:resp
      }, () => {
        viewer.forceUpdateImage()
        message.success(GetLocaledText(this,"CTViewerPage.remove-label"))
      })
    })      
      .catch((err)=>{
      message.error("API error, saveLabels",60)
      throw err
    })
  }

  isForbidLabeling() {
    return false
  }

    /**
     * 切换MPR三视图同步双击事件
     * @param nodule
     * @return {*}
     */
     changeToolState(data){
         let that=this;
         if(data){
             this.setState({
                 activeTool:[...this.state.activeTool,"MPR"]
             })
         }else {
             that.state.activeTool.map(function (item,index) {
                 if(item==="MPR"){
                     that.state.activeTool.splice(index,1)
                 }
             })
             that.setState({
                 activeTool:that.state.activeTool
             })
         }
     }


  renderNoduleInfoEditor = (nodule) => {
    if (!nodule) {return ''}
    const RISK_PROB = {
      Benign: 0,
      Low: 0.5,
      Middle: 0.8,
      High: 1
    }
    let risk
    let type
    if (nodule.malg !== null) {
      risk = getRiskText(nodule.malg)
    }
    if (nodule.subtTrue !== null) {
      type = getNoduleType(nodule)
    }
    return (
      <div className='nodule-info-editor'>
        <Row>
          <Col span={12}><FormattedMessage id='CTViewerPage.nodule-info-editor.subclass'/></Col>
          <Col span={12}>
            <Select value={type} disabled={this.isForbidLabeling()} className='selector-list' onChange={v => {
                this.updateNodule(this.nodulePanel.getWrappedInstance().getSelectedNodule().labelID, {subtTrue: v})
              }}>
              <Option value='GroundGlass'> {TYPE_EN2CN.GroundGlass} </Option>
              <Option value='PartSolid'> {TYPE_EN2CN.PartSolid} </Option>
              <Option value='Solid'> {TYPE_EN2CN.Solid} </Option>
              <Option value='Calcification'> {TYPE_EN2CN.Calcification} </Option>
            </Select>
          </Col>
        </Row>
        <Row>
          <Col span={12}><FormattedMessage id='CTViewerPage.nodule-info-editor.malignancy'/></Col>
          <Col span={12}>
            <Select value={risk} disabled={this.isForbidLabeling()} className='selector-list' onChange={v => {
                this.updateNodule(this.nodulePanel.getWrappedInstance().getSelectedNodule().labelID, {malg: RISK_PROB[v]})
              }}>
              <Option value='Benign'> {RISK_EN2CN.Benign} </Option>
              <Option value='Low'> {RISK_EN2CN.Low} </Option>
              <Option value='Middle'> {RISK_EN2CN.Middle} </Option>
              <Option value='High'> {RISK_EN2CN.High} </Option>
            </Select>
          </Col>
        </Row>

      </div>
    )
  }

  initObj(item,a){
      let itemObj= {
          addByUser: item.addByUser,
          coord: {
              x: a?a.x:item.coord.x,
              y: a?a.y:item.coord.y,
              z: a? a.z : item.coord.z
          },
          height: item.height?item.height:"",
          labelID: item.labelID?item.labelID:"",
          nodule_avgHU: item.nodule_avgHU?item.nodule_avgHU:"",
          nodule_bm: {
              Benign: item.Benign?item.Benign:"",
              Mainly_Benign: item.Mainly_Benign?item.Mainly_Benign:"",
              Mainly_Malignant: item.Mainly_Malignant?item.Mainly_Malignant:"",
              Malignant: item.Malignant?item.Malignant:""
          },
          type:item.type?item.type:"",
          major_diam:Math.sqrt(Math.abs(a.major_diam[0][0]-a.major_diam[1][0])*Math.abs(a.major_diam[0][0]-a.major_diam[1][0])+Math.abs(a.major_diam[0][1]-a.major_diam[1][1])*Math.abs(a.major_diam[0][1]-a.major_diam[1][1])),
          minor_diam:Math.sqrt(Math.abs(a.minor_diam[0][0]-a.minor_diam[1][0])*Math.abs(a.minor_diam[0][0]-a.minor_diam[1][0])+Math.abs(a.minor_diam[0][1]-a.minor_diam[1][1])*Math.abs(a.minor_diam[0][1]-a.minor_diam[1][1])),
          major_diam1:a?{x:a.major_diam[0][0],y:a.major_diam[0][1],z:a.z}:"",
          major_diam2:a?{x:a.major_diam[1][0],y:a.major_diam[1][1],z:a.z}:"",
          minor_diam1:a?{x:a.minor_diam[0][0],y:a.minor_diam[0][1],z:a.z}:"",
          minor_diam2:a?{x:a.minor_diam[1][0],y:a.minor_diam[1][1],z:a.z}:"",
          nodule_cross: item.nodule_cross?item.nodule_cross:"",
          nodule_diameter: item.nodule_diameter?item.nodule_diameter:"",
          nodule_location: item.nodule_location?item.nodule_location:"",
          nodule_prob: item.nodule_prob?item.nodule_prob:"",
          nodule_type: {
              Calcification: item.nodule_type.Calcification?item.nodule_type.Calcification:"",
              Ground_Glass: item.nodule_type.Ground_Glass?item.nodule_type.Ground_Glass:"",
              Part_Solid: item.nodule_type.Part_Solid?item.nodule_type.Part_Solid:"",
              Solid: item.nodule_type.Solid?item.nodule_type.Solid:""
          },
          nodule_volume: item.nodule_volume?item.nodule_volume:"",
          seriesID: item.seriesID?item.seriesID:"",
          state: item.state?item.state:"",
          url: item.url? item.url:"",
          width: item.width?item.width:""
      };
      return itemObj
  }

  renderGrid() {
    let rows
      let that=this;
    let series = this.state.displaySeries.map(seriesID => {
      return seriesID ? this.state.series[seriesID] : null
    })
    if (series.length <= 3) {
      rows = [series]
    } else {
      rows = _.chunk(series, 2)
    }
    let idx = 0
    return rows.map((row, i) => {
      const span = 24 / row.length
      return (
        <Row>
          {row.map((data, j) => {
            let pos = idx++
            if (data) {
              // const id = `r${i}c${j}`
              const id = data.seriesID
              let labels = this.state.activeTabKey === 'chest' ? this.state.lungNodules :this.state.activeTabKey === 'ribs'?this.state.ribsNodules:this.state.liverTumors
                   labels = labels.filter(l => l.seriesID === id)
                   let lablesNodules=[];
                       if( this.state.activeTabKey === 'chest'){
                           labels.forEach(function (item) {
                               if(item.nodule_cross.length>0) {
                                   item.nodule_cross.forEach(function (a) {
                                       let itemObj = that.initObj(item,a)
                                       lablesNodules.push(itemObj)
                                   })
                               }else{
                                       let itemObj=that.initObj(item)
                                       lablesNodules.push(itemObj)

                               }
                           })
                           labels=lablesNodules;
                       }
               return (
                <Col style={{width:`${span/24*100}%`}} span={span} className='grid-cell'>
                  <DropZoom onDrop={data => {
                      let displaySeries = [...this.state.displaySeries]
                      displaySeries[pos] = data.id
                      this.setState({
                        displaySeries,
                        targetSeriesID:data.id
                      })
                    }}>
                  <CT3Viewer
                    ref={input => {this.viewers[id] = input}}
                    id={id}
                    study={this.state.study}
                    series={data}
                    activeTabKey={this.state.activeTabKey}
                    labels={Immutable.fromJS(labels)}
                    instance={this.state.displayInstance}
                    changeToolState={this.changeToolState}
                    tools={this.state.tools}
                    onLabelsChange={labels => this.setState({labels})}
                    onToolsChange={tools=>this.setState({tools})}
                    onlyZ={true}
                    beginXYConvert={()=>
                      api.beginXYConvert({
                        studyUID:this.state.study.studyUID,
                        taskID:this.state.study.taskID})
                    } 
                    enableToggler={series.length >1}
                    onClick={e => {
                      // TODO: liver label
                      if (this.state.activeTabKey === 'liver') {
                        const imgPos = this.viewers[id].getImgPos()
                        const clickedTumors = noduleBox.getInsideNodules(e.detail.element, imgPos)
                        if (clickedTumors.length) {
                          this.setState({targetSeriesID: id}, () => this.nodulePanelLiver.getWrappedInstance().selectNodule(clickedTumors[0]))
                        }
                      }else{
                        const imgPos = this.viewers[id].getImgPos()
                        const clickedNodules = noduleBox.getInsideNodules(e.detail.element, imgPos)
                        if (clickedNodules.length) {
                          this.setState({targetSeriesID: id}, () => this.nodulePanel.getWrappedInstance().selectNodule(clickedNodules[0]))
                        }
                        if(this.state.activeTabKey=="ribs"){
                            this.setState({
                                nodulePopoverBtn: Object.assign({}, this.state.nodulePopoverBtn, {show: false})
                            })
                            return;
                        }
                        if (e.detail.which === 3) {
                          this.setState({
                            nodulePopoverBtn: {
                              action: clickedNodules.length > 0 ? 'remove' : 'add',
                              show: true,
                              left: e.detail.currentPoints.page.x,
                              top: e.detail.currentPoints.page.y
                            }
                          })
                        } else if (e.detail.which === 1) {
                          this.setState({
                            nodulePopoverBtn: Object.assign({}, this.state.nodulePopoverBtn, {show: false})
                          })
                        }
                      }
                      this.setState({targetSeriesID: id})
                    }}
                    style={{height: '100%', position: 'relative'}}/>
                  </DropZoom>
                </Col>
              )
            } else {
              return (
                <Col style={{height:'100%',width:`${span/24*100}%`}} span={span} className='grid-cell'>
                  <DropZoom onDrop={data => {
                      let displaySeries = [...this.state.displaySeries]
                      displaySeries[pos] = data.id
                      this.setState({displaySeries})
                    }}/>
                </Col>
              )
            }
          })}
        </Row>
      )
    })
  }
    /**
     * pdf预览
     */
    pdfPre(){
        this.setState({
            visiblePdf:true,
        })
    }
    handleOkPdfModel(){
        this.setState({
            visiblePdf:false,
        })
   }
    handleCancelPdfModel(){
        this.setState({
            visiblePdf:false,
        })
  }

    /**
     * 判断是否启动调窗快捷键
     */
    onBlurSetww(data){
        const viewer = this.viewers[this.state.targetSeriesID]
        if(data){
            viewer.closeKeyDown()
        }else{
            viewer.addKeyDown()
        }
    }

    //帮助文档
    onClickModel(){
        this.setState({
            visible:true,
        })
    }
    handleOkModel(){
        this.setState({
            visible:false,
        })
    }
    handleCancelModel(){
        this.setState({
            visible:false,
        })
    }


  render() {
    let user = getUser()
    let userID = ''
    if (user) {
      userID = user.account
    }
    const studyUID = this.props.match.params.id.replace(/-/g, '.')
    const nodulePopoverBtn = this.state.nodulePopoverBtn
    let {age, gender, patientName, studyDate, studyTime,patientID} = this.state.study;
    let toolsArr=['wwwc', 'referenceLines','hideBoxes','grid','length','angle','freehand','MPR','eraser'/*'detect'*/]
    return (
            <div style={{height: '100%', backgroundColor: '#000'}} id='ct-viwer-page'>
              <div style={{ position:"absolute",
                  width:"100%",
                  height:"3.1rem",
                  background:"#001529",
                  borderBottom:"1px solid #3e3e3e",
                  zIndex:"111111"}}>
                 <Header style={{height:"48px",lineHeight:"48px",padding:"0 1rem"}}>
                     <Logo />
                     <Return  />
                     <div style={{float:"left"}}><ToolBar  tools={toolsArr} activeTool={this.state.activeTool} forbidEdit={false} onToolClick={this.onToolClick}/></div>
                    <SignoutButton/>
                    <span style={{color:"#fff",float:"right",marginRight:"10px",cursor:"pointer",marginTop:"1px",height:"48px"}} onClick={this.onClickModel}><img style={{height:"3rem"}} src={require('../../../static/images/help_nor.png')}/></span>
                </Header>
              </div>
              <div className='content'  style={{paddingTop:"3rem"}}>
              {this.renderGrid()}
              {
                  Object.keys(this.state.series).length > 1 ?
              <ThumbnailList
                  activeIds={this.state.displaySeries.filter(s => !!s)}
                  data={Object.values(this.state.series).map(series => {
                      return Object.assign({id: series.seriesID, stacks: series.stacks}, this.state.study)
                  })}
                  renderItem={study => <Snapshot study={study}/>}/>
              :
                  ''
              }
              </div>
              <div class="asideContiner">
                <div className='aside'>
                  <Row>
                    <Col  xs={21} sm={20} md={20} lg={20} xl={24} className="asideHeight">
                    <Tabs defaultActiveKey='chest'
                          type="card"
                          activeKey={this.state.activeTabKey}
                          onTabClick={(activeTabKey) =>{
                                  this.setState({activeTabKey},function () {
                                         switch(activeTabKey){
                                          case "ribs":
                                              for (let viewer of Object.values(this.viewers)) {
                                                  viewer.setWWWC(1500,300)
                                                  viewer.lablesMark("scrollTo")
                                                  viewer.initLabelTool()
                                              }
                                              break;
                                          default:
                                              for (let viewer of Object.values(this.viewers)) {
                                                  viewer.setWWWC(1500, -550)
                                                  viewer.lablesMark("scrollTo")
                                                  viewer.initLabelTool()
                                              }
                                              break;
                                       }
                                      for (let viewer of Object.values(this.viewers)) {
                                      viewer.lablesMark()
                                     }
                                  })
                          }}>
                        <TabPane tab={GetLocaledText(this, "CTViewerPage.lung")+ " "+(this.state.lungNodules.filter(l=>l.seriesID===this.state.targetSeriesID).length >99?"99+":this.state.lungNodules.filter(l=>l.seriesID===this.state.targetSeriesID).length)} key='chest'>
                               <NodulePanel data={this.state.lungNodules?this.state.lungNodules.filter(l=>l.seriesID===this.state.targetSeriesID):[]}
                                         ref={input => {this.nodulePanel = input}}
                                         onSelectNodule={(nodule) => {
                                             const {seriesID} = nodule
                                             if (!(seriesID in this.viewers)) {
                                                 return
                                             }
                                             this.viewers[seriesID].scrollTo('x', null, nodule.coord.x)
                                             this.viewers[seriesID].scrollTo('y', null, nodule.coord.y)
                                             this.viewers[seriesID].setState({HU: nodule.nodule_avgHU})
                                              return this.viewers[seriesID].scrollTo('z', null, nodule.coord.z)
                                         }}
                                         renderDetail={null}
                                         maxZ={this.state.series[this.state.targetSeriesID].stacks.z.imageIds.length}
		               		             zReverse={this.state.study.reverse}
                                         hideSelection={!this.props.data.report}
                                         hideEditNoduleBtn={this.isForbidLabeling()}
                                         onAddScreenshot={this.saveScreenshotToReport}
                                         onRemoveScreenshot={this.removeScreenshotInReport}
                                         onAdd={this.onAddNoduleClick}
                                         onDelete={this.removeNodule}/>
                        {this.state.study.state=="algo_unsupport"||this.state.study.state=="file_only"?"":<ReportForm onShowPdfPre={this.pdfPre} blurSetww={this.onBlurSetww} />}
                        </TabPane>
                        <TabPane tab={GetLocaledText(this, "CTViewerPage.ribs")+ " "+(this.state.ribsNodules.filter(l=>l.seriesID===this.state.targetSeriesID).length >99?"99+":this.state.ribsNodules.filter(l=>l.seriesID===this.state.targetSeriesID).length)} key='ribs'>
                            <NodulePanelRibs data={this.state.ribsNodules?this.state.ribsNodules.filter(l=>l.seriesID===this.state.targetSeriesID):[]}
                            ref={input => {this.nodulePanel = input}}
                            onSelectNodule={(nodule) => {
                            const {seriesID} = nodule
                            if (!(seriesID in this.viewers)) {
                                return
                            }
                            // console.log("seriesID",seriesID)
                            // console.log(nodule)
                            this.viewers[seriesID].scrollTo('x', null, nodule.coord.x - 1)
                            this.viewers[seriesID].scrollTo('y', null, nodule.coord.y - 1)
                            //this.viewers[seriesID].setState({HU: nodule.nodule_avgHU})
                            return this.viewers[seriesID].scrollTo('z', null, nodule.coord.z - 1)
                            }}
                            renderDetail={null}
                            maxZ={this.state.series[this.state.targetSeriesID].stacks.z.imageIds.length}
                            zReverse={this.state.study.reverse}
                            hideSelection={!this.props.data.report}
                            hideEditNoduleBtn={this.isForbidLabeling()}
                            onAddScreenshot={this.saveScreenshotToReport}
                            onRemoveScreenshot={this.removeScreenshotInReport}
                            onAdd={this.onAddNoduleClick}
                            onDelete={this.removeNodule}/>
                            </TabPane>
                  <TabPane  tab={GetLocaledText(this, "CTViewerPage.liver")+" "+(this.state.liverTumors.length>99?"99+":this.state.liverTumors.length)} key='liver'>
                   <NodulePanelLiver data={this.state.liverTumors}
                                               ref={input => {this.nodulePanelLiver = input}}
                                               onSelectNodule={(tumor) => {
                                                   const {seriesID} = tumor
                                                  if (!(seriesID in this.viewers)) {
                                                      return
                                                 }
                                                   this.viewers[seriesID].scrollTo('x', null, tumor.coord.x - 1)
                                                 this.viewers[seriesID].scrollTo('y', null, tumor.coord.y - 1)
                                                   this.viewers[seriesID].setState({HU: tumor.avgHU,
                                                      imgPosZ: tumor.coord.z,
                                                     imgPosY: tumor.coord.y,
                                                      imgPosX: tumor.coord.x,
                                                  })
                                                  return this.viewers[seriesID].scrollTo('z', null, tumor.coord.z - 1)
                                              }}
                                             renderDetail={null}
                                              maxZ={this.state.series[this.state.targetSeriesID].stacks.z.imageIds.length}
					       zReverse={this.state.study.reverse}
				              hideSelection={!this.props.data.report}
                                              hideEditNoduleBtn={this.isForbidLabeling()}
                                              onAddScreenshot={this.saveScreenshotToReport}
                                             onRemoveScreenshot={this.removeScreenshotInReport}
                                             onAdd={this.onAddNoduleClick}
                                             onDelete={this.removeTumor}
                                              onTumorChanged = {this.updateTumor.bind(this)}
                            />
                                                   </TabPane>
                    </Tabs>
                    </Col>
                   </Row>
                </div>
                {
                    <div className='patient-info' style={{marginTop:"50px"}}>
                        <p><FormattedMessage id='CTViewerPage.patient-id'/>：{patientID}</p>
                        <p><FormattedMessage id='CTViewerPage.patient-name'/>：{patientName}</p>
                        <p><FormattedMessage id='CTViewerPage.gender'/>：{gender==="MALE"?GetLocaledText(this,"CTViewerPage.gender.male"):GetLocaledText(this,"CTViewerPage.gender.female")}</p>
                        <p><FormattedMessage id='CTViewerPage.age'/>：{age}</p>
                        <p><FormattedMessage id='CTViewerPage.study-date'/>：{studyDate}</p>
                        <p><FormattedMessage id='CTViewerPage.study-time'/>：{studyTime}</p>
                    </div>
                }
              </div>
                <AddNoduleModal
                    visible={this.state.addModalVisible}
                    status={this.state.addModalStatus}
                    handleOk={() => {
                        this.setState({
                            addModalStatus: 'adding'
                        })
                        this.analyzeNodule().finally(() => {
                            this.setState({
                                addModalStatus: 'finished',
                                addModalVisible: false
                            })
                        })
                    }}
                    handleCancel={() => this.setState({addModalVisible: false})}
                />
                {/*<RunDetectionModal
          visible={this.state.detectionModalVisible}
          status={this.state.detectionStatus}
          handleOk={()=>this.setState({detectionStatus:'detecting'})}
          handleCheck={(checkedValues)=>{
                       this.setState({detectionList:checkedValues})
                      }}
          handleCancel={()=>this.setState({detectionModalVisible:false})}
        />*/}
                <Button style={{
                    position: 'fixed',
                    top: nodulePopoverBtn.top,
                    left: nodulePopoverBtn.left,
                    display: !this.isForbidLabeling() && nodulePopoverBtn.show ? 'block' : 'none'
                }}
                        onClick={() => {
                            this.state.nodulePopoverBtn.action === 'add' ? this.onAddNoduleClick() : this.removeNodule(this.nodulePanel.getWrappedInstance().getSelectedNodule().labelID)
                            this.setState({
                                nodulePopoverBtn: Object.assign({}, this.state.nodulePopoverBtn, {show: false})
                            })
                        }}
                >
                    {nodulePopoverBtn.action === 'add' ? GetLocaledText(this, "CTViewerPage.add-label") : GetLocaledText(this, "CTViewerPage.remove-label")}
                </Button>
                <Modal
                    title={GetLocaledText(this, "HelpDocument")}
                    visible={this.state.visible}
                    onOk={this.handleOkModel}
                    onCancel={this.handleCancelModel}
                    width={840}
                    bodyStyle={{background:"#000",color:"#fff"}}
                    footer={null}
                >
                    <img src={require('../../../static/images/helpDocument.png')} style={{width:"794px"}} />
                </Modal>
                <Modal
                  title={GetLocaledText(this, "textPdfPrint")}
                  visible={this.state.visiblePdf}
                  onOk={this.handleOkPdfModel}
                  onCancel={this.handleCancelPdfModel}
                  width={900}
                  height={850}
                  footer={null}>
                      <div style={{overflow:"hidden",textAlign:"center"}}>
                          <iframe style={{marginLeft:"-90px"}} name="myiframe" id="myrame" src={window.location.href.replace("view_ct","report_print_model")} frameborder="0" align="left"  scrolling="no" width="1000" height="900">
                                <p>你的浏览器不支持iframe标签</p>
                          </iframe>
                          <a href={window.location.href.replace("view_ct","report_print")} target="blank"><Button style={{margin:"0 auto"}} type="primary" ><FormattedMessage id="ReportPageLung.print"/></Button></a>
                      </div>
                 </Modal>
            </div>
    )
  }

}

async function getData(props) {
  let studyAPIErr = false
  let labelAPIErr = false
  let reportAPIErr= false
  const user = getUser()
  let studyUID = props.match.params.id.replace(/-/g, '.')
  let study
  let report
  let labels
  study = await api.getStudyDetail({studyUID})
    .catch((err)=>{
    message.error("API error, getStudy",60)
    studyAPIErr = true
  })
    localStorage.setItem("fellowParams",JSON.stringify(study))
  if(!studyAPIErr){
    study.tumorLabels = []
    study.noduleLabels = []
    study.ribsLables=[]
    for(let seriesID in study.series)
    {
       if(study.noduleLabels)
       {     
         let labels = await api.getLabels({
             taskID:study.taskID,
             seriesID:seriesID,
             labelType:"Nodule"})
         .catch((err)=>{
           message.error("API error, getLabels",60)
           labelAPIErr = true
         })
         study.noduleLabels=[...labels,...study.noduleLabels]
       }else
       {
         study.noduleLabels = await api.getLabels({
             taskID:study.taskID,
             seriesID:seriesID,
             labelType:"Nodule"})
           .catch((err)=>{
           message.error("API error, getLabels",60)
           labelAPIErr = true
         })
       }
    }

    for(let seriesID in study.series)
      {
          if(study.tumorLabels)
          {
              let labels = await api.getLabels({
                  taskID:study.taskID,
                  seriesID:seriesID,
                  labelType:"LiverTumor"})
                  .catch((err)=>{
                      message.error("API error, getLabels",60)
                      labelAPIErr = true
                  })
              study.tumorLabels=[...labels,...study.tumorLabels]
          }else
          {
              study.tumorLabels = await api.getLabels({
                  taskID:study.taskID,
                  seriesID:seriesID,
                  labelType:"LiverTumor"})
                  .catch((err)=>{
                      message.error("API error, getLabels",60)
                      labelAPIErr = true
                  })
          }
      }

      for(let seriesID in study.series)
      {
          if(study.ribsLables)
          {
              let labels = await api.getLabels({
                  taskID:study.taskID,
                  seriesID:seriesID,
                  labelType:"Rib"})
                  .catch((err)=>{
                      message.error("API error, getLabels",60)
                      labelAPIErr = true
                  })
              study.ribsLables=[...labels,...study.ribsLables]
          }else
          {
              study.ribsLables = await api.getLabels({
                  taskID:study.taskID,
                  seriesID:seriesID,
                  labelType:"Rib"})
                  .catch((err)=>{
                      message.error("API error, getLabels",60)
                      labelAPIErr = true
                  })
          }
      }
      let arr=[];
      study.ribsLables.forEach(function (item,index) {
          let x=0,y=0,z=0;
          let xs=[];
          let ys=[];
          let zs=[];
          for(let i=0;i<item.coord.length;i++){
             if(i==0){
                 xs.push(item.coord[0])
                 ys.push(item.coord[0])
                 zs.push(item.coord[0])
             }else{
               if(xs[0][0]==item.coord[i][0]){
                  xs.push(item.coord[i])
               }
               if(ys[0][1]==item.coord[i][1]){
                  ys.push(item.coord[i])
               }
               if(zs[0][2]==item.coord[i][2]){
                  zs.push(item.coord[i])
               }
             }
          }
          item.coord.forEach(function (item1,index1) {
              x+=item1[0];
              y+=item1[1];
              z+=item1[2];
          })
          const centerLocation={
              coord: {x:x/8, y: y/8, z: z/8},
              labelID: item.labelID,
              seriesID: item.seriesID,
              url: item.url,
              zs:zs,
              ys:ys,
              xs:xs,
              type:"Ribs",
              nodule_avgHU:-(z/8),
          }
          item=centerLocation;
          arr.push(item)
      })
      study.ribsLables=arr;
  }
  if(studyAPIErr){
     study = EMPTYSTUDYCT
     study.tumorLabels = EMPTYNODULELABELS
     study.noduleLabels = EMPTYTUMORLABELS
     study.ribsLables=EMPTYTUMORLABELS
  }
  if(labelAPIErr)
  {
     study.tumorLabels = EMPTYNODULELABELS
     study.noduleLabels = EMPTYTUMORLABELS
     study.ribsLables=EMPTYTUMORLABELS
  }

  Object.values(study.series).forEach(series =>{
    series.instances.x.map(ins=>ins.url = URL_PREFIX + ins.url)
    series.instances.y.map(ins=>ins.url = URL_PREFIX + ins.url)
    series.instances.z.map(ins=>ins.url = DICOM_URL_PREFIX + ins.url)
  })

  addCTMetaData(study)
  Object.values(study.series).forEach(series => {
    series['stacks'] = {
      x: {
        currentImageIdIndex: 0,
        imageIds: series.instances.x.map(ins => ins.url)
      },
      z: {
        currentImageIdIndex: 0,
        imageIds: series.instances.z.map(ins => ins.url)
      },
      y: {
        currentImageIdIndex: 0,
        imageIds: series.instances.y.map(ins => ins.url)
      },
    }
  })

  if(canOpenReport(study))
  {
    report = await api.getReport({taskID: study.taskID})	
      .catch((err)=>{
      message.error("API error, getReport",60)
      reportAPIErr = true
    })
  }

  if(reportAPIErr)
  {
     report= EMPTYREPORT
  }

  let data = {study, report}
  // get label task list
  return await data
}

const mapStateToProps = (state, ownProps) => {
  // console.log(state, ownProps);
  // NOTICE: use redux to control state
  return Object.assign({}, ownProps, state)
}
CTViewerPage = connect(mapStateToProps)(CTViewerPage)
CTViewerPage = LoadingWrapper(CTViewerPage, getData)
CTViewerPage = injectIntl(CTViewerPage)
export default CTViewerPage
